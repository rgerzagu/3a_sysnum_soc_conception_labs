#-------------------------------------------------------------------------------
#-- Description    : initial settings for Vivado
#-------------------------------------------------------------------------------

# set the boards
set boards_path "./zybo-z7-10/"
# now set the board
set_param board.repoPaths ${boards_path}
puts "Set Board Part RepoPath: [get_param board.repoPaths]"

