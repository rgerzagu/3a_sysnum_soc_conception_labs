# List of modification done in Vivado project. 


This corresponds to the version used for the student. As they get a full version of the project, several modification have been done in order to make the project no working when exported. 
Below the list of all modifications done in the Vivado project.
- In top block
	- Link between axi_vdma_0 [M_AXI_S2MM] and the control block [S_00_AXI] (See Figure remove1)
	- Link between hdmi_tx and the output hdmi_tx (see Figure remove2)
- In camera in 
	- Link between AXI_Lite_Reg_Intf and AXI_Lite_Reg_Intf in AXI_GammaCorrection (see Figure remove3)
- In control block 
	- Clock values 
		clock name | Initial (good values) | Changed to (wrong values)
		clk_out1 | 50 MHz | 50 MHz 
		clk_out2 | 150 MHz | 10MHz 
		clk_out3 | 200MHz | 250MHz 
- In video_out  (See Figure remove4)
	- video_in to video_in in v_axi4s_vid_out_0 
	- ctrl to ctrl in vtg 

