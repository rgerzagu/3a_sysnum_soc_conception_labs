// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Wed Mar 28 23:51:38 2018
// Host        : DLT02-RO running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top system_edge_detect_0_1 -prefix
//               system_edge_detect_0_1_ system_edge_detect_0_0_sim_netlist.v
// Design      : system_edge_detect_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module system_edge_detect_0_1_AXIvideo2Mat
   (ap_rst_n_inv,
    stream_in_TREADY,
    start_once_reg,
    \SRL_SIG_reg[1][0] ,
    D,
    \SRL_SIG_reg[0][7] ,
    \SRL_SIG_reg[0][7]_0 ,
    ap_clk,
    ap_rst_n,
    start_for_CvtColor_U0_full_n,
    stream_in_TVALID,
    img0_data_stream_2_s_full_n,
    img0_data_stream_1_s_full_n,
    img0_data_stream_0_s_full_n,
    stream_in_TLAST,
    stream_in_TUSER,
    stream_in_TDATA);
  output ap_rst_n_inv;
  output stream_in_TREADY;
  output start_once_reg;
  output \SRL_SIG_reg[1][0] ;
  output [7:0]D;
  output [7:0]\SRL_SIG_reg[0][7] ;
  output [7:0]\SRL_SIG_reg[0][7]_0 ;
  input ap_clk;
  input ap_rst_n;
  input start_for_CvtColor_U0_full_n;
  input stream_in_TVALID;
  input img0_data_stream_2_s_full_n;
  input img0_data_stream_1_s_full_n;
  input img0_data_stream_0_s_full_n;
  input [0:0]stream_in_TLAST;
  input [0:0]stream_in_TUSER;
  input [23:0]stream_in_TDATA;

  wire AXI_video_strm_V_data_V_0_ack_in;
  wire [23:0]AXI_video_strm_V_data_V_0_data_out;
  wire AXI_video_strm_V_data_V_0_load_A;
  wire AXI_video_strm_V_data_V_0_load_B;
  wire [23:0]AXI_video_strm_V_data_V_0_payload_A;
  wire [23:0]AXI_video_strm_V_data_V_0_payload_B;
  wire AXI_video_strm_V_data_V_0_sel;
  wire AXI_video_strm_V_data_V_0_sel2;
  wire AXI_video_strm_V_data_V_0_sel_rd_i_1_n_2;
  wire AXI_video_strm_V_data_V_0_sel_wr;
  wire AXI_video_strm_V_data_V_0_sel_wr_i_1_n_2;
  wire [1:1]AXI_video_strm_V_data_V_0_state;
  wire \AXI_video_strm_V_data_V_0_state[0]_i_1_n_2 ;
  wire \AXI_video_strm_V_data_V_0_state_reg_n_2_[0] ;
  wire [1:1]AXI_video_strm_V_dest_V_0_state;
  wire \AXI_video_strm_V_dest_V_0_state[0]_i_1_n_2 ;
  wire \AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2 ;
  wire \AXI_video_strm_V_dest_V_0_state[1]_i_4_n_2 ;
  wire \AXI_video_strm_V_dest_V_0_state_reg_n_2_[0] ;
  wire AXI_video_strm_V_last_V_0_ack_in;
  wire AXI_video_strm_V_last_V_0_data_out;
  wire AXI_video_strm_V_last_V_0_payload_A;
  wire \AXI_video_strm_V_last_V_0_payload_A[0]_i_1_n_2 ;
  wire AXI_video_strm_V_last_V_0_payload_B;
  wire \AXI_video_strm_V_last_V_0_payload_B[0]_i_1_n_2 ;
  wire AXI_video_strm_V_last_V_0_sel;
  wire AXI_video_strm_V_last_V_0_sel_rd_i_1_n_2;
  wire AXI_video_strm_V_last_V_0_sel_wr;
  wire AXI_video_strm_V_last_V_0_sel_wr_i_1_n_2;
  wire [1:1]AXI_video_strm_V_last_V_0_state;
  wire \AXI_video_strm_V_last_V_0_state[0]_i_1_n_2 ;
  wire \AXI_video_strm_V_last_V_0_state_reg_n_2_[0] ;
  wire AXI_video_strm_V_user_V_0_ack_in;
  wire AXI_video_strm_V_user_V_0_payload_A;
  wire \AXI_video_strm_V_user_V_0_payload_A[0]_i_1_n_2 ;
  wire AXI_video_strm_V_user_V_0_payload_B;
  wire \AXI_video_strm_V_user_V_0_payload_B[0]_i_1_n_2 ;
  wire AXI_video_strm_V_user_V_0_sel;
  wire AXI_video_strm_V_user_V_0_sel_rd_i_1_n_2;
  wire AXI_video_strm_V_user_V_0_sel_wr;
  wire AXI_video_strm_V_user_V_0_sel_wr_i_1_n_2;
  wire [1:1]AXI_video_strm_V_user_V_0_state;
  wire \AXI_video_strm_V_user_V_0_state[0]_i_1_n_2 ;
  wire \AXI_video_strm_V_user_V_0_state_reg_n_2_[0] ;
  wire [7:0]D;
  wire [7:0]\SRL_SIG_reg[0][7] ;
  wire [7:0]\SRL_SIG_reg[0][7]_0 ;
  wire \SRL_SIG_reg[1][0] ;
  wire \ap_CS_fsm[0]_i_2_n_2 ;
  wire \ap_CS_fsm[0]_i_3_n_2 ;
  wire \ap_CS_fsm[0]_i_4_n_2 ;
  wire \ap_CS_fsm[5]_i_2_n_2 ;
  wire ap_CS_fsm_pp1_stage0;
  wire ap_CS_fsm_pp2_stage0;
  wire \ap_CS_fsm_reg_n_2_[0] ;
  wire ap_CS_fsm_state10;
  wire ap_CS_fsm_state2;
  wire ap_CS_fsm_state3;
  wire ap_CS_fsm_state4;
  wire ap_CS_fsm_state7;
  wire [7:0]ap_NS_fsm;
  wire ap_clk;
  wire ap_enable_reg_pp1_iter0;
  wire ap_enable_reg_pp1_iter0_i_1_n_2;
  wire ap_enable_reg_pp1_iter0_i_3_n_2;
  wire ap_enable_reg_pp1_iter0_i_4_n_2;
  wire ap_enable_reg_pp1_iter1_i_1_n_2;
  wire ap_enable_reg_pp1_iter1_reg_n_2;
  wire ap_enable_reg_pp2_iter0;
  wire ap_enable_reg_pp2_iter0_i_1_n_2;
  wire ap_enable_reg_pp2_iter0_i_2_n_2;
  wire ap_enable_reg_pp2_iter1_i_1_n_2;
  wire ap_enable_reg_pp2_iter1_reg_n_2;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire [23:0]axi_data_V1_reg_181;
  wire \axi_data_V1_reg_181[0]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[10]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[11]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[12]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[13]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[14]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[15]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[16]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[17]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[18]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[19]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[1]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[20]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[21]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[22]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[23]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[2]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[3]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[4]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[5]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[6]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[7]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[8]_i_1_n_2 ;
  wire \axi_data_V1_reg_181[9]_i_1_n_2 ;
  wire [23:0]axi_data_V_1_reg_236;
  wire \axi_data_V_1_reg_236[0]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[10]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[11]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[12]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[13]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[14]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[15]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[16]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[17]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[18]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[19]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[1]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[20]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[21]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[22]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[23]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[2]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[3]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[4]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[5]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[6]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[7]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[8]_i_1_n_2 ;
  wire \axi_data_V_1_reg_236[9]_i_1_n_2 ;
  wire [23:0]axi_data_V_3_reg_295;
  wire \axi_data_V_3_reg_295[0]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[10]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[11]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[12]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[13]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[14]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[15]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[16]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[17]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[18]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[19]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[1]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[20]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[21]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[22]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[23]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[2]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[3]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[4]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[5]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[6]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[7]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[8]_i_1_n_2 ;
  wire \axi_data_V_3_reg_295[9]_i_1_n_2 ;
  wire axi_last_V1_reg_171;
  wire \axi_last_V1_reg_171[0]_i_1_n_2 ;
  wire axi_last_V_3_reg_283;
  wire \axi_last_V_3_reg_283[0]_i_1_n_2 ;
  wire brmerge_reg_429;
  wire brmerge_reg_4290;
  wire \brmerge_reg_429[0]_i_1_n_2 ;
  wire \brmerge_reg_429[0]_i_2_n_2 ;
  wire \brmerge_reg_429[0]_i_3_n_2 ;
  wire eol_1_reg_225;
  wire \eol_1_reg_225[0]_i_2_n_2 ;
  wire \eol_2_reg_272[0]_i_1_n_2 ;
  wire \eol_2_reg_272[0]_i_2_n_2 ;
  wire \eol_2_reg_272_reg_n_2_[0] ;
  wire eol_reg_213;
  wire \eol_reg_213[0]_i_1_n_2 ;
  wire \eol_reg_213[0]_i_2_n_2 ;
  wire \eol_reg_213_reg_n_2_[0] ;
  wire exitcond_fu_338_p2;
  wire exitcond_reg_4200;
  wire \exitcond_reg_420[0]_i_1_n_2 ;
  wire \exitcond_reg_420_reg_n_2_[0] ;
  wire [9:0]i_V_fu_332_p2;
  wire [9:0]i_V_reg_415;
  wire \i_V_reg_415[9]_i_2_n_2 ;
  wire img0_data_stream_0_s_full_n;
  wire img0_data_stream_1_s_full_n;
  wire img0_data_stream_2_s_full_n;
  wire [10:0]j_V_fu_344_p2;
  wire p_1_in;
  wire sof_1_fu_128;
  wire sof_1_fu_1280;
  wire \sof_1_fu_128[0]_i_1_n_2 ;
  wire start_for_CvtColor_U0_full_n;
  wire start_once_reg;
  wire start_once_reg_i_1__2_n_2;
  wire [23:0]stream_in_TDATA;
  wire [0:0]stream_in_TLAST;
  wire stream_in_TREADY;
  wire [0:0]stream_in_TUSER;
  wire stream_in_TVALID;
  wire t_V_3_reg_202;
  wire \t_V_3_reg_202[10]_i_5_n_2 ;
  wire [10:0]t_V_3_reg_202_reg__0;
  wire [9:0]t_V_reg_191;
  wire [23:0]tmp_data_V_reg_391;
  wire tmp_last_V_reg_399;

  LUT3 #(
    .INIT(8'h45)) 
    \AXI_video_strm_V_data_V_0_payload_A[23]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_sel_wr),
        .I1(AXI_video_strm_V_data_V_0_ack_in),
        .I2(\AXI_video_strm_V_data_V_0_state_reg_n_2_[0] ),
        .O(AXI_video_strm_V_data_V_0_load_A));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[0]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[0]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[10] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[10]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[10]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[11] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[11]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[11]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[12] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[12]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[12]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[13] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[13]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[13]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[14] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[14]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[14]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[15] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[15]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[15]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[16] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[16]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[16]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[17] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[17]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[17]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[18] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[18]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[18]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[19] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[19]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[19]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[1]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[1]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[20] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[20]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[20]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[21] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[21]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[21]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[22] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[22]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[22]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[23] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[23]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[23]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[2]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[2]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[3]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[3]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[4]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[4]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[5] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[5]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[5]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[6] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[6]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[6]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[7] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[7]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[7]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[8] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[8]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[8]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_A_reg[9] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_A),
        .D(stream_in_TDATA[9]),
        .Q(AXI_video_strm_V_data_V_0_payload_A[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h8A)) 
    \AXI_video_strm_V_data_V_0_payload_B[23]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_sel_wr),
        .I1(AXI_video_strm_V_data_V_0_ack_in),
        .I2(\AXI_video_strm_V_data_V_0_state_reg_n_2_[0] ),
        .O(AXI_video_strm_V_data_V_0_load_B));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[0]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[0]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[10] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[10]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[10]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[11] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[11]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[11]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[12] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[12]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[12]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[13] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[13]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[13]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[14] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[14]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[14]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[15] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[15]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[15]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[16] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[16]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[16]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[17] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[17]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[17]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[18] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[18]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[18]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[19] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[19]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[19]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[1]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[1]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[20] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[20]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[20]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[21] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[21]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[21]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[22] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[22]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[22]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[23] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[23]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[23]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[2]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[2]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[3]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[3]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[4]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[4]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[5] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[5]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[5]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[6] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[6]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[6]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[7] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[7]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[7]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[8] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[8]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[8]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_0_payload_B_reg[9] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_load_B),
        .D(stream_in_TDATA[9]),
        .Q(AXI_video_strm_V_data_V_0_payload_B[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'h9)) 
    AXI_video_strm_V_data_V_0_sel_rd_i_1
       (.I0(\AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2 ),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .O(AXI_video_strm_V_data_V_0_sel_rd_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_data_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_data_V_0_sel_rd_i_1_n_2),
        .Q(AXI_video_strm_V_data_V_0_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_data_V_0_sel_wr_i_1
       (.I0(AXI_video_strm_V_data_V_0_ack_in),
        .I1(stream_in_TVALID),
        .I2(AXI_video_strm_V_data_V_0_sel_wr),
        .O(AXI_video_strm_V_data_V_0_sel_wr_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_data_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_data_V_0_sel_wr_i_1_n_2),
        .Q(AXI_video_strm_V_data_V_0_sel_wr),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT4 #(
    .INIT(16'hFD88)) 
    \AXI_video_strm_V_data_V_0_state[0]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_ack_in),
        .I1(stream_in_TVALID),
        .I2(\AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2 ),
        .I3(\AXI_video_strm_V_data_V_0_state_reg_n_2_[0] ),
        .O(\AXI_video_strm_V_data_V_0_state[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT4 #(
    .INIT(16'h7F77)) 
    \AXI_video_strm_V_data_V_0_state[1]_i_1 
       (.I0(\AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2 ),
        .I1(\AXI_video_strm_V_data_V_0_state_reg_n_2_[0] ),
        .I2(stream_in_TVALID),
        .I3(AXI_video_strm_V_data_V_0_ack_in),
        .O(AXI_video_strm_V_data_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_data_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_data_V_0_state[0]_i_1_n_2 ),
        .Q(\AXI_video_strm_V_data_V_0_state_reg_n_2_[0] ),
        .R(ap_rst_n_inv));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_data_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_data_V_0_state),
        .Q(AXI_video_strm_V_data_V_0_ack_in),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT4 #(
    .INIT(16'hFD88)) 
    \AXI_video_strm_V_dest_V_0_state[0]_i_1 
       (.I0(stream_in_TREADY),
        .I1(stream_in_TVALID),
        .I2(\AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2 ),
        .I3(\AXI_video_strm_V_dest_V_0_state_reg_n_2_[0] ),
        .O(\AXI_video_strm_V_dest_V_0_state[0]_i_1_n_2 ));
  LUT1 #(
    .INIT(2'h1)) 
    \AXI_video_strm_V_dest_V_0_state[1]_i_1 
       (.I0(ap_rst_n),
        .O(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT4 #(
    .INIT(16'h7F77)) 
    \AXI_video_strm_V_dest_V_0_state[1]_i_2 
       (.I0(\AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2 ),
        .I1(\AXI_video_strm_V_dest_V_0_state_reg_n_2_[0] ),
        .I2(stream_in_TVALID),
        .I3(stream_in_TREADY),
        .O(AXI_video_strm_V_dest_V_0_state));
  LUT5 #(
    .INIT(32'h00000BBB)) 
    \AXI_video_strm_V_dest_V_0_state[1]_i_3 
       (.I0(brmerge_reg_429),
        .I1(\SRL_SIG_reg[1][0] ),
        .I2(\AXI_video_strm_V_data_V_0_state_reg_n_2_[0] ),
        .I3(ap_CS_fsm_state2),
        .I4(\AXI_video_strm_V_dest_V_0_state[1]_i_4_n_2 ),
        .O(\AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \AXI_video_strm_V_dest_V_0_state[1]_i_4 
       (.I0(\AXI_video_strm_V_data_V_0_state_reg_n_2_[0] ),
        .I1(ap_CS_fsm_pp2_stage0),
        .I2(\eol_2_reg_272_reg_n_2_[0] ),
        .I3(ap_enable_reg_pp2_iter1_reg_n_2),
        .O(\AXI_video_strm_V_dest_V_0_state[1]_i_4_n_2 ));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_dest_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_dest_V_0_state[0]_i_1_n_2 ),
        .Q(\AXI_video_strm_V_dest_V_0_state_reg_n_2_[0] ),
        .R(ap_rst_n_inv));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_dest_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_dest_V_0_state),
        .Q(stream_in_TREADY),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hEFEE2022)) 
    \AXI_video_strm_V_last_V_0_payload_A[0]_i_1 
       (.I0(stream_in_TLAST),
        .I1(AXI_video_strm_V_last_V_0_sel_wr),
        .I2(AXI_video_strm_V_last_V_0_ack_in),
        .I3(\AXI_video_strm_V_last_V_0_state_reg_n_2_[0] ),
        .I4(AXI_video_strm_V_last_V_0_payload_A),
        .O(\AXI_video_strm_V_last_V_0_payload_A[0]_i_1_n_2 ));
  FDRE \AXI_video_strm_V_last_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_last_V_0_payload_A[0]_i_1_n_2 ),
        .Q(AXI_video_strm_V_last_V_0_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hBFBB8088)) 
    \AXI_video_strm_V_last_V_0_payload_B[0]_i_1 
       (.I0(stream_in_TLAST),
        .I1(AXI_video_strm_V_last_V_0_sel_wr),
        .I2(AXI_video_strm_V_last_V_0_ack_in),
        .I3(\AXI_video_strm_V_last_V_0_state_reg_n_2_[0] ),
        .I4(AXI_video_strm_V_last_V_0_payload_B),
        .O(\AXI_video_strm_V_last_V_0_payload_B[0]_i_1_n_2 ));
  FDRE \AXI_video_strm_V_last_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_last_V_0_payload_B[0]_i_1_n_2 ),
        .Q(AXI_video_strm_V_last_V_0_payload_B),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hB4)) 
    AXI_video_strm_V_last_V_0_sel_rd_i_1
       (.I0(\AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2 ),
        .I1(\AXI_video_strm_V_last_V_0_state_reg_n_2_[0] ),
        .I2(AXI_video_strm_V_last_V_0_sel),
        .O(AXI_video_strm_V_last_V_0_sel_rd_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_last_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_last_V_0_sel_rd_i_1_n_2),
        .Q(AXI_video_strm_V_last_V_0_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_last_V_0_sel_wr_i_1
       (.I0(AXI_video_strm_V_last_V_0_ack_in),
        .I1(stream_in_TVALID),
        .I2(AXI_video_strm_V_last_V_0_sel_wr),
        .O(AXI_video_strm_V_last_V_0_sel_wr_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_last_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_last_V_0_sel_wr_i_1_n_2),
        .Q(AXI_video_strm_V_last_V_0_sel_wr),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT4 #(
    .INIT(16'hFD88)) 
    \AXI_video_strm_V_last_V_0_state[0]_i_1 
       (.I0(AXI_video_strm_V_last_V_0_ack_in),
        .I1(stream_in_TVALID),
        .I2(\AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2 ),
        .I3(\AXI_video_strm_V_last_V_0_state_reg_n_2_[0] ),
        .O(\AXI_video_strm_V_last_V_0_state[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT4 #(
    .INIT(16'h7F77)) 
    \AXI_video_strm_V_last_V_0_state[1]_i_1 
       (.I0(\AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2 ),
        .I1(\AXI_video_strm_V_last_V_0_state_reg_n_2_[0] ),
        .I2(stream_in_TVALID),
        .I3(AXI_video_strm_V_last_V_0_ack_in),
        .O(AXI_video_strm_V_last_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_last_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_last_V_0_state[0]_i_1_n_2 ),
        .Q(\AXI_video_strm_V_last_V_0_state_reg_n_2_[0] ),
        .R(ap_rst_n_inv));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_last_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_last_V_0_state),
        .Q(AXI_video_strm_V_last_V_0_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hEFEE2022)) 
    \AXI_video_strm_V_user_V_0_payload_A[0]_i_1 
       (.I0(stream_in_TUSER),
        .I1(AXI_video_strm_V_user_V_0_sel_wr),
        .I2(AXI_video_strm_V_user_V_0_ack_in),
        .I3(\AXI_video_strm_V_user_V_0_state_reg_n_2_[0] ),
        .I4(AXI_video_strm_V_user_V_0_payload_A),
        .O(\AXI_video_strm_V_user_V_0_payload_A[0]_i_1_n_2 ));
  FDRE \AXI_video_strm_V_user_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_user_V_0_payload_A[0]_i_1_n_2 ),
        .Q(AXI_video_strm_V_user_V_0_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hBFBB8088)) 
    \AXI_video_strm_V_user_V_0_payload_B[0]_i_1 
       (.I0(stream_in_TUSER),
        .I1(AXI_video_strm_V_user_V_0_sel_wr),
        .I2(AXI_video_strm_V_user_V_0_ack_in),
        .I3(\AXI_video_strm_V_user_V_0_state_reg_n_2_[0] ),
        .I4(AXI_video_strm_V_user_V_0_payload_B),
        .O(\AXI_video_strm_V_user_V_0_payload_B[0]_i_1_n_2 ));
  FDRE \AXI_video_strm_V_user_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_user_V_0_payload_B[0]_i_1_n_2 ),
        .Q(AXI_video_strm_V_user_V_0_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hB4)) 
    AXI_video_strm_V_user_V_0_sel_rd_i_1
       (.I0(\AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2 ),
        .I1(\AXI_video_strm_V_user_V_0_state_reg_n_2_[0] ),
        .I2(AXI_video_strm_V_user_V_0_sel),
        .O(AXI_video_strm_V_user_V_0_sel_rd_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_user_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_user_V_0_sel_rd_i_1_n_2),
        .Q(AXI_video_strm_V_user_V_0_sel),
        .R(ap_rst_n_inv));
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_user_V_0_sel_wr_i_1
       (.I0(AXI_video_strm_V_user_V_0_ack_in),
        .I1(stream_in_TVALID),
        .I2(AXI_video_strm_V_user_V_0_sel_wr),
        .O(AXI_video_strm_V_user_V_0_sel_wr_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_user_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_user_V_0_sel_wr_i_1_n_2),
        .Q(AXI_video_strm_V_user_V_0_sel_wr),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT4 #(
    .INIT(16'hFD88)) 
    \AXI_video_strm_V_user_V_0_state[0]_i_1 
       (.I0(AXI_video_strm_V_user_V_0_ack_in),
        .I1(stream_in_TVALID),
        .I2(\AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2 ),
        .I3(\AXI_video_strm_V_user_V_0_state_reg_n_2_[0] ),
        .O(\AXI_video_strm_V_user_V_0_state[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT4 #(
    .INIT(16'h7F77)) 
    \AXI_video_strm_V_user_V_0_state[1]_i_1 
       (.I0(\AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2 ),
        .I1(\AXI_video_strm_V_user_V_0_state_reg_n_2_[0] ),
        .I2(stream_in_TVALID),
        .I3(AXI_video_strm_V_user_V_0_ack_in),
        .O(AXI_video_strm_V_user_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_user_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_user_V_0_state[0]_i_1_n_2 ),
        .Q(\AXI_video_strm_V_user_V_0_state_reg_n_2_[0] ),
        .R(ap_rst_n_inv));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_user_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_user_V_0_state),
        .Q(AXI_video_strm_V_user_V_0_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][0]_i_1__0 
       (.I0(axi_data_V_1_reg_236[16]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[16]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[16]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][0]_i_1__1 
       (.I0(axi_data_V_1_reg_236[8]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[8]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[8]),
        .O(\SRL_SIG_reg[0][7] [0]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][0]_i_1__2 
       (.I0(axi_data_V_1_reg_236[0]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[0]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[0]),
        .O(\SRL_SIG_reg[0][7]_0 [0]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][1]_i_1 
       (.I0(axi_data_V_1_reg_236[17]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[17]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[17]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][1]_i_1__0 
       (.I0(axi_data_V_1_reg_236[9]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[9]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[9]),
        .O(\SRL_SIG_reg[0][7] [1]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][1]_i_1__1 
       (.I0(axi_data_V_1_reg_236[1]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[1]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[1]),
        .O(\SRL_SIG_reg[0][7]_0 [1]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][2]_i_1 
       (.I0(axi_data_V_1_reg_236[18]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[18]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[18]),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][2]_i_1__0 
       (.I0(axi_data_V_1_reg_236[10]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[10]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[10]),
        .O(\SRL_SIG_reg[0][7] [2]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][2]_i_1__1 
       (.I0(axi_data_V_1_reg_236[2]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[2]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[2]),
        .O(\SRL_SIG_reg[0][7]_0 [2]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][3]_i_1 
       (.I0(axi_data_V_1_reg_236[19]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[19]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[19]),
        .O(D[3]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][3]_i_1__0 
       (.I0(axi_data_V_1_reg_236[11]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[11]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[11]),
        .O(\SRL_SIG_reg[0][7] [3]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][3]_i_1__1 
       (.I0(axi_data_V_1_reg_236[3]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[3]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[3]),
        .O(\SRL_SIG_reg[0][7]_0 [3]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][4]_i_1 
       (.I0(axi_data_V_1_reg_236[20]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[20]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[20]),
        .O(D[4]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][4]_i_1__0 
       (.I0(axi_data_V_1_reg_236[12]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[12]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[12]),
        .O(\SRL_SIG_reg[0][7] [4]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][4]_i_1__1 
       (.I0(axi_data_V_1_reg_236[4]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[4]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[4]),
        .O(\SRL_SIG_reg[0][7]_0 [4]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][5]_i_1 
       (.I0(axi_data_V_1_reg_236[21]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[21]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[21]),
        .O(D[5]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][5]_i_1__0 
       (.I0(axi_data_V_1_reg_236[13]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[13]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[13]),
        .O(\SRL_SIG_reg[0][7] [5]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][5]_i_1__1 
       (.I0(axi_data_V_1_reg_236[5]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[5]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[5]),
        .O(\SRL_SIG_reg[0][7]_0 [5]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][6]_i_1 
       (.I0(axi_data_V_1_reg_236[22]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[22]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[22]),
        .O(D[6]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][6]_i_1__0 
       (.I0(axi_data_V_1_reg_236[14]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[14]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[14]),
        .O(\SRL_SIG_reg[0][7] [6]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][6]_i_1__1 
       (.I0(axi_data_V_1_reg_236[6]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[6]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[6]),
        .O(\SRL_SIG_reg[0][7]_0 [6]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][7]_i_1 
       (.I0(axi_data_V_1_reg_236[23]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[23]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[23]),
        .O(D[7]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][7]_i_1__0 
       (.I0(axi_data_V_1_reg_236[15]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[15]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[15]),
        .O(\SRL_SIG_reg[0][7] [7]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'h2000)) 
    \SRL_SIG[0][7]_i_1__2 
       (.I0(\ap_CS_fsm[5]_i_2_n_2 ),
        .I1(\exitcond_reg_420_reg_n_2_[0] ),
        .I2(ap_enable_reg_pp1_iter1_reg_n_2),
        .I3(ap_CS_fsm_pp1_stage0),
        .O(\SRL_SIG_reg[1][0] ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \SRL_SIG[0][7]_i_2__1 
       (.I0(axi_data_V_1_reg_236[7]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_payload_B[7]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[7]),
        .O(\SRL_SIG_reg[0][7]_0 [7]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h222F2222)) 
    \ap_CS_fsm[0]_i_1__2 
       (.I0(ap_CS_fsm_state4),
        .I1(\ap_CS_fsm[0]_i_2_n_2 ),
        .I2(start_once_reg),
        .I3(start_for_CvtColor_U0_full_n),
        .I4(\ap_CS_fsm_reg_n_2_[0] ),
        .O(ap_NS_fsm[0]));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \ap_CS_fsm[0]_i_2 
       (.I0(t_V_reg_191[8]),
        .I1(t_V_reg_191[3]),
        .I2(\ap_CS_fsm[0]_i_3_n_2 ),
        .I3(\ap_CS_fsm[0]_i_4_n_2 ),
        .O(\ap_CS_fsm[0]_i_2_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \ap_CS_fsm[0]_i_3 
       (.I0(t_V_reg_191[1]),
        .I1(t_V_reg_191[0]),
        .I2(t_V_reg_191[5]),
        .I3(t_V_reg_191[2]),
        .O(\ap_CS_fsm[0]_i_3_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \ap_CS_fsm[0]_i_4 
       (.I0(t_V_reg_191[9]),
        .I1(t_V_reg_191[4]),
        .I2(t_V_reg_191[7]),
        .I3(t_V_reg_191[6]),
        .O(\ap_CS_fsm[0]_i_4_n_2 ));
  LUT5 #(
    .INIT(32'hFFF44444)) 
    \ap_CS_fsm[1]_i_1 
       (.I0(ap_NS_fsm[2]),
        .I1(ap_CS_fsm_state2),
        .I2(start_once_reg),
        .I3(start_for_CvtColor_U0_full_n),
        .I4(\ap_CS_fsm_reg_n_2_[0] ),
        .O(ap_NS_fsm[1]));
  LUT5 #(
    .INIT(32'h88800080)) 
    \ap_CS_fsm[2]_i_1 
       (.I0(ap_CS_fsm_state2),
        .I1(\AXI_video_strm_V_data_V_0_state_reg_n_2_[0] ),
        .I2(AXI_video_strm_V_user_V_0_payload_A),
        .I3(AXI_video_strm_V_user_V_0_sel),
        .I4(AXI_video_strm_V_user_V_0_payload_B),
        .O(ap_NS_fsm[2]));
  LUT2 #(
    .INIT(4'hE)) 
    \ap_CS_fsm[3]_i_1__3 
       (.I0(ap_CS_fsm_state3),
        .I1(ap_CS_fsm_state10),
        .O(ap_NS_fsm[3]));
  LUT5 #(
    .INIT(32'hFFBFAAAA)) 
    \ap_CS_fsm[4]_i_1 
       (.I0(p_1_in),
        .I1(exitcond_reg_4200),
        .I2(ap_enable_reg_pp1_iter1_reg_n_2),
        .I3(ap_enable_reg_pp1_iter0),
        .I4(ap_CS_fsm_pp1_stage0),
        .O(ap_NS_fsm[4]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \ap_CS_fsm[4]_i_2 
       (.I0(ap_CS_fsm_state4),
        .I1(\ap_CS_fsm[0]_i_2_n_2 ),
        .O(p_1_in));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT4 #(
    .INIT(16'hAA8A)) 
    \ap_CS_fsm[4]_i_3 
       (.I0(ap_CS_fsm_pp1_stage0),
        .I1(\exitcond_reg_420_reg_n_2_[0] ),
        .I2(ap_enable_reg_pp1_iter1_reg_n_2),
        .I3(\ap_CS_fsm[5]_i_2_n_2 ),
        .O(exitcond_reg_4200));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h0000E000)) 
    \ap_CS_fsm[5]_i_1 
       (.I0(\ap_CS_fsm[5]_i_2_n_2 ),
        .I1(\exitcond_reg_420_reg_n_2_[0] ),
        .I2(ap_CS_fsm_pp1_stage0),
        .I3(ap_enable_reg_pp1_iter1_reg_n_2),
        .I4(ap_enable_reg_pp1_iter0),
        .O(ap_NS_fsm[5]));
  LUT5 #(
    .INIT(32'h80808000)) 
    \ap_CS_fsm[5]_i_2 
       (.I0(img0_data_stream_0_s_full_n),
        .I1(img0_data_stream_1_s_full_n),
        .I2(img0_data_stream_2_s_full_n),
        .I3(brmerge_reg_429),
        .I4(\AXI_video_strm_V_data_V_0_state_reg_n_2_[0] ),
        .O(\ap_CS_fsm[5]_i_2_n_2 ));
  LUT6 #(
    .INIT(64'hFFAAFFAAABAAFFAA)) 
    \ap_CS_fsm[6]_i_1 
       (.I0(ap_CS_fsm_state7),
        .I1(\AXI_video_strm_V_data_V_0_state_reg_n_2_[0] ),
        .I2(\eol_2_reg_272_reg_n_2_[0] ),
        .I3(ap_CS_fsm_pp2_stage0),
        .I4(ap_enable_reg_pp2_iter1_reg_n_2),
        .I5(ap_enable_reg_pp2_iter0),
        .O(ap_NS_fsm[6]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT5 #(
    .INIT(32'h0000E000)) 
    \ap_CS_fsm[7]_i_1 
       (.I0(\AXI_video_strm_V_data_V_0_state_reg_n_2_[0] ),
        .I1(\eol_2_reg_272_reg_n_2_[0] ),
        .I2(ap_CS_fsm_pp2_stage0),
        .I3(ap_enable_reg_pp2_iter1_reg_n_2),
        .I4(ap_enable_reg_pp2_iter0),
        .O(ap_NS_fsm[7]));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[0]),
        .Q(\ap_CS_fsm_reg_n_2_[0] ),
        .S(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ap_CS_fsm_state2),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[2]),
        .Q(ap_CS_fsm_state3),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[3]),
        .Q(ap_CS_fsm_state4),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[4] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[4]),
        .Q(ap_CS_fsm_pp1_stage0),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[5] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[5]),
        .Q(ap_CS_fsm_state7),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[6] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[6]),
        .Q(ap_CS_fsm_pp2_stage0),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[7] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[7]),
        .Q(ap_CS_fsm_state10),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h70707000)) 
    ap_enable_reg_pp1_iter0_i_1
       (.I0(exitcond_fu_338_p2),
        .I1(exitcond_reg_4200),
        .I2(ap_rst_n),
        .I3(p_1_in),
        .I4(ap_enable_reg_pp1_iter0),
        .O(ap_enable_reg_pp1_iter0_i_1_n_2));
  LUT5 #(
    .INIT(32'h04000000)) 
    ap_enable_reg_pp1_iter0_i_2
       (.I0(t_V_3_reg_202_reg__0[9]),
        .I1(t_V_3_reg_202_reg__0[8]),
        .I2(t_V_3_reg_202_reg__0[2]),
        .I3(ap_enable_reg_pp1_iter0_i_3_n_2),
        .I4(ap_enable_reg_pp1_iter0_i_4_n_2),
        .O(exitcond_fu_338_p2));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT4 #(
    .INIT(16'h0002)) 
    ap_enable_reg_pp1_iter0_i_3
       (.I0(t_V_3_reg_202_reg__0[10]),
        .I1(t_V_3_reg_202_reg__0[3]),
        .I2(t_V_3_reg_202_reg__0[4]),
        .I3(t_V_3_reg_202_reg__0[1]),
        .O(ap_enable_reg_pp1_iter0_i_3_n_2));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    ap_enable_reg_pp1_iter0_i_4
       (.I0(t_V_3_reg_202_reg__0[7]),
        .I1(t_V_3_reg_202_reg__0[6]),
        .I2(t_V_3_reg_202_reg__0[5]),
        .I3(t_V_3_reg_202_reg__0[0]),
        .O(ap_enable_reg_pp1_iter0_i_4_n_2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp1_iter0_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp1_iter0_i_1_n_2),
        .Q(ap_enable_reg_pp1_iter0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hCCCCC4CC00000400)) 
    ap_enable_reg_pp1_iter1_i_1
       (.I0(p_1_in),
        .I1(ap_rst_n),
        .I2(\ap_CS_fsm[5]_i_2_n_2 ),
        .I3(ap_enable_reg_pp1_iter1_reg_n_2),
        .I4(\exitcond_reg_420_reg_n_2_[0] ),
        .I5(ap_enable_reg_pp1_iter0),
        .O(ap_enable_reg_pp1_iter1_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp1_iter1_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp1_iter1_i_1_n_2),
        .Q(ap_enable_reg_pp1_iter1_reg_n_2),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000000057777777)) 
    ap_enable_reg_pp2_iter0_i_1
       (.I0(ap_CS_fsm_pp2_stage0),
        .I1(\eol_2_reg_272_reg_n_2_[0] ),
        .I2(ap_enable_reg_pp2_iter1_reg_n_2),
        .I3(\AXI_video_strm_V_data_V_0_state_reg_n_2_[0] ),
        .I4(AXI_video_strm_V_last_V_0_data_out),
        .I5(ap_enable_reg_pp2_iter0_i_2_n_2),
        .O(ap_enable_reg_pp2_iter0_i_1_n_2));
  LUT3 #(
    .INIT(8'h1F)) 
    ap_enable_reg_pp2_iter0_i_2
       (.I0(ap_enable_reg_pp2_iter0),
        .I1(ap_CS_fsm_state7),
        .I2(ap_rst_n),
        .O(ap_enable_reg_pp2_iter0_i_2_n_2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp2_iter0_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp2_iter0_i_1_n_2),
        .Q(ap_enable_reg_pp2_iter0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hCCCCC4CC00000400)) 
    ap_enable_reg_pp2_iter1_i_1
       (.I0(ap_CS_fsm_state7),
        .I1(ap_rst_n),
        .I2(\AXI_video_strm_V_data_V_0_state_reg_n_2_[0] ),
        .I3(ap_enable_reg_pp2_iter1_reg_n_2),
        .I4(\eol_2_reg_272_reg_n_2_[0] ),
        .I5(ap_enable_reg_pp2_iter0),
        .O(ap_enable_reg_pp2_iter1_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp2_iter1_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp2_iter1_i_1_n_2),
        .Q(ap_enable_reg_pp2_iter1_reg_n_2),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[0]_i_1 
       (.I0(tmp_data_V_reg_391[0]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[0]),
        .O(\axi_data_V1_reg_181[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[10]_i_1 
       (.I0(tmp_data_V_reg_391[10]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[10]),
        .O(\axi_data_V1_reg_181[10]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[11]_i_1 
       (.I0(tmp_data_V_reg_391[11]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[11]),
        .O(\axi_data_V1_reg_181[11]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[12]_i_1 
       (.I0(tmp_data_V_reg_391[12]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[12]),
        .O(\axi_data_V1_reg_181[12]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[13]_i_1 
       (.I0(tmp_data_V_reg_391[13]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[13]),
        .O(\axi_data_V1_reg_181[13]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[14]_i_1 
       (.I0(tmp_data_V_reg_391[14]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[14]),
        .O(\axi_data_V1_reg_181[14]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[15]_i_1 
       (.I0(tmp_data_V_reg_391[15]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[15]),
        .O(\axi_data_V1_reg_181[15]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[16]_i_1 
       (.I0(tmp_data_V_reg_391[16]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[16]),
        .O(\axi_data_V1_reg_181[16]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[17]_i_1 
       (.I0(tmp_data_V_reg_391[17]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[17]),
        .O(\axi_data_V1_reg_181[17]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[18]_i_1 
       (.I0(tmp_data_V_reg_391[18]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[18]),
        .O(\axi_data_V1_reg_181[18]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[19]_i_1 
       (.I0(tmp_data_V_reg_391[19]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[19]),
        .O(\axi_data_V1_reg_181[19]_i_1_n_2 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[1]_i_1 
       (.I0(tmp_data_V_reg_391[1]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[1]),
        .O(\axi_data_V1_reg_181[1]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[20]_i_1 
       (.I0(tmp_data_V_reg_391[20]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[20]),
        .O(\axi_data_V1_reg_181[20]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[21]_i_1 
       (.I0(tmp_data_V_reg_391[21]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[21]),
        .O(\axi_data_V1_reg_181[21]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[22]_i_1 
       (.I0(tmp_data_V_reg_391[22]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[22]),
        .O(\axi_data_V1_reg_181[22]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[23]_i_1 
       (.I0(tmp_data_V_reg_391[23]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[23]),
        .O(\axi_data_V1_reg_181[23]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[2]_i_1 
       (.I0(tmp_data_V_reg_391[2]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[2]),
        .O(\axi_data_V1_reg_181[2]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[3]_i_1 
       (.I0(tmp_data_V_reg_391[3]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[3]),
        .O(\axi_data_V1_reg_181[3]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[4]_i_1 
       (.I0(tmp_data_V_reg_391[4]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[4]),
        .O(\axi_data_V1_reg_181[4]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[5]_i_1 
       (.I0(tmp_data_V_reg_391[5]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[5]),
        .O(\axi_data_V1_reg_181[5]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[6]_i_1 
       (.I0(tmp_data_V_reg_391[6]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[6]),
        .O(\axi_data_V1_reg_181[6]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[7]_i_1 
       (.I0(tmp_data_V_reg_391[7]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[7]),
        .O(\axi_data_V1_reg_181[7]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[8]_i_1 
       (.I0(tmp_data_V_reg_391[8]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[8]),
        .O(\axi_data_V1_reg_181[8]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_data_V1_reg_181[9]_i_1 
       (.I0(tmp_data_V_reg_391[9]),
        .I1(ap_CS_fsm_state3),
        .I2(axi_data_V_3_reg_295[9]),
        .O(\axi_data_V1_reg_181[9]_i_1_n_2 ));
  FDRE \axi_data_V1_reg_181_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[0]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[0]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[10] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[10]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[10]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[11] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[11]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[11]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[12] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[12]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[12]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[13] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[13]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[13]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[14] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[14]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[14]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[15] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[15]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[15]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[16] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[16]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[16]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[17] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[17]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[17]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[18] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[18]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[18]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[19] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[19]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[19]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[1]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[1]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[20] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[20]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[20]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[21] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[21]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[21]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[22] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[22]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[22]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[23] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[23]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[23]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[2]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[2]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[3]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[3]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[4]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[4]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[5]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[5]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[6] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[6]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[6]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[7] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[7]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[7]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[8] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[8]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[8]),
        .R(1'b0));
  FDRE \axi_data_V1_reg_181_reg[9] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_data_V1_reg_181[9]_i_1_n_2 ),
        .Q(axi_data_V1_reg_181[9]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[0]_i_1 
       (.I0(axi_data_V_1_reg_236[0]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[0]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[0]),
        .O(\axi_data_V_1_reg_236[0]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[10]_i_1 
       (.I0(axi_data_V_1_reg_236[10]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[10]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[10]),
        .O(\axi_data_V_1_reg_236[10]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[11]_i_1 
       (.I0(axi_data_V_1_reg_236[11]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[11]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[11]),
        .O(\axi_data_V_1_reg_236[11]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[12]_i_1 
       (.I0(axi_data_V_1_reg_236[12]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[12]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[12]),
        .O(\axi_data_V_1_reg_236[12]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[13]_i_1 
       (.I0(axi_data_V_1_reg_236[13]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[13]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[13]),
        .O(\axi_data_V_1_reg_236[13]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[14]_i_1 
       (.I0(axi_data_V_1_reg_236[14]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[14]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[14]),
        .O(\axi_data_V_1_reg_236[14]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[15]_i_1 
       (.I0(axi_data_V_1_reg_236[15]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[15]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[15]),
        .O(\axi_data_V_1_reg_236[15]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[16]_i_1 
       (.I0(axi_data_V_1_reg_236[16]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[16]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[16]),
        .O(\axi_data_V_1_reg_236[16]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[17]_i_1 
       (.I0(axi_data_V_1_reg_236[17]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[17]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[17]),
        .O(\axi_data_V_1_reg_236[17]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[18]_i_1 
       (.I0(axi_data_V_1_reg_236[18]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[18]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[18]),
        .O(\axi_data_V_1_reg_236[18]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[19]_i_1 
       (.I0(axi_data_V_1_reg_236[19]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[19]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[19]),
        .O(\axi_data_V_1_reg_236[19]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[1]_i_1 
       (.I0(axi_data_V_1_reg_236[1]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[1]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[1]),
        .O(\axi_data_V_1_reg_236[1]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[20]_i_1 
       (.I0(axi_data_V_1_reg_236[20]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[20]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[20]),
        .O(\axi_data_V_1_reg_236[20]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[21]_i_1 
       (.I0(axi_data_V_1_reg_236[21]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[21]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[21]),
        .O(\axi_data_V_1_reg_236[21]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[22]_i_1 
       (.I0(axi_data_V_1_reg_236[22]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[22]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[22]),
        .O(\axi_data_V_1_reg_236[22]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[23]_i_1 
       (.I0(axi_data_V_1_reg_236[23]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[23]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[23]),
        .O(\axi_data_V_1_reg_236[23]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[2]_i_1 
       (.I0(axi_data_V_1_reg_236[2]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[2]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[2]),
        .O(\axi_data_V_1_reg_236[2]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[3]_i_1 
       (.I0(axi_data_V_1_reg_236[3]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[3]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[3]),
        .O(\axi_data_V_1_reg_236[3]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[4]_i_1 
       (.I0(axi_data_V_1_reg_236[4]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[4]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[4]),
        .O(\axi_data_V_1_reg_236[4]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[5]_i_1 
       (.I0(axi_data_V_1_reg_236[5]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[5]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[5]),
        .O(\axi_data_V_1_reg_236[5]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[6]_i_1 
       (.I0(axi_data_V_1_reg_236[6]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[6]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[6]),
        .O(\axi_data_V_1_reg_236[6]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[7]_i_1 
       (.I0(axi_data_V_1_reg_236[7]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[7]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[7]),
        .O(\axi_data_V_1_reg_236[7]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[8]_i_1 
       (.I0(axi_data_V_1_reg_236[8]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[8]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[8]),
        .O(\axi_data_V_1_reg_236[8]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_data_V_1_reg_236[9]_i_1 
       (.I0(axi_data_V_1_reg_236[9]),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_data_V_0_data_out[9]),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_data_V1_reg_181[9]),
        .O(\axi_data_V_1_reg_236[9]_i_1_n_2 ));
  FDRE \axi_data_V_1_reg_236_reg[0] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[0]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[0]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[10] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[10]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[10]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[11] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[11]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[11]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[12] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[12]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[12]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[13] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[13]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[13]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[14] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[14]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[14]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[15] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[15]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[15]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[16] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[16]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[16]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[17] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[17]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[17]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[18] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[18]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[18]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[19] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[19]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[19]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[1] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[1]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[1]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[20] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[20]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[20]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[21] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[21]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[21]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[22] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[22]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[22]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[23] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[23]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[23]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[2] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[2]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[2]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[3] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[3]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[3]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[4] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[4]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[4]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[5] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[5]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[5]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[6] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[6]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[6]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[7] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[7]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[7]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[8] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[8]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[8]),
        .R(1'b0));
  FDRE \axi_data_V_1_reg_236_reg[9] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\axi_data_V_1_reg_236[9]_i_1_n_2 ),
        .Q(axi_data_V_1_reg_236[9]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[0]_i_1 
       (.I0(axi_data_V_1_reg_236[0]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[0]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[0]),
        .O(\axi_data_V_3_reg_295[0]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[10]_i_1 
       (.I0(axi_data_V_1_reg_236[10]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[10]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[10]),
        .O(\axi_data_V_3_reg_295[10]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[11]_i_1 
       (.I0(axi_data_V_1_reg_236[11]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[11]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[11]),
        .O(\axi_data_V_3_reg_295[11]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[12]_i_1 
       (.I0(axi_data_V_1_reg_236[12]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[12]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[12]),
        .O(\axi_data_V_3_reg_295[12]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[13]_i_1 
       (.I0(axi_data_V_1_reg_236[13]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[13]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[13]),
        .O(\axi_data_V_3_reg_295[13]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[14]_i_1 
       (.I0(axi_data_V_1_reg_236[14]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[14]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[14]),
        .O(\axi_data_V_3_reg_295[14]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[15]_i_1 
       (.I0(axi_data_V_1_reg_236[15]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[15]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[15]),
        .O(\axi_data_V_3_reg_295[15]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[16]_i_1 
       (.I0(axi_data_V_1_reg_236[16]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[16]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[16]),
        .O(\axi_data_V_3_reg_295[16]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[17]_i_1 
       (.I0(axi_data_V_1_reg_236[17]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[17]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[17]),
        .O(\axi_data_V_3_reg_295[17]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[18]_i_1 
       (.I0(axi_data_V_1_reg_236[18]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[18]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[18]),
        .O(\axi_data_V_3_reg_295[18]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[19]_i_1 
       (.I0(axi_data_V_1_reg_236[19]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[19]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[19]),
        .O(\axi_data_V_3_reg_295[19]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[1]_i_1 
       (.I0(axi_data_V_1_reg_236[1]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[1]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[1]),
        .O(\axi_data_V_3_reg_295[1]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[20]_i_1 
       (.I0(axi_data_V_1_reg_236[20]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[20]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[20]),
        .O(\axi_data_V_3_reg_295[20]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[21]_i_1 
       (.I0(axi_data_V_1_reg_236[21]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[21]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[21]),
        .O(\axi_data_V_3_reg_295[21]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[22]_i_1 
       (.I0(axi_data_V_1_reg_236[22]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[22]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[22]),
        .O(\axi_data_V_3_reg_295[22]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[23]_i_1 
       (.I0(axi_data_V_1_reg_236[23]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[23]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[23]),
        .O(\axi_data_V_3_reg_295[23]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[2]_i_1 
       (.I0(axi_data_V_1_reg_236[2]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[2]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[2]),
        .O(\axi_data_V_3_reg_295[2]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[3]_i_1 
       (.I0(axi_data_V_1_reg_236[3]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[3]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[3]),
        .O(\axi_data_V_3_reg_295[3]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[4]_i_1 
       (.I0(axi_data_V_1_reg_236[4]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[4]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[4]),
        .O(\axi_data_V_3_reg_295[4]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[5]_i_1 
       (.I0(axi_data_V_1_reg_236[5]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[5]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[5]),
        .O(\axi_data_V_3_reg_295[5]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[6]_i_1 
       (.I0(axi_data_V_1_reg_236[6]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[6]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[6]),
        .O(\axi_data_V_3_reg_295[6]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[7]_i_1 
       (.I0(axi_data_V_1_reg_236[7]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[7]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[7]),
        .O(\axi_data_V_3_reg_295[7]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[8]_i_1 
       (.I0(axi_data_V_1_reg_236[8]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[8]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[8]),
        .O(\axi_data_V_3_reg_295[8]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_data_V_3_reg_295[9]_i_1 
       (.I0(axi_data_V_1_reg_236[9]),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_data_V_0_payload_B[9]),
        .I3(AXI_video_strm_V_data_V_0_sel),
        .I4(AXI_video_strm_V_data_V_0_payload_A[9]),
        .O(\axi_data_V_3_reg_295[9]_i_1_n_2 ));
  FDRE \axi_data_V_3_reg_295_reg[0] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[0]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[0]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[10] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[10]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[10]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[11] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[11]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[11]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[12] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[12]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[12]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[13] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[13]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[13]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[14] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[14]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[14]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[15] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[15]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[15]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[16] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[16]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[16]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[17] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[17]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[17]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[18] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[18]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[18]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[19] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[19]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[19]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[1] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[1]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[1]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[20] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[20]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[20]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[21] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[21]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[21]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[22] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[22]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[22]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[23] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[23]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[23]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[2] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[2]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[2]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[3] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[3]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[3]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[4] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[4]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[4]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[5] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[5]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[5]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[6] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[6]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[6]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[7] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[7]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[7]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[8] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[8]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[8]),
        .R(1'b0));
  FDRE \axi_data_V_3_reg_295_reg[9] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_data_V_3_reg_295[9]_i_1_n_2 ),
        .Q(axi_data_V_3_reg_295[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_last_V1_reg_171[0]_i_1 
       (.I0(tmp_last_V_reg_399),
        .I1(ap_CS_fsm_state3),
        .I2(axi_last_V_3_reg_283),
        .O(\axi_last_V1_reg_171[0]_i_1_n_2 ));
  FDRE \axi_last_V1_reg_171_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\axi_last_V1_reg_171[0]_i_1_n_2 ),
        .Q(axi_last_V1_reg_171),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_last_V_3_reg_283[0]_i_1 
       (.I0(eol_1_reg_225),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_last_V_0_payload_B),
        .I3(AXI_video_strm_V_last_V_0_sel),
        .I4(AXI_video_strm_V_last_V_0_payload_A),
        .O(\axi_last_V_3_reg_283[0]_i_1_n_2 ));
  FDRE \axi_last_V_3_reg_283_reg[0] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\axi_last_V_3_reg_283[0]_i_1_n_2 ),
        .Q(axi_last_V_3_reg_283),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFACFFFFFFAC0000)) 
    \brmerge_reg_429[0]_i_1 
       (.I0(\brmerge_reg_429[0]_i_2_n_2 ),
        .I1(\eol_reg_213_reg_n_2_[0] ),
        .I2(\brmerge_reg_429[0]_i_3_n_2 ),
        .I3(sof_1_fu_128),
        .I4(brmerge_reg_4290),
        .I5(brmerge_reg_429),
        .O(\brmerge_reg_429[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \brmerge_reg_429[0]_i_2 
       (.I0(eol_1_reg_225),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_last_V_0_payload_B),
        .I3(AXI_video_strm_V_last_V_0_sel),
        .I4(AXI_video_strm_V_last_V_0_payload_A),
        .O(\brmerge_reg_429[0]_i_2_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \brmerge_reg_429[0]_i_3 
       (.I0(ap_CS_fsm_pp1_stage0),
        .I1(ap_enable_reg_pp1_iter1_reg_n_2),
        .I2(\exitcond_reg_420_reg_n_2_[0] ),
        .O(\brmerge_reg_429[0]_i_3_n_2 ));
  FDRE \brmerge_reg_429_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\brmerge_reg_429[0]_i_1_n_2 ),
        .Q(brmerge_reg_429),
        .R(1'b0));
  LUT2 #(
    .INIT(4'hE)) 
    \eol_1_reg_225[0]_i_1 
       (.I0(p_1_in),
        .I1(\SRL_SIG_reg[1][0] ),
        .O(eol_reg_213));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \eol_1_reg_225[0]_i_2 
       (.I0(eol_1_reg_225),
        .I1(brmerge_reg_429),
        .I2(AXI_video_strm_V_last_V_0_data_out),
        .I3(\SRL_SIG_reg[1][0] ),
        .I4(axi_last_V1_reg_171),
        .O(\eol_1_reg_225[0]_i_2_n_2 ));
  FDRE \eol_1_reg_225_reg[0] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\eol_1_reg_225[0]_i_2_n_2 ),
        .Q(eol_1_reg_225),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEAAAAAA)) 
    \eol_2_reg_272[0]_i_1 
       (.I0(ap_CS_fsm_state7),
        .I1(ap_enable_reg_pp2_iter1_reg_n_2),
        .I2(\eol_2_reg_272_reg_n_2_[0] ),
        .I3(ap_CS_fsm_pp2_stage0),
        .I4(\AXI_video_strm_V_data_V_0_state_reg_n_2_[0] ),
        .O(\eol_2_reg_272[0]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \eol_2_reg_272[0]_i_2 
       (.I0(\eol_reg_213_reg_n_2_[0] ),
        .I1(ap_CS_fsm_state7),
        .I2(AXI_video_strm_V_last_V_0_payload_B),
        .I3(AXI_video_strm_V_last_V_0_sel),
        .I4(AXI_video_strm_V_last_V_0_payload_A),
        .O(\eol_2_reg_272[0]_i_2_n_2 ));
  FDRE \eol_2_reg_272_reg[0] 
       (.C(ap_clk),
        .CE(\eol_2_reg_272[0]_i_1_n_2 ),
        .D(\eol_2_reg_272[0]_i_2_n_2 ),
        .Q(\eol_2_reg_272_reg_n_2_[0] ),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hE000000000000000)) 
    \eol_reg_213[0]_i_1 
       (.I0(\AXI_video_strm_V_data_V_0_state_reg_n_2_[0] ),
        .I1(brmerge_reg_429),
        .I2(img0_data_stream_2_s_full_n),
        .I3(img0_data_stream_1_s_full_n),
        .I4(img0_data_stream_0_s_full_n),
        .I5(\eol_reg_213[0]_i_2_n_2 ),
        .O(\eol_reg_213[0]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'h00E2000000000000)) 
    \eol_reg_213[0]_i_2 
       (.I0(AXI_video_strm_V_last_V_0_data_out),
        .I1(brmerge_reg_429),
        .I2(eol_1_reg_225),
        .I3(\exitcond_reg_420_reg_n_2_[0] ),
        .I4(ap_enable_reg_pp1_iter1_reg_n_2),
        .I5(ap_CS_fsm_pp1_stage0),
        .O(\eol_reg_213[0]_i_2_n_2 ));
  FDRE \eol_reg_213_reg[0] 
       (.C(ap_clk),
        .CE(eol_reg_213),
        .D(\eol_reg_213[0]_i_1_n_2 ),
        .Q(\eol_reg_213_reg_n_2_[0] ),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hB8)) 
    \exitcond_reg_420[0]_i_1 
       (.I0(exitcond_fu_338_p2),
        .I1(exitcond_reg_4200),
        .I2(\exitcond_reg_420_reg_n_2_[0] ),
        .O(\exitcond_reg_420[0]_i_1_n_2 ));
  FDRE \exitcond_reg_420_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\exitcond_reg_420[0]_i_1_n_2 ),
        .Q(\exitcond_reg_420_reg_n_2_[0] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \i_V_reg_415[0]_i_1 
       (.I0(t_V_reg_191[0]),
        .O(i_V_fu_332_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_V_reg_415[1]_i_1 
       (.I0(t_V_reg_191[0]),
        .I1(t_V_reg_191[1]),
        .O(i_V_fu_332_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \i_V_reg_415[2]_i_1 
       (.I0(t_V_reg_191[2]),
        .I1(t_V_reg_191[1]),
        .I2(t_V_reg_191[0]),
        .O(i_V_fu_332_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \i_V_reg_415[3]_i_1 
       (.I0(t_V_reg_191[3]),
        .I1(t_V_reg_191[0]),
        .I2(t_V_reg_191[1]),
        .I3(t_V_reg_191[2]),
        .O(i_V_fu_332_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \i_V_reg_415[4]_i_1 
       (.I0(t_V_reg_191[4]),
        .I1(t_V_reg_191[2]),
        .I2(t_V_reg_191[1]),
        .I3(t_V_reg_191[0]),
        .I4(t_V_reg_191[3]),
        .O(i_V_fu_332_p2[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \i_V_reg_415[5]_i_1 
       (.I0(t_V_reg_191[5]),
        .I1(t_V_reg_191[3]),
        .I2(t_V_reg_191[0]),
        .I3(t_V_reg_191[1]),
        .I4(t_V_reg_191[2]),
        .I5(t_V_reg_191[4]),
        .O(i_V_fu_332_p2[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \i_V_reg_415[6]_i_1 
       (.I0(t_V_reg_191[6]),
        .I1(\i_V_reg_415[9]_i_2_n_2 ),
        .O(i_V_fu_332_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \i_V_reg_415[7]_i_1 
       (.I0(t_V_reg_191[7]),
        .I1(\i_V_reg_415[9]_i_2_n_2 ),
        .I2(t_V_reg_191[6]),
        .O(i_V_fu_332_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \i_V_reg_415[8]_i_1 
       (.I0(t_V_reg_191[8]),
        .I1(t_V_reg_191[6]),
        .I2(\i_V_reg_415[9]_i_2_n_2 ),
        .I3(t_V_reg_191[7]),
        .O(i_V_fu_332_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \i_V_reg_415[9]_i_1 
       (.I0(t_V_reg_191[9]),
        .I1(t_V_reg_191[7]),
        .I2(\i_V_reg_415[9]_i_2_n_2 ),
        .I3(t_V_reg_191[6]),
        .I4(t_V_reg_191[8]),
        .O(i_V_fu_332_p2[9]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \i_V_reg_415[9]_i_2 
       (.I0(t_V_reg_191[5]),
        .I1(t_V_reg_191[3]),
        .I2(t_V_reg_191[0]),
        .I3(t_V_reg_191[1]),
        .I4(t_V_reg_191[2]),
        .I5(t_V_reg_191[4]),
        .O(\i_V_reg_415[9]_i_2_n_2 ));
  FDRE \i_V_reg_415_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_V_fu_332_p2[0]),
        .Q(i_V_reg_415[0]),
        .R(1'b0));
  FDRE \i_V_reg_415_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_V_fu_332_p2[1]),
        .Q(i_V_reg_415[1]),
        .R(1'b0));
  FDRE \i_V_reg_415_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_V_fu_332_p2[2]),
        .Q(i_V_reg_415[2]),
        .R(1'b0));
  FDRE \i_V_reg_415_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_V_fu_332_p2[3]),
        .Q(i_V_reg_415[3]),
        .R(1'b0));
  FDRE \i_V_reg_415_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_V_fu_332_p2[4]),
        .Q(i_V_reg_415[4]),
        .R(1'b0));
  FDRE \i_V_reg_415_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_V_fu_332_p2[5]),
        .Q(i_V_reg_415[5]),
        .R(1'b0));
  FDRE \i_V_reg_415_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_V_fu_332_p2[6]),
        .Q(i_V_reg_415[6]),
        .R(1'b0));
  FDRE \i_V_reg_415_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_V_fu_332_p2[7]),
        .Q(i_V_reg_415[7]),
        .R(1'b0));
  FDRE \i_V_reg_415_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_V_fu_332_p2[8]),
        .Q(i_V_reg_415[8]),
        .R(1'b0));
  FDRE \i_V_reg_415_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state4),
        .D(i_V_fu_332_p2[9]),
        .Q(i_V_reg_415[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h0EEE)) 
    \sof_1_fu_128[0]_i_1 
       (.I0(sof_1_fu_128),
        .I1(ap_CS_fsm_state3),
        .I2(brmerge_reg_4290),
        .I3(ap_enable_reg_pp1_iter0),
        .O(\sof_1_fu_128[0]_i_1_n_2 ));
  FDRE \sof_1_fu_128_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\sof_1_fu_128[0]_i_1_n_2 ),
        .Q(sof_1_fu_128),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hDDD0)) 
    start_once_reg_i_1__2
       (.I0(ap_CS_fsm_state4),
        .I1(\ap_CS_fsm[0]_i_2_n_2 ),
        .I2(start_for_CvtColor_U0_full_n),
        .I3(start_once_reg),
        .O(start_once_reg_i_1__2_n_2));
  FDRE #(
    .INIT(1'b0)) 
    start_once_reg_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(start_once_reg_i_1__2_n_2),
        .Q(start_once_reg),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \t_V_3_reg_202[0]_i_1 
       (.I0(t_V_3_reg_202_reg__0[0]),
        .O(j_V_fu_344_p2[0]));
  LUT3 #(
    .INIT(8'h2A)) 
    \t_V_3_reg_202[10]_i_1 
       (.I0(p_1_in),
        .I1(brmerge_reg_4290),
        .I2(ap_enable_reg_pp1_iter0),
        .O(t_V_3_reg_202));
  LUT2 #(
    .INIT(4'h8)) 
    \t_V_3_reg_202[10]_i_2 
       (.I0(ap_enable_reg_pp1_iter0),
        .I1(brmerge_reg_4290),
        .O(sof_1_fu_1280));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \t_V_3_reg_202[10]_i_3 
       (.I0(t_V_3_reg_202_reg__0[10]),
        .I1(t_V_3_reg_202_reg__0[8]),
        .I2(t_V_3_reg_202_reg__0[6]),
        .I3(\t_V_3_reg_202[10]_i_5_n_2 ),
        .I4(t_V_3_reg_202_reg__0[7]),
        .I5(t_V_3_reg_202_reg__0[9]),
        .O(j_V_fu_344_p2[10]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \t_V_3_reg_202[10]_i_4 
       (.I0(exitcond_reg_4200),
        .I1(exitcond_fu_338_p2),
        .O(brmerge_reg_4290));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \t_V_3_reg_202[10]_i_5 
       (.I0(t_V_3_reg_202_reg__0[5]),
        .I1(t_V_3_reg_202_reg__0[3]),
        .I2(t_V_3_reg_202_reg__0[2]),
        .I3(t_V_3_reg_202_reg__0[0]),
        .I4(t_V_3_reg_202_reg__0[1]),
        .I5(t_V_3_reg_202_reg__0[4]),
        .O(\t_V_3_reg_202[10]_i_5_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \t_V_3_reg_202[1]_i_1 
       (.I0(t_V_3_reg_202_reg__0[0]),
        .I1(t_V_3_reg_202_reg__0[1]),
        .O(j_V_fu_344_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \t_V_3_reg_202[2]_i_1 
       (.I0(t_V_3_reg_202_reg__0[2]),
        .I1(t_V_3_reg_202_reg__0[1]),
        .I2(t_V_3_reg_202_reg__0[0]),
        .O(j_V_fu_344_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \t_V_3_reg_202[3]_i_1 
       (.I0(t_V_3_reg_202_reg__0[3]),
        .I1(t_V_3_reg_202_reg__0[2]),
        .I2(t_V_3_reg_202_reg__0[0]),
        .I3(t_V_3_reg_202_reg__0[1]),
        .O(j_V_fu_344_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \t_V_3_reg_202[4]_i_1 
       (.I0(t_V_3_reg_202_reg__0[4]),
        .I1(t_V_3_reg_202_reg__0[1]),
        .I2(t_V_3_reg_202_reg__0[0]),
        .I3(t_V_3_reg_202_reg__0[2]),
        .I4(t_V_3_reg_202_reg__0[3]),
        .O(j_V_fu_344_p2[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \t_V_3_reg_202[5]_i_1 
       (.I0(t_V_3_reg_202_reg__0[5]),
        .I1(t_V_3_reg_202_reg__0[3]),
        .I2(t_V_3_reg_202_reg__0[2]),
        .I3(t_V_3_reg_202_reg__0[0]),
        .I4(t_V_3_reg_202_reg__0[1]),
        .I5(t_V_3_reg_202_reg__0[4]),
        .O(j_V_fu_344_p2[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \t_V_3_reg_202[6]_i_1 
       (.I0(t_V_3_reg_202_reg__0[6]),
        .I1(\t_V_3_reg_202[10]_i_5_n_2 ),
        .O(j_V_fu_344_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \t_V_3_reg_202[7]_i_1 
       (.I0(t_V_3_reg_202_reg__0[7]),
        .I1(\t_V_3_reg_202[10]_i_5_n_2 ),
        .I2(t_V_3_reg_202_reg__0[6]),
        .O(j_V_fu_344_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \t_V_3_reg_202[8]_i_1 
       (.I0(t_V_3_reg_202_reg__0[8]),
        .I1(t_V_3_reg_202_reg__0[6]),
        .I2(\t_V_3_reg_202[10]_i_5_n_2 ),
        .I3(t_V_3_reg_202_reg__0[7]),
        .O(j_V_fu_344_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \t_V_3_reg_202[9]_i_1 
       (.I0(t_V_3_reg_202_reg__0[9]),
        .I1(t_V_3_reg_202_reg__0[7]),
        .I2(\t_V_3_reg_202[10]_i_5_n_2 ),
        .I3(t_V_3_reg_202_reg__0[6]),
        .I4(t_V_3_reg_202_reg__0[8]),
        .O(j_V_fu_344_p2[9]));
  FDRE \t_V_3_reg_202_reg[0] 
       (.C(ap_clk),
        .CE(sof_1_fu_1280),
        .D(j_V_fu_344_p2[0]),
        .Q(t_V_3_reg_202_reg__0[0]),
        .R(t_V_3_reg_202));
  FDRE \t_V_3_reg_202_reg[10] 
       (.C(ap_clk),
        .CE(sof_1_fu_1280),
        .D(j_V_fu_344_p2[10]),
        .Q(t_V_3_reg_202_reg__0[10]),
        .R(t_V_3_reg_202));
  FDRE \t_V_3_reg_202_reg[1] 
       (.C(ap_clk),
        .CE(sof_1_fu_1280),
        .D(j_V_fu_344_p2[1]),
        .Q(t_V_3_reg_202_reg__0[1]),
        .R(t_V_3_reg_202));
  FDRE \t_V_3_reg_202_reg[2] 
       (.C(ap_clk),
        .CE(sof_1_fu_1280),
        .D(j_V_fu_344_p2[2]),
        .Q(t_V_3_reg_202_reg__0[2]),
        .R(t_V_3_reg_202));
  FDRE \t_V_3_reg_202_reg[3] 
       (.C(ap_clk),
        .CE(sof_1_fu_1280),
        .D(j_V_fu_344_p2[3]),
        .Q(t_V_3_reg_202_reg__0[3]),
        .R(t_V_3_reg_202));
  FDRE \t_V_3_reg_202_reg[4] 
       (.C(ap_clk),
        .CE(sof_1_fu_1280),
        .D(j_V_fu_344_p2[4]),
        .Q(t_V_3_reg_202_reg__0[4]),
        .R(t_V_3_reg_202));
  FDRE \t_V_3_reg_202_reg[5] 
       (.C(ap_clk),
        .CE(sof_1_fu_1280),
        .D(j_V_fu_344_p2[5]),
        .Q(t_V_3_reg_202_reg__0[5]),
        .R(t_V_3_reg_202));
  FDRE \t_V_3_reg_202_reg[6] 
       (.C(ap_clk),
        .CE(sof_1_fu_1280),
        .D(j_V_fu_344_p2[6]),
        .Q(t_V_3_reg_202_reg__0[6]),
        .R(t_V_3_reg_202));
  FDRE \t_V_3_reg_202_reg[7] 
       (.C(ap_clk),
        .CE(sof_1_fu_1280),
        .D(j_V_fu_344_p2[7]),
        .Q(t_V_3_reg_202_reg__0[7]),
        .R(t_V_3_reg_202));
  FDRE \t_V_3_reg_202_reg[8] 
       (.C(ap_clk),
        .CE(sof_1_fu_1280),
        .D(j_V_fu_344_p2[8]),
        .Q(t_V_3_reg_202_reg__0[8]),
        .R(t_V_3_reg_202));
  FDRE \t_V_3_reg_202_reg[9] 
       (.C(ap_clk),
        .CE(sof_1_fu_1280),
        .D(j_V_fu_344_p2[9]),
        .Q(t_V_3_reg_202_reg__0[9]),
        .R(t_V_3_reg_202));
  FDRE \t_V_reg_191_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state10),
        .D(i_V_reg_415[0]),
        .Q(t_V_reg_191[0]),
        .R(ap_CS_fsm_state3));
  FDRE \t_V_reg_191_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state10),
        .D(i_V_reg_415[1]),
        .Q(t_V_reg_191[1]),
        .R(ap_CS_fsm_state3));
  FDRE \t_V_reg_191_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state10),
        .D(i_V_reg_415[2]),
        .Q(t_V_reg_191[2]),
        .R(ap_CS_fsm_state3));
  FDRE \t_V_reg_191_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state10),
        .D(i_V_reg_415[3]),
        .Q(t_V_reg_191[3]),
        .R(ap_CS_fsm_state3));
  FDRE \t_V_reg_191_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state10),
        .D(i_V_reg_415[4]),
        .Q(t_V_reg_191[4]),
        .R(ap_CS_fsm_state3));
  FDRE \t_V_reg_191_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state10),
        .D(i_V_reg_415[5]),
        .Q(t_V_reg_191[5]),
        .R(ap_CS_fsm_state3));
  FDRE \t_V_reg_191_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state10),
        .D(i_V_reg_415[6]),
        .Q(t_V_reg_191[6]),
        .R(ap_CS_fsm_state3));
  FDRE \t_V_reg_191_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state10),
        .D(i_V_reg_415[7]),
        .Q(t_V_reg_191[7]),
        .R(ap_CS_fsm_state3));
  FDRE \t_V_reg_191_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state10),
        .D(i_V_reg_415[8]),
        .Q(t_V_reg_191[8]),
        .R(ap_CS_fsm_state3));
  FDRE \t_V_reg_191_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state10),
        .D(i_V_reg_415[9]),
        .Q(t_V_reg_191[9]),
        .R(ap_CS_fsm_state3));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[0]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[0]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[0]),
        .O(AXI_video_strm_V_data_V_0_data_out[0]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[10]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[10]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[10]),
        .O(AXI_video_strm_V_data_V_0_data_out[10]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[11]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[11]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[11]),
        .O(AXI_video_strm_V_data_V_0_data_out[11]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[12]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[12]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[12]),
        .O(AXI_video_strm_V_data_V_0_data_out[12]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[13]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[13]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[13]),
        .O(AXI_video_strm_V_data_V_0_data_out[13]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[14]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[14]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[14]),
        .O(AXI_video_strm_V_data_V_0_data_out[14]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[15]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[15]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[15]),
        .O(AXI_video_strm_V_data_V_0_data_out[15]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[16]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[16]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[16]),
        .O(AXI_video_strm_V_data_V_0_data_out[16]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[17]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[17]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[17]),
        .O(AXI_video_strm_V_data_V_0_data_out[17]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[18]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[18]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[18]),
        .O(AXI_video_strm_V_data_V_0_data_out[18]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[19]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[19]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[19]),
        .O(AXI_video_strm_V_data_V_0_data_out[19]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[1]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[1]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[1]),
        .O(AXI_video_strm_V_data_V_0_data_out[1]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[20]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[20]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[20]),
        .O(AXI_video_strm_V_data_V_0_data_out[20]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[21]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[21]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[21]),
        .O(AXI_video_strm_V_data_V_0_data_out[21]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[22]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[22]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[22]),
        .O(AXI_video_strm_V_data_V_0_data_out[22]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[23]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[23]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[23]),
        .O(AXI_video_strm_V_data_V_0_data_out[23]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[2]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[2]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[2]),
        .O(AXI_video_strm_V_data_V_0_data_out[2]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[3]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[3]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[3]),
        .O(AXI_video_strm_V_data_V_0_data_out[3]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[4]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[4]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[4]),
        .O(AXI_video_strm_V_data_V_0_data_out[4]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[5]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[5]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[5]),
        .O(AXI_video_strm_V_data_V_0_data_out[5]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[6]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[6]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[6]),
        .O(AXI_video_strm_V_data_V_0_data_out[6]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[7]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[7]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[7]),
        .O(AXI_video_strm_V_data_V_0_data_out[7]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[8]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[8]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[8]),
        .O(AXI_video_strm_V_data_V_0_data_out[8]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_data_V_reg_391[9]_i_1 
       (.I0(AXI_video_strm_V_data_V_0_payload_B[9]),
        .I1(AXI_video_strm_V_data_V_0_sel),
        .I2(AXI_video_strm_V_data_V_0_payload_A[9]),
        .O(AXI_video_strm_V_data_V_0_data_out[9]));
  FDRE \tmp_data_V_reg_391_reg[0] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[0]),
        .Q(tmp_data_V_reg_391[0]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[10] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[10]),
        .Q(tmp_data_V_reg_391[10]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[11] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[11]),
        .Q(tmp_data_V_reg_391[11]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[12] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[12]),
        .Q(tmp_data_V_reg_391[12]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[13] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[13]),
        .Q(tmp_data_V_reg_391[13]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[14] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[14]),
        .Q(tmp_data_V_reg_391[14]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[15] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[15]),
        .Q(tmp_data_V_reg_391[15]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[16] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[16]),
        .Q(tmp_data_V_reg_391[16]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[17] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[17]),
        .Q(tmp_data_V_reg_391[17]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[18] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[18]),
        .Q(tmp_data_V_reg_391[18]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[19] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[19]),
        .Q(tmp_data_V_reg_391[19]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[1] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[1]),
        .Q(tmp_data_V_reg_391[1]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[20] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[20]),
        .Q(tmp_data_V_reg_391[20]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[21] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[21]),
        .Q(tmp_data_V_reg_391[21]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[22] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[22]),
        .Q(tmp_data_V_reg_391[22]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[23] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[23]),
        .Q(tmp_data_V_reg_391[23]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[2] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[2]),
        .Q(tmp_data_V_reg_391[2]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[3] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[3]),
        .Q(tmp_data_V_reg_391[3]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[4] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[4]),
        .Q(tmp_data_V_reg_391[4]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[5] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[5]),
        .Q(tmp_data_V_reg_391[5]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[6] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[6]),
        .Q(tmp_data_V_reg_391[6]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[7] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[7]),
        .Q(tmp_data_V_reg_391[7]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[8] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[8]),
        .Q(tmp_data_V_reg_391[8]),
        .R(1'b0));
  FDRE \tmp_data_V_reg_391_reg[9] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_data_V_0_data_out[9]),
        .Q(tmp_data_V_reg_391[9]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    \tmp_last_V_reg_399[0]_i_1 
       (.I0(\AXI_video_strm_V_data_V_0_state_reg_n_2_[0] ),
        .I1(ap_CS_fsm_state2),
        .O(AXI_video_strm_V_data_V_0_sel2));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \tmp_last_V_reg_399[0]_i_2 
       (.I0(AXI_video_strm_V_last_V_0_payload_B),
        .I1(AXI_video_strm_V_last_V_0_sel),
        .I2(AXI_video_strm_V_last_V_0_payload_A),
        .O(AXI_video_strm_V_last_V_0_data_out));
  FDRE \tmp_last_V_reg_399_reg[0] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_0_sel2),
        .D(AXI_video_strm_V_last_V_0_data_out),
        .Q(tmp_last_V_reg_399),
        .R(1'b0));
endmodule

module system_edge_detect_0_1_CvtColor
   (start_once_reg,
    E,
    internal_full_n_reg,
    internal_empty_n4_out,
    start_once_reg_reg_0,
    shiftReg_ce,
    \SRL_SIG_reg[0][7] ,
    Q,
    ap_clk,
    B,
    ap_rst_n_inv,
    ap_rst_n,
    \exitcond_reg_420_reg[0] ,
    CvtColor_U0_ap_start,
    start_for_Sobel_U0_full_n,
    img0_data_stream_2_s_empty_n,
    img0_data_stream_0_s_empty_n,
    img0_data_stream_1_s_empty_n,
    img1_data_stream_0_s_full_n,
    img1_data_stream_1_s_full_n,
    img1_data_stream_2_s_full_n,
    D,
    \SRL_SIG_reg[0][7]_0 );
  output start_once_reg;
  output [0:0]E;
  output internal_full_n_reg;
  output internal_empty_n4_out;
  output start_once_reg_reg_0;
  output shiftReg_ce;
  output \SRL_SIG_reg[0][7] ;
  output [7:0]Q;
  input ap_clk;
  input [7:0]B;
  input ap_rst_n_inv;
  input ap_rst_n;
  input \exitcond_reg_420_reg[0] ;
  input CvtColor_U0_ap_start;
  input start_for_Sobel_U0_full_n;
  input img0_data_stream_2_s_empty_n;
  input img0_data_stream_0_s_empty_n;
  input img0_data_stream_1_s_empty_n;
  input img1_data_stream_0_s_full_n;
  input img1_data_stream_1_s_full_n;
  input img1_data_stream_2_s_full_n;
  input [7:0]D;
  input [7:0]\SRL_SIG_reg[0][7]_0 ;

  wire [7:0]B;
  wire CvtColor_U0_ap_start;
  wire [7:0]D;
  wire [0:0]E;
  wire [7:0]Q;
  wire \SRL_SIG_reg[0][7] ;
  wire [7:0]\SRL_SIG_reg[0][7]_0 ;
  wire \ap_CS_fsm[2]_i_3_n_2 ;
  wire \ap_CS_fsm[3]_i_2_n_2 ;
  wire ap_CS_fsm_pp0_stage0;
  wire \ap_CS_fsm_reg_n_2_[0] ;
  wire ap_CS_fsm_state2;
  wire ap_CS_fsm_state9;
  wire [3:0]ap_NS_fsm;
  wire ap_block_pp0_stage0_subdone3_in;
  wire ap_clk;
  wire ap_enable_reg_pp0_iter0;
  wire ap_enable_reg_pp0_iter00;
  wire ap_enable_reg_pp0_iter0_i_1_n_2;
  wire ap_enable_reg_pp0_iter1_i_1_n_2;
  wire ap_enable_reg_pp0_iter1_reg_n_2;
  wire ap_enable_reg_pp0_iter2;
  wire ap_enable_reg_pp0_iter2_i_1__0_n_2;
  wire ap_enable_reg_pp0_iter3;
  wire ap_enable_reg_pp0_iter3_i_1__0_n_2;
  wire ap_enable_reg_pp0_iter4;
  wire ap_enable_reg_pp0_iter4_i_1_n_2;
  wire ap_enable_reg_pp0_iter5_i_1_n_2;
  wire ap_enable_reg_pp0_iter5_reg_n_2;
  wire ap_reg_pp0_iter1_tmp_20_reg_355;
  wire \ap_reg_pp0_iter1_tmp_20_reg_355[0]_i_1_n_2 ;
  wire ap_reg_pp0_iter2_tmp_20_reg_355;
  wire \ap_reg_pp0_iter2_tmp_20_reg_355[0]_i_1_n_2 ;
  wire ap_reg_pp0_iter3_tmp_20_reg_355;
  wire \ap_reg_pp0_iter3_tmp_20_reg_355[0]_i_1_n_2 ;
  wire ap_reg_pp0_iter4_tmp_20_reg_355;
  wire \ap_reg_pp0_iter4_tmp_20_reg_355[0]_i_1_n_2 ;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire edge_detect_mac_mdEe_U13_n_12;
  wire \exitcond_reg_420_reg[0] ;
  wire [9:0]i_1_fu_231_p2;
  wire [9:0]i_1_reg_350;
  wire \i_1_reg_350[9]_i_2_n_2 ;
  wire i_reg_203;
  wire \i_reg_203_reg_n_2_[0] ;
  wire \i_reg_203_reg_n_2_[1] ;
  wire \i_reg_203_reg_n_2_[2] ;
  wire \i_reg_203_reg_n_2_[3] ;
  wire \i_reg_203_reg_n_2_[4] ;
  wire \i_reg_203_reg_n_2_[5] ;
  wire \i_reg_203_reg_n_2_[6] ;
  wire \i_reg_203_reg_n_2_[7] ;
  wire \i_reg_203_reg_n_2_[8] ;
  wire \i_reg_203_reg_n_2_[9] ;
  wire img0_data_stream_0_s_empty_n;
  wire img0_data_stream_1_s_empty_n;
  wire img0_data_stream_2_s_empty_n;
  wire img1_data_stream_0_s_full_n;
  wire img1_data_stream_1_s_full_n;
  wire img1_data_stream_2_s_full_n;
  wire internal_empty_n4_out;
  wire internal_full_n_reg;
  wire [10:0]j_1_fu_243_p2;
  wire j_reg_214;
  wire j_reg_2140;
  wire \j_reg_214[10]_i_4_n_2 ;
  wire [10:8]j_reg_214_reg__0;
  wire \j_reg_214_reg_n_2_[0] ;
  wire \j_reg_214_reg_n_2_[1] ;
  wire \j_reg_214_reg_n_2_[2] ;
  wire \j_reg_214_reg_n_2_[3] ;
  wire \j_reg_214_reg_n_2_[4] ;
  wire \j_reg_214_reg_n_2_[5] ;
  wire \j_reg_214_reg_n_2_[6] ;
  wire \j_reg_214_reg_n_2_[7] ;
  wire p_Val2_12_reg_3840;
  wire p_Val2_12_reg_384_reg_n_100;
  wire p_Val2_12_reg_384_reg_n_101;
  wire p_Val2_12_reg_384_reg_n_102;
  wire p_Val2_12_reg_384_reg_n_103;
  wire p_Val2_12_reg_384_reg_n_104;
  wire p_Val2_12_reg_384_reg_n_105;
  wire p_Val2_12_reg_384_reg_n_106;
  wire p_Val2_12_reg_384_reg_n_107;
  wire p_Val2_12_reg_384_reg_n_79;
  wire p_Val2_12_reg_384_reg_n_80;
  wire p_Val2_12_reg_384_reg_n_81;
  wire p_Val2_12_reg_384_reg_n_82;
  wire p_Val2_12_reg_384_reg_n_83;
  wire p_Val2_12_reg_384_reg_n_84;
  wire p_Val2_12_reg_384_reg_n_85;
  wire p_Val2_12_reg_384_reg_n_86;
  wire p_Val2_12_reg_384_reg_n_87;
  wire p_Val2_12_reg_384_reg_n_88;
  wire p_Val2_12_reg_384_reg_n_89;
  wire p_Val2_12_reg_384_reg_n_90;
  wire p_Val2_12_reg_384_reg_n_91;
  wire p_Val2_12_reg_384_reg_n_92;
  wire p_Val2_12_reg_384_reg_n_93;
  wire p_Val2_12_reg_384_reg_n_94;
  wire p_Val2_12_reg_384_reg_n_95;
  wire p_Val2_12_reg_384_reg_n_96;
  wire p_Val2_12_reg_384_reg_n_97;
  wire p_Val2_12_reg_384_reg_n_98;
  wire p_Val2_12_reg_384_reg_n_99;
  wire [7:0]p_Val2_14_fu_261_p4;
  wire [7:0]p_Val2_15_fu_281_p2__0;
  wire p_Val2_15_reg_3940;
  wire \p_Val2_15_reg_394[7]_i_3_n_2 ;
  wire [28:0]r_V_i_fu_323_p2;
  wire r_V_i_reg_3790;
  wire shiftReg_ce;
  wire start_for_Sobel_U0_full_n;
  wire start_once_reg;
  wire start_once_reg_i_1_n_2;
  wire start_once_reg_reg_0;
  wire [7:0]tmp_101_reg_364;
  wire tmp_101_reg_3640;
  wire [7:0]tmp_102_reg_369;
  wire tmp_20_fu_237_p2;
  wire tmp_20_reg_355;
  wire \tmp_20_reg_355[0]_i_1_n_2 ;
  wire tmp_3_i_i_i_fu_277_p1;
  wire tmp_98_fu_287_p3;
  wire NLW_p_Val2_12_reg_384_reg_CARRYCASCOUT_UNCONNECTED;
  wire NLW_p_Val2_12_reg_384_reg_MULTSIGNOUT_UNCONNECTED;
  wire NLW_p_Val2_12_reg_384_reg_OVERFLOW_UNCONNECTED;
  wire NLW_p_Val2_12_reg_384_reg_PATTERNBDETECT_UNCONNECTED;
  wire NLW_p_Val2_12_reg_384_reg_PATTERNDETECT_UNCONNECTED;
  wire NLW_p_Val2_12_reg_384_reg_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_p_Val2_12_reg_384_reg_ACOUT_UNCONNECTED;
  wire [17:0]NLW_p_Val2_12_reg_384_reg_BCOUT_UNCONNECTED;
  wire [3:0]NLW_p_Val2_12_reg_384_reg_CARRYOUT_UNCONNECTED;
  wire [47:29]NLW_p_Val2_12_reg_384_reg_P_UNCONNECTED;
  wire [47:0]NLW_p_Val2_12_reg_384_reg_PCOUT_UNCONNECTED;

  LUT3 #(
    .INIT(8'h08)) 
    \SRL_SIG[0][7]_i_1__3 
       (.I0(shiftReg_ce),
        .I1(tmp_98_fu_287_p3),
        .I2(Q[7]),
        .O(\SRL_SIG_reg[0][7] ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \SRL_SIG[0][7]_i_2 
       (.I0(ap_reg_pp0_iter4_tmp_20_reg_355),
        .I1(ap_block_pp0_stage0_subdone3_in),
        .I2(ap_enable_reg_pp0_iter5_reg_n_2),
        .O(shiftReg_ce));
  LUT5 #(
    .INIT(32'hBBBFAAAA)) 
    \ap_CS_fsm[0]_i_1 
       (.I0(start_once_reg_reg_0),
        .I1(CvtColor_U0_ap_start),
        .I2(start_for_Sobel_U0_full_n),
        .I3(start_once_reg),
        .I4(\ap_CS_fsm_reg_n_2_[0] ),
        .O(ap_NS_fsm[0]));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \ap_CS_fsm[0]_i_2__0 
       (.I0(ap_CS_fsm_state2),
        .I1(\ap_CS_fsm[2]_i_3_n_2 ),
        .O(start_once_reg_reg_0));
  LUT5 #(
    .INIT(32'hEEEAAAAA)) 
    \ap_CS_fsm[1]_i_1__0 
       (.I0(ap_CS_fsm_state9),
        .I1(CvtColor_U0_ap_start),
        .I2(start_for_Sobel_U0_full_n),
        .I3(start_once_reg),
        .I4(\ap_CS_fsm_reg_n_2_[0] ),
        .O(ap_NS_fsm[1]));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    \ap_CS_fsm[2]_i_1__0 
       (.I0(ap_enable_reg_pp0_iter00),
        .I1(\ap_CS_fsm[3]_i_2_n_2 ),
        .I2(ap_CS_fsm_pp0_stage0),
        .O(ap_NS_fsm[2]));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \ap_CS_fsm[2]_i_2 
       (.I0(ap_CS_fsm_state2),
        .I1(\ap_CS_fsm[2]_i_3_n_2 ),
        .O(ap_enable_reg_pp0_iter00));
  LUT6 #(
    .INIT(64'h5555555557FFFFFF)) 
    \ap_CS_fsm[2]_i_3 
       (.I0(\i_reg_203_reg_n_2_[9] ),
        .I1(\i_reg_203_reg_n_2_[5] ),
        .I2(\i_reg_203_reg_n_2_[4] ),
        .I3(\i_reg_203_reg_n_2_[7] ),
        .I4(\i_reg_203_reg_n_2_[6] ),
        .I5(\i_reg_203_reg_n_2_[8] ),
        .O(\ap_CS_fsm[2]_i_3_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \ap_CS_fsm[3]_i_1 
       (.I0(ap_CS_fsm_pp0_stage0),
        .I1(\ap_CS_fsm[3]_i_2_n_2 ),
        .O(ap_NS_fsm[3]));
  LUT6 #(
    .INIT(64'hDDDDFFFFD0DDFFFF)) 
    \ap_CS_fsm[3]_i_2 
       (.I0(ap_enable_reg_pp0_iter5_reg_n_2),
        .I1(ap_enable_reg_pp0_iter4),
        .I2(ap_enable_reg_pp0_iter1_reg_n_2),
        .I3(ap_enable_reg_pp0_iter0),
        .I4(ap_block_pp0_stage0_subdone3_in),
        .I5(tmp_20_fu_237_p2),
        .O(\ap_CS_fsm[3]_i_2_n_2 ));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[0]),
        .Q(\ap_CS_fsm_reg_n_2_[0] ),
        .S(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ap_CS_fsm_state2),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[2]),
        .Q(ap_CS_fsm_pp0_stage0),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[3]),
        .Q(ap_CS_fsm_state9),
        .R(ap_rst_n_inv));
  LUT6 #(
    .INIT(64'hF700F700F7000000)) 
    ap_enable_reg_pp0_iter0_i_1
       (.I0(ap_CS_fsm_pp0_stage0),
        .I1(ap_block_pp0_stage0_subdone3_in),
        .I2(tmp_20_fu_237_p2),
        .I3(ap_rst_n),
        .I4(ap_enable_reg_pp0_iter00),
        .I5(ap_enable_reg_pp0_iter0),
        .O(ap_enable_reg_pp0_iter0_i_1_n_2));
  LUT3 #(
    .INIT(8'h57)) 
    ap_enable_reg_pp0_iter0_i_2
       (.I0(j_reg_214_reg__0[10]),
        .I1(j_reg_214_reg__0[9]),
        .I2(j_reg_214_reg__0[8]),
        .O(tmp_20_fu_237_p2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter0_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter0_i_1_n_2),
        .Q(ap_enable_reg_pp0_iter0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h8800A0A0)) 
    ap_enable_reg_pp0_iter1_i_1
       (.I0(ap_rst_n),
        .I1(ap_enable_reg_pp0_iter0),
        .I2(ap_enable_reg_pp0_iter1_reg_n_2),
        .I3(tmp_20_fu_237_p2),
        .I4(ap_block_pp0_stage0_subdone3_in),
        .O(ap_enable_reg_pp0_iter1_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter1_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter1_i_1_n_2),
        .Q(ap_enable_reg_pp0_iter1_reg_n_2),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    ap_enable_reg_pp0_iter2_i_1__0
       (.I0(ap_enable_reg_pp0_iter1_reg_n_2),
        .I1(ap_block_pp0_stage0_subdone3_in),
        .I2(ap_enable_reg_pp0_iter2),
        .O(ap_enable_reg_pp0_iter2_i_1__0_n_2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter2_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter2_i_1__0_n_2),
        .Q(ap_enable_reg_pp0_iter2),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    ap_enable_reg_pp0_iter3_i_1__0
       (.I0(ap_enable_reg_pp0_iter2),
        .I1(ap_block_pp0_stage0_subdone3_in),
        .I2(ap_enable_reg_pp0_iter3),
        .O(ap_enable_reg_pp0_iter3_i_1__0_n_2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter3_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter3_i_1__0_n_2),
        .Q(ap_enable_reg_pp0_iter3),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    ap_enable_reg_pp0_iter4_i_1
       (.I0(ap_enable_reg_pp0_iter3),
        .I1(ap_block_pp0_stage0_subdone3_in),
        .I2(ap_enable_reg_pp0_iter4),
        .O(ap_enable_reg_pp0_iter4_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter4_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter4_i_1_n_2),
        .Q(ap_enable_reg_pp0_iter4),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hC044C000)) 
    ap_enable_reg_pp0_iter5_i_1
       (.I0(ap_enable_reg_pp0_iter00),
        .I1(ap_rst_n),
        .I2(ap_enable_reg_pp0_iter4),
        .I3(ap_block_pp0_stage0_subdone3_in),
        .I4(ap_enable_reg_pp0_iter5_reg_n_2),
        .O(ap_enable_reg_pp0_iter5_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter5_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter5_i_1_n_2),
        .Q(ap_enable_reg_pp0_iter5_reg_n_2),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT4 #(
    .INIT(16'hBF80)) 
    \ap_reg_pp0_iter1_tmp_20_reg_355[0]_i_1 
       (.I0(tmp_20_reg_355),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(ap_block_pp0_stage0_subdone3_in),
        .I3(ap_reg_pp0_iter1_tmp_20_reg_355),
        .O(\ap_reg_pp0_iter1_tmp_20_reg_355[0]_i_1_n_2 ));
  FDRE \ap_reg_pp0_iter1_tmp_20_reg_355_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\ap_reg_pp0_iter1_tmp_20_reg_355[0]_i_1_n_2 ),
        .Q(ap_reg_pp0_iter1_tmp_20_reg_355),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ap_reg_pp0_iter2_tmp_20_reg_355[0]_i_1 
       (.I0(ap_reg_pp0_iter1_tmp_20_reg_355),
        .I1(ap_block_pp0_stage0_subdone3_in),
        .I2(ap_reg_pp0_iter2_tmp_20_reg_355),
        .O(\ap_reg_pp0_iter2_tmp_20_reg_355[0]_i_1_n_2 ));
  FDRE \ap_reg_pp0_iter2_tmp_20_reg_355_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\ap_reg_pp0_iter2_tmp_20_reg_355[0]_i_1_n_2 ),
        .Q(ap_reg_pp0_iter2_tmp_20_reg_355),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ap_reg_pp0_iter3_tmp_20_reg_355[0]_i_1 
       (.I0(ap_reg_pp0_iter2_tmp_20_reg_355),
        .I1(ap_block_pp0_stage0_subdone3_in),
        .I2(ap_reg_pp0_iter3_tmp_20_reg_355),
        .O(\ap_reg_pp0_iter3_tmp_20_reg_355[0]_i_1_n_2 ));
  FDRE \ap_reg_pp0_iter3_tmp_20_reg_355_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\ap_reg_pp0_iter3_tmp_20_reg_355[0]_i_1_n_2 ),
        .Q(ap_reg_pp0_iter3_tmp_20_reg_355),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ap_reg_pp0_iter4_tmp_20_reg_355[0]_i_1 
       (.I0(ap_reg_pp0_iter3_tmp_20_reg_355),
        .I1(ap_block_pp0_stage0_subdone3_in),
        .I2(ap_reg_pp0_iter4_tmp_20_reg_355),
        .O(\ap_reg_pp0_iter4_tmp_20_reg_355[0]_i_1_n_2 ));
  FDRE \ap_reg_pp0_iter4_tmp_20_reg_355_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\ap_reg_pp0_iter4_tmp_20_reg_355[0]_i_1_n_2 ),
        .Q(ap_reg_pp0_iter4_tmp_20_reg_355),
        .R(1'b0));
  system_edge_detect_0_1_edge_detect_mac_mdEe edge_detect_mac_mdEe_U13
       (.P({p_Val2_14_fu_261_p4,tmp_3_i_i_i_fu_277_p1}),
        .Q(tmp_102_reg_369),
        .ap_block_pp0_stage0_subdone3_in(ap_block_pp0_stage0_subdone3_in),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp0_iter1_reg(ap_enable_reg_pp0_iter1_reg_n_2),
        .ap_enable_reg_pp0_iter4(ap_enable_reg_pp0_iter4),
        .ap_enable_reg_pp0_iter5_reg(ap_enable_reg_pp0_iter5_reg_n_2),
        .ap_reg_pp0_iter3_tmp_20_reg_355(ap_reg_pp0_iter3_tmp_20_reg_355),
        .ap_reg_pp0_iter4_tmp_20_reg_355(ap_reg_pp0_iter4_tmp_20_reg_355),
        .img0_data_stream_0_s_empty_n(img0_data_stream_0_s_empty_n),
        .img0_data_stream_1_s_empty_n(img0_data_stream_1_s_empty_n),
        .img0_data_stream_2_s_empty_n(img0_data_stream_2_s_empty_n),
        .img1_data_stream_0_s_full_n(img1_data_stream_0_s_full_n),
        .img1_data_stream_1_s_full_n(img1_data_stream_1_s_full_n),
        .img1_data_stream_2_s_full_n(img1_data_stream_2_s_full_n),
        .p_Val2_12_reg_384_reg({p_Val2_12_reg_384_reg_n_79,p_Val2_12_reg_384_reg_n_80,p_Val2_12_reg_384_reg_n_81,p_Val2_12_reg_384_reg_n_82,p_Val2_12_reg_384_reg_n_83,p_Val2_12_reg_384_reg_n_84,p_Val2_12_reg_384_reg_n_85,p_Val2_12_reg_384_reg_n_86,p_Val2_12_reg_384_reg_n_87,p_Val2_12_reg_384_reg_n_88,p_Val2_12_reg_384_reg_n_89,p_Val2_12_reg_384_reg_n_90,p_Val2_12_reg_384_reg_n_91,p_Val2_12_reg_384_reg_n_92,p_Val2_12_reg_384_reg_n_93,p_Val2_12_reg_384_reg_n_94,p_Val2_12_reg_384_reg_n_95,p_Val2_12_reg_384_reg_n_96,p_Val2_12_reg_384_reg_n_97,p_Val2_12_reg_384_reg_n_98,p_Val2_12_reg_384_reg_n_99,p_Val2_12_reg_384_reg_n_100,p_Val2_12_reg_384_reg_n_101,p_Val2_12_reg_384_reg_n_102,p_Val2_12_reg_384_reg_n_103,p_Val2_12_reg_384_reg_n_104,p_Val2_12_reg_384_reg_n_105,p_Val2_12_reg_384_reg_n_106,p_Val2_12_reg_384_reg_n_107}),
        .\r_V_1_reg_389_reg[29] (edge_detect_mac_mdEe_U13_n_12),
        .tmp_20_reg_355(tmp_20_reg_355),
        .tmp_98_fu_287_p3(tmp_98_fu_287_p3));
  system_edge_detect_0_1_edge_detect_mul_mbkb edge_detect_mul_mbkb_U11
       (.Q(tmp_101_reg_364),
        .out(r_V_i_fu_323_p2));
  LUT1 #(
    .INIT(2'h1)) 
    \i_1_reg_350[0]_i_1 
       (.I0(\i_reg_203_reg_n_2_[0] ),
        .O(i_1_fu_231_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_1_reg_350[1]_i_1 
       (.I0(\i_reg_203_reg_n_2_[0] ),
        .I1(\i_reg_203_reg_n_2_[1] ),
        .O(i_1_fu_231_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \i_1_reg_350[2]_i_1 
       (.I0(\i_reg_203_reg_n_2_[2] ),
        .I1(\i_reg_203_reg_n_2_[1] ),
        .I2(\i_reg_203_reg_n_2_[0] ),
        .O(i_1_fu_231_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \i_1_reg_350[3]_i_1 
       (.I0(\i_reg_203_reg_n_2_[3] ),
        .I1(\i_reg_203_reg_n_2_[0] ),
        .I2(\i_reg_203_reg_n_2_[1] ),
        .I3(\i_reg_203_reg_n_2_[2] ),
        .O(i_1_fu_231_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \i_1_reg_350[4]_i_1 
       (.I0(\i_reg_203_reg_n_2_[4] ),
        .I1(\i_reg_203_reg_n_2_[2] ),
        .I2(\i_reg_203_reg_n_2_[1] ),
        .I3(\i_reg_203_reg_n_2_[0] ),
        .I4(\i_reg_203_reg_n_2_[3] ),
        .O(i_1_fu_231_p2[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \i_1_reg_350[5]_i_1 
       (.I0(\i_reg_203_reg_n_2_[5] ),
        .I1(\i_reg_203_reg_n_2_[3] ),
        .I2(\i_reg_203_reg_n_2_[0] ),
        .I3(\i_reg_203_reg_n_2_[1] ),
        .I4(\i_reg_203_reg_n_2_[2] ),
        .I5(\i_reg_203_reg_n_2_[4] ),
        .O(i_1_fu_231_p2[5]));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_1_reg_350[6]_i_1 
       (.I0(\i_reg_203_reg_n_2_[6] ),
        .I1(\i_1_reg_350[9]_i_2_n_2 ),
        .O(i_1_fu_231_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \i_1_reg_350[7]_i_1 
       (.I0(\i_reg_203_reg_n_2_[7] ),
        .I1(\i_1_reg_350[9]_i_2_n_2 ),
        .I2(\i_reg_203_reg_n_2_[6] ),
        .O(i_1_fu_231_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \i_1_reg_350[8]_i_1 
       (.I0(\i_reg_203_reg_n_2_[8] ),
        .I1(\i_reg_203_reg_n_2_[6] ),
        .I2(\i_reg_203_reg_n_2_[7] ),
        .I3(\i_1_reg_350[9]_i_2_n_2 ),
        .O(i_1_fu_231_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \i_1_reg_350[9]_i_1 
       (.I0(\i_reg_203_reg_n_2_[9] ),
        .I1(\i_1_reg_350[9]_i_2_n_2 ),
        .I2(\i_reg_203_reg_n_2_[7] ),
        .I3(\i_reg_203_reg_n_2_[6] ),
        .I4(\i_reg_203_reg_n_2_[8] ),
        .O(i_1_fu_231_p2[9]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \i_1_reg_350[9]_i_2 
       (.I0(\i_reg_203_reg_n_2_[5] ),
        .I1(\i_reg_203_reg_n_2_[3] ),
        .I2(\i_reg_203_reg_n_2_[0] ),
        .I3(\i_reg_203_reg_n_2_[1] ),
        .I4(\i_reg_203_reg_n_2_[2] ),
        .I5(\i_reg_203_reg_n_2_[4] ),
        .O(\i_1_reg_350[9]_i_2_n_2 ));
  FDRE \i_1_reg_350_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_1_fu_231_p2[0]),
        .Q(i_1_reg_350[0]),
        .R(1'b0));
  FDRE \i_1_reg_350_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_1_fu_231_p2[1]),
        .Q(i_1_reg_350[1]),
        .R(1'b0));
  FDRE \i_1_reg_350_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_1_fu_231_p2[2]),
        .Q(i_1_reg_350[2]),
        .R(1'b0));
  FDRE \i_1_reg_350_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_1_fu_231_p2[3]),
        .Q(i_1_reg_350[3]),
        .R(1'b0));
  FDRE \i_1_reg_350_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_1_fu_231_p2[4]),
        .Q(i_1_reg_350[4]),
        .R(1'b0));
  FDRE \i_1_reg_350_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_1_fu_231_p2[5]),
        .Q(i_1_reg_350[5]),
        .R(1'b0));
  FDRE \i_1_reg_350_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_1_fu_231_p2[6]),
        .Q(i_1_reg_350[6]),
        .R(1'b0));
  FDRE \i_1_reg_350_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_1_fu_231_p2[7]),
        .Q(i_1_reg_350[7]),
        .R(1'b0));
  FDRE \i_1_reg_350_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_1_fu_231_p2[8]),
        .Q(i_1_reg_350[8]),
        .R(1'b0));
  FDRE \i_1_reg_350_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_1_fu_231_p2[9]),
        .Q(i_1_reg_350[9]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h0000A800)) 
    \i_reg_203[9]_i_1 
       (.I0(CvtColor_U0_ap_start),
        .I1(start_for_Sobel_U0_full_n),
        .I2(start_once_reg),
        .I3(\ap_CS_fsm_reg_n_2_[0] ),
        .I4(ap_CS_fsm_state9),
        .O(i_reg_203));
  FDRE \i_reg_203_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(i_1_reg_350[0]),
        .Q(\i_reg_203_reg_n_2_[0] ),
        .R(i_reg_203));
  FDRE \i_reg_203_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(i_1_reg_350[1]),
        .Q(\i_reg_203_reg_n_2_[1] ),
        .R(i_reg_203));
  FDRE \i_reg_203_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(i_1_reg_350[2]),
        .Q(\i_reg_203_reg_n_2_[2] ),
        .R(i_reg_203));
  FDRE \i_reg_203_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(i_1_reg_350[3]),
        .Q(\i_reg_203_reg_n_2_[3] ),
        .R(i_reg_203));
  FDRE \i_reg_203_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(i_1_reg_350[4]),
        .Q(\i_reg_203_reg_n_2_[4] ),
        .R(i_reg_203));
  FDRE \i_reg_203_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(i_1_reg_350[5]),
        .Q(\i_reg_203_reg_n_2_[5] ),
        .R(i_reg_203));
  FDRE \i_reg_203_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(i_1_reg_350[6]),
        .Q(\i_reg_203_reg_n_2_[6] ),
        .R(i_reg_203));
  FDRE \i_reg_203_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(i_1_reg_350[7]),
        .Q(\i_reg_203_reg_n_2_[7] ),
        .R(i_reg_203));
  FDRE \i_reg_203_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(i_1_reg_350[8]),
        .Q(\i_reg_203_reg_n_2_[8] ),
        .R(i_reg_203));
  FDRE \i_reg_203_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(i_1_reg_350[9]),
        .Q(\i_reg_203_reg_n_2_[9] ),
        .R(i_reg_203));
  LUT5 #(
    .INIT(32'h2AAAAAAA)) 
    internal_full_n_i_2
       (.I0(\exitcond_reg_420_reg[0] ),
        .I1(tmp_20_reg_355),
        .I2(ap_enable_reg_pp0_iter1_reg_n_2),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(ap_block_pp0_stage0_subdone3_in),
        .O(internal_empty_n4_out));
  LUT1 #(
    .INIT(2'h1)) 
    \j_reg_214[0]_i_1 
       (.I0(\j_reg_214_reg_n_2_[0] ),
        .O(j_1_fu_243_p2[0]));
  LUT2 #(
    .INIT(4'h2)) 
    \j_reg_214[10]_i_1 
       (.I0(ap_enable_reg_pp0_iter00),
        .I1(j_reg_2140),
        .O(j_reg_214));
  LUT6 #(
    .INIT(64'h0808088800000000)) 
    \j_reg_214[10]_i_2 
       (.I0(ap_block_pp0_stage0_subdone3_in),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(j_reg_214_reg__0[10]),
        .I3(j_reg_214_reg__0[9]),
        .I4(j_reg_214_reg__0[8]),
        .I5(ap_enable_reg_pp0_iter0),
        .O(j_reg_2140));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \j_reg_214[10]_i_3 
       (.I0(j_reg_214_reg__0[10]),
        .I1(j_reg_214_reg__0[8]),
        .I2(\j_reg_214_reg_n_2_[6] ),
        .I3(\j_reg_214[10]_i_4_n_2 ),
        .I4(\j_reg_214_reg_n_2_[7] ),
        .I5(j_reg_214_reg__0[9]),
        .O(j_1_fu_243_p2[10]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \j_reg_214[10]_i_4 
       (.I0(\j_reg_214_reg_n_2_[5] ),
        .I1(\j_reg_214_reg_n_2_[3] ),
        .I2(\j_reg_214_reg_n_2_[0] ),
        .I3(\j_reg_214_reg_n_2_[1] ),
        .I4(\j_reg_214_reg_n_2_[2] ),
        .I5(\j_reg_214_reg_n_2_[4] ),
        .O(\j_reg_214[10]_i_4_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \j_reg_214[1]_i_1 
       (.I0(\j_reg_214_reg_n_2_[0] ),
        .I1(\j_reg_214_reg_n_2_[1] ),
        .O(j_1_fu_243_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \j_reg_214[2]_i_1 
       (.I0(\j_reg_214_reg_n_2_[2] ),
        .I1(\j_reg_214_reg_n_2_[1] ),
        .I2(\j_reg_214_reg_n_2_[0] ),
        .O(j_1_fu_243_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \j_reg_214[3]_i_1 
       (.I0(\j_reg_214_reg_n_2_[3] ),
        .I1(\j_reg_214_reg_n_2_[0] ),
        .I2(\j_reg_214_reg_n_2_[1] ),
        .I3(\j_reg_214_reg_n_2_[2] ),
        .O(j_1_fu_243_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \j_reg_214[4]_i_1 
       (.I0(\j_reg_214_reg_n_2_[4] ),
        .I1(\j_reg_214_reg_n_2_[2] ),
        .I2(\j_reg_214_reg_n_2_[1] ),
        .I3(\j_reg_214_reg_n_2_[0] ),
        .I4(\j_reg_214_reg_n_2_[3] ),
        .O(j_1_fu_243_p2[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \j_reg_214[5]_i_1 
       (.I0(\j_reg_214_reg_n_2_[5] ),
        .I1(\j_reg_214_reg_n_2_[3] ),
        .I2(\j_reg_214_reg_n_2_[0] ),
        .I3(\j_reg_214_reg_n_2_[1] ),
        .I4(\j_reg_214_reg_n_2_[2] ),
        .I5(\j_reg_214_reg_n_2_[4] ),
        .O(j_1_fu_243_p2[5]));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \j_reg_214[6]_i_1 
       (.I0(\j_reg_214_reg_n_2_[6] ),
        .I1(\j_reg_214[10]_i_4_n_2 ),
        .O(j_1_fu_243_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \j_reg_214[7]_i_1 
       (.I0(\j_reg_214_reg_n_2_[7] ),
        .I1(\j_reg_214[10]_i_4_n_2 ),
        .I2(\j_reg_214_reg_n_2_[6] ),
        .O(j_1_fu_243_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \j_reg_214[8]_i_1 
       (.I0(j_reg_214_reg__0[8]),
        .I1(\j_reg_214_reg_n_2_[6] ),
        .I2(\j_reg_214[10]_i_4_n_2 ),
        .I3(\j_reg_214_reg_n_2_[7] ),
        .O(j_1_fu_243_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \j_reg_214[9]_i_1 
       (.I0(j_reg_214_reg__0[9]),
        .I1(\j_reg_214_reg_n_2_[7] ),
        .I2(\j_reg_214[10]_i_4_n_2 ),
        .I3(\j_reg_214_reg_n_2_[6] ),
        .I4(j_reg_214_reg__0[8]),
        .O(j_1_fu_243_p2[9]));
  FDRE \j_reg_214_reg[0] 
       (.C(ap_clk),
        .CE(j_reg_2140),
        .D(j_1_fu_243_p2[0]),
        .Q(\j_reg_214_reg_n_2_[0] ),
        .R(j_reg_214));
  FDRE \j_reg_214_reg[10] 
       (.C(ap_clk),
        .CE(j_reg_2140),
        .D(j_1_fu_243_p2[10]),
        .Q(j_reg_214_reg__0[10]),
        .R(j_reg_214));
  FDRE \j_reg_214_reg[1] 
       (.C(ap_clk),
        .CE(j_reg_2140),
        .D(j_1_fu_243_p2[1]),
        .Q(\j_reg_214_reg_n_2_[1] ),
        .R(j_reg_214));
  FDRE \j_reg_214_reg[2] 
       (.C(ap_clk),
        .CE(j_reg_2140),
        .D(j_1_fu_243_p2[2]),
        .Q(\j_reg_214_reg_n_2_[2] ),
        .R(j_reg_214));
  FDRE \j_reg_214_reg[3] 
       (.C(ap_clk),
        .CE(j_reg_2140),
        .D(j_1_fu_243_p2[3]),
        .Q(\j_reg_214_reg_n_2_[3] ),
        .R(j_reg_214));
  FDRE \j_reg_214_reg[4] 
       (.C(ap_clk),
        .CE(j_reg_2140),
        .D(j_1_fu_243_p2[4]),
        .Q(\j_reg_214_reg_n_2_[4] ),
        .R(j_reg_214));
  FDRE \j_reg_214_reg[5] 
       (.C(ap_clk),
        .CE(j_reg_2140),
        .D(j_1_fu_243_p2[5]),
        .Q(\j_reg_214_reg_n_2_[5] ),
        .R(j_reg_214));
  FDRE \j_reg_214_reg[6] 
       (.C(ap_clk),
        .CE(j_reg_2140),
        .D(j_1_fu_243_p2[6]),
        .Q(\j_reg_214_reg_n_2_[6] ),
        .R(j_reg_214));
  FDRE \j_reg_214_reg[7] 
       (.C(ap_clk),
        .CE(j_reg_2140),
        .D(j_1_fu_243_p2[7]),
        .Q(\j_reg_214_reg_n_2_[7] ),
        .R(j_reg_214));
  FDRE \j_reg_214_reg[8] 
       (.C(ap_clk),
        .CE(j_reg_2140),
        .D(j_1_fu_243_p2[8]),
        .Q(j_reg_214_reg__0[8]),
        .R(j_reg_214));
  FDRE \j_reg_214_reg[9] 
       (.C(ap_clk),
        .CE(j_reg_2140),
        .D(j_1_fu_243_p2[9]),
        .Q(j_reg_214_reg__0[9]),
        .R(j_reg_214));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \mOutPtr[1]_i_1 
       (.I0(\exitcond_reg_420_reg[0] ),
        .I1(tmp_20_reg_355),
        .I2(ap_enable_reg_pp0_iter1_reg_n_2),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(ap_block_pp0_stage0_subdone3_in),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT5 #(
    .INIT(32'h00008000)) 
    \mOutPtr[1]_i_3 
       (.I0(tmp_20_reg_355),
        .I1(ap_enable_reg_pp0_iter1_reg_n_2),
        .I2(ap_CS_fsm_pp0_stage0),
        .I3(ap_block_pp0_stage0_subdone3_in),
        .I4(\exitcond_reg_420_reg[0] ),
        .O(internal_full_n_reg));
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(2),
    .BREG(2),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    p_Val2_12_reg_384_reg
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b0,1'b1,1'b0,1'b0,1'b1,1'b0,1'b1,1'b1,1'b1,1'b1,1'b0,1'b0,1'b0,1'b1,1'b1,1'b0}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_p_Val2_12_reg_384_reg_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,B}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_p_Val2_12_reg_384_reg_BCOUT_UNCONNECTED[17:0]),
        .C({r_V_i_fu_323_p2[28],r_V_i_fu_323_p2[28],r_V_i_fu_323_p2[28],r_V_i_fu_323_p2[28],r_V_i_fu_323_p2[28],r_V_i_fu_323_p2[28],r_V_i_fu_323_p2[28],r_V_i_fu_323_p2[28],r_V_i_fu_323_p2[28],r_V_i_fu_323_p2[28],r_V_i_fu_323_p2[28],r_V_i_fu_323_p2[28],r_V_i_fu_323_p2[28],r_V_i_fu_323_p2[28],r_V_i_fu_323_p2[28],r_V_i_fu_323_p2[28],r_V_i_fu_323_p2[28],r_V_i_fu_323_p2[28],r_V_i_fu_323_p2[28],r_V_i_fu_323_p2}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_p_Val2_12_reg_384_reg_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_p_Val2_12_reg_384_reg_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(tmp_101_reg_3640),
        .CEB2(ap_block_pp0_stage0_subdone3_in),
        .CEC(r_V_i_reg_3790),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(p_Val2_12_reg_3840),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_p_Val2_12_reg_384_reg_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b1,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_p_Val2_12_reg_384_reg_OVERFLOW_UNCONNECTED),
        .P({NLW_p_Val2_12_reg_384_reg_P_UNCONNECTED[47:29],p_Val2_12_reg_384_reg_n_79,p_Val2_12_reg_384_reg_n_80,p_Val2_12_reg_384_reg_n_81,p_Val2_12_reg_384_reg_n_82,p_Val2_12_reg_384_reg_n_83,p_Val2_12_reg_384_reg_n_84,p_Val2_12_reg_384_reg_n_85,p_Val2_12_reg_384_reg_n_86,p_Val2_12_reg_384_reg_n_87,p_Val2_12_reg_384_reg_n_88,p_Val2_12_reg_384_reg_n_89,p_Val2_12_reg_384_reg_n_90,p_Val2_12_reg_384_reg_n_91,p_Val2_12_reg_384_reg_n_92,p_Val2_12_reg_384_reg_n_93,p_Val2_12_reg_384_reg_n_94,p_Val2_12_reg_384_reg_n_95,p_Val2_12_reg_384_reg_n_96,p_Val2_12_reg_384_reg_n_97,p_Val2_12_reg_384_reg_n_98,p_Val2_12_reg_384_reg_n_99,p_Val2_12_reg_384_reg_n_100,p_Val2_12_reg_384_reg_n_101,p_Val2_12_reg_384_reg_n_102,p_Val2_12_reg_384_reg_n_103,p_Val2_12_reg_384_reg_n_104,p_Val2_12_reg_384_reg_n_105,p_Val2_12_reg_384_reg_n_106,p_Val2_12_reg_384_reg_n_107}),
        .PATTERNBDETECT(NLW_p_Val2_12_reg_384_reg_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_p_Val2_12_reg_384_reg_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_p_Val2_12_reg_384_reg_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_p_Val2_12_reg_384_reg_UNDERFLOW_UNCONNECTED));
  LUT3 #(
    .INIT(8'h80)) 
    p_Val2_12_reg_384_reg_i_1
       (.I0(tmp_20_reg_355),
        .I1(ap_block_pp0_stage0_subdone3_in),
        .I2(ap_CS_fsm_pp0_stage0),
        .O(tmp_101_reg_3640));
  LUT2 #(
    .INIT(4'h8)) 
    p_Val2_12_reg_384_reg_i_3
       (.I0(ap_reg_pp0_iter1_tmp_20_reg_355),
        .I1(ap_block_pp0_stage0_subdone3_in),
        .O(r_V_i_reg_3790));
  LUT3 #(
    .INIT(8'h80)) 
    p_Val2_12_reg_384_reg_i_4
       (.I0(ap_block_pp0_stage0_subdone3_in),
        .I1(ap_enable_reg_pp0_iter3),
        .I2(ap_reg_pp0_iter2_tmp_20_reg_355),
        .O(p_Val2_12_reg_3840));
  LUT2 #(
    .INIT(4'h6)) 
    \p_Val2_15_reg_394[0]_i_1 
       (.I0(p_Val2_14_fu_261_p4[0]),
        .I1(tmp_3_i_i_i_fu_277_p1),
        .O(p_Val2_15_fu_281_p2__0[0]));
  LUT3 #(
    .INIT(8'h78)) 
    \p_Val2_15_reg_394[1]_i_1 
       (.I0(p_Val2_14_fu_261_p4[0]),
        .I1(tmp_3_i_i_i_fu_277_p1),
        .I2(p_Val2_14_fu_261_p4[1]),
        .O(p_Val2_15_fu_281_p2__0[1]));
  LUT4 #(
    .INIT(16'h7F80)) 
    \p_Val2_15_reg_394[2]_i_1 
       (.I0(tmp_3_i_i_i_fu_277_p1),
        .I1(p_Val2_14_fu_261_p4[0]),
        .I2(p_Val2_14_fu_261_p4[1]),
        .I3(p_Val2_14_fu_261_p4[2]),
        .O(p_Val2_15_fu_281_p2__0[2]));
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \p_Val2_15_reg_394[3]_i_1 
       (.I0(p_Val2_14_fu_261_p4[1]),
        .I1(p_Val2_14_fu_261_p4[0]),
        .I2(tmp_3_i_i_i_fu_277_p1),
        .I3(p_Val2_14_fu_261_p4[2]),
        .I4(p_Val2_14_fu_261_p4[3]),
        .O(p_Val2_15_fu_281_p2__0[3]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \p_Val2_15_reg_394[4]_i_1 
       (.I0(p_Val2_14_fu_261_p4[2]),
        .I1(tmp_3_i_i_i_fu_277_p1),
        .I2(p_Val2_14_fu_261_p4[0]),
        .I3(p_Val2_14_fu_261_p4[1]),
        .I4(p_Val2_14_fu_261_p4[3]),
        .I5(p_Val2_14_fu_261_p4[4]),
        .O(p_Val2_15_fu_281_p2__0[4]));
  LUT2 #(
    .INIT(4'h6)) 
    \p_Val2_15_reg_394[5]_i_1 
       (.I0(\p_Val2_15_reg_394[7]_i_3_n_2 ),
        .I1(p_Val2_14_fu_261_p4[5]),
        .O(p_Val2_15_fu_281_p2__0[5]));
  LUT3 #(
    .INIT(8'h78)) 
    \p_Val2_15_reg_394[6]_i_1 
       (.I0(\p_Val2_15_reg_394[7]_i_3_n_2 ),
        .I1(p_Val2_14_fu_261_p4[5]),
        .I2(p_Val2_14_fu_261_p4[6]),
        .O(p_Val2_15_fu_281_p2__0[6]));
  LUT2 #(
    .INIT(4'h8)) 
    \p_Val2_15_reg_394[7]_i_1 
       (.I0(ap_reg_pp0_iter3_tmp_20_reg_355),
        .I1(ap_block_pp0_stage0_subdone3_in),
        .O(p_Val2_15_reg_3940));
  LUT4 #(
    .INIT(16'h7F80)) 
    \p_Val2_15_reg_394[7]_i_2 
       (.I0(p_Val2_14_fu_261_p4[5]),
        .I1(\p_Val2_15_reg_394[7]_i_3_n_2 ),
        .I2(p_Val2_14_fu_261_p4[6]),
        .I3(p_Val2_14_fu_261_p4[7]),
        .O(p_Val2_15_fu_281_p2__0[7]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \p_Val2_15_reg_394[7]_i_3 
       (.I0(p_Val2_14_fu_261_p4[4]),
        .I1(p_Val2_14_fu_261_p4[2]),
        .I2(tmp_3_i_i_i_fu_277_p1),
        .I3(p_Val2_14_fu_261_p4[0]),
        .I4(p_Val2_14_fu_261_p4[1]),
        .I5(p_Val2_14_fu_261_p4[3]),
        .O(\p_Val2_15_reg_394[7]_i_3_n_2 ));
  FDRE \p_Val2_15_reg_394_reg[0] 
       (.C(ap_clk),
        .CE(p_Val2_15_reg_3940),
        .D(p_Val2_15_fu_281_p2__0[0]),
        .Q(Q[0]),
        .R(1'b0));
  FDRE \p_Val2_15_reg_394_reg[1] 
       (.C(ap_clk),
        .CE(p_Val2_15_reg_3940),
        .D(p_Val2_15_fu_281_p2__0[1]),
        .Q(Q[1]),
        .R(1'b0));
  FDRE \p_Val2_15_reg_394_reg[2] 
       (.C(ap_clk),
        .CE(p_Val2_15_reg_3940),
        .D(p_Val2_15_fu_281_p2__0[2]),
        .Q(Q[2]),
        .R(1'b0));
  FDRE \p_Val2_15_reg_394_reg[3] 
       (.C(ap_clk),
        .CE(p_Val2_15_reg_3940),
        .D(p_Val2_15_fu_281_p2__0[3]),
        .Q(Q[3]),
        .R(1'b0));
  FDRE \p_Val2_15_reg_394_reg[4] 
       (.C(ap_clk),
        .CE(p_Val2_15_reg_3940),
        .D(p_Val2_15_fu_281_p2__0[4]),
        .Q(Q[4]),
        .R(1'b0));
  FDRE \p_Val2_15_reg_394_reg[5] 
       (.C(ap_clk),
        .CE(p_Val2_15_reg_3940),
        .D(p_Val2_15_fu_281_p2__0[5]),
        .Q(Q[5]),
        .R(1'b0));
  FDRE \p_Val2_15_reg_394_reg[6] 
       (.C(ap_clk),
        .CE(p_Val2_15_reg_3940),
        .D(p_Val2_15_fu_281_p2__0[6]),
        .Q(Q[6]),
        .R(1'b0));
  FDRE \p_Val2_15_reg_394_reg[7] 
       (.C(ap_clk),
        .CE(p_Val2_15_reg_3940),
        .D(p_Val2_15_fu_281_p2__0[7]),
        .Q(Q[7]),
        .R(1'b0));
  FDRE \r_V_1_reg_389_reg[29] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(edge_detect_mac_mdEe_U13_n_12),
        .Q(tmp_98_fu_287_p3),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h5444)) 
    start_once_reg_i_1
       (.I0(start_once_reg_reg_0),
        .I1(start_once_reg),
        .I2(start_for_Sobel_U0_full_n),
        .I3(CvtColor_U0_ap_start),
        .O(start_once_reg_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    start_once_reg_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(start_once_reg_i_1_n_2),
        .Q(start_once_reg),
        .R(ap_rst_n_inv));
  FDRE \tmp_101_reg_364_reg[0] 
       (.C(ap_clk),
        .CE(tmp_101_reg_3640),
        .D(\SRL_SIG_reg[0][7]_0 [0]),
        .Q(tmp_101_reg_364[0]),
        .R(1'b0));
  FDRE \tmp_101_reg_364_reg[1] 
       (.C(ap_clk),
        .CE(tmp_101_reg_3640),
        .D(\SRL_SIG_reg[0][7]_0 [1]),
        .Q(tmp_101_reg_364[1]),
        .R(1'b0));
  FDRE \tmp_101_reg_364_reg[2] 
       (.C(ap_clk),
        .CE(tmp_101_reg_3640),
        .D(\SRL_SIG_reg[0][7]_0 [2]),
        .Q(tmp_101_reg_364[2]),
        .R(1'b0));
  FDRE \tmp_101_reg_364_reg[3] 
       (.C(ap_clk),
        .CE(tmp_101_reg_3640),
        .D(\SRL_SIG_reg[0][7]_0 [3]),
        .Q(tmp_101_reg_364[3]),
        .R(1'b0));
  FDRE \tmp_101_reg_364_reg[4] 
       (.C(ap_clk),
        .CE(tmp_101_reg_3640),
        .D(\SRL_SIG_reg[0][7]_0 [4]),
        .Q(tmp_101_reg_364[4]),
        .R(1'b0));
  FDRE \tmp_101_reg_364_reg[5] 
       (.C(ap_clk),
        .CE(tmp_101_reg_3640),
        .D(\SRL_SIG_reg[0][7]_0 [5]),
        .Q(tmp_101_reg_364[5]),
        .R(1'b0));
  FDRE \tmp_101_reg_364_reg[6] 
       (.C(ap_clk),
        .CE(tmp_101_reg_3640),
        .D(\SRL_SIG_reg[0][7]_0 [6]),
        .Q(tmp_101_reg_364[6]),
        .R(1'b0));
  FDRE \tmp_101_reg_364_reg[7] 
       (.C(ap_clk),
        .CE(tmp_101_reg_3640),
        .D(\SRL_SIG_reg[0][7]_0 [7]),
        .Q(tmp_101_reg_364[7]),
        .R(1'b0));
  FDRE \tmp_102_reg_369_reg[0] 
       (.C(ap_clk),
        .CE(tmp_101_reg_3640),
        .D(D[0]),
        .Q(tmp_102_reg_369[0]),
        .R(1'b0));
  FDRE \tmp_102_reg_369_reg[1] 
       (.C(ap_clk),
        .CE(tmp_101_reg_3640),
        .D(D[1]),
        .Q(tmp_102_reg_369[1]),
        .R(1'b0));
  FDRE \tmp_102_reg_369_reg[2] 
       (.C(ap_clk),
        .CE(tmp_101_reg_3640),
        .D(D[2]),
        .Q(tmp_102_reg_369[2]),
        .R(1'b0));
  FDRE \tmp_102_reg_369_reg[3] 
       (.C(ap_clk),
        .CE(tmp_101_reg_3640),
        .D(D[3]),
        .Q(tmp_102_reg_369[3]),
        .R(1'b0));
  FDRE \tmp_102_reg_369_reg[4] 
       (.C(ap_clk),
        .CE(tmp_101_reg_3640),
        .D(D[4]),
        .Q(tmp_102_reg_369[4]),
        .R(1'b0));
  FDRE \tmp_102_reg_369_reg[5] 
       (.C(ap_clk),
        .CE(tmp_101_reg_3640),
        .D(D[5]),
        .Q(tmp_102_reg_369[5]),
        .R(1'b0));
  FDRE \tmp_102_reg_369_reg[6] 
       (.C(ap_clk),
        .CE(tmp_101_reg_3640),
        .D(D[6]),
        .Q(tmp_102_reg_369[6]),
        .R(1'b0));
  FDRE \tmp_102_reg_369_reg[7] 
       (.C(ap_clk),
        .CE(tmp_101_reg_3640),
        .D(D[7]),
        .Q(tmp_102_reg_369[7]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h57FFFFFF57000000)) 
    \tmp_20_reg_355[0]_i_1 
       (.I0(j_reg_214_reg__0[10]),
        .I1(j_reg_214_reg__0[9]),
        .I2(j_reg_214_reg__0[8]),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(ap_block_pp0_stage0_subdone3_in),
        .I5(tmp_20_reg_355),
        .O(\tmp_20_reg_355[0]_i_1_n_2 ));
  FDRE \tmp_20_reg_355_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_20_reg_355[0]_i_1_n_2 ),
        .Q(tmp_20_reg_355),
        .R(1'b0));
endmodule

module system_edge_detect_0_1_CvtColor_1
   (start_once_reg,
    start_once_reg_reg_0,
    shiftReg_ce,
    ap_rst_n_inv,
    ap_clk,
    ap_rst_n,
    CvtColor_1_U0_ap_start,
    start_for_Mat2AXIvideo_U0_full_n,
    internal_empty_n_reg);
  output start_once_reg;
  output start_once_reg_reg_0;
  output shiftReg_ce;
  input ap_rst_n_inv;
  input ap_clk;
  input ap_rst_n;
  input CvtColor_1_U0_ap_start;
  input start_for_Mat2AXIvideo_U0_full_n;
  input internal_empty_n_reg;

  wire CvtColor_1_U0_ap_start;
  wire \ap_CS_fsm[2]_i_3__0_n_2 ;
  wire \ap_CS_fsm[2]_i_4_n_2 ;
  wire \ap_CS_fsm[3]_i_2__1_n_2 ;
  wire ap_CS_fsm_pp0_stage0;
  wire \ap_CS_fsm_reg_n_2_[0] ;
  wire ap_CS_fsm_state2;
  wire ap_CS_fsm_state5;
  wire [3:0]ap_NS_fsm;
  wire ap_clk;
  wire ap_enable_reg_pp0_iter0;
  wire ap_enable_reg_pp0_iter00;
  wire ap_enable_reg_pp0_iter0_i_1__1_n_2;
  wire ap_enable_reg_pp0_iter1_i_1__0_n_2;
  wire ap_enable_reg_pp0_iter1_reg_n_2;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire [9:0]i_1_fu_210_p2;
  wire [9:0]i_1_reg_232;
  wire \i_1_reg_232[9]_i_2_n_2 ;
  wire i_reg_182;
  wire \i_reg_182_reg_n_2_[0] ;
  wire \i_reg_182_reg_n_2_[1] ;
  wire \i_reg_182_reg_n_2_[2] ;
  wire \i_reg_182_reg_n_2_[3] ;
  wire \i_reg_182_reg_n_2_[4] ;
  wire \i_reg_182_reg_n_2_[5] ;
  wire \i_reg_182_reg_n_2_[6] ;
  wire \i_reg_182_reg_n_2_[7] ;
  wire \i_reg_182_reg_n_2_[8] ;
  wire \i_reg_182_reg_n_2_[9] ;
  wire internal_empty_n_reg;
  wire [10:0]j_1_fu_222_p2;
  wire j_reg_193;
  wire j_reg_1930;
  wire \j_reg_193[10]_i_4_n_2 ;
  wire [10:8]j_reg_193_reg__0;
  wire \j_reg_193_reg_n_2_[0] ;
  wire \j_reg_193_reg_n_2_[1] ;
  wire \j_reg_193_reg_n_2_[2] ;
  wire \j_reg_193_reg_n_2_[3] ;
  wire \j_reg_193_reg_n_2_[4] ;
  wire \j_reg_193_reg_n_2_[5] ;
  wire \j_reg_193_reg_n_2_[6] ;
  wire \j_reg_193_reg_n_2_[7] ;
  wire shiftReg_ce;
  wire start_for_Mat2AXIvideo_U0_full_n;
  wire start_once_reg;
  wire start_once_reg_i_1__1_n_2;
  wire start_once_reg_reg_0;
  wire tmp_21_fu_216_p2;
  wire tmp_21_reg_237;
  wire \tmp_21_reg_237[0]_i_1_n_2 ;

  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \SRL_SIG[0][0]_i_1 
       (.I0(tmp_21_reg_237),
        .I1(ap_enable_reg_pp0_iter1_reg_n_2),
        .I2(\ap_CS_fsm[3]_i_2__1_n_2 ),
        .I3(ap_CS_fsm_pp0_stage0),
        .O(shiftReg_ce));
  LUT5 #(
    .INIT(32'hBBBFAAAA)) 
    \ap_CS_fsm[0]_i_1__1 
       (.I0(start_once_reg_reg_0),
        .I1(CvtColor_1_U0_ap_start),
        .I2(start_for_Mat2AXIvideo_U0_full_n),
        .I3(start_once_reg),
        .I4(\ap_CS_fsm_reg_n_2_[0] ),
        .O(ap_NS_fsm[0]));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \ap_CS_fsm[0]_i_2__1 
       (.I0(ap_CS_fsm_state2),
        .I1(\ap_CS_fsm[2]_i_4_n_2 ),
        .O(start_once_reg_reg_0));
  LUT5 #(
    .INIT(32'hEEEAAAAA)) 
    \ap_CS_fsm[1]_i_1__3 
       (.I0(ap_CS_fsm_state5),
        .I1(CvtColor_1_U0_ap_start),
        .I2(start_for_Mat2AXIvideo_U0_full_n),
        .I3(start_once_reg),
        .I4(\ap_CS_fsm_reg_n_2_[0] ),
        .O(ap_NS_fsm[1]));
  LUT4 #(
    .INIT(16'hBFAA)) 
    \ap_CS_fsm[2]_i_1__2 
       (.I0(ap_enable_reg_pp0_iter00),
        .I1(ap_enable_reg_pp0_iter0),
        .I2(\ap_CS_fsm[2]_i_3__0_n_2 ),
        .I3(ap_CS_fsm_pp0_stage0),
        .O(ap_NS_fsm[2]));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \ap_CS_fsm[2]_i_2__0 
       (.I0(ap_CS_fsm_state2),
        .I1(\ap_CS_fsm[2]_i_4_n_2 ),
        .O(ap_enable_reg_pp0_iter00));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT4 #(
    .INIT(16'h5400)) 
    \ap_CS_fsm[2]_i_3__0 
       (.I0(\ap_CS_fsm[3]_i_2__1_n_2 ),
        .I1(j_reg_193_reg__0[8]),
        .I2(j_reg_193_reg__0[9]),
        .I3(j_reg_193_reg__0[10]),
        .O(\ap_CS_fsm[2]_i_3__0_n_2 ));
  LUT6 #(
    .INIT(64'h5555555557FFFFFF)) 
    \ap_CS_fsm[2]_i_4 
       (.I0(\i_reg_182_reg_n_2_[9] ),
        .I1(\i_reg_182_reg_n_2_[5] ),
        .I2(\i_reg_182_reg_n_2_[4] ),
        .I3(\i_reg_182_reg_n_2_[7] ),
        .I4(\i_reg_182_reg_n_2_[6] ),
        .I5(\i_reg_182_reg_n_2_[8] ),
        .O(\ap_CS_fsm[2]_i_4_n_2 ));
  LUT6 #(
    .INIT(64'h2220000000000000)) 
    \ap_CS_fsm[3]_i_1__1 
       (.I0(ap_enable_reg_pp0_iter0),
        .I1(\ap_CS_fsm[3]_i_2__1_n_2 ),
        .I2(j_reg_193_reg__0[8]),
        .I3(j_reg_193_reg__0[9]),
        .I4(j_reg_193_reg__0[10]),
        .I5(ap_CS_fsm_pp0_stage0),
        .O(ap_NS_fsm[3]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \ap_CS_fsm[3]_i_2__1 
       (.I0(tmp_21_reg_237),
        .I1(ap_enable_reg_pp0_iter1_reg_n_2),
        .I2(internal_empty_n_reg),
        .O(\ap_CS_fsm[3]_i_2__1_n_2 ));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[0]),
        .Q(\ap_CS_fsm_reg_n_2_[0] ),
        .S(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ap_CS_fsm_state2),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[2]),
        .Q(ap_CS_fsm_pp0_stage0),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[3]),
        .Q(ap_CS_fsm_state5),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'h70707000)) 
    ap_enable_reg_pp0_iter0_i_1__1
       (.I0(\ap_CS_fsm[2]_i_3__0_n_2 ),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(ap_rst_n),
        .I3(ap_enable_reg_pp0_iter00),
        .I4(ap_enable_reg_pp0_iter0),
        .O(ap_enable_reg_pp0_iter0_i_1__1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter0_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter0_i_1__1_n_2),
        .Q(ap_enable_reg_pp0_iter0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h40CC400040004000)) 
    ap_enable_reg_pp0_iter1_i_1__0
       (.I0(ap_enable_reg_pp0_iter00),
        .I1(ap_rst_n),
        .I2(ap_enable_reg_pp0_iter1_reg_n_2),
        .I3(\ap_CS_fsm[3]_i_2__1_n_2 ),
        .I4(ap_enable_reg_pp0_iter0),
        .I5(tmp_21_fu_216_p2),
        .O(ap_enable_reg_pp0_iter1_i_1__0_n_2));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'h57)) 
    ap_enable_reg_pp0_iter1_i_2__0
       (.I0(j_reg_193_reg__0[10]),
        .I1(j_reg_193_reg__0[9]),
        .I2(j_reg_193_reg__0[8]),
        .O(tmp_21_fu_216_p2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter1_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter1_i_1__0_n_2),
        .Q(ap_enable_reg_pp0_iter1_reg_n_2),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \i_1_reg_232[0]_i_1 
       (.I0(\i_reg_182_reg_n_2_[0] ),
        .O(i_1_fu_210_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_1_reg_232[1]_i_1 
       (.I0(\i_reg_182_reg_n_2_[0] ),
        .I1(\i_reg_182_reg_n_2_[1] ),
        .O(i_1_fu_210_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \i_1_reg_232[2]_i_1 
       (.I0(\i_reg_182_reg_n_2_[2] ),
        .I1(\i_reg_182_reg_n_2_[1] ),
        .I2(\i_reg_182_reg_n_2_[0] ),
        .O(i_1_fu_210_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \i_1_reg_232[3]_i_1 
       (.I0(\i_reg_182_reg_n_2_[3] ),
        .I1(\i_reg_182_reg_n_2_[0] ),
        .I2(\i_reg_182_reg_n_2_[1] ),
        .I3(\i_reg_182_reg_n_2_[2] ),
        .O(i_1_fu_210_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \i_1_reg_232[4]_i_1 
       (.I0(\i_reg_182_reg_n_2_[4] ),
        .I1(\i_reg_182_reg_n_2_[2] ),
        .I2(\i_reg_182_reg_n_2_[1] ),
        .I3(\i_reg_182_reg_n_2_[0] ),
        .I4(\i_reg_182_reg_n_2_[3] ),
        .O(i_1_fu_210_p2[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \i_1_reg_232[5]_i_1 
       (.I0(\i_reg_182_reg_n_2_[5] ),
        .I1(\i_reg_182_reg_n_2_[3] ),
        .I2(\i_reg_182_reg_n_2_[0] ),
        .I3(\i_reg_182_reg_n_2_[1] ),
        .I4(\i_reg_182_reg_n_2_[2] ),
        .I5(\i_reg_182_reg_n_2_[4] ),
        .O(i_1_fu_210_p2[5]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_1_reg_232[6]_i_1 
       (.I0(\i_reg_182_reg_n_2_[6] ),
        .I1(\i_1_reg_232[9]_i_2_n_2 ),
        .O(i_1_fu_210_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \i_1_reg_232[7]_i_1 
       (.I0(\i_reg_182_reg_n_2_[7] ),
        .I1(\i_1_reg_232[9]_i_2_n_2 ),
        .I2(\i_reg_182_reg_n_2_[6] ),
        .O(i_1_fu_210_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \i_1_reg_232[8]_i_1 
       (.I0(\i_reg_182_reg_n_2_[8] ),
        .I1(\i_reg_182_reg_n_2_[6] ),
        .I2(\i_reg_182_reg_n_2_[7] ),
        .I3(\i_1_reg_232[9]_i_2_n_2 ),
        .O(i_1_fu_210_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \i_1_reg_232[9]_i_1 
       (.I0(\i_reg_182_reg_n_2_[9] ),
        .I1(\i_1_reg_232[9]_i_2_n_2 ),
        .I2(\i_reg_182_reg_n_2_[7] ),
        .I3(\i_reg_182_reg_n_2_[6] ),
        .I4(\i_reg_182_reg_n_2_[8] ),
        .O(i_1_fu_210_p2[9]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \i_1_reg_232[9]_i_2 
       (.I0(\i_reg_182_reg_n_2_[5] ),
        .I1(\i_reg_182_reg_n_2_[3] ),
        .I2(\i_reg_182_reg_n_2_[0] ),
        .I3(\i_reg_182_reg_n_2_[1] ),
        .I4(\i_reg_182_reg_n_2_[2] ),
        .I5(\i_reg_182_reg_n_2_[4] ),
        .O(\i_1_reg_232[9]_i_2_n_2 ));
  FDRE \i_1_reg_232_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_1_fu_210_p2[0]),
        .Q(i_1_reg_232[0]),
        .R(1'b0));
  FDRE \i_1_reg_232_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_1_fu_210_p2[1]),
        .Q(i_1_reg_232[1]),
        .R(1'b0));
  FDRE \i_1_reg_232_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_1_fu_210_p2[2]),
        .Q(i_1_reg_232[2]),
        .R(1'b0));
  FDRE \i_1_reg_232_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_1_fu_210_p2[3]),
        .Q(i_1_reg_232[3]),
        .R(1'b0));
  FDRE \i_1_reg_232_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_1_fu_210_p2[4]),
        .Q(i_1_reg_232[4]),
        .R(1'b0));
  FDRE \i_1_reg_232_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_1_fu_210_p2[5]),
        .Q(i_1_reg_232[5]),
        .R(1'b0));
  FDRE \i_1_reg_232_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_1_fu_210_p2[6]),
        .Q(i_1_reg_232[6]),
        .R(1'b0));
  FDRE \i_1_reg_232_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_1_fu_210_p2[7]),
        .Q(i_1_reg_232[7]),
        .R(1'b0));
  FDRE \i_1_reg_232_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_1_fu_210_p2[8]),
        .Q(i_1_reg_232[8]),
        .R(1'b0));
  FDRE \i_1_reg_232_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(i_1_fu_210_p2[9]),
        .Q(i_1_reg_232[9]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h0000A800)) 
    \i_reg_182[9]_i_1 
       (.I0(CvtColor_1_U0_ap_start),
        .I1(start_for_Mat2AXIvideo_U0_full_n),
        .I2(start_once_reg),
        .I3(\ap_CS_fsm_reg_n_2_[0] ),
        .I4(ap_CS_fsm_state5),
        .O(i_reg_182));
  FDRE \i_reg_182_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state5),
        .D(i_1_reg_232[0]),
        .Q(\i_reg_182_reg_n_2_[0] ),
        .R(i_reg_182));
  FDRE \i_reg_182_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state5),
        .D(i_1_reg_232[1]),
        .Q(\i_reg_182_reg_n_2_[1] ),
        .R(i_reg_182));
  FDRE \i_reg_182_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state5),
        .D(i_1_reg_232[2]),
        .Q(\i_reg_182_reg_n_2_[2] ),
        .R(i_reg_182));
  FDRE \i_reg_182_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state5),
        .D(i_1_reg_232[3]),
        .Q(\i_reg_182_reg_n_2_[3] ),
        .R(i_reg_182));
  FDRE \i_reg_182_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state5),
        .D(i_1_reg_232[4]),
        .Q(\i_reg_182_reg_n_2_[4] ),
        .R(i_reg_182));
  FDRE \i_reg_182_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state5),
        .D(i_1_reg_232[5]),
        .Q(\i_reg_182_reg_n_2_[5] ),
        .R(i_reg_182));
  FDRE \i_reg_182_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state5),
        .D(i_1_reg_232[6]),
        .Q(\i_reg_182_reg_n_2_[6] ),
        .R(i_reg_182));
  FDRE \i_reg_182_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state5),
        .D(i_1_reg_232[7]),
        .Q(\i_reg_182_reg_n_2_[7] ),
        .R(i_reg_182));
  FDRE \i_reg_182_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state5),
        .D(i_1_reg_232[8]),
        .Q(\i_reg_182_reg_n_2_[8] ),
        .R(i_reg_182));
  FDRE \i_reg_182_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state5),
        .D(i_1_reg_232[9]),
        .Q(\i_reg_182_reg_n_2_[9] ),
        .R(i_reg_182));
  LUT1 #(
    .INIT(2'h1)) 
    \j_reg_193[0]_i_1 
       (.I0(\j_reg_193_reg_n_2_[0] ),
        .O(j_1_fu_222_p2[0]));
  LUT2 #(
    .INIT(4'h2)) 
    \j_reg_193[10]_i_1 
       (.I0(ap_enable_reg_pp0_iter00),
        .I1(j_reg_1930),
        .O(j_reg_193));
  LUT6 #(
    .INIT(64'h0404044400000000)) 
    \j_reg_193[10]_i_2 
       (.I0(\ap_CS_fsm[3]_i_2__1_n_2 ),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(j_reg_193_reg__0[10]),
        .I3(j_reg_193_reg__0[9]),
        .I4(j_reg_193_reg__0[8]),
        .I5(ap_enable_reg_pp0_iter0),
        .O(j_reg_1930));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \j_reg_193[10]_i_3 
       (.I0(j_reg_193_reg__0[10]),
        .I1(j_reg_193_reg__0[8]),
        .I2(\j_reg_193_reg_n_2_[6] ),
        .I3(\j_reg_193[10]_i_4_n_2 ),
        .I4(\j_reg_193_reg_n_2_[7] ),
        .I5(j_reg_193_reg__0[9]),
        .O(j_1_fu_222_p2[10]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \j_reg_193[10]_i_4 
       (.I0(\j_reg_193_reg_n_2_[5] ),
        .I1(\j_reg_193_reg_n_2_[3] ),
        .I2(\j_reg_193_reg_n_2_[0] ),
        .I3(\j_reg_193_reg_n_2_[1] ),
        .I4(\j_reg_193_reg_n_2_[2] ),
        .I5(\j_reg_193_reg_n_2_[4] ),
        .O(\j_reg_193[10]_i_4_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \j_reg_193[1]_i_1 
       (.I0(\j_reg_193_reg_n_2_[0] ),
        .I1(\j_reg_193_reg_n_2_[1] ),
        .O(j_1_fu_222_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \j_reg_193[2]_i_1 
       (.I0(\j_reg_193_reg_n_2_[2] ),
        .I1(\j_reg_193_reg_n_2_[1] ),
        .I2(\j_reg_193_reg_n_2_[0] ),
        .O(j_1_fu_222_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \j_reg_193[3]_i_1 
       (.I0(\j_reg_193_reg_n_2_[3] ),
        .I1(\j_reg_193_reg_n_2_[0] ),
        .I2(\j_reg_193_reg_n_2_[1] ),
        .I3(\j_reg_193_reg_n_2_[2] ),
        .O(j_1_fu_222_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \j_reg_193[4]_i_1 
       (.I0(\j_reg_193_reg_n_2_[4] ),
        .I1(\j_reg_193_reg_n_2_[2] ),
        .I2(\j_reg_193_reg_n_2_[1] ),
        .I3(\j_reg_193_reg_n_2_[0] ),
        .I4(\j_reg_193_reg_n_2_[3] ),
        .O(j_1_fu_222_p2[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \j_reg_193[5]_i_1 
       (.I0(\j_reg_193_reg_n_2_[5] ),
        .I1(\j_reg_193_reg_n_2_[3] ),
        .I2(\j_reg_193_reg_n_2_[0] ),
        .I3(\j_reg_193_reg_n_2_[1] ),
        .I4(\j_reg_193_reg_n_2_[2] ),
        .I5(\j_reg_193_reg_n_2_[4] ),
        .O(j_1_fu_222_p2[5]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \j_reg_193[6]_i_1 
       (.I0(\j_reg_193_reg_n_2_[6] ),
        .I1(\j_reg_193[10]_i_4_n_2 ),
        .O(j_1_fu_222_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \j_reg_193[7]_i_1 
       (.I0(\j_reg_193_reg_n_2_[7] ),
        .I1(\j_reg_193[10]_i_4_n_2 ),
        .I2(\j_reg_193_reg_n_2_[6] ),
        .O(j_1_fu_222_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \j_reg_193[8]_i_1 
       (.I0(j_reg_193_reg__0[8]),
        .I1(\j_reg_193_reg_n_2_[6] ),
        .I2(\j_reg_193[10]_i_4_n_2 ),
        .I3(\j_reg_193_reg_n_2_[7] ),
        .O(j_1_fu_222_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \j_reg_193[9]_i_1 
       (.I0(j_reg_193_reg__0[9]),
        .I1(\j_reg_193_reg_n_2_[7] ),
        .I2(\j_reg_193[10]_i_4_n_2 ),
        .I3(\j_reg_193_reg_n_2_[6] ),
        .I4(j_reg_193_reg__0[8]),
        .O(j_1_fu_222_p2[9]));
  FDRE \j_reg_193_reg[0] 
       (.C(ap_clk),
        .CE(j_reg_1930),
        .D(j_1_fu_222_p2[0]),
        .Q(\j_reg_193_reg_n_2_[0] ),
        .R(j_reg_193));
  FDRE \j_reg_193_reg[10] 
       (.C(ap_clk),
        .CE(j_reg_1930),
        .D(j_1_fu_222_p2[10]),
        .Q(j_reg_193_reg__0[10]),
        .R(j_reg_193));
  FDRE \j_reg_193_reg[1] 
       (.C(ap_clk),
        .CE(j_reg_1930),
        .D(j_1_fu_222_p2[1]),
        .Q(\j_reg_193_reg_n_2_[1] ),
        .R(j_reg_193));
  FDRE \j_reg_193_reg[2] 
       (.C(ap_clk),
        .CE(j_reg_1930),
        .D(j_1_fu_222_p2[2]),
        .Q(\j_reg_193_reg_n_2_[2] ),
        .R(j_reg_193));
  FDRE \j_reg_193_reg[3] 
       (.C(ap_clk),
        .CE(j_reg_1930),
        .D(j_1_fu_222_p2[3]),
        .Q(\j_reg_193_reg_n_2_[3] ),
        .R(j_reg_193));
  FDRE \j_reg_193_reg[4] 
       (.C(ap_clk),
        .CE(j_reg_1930),
        .D(j_1_fu_222_p2[4]),
        .Q(\j_reg_193_reg_n_2_[4] ),
        .R(j_reg_193));
  FDRE \j_reg_193_reg[5] 
       (.C(ap_clk),
        .CE(j_reg_1930),
        .D(j_1_fu_222_p2[5]),
        .Q(\j_reg_193_reg_n_2_[5] ),
        .R(j_reg_193));
  FDRE \j_reg_193_reg[6] 
       (.C(ap_clk),
        .CE(j_reg_1930),
        .D(j_1_fu_222_p2[6]),
        .Q(\j_reg_193_reg_n_2_[6] ),
        .R(j_reg_193));
  FDRE \j_reg_193_reg[7] 
       (.C(ap_clk),
        .CE(j_reg_1930),
        .D(j_1_fu_222_p2[7]),
        .Q(\j_reg_193_reg_n_2_[7] ),
        .R(j_reg_193));
  FDRE \j_reg_193_reg[8] 
       (.C(ap_clk),
        .CE(j_reg_1930),
        .D(j_1_fu_222_p2[8]),
        .Q(j_reg_193_reg__0[8]),
        .R(j_reg_193));
  FDRE \j_reg_193_reg[9] 
       (.C(ap_clk),
        .CE(j_reg_1930),
        .D(j_1_fu_222_p2[9]),
        .Q(j_reg_193_reg__0[9]),
        .R(j_reg_193));
  LUT4 #(
    .INIT(16'h5444)) 
    start_once_reg_i_1__1
       (.I0(start_once_reg_reg_0),
        .I1(start_once_reg),
        .I2(start_for_Mat2AXIvideo_U0_full_n),
        .I3(CvtColor_1_U0_ap_start),
        .O(start_once_reg_i_1__1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    start_once_reg_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(start_once_reg_i_1__1_n_2),
        .Q(start_once_reg),
        .R(ap_rst_n_inv));
  LUT6 #(
    .INIT(64'hFFFF57FF00005700)) 
    \tmp_21_reg_237[0]_i_1 
       (.I0(j_reg_193_reg__0[10]),
        .I1(j_reg_193_reg__0[9]),
        .I2(j_reg_193_reg__0[8]),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(\ap_CS_fsm[3]_i_2__1_n_2 ),
        .I5(tmp_21_reg_237),
        .O(\tmp_21_reg_237[0]_i_1_n_2 ));
  FDRE \tmp_21_reg_237_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_21_reg_237[0]_i_1_n_2 ),
        .Q(tmp_21_reg_237),
        .R(1'b0));
endmodule

module system_edge_detect_0_1_Filter2D
   (D,
    E,
    \mOutPtr_reg[1] ,
    \mOutPtr_reg[1]_0 ,
    \mOutPtr_reg[0] ,
    \mOutPtr_reg[0]_0 ,
    shiftReg_ce,
    \ap_CS_fsm_reg[1]_0 ,
    \ap_CS_fsm_reg[0]_0 ,
    start_once_reg_reg,
    ap_reg_grp_Filter2D_fu_96_ap_start_reg,
    \SRL_SIG_reg[0][7] ,
    \SRL_SIG_reg[0][0] ,
    \SRL_SIG_reg[0][1] ,
    \SRL_SIG_reg[0][2] ,
    \SRL_SIG_reg[0][3] ,
    \SRL_SIG_reg[0][4] ,
    \SRL_SIG_reg[0][5] ,
    \SRL_SIG_reg[0][6] ,
    \SRL_SIG_reg[0][7]_0 ,
    ap_clk,
    ap_rst_n_inv,
    ap_rst_n,
    shiftReg_ce_0,
    Q,
    \mOutPtr_reg[1]_1 ,
    \mOutPtr_reg[1]_2 ,
    \mOutPtr_reg[1]_3 ,
    ap_reg_grp_Filter2D_fu_96_ap_start,
    shiftReg_ce_1,
    img1_data_stream_0_s_empty_n,
    img1_data_stream_2_s_empty_n,
    img1_data_stream_1_s_empty_n,
    img2_data_stream_0_s_full_n,
    img2_data_stream_1_s_full_n,
    img2_data_stream_2_s_full_n,
    start_once_reg_reg_0,
    start_for_CvtColor_1_U0_full_n,
    Sobel_U0_ap_start,
    \SRL_SIG_reg[0][7]_1 );
  output [0:0]D;
  output [0:0]E;
  output [0:0]\mOutPtr_reg[1] ;
  output [0:0]\mOutPtr_reg[1]_0 ;
  output [0:0]\mOutPtr_reg[0] ;
  output [0:0]\mOutPtr_reg[0]_0 ;
  output shiftReg_ce;
  output [1:0]\ap_CS_fsm_reg[1]_0 ;
  output \ap_CS_fsm_reg[0]_0 ;
  output start_once_reg_reg;
  output ap_reg_grp_Filter2D_fu_96_ap_start_reg;
  output \SRL_SIG_reg[0][7] ;
  output \SRL_SIG_reg[0][0] ;
  output \SRL_SIG_reg[0][1] ;
  output \SRL_SIG_reg[0][2] ;
  output \SRL_SIG_reg[0][3] ;
  output \SRL_SIG_reg[0][4] ;
  output \SRL_SIG_reg[0][5] ;
  output \SRL_SIG_reg[0][6] ;
  output \SRL_SIG_reg[0][7]_0 ;
  input ap_clk;
  input ap_rst_n_inv;
  input ap_rst_n;
  input shiftReg_ce_0;
  input [1:0]Q;
  input [1:0]\mOutPtr_reg[1]_1 ;
  input [1:0]\mOutPtr_reg[1]_2 ;
  input [1:0]\mOutPtr_reg[1]_3 ;
  input ap_reg_grp_Filter2D_fu_96_ap_start;
  input shiftReg_ce_1;
  input img1_data_stream_0_s_empty_n;
  input img1_data_stream_2_s_empty_n;
  input img1_data_stream_1_s_empty_n;
  input img2_data_stream_0_s_full_n;
  input img2_data_stream_1_s_full_n;
  input img2_data_stream_2_s_full_n;
  input start_once_reg_reg_0;
  input start_for_CvtColor_1_U0_full_n;
  input Sobel_U0_ap_start;
  input [7:0]\SRL_SIG_reg[0][7]_1 ;

  wire [0:0]D;
  wire [0:0]E;
  wire [11:11]ImagLoc_x_fu_964_p2;
  wire [10:1]ImagLoc_x_reg_2592;
  wire ImagLoc_x_reg_25920;
  wire \ImagLoc_x_reg_2592[10]_i_1_n_2 ;
  wire \ImagLoc_x_reg_2592[10]_i_2_n_2 ;
  wire \ImagLoc_x_reg_2592[1]_i_1_n_2 ;
  wire \ImagLoc_x_reg_2592[2]_i_1_n_2 ;
  wire \ImagLoc_x_reg_2592[3]_i_1_n_2 ;
  wire \ImagLoc_x_reg_2592[4]_i_1_n_2 ;
  wire \ImagLoc_x_reg_2592[5]_i_1_n_2 ;
  wire \ImagLoc_x_reg_2592[6]_i_1_n_2 ;
  wire \ImagLoc_x_reg_2592[7]_i_1_n_2 ;
  wire \ImagLoc_x_reg_2592[8]_i_1_n_2 ;
  wire \ImagLoc_x_reg_2592[9]_i_1_n_2 ;
  wire [1:0]Q;
  wire \SRL_SIG_reg[0][0] ;
  wire \SRL_SIG_reg[0][1] ;
  wire \SRL_SIG_reg[0][2] ;
  wire \SRL_SIG_reg[0][3] ;
  wire \SRL_SIG_reg[0][4] ;
  wire \SRL_SIG_reg[0][5] ;
  wire \SRL_SIG_reg[0][6] ;
  wire \SRL_SIG_reg[0][7] ;
  wire \SRL_SIG_reg[0][7]_0 ;
  wire [7:0]\SRL_SIG_reg[0][7]_1 ;
  wire Sobel_U0_ap_start;
  wire \ap_CS_fsm[3]_i_2__0_n_2 ;
  wire \ap_CS_fsm[3]_i_3_n_2 ;
  wire \ap_CS_fsm[5]_i_2__0_n_2 ;
  wire ap_CS_fsm_pp0_stage0;
  wire \ap_CS_fsm_reg[0]_0 ;
  wire [1:0]\ap_CS_fsm_reg[1]_0 ;
  wire \ap_CS_fsm_reg_n_2_[0] ;
  wire ap_CS_fsm_state11;
  wire ap_CS_fsm_state2;
  wire ap_CS_fsm_state3;
  wire ap_CS_fsm_state4;
  wire [5:0]ap_NS_fsm;
  wire ap_NS_fsm1;
  wire ap_block_pp0_stage0_subdone2_in;
  wire ap_clk;
  wire ap_enable_reg_pp0_iter0;
  wire ap_enable_reg_pp0_iter0_i_1__0_n_2;
  wire ap_enable_reg_pp0_iter1;
  wire ap_enable_reg_pp0_iter2;
  wire ap_enable_reg_pp0_iter3;
  wire ap_enable_reg_pp0_iter3_i_1_n_2;
  wire ap_enable_reg_pp0_iter4;
  wire ap_enable_reg_pp0_iter5_i_1__0_n_2;
  wire ap_enable_reg_pp0_iter5_reg_n_2;
  wire ap_reg_grp_Filter2D_fu_96_ap_start;
  wire ap_reg_grp_Filter2D_fu_96_ap_start_i_2_n_2;
  wire ap_reg_grp_Filter2D_fu_96_ap_start_reg;
  wire ap_reg_pp0_iter1_brmerge_reg_2627;
  wire ap_reg_pp0_iter1_brmerge_reg_26270;
  wire \ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg_n_2_[0] ;
  wire ap_reg_pp0_iter1_or_cond_i_i_reg_2607;
  wire ap_reg_pp0_iter1_or_cond_i_reg_2640;
  wire ap_reg_pp0_iter2_exitcond389_i_reg_2583;
  wire [10:0]ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697;
  wire ap_reg_pp0_iter2_or_cond_i_i_reg_2607;
  wire ap_reg_pp0_iter2_or_cond_i_reg_2640;
  wire [7:0]ap_reg_pp0_iter2_reg_588;
  wire ap_reg_pp0_iter3_or_cond_i_reg_2640;
  wire ap_reg_pp0_iter4_or_cond_i_reg_2640;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire brmerge_fu_1034_p2;
  wire brmerge_reg_2627;
  wire ce119_out;
  wire [7:0]col_buf_0_val_0_0_fu_1140_p3;
  wire [7:0]col_buf_0_val_0_0_reg_2708;
  wire col_buf_0_val_0_0_reg_27080;
  wire [7:0]col_buf_0_val_1_0_fu_1159_p3;
  wire [7:0]col_buf_0_val_1_0_reg_2721;
  wire [7:0]col_buf_0_val_2_0_fu_1178_p3;
  wire [7:0]col_buf_0_val_2_0_reg_2729;
  wire exitcond389_i_fu_936_p2;
  wire \exitcond389_i_reg_2583[0]_i_3_n_2 ;
  wire \exitcond389_i_reg_2583[0]_i_4_n_2 ;
  wire \exitcond389_i_reg_2583[0]_i_5_n_2 ;
  wire \exitcond389_i_reg_2583_reg_n_2_[0] ;
  wire [9:0]i_V_fu_631_p2;
  wire [9:0]i_V_reg_2495;
  wire \i_V_reg_2495[9]_i_2_n_2 ;
  wire \icmp_reg_2509[0]_i_1_n_2 ;
  wire \icmp_reg_2509[0]_i_2_n_2 ;
  wire \icmp_reg_2509[0]_i_3_n_2 ;
  wire \icmp_reg_2509_reg_n_2_[0] ;
  wire img1_data_stream_0_s_empty_n;
  wire img1_data_stream_1_s_empty_n;
  wire img1_data_stream_2_s_empty_n;
  wire img2_data_stream_0_s_full_n;
  wire img2_data_stream_1_s_full_n;
  wire img2_data_stream_2_s_full_n;
  wire isneg_1_reg_29320;
  wire [10:1]j_V_fu_942_p2;
  wire k_buf_0_val_3_U_n_10;
  wire k_buf_0_val_3_U_n_13;
  wire k_buf_0_val_3_addr_reg_26490;
  wire [7:0]k_buf_0_val_3_load_reg_2703;
  wire k_buf_0_val_3_load_reg_27030;
  wire [7:0]k_buf_0_val_3_q0;
  wire [7:0]k_buf_0_val_4_load_reg_2716;
  wire [7:0]k_buf_0_val_4_q0;
  wire k_buf_0_val_5_U_n_10;
  wire k_buf_0_val_5_U_n_11;
  wire k_buf_0_val_5_U_n_12;
  wire k_buf_0_val_5_U_n_13;
  wire k_buf_0_val_5_U_n_14;
  wire k_buf_0_val_5_U_n_15;
  wire k_buf_0_val_5_U_n_16;
  wire k_buf_0_val_5_U_n_17;
  wire k_buf_0_val_5_U_n_18;
  wire k_buf_0_val_5_U_n_19;
  wire [7:0]k_buf_0_val_5_q0;
  wire [10:2]k_buf_2_val_5_addr_reg_2697;
  wire [0:0]\mOutPtr_reg[0] ;
  wire [0:0]\mOutPtr_reg[0]_0 ;
  wire [0:0]\mOutPtr_reg[1] ;
  wire [0:0]\mOutPtr_reg[1]_0 ;
  wire [1:0]\mOutPtr_reg[1]_1 ;
  wire [1:0]\mOutPtr_reg[1]_2 ;
  wire [1:0]\mOutPtr_reg[1]_3 ;
  wire or_cond_i425_i_fu_709_p2;
  wire or_cond_i425_i_reg_2542;
  wire or_cond_i_fu_1039_p2;
  wire or_cond_i_i_fu_990_p2;
  wire or_cond_i_i_reg_2607;
  wire or_cond_i_reg_2640;
  wire \or_cond_i_reg_2640[0]_i_2_n_2 ;
  wire \or_cond_i_reg_2640[0]_i_3_n_2 ;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_1_n_2;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_2_n_2;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_3_n_2;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_4_n_2;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_2;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_3;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_4;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_5;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_6;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_7;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_8;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_9;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry__1_n_9;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry_i_1_n_2;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry_i_2_n_2;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry_i_3_n_2;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry_i_4_n_2;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry_i_5_n_2;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry_n_2;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry_n_3;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry_n_4;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry_n_5;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry_n_6;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry_n_7;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry_n_8;
  wire p_Val2_10_0_0_2_fu_1493_p2_carry_n_9;
  wire [8:8]p_Val2_10_0_0_2_reg_2817;
  wire p_Val2_10_0_0_2_reg_28170;
  wire [7:0]p_Val2_1_fu_1973_p2;
  wire p_Val2_1_fu_1973_p2__1_carry__0_i_1_n_2;
  wire p_Val2_1_fu_1973_p2__1_carry__0_i_2_n_2;
  wire p_Val2_1_fu_1973_p2__1_carry__0_i_3_n_2;
  wire p_Val2_1_fu_1973_p2__1_carry__0_i_4_n_2;
  wire p_Val2_1_fu_1973_p2__1_carry__0_i_5_n_2;
  wire p_Val2_1_fu_1973_p2__1_carry__0_n_4;
  wire p_Val2_1_fu_1973_p2__1_carry__0_n_5;
  wire p_Val2_1_fu_1973_p2__1_carry__0_n_7;
  wire p_Val2_1_fu_1973_p2__1_carry__0_n_8;
  wire p_Val2_1_fu_1973_p2__1_carry__0_n_9;
  wire p_Val2_1_fu_1973_p2__1_carry_i_1_n_2;
  wire p_Val2_1_fu_1973_p2__1_carry_i_2_n_2;
  wire p_Val2_1_fu_1973_p2__1_carry_i_3_n_2;
  wire p_Val2_1_fu_1973_p2__1_carry_i_4_n_2;
  wire p_Val2_1_fu_1973_p2__1_carry_i_5_n_2;
  wire p_Val2_1_fu_1973_p2__1_carry_i_6_n_2;
  wire p_Val2_1_fu_1973_p2__1_carry_i_7_n_2;
  wire p_Val2_1_fu_1973_p2__1_carry_n_2;
  wire p_Val2_1_fu_1973_p2__1_carry_n_3;
  wire p_Val2_1_fu_1973_p2__1_carry_n_4;
  wire p_Val2_1_fu_1973_p2__1_carry_n_5;
  wire p_Val2_1_fu_1973_p2__1_carry_n_6;
  wire p_Val2_1_fu_1973_p2__1_carry_n_7;
  wire p_Val2_1_fu_1973_p2__1_carry_n_8;
  wire p_Val2_1_fu_1973_p2__20_carry__0_i_1_n_2;
  wire p_Val2_1_fu_1973_p2__20_carry__0_i_2_n_2;
  wire p_Val2_1_fu_1973_p2__20_carry__0_i_3_n_2;
  wire p_Val2_1_fu_1973_p2__20_carry__0_i_4_n_2;
  wire p_Val2_1_fu_1973_p2__20_carry__0_i_5_n_2;
  wire p_Val2_1_fu_1973_p2__20_carry__0_i_6_n_2;
  wire p_Val2_1_fu_1973_p2__20_carry__0_i_7_n_2;
  wire p_Val2_1_fu_1973_p2__20_carry__0_n_3;
  wire p_Val2_1_fu_1973_p2__20_carry__0_n_4;
  wire p_Val2_1_fu_1973_p2__20_carry__0_n_5;
  wire p_Val2_1_fu_1973_p2__20_carry_i_1_n_2;
  wire p_Val2_1_fu_1973_p2__20_carry_i_2_n_2;
  wire p_Val2_1_fu_1973_p2__20_carry_i_3_n_2;
  wire p_Val2_1_fu_1973_p2__20_carry_i_4_n_2;
  wire p_Val2_1_fu_1973_p2__20_carry_i_5_n_2;
  wire p_Val2_1_fu_1973_p2__20_carry_i_6_n_2;
  wire p_Val2_1_fu_1973_p2__20_carry_i_7_n_2;
  wire p_Val2_1_fu_1973_p2__20_carry_n_2;
  wire p_Val2_1_fu_1973_p2__20_carry_n_3;
  wire p_Val2_1_fu_1973_p2__20_carry_n_4;
  wire p_Val2_1_fu_1973_p2__20_carry_n_5;
  wire [7:0]p_Val2_1_reg_2922;
  wire [10:8]p_Val2_2_fu_1946_p2;
  wire p_Val2_2_fu_1946_p2__27_carry__0_i_1_n_2;
  wire p_Val2_2_fu_1946_p2__27_carry__0_i_2_n_2;
  wire p_Val2_2_fu_1946_p2__27_carry__0_i_3_n_2;
  wire p_Val2_2_fu_1946_p2__27_carry__0_i_4_n_2;
  wire p_Val2_2_fu_1946_p2__27_carry__0_i_5_n_2;
  wire p_Val2_2_fu_1946_p2__27_carry__0_i_6_n_2;
  wire p_Val2_2_fu_1946_p2__27_carry__0_i_7_n_2;
  wire p_Val2_2_fu_1946_p2__27_carry__0_i_8_n_2;
  wire p_Val2_2_fu_1946_p2__27_carry__0_n_2;
  wire p_Val2_2_fu_1946_p2__27_carry__0_n_3;
  wire p_Val2_2_fu_1946_p2__27_carry__0_n_4;
  wire p_Val2_2_fu_1946_p2__27_carry__0_n_5;
  wire p_Val2_2_fu_1946_p2__27_carry__1_i_1_n_2;
  wire p_Val2_2_fu_1946_p2__27_carry__1_i_2_n_2;
  wire p_Val2_2_fu_1946_p2__27_carry__1_i_3_n_2;
  wire p_Val2_2_fu_1946_p2__27_carry__1_i_4_n_2;
  wire p_Val2_2_fu_1946_p2__27_carry__1_i_5_n_2;
  wire p_Val2_2_fu_1946_p2__27_carry__1_n_4;
  wire p_Val2_2_fu_1946_p2__27_carry__1_n_5;
  wire p_Val2_2_fu_1946_p2__27_carry_i_1_n_2;
  wire p_Val2_2_fu_1946_p2__27_carry_i_2_n_2;
  wire p_Val2_2_fu_1946_p2__27_carry_i_3_n_2;
  wire p_Val2_2_fu_1946_p2__27_carry_i_4_n_2;
  wire p_Val2_2_fu_1946_p2__27_carry_i_5_n_2;
  wire p_Val2_2_fu_1946_p2__27_carry_i_6_n_2;
  wire p_Val2_2_fu_1946_p2__27_carry_n_2;
  wire p_Val2_2_fu_1946_p2__27_carry_n_3;
  wire p_Val2_2_fu_1946_p2__27_carry_n_4;
  wire p_Val2_2_fu_1946_p2__27_carry_n_5;
  wire p_Val2_2_fu_1946_p2_carry__0_i_1_n_2;
  wire p_Val2_2_fu_1946_p2_carry__0_i_2_n_2;
  wire p_Val2_2_fu_1946_p2_carry__0_i_3_n_2;
  wire p_Val2_2_fu_1946_p2_carry__0_i_4_n_2;
  wire p_Val2_2_fu_1946_p2_carry__0_n_2;
  wire p_Val2_2_fu_1946_p2_carry__0_n_3;
  wire p_Val2_2_fu_1946_p2_carry__0_n_4;
  wire p_Val2_2_fu_1946_p2_carry__0_n_5;
  wire p_Val2_2_fu_1946_p2_carry__1_n_4;
  wire p_Val2_2_fu_1946_p2_carry_i_1_n_2;
  wire p_Val2_2_fu_1946_p2_carry_i_2_n_2;
  wire p_Val2_2_fu_1946_p2_carry_i_3_n_2;
  wire p_Val2_2_fu_1946_p2_carry_i_4_n_2;
  wire p_Val2_2_fu_1946_p2_carry_n_2;
  wire p_Val2_2_fu_1946_p2_carry_n_3;
  wire p_Val2_2_fu_1946_p2_carry_n_4;
  wire p_Val2_2_fu_1946_p2_carry_n_5;
  wire [8:1]p_assign_2_fu_1028_p2;
  wire [10:0]p_assign_2_reg_2622;
  wire \p_assign_2_reg_2622[0]_i_1_n_2 ;
  wire \p_assign_2_reg_2622[10]_i_1_n_2 ;
  wire \p_assign_2_reg_2622[10]_i_2_n_2 ;
  wire \p_assign_2_reg_2622[10]_i_3_n_2 ;
  wire \p_assign_2_reg_2622[10]_i_4_n_2 ;
  wire \p_assign_2_reg_2622[10]_i_5_n_2 ;
  wire \p_assign_2_reg_2622[10]_i_6_n_2 ;
  wire \p_assign_2_reg_2622[10]_i_7_n_2 ;
  wire \p_assign_2_reg_2622[10]_i_8_n_2 ;
  wire [7:7]p_assign_7_fu_723_p2;
  wire [9:1]p_assign_7_reg_2553;
  wire \p_assign_7_reg_2553[4]_i_1_n_2 ;
  wire \p_assign_7_reg_2553[5]_i_1_n_2 ;
  wire \p_assign_7_reg_2553[6]_i_1_n_2 ;
  wire \p_assign_7_reg_2553[8]_i_1_n_2 ;
  wire \p_assign_7_reg_2553[8]_i_2_n_2 ;
  wire \p_assign_7_reg_2553[9]_i_1_n_2 ;
  wire \p_assign_7_reg_2553[9]_i_2_n_2 ;
  wire \p_p2_i_i_cast_cast_reg_2612[10]_i_1_n_2 ;
  wire \p_p2_i_i_cast_cast_reg_2612[1]_i_1_n_2 ;
  wire \p_p2_i_i_cast_cast_reg_2612[2]_i_1_n_2 ;
  wire \p_p2_i_i_cast_cast_reg_2612[3]_i_1_n_2 ;
  wire \p_p2_i_i_cast_cast_reg_2612[4]_i_1_n_2 ;
  wire \p_p2_i_i_cast_cast_reg_2612[5]_i_1_n_2 ;
  wire \p_p2_i_i_cast_cast_reg_2612[5]_i_2_n_2 ;
  wire \p_p2_i_i_cast_cast_reg_2612[6]_i_1_n_2 ;
  wire \p_p2_i_i_cast_cast_reg_2612[7]_i_1_n_2 ;
  wire \p_p2_i_i_cast_cast_reg_2612[8]_i_1_n_2 ;
  wire \p_p2_i_i_cast_cast_reg_2612[9]_i_1_n_2 ;
  wire [10:1]p_p2_i_i_cast_cast_reg_2612_reg;
  wire [7:2]r_V_7_0_1_fu_1511_p2;
  wire [7:0]reg_588;
  wire [7:0]right_border_buf_0_1_fu_322;
  wire [7:0]right_border_buf_0_2_fu_330;
  wire [7:0]right_border_buf_0_3_fu_334;
  wire [7:0]right_border_buf_0_4_fu_342;
  wire [7:0]right_border_buf_0_5_fu_346;
  wire [7:0]right_border_buf_0_s_fu_318;
  wire [1:1]row_assign_9_0_1_t_fu_825_p2;
  wire [1:0]row_assign_9_0_1_t_reg_2558;
  wire [1:1]row_assign_9_0_2_t_fu_839_p2;
  wire [1:0]row_assign_9_0_2_t_reg_2565;
  wire shiftReg_ce;
  wire shiftReg_ce_0;
  wire shiftReg_ce_1;
  wire [7:0]src_kernel_win_0_va_1_fu_250;
  wire src_kernel_win_0_va_1_fu_2500;
  wire [7:0]src_kernel_win_0_va_2_fu_254;
  wire [7:0]src_kernel_win_0_va_3_fu_258;
  wire [7:0]src_kernel_win_0_va_4_fu_262;
  wire [7:0]src_kernel_win_0_va_5_fu_266;
  wire [7:0]src_kernel_win_0_va_6_fu_1442_p3;
  wire [7:0]src_kernel_win_0_va_6_reg_2805;
  wire [7:0]src_kernel_win_0_va_7_fu_1456_p3;
  wire [7:0]src_kernel_win_0_va_7_reg_2811;
  wire [7:0]src_kernel_win_0_va_fu_246;
  wire start_for_CvtColor_1_U0_full_n;
  wire start_once_reg_reg;
  wire start_once_reg_reg_0;
  wire t_V_2_reg_577;
  wire t_V_2_reg_5770;
  wire \t_V_2_reg_577[10]_i_4_n_2 ;
  wire [10:1]t_V_2_reg_577_reg__0;
  wire [0:0]t_V_2_reg_577_reg__0__0;
  wire [9:0]t_V_reg_566;
  wire [10:0]tmp57_fu_1547_p2;
  wire tmp57_fu_1547_p2_carry__0_i_1_n_2;
  wire tmp57_fu_1547_p2_carry__0_i_2_n_2;
  wire tmp57_fu_1547_p2_carry__0_i_3_n_2;
  wire tmp57_fu_1547_p2_carry__0_i_4_n_2;
  wire tmp57_fu_1547_p2_carry__0_i_5_n_2;
  wire tmp57_fu_1547_p2_carry__0_n_2;
  wire tmp57_fu_1547_p2_carry__0_n_3;
  wire tmp57_fu_1547_p2_carry__0_n_4;
  wire tmp57_fu_1547_p2_carry__0_n_5;
  wire tmp57_fu_1547_p2_carry__1_i_1_n_2;
  wire tmp57_fu_1547_p2_carry__1_i_2_n_2;
  wire tmp57_fu_1547_p2_carry__1_i_3_n_2;
  wire tmp57_fu_1547_p2_carry__1_i_4_n_2;
  wire tmp57_fu_1547_p2_carry__1_n_4;
  wire tmp57_fu_1547_p2_carry__1_n_5;
  wire tmp57_fu_1547_p2_carry_i_1_n_2;
  wire tmp57_fu_1547_p2_carry_i_2_n_2;
  wire tmp57_fu_1547_p2_carry_i_3_n_2;
  wire tmp57_fu_1547_p2_carry_i_4_n_2;
  wire tmp57_fu_1547_p2_carry_n_2;
  wire tmp57_fu_1547_p2_carry_n_3;
  wire tmp57_fu_1547_p2_carry_n_4;
  wire tmp57_fu_1547_p2_carry_n_5;
  wire [10:0]tmp57_reg_2837;
  wire [8:0]tmp69_cast_fu_1936_p1;
  wire \tmp_116_0_1_reg_2518[0]_i_1_n_2 ;
  wire \tmp_116_0_1_reg_2518_reg_n_2_[0] ;
  wire [7:0]tmp_160_0_0_2_cast_fu_1489_p1;
  wire [7:1]tmp_160_0_2_cast_fu_1539_p1;
  wire tmp_1_fu_637_p2;
  wire tmp_1_reg_2500;
  wire \tmp_2_reg_2514[0]_i_1_n_2 ;
  wire \tmp_2_reg_2514_reg_n_2_[0] ;
  wire tmp_31_fu_984_p2;
  wire tmp_31_fu_984_p2_carry_i_1_n_2;
  wire tmp_31_fu_984_p2_carry_i_2_n_2;
  wire tmp_31_fu_984_p2_carry_i_3_n_2;
  wire tmp_31_fu_984_p2_carry_i_4_n_2;
  wire tmp_31_fu_984_p2_carry_n_5;
  wire tmp_31_reg_2602;
  wire [1:1]tmp_32_fu_896_p2;
  wire [1:0]tmp_32_reg_2572;
  wire \tmp_32_reg_2572[0]_i_1_n_2 ;
  wire \tmp_32_reg_2572[1]_i_1_n_2 ;
  wire \tmp_32_reg_2572[1]_i_3_n_2 ;
  wire \tmp_32_reg_2572[1]_i_4_n_2 ;
  wire \tmp_32_reg_2572[1]_i_5_n_2 ;
  wire tmp_33_fu_1022_p2;
  wire tmp_33_fu_1022_p2_carry_i_1_n_2;
  wire tmp_33_fu_1022_p2_carry_i_2_n_2;
  wire tmp_33_fu_1022_p2_carry_i_3_n_2;
  wire tmp_33_fu_1022_p2_carry_n_5;
  wire tmp_33_reg_2617;
  wire tmp_3_fu_677_p2;
  wire tmp_3_reg_2522;
  wire \tmp_3_reg_2522[0]_i_2_n_2 ;
  wire \tmp_3_reg_2522[0]_i_3_n_2 ;
  wire tmp_47_reg_2597;
  wire [1:0]tmp_49_reg_2644;
  wire [1:1]tmp_4_reg_2535;
  wire \tmp_4_reg_2535[10]_i_1_n_2 ;
  wire \tmp_4_reg_2535[1]_i_1_n_2 ;
  wire \tmp_4_reg_2535[4]_i_1_n_2 ;
  wire \tmp_4_reg_2535[5]_i_1_n_2 ;
  wire \tmp_4_reg_2535[6]_i_1_n_2 ;
  wire \tmp_4_reg_2535[6]_i_2_n_2 ;
  wire \tmp_4_reg_2535[7]_i_1_n_2 ;
  wire \tmp_4_reg_2535[8]_i_1_n_2 ;
  wire \tmp_4_reg_2535[8]_i_2_n_2 ;
  wire \tmp_4_reg_2535[9]_i_1_n_2 ;
  wire \tmp_4_reg_2535_reg_n_2_[10] ;
  wire \tmp_4_reg_2535_reg_n_2_[4] ;
  wire \tmp_4_reg_2535_reg_n_2_[5] ;
  wire \tmp_4_reg_2535_reg_n_2_[6] ;
  wire \tmp_4_reg_2535_reg_n_2_[7] ;
  wire \tmp_4_reg_2535_reg_n_2_[8] ;
  wire \tmp_4_reg_2535_reg_n_2_[9] ;
  wire [7:0]tmp_53_reg_2822;
  wire [7:1]tmp_54_reg_2827;
  wire \tmp_54_reg_2827[7]_i_2_n_2 ;
  wire [7:0]tmp_56_reg_2832;
  wire \tmp_56_reg_2832[7]_i_2_n_2 ;
  wire [2:0]tmp_57_reg_2927;
  wire [1:0]tmp_5_reg_555;
  wire \tmp_5_reg_555[0]_i_1_n_2 ;
  wire \tmp_5_reg_555[1]_i_1_n_2 ;
  wire tmp_72_not_fu_643_p2;
  wire tmp_72_not_reg_2504;
  wire [3:0]NLW_p_Val2_10_0_0_2_fu_1493_p2_carry__1_CO_UNCONNECTED;
  wire [3:1]NLW_p_Val2_10_0_0_2_fu_1493_p2_carry__1_O_UNCONNECTED;
  wire [0:0]NLW_p_Val2_1_fu_1973_p2__1_carry_O_UNCONNECTED;
  wire [3:2]NLW_p_Val2_1_fu_1973_p2__1_carry__0_CO_UNCONNECTED;
  wire [3:3]NLW_p_Val2_1_fu_1973_p2__1_carry__0_O_UNCONNECTED;
  wire [3:3]NLW_p_Val2_1_fu_1973_p2__20_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_p_Val2_2_fu_1946_p2__27_carry_O_UNCONNECTED;
  wire [3:0]NLW_p_Val2_2_fu_1946_p2__27_carry__0_O_UNCONNECTED;
  wire [3:2]NLW_p_Val2_2_fu_1946_p2__27_carry__1_CO_UNCONNECTED;
  wire [3:3]NLW_p_Val2_2_fu_1946_p2__27_carry__1_O_UNCONNECTED;
  wire [3:0]NLW_p_Val2_2_fu_1946_p2_carry__1_CO_UNCONNECTED;
  wire [3:1]NLW_p_Val2_2_fu_1946_p2_carry__1_O_UNCONNECTED;
  wire [3:2]NLW_tmp57_fu_1547_p2_carry__1_CO_UNCONNECTED;
  wire [3:3]NLW_tmp57_fu_1547_p2_carry__1_O_UNCONNECTED;
  wire [3:2]NLW_tmp_31_fu_984_p2_carry_CO_UNCONNECTED;
  wire [3:0]NLW_tmp_31_fu_984_p2_carry_O_UNCONNECTED;
  wire [3:2]NLW_tmp_33_fu_1022_p2_carry_CO_UNCONNECTED;
  wire [3:0]NLW_tmp_33_fu_1022_p2_carry_O_UNCONNECTED;

  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA9)) 
    \ImagLoc_x_reg_2592[10]_i_1 
       (.I0(t_V_2_reg_577_reg__0[10]),
        .I1(t_V_2_reg_577_reg__0[8]),
        .I2(t_V_2_reg_577_reg__0[9]),
        .I3(t_V_2_reg_577_reg__0[7]),
        .I4(t_V_2_reg_577_reg__0[6]),
        .I5(\ImagLoc_x_reg_2592[10]_i_2_n_2 ),
        .O(\ImagLoc_x_reg_2592[10]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \ImagLoc_x_reg_2592[10]_i_2 
       (.I0(t_V_2_reg_577_reg__0[5]),
        .I1(t_V_2_reg_577_reg__0[2]),
        .I2(t_V_2_reg_577_reg__0[1]),
        .I3(t_V_2_reg_577_reg__0__0),
        .I4(t_V_2_reg_577_reg__0[3]),
        .I5(t_V_2_reg_577_reg__0[4]),
        .O(\ImagLoc_x_reg_2592[10]_i_2_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair156" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \ImagLoc_x_reg_2592[1]_i_1 
       (.I0(t_V_2_reg_577_reg__0[1]),
        .I1(t_V_2_reg_577_reg__0__0),
        .O(\ImagLoc_x_reg_2592[1]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair140" *) 
  LUT3 #(
    .INIT(8'hE1)) 
    \ImagLoc_x_reg_2592[2]_i_1 
       (.I0(t_V_2_reg_577_reg__0__0),
        .I1(t_V_2_reg_577_reg__0[1]),
        .I2(t_V_2_reg_577_reg__0[2]),
        .O(\ImagLoc_x_reg_2592[2]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair146" *) 
  LUT4 #(
    .INIT(16'hFE01)) 
    \ImagLoc_x_reg_2592[3]_i_1 
       (.I0(t_V_2_reg_577_reg__0[2]),
        .I1(t_V_2_reg_577_reg__0[1]),
        .I2(t_V_2_reg_577_reg__0__0),
        .I3(t_V_2_reg_577_reg__0[3]),
        .O(\ImagLoc_x_reg_2592[3]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair129" *) 
  LUT5 #(
    .INIT(32'hFFFE0001)) 
    \ImagLoc_x_reg_2592[4]_i_1 
       (.I0(t_V_2_reg_577_reg__0[3]),
        .I1(t_V_2_reg_577_reg__0__0),
        .I2(t_V_2_reg_577_reg__0[1]),
        .I3(t_V_2_reg_577_reg__0[2]),
        .I4(t_V_2_reg_577_reg__0[4]),
        .O(\ImagLoc_x_reg_2592[4]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000001)) 
    \ImagLoc_x_reg_2592[5]_i_1 
       (.I0(t_V_2_reg_577_reg__0[4]),
        .I1(t_V_2_reg_577_reg__0[3]),
        .I2(t_V_2_reg_577_reg__0__0),
        .I3(t_V_2_reg_577_reg__0[1]),
        .I4(t_V_2_reg_577_reg__0[2]),
        .I5(t_V_2_reg_577_reg__0[5]),
        .O(\ImagLoc_x_reg_2592[5]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair158" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \ImagLoc_x_reg_2592[6]_i_1 
       (.I0(\ImagLoc_x_reg_2592[10]_i_2_n_2 ),
        .I1(t_V_2_reg_577_reg__0[6]),
        .O(\ImagLoc_x_reg_2592[6]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair154" *) 
  LUT3 #(
    .INIT(8'hE1)) 
    \ImagLoc_x_reg_2592[7]_i_1 
       (.I0(t_V_2_reg_577_reg__0[6]),
        .I1(\ImagLoc_x_reg_2592[10]_i_2_n_2 ),
        .I2(t_V_2_reg_577_reg__0[7]),
        .O(\ImagLoc_x_reg_2592[7]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair118" *) 
  LUT4 #(
    .INIT(16'hAAA9)) 
    \ImagLoc_x_reg_2592[8]_i_1 
       (.I0(t_V_2_reg_577_reg__0[8]),
        .I1(\ImagLoc_x_reg_2592[10]_i_2_n_2 ),
        .I2(t_V_2_reg_577_reg__0[6]),
        .I3(t_V_2_reg_577_reg__0[7]),
        .O(\ImagLoc_x_reg_2592[8]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair118" *) 
  LUT5 #(
    .INIT(32'hAAAAAAA9)) 
    \ImagLoc_x_reg_2592[9]_i_1 
       (.I0(t_V_2_reg_577_reg__0[9]),
        .I1(t_V_2_reg_577_reg__0[8]),
        .I2(t_V_2_reg_577_reg__0[7]),
        .I3(t_V_2_reg_577_reg__0[6]),
        .I4(\ImagLoc_x_reg_2592[10]_i_2_n_2 ),
        .O(\ImagLoc_x_reg_2592[9]_i_1_n_2 ));
  FDRE \ImagLoc_x_reg_2592_reg[10] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\ImagLoc_x_reg_2592[10]_i_1_n_2 ),
        .Q(ImagLoc_x_reg_2592[10]),
        .R(1'b0));
  FDRE \ImagLoc_x_reg_2592_reg[1] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\ImagLoc_x_reg_2592[1]_i_1_n_2 ),
        .Q(ImagLoc_x_reg_2592[1]),
        .R(1'b0));
  FDRE \ImagLoc_x_reg_2592_reg[2] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\ImagLoc_x_reg_2592[2]_i_1_n_2 ),
        .Q(ImagLoc_x_reg_2592[2]),
        .R(1'b0));
  FDRE \ImagLoc_x_reg_2592_reg[3] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\ImagLoc_x_reg_2592[3]_i_1_n_2 ),
        .Q(ImagLoc_x_reg_2592[3]),
        .R(1'b0));
  FDRE \ImagLoc_x_reg_2592_reg[4] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\ImagLoc_x_reg_2592[4]_i_1_n_2 ),
        .Q(ImagLoc_x_reg_2592[4]),
        .R(1'b0));
  FDRE \ImagLoc_x_reg_2592_reg[5] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\ImagLoc_x_reg_2592[5]_i_1_n_2 ),
        .Q(ImagLoc_x_reg_2592[5]),
        .R(1'b0));
  FDRE \ImagLoc_x_reg_2592_reg[6] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\ImagLoc_x_reg_2592[6]_i_1_n_2 ),
        .Q(ImagLoc_x_reg_2592[6]),
        .R(1'b0));
  FDRE \ImagLoc_x_reg_2592_reg[7] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\ImagLoc_x_reg_2592[7]_i_1_n_2 ),
        .Q(ImagLoc_x_reg_2592[7]),
        .R(1'b0));
  FDRE \ImagLoc_x_reg_2592_reg[8] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\ImagLoc_x_reg_2592[8]_i_1_n_2 ),
        .Q(ImagLoc_x_reg_2592[8]),
        .R(1'b0));
  FDRE \ImagLoc_x_reg_2592_reg[9] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\ImagLoc_x_reg_2592[9]_i_1_n_2 ),
        .Q(ImagLoc_x_reg_2592[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair141" *) 
  LUT4 #(
    .INIT(16'h0002)) 
    \SRL_SIG[0][0]_i_1__3 
       (.I0(p_Val2_1_reg_2922[0]),
        .I1(tmp_57_reg_2927[0]),
        .I2(tmp_57_reg_2927[1]),
        .I3(tmp_57_reg_2927[2]),
        .O(\SRL_SIG_reg[0][0] ));
  (* SOFT_HLUTNM = "soft_lutpair142" *) 
  LUT4 #(
    .INIT(16'h0002)) 
    \SRL_SIG[0][1]_i_1__3 
       (.I0(p_Val2_1_reg_2922[1]),
        .I1(tmp_57_reg_2927[0]),
        .I2(tmp_57_reg_2927[1]),
        .I3(tmp_57_reg_2927[2]),
        .O(\SRL_SIG_reg[0][1] ));
  (* SOFT_HLUTNM = "soft_lutpair143" *) 
  LUT4 #(
    .INIT(16'h0002)) 
    \SRL_SIG[0][2]_i_1__3 
       (.I0(p_Val2_1_reg_2922[2]),
        .I1(tmp_57_reg_2927[0]),
        .I2(tmp_57_reg_2927[1]),
        .I3(tmp_57_reg_2927[2]),
        .O(\SRL_SIG_reg[0][2] ));
  (* SOFT_HLUTNM = "soft_lutpair144" *) 
  LUT4 #(
    .INIT(16'h0002)) 
    \SRL_SIG[0][3]_i_1__3 
       (.I0(p_Val2_1_reg_2922[3]),
        .I1(tmp_57_reg_2927[0]),
        .I2(tmp_57_reg_2927[1]),
        .I3(tmp_57_reg_2927[2]),
        .O(\SRL_SIG_reg[0][3] ));
  (* SOFT_HLUTNM = "soft_lutpair142" *) 
  LUT4 #(
    .INIT(16'h0002)) 
    \SRL_SIG[0][4]_i_1__3 
       (.I0(p_Val2_1_reg_2922[4]),
        .I1(tmp_57_reg_2927[0]),
        .I2(tmp_57_reg_2927[1]),
        .I3(tmp_57_reg_2927[2]),
        .O(\SRL_SIG_reg[0][4] ));
  (* SOFT_HLUTNM = "soft_lutpair144" *) 
  LUT4 #(
    .INIT(16'h0002)) 
    \SRL_SIG[0][5]_i_1__3 
       (.I0(p_Val2_1_reg_2922[5]),
        .I1(tmp_57_reg_2927[0]),
        .I2(tmp_57_reg_2927[1]),
        .I3(tmp_57_reg_2927[2]),
        .O(\SRL_SIG_reg[0][5] ));
  LUT4 #(
    .INIT(16'h0002)) 
    \SRL_SIG[0][6]_i_1__3 
       (.I0(p_Val2_1_reg_2922[6]),
        .I1(tmp_57_reg_2927[0]),
        .I2(tmp_57_reg_2927[1]),
        .I3(tmp_57_reg_2927[2]),
        .O(\SRL_SIG_reg[0][6] ));
  (* SOFT_HLUTNM = "soft_lutpair141" *) 
  LUT4 #(
    .INIT(16'h2220)) 
    \SRL_SIG[0][7]_i_1__4 
       (.I0(shiftReg_ce),
        .I1(tmp_57_reg_2927[2]),
        .I2(tmp_57_reg_2927[1]),
        .I3(tmp_57_reg_2927[0]),
        .O(\SRL_SIG_reg[0][7] ));
  (* SOFT_HLUTNM = "soft_lutpair130" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \SRL_SIG[0][7]_i_2__0 
       (.I0(ap_block_pp0_stage0_subdone2_in),
        .I1(ap_enable_reg_pp0_iter5_reg_n_2),
        .I2(ap_reg_pp0_iter4_or_cond_i_reg_2640),
        .I3(Q[1]),
        .O(shiftReg_ce));
  (* SOFT_HLUTNM = "soft_lutpair143" *) 
  LUT4 #(
    .INIT(16'h0002)) 
    \SRL_SIG[0][7]_i_3 
       (.I0(p_Val2_1_reg_2922[7]),
        .I1(tmp_57_reg_2927[0]),
        .I2(tmp_57_reg_2927[1]),
        .I3(tmp_57_reg_2927[2]),
        .O(\SRL_SIG_reg[0][7]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair117" *) 
  LUT5 #(
    .INIT(32'h5757FF00)) 
    \ap_CS_fsm[0]_i_1__0 
       (.I0(Sobel_U0_ap_start),
        .I1(start_for_CvtColor_1_U0_full_n),
        .I2(start_once_reg_reg_0),
        .I3(\ap_CS_fsm_reg[0]_0 ),
        .I4(Q[0]),
        .O(\ap_CS_fsm_reg[1]_0 [0]));
  (* SOFT_HLUTNM = "soft_lutpair119" *) 
  LUT4 #(
    .INIT(16'h4F44)) 
    \ap_CS_fsm[0]_i_1__3 
       (.I0(\ap_CS_fsm[3]_i_2__0_n_2 ),
        .I1(ap_CS_fsm_state3),
        .I2(ap_reg_grp_Filter2D_fu_96_ap_start),
        .I3(\ap_CS_fsm_reg_n_2_[0] ),
        .O(ap_NS_fsm[0]));
  LUT5 #(
    .INIT(32'hFF8F8888)) 
    \ap_CS_fsm[1]_i_1__1 
       (.I0(ap_reg_grp_Filter2D_fu_96_ap_start),
        .I1(\ap_CS_fsm_reg_n_2_[0] ),
        .I2(tmp_5_reg_555[1]),
        .I3(tmp_5_reg_555[0]),
        .I4(ap_CS_fsm_state2),
        .O(ap_NS_fsm[1]));
  (* SOFT_HLUTNM = "soft_lutpair117" *) 
  LUT5 #(
    .INIT(32'hBBB11111)) 
    \ap_CS_fsm[1]_i_1__2 
       (.I0(Q[0]),
        .I1(\ap_CS_fsm_reg[0]_0 ),
        .I2(start_once_reg_reg_0),
        .I3(start_for_CvtColor_1_U0_full_n),
        .I4(Sobel_U0_ap_start),
        .O(\ap_CS_fsm_reg[1]_0 [1]));
  (* SOFT_HLUTNM = "soft_lutpair119" *) 
  LUT5 #(
    .INIT(32'h0808AA08)) 
    \ap_CS_fsm[1]_i_2 
       (.I0(Q[1]),
        .I1(\ap_CS_fsm_reg_n_2_[0] ),
        .I2(ap_reg_grp_Filter2D_fu_96_ap_start),
        .I3(ap_CS_fsm_state3),
        .I4(\ap_CS_fsm[3]_i_2__0_n_2 ),
        .O(\ap_CS_fsm_reg[0]_0 ));
  LUT4 #(
    .INIT(16'hAEAA)) 
    \ap_CS_fsm[2]_i_1__1 
       (.I0(ap_CS_fsm_state11),
        .I1(tmp_5_reg_555[1]),
        .I2(tmp_5_reg_555[0]),
        .I3(ap_CS_fsm_state2),
        .O(ap_NS_fsm[2]));
  LUT2 #(
    .INIT(4'h8)) 
    \ap_CS_fsm[3]_i_1__0 
       (.I0(ap_CS_fsm_state3),
        .I1(\ap_CS_fsm[3]_i_2__0_n_2 ),
        .O(ap_NS_fsm[3]));
  LUT6 #(
    .INIT(64'hFFFFBFFFFFFFFFFF)) 
    \ap_CS_fsm[3]_i_2__0 
       (.I0(\ap_CS_fsm[3]_i_3_n_2 ),
        .I1(t_V_reg_566[9]),
        .I2(t_V_reg_566[4]),
        .I3(t_V_reg_566[1]),
        .I4(t_V_reg_566[8]),
        .I5(\tmp_4_reg_2535[6]_i_2_n_2 ),
        .O(\ap_CS_fsm[3]_i_2__0_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair138" *) 
  LUT4 #(
    .INIT(16'hFFF7)) 
    \ap_CS_fsm[3]_i_3 
       (.I0(t_V_reg_566[7]),
        .I1(t_V_reg_566[6]),
        .I2(t_V_reg_566[0]),
        .I3(t_V_reg_566[5]),
        .O(\ap_CS_fsm[3]_i_3_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair150" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    \ap_CS_fsm[4]_i_1__0 
       (.I0(ap_CS_fsm_state4),
        .I1(\ap_CS_fsm[5]_i_2__0_n_2 ),
        .I2(ap_CS_fsm_pp0_stage0),
        .O(ap_NS_fsm[4]));
  (* SOFT_HLUTNM = "soft_lutpair150" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \ap_CS_fsm[5]_i_1__0 
       (.I0(ap_CS_fsm_pp0_stage0),
        .I1(\ap_CS_fsm[5]_i_2__0_n_2 ),
        .O(ap_NS_fsm[5]));
  LUT6 #(
    .INIT(64'hFB00FBFBFFFFFFFF)) 
    \ap_CS_fsm[5]_i_2__0 
       (.I0(ap_enable_reg_pp0_iter1),
        .I1(ap_enable_reg_pp0_iter2),
        .I2(ap_enable_reg_pp0_iter3),
        .I3(ap_enable_reg_pp0_iter4),
        .I4(ap_enable_reg_pp0_iter5_reg_n_2),
        .I5(ap_block_pp0_stage0_subdone2_in),
        .O(\ap_CS_fsm[5]_i_2__0_n_2 ));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[0]),
        .Q(\ap_CS_fsm_reg_n_2_[0] ),
        .S(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ap_CS_fsm_state2),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[2]),
        .Q(ap_CS_fsm_state3),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[3]),
        .Q(ap_CS_fsm_state4),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[4] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[4]),
        .Q(ap_CS_fsm_pp0_stage0),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[5] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[5]),
        .Q(ap_CS_fsm_state11),
        .R(ap_rst_n_inv));
  LUT6 #(
    .INIT(64'h7F007F007F000000)) 
    ap_enable_reg_pp0_iter0_i_1__0
       (.I0(exitcond389_i_fu_936_p2),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(ap_block_pp0_stage0_subdone2_in),
        .I3(ap_rst_n),
        .I4(ap_CS_fsm_state4),
        .I5(ap_enable_reg_pp0_iter0),
        .O(ap_enable_reg_pp0_iter0_i_1__0_n_2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter0_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter0_i_1__0_n_2),
        .Q(ap_enable_reg_pp0_iter0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter1_reg
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(ap_enable_reg_pp0_iter0),
        .Q(ap_enable_reg_pp0_iter1),
        .R(ap_rst_n_inv));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter2_reg
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(ap_enable_reg_pp0_iter1),
        .Q(ap_enable_reg_pp0_iter2),
        .R(ap_rst_n_inv));
  LUT2 #(
    .INIT(4'h8)) 
    ap_enable_reg_pp0_iter3_i_1
       (.I0(ap_enable_reg_pp0_iter2),
        .I1(ap_enable_reg_pp0_iter1),
        .O(ap_enable_reg_pp0_iter3_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter3_reg
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(ap_enable_reg_pp0_iter3_i_1_n_2),
        .Q(ap_enable_reg_pp0_iter3),
        .R(ap_rst_n_inv));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter4_reg
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(ap_enable_reg_pp0_iter3),
        .Q(ap_enable_reg_pp0_iter4),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hC044C000)) 
    ap_enable_reg_pp0_iter5_i_1__0
       (.I0(ap_CS_fsm_state4),
        .I1(ap_rst_n),
        .I2(ap_enable_reg_pp0_iter4),
        .I3(ap_block_pp0_stage0_subdone2_in),
        .I4(ap_enable_reg_pp0_iter5_reg_n_2),
        .O(ap_enable_reg_pp0_iter5_i_1__0_n_2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter5_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter5_i_1__0_n_2),
        .Q(ap_enable_reg_pp0_iter5_reg_n_2),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hD5D5D555C0C0C000)) 
    ap_reg_grp_Filter2D_fu_96_ap_start_i_1
       (.I0(ap_reg_grp_Filter2D_fu_96_ap_start_i_2_n_2),
        .I1(Q[0]),
        .I2(Sobel_U0_ap_start),
        .I3(start_for_CvtColor_1_U0_full_n),
        .I4(start_once_reg_reg_0),
        .I5(ap_reg_grp_Filter2D_fu_96_ap_start),
        .O(ap_reg_grp_Filter2D_fu_96_ap_start_reg));
  (* SOFT_HLUTNM = "soft_lutpair136" *) 
  LUT2 #(
    .INIT(4'h2)) 
    ap_reg_grp_Filter2D_fu_96_ap_start_i_2
       (.I0(ap_CS_fsm_state3),
        .I1(\ap_CS_fsm[3]_i_2__0_n_2 ),
        .O(ap_reg_grp_Filter2D_fu_96_ap_start_i_2_n_2));
  FDRE \ap_reg_pp0_iter1_brmerge_reg_2627_reg[0] 
       (.C(ap_clk),
        .CE(ap_reg_pp0_iter1_brmerge_reg_26270),
        .D(brmerge_reg_2627),
        .Q(ap_reg_pp0_iter1_brmerge_reg_2627),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg[0] 
       (.C(ap_clk),
        .CE(ap_reg_pp0_iter1_brmerge_reg_26270),
        .D(\exitcond389_i_reg_2583_reg_n_2_[0] ),
        .Q(\ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg_n_2_[0] ),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter1_or_cond_i_i_reg_2607_reg[0] 
       (.C(ap_clk),
        .CE(ap_reg_pp0_iter1_brmerge_reg_26270),
        .D(or_cond_i_i_reg_2607),
        .Q(ap_reg_pp0_iter1_or_cond_i_i_reg_2607),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter1_or_cond_i_reg_2640_reg[0] 
       (.C(ap_clk),
        .CE(ap_reg_pp0_iter1_brmerge_reg_26270),
        .D(or_cond_i_reg_2640),
        .Q(ap_reg_pp0_iter1_or_cond_i_reg_2640),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_exitcond389_i_reg_2583_reg[0] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(\ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg_n_2_[0] ),
        .Q(ap_reg_pp0_iter2_exitcond389_i_reg_2583),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[0] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(tmp_49_reg_2644[0]),
        .Q(ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697[0]),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[10] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(k_buf_2_val_5_addr_reg_2697[10]),
        .Q(ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697[10]),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[1] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(tmp_49_reg_2644[1]),
        .Q(ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697[1]),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[2] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(k_buf_2_val_5_addr_reg_2697[2]),
        .Q(ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697[2]),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[3] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(k_buf_2_val_5_addr_reg_2697[3]),
        .Q(ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697[3]),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[4] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(k_buf_2_val_5_addr_reg_2697[4]),
        .Q(ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697[4]),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[5] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(k_buf_2_val_5_addr_reg_2697[5]),
        .Q(ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697[5]),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[6] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(k_buf_2_val_5_addr_reg_2697[6]),
        .Q(ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697[6]),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[7] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(k_buf_2_val_5_addr_reg_2697[7]),
        .Q(ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697[7]),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[8] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(k_buf_2_val_5_addr_reg_2697[8]),
        .Q(ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697[8]),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[9] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(k_buf_2_val_5_addr_reg_2697[9]),
        .Q(ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697[9]),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_or_cond_i_i_reg_2607_reg[0] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(ap_reg_pp0_iter1_or_cond_i_i_reg_2607),
        .Q(ap_reg_pp0_iter2_or_cond_i_i_reg_2607),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_or_cond_i_reg_2640_reg[0] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(ap_reg_pp0_iter1_or_cond_i_reg_2640),
        .Q(ap_reg_pp0_iter2_or_cond_i_reg_2640),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_reg_588_reg[0] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(reg_588[0]),
        .Q(ap_reg_pp0_iter2_reg_588[0]),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_reg_588_reg[1] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(reg_588[1]),
        .Q(ap_reg_pp0_iter2_reg_588[1]),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_reg_588_reg[2] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(reg_588[2]),
        .Q(ap_reg_pp0_iter2_reg_588[2]),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_reg_588_reg[3] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(reg_588[3]),
        .Q(ap_reg_pp0_iter2_reg_588[3]),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_reg_588_reg[4] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(reg_588[4]),
        .Q(ap_reg_pp0_iter2_reg_588[4]),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_reg_588_reg[5] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(reg_588[5]),
        .Q(ap_reg_pp0_iter2_reg_588[5]),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_reg_588_reg[6] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(reg_588[6]),
        .Q(ap_reg_pp0_iter2_reg_588[6]),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter2_reg_588_reg[7] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(reg_588[7]),
        .Q(ap_reg_pp0_iter2_reg_588[7]),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter3_or_cond_i_reg_2640_reg[0] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(ap_reg_pp0_iter2_or_cond_i_reg_2640),
        .Q(ap_reg_pp0_iter3_or_cond_i_reg_2640),
        .R(1'b0));
  FDRE \ap_reg_pp0_iter4_or_cond_i_reg_2640_reg[0] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(ap_reg_pp0_iter3_or_cond_i_reg_2640),
        .Q(ap_reg_pp0_iter4_or_cond_i_reg_2640),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair159" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \brmerge_reg_2627[0]_i_1 
       (.I0(tmp_31_fu_984_p2),
        .I1(tmp_72_not_reg_2504),
        .O(brmerge_fu_1034_p2));
  FDRE \brmerge_reg_2627_reg[0] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(brmerge_fu_1034_p2),
        .Q(brmerge_reg_2627),
        .R(1'b0));
  FDRE \col_buf_0_val_0_0_reg_2708_reg[0] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_0_0_fu_1140_p3[0]),
        .Q(col_buf_0_val_0_0_reg_2708[0]),
        .R(1'b0));
  FDRE \col_buf_0_val_0_0_reg_2708_reg[1] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_0_0_fu_1140_p3[1]),
        .Q(col_buf_0_val_0_0_reg_2708[1]),
        .R(1'b0));
  FDRE \col_buf_0_val_0_0_reg_2708_reg[2] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_0_0_fu_1140_p3[2]),
        .Q(col_buf_0_val_0_0_reg_2708[2]),
        .R(1'b0));
  FDRE \col_buf_0_val_0_0_reg_2708_reg[3] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_0_0_fu_1140_p3[3]),
        .Q(col_buf_0_val_0_0_reg_2708[3]),
        .R(1'b0));
  FDRE \col_buf_0_val_0_0_reg_2708_reg[4] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_0_0_fu_1140_p3[4]),
        .Q(col_buf_0_val_0_0_reg_2708[4]),
        .R(1'b0));
  FDRE \col_buf_0_val_0_0_reg_2708_reg[5] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_0_0_fu_1140_p3[5]),
        .Q(col_buf_0_val_0_0_reg_2708[5]),
        .R(1'b0));
  FDRE \col_buf_0_val_0_0_reg_2708_reg[6] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_0_0_fu_1140_p3[6]),
        .Q(col_buf_0_val_0_0_reg_2708[6]),
        .R(1'b0));
  FDRE \col_buf_0_val_0_0_reg_2708_reg[7] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_0_0_fu_1140_p3[7]),
        .Q(col_buf_0_val_0_0_reg_2708[7]),
        .R(1'b0));
  FDRE \col_buf_0_val_1_0_reg_2721_reg[0] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_1_0_fu_1159_p3[0]),
        .Q(col_buf_0_val_1_0_reg_2721[0]),
        .R(1'b0));
  FDRE \col_buf_0_val_1_0_reg_2721_reg[1] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_1_0_fu_1159_p3[1]),
        .Q(col_buf_0_val_1_0_reg_2721[1]),
        .R(1'b0));
  FDRE \col_buf_0_val_1_0_reg_2721_reg[2] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_1_0_fu_1159_p3[2]),
        .Q(col_buf_0_val_1_0_reg_2721[2]),
        .R(1'b0));
  FDRE \col_buf_0_val_1_0_reg_2721_reg[3] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_1_0_fu_1159_p3[3]),
        .Q(col_buf_0_val_1_0_reg_2721[3]),
        .R(1'b0));
  FDRE \col_buf_0_val_1_0_reg_2721_reg[4] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_1_0_fu_1159_p3[4]),
        .Q(col_buf_0_val_1_0_reg_2721[4]),
        .R(1'b0));
  FDRE \col_buf_0_val_1_0_reg_2721_reg[5] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_1_0_fu_1159_p3[5]),
        .Q(col_buf_0_val_1_0_reg_2721[5]),
        .R(1'b0));
  FDRE \col_buf_0_val_1_0_reg_2721_reg[6] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_1_0_fu_1159_p3[6]),
        .Q(col_buf_0_val_1_0_reg_2721[6]),
        .R(1'b0));
  FDRE \col_buf_0_val_1_0_reg_2721_reg[7] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_1_0_fu_1159_p3[7]),
        .Q(col_buf_0_val_1_0_reg_2721[7]),
        .R(1'b0));
  FDRE \col_buf_0_val_2_0_reg_2729_reg[0] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_2_0_fu_1178_p3[0]),
        .Q(col_buf_0_val_2_0_reg_2729[0]),
        .R(1'b0));
  FDRE \col_buf_0_val_2_0_reg_2729_reg[1] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_2_0_fu_1178_p3[1]),
        .Q(col_buf_0_val_2_0_reg_2729[1]),
        .R(1'b0));
  FDRE \col_buf_0_val_2_0_reg_2729_reg[2] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_2_0_fu_1178_p3[2]),
        .Q(col_buf_0_val_2_0_reg_2729[2]),
        .R(1'b0));
  FDRE \col_buf_0_val_2_0_reg_2729_reg[3] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_2_0_fu_1178_p3[3]),
        .Q(col_buf_0_val_2_0_reg_2729[3]),
        .R(1'b0));
  FDRE \col_buf_0_val_2_0_reg_2729_reg[4] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_2_0_fu_1178_p3[4]),
        .Q(col_buf_0_val_2_0_reg_2729[4]),
        .R(1'b0));
  FDRE \col_buf_0_val_2_0_reg_2729_reg[5] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_2_0_fu_1178_p3[5]),
        .Q(col_buf_0_val_2_0_reg_2729[5]),
        .R(1'b0));
  FDRE \col_buf_0_val_2_0_reg_2729_reg[6] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_2_0_fu_1178_p3[6]),
        .Q(col_buf_0_val_2_0_reg_2729[6]),
        .R(1'b0));
  FDRE \col_buf_0_val_2_0_reg_2729_reg[7] 
       (.C(ap_clk),
        .CE(col_buf_0_val_0_0_reg_27080),
        .D(col_buf_0_val_2_0_fu_1178_p3[7]),
        .Q(col_buf_0_val_2_0_reg_2729[7]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    \exitcond389_i_reg_2583[0]_i_1 
       (.I0(ap_CS_fsm_pp0_stage0),
        .I1(ap_block_pp0_stage0_subdone2_in),
        .O(ap_reg_pp0_iter1_brmerge_reg_26270));
  LUT6 #(
    .INIT(64'h0000000000100000)) 
    \exitcond389_i_reg_2583[0]_i_2 
       (.I0(t_V_2_reg_577_reg__0__0),
        .I1(t_V_2_reg_577_reg__0[9]),
        .I2(t_V_2_reg_577_reg__0[8]),
        .I3(\exitcond389_i_reg_2583[0]_i_3_n_2 ),
        .I4(\exitcond389_i_reg_2583[0]_i_4_n_2 ),
        .I5(\exitcond389_i_reg_2583[0]_i_5_n_2 ),
        .O(exitcond389_i_fu_936_p2));
  (* SOFT_HLUTNM = "soft_lutpair122" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \exitcond389_i_reg_2583[0]_i_3 
       (.I0(t_V_2_reg_577_reg__0[3]),
        .I1(t_V_2_reg_577_reg__0[4]),
        .O(\exitcond389_i_reg_2583[0]_i_3_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair147" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \exitcond389_i_reg_2583[0]_i_4 
       (.I0(t_V_2_reg_577_reg__0[6]),
        .I1(t_V_2_reg_577_reg__0[7]),
        .O(\exitcond389_i_reg_2583[0]_i_4_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair139" *) 
  LUT4 #(
    .INIT(16'hFFDF)) 
    \exitcond389_i_reg_2583[0]_i_5 
       (.I0(t_V_2_reg_577_reg__0[10]),
        .I1(t_V_2_reg_577_reg__0[5]),
        .I2(t_V_2_reg_577_reg__0[1]),
        .I3(t_V_2_reg_577_reg__0[2]),
        .O(\exitcond389_i_reg_2583[0]_i_5_n_2 ));
  FDRE \exitcond389_i_reg_2583_reg[0] 
       (.C(ap_clk),
        .CE(ap_reg_pp0_iter1_brmerge_reg_26270),
        .D(exitcond389_i_fu_936_p2),
        .Q(\exitcond389_i_reg_2583_reg_n_2_[0] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair157" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \i_V_reg_2495[0]_i_1 
       (.I0(t_V_reg_566[0]),
        .O(i_V_fu_631_p2[0]));
  LUT2 #(
    .INIT(4'h6)) 
    \i_V_reg_2495[1]_i_1 
       (.I0(t_V_reg_566[1]),
        .I1(t_V_reg_566[0]),
        .O(i_V_fu_631_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair145" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \i_V_reg_2495[2]_i_1 
       (.I0(t_V_reg_566[2]),
        .I1(t_V_reg_566[0]),
        .I2(t_V_reg_566[1]),
        .O(i_V_fu_631_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair145" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \i_V_reg_2495[3]_i_1 
       (.I0(t_V_reg_566[3]),
        .I1(t_V_reg_566[1]),
        .I2(t_V_reg_566[0]),
        .I3(t_V_reg_566[2]),
        .O(i_V_fu_631_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair127" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \i_V_reg_2495[4]_i_1 
       (.I0(t_V_reg_566[4]),
        .I1(t_V_reg_566[2]),
        .I2(t_V_reg_566[0]),
        .I3(t_V_reg_566[1]),
        .I4(t_V_reg_566[3]),
        .O(i_V_fu_631_p2[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \i_V_reg_2495[5]_i_1 
       (.I0(t_V_reg_566[5]),
        .I1(t_V_reg_566[4]),
        .I2(t_V_reg_566[3]),
        .I3(t_V_reg_566[1]),
        .I4(t_V_reg_566[0]),
        .I5(t_V_reg_566[2]),
        .O(i_V_fu_631_p2[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \i_V_reg_2495[6]_i_1 
       (.I0(t_V_reg_566[6]),
        .I1(\i_V_reg_2495[9]_i_2_n_2 ),
        .O(i_V_fu_631_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair138" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \i_V_reg_2495[7]_i_1 
       (.I0(t_V_reg_566[7]),
        .I1(t_V_reg_566[6]),
        .I2(\i_V_reg_2495[9]_i_2_n_2 ),
        .O(i_V_fu_631_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair131" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \i_V_reg_2495[8]_i_1 
       (.I0(t_V_reg_566[8]),
        .I1(\i_V_reg_2495[9]_i_2_n_2 ),
        .I2(t_V_reg_566[6]),
        .I3(t_V_reg_566[7]),
        .O(i_V_fu_631_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair131" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \i_V_reg_2495[9]_i_1 
       (.I0(t_V_reg_566[9]),
        .I1(t_V_reg_566[8]),
        .I2(t_V_reg_566[7]),
        .I3(t_V_reg_566[6]),
        .I4(\i_V_reg_2495[9]_i_2_n_2 ),
        .O(i_V_fu_631_p2[9]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \i_V_reg_2495[9]_i_2 
       (.I0(t_V_reg_566[4]),
        .I1(t_V_reg_566[3]),
        .I2(t_V_reg_566[1]),
        .I3(t_V_reg_566[0]),
        .I4(t_V_reg_566[2]),
        .I5(t_V_reg_566[5]),
        .O(\i_V_reg_2495[9]_i_2_n_2 ));
  FDRE \i_V_reg_2495_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state3),
        .D(i_V_fu_631_p2[0]),
        .Q(i_V_reg_2495[0]),
        .R(1'b0));
  FDRE \i_V_reg_2495_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state3),
        .D(i_V_fu_631_p2[1]),
        .Q(i_V_reg_2495[1]),
        .R(1'b0));
  FDRE \i_V_reg_2495_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state3),
        .D(i_V_fu_631_p2[2]),
        .Q(i_V_reg_2495[2]),
        .R(1'b0));
  FDRE \i_V_reg_2495_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state3),
        .D(i_V_fu_631_p2[3]),
        .Q(i_V_reg_2495[3]),
        .R(1'b0));
  FDRE \i_V_reg_2495_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state3),
        .D(i_V_fu_631_p2[4]),
        .Q(i_V_reg_2495[4]),
        .R(1'b0));
  FDRE \i_V_reg_2495_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state3),
        .D(i_V_fu_631_p2[5]),
        .Q(i_V_reg_2495[5]),
        .R(1'b0));
  FDRE \i_V_reg_2495_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state3),
        .D(i_V_fu_631_p2[6]),
        .Q(i_V_reg_2495[6]),
        .R(1'b0));
  FDRE \i_V_reg_2495_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state3),
        .D(i_V_fu_631_p2[7]),
        .Q(i_V_reg_2495[7]),
        .R(1'b0));
  FDRE \i_V_reg_2495_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state3),
        .D(i_V_fu_631_p2[8]),
        .Q(i_V_reg_2495[8]),
        .R(1'b0));
  FDRE \i_V_reg_2495_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state3),
        .D(i_V_fu_631_p2[9]),
        .Q(i_V_reg_2495[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair155" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \icmp_reg_2509[0]_i_1 
       (.I0(t_V_reg_566[1]),
        .I1(\icmp_reg_2509[0]_i_2_n_2 ),
        .O(\icmp_reg_2509[0]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \icmp_reg_2509[0]_i_2 
       (.I0(\icmp_reg_2509[0]_i_3_n_2 ),
        .I1(t_V_reg_566[2]),
        .I2(t_V_reg_566[3]),
        .I3(t_V_reg_566[7]),
        .I4(t_V_reg_566[9]),
        .I5(t_V_reg_566[8]),
        .O(\icmp_reg_2509[0]_i_2_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair134" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \icmp_reg_2509[0]_i_3 
       (.I0(t_V_reg_566[6]),
        .I1(t_V_reg_566[4]),
        .I2(t_V_reg_566[5]),
        .O(\icmp_reg_2509[0]_i_3_n_2 ));
  FDRE \icmp_reg_2509_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\icmp_reg_2509[0]_i_1_n_2 ),
        .Q(\icmp_reg_2509_reg_n_2_[0] ),
        .R(1'b0));
  system_edge_detect_0_1_Filter2D_k_buf_0_eOg k_buf_0_val_3_U
       (.ADDRBWRADDR({k_buf_0_val_5_U_n_10,k_buf_0_val_5_U_n_11,k_buf_0_val_5_U_n_12,k_buf_0_val_5_U_n_13,k_buf_0_val_5_U_n_14,k_buf_0_val_5_U_n_15,k_buf_0_val_5_U_n_16,k_buf_0_val_5_U_n_17,k_buf_0_val_5_U_n_18,k_buf_0_val_5_U_n_19,p_assign_2_reg_2622[0]}),
        .D(k_buf_0_val_3_q0),
        .Q(reg_588),
        .\ap_CS_fsm_reg[4] (ap_CS_fsm_pp0_stage0),
        .ap_block_pp0_stage0_subdone2_in(ap_block_pp0_stage0_subdone2_in),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp0_iter1(ap_enable_reg_pp0_iter1),
        .ap_enable_reg_pp0_iter2(ap_enable_reg_pp0_iter2),
        .ap_enable_reg_pp0_iter5_reg(ap_enable_reg_pp0_iter5_reg_n_2),
        .\ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg[0] (\ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg_n_2_[0] ),
        .ap_reg_pp0_iter1_or_cond_i_i_reg_2607(ap_reg_pp0_iter1_or_cond_i_i_reg_2607),
        .\ap_reg_pp0_iter2_reg_588_reg[0] (k_buf_0_val_3_U_n_13),
        .ap_reg_pp0_iter4_or_cond_i_reg_2640(ap_reg_pp0_iter4_or_cond_i_reg_2640),
        .col_buf_0_val_0_0_reg_27080(col_buf_0_val_0_0_reg_27080),
        .\exitcond389_i_reg_2583_reg[0] (\exitcond389_i_reg_2583_reg_n_2_[0] ),
        .\icmp_reg_2509_reg[0] (\icmp_reg_2509_reg_n_2_[0] ),
        .img1_data_stream_0_s_empty_n(img1_data_stream_0_s_empty_n),
        .img1_data_stream_1_s_empty_n(img1_data_stream_1_s_empty_n),
        .img1_data_stream_2_s_empty_n(img1_data_stream_2_s_empty_n),
        .img2_data_stream_0_s_full_n(img2_data_stream_0_s_full_n),
        .img2_data_stream_1_s_full_n(img2_data_stream_1_s_full_n),
        .img2_data_stream_2_s_full_n(img2_data_stream_2_s_full_n),
        .\k_buf_0_val_3_addr_reg_2649_reg[10] ({k_buf_2_val_5_addr_reg_2697,tmp_49_reg_2644}),
        .or_cond_i_i_reg_2607(or_cond_i_i_reg_2607),
        .ram_reg(k_buf_0_val_3_U_n_10),
        .tmp_1_reg_2500(tmp_1_reg_2500),
        .\tmp_2_reg_2514_reg[0] (\tmp_2_reg_2514_reg_n_2_[0] ));
  LUT3 #(
    .INIT(8'h08)) 
    \k_buf_0_val_3_addr_reg_2649[10]_i_1 
       (.I0(ap_block_pp0_stage0_subdone2_in),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(\exitcond389_i_reg_2583_reg_n_2_[0] ),
        .O(k_buf_0_val_3_addr_reg_26490));
  FDRE \k_buf_0_val_3_addr_reg_2649_reg[10] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_addr_reg_26490),
        .D(k_buf_0_val_5_U_n_10),
        .Q(k_buf_2_val_5_addr_reg_2697[10]),
        .R(1'b0));
  FDRE \k_buf_0_val_3_addr_reg_2649_reg[2] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_addr_reg_26490),
        .D(k_buf_0_val_5_U_n_18),
        .Q(k_buf_2_val_5_addr_reg_2697[2]),
        .R(1'b0));
  FDRE \k_buf_0_val_3_addr_reg_2649_reg[3] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_addr_reg_26490),
        .D(k_buf_0_val_5_U_n_17),
        .Q(k_buf_2_val_5_addr_reg_2697[3]),
        .R(1'b0));
  FDRE \k_buf_0_val_3_addr_reg_2649_reg[4] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_addr_reg_26490),
        .D(k_buf_0_val_5_U_n_16),
        .Q(k_buf_2_val_5_addr_reg_2697[4]),
        .R(1'b0));
  FDRE \k_buf_0_val_3_addr_reg_2649_reg[5] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_addr_reg_26490),
        .D(k_buf_0_val_5_U_n_15),
        .Q(k_buf_2_val_5_addr_reg_2697[5]),
        .R(1'b0));
  FDRE \k_buf_0_val_3_addr_reg_2649_reg[6] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_addr_reg_26490),
        .D(k_buf_0_val_5_U_n_14),
        .Q(k_buf_2_val_5_addr_reg_2697[6]),
        .R(1'b0));
  FDRE \k_buf_0_val_3_addr_reg_2649_reg[7] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_addr_reg_26490),
        .D(k_buf_0_val_5_U_n_13),
        .Q(k_buf_2_val_5_addr_reg_2697[7]),
        .R(1'b0));
  FDRE \k_buf_0_val_3_addr_reg_2649_reg[8] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_addr_reg_26490),
        .D(k_buf_0_val_5_U_n_12),
        .Q(k_buf_2_val_5_addr_reg_2697[8]),
        .R(1'b0));
  FDRE \k_buf_0_val_3_addr_reg_2649_reg[9] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_addr_reg_26490),
        .D(k_buf_0_val_5_U_n_11),
        .Q(k_buf_2_val_5_addr_reg_2697[9]),
        .R(1'b0));
  FDRE \k_buf_0_val_3_load_reg_2703_reg[0] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_load_reg_27030),
        .D(k_buf_0_val_3_q0[0]),
        .Q(k_buf_0_val_3_load_reg_2703[0]),
        .R(1'b0));
  FDRE \k_buf_0_val_3_load_reg_2703_reg[1] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_load_reg_27030),
        .D(k_buf_0_val_3_q0[1]),
        .Q(k_buf_0_val_3_load_reg_2703[1]),
        .R(1'b0));
  FDRE \k_buf_0_val_3_load_reg_2703_reg[2] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_load_reg_27030),
        .D(k_buf_0_val_3_q0[2]),
        .Q(k_buf_0_val_3_load_reg_2703[2]),
        .R(1'b0));
  FDRE \k_buf_0_val_3_load_reg_2703_reg[3] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_load_reg_27030),
        .D(k_buf_0_val_3_q0[3]),
        .Q(k_buf_0_val_3_load_reg_2703[3]),
        .R(1'b0));
  FDRE \k_buf_0_val_3_load_reg_2703_reg[4] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_load_reg_27030),
        .D(k_buf_0_val_3_q0[4]),
        .Q(k_buf_0_val_3_load_reg_2703[4]),
        .R(1'b0));
  FDRE \k_buf_0_val_3_load_reg_2703_reg[5] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_load_reg_27030),
        .D(k_buf_0_val_3_q0[5]),
        .Q(k_buf_0_val_3_load_reg_2703[5]),
        .R(1'b0));
  FDRE \k_buf_0_val_3_load_reg_2703_reg[6] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_load_reg_27030),
        .D(k_buf_0_val_3_q0[6]),
        .Q(k_buf_0_val_3_load_reg_2703[6]),
        .R(1'b0));
  FDRE \k_buf_0_val_3_load_reg_2703_reg[7] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_load_reg_27030),
        .D(k_buf_0_val_3_q0[7]),
        .Q(k_buf_0_val_3_load_reg_2703[7]),
        .R(1'b0));
  system_edge_detect_0_1_Filter2D_k_buf_0_eOg_16 k_buf_0_val_4_U
       (.ADDRBWRADDR({k_buf_0_val_5_U_n_10,k_buf_0_val_5_U_n_11,k_buf_0_val_5_U_n_12,k_buf_0_val_5_U_n_13,k_buf_0_val_5_U_n_14,k_buf_0_val_5_U_n_15,k_buf_0_val_5_U_n_16,k_buf_0_val_5_U_n_17,k_buf_0_val_5_U_n_18,k_buf_0_val_5_U_n_19,p_assign_2_reg_2622[0]}),
        .D(k_buf_0_val_4_q0),
        .Q(ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697),
        .\ap_CS_fsm_reg[4] (k_buf_0_val_3_U_n_10),
        .ap_block_pp0_stage0_subdone2_in(ap_block_pp0_stage0_subdone2_in),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp0_iter3(ap_enable_reg_pp0_iter3),
        .ap_reg_pp0_iter2_or_cond_i_i_reg_2607(ap_reg_pp0_iter2_or_cond_i_i_reg_2607),
        .\ap_reg_pp0_iter2_reg_588_reg[7] (ap_reg_pp0_iter2_reg_588),
        .\icmp_reg_2509_reg[0] (\icmp_reg_2509_reg_n_2_[0] ),
        .\k_buf_0_val_3_load_reg_2703_reg[7] (k_buf_0_val_3_load_reg_2703),
        .\tmp_116_0_1_reg_2518_reg[0] (\tmp_116_0_1_reg_2518_reg_n_2_[0] ),
        .tmp_1_reg_2500(tmp_1_reg_2500));
  LUT2 #(
    .INIT(4'h8)) 
    \k_buf_0_val_4_load_reg_2716[7]_i_1 
       (.I0(ap_enable_reg_pp0_iter2),
        .I1(col_buf_0_val_0_0_reg_27080),
        .O(k_buf_0_val_3_load_reg_27030));
  FDRE \k_buf_0_val_4_load_reg_2716_reg[0] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_load_reg_27030),
        .D(k_buf_0_val_4_q0[0]),
        .Q(k_buf_0_val_4_load_reg_2716[0]),
        .R(1'b0));
  FDRE \k_buf_0_val_4_load_reg_2716_reg[1] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_load_reg_27030),
        .D(k_buf_0_val_4_q0[1]),
        .Q(k_buf_0_val_4_load_reg_2716[1]),
        .R(1'b0));
  FDRE \k_buf_0_val_4_load_reg_2716_reg[2] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_load_reg_27030),
        .D(k_buf_0_val_4_q0[2]),
        .Q(k_buf_0_val_4_load_reg_2716[2]),
        .R(1'b0));
  FDRE \k_buf_0_val_4_load_reg_2716_reg[3] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_load_reg_27030),
        .D(k_buf_0_val_4_q0[3]),
        .Q(k_buf_0_val_4_load_reg_2716[3]),
        .R(1'b0));
  FDRE \k_buf_0_val_4_load_reg_2716_reg[4] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_load_reg_27030),
        .D(k_buf_0_val_4_q0[4]),
        .Q(k_buf_0_val_4_load_reg_2716[4]),
        .R(1'b0));
  FDRE \k_buf_0_val_4_load_reg_2716_reg[5] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_load_reg_27030),
        .D(k_buf_0_val_4_q0[5]),
        .Q(k_buf_0_val_4_load_reg_2716[5]),
        .R(1'b0));
  FDRE \k_buf_0_val_4_load_reg_2716_reg[6] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_load_reg_27030),
        .D(k_buf_0_val_4_q0[6]),
        .Q(k_buf_0_val_4_load_reg_2716[6]),
        .R(1'b0));
  FDRE \k_buf_0_val_4_load_reg_2716_reg[7] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_load_reg_27030),
        .D(k_buf_0_val_4_q0[7]),
        .Q(k_buf_0_val_4_load_reg_2716[7]),
        .R(1'b0));
  system_edge_detect_0_1_Filter2D_k_buf_0_eOg_17 k_buf_0_val_5_U
       (.ADDRBWRADDR({k_buf_0_val_5_U_n_10,k_buf_0_val_5_U_n_11,k_buf_0_val_5_U_n_12,k_buf_0_val_5_U_n_13,k_buf_0_val_5_U_n_14,k_buf_0_val_5_U_n_15,k_buf_0_val_5_U_n_16,k_buf_0_val_5_U_n_17,k_buf_0_val_5_U_n_18,k_buf_0_val_5_U_n_19}),
        .DOBDO(k_buf_0_val_5_q0),
        .\ImagLoc_x_reg_2592_reg[10] (ImagLoc_x_reg_2592),
        .Q(ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697),
        .\ap_CS_fsm_reg[4] (k_buf_0_val_3_U_n_10),
        .ap_block_pp0_stage0_subdone2_in(ap_block_pp0_stage0_subdone2_in),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp0_iter3(ap_enable_reg_pp0_iter3),
        .ap_reg_pp0_iter2_or_cond_i_i_reg_2607(ap_reg_pp0_iter2_or_cond_i_i_reg_2607),
        .\ap_reg_pp0_iter2_reg_588_reg[7] (ap_reg_pp0_iter2_reg_588),
        .\icmp_reg_2509_reg[0] (\icmp_reg_2509_reg_n_2_[0] ),
        .\k_buf_0_val_4_load_reg_2716_reg[7] (k_buf_0_val_4_load_reg_2716),
        .or_cond_i_i_reg_2607(or_cond_i_i_reg_2607),
        .p_assign_2_reg_2622(p_assign_2_reg_2622),
        .\p_p2_i_i_cast_cast_reg_2612_reg[10] (p_p2_i_i_cast_cast_reg_2612_reg),
        .tmp_1_reg_2500(tmp_1_reg_2500),
        .\tmp_2_reg_2514_reg[0] (\tmp_2_reg_2514_reg_n_2_[0] ),
        .tmp_31_reg_2602(tmp_31_reg_2602),
        .tmp_33_reg_2617(tmp_33_reg_2617),
        .tmp_47_reg_2597(tmp_47_reg_2597));
  LUT5 #(
    .INIT(32'h40BFBF40)) 
    \mOutPtr[1]_i_1__2 
       (.I0(shiftReg_ce_0),
        .I1(Q[1]),
        .I2(E),
        .I3(\mOutPtr_reg[1]_1 [0]),
        .I4(\mOutPtr_reg[1]_1 [1]),
        .O(D));
  LUT5 #(
    .INIT(32'h40BFBF40)) 
    \mOutPtr[1]_i_1__3 
       (.I0(shiftReg_ce_0),
        .I1(Q[1]),
        .I2(E),
        .I3(\mOutPtr_reg[1]_2 [0]),
        .I4(\mOutPtr_reg[1]_2 [1]),
        .O(\mOutPtr_reg[1] ));
  (* SOFT_HLUTNM = "soft_lutpair123" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \mOutPtr[1]_i_1__4 
       (.I0(shiftReg_ce_0),
        .I1(E),
        .I2(Q[1]),
        .O(\mOutPtr_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair130" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \mOutPtr[1]_i_1__5 
       (.I0(Q[1]),
        .I1(ap_reg_pp0_iter4_or_cond_i_reg_2640),
        .I2(ap_enable_reg_pp0_iter5_reg_n_2),
        .I3(ap_block_pp0_stage0_subdone2_in),
        .I4(shiftReg_ce_1),
        .O(\mOutPtr_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair123" *) 
  LUT5 #(
    .INIT(32'h40BFBF40)) 
    \mOutPtr[1]_i_2__1 
       (.I0(shiftReg_ce_0),
        .I1(Q[1]),
        .I2(E),
        .I3(\mOutPtr_reg[1]_3 [0]),
        .I4(\mOutPtr_reg[1]_3 [1]),
        .O(\mOutPtr_reg[1]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair125" *) 
  LUT5 #(
    .INIT(32'h5D55FBDB)) 
    \or_cond_i425_i_reg_2542[0]_i_1 
       (.I0(t_V_reg_566[9]),
        .I1(\tmp_3_reg_2522[0]_i_2_n_2 ),
        .I2(t_V_reg_566[7]),
        .I3(\tmp_3_reg_2522[0]_i_3_n_2 ),
        .I4(t_V_reg_566[8]),
        .O(or_cond_i425_i_fu_709_p2));
  FDRE \or_cond_i425_i_reg_2542_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(or_cond_i425_i_fu_709_p2),
        .Q(or_cond_i425_i_reg_2542),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h08)) 
    \or_cond_i_i_reg_2607[0]_i_1 
       (.I0(ap_block_pp0_stage0_subdone2_in),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(exitcond389_i_fu_936_p2),
        .O(ImagLoc_x_reg_25920));
  (* SOFT_HLUTNM = "soft_lutpair159" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \or_cond_i_i_reg_2607[0]_i_2 
       (.I0(tmp_31_fu_984_p2),
        .I1(ImagLoc_x_fu_964_p2),
        .O(or_cond_i_i_fu_990_p2));
  FDRE \or_cond_i_i_reg_2607_reg[0] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(or_cond_i_i_fu_990_p2),
        .Q(or_cond_i_i_reg_2607),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    \or_cond_i_reg_2640[0]_i_1 
       (.I0(\icmp_reg_2509_reg_n_2_[0] ),
        .I1(\or_cond_i_reg_2640[0]_i_2_n_2 ),
        .O(or_cond_i_fu_1039_p2));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \or_cond_i_reg_2640[0]_i_2 
       (.I0(\or_cond_i_reg_2640[0]_i_3_n_2 ),
        .I1(t_V_2_reg_577_reg__0[6]),
        .I2(t_V_2_reg_577_reg__0[7]),
        .I3(t_V_2_reg_577_reg__0[8]),
        .I4(t_V_2_reg_577_reg__0[9]),
        .I5(t_V_2_reg_577_reg__0[10]),
        .O(\or_cond_i_reg_2640[0]_i_2_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair124" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \or_cond_i_reg_2640[0]_i_3 
       (.I0(t_V_2_reg_577_reg__0[5]),
        .I1(t_V_2_reg_577_reg__0[1]),
        .I2(t_V_2_reg_577_reg__0[2]),
        .I3(t_V_2_reg_577_reg__0[3]),
        .I4(t_V_2_reg_577_reg__0[4]),
        .O(\or_cond_i_reg_2640[0]_i_3_n_2 ));
  FDRE \or_cond_i_reg_2640_reg[0] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(or_cond_i_fu_1039_p2),
        .Q(or_cond_i_reg_2640),
        .R(1'b0));
  CARRY4 p_Val2_10_0_0_2_fu_1493_p2_carry
       (.CI(1'b0),
        .CO({p_Val2_10_0_0_2_fu_1493_p2_carry_n_2,p_Val2_10_0_0_2_fu_1493_p2_carry_n_3,p_Val2_10_0_0_2_fu_1493_p2_carry_n_4,p_Val2_10_0_0_2_fu_1493_p2_carry_n_5}),
        .CYINIT(1'b1),
        .DI(tmp_160_0_0_2_cast_fu_1489_p1[3:0]),
        .O({p_Val2_10_0_0_2_fu_1493_p2_carry_n_6,p_Val2_10_0_0_2_fu_1493_p2_carry_n_7,p_Val2_10_0_0_2_fu_1493_p2_carry_n_8,p_Val2_10_0_0_2_fu_1493_p2_carry_n_9}),
        .S({p_Val2_10_0_0_2_fu_1493_p2_carry_i_1_n_2,p_Val2_10_0_0_2_fu_1493_p2_carry_i_2_n_2,p_Val2_10_0_0_2_fu_1493_p2_carry_i_3_n_2,p_Val2_10_0_0_2_fu_1493_p2_carry_i_4_n_2}));
  CARRY4 p_Val2_10_0_0_2_fu_1493_p2_carry__0
       (.CI(p_Val2_10_0_0_2_fu_1493_p2_carry_n_2),
        .CO({p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_2,p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_3,p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_4,p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_5}),
        .CYINIT(1'b0),
        .DI(tmp_160_0_0_2_cast_fu_1489_p1[7:4]),
        .O({p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_6,p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_7,p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_8,p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_9}),
        .S({p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_1_n_2,p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_2_n_2,p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_3_n_2,p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_4_n_2}));
  LUT6 #(
    .INIT(64'hB8FFB800470047FF)) 
    p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_1
       (.I0(col_buf_0_val_1_0_reg_2721[7]),
        .I1(row_assign_9_0_2_t_reg_2565[0]),
        .I2(col_buf_0_val_0_0_reg_2708[7]),
        .I3(p_Val2_10_0_0_2_fu_1493_p2_carry_i_5_n_2),
        .I4(col_buf_0_val_2_0_reg_2729[7]),
        .I5(src_kernel_win_0_va_5_fu_266[7]),
        .O(p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_1_n_2));
  LUT6 #(
    .INIT(64'hE2FFE2001D001DFF)) 
    p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_2
       (.I0(col_buf_0_val_0_0_reg_2708[6]),
        .I1(row_assign_9_0_2_t_reg_2565[0]),
        .I2(col_buf_0_val_1_0_reg_2721[6]),
        .I3(p_Val2_10_0_0_2_fu_1493_p2_carry_i_5_n_2),
        .I4(col_buf_0_val_2_0_reg_2729[6]),
        .I5(src_kernel_win_0_va_5_fu_266[6]),
        .O(p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_2_n_2));
  LUT6 #(
    .INIT(64'hEEE222E2111DDD1D)) 
    p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_3
       (.I0(col_buf_0_val_2_0_reg_2729[5]),
        .I1(p_Val2_10_0_0_2_fu_1493_p2_carry_i_5_n_2),
        .I2(col_buf_0_val_0_0_reg_2708[5]),
        .I3(row_assign_9_0_2_t_reg_2565[0]),
        .I4(col_buf_0_val_1_0_reg_2721[5]),
        .I5(src_kernel_win_0_va_5_fu_266[5]),
        .O(p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_3_n_2));
  LUT6 #(
    .INIT(64'hE2FFE2001D001DFF)) 
    p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_4
       (.I0(col_buf_0_val_0_0_reg_2708[4]),
        .I1(row_assign_9_0_2_t_reg_2565[0]),
        .I2(col_buf_0_val_1_0_reg_2721[4]),
        .I3(p_Val2_10_0_0_2_fu_1493_p2_carry_i_5_n_2),
        .I4(col_buf_0_val_2_0_reg_2729[4]),
        .I5(src_kernel_win_0_va_5_fu_266[4]),
        .O(p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_4_n_2));
  CARRY4 p_Val2_10_0_0_2_fu_1493_p2_carry__1
       (.CI(p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_2),
        .CO(NLW_p_Val2_10_0_0_2_fu_1493_p2_carry__1_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_p_Val2_10_0_0_2_fu_1493_p2_carry__1_O_UNCONNECTED[3:1],p_Val2_10_0_0_2_fu_1493_p2_carry__1_n_9}),
        .S({1'b0,1'b0,1'b0,1'b1}));
  LUT6 #(
    .INIT(64'hB8FFB800470047FF)) 
    p_Val2_10_0_0_2_fu_1493_p2_carry_i_1
       (.I0(col_buf_0_val_1_0_reg_2721[3]),
        .I1(row_assign_9_0_2_t_reg_2565[0]),
        .I2(col_buf_0_val_0_0_reg_2708[3]),
        .I3(p_Val2_10_0_0_2_fu_1493_p2_carry_i_5_n_2),
        .I4(col_buf_0_val_2_0_reg_2729[3]),
        .I5(src_kernel_win_0_va_5_fu_266[3]),
        .O(p_Val2_10_0_0_2_fu_1493_p2_carry_i_1_n_2));
  LUT6 #(
    .INIT(64'hEEE222E2111DDD1D)) 
    p_Val2_10_0_0_2_fu_1493_p2_carry_i_2
       (.I0(col_buf_0_val_2_0_reg_2729[2]),
        .I1(p_Val2_10_0_0_2_fu_1493_p2_carry_i_5_n_2),
        .I2(col_buf_0_val_0_0_reg_2708[2]),
        .I3(row_assign_9_0_2_t_reg_2565[0]),
        .I4(col_buf_0_val_1_0_reg_2721[2]),
        .I5(src_kernel_win_0_va_5_fu_266[2]),
        .O(p_Val2_10_0_0_2_fu_1493_p2_carry_i_2_n_2));
  LUT6 #(
    .INIT(64'hEEE222E2111DDD1D)) 
    p_Val2_10_0_0_2_fu_1493_p2_carry_i_3
       (.I0(col_buf_0_val_2_0_reg_2729[1]),
        .I1(p_Val2_10_0_0_2_fu_1493_p2_carry_i_5_n_2),
        .I2(col_buf_0_val_0_0_reg_2708[1]),
        .I3(row_assign_9_0_2_t_reg_2565[0]),
        .I4(col_buf_0_val_1_0_reg_2721[1]),
        .I5(src_kernel_win_0_va_5_fu_266[1]),
        .O(p_Val2_10_0_0_2_fu_1493_p2_carry_i_3_n_2));
  LUT6 #(
    .INIT(64'hEEE222E2111DDD1D)) 
    p_Val2_10_0_0_2_fu_1493_p2_carry_i_4
       (.I0(col_buf_0_val_2_0_reg_2729[0]),
        .I1(p_Val2_10_0_0_2_fu_1493_p2_carry_i_5_n_2),
        .I2(col_buf_0_val_0_0_reg_2708[0]),
        .I3(row_assign_9_0_2_t_reg_2565[0]),
        .I4(col_buf_0_val_1_0_reg_2721[0]),
        .I5(src_kernel_win_0_va_5_fu_266[0]),
        .O(p_Val2_10_0_0_2_fu_1493_p2_carry_i_4_n_2));
  (* SOFT_HLUTNM = "soft_lutpair137" *) 
  LUT2 #(
    .INIT(4'h2)) 
    p_Val2_10_0_0_2_fu_1493_p2_carry_i_5
       (.I0(tmp_3_reg_2522),
        .I1(row_assign_9_0_2_t_reg_2565[1]),
        .O(p_Val2_10_0_0_2_fu_1493_p2_carry_i_5_n_2));
  LUT2 #(
    .INIT(4'h8)) 
    \p_Val2_10_0_0_2_reg_2817[8]_i_1 
       (.I0(ap_reg_pp0_iter2_or_cond_i_reg_2640),
        .I1(ap_block_pp0_stage0_subdone2_in),
        .O(p_Val2_10_0_0_2_reg_28170));
  FDRE \p_Val2_10_0_0_2_reg_2817_reg[8] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(p_Val2_10_0_0_2_fu_1493_p2_carry__1_n_9),
        .Q(p_Val2_10_0_0_2_reg_2817),
        .R(1'b0));
  CARRY4 p_Val2_1_fu_1973_p2__1_carry
       (.CI(1'b0),
        .CO({p_Val2_1_fu_1973_p2__1_carry_n_2,p_Val2_1_fu_1973_p2__1_carry_n_3,p_Val2_1_fu_1973_p2__1_carry_n_4,p_Val2_1_fu_1973_p2__1_carry_n_5}),
        .CYINIT(1'b0),
        .DI({p_Val2_1_fu_1973_p2__1_carry_i_1_n_2,p_Val2_1_fu_1973_p2__1_carry_i_2_n_2,p_Val2_1_fu_1973_p2__1_carry_i_3_n_2,tmp_54_reg_2827[1]}),
        .O({p_Val2_1_fu_1973_p2__1_carry_n_6,p_Val2_1_fu_1973_p2__1_carry_n_7,p_Val2_1_fu_1973_p2__1_carry_n_8,NLW_p_Val2_1_fu_1973_p2__1_carry_O_UNCONNECTED[0]}),
        .S({p_Val2_1_fu_1973_p2__1_carry_i_4_n_2,p_Val2_1_fu_1973_p2__1_carry_i_5_n_2,p_Val2_1_fu_1973_p2__1_carry_i_6_n_2,p_Val2_1_fu_1973_p2__1_carry_i_7_n_2}));
  CARRY4 p_Val2_1_fu_1973_p2__1_carry__0
       (.CI(p_Val2_1_fu_1973_p2__1_carry_n_2),
        .CO({NLW_p_Val2_1_fu_1973_p2__1_carry__0_CO_UNCONNECTED[3:2],p_Val2_1_fu_1973_p2__1_carry__0_n_4,p_Val2_1_fu_1973_p2__1_carry__0_n_5}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,p_Val2_1_fu_1973_p2__1_carry__0_i_1_n_2,p_Val2_1_fu_1973_p2__1_carry__0_i_2_n_2}),
        .O({NLW_p_Val2_1_fu_1973_p2__1_carry__0_O_UNCONNECTED[3],p_Val2_1_fu_1973_p2__1_carry__0_n_7,p_Val2_1_fu_1973_p2__1_carry__0_n_8,p_Val2_1_fu_1973_p2__1_carry__0_n_9}),
        .S({1'b0,p_Val2_1_fu_1973_p2__1_carry__0_i_3_n_2,p_Val2_1_fu_1973_p2__1_carry__0_i_4_n_2,p_Val2_1_fu_1973_p2__1_carry__0_i_5_n_2}));
  (* HLUTNM = "lutpair3" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    p_Val2_1_fu_1973_p2__1_carry__0_i_1
       (.I0(tmp_53_reg_2822[5]),
        .I1(src_kernel_win_0_va_7_reg_2811[4]),
        .I2(tmp_54_reg_2827[5]),
        .O(p_Val2_1_fu_1973_p2__1_carry__0_i_1_n_2));
  LUT3 #(
    .INIT(8'hE8)) 
    p_Val2_1_fu_1973_p2__1_carry__0_i_2
       (.I0(tmp_53_reg_2822[4]),
        .I1(src_kernel_win_0_va_7_reg_2811[3]),
        .I2(tmp_54_reg_2827[4]),
        .O(p_Val2_1_fu_1973_p2__1_carry__0_i_2_n_2));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    p_Val2_1_fu_1973_p2__1_carry__0_i_3
       (.I0(tmp_54_reg_2827[6]),
        .I1(src_kernel_win_0_va_7_reg_2811[5]),
        .I2(tmp_53_reg_2822[6]),
        .I3(src_kernel_win_0_va_7_reg_2811[6]),
        .I4(tmp_53_reg_2822[7]),
        .I5(tmp_54_reg_2827[7]),
        .O(p_Val2_1_fu_1973_p2__1_carry__0_i_3_n_2));
  LUT4 #(
    .INIT(16'h6996)) 
    p_Val2_1_fu_1973_p2__1_carry__0_i_4
       (.I0(p_Val2_1_fu_1973_p2__1_carry__0_i_1_n_2),
        .I1(src_kernel_win_0_va_7_reg_2811[5]),
        .I2(tmp_53_reg_2822[6]),
        .I3(tmp_54_reg_2827[6]),
        .O(p_Val2_1_fu_1973_p2__1_carry__0_i_4_n_2));
  (* HLUTNM = "lutpair3" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    p_Val2_1_fu_1973_p2__1_carry__0_i_5
       (.I0(tmp_53_reg_2822[5]),
        .I1(src_kernel_win_0_va_7_reg_2811[4]),
        .I2(tmp_54_reg_2827[5]),
        .I3(p_Val2_1_fu_1973_p2__1_carry__0_i_2_n_2),
        .O(p_Val2_1_fu_1973_p2__1_carry__0_i_5_n_2));
  LUT3 #(
    .INIT(8'hE8)) 
    p_Val2_1_fu_1973_p2__1_carry_i_1
       (.I0(tmp_53_reg_2822[3]),
        .I1(src_kernel_win_0_va_7_reg_2811[2]),
        .I2(tmp_54_reg_2827[3]),
        .O(p_Val2_1_fu_1973_p2__1_carry_i_1_n_2));
  LUT3 #(
    .INIT(8'hE8)) 
    p_Val2_1_fu_1973_p2__1_carry_i_2
       (.I0(tmp_53_reg_2822[2]),
        .I1(src_kernel_win_0_va_7_reg_2811[1]),
        .I2(tmp_54_reg_2827[2]),
        .O(p_Val2_1_fu_1973_p2__1_carry_i_2_n_2));
  LUT3 #(
    .INIT(8'h96)) 
    p_Val2_1_fu_1973_p2__1_carry_i_3
       (.I0(tmp_54_reg_2827[2]),
        .I1(tmp_53_reg_2822[2]),
        .I2(src_kernel_win_0_va_7_reg_2811[1]),
        .O(p_Val2_1_fu_1973_p2__1_carry_i_3_n_2));
  LUT4 #(
    .INIT(16'h6996)) 
    p_Val2_1_fu_1973_p2__1_carry_i_4
       (.I0(tmp_53_reg_2822[4]),
        .I1(src_kernel_win_0_va_7_reg_2811[3]),
        .I2(tmp_54_reg_2827[4]),
        .I3(p_Val2_1_fu_1973_p2__1_carry_i_1_n_2),
        .O(p_Val2_1_fu_1973_p2__1_carry_i_4_n_2));
  LUT4 #(
    .INIT(16'h6996)) 
    p_Val2_1_fu_1973_p2__1_carry_i_5
       (.I0(tmp_53_reg_2822[3]),
        .I1(src_kernel_win_0_va_7_reg_2811[2]),
        .I2(tmp_54_reg_2827[3]),
        .I3(p_Val2_1_fu_1973_p2__1_carry_i_2_n_2),
        .O(p_Val2_1_fu_1973_p2__1_carry_i_5_n_2));
  LUT5 #(
    .INIT(32'h69969696)) 
    p_Val2_1_fu_1973_p2__1_carry_i_6
       (.I0(tmp_53_reg_2822[2]),
        .I1(src_kernel_win_0_va_7_reg_2811[1]),
        .I2(tmp_54_reg_2827[2]),
        .I3(src_kernel_win_0_va_7_reg_2811[0]),
        .I4(tmp_53_reg_2822[1]),
        .O(p_Val2_1_fu_1973_p2__1_carry_i_6_n_2));
  LUT3 #(
    .INIT(8'h96)) 
    p_Val2_1_fu_1973_p2__1_carry_i_7
       (.I0(tmp_53_reg_2822[1]),
        .I1(src_kernel_win_0_va_7_reg_2811[0]),
        .I2(tmp_54_reg_2827[1]),
        .O(p_Val2_1_fu_1973_p2__1_carry_i_7_n_2));
  CARRY4 p_Val2_1_fu_1973_p2__20_carry
       (.CI(1'b0),
        .CO({p_Val2_1_fu_1973_p2__20_carry_n_2,p_Val2_1_fu_1973_p2__20_carry_n_3,p_Val2_1_fu_1973_p2__20_carry_n_4,p_Val2_1_fu_1973_p2__20_carry_n_5}),
        .CYINIT(1'b0),
        .DI({p_Val2_1_fu_1973_p2__20_carry_i_1_n_2,p_Val2_1_fu_1973_p2__20_carry_i_2_n_2,p_Val2_1_fu_1973_p2__20_carry_i_3_n_2,1'b0}),
        .O(p_Val2_1_fu_1973_p2[3:0]),
        .S({p_Val2_1_fu_1973_p2__20_carry_i_4_n_2,p_Val2_1_fu_1973_p2__20_carry_i_5_n_2,p_Val2_1_fu_1973_p2__20_carry_i_6_n_2,p_Val2_1_fu_1973_p2__20_carry_i_7_n_2}));
  CARRY4 p_Val2_1_fu_1973_p2__20_carry__0
       (.CI(p_Val2_1_fu_1973_p2__20_carry_n_2),
        .CO({NLW_p_Val2_1_fu_1973_p2__20_carry__0_CO_UNCONNECTED[3],p_Val2_1_fu_1973_p2__20_carry__0_n_3,p_Val2_1_fu_1973_p2__20_carry__0_n_4,p_Val2_1_fu_1973_p2__20_carry__0_n_5}),
        .CYINIT(1'b0),
        .DI({1'b0,p_Val2_1_fu_1973_p2__20_carry__0_i_1_n_2,p_Val2_1_fu_1973_p2__20_carry__0_i_2_n_2,p_Val2_1_fu_1973_p2__20_carry__0_i_3_n_2}),
        .O(p_Val2_1_fu_1973_p2[7:4]),
        .S({p_Val2_1_fu_1973_p2__20_carry__0_i_4_n_2,p_Val2_1_fu_1973_p2__20_carry__0_i_5_n_2,p_Val2_1_fu_1973_p2__20_carry__0_i_6_n_2,p_Val2_1_fu_1973_p2__20_carry__0_i_7_n_2}));
  LUT3 #(
    .INIT(8'hE8)) 
    p_Val2_1_fu_1973_p2__20_carry__0_i_1
       (.I0(p_Val2_1_fu_1973_p2__1_carry__0_n_9),
        .I1(tmp_56_reg_2832[5]),
        .I2(src_kernel_win_0_va_6_reg_2805[5]),
        .O(p_Val2_1_fu_1973_p2__20_carry__0_i_1_n_2));
  LUT3 #(
    .INIT(8'hE8)) 
    p_Val2_1_fu_1973_p2__20_carry__0_i_2
       (.I0(p_Val2_1_fu_1973_p2__1_carry_n_6),
        .I1(tmp_56_reg_2832[4]),
        .I2(src_kernel_win_0_va_6_reg_2805[4]),
        .O(p_Val2_1_fu_1973_p2__20_carry__0_i_2_n_2));
  (* HLUTNM = "lutpair6" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    p_Val2_1_fu_1973_p2__20_carry__0_i_3
       (.I0(p_Val2_1_fu_1973_p2__1_carry_n_7),
        .I1(tmp_56_reg_2832[3]),
        .I2(src_kernel_win_0_va_6_reg_2805[3]),
        .O(p_Val2_1_fu_1973_p2__20_carry__0_i_3_n_2));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    p_Val2_1_fu_1973_p2__20_carry__0_i_4
       (.I0(src_kernel_win_0_va_6_reg_2805[6]),
        .I1(tmp_56_reg_2832[6]),
        .I2(p_Val2_1_fu_1973_p2__1_carry__0_n_8),
        .I3(tmp_56_reg_2832[7]),
        .I4(p_Val2_1_fu_1973_p2__1_carry__0_n_7),
        .I5(src_kernel_win_0_va_6_reg_2805[7]),
        .O(p_Val2_1_fu_1973_p2__20_carry__0_i_4_n_2));
  LUT4 #(
    .INIT(16'h6996)) 
    p_Val2_1_fu_1973_p2__20_carry__0_i_5
       (.I0(p_Val2_1_fu_1973_p2__20_carry__0_i_1_n_2),
        .I1(tmp_56_reg_2832[6]),
        .I2(p_Val2_1_fu_1973_p2__1_carry__0_n_8),
        .I3(src_kernel_win_0_va_6_reg_2805[6]),
        .O(p_Val2_1_fu_1973_p2__20_carry__0_i_5_n_2));
  LUT4 #(
    .INIT(16'h6996)) 
    p_Val2_1_fu_1973_p2__20_carry__0_i_6
       (.I0(p_Val2_1_fu_1973_p2__1_carry__0_n_9),
        .I1(tmp_56_reg_2832[5]),
        .I2(src_kernel_win_0_va_6_reg_2805[5]),
        .I3(p_Val2_1_fu_1973_p2__20_carry__0_i_2_n_2),
        .O(p_Val2_1_fu_1973_p2__20_carry__0_i_6_n_2));
  LUT4 #(
    .INIT(16'h6996)) 
    p_Val2_1_fu_1973_p2__20_carry__0_i_7
       (.I0(p_Val2_1_fu_1973_p2__1_carry_n_6),
        .I1(tmp_56_reg_2832[4]),
        .I2(src_kernel_win_0_va_6_reg_2805[4]),
        .I3(p_Val2_1_fu_1973_p2__20_carry__0_i_3_n_2),
        .O(p_Val2_1_fu_1973_p2__20_carry__0_i_7_n_2));
  (* HLUTNM = "lutpair5" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    p_Val2_1_fu_1973_p2__20_carry_i_1
       (.I0(p_Val2_1_fu_1973_p2__1_carry_n_8),
        .I1(tmp_56_reg_2832[2]),
        .I2(src_kernel_win_0_va_6_reg_2805[2]),
        .O(p_Val2_1_fu_1973_p2__20_carry_i_1_n_2));
  LUT5 #(
    .INIT(32'hFF969600)) 
    p_Val2_1_fu_1973_p2__20_carry_i_2
       (.I0(tmp_54_reg_2827[1]),
        .I1(src_kernel_win_0_va_7_reg_2811[0]),
        .I2(tmp_53_reg_2822[1]),
        .I3(tmp_56_reg_2832[1]),
        .I4(src_kernel_win_0_va_6_reg_2805[1]),
        .O(p_Val2_1_fu_1973_p2__20_carry_i_2_n_2));
  (* HLUTNM = "lutpair4" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    p_Val2_1_fu_1973_p2__20_carry_i_3
       (.I0(tmp_53_reg_2822[0]),
        .I1(tmp_56_reg_2832[0]),
        .I2(src_kernel_win_0_va_6_reg_2805[0]),
        .O(p_Val2_1_fu_1973_p2__20_carry_i_3_n_2));
  (* HLUTNM = "lutpair6" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    p_Val2_1_fu_1973_p2__20_carry_i_4
       (.I0(p_Val2_1_fu_1973_p2__1_carry_n_7),
        .I1(tmp_56_reg_2832[3]),
        .I2(src_kernel_win_0_va_6_reg_2805[3]),
        .I3(p_Val2_1_fu_1973_p2__20_carry_i_1_n_2),
        .O(p_Val2_1_fu_1973_p2__20_carry_i_4_n_2));
  (* HLUTNM = "lutpair5" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    p_Val2_1_fu_1973_p2__20_carry_i_5
       (.I0(p_Val2_1_fu_1973_p2__1_carry_n_8),
        .I1(tmp_56_reg_2832[2]),
        .I2(src_kernel_win_0_va_6_reg_2805[2]),
        .I3(p_Val2_1_fu_1973_p2__20_carry_i_2_n_2),
        .O(p_Val2_1_fu_1973_p2__20_carry_i_5_n_2));
  LUT6 #(
    .INIT(64'h6996966996696996)) 
    p_Val2_1_fu_1973_p2__20_carry_i_6
       (.I0(p_Val2_1_fu_1973_p2__20_carry_i_3_n_2),
        .I1(tmp_56_reg_2832[1]),
        .I2(tmp_54_reg_2827[1]),
        .I3(src_kernel_win_0_va_7_reg_2811[0]),
        .I4(tmp_53_reg_2822[1]),
        .I5(src_kernel_win_0_va_6_reg_2805[1]),
        .O(p_Val2_1_fu_1973_p2__20_carry_i_6_n_2));
  (* HLUTNM = "lutpair4" *) 
  LUT3 #(
    .INIT(8'h96)) 
    p_Val2_1_fu_1973_p2__20_carry_i_7
       (.I0(tmp_53_reg_2822[0]),
        .I1(tmp_56_reg_2832[0]),
        .I2(src_kernel_win_0_va_6_reg_2805[0]),
        .O(p_Val2_1_fu_1973_p2__20_carry_i_7_n_2));
  FDRE \p_Val2_1_reg_2922_reg[0] 
       (.C(ap_clk),
        .CE(isneg_1_reg_29320),
        .D(p_Val2_1_fu_1973_p2[0]),
        .Q(p_Val2_1_reg_2922[0]),
        .R(1'b0));
  FDRE \p_Val2_1_reg_2922_reg[1] 
       (.C(ap_clk),
        .CE(isneg_1_reg_29320),
        .D(p_Val2_1_fu_1973_p2[1]),
        .Q(p_Val2_1_reg_2922[1]),
        .R(1'b0));
  FDRE \p_Val2_1_reg_2922_reg[2] 
       (.C(ap_clk),
        .CE(isneg_1_reg_29320),
        .D(p_Val2_1_fu_1973_p2[2]),
        .Q(p_Val2_1_reg_2922[2]),
        .R(1'b0));
  FDRE \p_Val2_1_reg_2922_reg[3] 
       (.C(ap_clk),
        .CE(isneg_1_reg_29320),
        .D(p_Val2_1_fu_1973_p2[3]),
        .Q(p_Val2_1_reg_2922[3]),
        .R(1'b0));
  FDRE \p_Val2_1_reg_2922_reg[4] 
       (.C(ap_clk),
        .CE(isneg_1_reg_29320),
        .D(p_Val2_1_fu_1973_p2[4]),
        .Q(p_Val2_1_reg_2922[4]),
        .R(1'b0));
  FDRE \p_Val2_1_reg_2922_reg[5] 
       (.C(ap_clk),
        .CE(isneg_1_reg_29320),
        .D(p_Val2_1_fu_1973_p2[5]),
        .Q(p_Val2_1_reg_2922[5]),
        .R(1'b0));
  FDRE \p_Val2_1_reg_2922_reg[6] 
       (.C(ap_clk),
        .CE(isneg_1_reg_29320),
        .D(p_Val2_1_fu_1973_p2[6]),
        .Q(p_Val2_1_reg_2922[6]),
        .R(1'b0));
  FDRE \p_Val2_1_reg_2922_reg[7] 
       (.C(ap_clk),
        .CE(isneg_1_reg_29320),
        .D(p_Val2_1_fu_1973_p2[7]),
        .Q(p_Val2_1_reg_2922[7]),
        .R(1'b0));
  CARRY4 p_Val2_2_fu_1946_p2__27_carry
       (.CI(1'b0),
        .CO({p_Val2_2_fu_1946_p2__27_carry_n_2,p_Val2_2_fu_1946_p2__27_carry_n_3,p_Val2_2_fu_1946_p2__27_carry_n_4,p_Val2_2_fu_1946_p2__27_carry_n_5}),
        .CYINIT(1'b0),
        .DI({p_Val2_2_fu_1946_p2__27_carry_i_1_n_2,p_Val2_2_fu_1946_p2__27_carry_i_2_n_2,tmp57_reg_2837[1:0]}),
        .O(NLW_p_Val2_2_fu_1946_p2__27_carry_O_UNCONNECTED[3:0]),
        .S({p_Val2_2_fu_1946_p2__27_carry_i_3_n_2,p_Val2_2_fu_1946_p2__27_carry_i_4_n_2,p_Val2_2_fu_1946_p2__27_carry_i_5_n_2,p_Val2_2_fu_1946_p2__27_carry_i_6_n_2}));
  CARRY4 p_Val2_2_fu_1946_p2__27_carry__0
       (.CI(p_Val2_2_fu_1946_p2__27_carry_n_2),
        .CO({p_Val2_2_fu_1946_p2__27_carry__0_n_2,p_Val2_2_fu_1946_p2__27_carry__0_n_3,p_Val2_2_fu_1946_p2__27_carry__0_n_4,p_Val2_2_fu_1946_p2__27_carry__0_n_5}),
        .CYINIT(1'b0),
        .DI({p_Val2_2_fu_1946_p2__27_carry__0_i_1_n_2,p_Val2_2_fu_1946_p2__27_carry__0_i_2_n_2,p_Val2_2_fu_1946_p2__27_carry__0_i_3_n_2,p_Val2_2_fu_1946_p2__27_carry__0_i_4_n_2}),
        .O(NLW_p_Val2_2_fu_1946_p2__27_carry__0_O_UNCONNECTED[3:0]),
        .S({p_Val2_2_fu_1946_p2__27_carry__0_i_5_n_2,p_Val2_2_fu_1946_p2__27_carry__0_i_6_n_2,p_Val2_2_fu_1946_p2__27_carry__0_i_7_n_2,p_Val2_2_fu_1946_p2__27_carry__0_i_8_n_2}));
  LUT3 #(
    .INIT(8'hE8)) 
    p_Val2_2_fu_1946_p2__27_carry__0_i_1
       (.I0(tmp69_cast_fu_1936_p1[6]),
        .I1(src_kernel_win_0_va_7_reg_2811[5]),
        .I2(tmp57_reg_2837[6]),
        .O(p_Val2_2_fu_1946_p2__27_carry__0_i_1_n_2));
  LUT3 #(
    .INIT(8'hE8)) 
    p_Val2_2_fu_1946_p2__27_carry__0_i_2
       (.I0(tmp69_cast_fu_1936_p1[5]),
        .I1(src_kernel_win_0_va_7_reg_2811[4]),
        .I2(tmp57_reg_2837[5]),
        .O(p_Val2_2_fu_1946_p2__27_carry__0_i_2_n_2));
  LUT3 #(
    .INIT(8'hE8)) 
    p_Val2_2_fu_1946_p2__27_carry__0_i_3
       (.I0(tmp69_cast_fu_1936_p1[4]),
        .I1(src_kernel_win_0_va_7_reg_2811[3]),
        .I2(tmp57_reg_2837[4]),
        .O(p_Val2_2_fu_1946_p2__27_carry__0_i_3_n_2));
  (* HLUTNM = "lutpair1" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    p_Val2_2_fu_1946_p2__27_carry__0_i_4
       (.I0(tmp69_cast_fu_1936_p1[3]),
        .I1(src_kernel_win_0_va_7_reg_2811[2]),
        .I2(tmp57_reg_2837[3]),
        .O(p_Val2_2_fu_1946_p2__27_carry__0_i_4_n_2));
  (* HLUTNM = "lutpair2" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    p_Val2_2_fu_1946_p2__27_carry__0_i_5
       (.I0(tmp69_cast_fu_1936_p1[7]),
        .I1(src_kernel_win_0_va_7_reg_2811[6]),
        .I2(tmp57_reg_2837[7]),
        .I3(p_Val2_2_fu_1946_p2__27_carry__0_i_1_n_2),
        .O(p_Val2_2_fu_1946_p2__27_carry__0_i_5_n_2));
  LUT4 #(
    .INIT(16'h6996)) 
    p_Val2_2_fu_1946_p2__27_carry__0_i_6
       (.I0(tmp69_cast_fu_1936_p1[6]),
        .I1(src_kernel_win_0_va_7_reg_2811[5]),
        .I2(tmp57_reg_2837[6]),
        .I3(p_Val2_2_fu_1946_p2__27_carry__0_i_2_n_2),
        .O(p_Val2_2_fu_1946_p2__27_carry__0_i_6_n_2));
  LUT4 #(
    .INIT(16'h6996)) 
    p_Val2_2_fu_1946_p2__27_carry__0_i_7
       (.I0(tmp69_cast_fu_1936_p1[5]),
        .I1(src_kernel_win_0_va_7_reg_2811[4]),
        .I2(tmp57_reg_2837[5]),
        .I3(p_Val2_2_fu_1946_p2__27_carry__0_i_3_n_2),
        .O(p_Val2_2_fu_1946_p2__27_carry__0_i_7_n_2));
  LUT4 #(
    .INIT(16'h6996)) 
    p_Val2_2_fu_1946_p2__27_carry__0_i_8
       (.I0(tmp69_cast_fu_1936_p1[4]),
        .I1(src_kernel_win_0_va_7_reg_2811[3]),
        .I2(tmp57_reg_2837[4]),
        .I3(p_Val2_2_fu_1946_p2__27_carry__0_i_4_n_2),
        .O(p_Val2_2_fu_1946_p2__27_carry__0_i_8_n_2));
  CARRY4 p_Val2_2_fu_1946_p2__27_carry__1
       (.CI(p_Val2_2_fu_1946_p2__27_carry__0_n_2),
        .CO({NLW_p_Val2_2_fu_1946_p2__27_carry__1_CO_UNCONNECTED[3:2],p_Val2_2_fu_1946_p2__27_carry__1_n_4,p_Val2_2_fu_1946_p2__27_carry__1_n_5}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,p_Val2_2_fu_1946_p2__27_carry__1_i_1_n_2,p_Val2_2_fu_1946_p2__27_carry__1_i_2_n_2}),
        .O({NLW_p_Val2_2_fu_1946_p2__27_carry__1_O_UNCONNECTED[3],p_Val2_2_fu_1946_p2}),
        .S({1'b0,p_Val2_2_fu_1946_p2__27_carry__1_i_3_n_2,p_Val2_2_fu_1946_p2__27_carry__1_i_4_n_2,p_Val2_2_fu_1946_p2__27_carry__1_i_5_n_2}));
  LUT3 #(
    .INIT(8'hE8)) 
    p_Val2_2_fu_1946_p2__27_carry__1_i_1
       (.I0(tmp69_cast_fu_1936_p1[8]),
        .I1(src_kernel_win_0_va_7_reg_2811[7]),
        .I2(tmp57_reg_2837[8]),
        .O(p_Val2_2_fu_1946_p2__27_carry__1_i_1_n_2));
  (* HLUTNM = "lutpair2" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    p_Val2_2_fu_1946_p2__27_carry__1_i_2
       (.I0(tmp69_cast_fu_1936_p1[7]),
        .I1(src_kernel_win_0_va_7_reg_2811[6]),
        .I2(tmp57_reg_2837[7]),
        .O(p_Val2_2_fu_1946_p2__27_carry__1_i_2_n_2));
  LUT3 #(
    .INIT(8'hE1)) 
    p_Val2_2_fu_1946_p2__27_carry__1_i_3
       (.I0(p_Val2_2_fu_1946_p2_carry__1_n_4),
        .I1(tmp57_reg_2837[9]),
        .I2(tmp57_reg_2837[10]),
        .O(p_Val2_2_fu_1946_p2__27_carry__1_i_3_n_2));
  LUT5 #(
    .INIT(32'h17E8E817)) 
    p_Val2_2_fu_1946_p2__27_carry__1_i_4
       (.I0(tmp57_reg_2837[8]),
        .I1(src_kernel_win_0_va_7_reg_2811[7]),
        .I2(tmp69_cast_fu_1936_p1[8]),
        .I3(p_Val2_2_fu_1946_p2_carry__1_n_4),
        .I4(tmp57_reg_2837[9]),
        .O(p_Val2_2_fu_1946_p2__27_carry__1_i_4_n_2));
  LUT4 #(
    .INIT(16'h6996)) 
    p_Val2_2_fu_1946_p2__27_carry__1_i_5
       (.I0(p_Val2_2_fu_1946_p2__27_carry__1_i_2_n_2),
        .I1(src_kernel_win_0_va_7_reg_2811[7]),
        .I2(tmp69_cast_fu_1936_p1[8]),
        .I3(tmp57_reg_2837[8]),
        .O(p_Val2_2_fu_1946_p2__27_carry__1_i_5_n_2));
  (* HLUTNM = "lutpair0" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    p_Val2_2_fu_1946_p2__27_carry_i_1
       (.I0(tmp69_cast_fu_1936_p1[2]),
        .I1(src_kernel_win_0_va_7_reg_2811[1]),
        .I2(tmp57_reg_2837[2]),
        .O(p_Val2_2_fu_1946_p2__27_carry_i_1_n_2));
  LUT3 #(
    .INIT(8'h96)) 
    p_Val2_2_fu_1946_p2__27_carry_i_2
       (.I0(tmp57_reg_2837[2]),
        .I1(tmp69_cast_fu_1936_p1[2]),
        .I2(src_kernel_win_0_va_7_reg_2811[1]),
        .O(p_Val2_2_fu_1946_p2__27_carry_i_2_n_2));
  (* HLUTNM = "lutpair1" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    p_Val2_2_fu_1946_p2__27_carry_i_3
       (.I0(tmp69_cast_fu_1936_p1[3]),
        .I1(src_kernel_win_0_va_7_reg_2811[2]),
        .I2(tmp57_reg_2837[3]),
        .I3(p_Val2_2_fu_1946_p2__27_carry_i_1_n_2),
        .O(p_Val2_2_fu_1946_p2__27_carry_i_3_n_2));
  (* HLUTNM = "lutpair0" *) 
  LUT5 #(
    .INIT(32'h69969696)) 
    p_Val2_2_fu_1946_p2__27_carry_i_4
       (.I0(tmp69_cast_fu_1936_p1[2]),
        .I1(src_kernel_win_0_va_7_reg_2811[1]),
        .I2(tmp57_reg_2837[2]),
        .I3(src_kernel_win_0_va_7_reg_2811[0]),
        .I4(tmp69_cast_fu_1936_p1[1]),
        .O(p_Val2_2_fu_1946_p2__27_carry_i_4_n_2));
  LUT3 #(
    .INIT(8'h96)) 
    p_Val2_2_fu_1946_p2__27_carry_i_5
       (.I0(tmp69_cast_fu_1936_p1[1]),
        .I1(src_kernel_win_0_va_7_reg_2811[0]),
        .I2(tmp57_reg_2837[1]),
        .O(p_Val2_2_fu_1946_p2__27_carry_i_5_n_2));
  LUT2 #(
    .INIT(4'h6)) 
    p_Val2_2_fu_1946_p2__27_carry_i_6
       (.I0(tmp57_reg_2837[0]),
        .I1(tmp69_cast_fu_1936_p1[0]),
        .O(p_Val2_2_fu_1946_p2__27_carry_i_6_n_2));
  CARRY4 p_Val2_2_fu_1946_p2_carry
       (.CI(1'b0),
        .CO({p_Val2_2_fu_1946_p2_carry_n_2,p_Val2_2_fu_1946_p2_carry_n_3,p_Val2_2_fu_1946_p2_carry_n_4,p_Val2_2_fu_1946_p2_carry_n_5}),
        .CYINIT(1'b0),
        .DI(src_kernel_win_0_va_6_reg_2805[3:0]),
        .O(tmp69_cast_fu_1936_p1[3:0]),
        .S({p_Val2_2_fu_1946_p2_carry_i_1_n_2,p_Val2_2_fu_1946_p2_carry_i_2_n_2,p_Val2_2_fu_1946_p2_carry_i_3_n_2,p_Val2_2_fu_1946_p2_carry_i_4_n_2}));
  CARRY4 p_Val2_2_fu_1946_p2_carry__0
       (.CI(p_Val2_2_fu_1946_p2_carry_n_2),
        .CO({p_Val2_2_fu_1946_p2_carry__0_n_2,p_Val2_2_fu_1946_p2_carry__0_n_3,p_Val2_2_fu_1946_p2_carry__0_n_4,p_Val2_2_fu_1946_p2_carry__0_n_5}),
        .CYINIT(1'b0),
        .DI(src_kernel_win_0_va_6_reg_2805[7:4]),
        .O(tmp69_cast_fu_1936_p1[7:4]),
        .S({p_Val2_2_fu_1946_p2_carry__0_i_1_n_2,p_Val2_2_fu_1946_p2_carry__0_i_2_n_2,p_Val2_2_fu_1946_p2_carry__0_i_3_n_2,p_Val2_2_fu_1946_p2_carry__0_i_4_n_2}));
  LUT2 #(
    .INIT(4'h6)) 
    p_Val2_2_fu_1946_p2_carry__0_i_1
       (.I0(src_kernel_win_0_va_6_reg_2805[7]),
        .I1(tmp_53_reg_2822[7]),
        .O(p_Val2_2_fu_1946_p2_carry__0_i_1_n_2));
  LUT2 #(
    .INIT(4'h6)) 
    p_Val2_2_fu_1946_p2_carry__0_i_2
       (.I0(src_kernel_win_0_va_6_reg_2805[6]),
        .I1(tmp_53_reg_2822[6]),
        .O(p_Val2_2_fu_1946_p2_carry__0_i_2_n_2));
  LUT2 #(
    .INIT(4'h6)) 
    p_Val2_2_fu_1946_p2_carry__0_i_3
       (.I0(src_kernel_win_0_va_6_reg_2805[5]),
        .I1(tmp_53_reg_2822[5]),
        .O(p_Val2_2_fu_1946_p2_carry__0_i_3_n_2));
  LUT2 #(
    .INIT(4'h6)) 
    p_Val2_2_fu_1946_p2_carry__0_i_4
       (.I0(src_kernel_win_0_va_6_reg_2805[4]),
        .I1(tmp_53_reg_2822[4]),
        .O(p_Val2_2_fu_1946_p2_carry__0_i_4_n_2));
  CARRY4 p_Val2_2_fu_1946_p2_carry__1
       (.CI(p_Val2_2_fu_1946_p2_carry__0_n_2),
        .CO({NLW_p_Val2_2_fu_1946_p2_carry__1_CO_UNCONNECTED[3:2],p_Val2_2_fu_1946_p2_carry__1_n_4,NLW_p_Val2_2_fu_1946_p2_carry__1_CO_UNCONNECTED[0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({NLW_p_Val2_2_fu_1946_p2_carry__1_O_UNCONNECTED[3:1],tmp69_cast_fu_1936_p1[8]}),
        .S({1'b0,1'b0,1'b1,p_Val2_10_0_0_2_reg_2817}));
  LUT2 #(
    .INIT(4'h6)) 
    p_Val2_2_fu_1946_p2_carry_i_1
       (.I0(src_kernel_win_0_va_6_reg_2805[3]),
        .I1(tmp_53_reg_2822[3]),
        .O(p_Val2_2_fu_1946_p2_carry_i_1_n_2));
  LUT2 #(
    .INIT(4'h6)) 
    p_Val2_2_fu_1946_p2_carry_i_2
       (.I0(src_kernel_win_0_va_6_reg_2805[2]),
        .I1(tmp_53_reg_2822[2]),
        .O(p_Val2_2_fu_1946_p2_carry_i_2_n_2));
  LUT2 #(
    .INIT(4'h6)) 
    p_Val2_2_fu_1946_p2_carry_i_3
       (.I0(src_kernel_win_0_va_6_reg_2805[1]),
        .I1(tmp_53_reg_2822[1]),
        .O(p_Val2_2_fu_1946_p2_carry_i_3_n_2));
  LUT2 #(
    .INIT(4'h6)) 
    p_Val2_2_fu_1946_p2_carry_i_4
       (.I0(src_kernel_win_0_va_6_reg_2805[0]),
        .I1(tmp_53_reg_2822[0]),
        .O(p_Val2_2_fu_1946_p2_carry_i_4_n_2));
  LUT1 #(
    .INIT(2'h1)) 
    \p_assign_2_reg_2622[0]_i_1 
       (.I0(t_V_2_reg_577_reg__0__0),
        .O(\p_assign_2_reg_2622[0]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'h66566A666A666A66)) 
    \p_assign_2_reg_2622[10]_i_1 
       (.I0(t_V_2_reg_577_reg__0[10]),
        .I1(t_V_2_reg_577_reg__0[9]),
        .I2(t_V_2_reg_577_reg__0[8]),
        .I3(\p_assign_2_reg_2622[10]_i_2_n_2 ),
        .I4(\p_assign_2_reg_2622[10]_i_3_n_2 ),
        .I5(\p_p2_i_i_cast_cast_reg_2612[7]_i_1_n_2 ),
        .O(\p_assign_2_reg_2622[10]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair154" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \p_assign_2_reg_2622[10]_i_2 
       (.I0(t_V_2_reg_577_reg__0[7]),
        .I1(t_V_2_reg_577_reg__0[6]),
        .I2(\ImagLoc_x_reg_2592[10]_i_2_n_2 ),
        .O(\p_assign_2_reg_2622[10]_i_2_n_2 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \p_assign_2_reg_2622[10]_i_3 
       (.I0(\p_p2_i_i_cast_cast_reg_2612[6]_i_1_n_2 ),
        .I1(\p_assign_2_reg_2622[10]_i_4_n_2 ),
        .I2(\p_assign_2_reg_2622[10]_i_5_n_2 ),
        .I3(ImagLoc_x_fu_964_p2),
        .I4(\p_assign_2_reg_2622[10]_i_6_n_2 ),
        .I5(\p_assign_2_reg_2622[10]_i_7_n_2 ),
        .O(\p_assign_2_reg_2622[10]_i_3_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair129" *) 
  LUT5 #(
    .INIT(32'h55555556)) 
    \p_assign_2_reg_2622[10]_i_4 
       (.I0(t_V_2_reg_577_reg__0[4]),
        .I1(t_V_2_reg_577_reg__0[2]),
        .I2(t_V_2_reg_577_reg__0[1]),
        .I3(t_V_2_reg_577_reg__0__0),
        .I4(t_V_2_reg_577_reg__0[3]),
        .O(\p_assign_2_reg_2622[10]_i_4_n_2 ));
  LUT6 #(
    .INIT(64'hFEFEFEFEFEFFFEFE)) 
    \p_assign_2_reg_2622[10]_i_5 
       (.I0(t_V_2_reg_577_reg__0[2]),
        .I1(t_V_2_reg_577_reg__0[1]),
        .I2(t_V_2_reg_577_reg__0__0),
        .I3(\p_assign_2_reg_2622[10]_i_8_n_2 ),
        .I4(\exitcond389_i_reg_2583[0]_i_4_n_2 ),
        .I5(\ImagLoc_x_reg_2592[10]_i_2_n_2 ),
        .O(\p_assign_2_reg_2622[10]_i_5_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair121" *) 
  LUT4 #(
    .INIT(16'h5556)) 
    \p_assign_2_reg_2622[10]_i_6 
       (.I0(t_V_2_reg_577_reg__0[3]),
        .I1(t_V_2_reg_577_reg__0__0),
        .I2(t_V_2_reg_577_reg__0[1]),
        .I3(t_V_2_reg_577_reg__0[2]),
        .O(\p_assign_2_reg_2622[10]_i_6_n_2 ));
  LUT6 #(
    .INIT(64'h5555555555555556)) 
    \p_assign_2_reg_2622[10]_i_7 
       (.I0(t_V_2_reg_577_reg__0[5]),
        .I1(t_V_2_reg_577_reg__0[2]),
        .I2(t_V_2_reg_577_reg__0[1]),
        .I3(t_V_2_reg_577_reg__0__0),
        .I4(t_V_2_reg_577_reg__0[3]),
        .I5(t_V_2_reg_577_reg__0[4]),
        .O(\p_assign_2_reg_2622[10]_i_7_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair153" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \p_assign_2_reg_2622[10]_i_8 
       (.I0(t_V_2_reg_577_reg__0[10]),
        .I1(t_V_2_reg_577_reg__0[9]),
        .I2(t_V_2_reg_577_reg__0[8]),
        .O(\p_assign_2_reg_2622[10]_i_8_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair146" *) 
  LUT3 #(
    .INIT(8'h45)) 
    \p_assign_2_reg_2622[1]_i_1 
       (.I0(t_V_2_reg_577_reg__0[1]),
        .I1(t_V_2_reg_577_reg__0__0),
        .I2(ImagLoc_x_fu_964_p2),
        .O(p_assign_2_fu_1028_p2[1]));
  LUT1 #(
    .INIT(2'h1)) 
    \p_assign_2_reg_2622[2]_i_1 
       (.I0(t_V_2_reg_577_reg__0[2]),
        .O(p_assign_2_fu_1028_p2[2]));
  LUT1 #(
    .INIT(2'h1)) 
    \p_assign_2_reg_2622[3]_i_1 
       (.I0(t_V_2_reg_577_reg__0[3]),
        .O(p_assign_2_fu_1028_p2[3]));
  LUT1 #(
    .INIT(2'h1)) 
    \p_assign_2_reg_2622[4]_i_1 
       (.I0(t_V_2_reg_577_reg__0[4]),
        .O(p_assign_2_fu_1028_p2[4]));
  (* SOFT_HLUTNM = "soft_lutpair124" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \p_assign_2_reg_2622[5]_i_1 
       (.I0(t_V_2_reg_577_reg__0[5]),
        .O(p_assign_2_fu_1028_p2[5]));
  (* SOFT_HLUTNM = "soft_lutpair158" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \p_assign_2_reg_2622[6]_i_1 
       (.I0(t_V_2_reg_577_reg__0[6]),
        .O(p_assign_2_fu_1028_p2[6]));
  LUT1 #(
    .INIT(2'h1)) 
    \p_assign_2_reg_2622[7]_i_1 
       (.I0(t_V_2_reg_577_reg__0[7]),
        .O(p_assign_2_fu_1028_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair153" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \p_assign_2_reg_2622[8]_i_1 
       (.I0(t_V_2_reg_577_reg__0[8]),
        .O(p_assign_2_fu_1028_p2[8]));
  FDRE \p_assign_2_reg_2622_reg[0] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\p_assign_2_reg_2622[0]_i_1_n_2 ),
        .Q(p_assign_2_reg_2622[0]),
        .R(1'b0));
  FDRE \p_assign_2_reg_2622_reg[10] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\p_assign_2_reg_2622[10]_i_1_n_2 ),
        .Q(p_assign_2_reg_2622[10]),
        .R(1'b0));
  FDRE \p_assign_2_reg_2622_reg[1] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(p_assign_2_fu_1028_p2[1]),
        .Q(p_assign_2_reg_2622[1]),
        .R(1'b0));
  FDRE \p_assign_2_reg_2622_reg[2] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(p_assign_2_fu_1028_p2[2]),
        .Q(p_assign_2_reg_2622[2]),
        .R(1'b0));
  FDRE \p_assign_2_reg_2622_reg[3] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(p_assign_2_fu_1028_p2[3]),
        .Q(p_assign_2_reg_2622[3]),
        .R(1'b0));
  FDRE \p_assign_2_reg_2622_reg[4] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(p_assign_2_fu_1028_p2[4]),
        .Q(p_assign_2_reg_2622[4]),
        .R(1'b0));
  FDRE \p_assign_2_reg_2622_reg[5] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(p_assign_2_fu_1028_p2[5]),
        .Q(p_assign_2_reg_2622[5]),
        .R(1'b0));
  FDRE \p_assign_2_reg_2622_reg[6] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(p_assign_2_fu_1028_p2[6]),
        .Q(p_assign_2_reg_2622[6]),
        .R(1'b0));
  FDRE \p_assign_2_reg_2622_reg[7] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(p_assign_2_fu_1028_p2[7]),
        .Q(p_assign_2_reg_2622[7]),
        .R(1'b0));
  FDRE \p_assign_2_reg_2622_reg[8] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(p_assign_2_fu_1028_p2[8]),
        .Q(p_assign_2_reg_2622[8]),
        .R(1'b0));
  FDRE \p_assign_2_reg_2622_reg[9] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(t_V_2_reg_577_reg__0[9]),
        .Q(p_assign_2_reg_2622[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair120" *) 
  LUT4 #(
    .INIT(16'h5556)) 
    \p_assign_7_reg_2553[4]_i_1 
       (.I0(t_V_reg_566[4]),
        .I1(t_V_reg_566[1]),
        .I2(t_V_reg_566[2]),
        .I3(t_V_reg_566[3]),
        .O(\p_assign_7_reg_2553[4]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair120" *) 
  LUT5 #(
    .INIT(32'h55555556)) 
    \p_assign_7_reg_2553[5]_i_1 
       (.I0(t_V_reg_566[5]),
        .I1(t_V_reg_566[4]),
        .I2(t_V_reg_566[3]),
        .I3(t_V_reg_566[2]),
        .I4(t_V_reg_566[1]),
        .O(\p_assign_7_reg_2553[5]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'h5555555555555556)) 
    \p_assign_7_reg_2553[6]_i_1 
       (.I0(t_V_reg_566[6]),
        .I1(t_V_reg_566[5]),
        .I2(t_V_reg_566[4]),
        .I3(t_V_reg_566[3]),
        .I4(t_V_reg_566[2]),
        .I5(t_V_reg_566[1]),
        .O(\p_assign_7_reg_2553[6]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'h00000010FFFFFFEF)) 
    \p_assign_7_reg_2553[7]_i_1 
       (.I0(t_V_reg_566[5]),
        .I1(t_V_reg_566[4]),
        .I2(\tmp_4_reg_2535[6]_i_2_n_2 ),
        .I3(t_V_reg_566[1]),
        .I4(t_V_reg_566[6]),
        .I5(t_V_reg_566[7]),
        .O(p_assign_7_fu_723_p2));
  LUT6 #(
    .INIT(64'h5555555555565555)) 
    \p_assign_7_reg_2553[8]_i_1 
       (.I0(t_V_reg_566[8]),
        .I1(t_V_reg_566[7]),
        .I2(t_V_reg_566[5]),
        .I3(t_V_reg_566[4]),
        .I4(\p_assign_7_reg_2553[8]_i_2_n_2 ),
        .I5(t_V_reg_566[6]),
        .O(\p_assign_7_reg_2553[8]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair135" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \p_assign_7_reg_2553[8]_i_2 
       (.I0(t_V_reg_566[3]),
        .I1(t_V_reg_566[2]),
        .I2(t_V_reg_566[1]),
        .O(\p_assign_7_reg_2553[8]_i_2_n_2 ));
  LUT3 #(
    .INIT(8'h65)) 
    \p_assign_7_reg_2553[9]_i_1 
       (.I0(t_V_reg_566[9]),
        .I1(t_V_reg_566[8]),
        .I2(\p_assign_7_reg_2553[9]_i_2_n_2 ),
        .O(\p_assign_7_reg_2553[9]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \p_assign_7_reg_2553[9]_i_2 
       (.I0(t_V_reg_566[6]),
        .I1(t_V_reg_566[1]),
        .I2(\tmp_4_reg_2535[6]_i_2_n_2 ),
        .I3(t_V_reg_566[4]),
        .I4(t_V_reg_566[5]),
        .I5(t_V_reg_566[7]),
        .O(\p_assign_7_reg_2553[9]_i_2_n_2 ));
  FDRE \p_assign_7_reg_2553_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(t_V_reg_566[1]),
        .Q(p_assign_7_reg_2553[1]),
        .R(1'b0));
  FDRE \p_assign_7_reg_2553_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\p_assign_7_reg_2553[4]_i_1_n_2 ),
        .Q(p_assign_7_reg_2553[4]),
        .R(1'b0));
  FDRE \p_assign_7_reg_2553_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\p_assign_7_reg_2553[5]_i_1_n_2 ),
        .Q(p_assign_7_reg_2553[5]),
        .R(1'b0));
  FDRE \p_assign_7_reg_2553_reg[6] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\p_assign_7_reg_2553[6]_i_1_n_2 ),
        .Q(p_assign_7_reg_2553[6]),
        .R(1'b0));
  FDRE \p_assign_7_reg_2553_reg[7] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(p_assign_7_fu_723_p2),
        .Q(p_assign_7_reg_2553[7]),
        .R(1'b0));
  FDRE \p_assign_7_reg_2553_reg[8] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\p_assign_7_reg_2553[8]_i_1_n_2 ),
        .Q(p_assign_7_reg_2553[8]),
        .R(1'b0));
  FDRE \p_assign_7_reg_2553_reg[9] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\p_assign_7_reg_2553[9]_i_1_n_2 ),
        .Q(p_assign_7_reg_2553[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA8)) 
    \p_p2_i_i_cast_cast_reg_2612[10]_i_1 
       (.I0(t_V_2_reg_577_reg__0[10]),
        .I1(t_V_2_reg_577_reg__0[8]),
        .I2(t_V_2_reg_577_reg__0[9]),
        .I3(t_V_2_reg_577_reg__0[7]),
        .I4(t_V_2_reg_577_reg__0[6]),
        .I5(\ImagLoc_x_reg_2592[10]_i_2_n_2 ),
        .O(\p_p2_i_i_cast_cast_reg_2612[10]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair156" *) 
  LUT3 #(
    .INIT(8'h41)) 
    \p_p2_i_i_cast_cast_reg_2612[1]_i_1 
       (.I0(ImagLoc_x_fu_964_p2),
        .I1(t_V_2_reg_577_reg__0__0),
        .I2(t_V_2_reg_577_reg__0[1]),
        .O(\p_p2_i_i_cast_cast_reg_2612[1]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair140" *) 
  LUT4 #(
    .INIT(16'h00A9)) 
    \p_p2_i_i_cast_cast_reg_2612[2]_i_1 
       (.I0(t_V_2_reg_577_reg__0[2]),
        .I1(t_V_2_reg_577_reg__0[1]),
        .I2(t_V_2_reg_577_reg__0__0),
        .I3(ImagLoc_x_fu_964_p2),
        .O(\p_p2_i_i_cast_cast_reg_2612[2]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair121" *) 
  LUT5 #(
    .INIT(32'h0000AAA9)) 
    \p_p2_i_i_cast_cast_reg_2612[3]_i_1 
       (.I0(t_V_2_reg_577_reg__0[3]),
        .I1(t_V_2_reg_577_reg__0__0),
        .I2(t_V_2_reg_577_reg__0[1]),
        .I3(t_V_2_reg_577_reg__0[2]),
        .I4(ImagLoc_x_fu_964_p2),
        .O(\p_p2_i_i_cast_cast_reg_2612[3]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'h00000000AAAAAAA9)) 
    \p_p2_i_i_cast_cast_reg_2612[4]_i_1 
       (.I0(t_V_2_reg_577_reg__0[4]),
        .I1(t_V_2_reg_577_reg__0[2]),
        .I2(t_V_2_reg_577_reg__0[1]),
        .I3(t_V_2_reg_577_reg__0__0),
        .I4(t_V_2_reg_577_reg__0[3]),
        .I5(ImagLoc_x_fu_964_p2),
        .O(\p_p2_i_i_cast_cast_reg_2612[4]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair122" *) 
  LUT5 #(
    .INIT(32'h0000AAA9)) 
    \p_p2_i_i_cast_cast_reg_2612[5]_i_1 
       (.I0(t_V_2_reg_577_reg__0[5]),
        .I1(\p_p2_i_i_cast_cast_reg_2612[5]_i_2_n_2 ),
        .I2(t_V_2_reg_577_reg__0[3]),
        .I3(t_V_2_reg_577_reg__0[4]),
        .I4(ImagLoc_x_fu_964_p2),
        .O(\p_p2_i_i_cast_cast_reg_2612[5]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair139" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \p_p2_i_i_cast_cast_reg_2612[5]_i_2 
       (.I0(t_V_2_reg_577_reg__0[2]),
        .I1(t_V_2_reg_577_reg__0[1]),
        .I2(t_V_2_reg_577_reg__0__0),
        .O(\p_p2_i_i_cast_cast_reg_2612[5]_i_2_n_2 ));
  LUT6 #(
    .INIT(64'hFFFF00000000FFFE)) 
    \p_p2_i_i_cast_cast_reg_2612[6]_i_1 
       (.I0(t_V_2_reg_577_reg__0[10]),
        .I1(t_V_2_reg_577_reg__0[9]),
        .I2(t_V_2_reg_577_reg__0[8]),
        .I3(t_V_2_reg_577_reg__0[7]),
        .I4(t_V_2_reg_577_reg__0[6]),
        .I5(\ImagLoc_x_reg_2592[10]_i_2_n_2 ),
        .O(\p_p2_i_i_cast_cast_reg_2612[6]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'hFFFFFF00000000FE)) 
    \p_p2_i_i_cast_cast_reg_2612[7]_i_1 
       (.I0(t_V_2_reg_577_reg__0[10]),
        .I1(t_V_2_reg_577_reg__0[9]),
        .I2(t_V_2_reg_577_reg__0[8]),
        .I3(t_V_2_reg_577_reg__0[6]),
        .I4(\ImagLoc_x_reg_2592[10]_i_2_n_2 ),
        .I5(t_V_2_reg_577_reg__0[7]),
        .O(\p_p2_i_i_cast_cast_reg_2612[7]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'hF0F0F0F0F0F0F00E)) 
    \p_p2_i_i_cast_cast_reg_2612[8]_i_1 
       (.I0(t_V_2_reg_577_reg__0[10]),
        .I1(t_V_2_reg_577_reg__0[9]),
        .I2(t_V_2_reg_577_reg__0[8]),
        .I3(t_V_2_reg_577_reg__0[7]),
        .I4(t_V_2_reg_577_reg__0[6]),
        .I5(\ImagLoc_x_reg_2592[10]_i_2_n_2 ),
        .O(\p_p2_i_i_cast_cast_reg_2612[8]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'hCCCCCCCCCCCCCCC2)) 
    \p_p2_i_i_cast_cast_reg_2612[9]_i_1 
       (.I0(t_V_2_reg_577_reg__0[10]),
        .I1(t_V_2_reg_577_reg__0[9]),
        .I2(t_V_2_reg_577_reg__0[8]),
        .I3(t_V_2_reg_577_reg__0[7]),
        .I4(t_V_2_reg_577_reg__0[6]),
        .I5(\ImagLoc_x_reg_2592[10]_i_2_n_2 ),
        .O(\p_p2_i_i_cast_cast_reg_2612[9]_i_1_n_2 ));
  FDRE \p_p2_i_i_cast_cast_reg_2612_reg[10] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\p_p2_i_i_cast_cast_reg_2612[10]_i_1_n_2 ),
        .Q(p_p2_i_i_cast_cast_reg_2612_reg[10]),
        .R(1'b0));
  FDRE \p_p2_i_i_cast_cast_reg_2612_reg[1] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\p_p2_i_i_cast_cast_reg_2612[1]_i_1_n_2 ),
        .Q(p_p2_i_i_cast_cast_reg_2612_reg[1]),
        .R(1'b0));
  FDRE \p_p2_i_i_cast_cast_reg_2612_reg[2] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\p_p2_i_i_cast_cast_reg_2612[2]_i_1_n_2 ),
        .Q(p_p2_i_i_cast_cast_reg_2612_reg[2]),
        .R(1'b0));
  FDRE \p_p2_i_i_cast_cast_reg_2612_reg[3] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\p_p2_i_i_cast_cast_reg_2612[3]_i_1_n_2 ),
        .Q(p_p2_i_i_cast_cast_reg_2612_reg[3]),
        .R(1'b0));
  FDRE \p_p2_i_i_cast_cast_reg_2612_reg[4] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\p_p2_i_i_cast_cast_reg_2612[4]_i_1_n_2 ),
        .Q(p_p2_i_i_cast_cast_reg_2612_reg[4]),
        .R(1'b0));
  FDRE \p_p2_i_i_cast_cast_reg_2612_reg[5] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\p_p2_i_i_cast_cast_reg_2612[5]_i_1_n_2 ),
        .Q(p_p2_i_i_cast_cast_reg_2612_reg[5]),
        .R(1'b0));
  FDRE \p_p2_i_i_cast_cast_reg_2612_reg[6] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\p_p2_i_i_cast_cast_reg_2612[6]_i_1_n_2 ),
        .Q(p_p2_i_i_cast_cast_reg_2612_reg[6]),
        .R(1'b0));
  FDRE \p_p2_i_i_cast_cast_reg_2612_reg[7] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\p_p2_i_i_cast_cast_reg_2612[7]_i_1_n_2 ),
        .Q(p_p2_i_i_cast_cast_reg_2612_reg[7]),
        .R(1'b0));
  FDRE \p_p2_i_i_cast_cast_reg_2612_reg[8] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\p_p2_i_i_cast_cast_reg_2612[8]_i_1_n_2 ),
        .Q(p_p2_i_i_cast_cast_reg_2612_reg[8]),
        .R(1'b0));
  FDRE \p_p2_i_i_cast_cast_reg_2612_reg[9] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(\p_p2_i_i_cast_cast_reg_2612[9]_i_1_n_2 ),
        .Q(p_p2_i_i_cast_cast_reg_2612_reg[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h8000)) 
    \reg_588[7]_i_1 
       (.I0(k_buf_0_val_3_U_n_13),
        .I1(ap_enable_reg_pp0_iter1),
        .I2(ap_CS_fsm_pp0_stage0),
        .I3(ap_block_pp0_stage0_subdone2_in),
        .O(E));
  FDRE \reg_588_reg[0] 
       (.C(ap_clk),
        .CE(E),
        .D(\SRL_SIG_reg[0][7]_1 [0]),
        .Q(reg_588[0]),
        .R(1'b0));
  FDRE \reg_588_reg[1] 
       (.C(ap_clk),
        .CE(E),
        .D(\SRL_SIG_reg[0][7]_1 [1]),
        .Q(reg_588[1]),
        .R(1'b0));
  FDRE \reg_588_reg[2] 
       (.C(ap_clk),
        .CE(E),
        .D(\SRL_SIG_reg[0][7]_1 [2]),
        .Q(reg_588[2]),
        .R(1'b0));
  FDRE \reg_588_reg[3] 
       (.C(ap_clk),
        .CE(E),
        .D(\SRL_SIG_reg[0][7]_1 [3]),
        .Q(reg_588[3]),
        .R(1'b0));
  FDRE \reg_588_reg[4] 
       (.C(ap_clk),
        .CE(E),
        .D(\SRL_SIG_reg[0][7]_1 [4]),
        .Q(reg_588[4]),
        .R(1'b0));
  FDRE \reg_588_reg[5] 
       (.C(ap_clk),
        .CE(E),
        .D(\SRL_SIG_reg[0][7]_1 [5]),
        .Q(reg_588[5]),
        .R(1'b0));
  FDRE \reg_588_reg[6] 
       (.C(ap_clk),
        .CE(E),
        .D(\SRL_SIG_reg[0][7]_1 [6]),
        .Q(reg_588[6]),
        .R(1'b0));
  FDRE \reg_588_reg[7] 
       (.C(ap_clk),
        .CE(E),
        .D(\SRL_SIG_reg[0][7]_1 [7]),
        .Q(reg_588[7]),
        .R(1'b0));
  FDRE \right_border_buf_0_1_fu_322_reg[0] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_s_fu_318[0]),
        .Q(right_border_buf_0_1_fu_322[0]),
        .R(1'b0));
  FDRE \right_border_buf_0_1_fu_322_reg[1] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_s_fu_318[1]),
        .Q(right_border_buf_0_1_fu_322[1]),
        .R(1'b0));
  FDRE \right_border_buf_0_1_fu_322_reg[2] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_s_fu_318[2]),
        .Q(right_border_buf_0_1_fu_322[2]),
        .R(1'b0));
  FDRE \right_border_buf_0_1_fu_322_reg[3] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_s_fu_318[3]),
        .Q(right_border_buf_0_1_fu_322[3]),
        .R(1'b0));
  FDRE \right_border_buf_0_1_fu_322_reg[4] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_s_fu_318[4]),
        .Q(right_border_buf_0_1_fu_322[4]),
        .R(1'b0));
  FDRE \right_border_buf_0_1_fu_322_reg[5] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_s_fu_318[5]),
        .Q(right_border_buf_0_1_fu_322[5]),
        .R(1'b0));
  FDRE \right_border_buf_0_1_fu_322_reg[6] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_s_fu_318[6]),
        .Q(right_border_buf_0_1_fu_322[6]),
        .R(1'b0));
  FDRE \right_border_buf_0_1_fu_322_reg[7] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_s_fu_318[7]),
        .Q(right_border_buf_0_1_fu_322[7]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_2_fu_330[0]_i_1 
       (.I0(k_buf_0_val_4_q0[0]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_3_fu_334[0]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_2_fu_330[0]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_1_0_fu_1159_p3[0]));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_2_fu_330[1]_i_1 
       (.I0(k_buf_0_val_4_q0[1]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_3_fu_334[1]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_2_fu_330[1]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_1_0_fu_1159_p3[1]));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_2_fu_330[2]_i_1 
       (.I0(k_buf_0_val_4_q0[2]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_3_fu_334[2]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_2_fu_330[2]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_1_0_fu_1159_p3[2]));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_2_fu_330[3]_i_1 
       (.I0(k_buf_0_val_4_q0[3]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_3_fu_334[3]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_2_fu_330[3]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_1_0_fu_1159_p3[3]));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_2_fu_330[4]_i_1 
       (.I0(k_buf_0_val_4_q0[4]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_3_fu_334[4]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_2_fu_330[4]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_1_0_fu_1159_p3[4]));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_2_fu_330[5]_i_1 
       (.I0(k_buf_0_val_4_q0[5]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_3_fu_334[5]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_2_fu_330[5]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_1_0_fu_1159_p3[5]));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_2_fu_330[6]_i_1 
       (.I0(k_buf_0_val_4_q0[6]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_3_fu_334[6]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_2_fu_330[6]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_1_0_fu_1159_p3[6]));
  LUT5 #(
    .INIT(32'h80000000)) 
    \right_border_buf_0_2_fu_330[7]_i_1 
       (.I0(tmp_1_reg_2500),
        .I1(\icmp_reg_2509_reg_n_2_[0] ),
        .I2(ap_enable_reg_pp0_iter2),
        .I3(col_buf_0_val_0_0_reg_27080),
        .I4(ap_reg_pp0_iter1_or_cond_i_i_reg_2607),
        .O(ce119_out));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_2_fu_330[7]_i_2 
       (.I0(k_buf_0_val_4_q0[7]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_3_fu_334[7]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_2_fu_330[7]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_1_0_fu_1159_p3[7]));
  FDRE \right_border_buf_0_2_fu_330_reg[0] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_1_0_fu_1159_p3[0]),
        .Q(right_border_buf_0_2_fu_330[0]),
        .R(1'b0));
  FDRE \right_border_buf_0_2_fu_330_reg[1] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_1_0_fu_1159_p3[1]),
        .Q(right_border_buf_0_2_fu_330[1]),
        .R(1'b0));
  FDRE \right_border_buf_0_2_fu_330_reg[2] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_1_0_fu_1159_p3[2]),
        .Q(right_border_buf_0_2_fu_330[2]),
        .R(1'b0));
  FDRE \right_border_buf_0_2_fu_330_reg[3] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_1_0_fu_1159_p3[3]),
        .Q(right_border_buf_0_2_fu_330[3]),
        .R(1'b0));
  FDRE \right_border_buf_0_2_fu_330_reg[4] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_1_0_fu_1159_p3[4]),
        .Q(right_border_buf_0_2_fu_330[4]),
        .R(1'b0));
  FDRE \right_border_buf_0_2_fu_330_reg[5] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_1_0_fu_1159_p3[5]),
        .Q(right_border_buf_0_2_fu_330[5]),
        .R(1'b0));
  FDRE \right_border_buf_0_2_fu_330_reg[6] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_1_0_fu_1159_p3[6]),
        .Q(right_border_buf_0_2_fu_330[6]),
        .R(1'b0));
  FDRE \right_border_buf_0_2_fu_330_reg[7] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_1_0_fu_1159_p3[7]),
        .Q(right_border_buf_0_2_fu_330[7]),
        .R(1'b0));
  FDRE \right_border_buf_0_3_fu_334_reg[0] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_2_fu_330[0]),
        .Q(right_border_buf_0_3_fu_334[0]),
        .R(1'b0));
  FDRE \right_border_buf_0_3_fu_334_reg[1] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_2_fu_330[1]),
        .Q(right_border_buf_0_3_fu_334[1]),
        .R(1'b0));
  FDRE \right_border_buf_0_3_fu_334_reg[2] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_2_fu_330[2]),
        .Q(right_border_buf_0_3_fu_334[2]),
        .R(1'b0));
  FDRE \right_border_buf_0_3_fu_334_reg[3] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_2_fu_330[3]),
        .Q(right_border_buf_0_3_fu_334[3]),
        .R(1'b0));
  FDRE \right_border_buf_0_3_fu_334_reg[4] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_2_fu_330[4]),
        .Q(right_border_buf_0_3_fu_334[4]),
        .R(1'b0));
  FDRE \right_border_buf_0_3_fu_334_reg[5] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_2_fu_330[5]),
        .Q(right_border_buf_0_3_fu_334[5]),
        .R(1'b0));
  FDRE \right_border_buf_0_3_fu_334_reg[6] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_2_fu_330[6]),
        .Q(right_border_buf_0_3_fu_334[6]),
        .R(1'b0));
  FDRE \right_border_buf_0_3_fu_334_reg[7] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_2_fu_330[7]),
        .Q(right_border_buf_0_3_fu_334[7]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_4_fu_342[0]_i_1 
       (.I0(k_buf_0_val_5_q0[0]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_5_fu_346[0]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_4_fu_342[0]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_2_0_fu_1178_p3[0]));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_4_fu_342[1]_i_1 
       (.I0(k_buf_0_val_5_q0[1]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_5_fu_346[1]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_4_fu_342[1]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_2_0_fu_1178_p3[1]));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_4_fu_342[2]_i_1 
       (.I0(k_buf_0_val_5_q0[2]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_5_fu_346[2]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_4_fu_342[2]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_2_0_fu_1178_p3[2]));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_4_fu_342[3]_i_1 
       (.I0(k_buf_0_val_5_q0[3]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_5_fu_346[3]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_4_fu_342[3]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_2_0_fu_1178_p3[3]));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_4_fu_342[4]_i_1 
       (.I0(k_buf_0_val_5_q0[4]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_5_fu_346[4]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_4_fu_342[4]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_2_0_fu_1178_p3[4]));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_4_fu_342[5]_i_1 
       (.I0(k_buf_0_val_5_q0[5]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_5_fu_346[5]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_4_fu_342[5]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_2_0_fu_1178_p3[5]));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_4_fu_342[6]_i_1 
       (.I0(k_buf_0_val_5_q0[6]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_5_fu_346[6]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_4_fu_342[6]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_2_0_fu_1178_p3[6]));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_4_fu_342[7]_i_1 
       (.I0(k_buf_0_val_5_q0[7]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_5_fu_346[7]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_4_fu_342[7]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_2_0_fu_1178_p3[7]));
  FDRE \right_border_buf_0_4_fu_342_reg[0] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_2_0_fu_1178_p3[0]),
        .Q(right_border_buf_0_4_fu_342[0]),
        .R(1'b0));
  FDRE \right_border_buf_0_4_fu_342_reg[1] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_2_0_fu_1178_p3[1]),
        .Q(right_border_buf_0_4_fu_342[1]),
        .R(1'b0));
  FDRE \right_border_buf_0_4_fu_342_reg[2] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_2_0_fu_1178_p3[2]),
        .Q(right_border_buf_0_4_fu_342[2]),
        .R(1'b0));
  FDRE \right_border_buf_0_4_fu_342_reg[3] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_2_0_fu_1178_p3[3]),
        .Q(right_border_buf_0_4_fu_342[3]),
        .R(1'b0));
  FDRE \right_border_buf_0_4_fu_342_reg[4] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_2_0_fu_1178_p3[4]),
        .Q(right_border_buf_0_4_fu_342[4]),
        .R(1'b0));
  FDRE \right_border_buf_0_4_fu_342_reg[5] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_2_0_fu_1178_p3[5]),
        .Q(right_border_buf_0_4_fu_342[5]),
        .R(1'b0));
  FDRE \right_border_buf_0_4_fu_342_reg[6] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_2_0_fu_1178_p3[6]),
        .Q(right_border_buf_0_4_fu_342[6]),
        .R(1'b0));
  FDRE \right_border_buf_0_4_fu_342_reg[7] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_2_0_fu_1178_p3[7]),
        .Q(right_border_buf_0_4_fu_342[7]),
        .R(1'b0));
  FDRE \right_border_buf_0_5_fu_346_reg[0] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_4_fu_342[0]),
        .Q(right_border_buf_0_5_fu_346[0]),
        .R(1'b0));
  FDRE \right_border_buf_0_5_fu_346_reg[1] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_4_fu_342[1]),
        .Q(right_border_buf_0_5_fu_346[1]),
        .R(1'b0));
  FDRE \right_border_buf_0_5_fu_346_reg[2] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_4_fu_342[2]),
        .Q(right_border_buf_0_5_fu_346[2]),
        .R(1'b0));
  FDRE \right_border_buf_0_5_fu_346_reg[3] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_4_fu_342[3]),
        .Q(right_border_buf_0_5_fu_346[3]),
        .R(1'b0));
  FDRE \right_border_buf_0_5_fu_346_reg[4] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_4_fu_342[4]),
        .Q(right_border_buf_0_5_fu_346[4]),
        .R(1'b0));
  FDRE \right_border_buf_0_5_fu_346_reg[5] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_4_fu_342[5]),
        .Q(right_border_buf_0_5_fu_346[5]),
        .R(1'b0));
  FDRE \right_border_buf_0_5_fu_346_reg[6] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_4_fu_342[6]),
        .Q(right_border_buf_0_5_fu_346[6]),
        .R(1'b0));
  FDRE \right_border_buf_0_5_fu_346_reg[7] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(right_border_buf_0_4_fu_342[7]),
        .Q(right_border_buf_0_5_fu_346[7]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_s_fu_318[0]_i_1 
       (.I0(k_buf_0_val_3_q0[0]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_1_fu_322[0]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_s_fu_318[0]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_0_0_fu_1140_p3[0]));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_s_fu_318[1]_i_1 
       (.I0(k_buf_0_val_3_q0[1]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_1_fu_322[1]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_s_fu_318[1]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_0_0_fu_1140_p3[1]));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_s_fu_318[2]_i_1 
       (.I0(k_buf_0_val_3_q0[2]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_1_fu_322[2]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_s_fu_318[2]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_0_0_fu_1140_p3[2]));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_s_fu_318[3]_i_1 
       (.I0(k_buf_0_val_3_q0[3]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_1_fu_322[3]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_s_fu_318[3]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_0_0_fu_1140_p3[3]));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_s_fu_318[4]_i_1 
       (.I0(k_buf_0_val_3_q0[4]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_1_fu_322[4]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_s_fu_318[4]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_0_0_fu_1140_p3[4]));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_s_fu_318[5]_i_1 
       (.I0(k_buf_0_val_3_q0[5]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_1_fu_322[5]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_s_fu_318[5]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_0_0_fu_1140_p3[5]));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_s_fu_318[6]_i_1 
       (.I0(k_buf_0_val_3_q0[6]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_1_fu_322[6]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_s_fu_318[6]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_0_0_fu_1140_p3[6]));
  LUT6 #(
    .INIT(64'hBBB888B888888888)) 
    \right_border_buf_0_s_fu_318[7]_i_1 
       (.I0(k_buf_0_val_3_q0[7]),
        .I1(ap_reg_pp0_iter1_brmerge_reg_2627),
        .I2(right_border_buf_0_1_fu_322[7]),
        .I3(tmp_49_reg_2644[0]),
        .I4(right_border_buf_0_s_fu_318[7]),
        .I5(tmp_49_reg_2644[1]),
        .O(col_buf_0_val_0_0_fu_1140_p3[7]));
  FDRE \right_border_buf_0_s_fu_318_reg[0] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_0_0_fu_1140_p3[0]),
        .Q(right_border_buf_0_s_fu_318[0]),
        .R(1'b0));
  FDRE \right_border_buf_0_s_fu_318_reg[1] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_0_0_fu_1140_p3[1]),
        .Q(right_border_buf_0_s_fu_318[1]),
        .R(1'b0));
  FDRE \right_border_buf_0_s_fu_318_reg[2] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_0_0_fu_1140_p3[2]),
        .Q(right_border_buf_0_s_fu_318[2]),
        .R(1'b0));
  FDRE \right_border_buf_0_s_fu_318_reg[3] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_0_0_fu_1140_p3[3]),
        .Q(right_border_buf_0_s_fu_318[3]),
        .R(1'b0));
  FDRE \right_border_buf_0_s_fu_318_reg[4] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_0_0_fu_1140_p3[4]),
        .Q(right_border_buf_0_s_fu_318[4]),
        .R(1'b0));
  FDRE \right_border_buf_0_s_fu_318_reg[5] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_0_0_fu_1140_p3[5]),
        .Q(right_border_buf_0_s_fu_318[5]),
        .R(1'b0));
  FDRE \right_border_buf_0_s_fu_318_reg[6] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_0_0_fu_1140_p3[6]),
        .Q(right_border_buf_0_s_fu_318[6]),
        .R(1'b0));
  FDRE \right_border_buf_0_s_fu_318_reg[7] 
       (.C(ap_clk),
        .CE(ce119_out),
        .D(col_buf_0_val_0_0_fu_1140_p3[7]),
        .Q(right_border_buf_0_s_fu_318[7]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair152" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    \row_assign_9_0_1_t_reg_2558[1]_i_1 
       (.I0(t_V_reg_566[1]),
        .I1(t_V_reg_566[0]),
        .I2(\icmp_reg_2509[0]_i_2_n_2 ),
        .O(row_assign_9_0_1_t_fu_825_p2));
  FDRE \row_assign_9_0_1_t_reg_2558_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(i_V_fu_631_p2[0]),
        .Q(row_assign_9_0_1_t_reg_2558[0]),
        .R(1'b0));
  FDRE \row_assign_9_0_1_t_reg_2558_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(row_assign_9_0_1_t_fu_825_p2),
        .Q(row_assign_9_0_1_t_reg_2558[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair155" *) 
  LUT3 #(
    .INIT(8'hE1)) 
    \row_assign_9_0_2_t_reg_2565[1]_i_1 
       (.I0(\icmp_reg_2509[0]_i_2_n_2 ),
        .I1(t_V_reg_566[0]),
        .I2(t_V_reg_566[1]),
        .O(row_assign_9_0_2_t_fu_839_p2));
  FDRE \row_assign_9_0_2_t_reg_2565_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(t_V_reg_566[0]),
        .Q(row_assign_9_0_2_t_reg_2565[0]),
        .R(1'b0));
  FDRE \row_assign_9_0_2_t_reg_2565_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(row_assign_9_0_2_t_fu_839_p2),
        .Q(row_assign_9_0_2_t_reg_2565[1]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_1_fu_250_reg[0] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_fu_246[0]),
        .Q(src_kernel_win_0_va_1_fu_250[0]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_1_fu_250_reg[1] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_fu_246[1]),
        .Q(src_kernel_win_0_va_1_fu_250[1]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_1_fu_250_reg[2] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_fu_246[2]),
        .Q(src_kernel_win_0_va_1_fu_250[2]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_1_fu_250_reg[3] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_fu_246[3]),
        .Q(src_kernel_win_0_va_1_fu_250[3]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_1_fu_250_reg[4] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_fu_246[4]),
        .Q(src_kernel_win_0_va_1_fu_250[4]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_1_fu_250_reg[5] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_fu_246[5]),
        .Q(src_kernel_win_0_va_1_fu_250[5]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_1_fu_250_reg[6] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_fu_246[6]),
        .Q(src_kernel_win_0_va_1_fu_250[6]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_1_fu_250_reg[7] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_fu_246[7]),
        .Q(src_kernel_win_0_va_1_fu_250[7]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_2_fu_254_reg[0] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_7_fu_1456_p3[0]),
        .Q(src_kernel_win_0_va_2_fu_254[0]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_2_fu_254_reg[1] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_7_fu_1456_p3[1]),
        .Q(src_kernel_win_0_va_2_fu_254[1]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_2_fu_254_reg[2] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_7_fu_1456_p3[2]),
        .Q(src_kernel_win_0_va_2_fu_254[2]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_2_fu_254_reg[3] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_7_fu_1456_p3[3]),
        .Q(src_kernel_win_0_va_2_fu_254[3]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_2_fu_254_reg[4] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_7_fu_1456_p3[4]),
        .Q(src_kernel_win_0_va_2_fu_254[4]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_2_fu_254_reg[5] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_7_fu_1456_p3[5]),
        .Q(src_kernel_win_0_va_2_fu_254[5]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_2_fu_254_reg[6] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_7_fu_1456_p3[6]),
        .Q(src_kernel_win_0_va_2_fu_254[6]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_2_fu_254_reg[7] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_7_fu_1456_p3[7]),
        .Q(src_kernel_win_0_va_2_fu_254[7]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_3_fu_258_reg[0] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_2_fu_254[0]),
        .Q(src_kernel_win_0_va_3_fu_258[0]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_3_fu_258_reg[1] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_2_fu_254[1]),
        .Q(src_kernel_win_0_va_3_fu_258[1]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_3_fu_258_reg[2] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_2_fu_254[2]),
        .Q(src_kernel_win_0_va_3_fu_258[2]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_3_fu_258_reg[3] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_2_fu_254[3]),
        .Q(src_kernel_win_0_va_3_fu_258[3]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_3_fu_258_reg[4] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_2_fu_254[4]),
        .Q(src_kernel_win_0_va_3_fu_258[4]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_3_fu_258_reg[5] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_2_fu_254[5]),
        .Q(src_kernel_win_0_va_3_fu_258[5]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_3_fu_258_reg[6] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_2_fu_254[6]),
        .Q(src_kernel_win_0_va_3_fu_258[6]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_3_fu_258_reg[7] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_2_fu_254[7]),
        .Q(src_kernel_win_0_va_3_fu_258[7]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFB8FF0000B800)) 
    \src_kernel_win_0_va_4_fu_262[0]_i_1 
       (.I0(col_buf_0_val_1_0_reg_2721[0]),
        .I1(row_assign_9_0_2_t_reg_2565[0]),
        .I2(col_buf_0_val_0_0_reg_2708[0]),
        .I3(tmp_3_reg_2522),
        .I4(row_assign_9_0_2_t_reg_2565[1]),
        .I5(col_buf_0_val_2_0_reg_2729[0]),
        .O(tmp_160_0_0_2_cast_fu_1489_p1[0]));
  LUT6 #(
    .INIT(64'hFFFFB8FF0000B800)) 
    \src_kernel_win_0_va_4_fu_262[1]_i_1 
       (.I0(col_buf_0_val_1_0_reg_2721[1]),
        .I1(row_assign_9_0_2_t_reg_2565[0]),
        .I2(col_buf_0_val_0_0_reg_2708[1]),
        .I3(tmp_3_reg_2522),
        .I4(row_assign_9_0_2_t_reg_2565[1]),
        .I5(col_buf_0_val_2_0_reg_2729[1]),
        .O(tmp_160_0_0_2_cast_fu_1489_p1[1]));
  LUT6 #(
    .INIT(64'hFFFFB8FF0000B800)) 
    \src_kernel_win_0_va_4_fu_262[2]_i_1 
       (.I0(col_buf_0_val_1_0_reg_2721[2]),
        .I1(row_assign_9_0_2_t_reg_2565[0]),
        .I2(col_buf_0_val_0_0_reg_2708[2]),
        .I3(tmp_3_reg_2522),
        .I4(row_assign_9_0_2_t_reg_2565[1]),
        .I5(col_buf_0_val_2_0_reg_2729[2]),
        .O(tmp_160_0_0_2_cast_fu_1489_p1[2]));
  LUT6 #(
    .INIT(64'hBABABA8A8A8ABA8A)) 
    \src_kernel_win_0_va_4_fu_262[3]_i_1 
       (.I0(col_buf_0_val_2_0_reg_2729[3]),
        .I1(row_assign_9_0_2_t_reg_2565[1]),
        .I2(tmp_3_reg_2522),
        .I3(col_buf_0_val_0_0_reg_2708[3]),
        .I4(row_assign_9_0_2_t_reg_2565[0]),
        .I5(col_buf_0_val_1_0_reg_2721[3]),
        .O(tmp_160_0_0_2_cast_fu_1489_p1[3]));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \src_kernel_win_0_va_4_fu_262[4]_i_1 
       (.I0(col_buf_0_val_2_0_reg_2729[4]),
        .I1(row_assign_9_0_2_t_reg_2565[1]),
        .I2(tmp_3_reg_2522),
        .I3(col_buf_0_val_1_0_reg_2721[4]),
        .I4(row_assign_9_0_2_t_reg_2565[0]),
        .I5(col_buf_0_val_0_0_reg_2708[4]),
        .O(tmp_160_0_0_2_cast_fu_1489_p1[4]));
  LUT6 #(
    .INIT(64'hFFFFB8FF0000B800)) 
    \src_kernel_win_0_va_4_fu_262[5]_i_1 
       (.I0(col_buf_0_val_1_0_reg_2721[5]),
        .I1(row_assign_9_0_2_t_reg_2565[0]),
        .I2(col_buf_0_val_0_0_reg_2708[5]),
        .I3(tmp_3_reg_2522),
        .I4(row_assign_9_0_2_t_reg_2565[1]),
        .I5(col_buf_0_val_2_0_reg_2729[5]),
        .O(tmp_160_0_0_2_cast_fu_1489_p1[5]));
  LUT6 #(
    .INIT(64'hBA8ABABABA8A8A8A)) 
    \src_kernel_win_0_va_4_fu_262[6]_i_1 
       (.I0(col_buf_0_val_2_0_reg_2729[6]),
        .I1(row_assign_9_0_2_t_reg_2565[1]),
        .I2(tmp_3_reg_2522),
        .I3(col_buf_0_val_1_0_reg_2721[6]),
        .I4(row_assign_9_0_2_t_reg_2565[0]),
        .I5(col_buf_0_val_0_0_reg_2708[6]),
        .O(tmp_160_0_0_2_cast_fu_1489_p1[6]));
  LUT3 #(
    .INIT(8'h40)) 
    \src_kernel_win_0_va_4_fu_262[7]_i_1 
       (.I0(ap_reg_pp0_iter2_exitcond389_i_reg_2583),
        .I1(ap_block_pp0_stage0_subdone2_in),
        .I2(ap_enable_reg_pp0_iter3),
        .O(src_kernel_win_0_va_1_fu_2500));
  LUT6 #(
    .INIT(64'hBABABA8A8A8ABA8A)) 
    \src_kernel_win_0_va_4_fu_262[7]_i_2 
       (.I0(col_buf_0_val_2_0_reg_2729[7]),
        .I1(row_assign_9_0_2_t_reg_2565[1]),
        .I2(tmp_3_reg_2522),
        .I3(col_buf_0_val_0_0_reg_2708[7]),
        .I4(row_assign_9_0_2_t_reg_2565[0]),
        .I5(col_buf_0_val_1_0_reg_2721[7]),
        .O(tmp_160_0_0_2_cast_fu_1489_p1[7]));
  FDRE \src_kernel_win_0_va_4_fu_262_reg[0] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(tmp_160_0_0_2_cast_fu_1489_p1[0]),
        .Q(src_kernel_win_0_va_4_fu_262[0]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_4_fu_262_reg[1] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(tmp_160_0_0_2_cast_fu_1489_p1[1]),
        .Q(src_kernel_win_0_va_4_fu_262[1]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_4_fu_262_reg[2] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(tmp_160_0_0_2_cast_fu_1489_p1[2]),
        .Q(src_kernel_win_0_va_4_fu_262[2]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_4_fu_262_reg[3] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(tmp_160_0_0_2_cast_fu_1489_p1[3]),
        .Q(src_kernel_win_0_va_4_fu_262[3]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_4_fu_262_reg[4] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(tmp_160_0_0_2_cast_fu_1489_p1[4]),
        .Q(src_kernel_win_0_va_4_fu_262[4]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_4_fu_262_reg[5] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(tmp_160_0_0_2_cast_fu_1489_p1[5]),
        .Q(src_kernel_win_0_va_4_fu_262[5]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_4_fu_262_reg[6] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(tmp_160_0_0_2_cast_fu_1489_p1[6]),
        .Q(src_kernel_win_0_va_4_fu_262[6]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_4_fu_262_reg[7] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(tmp_160_0_0_2_cast_fu_1489_p1[7]),
        .Q(src_kernel_win_0_va_4_fu_262[7]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_5_fu_266_reg[0] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_4_fu_262[0]),
        .Q(src_kernel_win_0_va_5_fu_266[0]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_5_fu_266_reg[1] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_4_fu_262[1]),
        .Q(src_kernel_win_0_va_5_fu_266[1]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_5_fu_266_reg[2] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_4_fu_262[2]),
        .Q(src_kernel_win_0_va_5_fu_266[2]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_5_fu_266_reg[3] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_4_fu_262[3]),
        .Q(src_kernel_win_0_va_5_fu_266[3]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_5_fu_266_reg[4] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_4_fu_262[4]),
        .Q(src_kernel_win_0_va_5_fu_266[4]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_5_fu_266_reg[5] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_4_fu_262[5]),
        .Q(src_kernel_win_0_va_5_fu_266[5]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_5_fu_266_reg[6] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_4_fu_262[6]),
        .Q(src_kernel_win_0_va_5_fu_266[6]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_5_fu_266_reg[7] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_4_fu_262[7]),
        .Q(src_kernel_win_0_va_5_fu_266[7]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hB8BBFFFFB8880000)) 
    \src_kernel_win_0_va_6_reg_2805[0]_i_1 
       (.I0(col_buf_0_val_2_0_reg_2729[0]),
        .I1(tmp_32_reg_2572[1]),
        .I2(col_buf_0_val_1_0_reg_2721[0]),
        .I3(tmp_32_reg_2572[0]),
        .I4(tmp_3_reg_2522),
        .I5(col_buf_0_val_0_0_reg_2708[0]),
        .O(src_kernel_win_0_va_6_fu_1442_p3[0]));
  LUT6 #(
    .INIT(64'hB8BBFFFFB8880000)) 
    \src_kernel_win_0_va_6_reg_2805[1]_i_1 
       (.I0(col_buf_0_val_2_0_reg_2729[1]),
        .I1(tmp_32_reg_2572[1]),
        .I2(col_buf_0_val_1_0_reg_2721[1]),
        .I3(tmp_32_reg_2572[0]),
        .I4(tmp_3_reg_2522),
        .I5(col_buf_0_val_0_0_reg_2708[1]),
        .O(src_kernel_win_0_va_6_fu_1442_p3[1]));
  LUT6 #(
    .INIT(64'hB8BBFFFFB8880000)) 
    \src_kernel_win_0_va_6_reg_2805[2]_i_1 
       (.I0(col_buf_0_val_2_0_reg_2729[2]),
        .I1(tmp_32_reg_2572[1]),
        .I2(col_buf_0_val_1_0_reg_2721[2]),
        .I3(tmp_32_reg_2572[0]),
        .I4(tmp_3_reg_2522),
        .I5(col_buf_0_val_0_0_reg_2708[2]),
        .O(src_kernel_win_0_va_6_fu_1442_p3[2]));
  LUT6 #(
    .INIT(64'hB8BBFFFFB8880000)) 
    \src_kernel_win_0_va_6_reg_2805[3]_i_1 
       (.I0(col_buf_0_val_2_0_reg_2729[3]),
        .I1(tmp_32_reg_2572[1]),
        .I2(col_buf_0_val_1_0_reg_2721[3]),
        .I3(tmp_32_reg_2572[0]),
        .I4(tmp_3_reg_2522),
        .I5(col_buf_0_val_0_0_reg_2708[3]),
        .O(src_kernel_win_0_va_6_fu_1442_p3[3]));
  LUT6 #(
    .INIT(64'hB8BBFFFFB8880000)) 
    \src_kernel_win_0_va_6_reg_2805[4]_i_1 
       (.I0(col_buf_0_val_2_0_reg_2729[4]),
        .I1(tmp_32_reg_2572[1]),
        .I2(col_buf_0_val_1_0_reg_2721[4]),
        .I3(tmp_32_reg_2572[0]),
        .I4(tmp_3_reg_2522),
        .I5(col_buf_0_val_0_0_reg_2708[4]),
        .O(src_kernel_win_0_va_6_fu_1442_p3[4]));
  LUT6 #(
    .INIT(64'hB8BBFFFFB8880000)) 
    \src_kernel_win_0_va_6_reg_2805[5]_i_1 
       (.I0(col_buf_0_val_2_0_reg_2729[5]),
        .I1(tmp_32_reg_2572[1]),
        .I2(col_buf_0_val_1_0_reg_2721[5]),
        .I3(tmp_32_reg_2572[0]),
        .I4(tmp_3_reg_2522),
        .I5(col_buf_0_val_0_0_reg_2708[5]),
        .O(src_kernel_win_0_va_6_fu_1442_p3[5]));
  LUT6 #(
    .INIT(64'hB8BBFFFFB8880000)) 
    \src_kernel_win_0_va_6_reg_2805[6]_i_1 
       (.I0(col_buf_0_val_2_0_reg_2729[6]),
        .I1(tmp_32_reg_2572[1]),
        .I2(col_buf_0_val_1_0_reg_2721[6]),
        .I3(tmp_32_reg_2572[0]),
        .I4(tmp_3_reg_2522),
        .I5(col_buf_0_val_0_0_reg_2708[6]),
        .O(src_kernel_win_0_va_6_fu_1442_p3[6]));
  LUT6 #(
    .INIT(64'hB8BBFFFFB8880000)) 
    \src_kernel_win_0_va_6_reg_2805[7]_i_1 
       (.I0(col_buf_0_val_2_0_reg_2729[7]),
        .I1(tmp_32_reg_2572[1]),
        .I2(col_buf_0_val_1_0_reg_2721[7]),
        .I3(tmp_32_reg_2572[0]),
        .I4(tmp_3_reg_2522),
        .I5(col_buf_0_val_0_0_reg_2708[7]),
        .O(src_kernel_win_0_va_6_fu_1442_p3[7]));
  FDRE \src_kernel_win_0_va_6_reg_2805_reg[0] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(src_kernel_win_0_va_6_fu_1442_p3[0]),
        .Q(src_kernel_win_0_va_6_reg_2805[0]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_6_reg_2805_reg[1] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(src_kernel_win_0_va_6_fu_1442_p3[1]),
        .Q(src_kernel_win_0_va_6_reg_2805[1]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_6_reg_2805_reg[2] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(src_kernel_win_0_va_6_fu_1442_p3[2]),
        .Q(src_kernel_win_0_va_6_reg_2805[2]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_6_reg_2805_reg[3] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(src_kernel_win_0_va_6_fu_1442_p3[3]),
        .Q(src_kernel_win_0_va_6_reg_2805[3]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_6_reg_2805_reg[4] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(src_kernel_win_0_va_6_fu_1442_p3[4]),
        .Q(src_kernel_win_0_va_6_reg_2805[4]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_6_reg_2805_reg[5] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(src_kernel_win_0_va_6_fu_1442_p3[5]),
        .Q(src_kernel_win_0_va_6_reg_2805[5]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_6_reg_2805_reg[6] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(src_kernel_win_0_va_6_fu_1442_p3[6]),
        .Q(src_kernel_win_0_va_6_reg_2805[6]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_6_reg_2805_reg[7] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(src_kernel_win_0_va_6_fu_1442_p3[7]),
        .Q(src_kernel_win_0_va_6_reg_2805[7]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hBBB8FFFF8B880000)) 
    \src_kernel_win_0_va_7_reg_2811[0]_i_1 
       (.I0(col_buf_0_val_2_0_reg_2729[0]),
        .I1(row_assign_9_0_1_t_reg_2558[1]),
        .I2(row_assign_9_0_1_t_reg_2558[0]),
        .I3(col_buf_0_val_0_0_reg_2708[0]),
        .I4(tmp_3_reg_2522),
        .I5(col_buf_0_val_1_0_reg_2721[0]),
        .O(src_kernel_win_0_va_7_fu_1456_p3[0]));
  LUT6 #(
    .INIT(64'hBBB8FFFF8B880000)) 
    \src_kernel_win_0_va_7_reg_2811[1]_i_1 
       (.I0(col_buf_0_val_2_0_reg_2729[1]),
        .I1(row_assign_9_0_1_t_reg_2558[1]),
        .I2(row_assign_9_0_1_t_reg_2558[0]),
        .I3(col_buf_0_val_0_0_reg_2708[1]),
        .I4(tmp_3_reg_2522),
        .I5(col_buf_0_val_1_0_reg_2721[1]),
        .O(src_kernel_win_0_va_7_fu_1456_p3[1]));
  LUT6 #(
    .INIT(64'hBBB8FFFF8B880000)) 
    \src_kernel_win_0_va_7_reg_2811[2]_i_1 
       (.I0(col_buf_0_val_2_0_reg_2729[2]),
        .I1(row_assign_9_0_1_t_reg_2558[1]),
        .I2(row_assign_9_0_1_t_reg_2558[0]),
        .I3(col_buf_0_val_0_0_reg_2708[2]),
        .I4(tmp_3_reg_2522),
        .I5(col_buf_0_val_1_0_reg_2721[2]),
        .O(src_kernel_win_0_va_7_fu_1456_p3[2]));
  LUT6 #(
    .INIT(64'hBBB8FFFF8B880000)) 
    \src_kernel_win_0_va_7_reg_2811[3]_i_1 
       (.I0(col_buf_0_val_2_0_reg_2729[3]),
        .I1(row_assign_9_0_1_t_reg_2558[1]),
        .I2(row_assign_9_0_1_t_reg_2558[0]),
        .I3(col_buf_0_val_0_0_reg_2708[3]),
        .I4(tmp_3_reg_2522),
        .I5(col_buf_0_val_1_0_reg_2721[3]),
        .O(src_kernel_win_0_va_7_fu_1456_p3[3]));
  LUT6 #(
    .INIT(64'hBBB8FFFF8B880000)) 
    \src_kernel_win_0_va_7_reg_2811[4]_i_1 
       (.I0(col_buf_0_val_2_0_reg_2729[4]),
        .I1(row_assign_9_0_1_t_reg_2558[1]),
        .I2(row_assign_9_0_1_t_reg_2558[0]),
        .I3(col_buf_0_val_0_0_reg_2708[4]),
        .I4(tmp_3_reg_2522),
        .I5(col_buf_0_val_1_0_reg_2721[4]),
        .O(src_kernel_win_0_va_7_fu_1456_p3[4]));
  LUT6 #(
    .INIT(64'hBBB8FFFF8B880000)) 
    \src_kernel_win_0_va_7_reg_2811[5]_i_1 
       (.I0(col_buf_0_val_2_0_reg_2729[5]),
        .I1(row_assign_9_0_1_t_reg_2558[1]),
        .I2(row_assign_9_0_1_t_reg_2558[0]),
        .I3(col_buf_0_val_0_0_reg_2708[5]),
        .I4(tmp_3_reg_2522),
        .I5(col_buf_0_val_1_0_reg_2721[5]),
        .O(src_kernel_win_0_va_7_fu_1456_p3[5]));
  LUT6 #(
    .INIT(64'hBBB8FFFF8B880000)) 
    \src_kernel_win_0_va_7_reg_2811[6]_i_1 
       (.I0(col_buf_0_val_2_0_reg_2729[6]),
        .I1(row_assign_9_0_1_t_reg_2558[1]),
        .I2(row_assign_9_0_1_t_reg_2558[0]),
        .I3(col_buf_0_val_0_0_reg_2708[6]),
        .I4(tmp_3_reg_2522),
        .I5(col_buf_0_val_1_0_reg_2721[6]),
        .O(src_kernel_win_0_va_7_fu_1456_p3[6]));
  LUT6 #(
    .INIT(64'hBBB8FFFF8B880000)) 
    \src_kernel_win_0_va_7_reg_2811[7]_i_1 
       (.I0(col_buf_0_val_2_0_reg_2729[7]),
        .I1(row_assign_9_0_1_t_reg_2558[1]),
        .I2(row_assign_9_0_1_t_reg_2558[0]),
        .I3(col_buf_0_val_0_0_reg_2708[7]),
        .I4(tmp_3_reg_2522),
        .I5(col_buf_0_val_1_0_reg_2721[7]),
        .O(src_kernel_win_0_va_7_fu_1456_p3[7]));
  FDRE \src_kernel_win_0_va_7_reg_2811_reg[0] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(src_kernel_win_0_va_7_fu_1456_p3[0]),
        .Q(src_kernel_win_0_va_7_reg_2811[0]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_7_reg_2811_reg[1] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(src_kernel_win_0_va_7_fu_1456_p3[1]),
        .Q(src_kernel_win_0_va_7_reg_2811[1]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_7_reg_2811_reg[2] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(src_kernel_win_0_va_7_fu_1456_p3[2]),
        .Q(src_kernel_win_0_va_7_reg_2811[2]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_7_reg_2811_reg[3] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(src_kernel_win_0_va_7_fu_1456_p3[3]),
        .Q(src_kernel_win_0_va_7_reg_2811[3]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_7_reg_2811_reg[4] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(src_kernel_win_0_va_7_fu_1456_p3[4]),
        .Q(src_kernel_win_0_va_7_reg_2811[4]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_7_reg_2811_reg[5] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(src_kernel_win_0_va_7_fu_1456_p3[5]),
        .Q(src_kernel_win_0_va_7_reg_2811[5]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_7_reg_2811_reg[6] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(src_kernel_win_0_va_7_fu_1456_p3[6]),
        .Q(src_kernel_win_0_va_7_reg_2811[6]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_7_reg_2811_reg[7] 
       (.C(ap_clk),
        .CE(ap_block_pp0_stage0_subdone2_in),
        .D(src_kernel_win_0_va_7_fu_1456_p3[7]),
        .Q(src_kernel_win_0_va_7_reg_2811[7]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_fu_246_reg[0] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_6_fu_1442_p3[0]),
        .Q(src_kernel_win_0_va_fu_246[0]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_fu_246_reg[1] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_6_fu_1442_p3[1]),
        .Q(src_kernel_win_0_va_fu_246[1]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_fu_246_reg[2] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_6_fu_1442_p3[2]),
        .Q(src_kernel_win_0_va_fu_246[2]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_fu_246_reg[3] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_6_fu_1442_p3[3]),
        .Q(src_kernel_win_0_va_fu_246[3]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_fu_246_reg[4] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_6_fu_1442_p3[4]),
        .Q(src_kernel_win_0_va_fu_246[4]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_fu_246_reg[5] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_6_fu_1442_p3[5]),
        .Q(src_kernel_win_0_va_fu_246[5]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_fu_246_reg[6] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_6_fu_1442_p3[6]),
        .Q(src_kernel_win_0_va_fu_246[6]),
        .R(1'b0));
  FDRE \src_kernel_win_0_va_fu_246_reg[7] 
       (.C(ap_clk),
        .CE(src_kernel_win_0_va_1_fu_2500),
        .D(src_kernel_win_0_va_6_fu_1442_p3[7]),
        .Q(src_kernel_win_0_va_fu_246[7]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h5444)) 
    start_once_reg_i_1__0
       (.I0(\ap_CS_fsm_reg[0]_0 ),
        .I1(start_once_reg_reg_0),
        .I2(start_for_CvtColor_1_U0_full_n),
        .I3(Sobel_U0_ap_start),
        .O(start_once_reg_reg));
  LUT5 #(
    .INIT(32'hF700FF00)) 
    \t_V_2_reg_577[10]_i_1 
       (.I0(ap_block_pp0_stage0_subdone2_in),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(exitcond389_i_fu_936_p2),
        .I3(ap_CS_fsm_state4),
        .I4(ap_enable_reg_pp0_iter0),
        .O(t_V_2_reg_577));
  LUT4 #(
    .INIT(16'h0800)) 
    \t_V_2_reg_577[10]_i_2 
       (.I0(ap_block_pp0_stage0_subdone2_in),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(exitcond389_i_fu_936_p2),
        .I3(ap_enable_reg_pp0_iter0),
        .O(t_V_2_reg_5770));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \t_V_2_reg_577[10]_i_3 
       (.I0(t_V_2_reg_577_reg__0[10]),
        .I1(t_V_2_reg_577_reg__0[9]),
        .I2(t_V_2_reg_577_reg__0[7]),
        .I3(\t_V_2_reg_577[10]_i_4_n_2 ),
        .I4(t_V_2_reg_577_reg__0[6]),
        .I5(t_V_2_reg_577_reg__0[8]),
        .O(j_V_fu_942_p2[10]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \t_V_2_reg_577[10]_i_4 
       (.I0(t_V_2_reg_577_reg__0[4]),
        .I1(t_V_2_reg_577_reg__0[2]),
        .I2(t_V_2_reg_577_reg__0__0),
        .I3(t_V_2_reg_577_reg__0[1]),
        .I4(t_V_2_reg_577_reg__0[3]),
        .I5(t_V_2_reg_577_reg__0[5]),
        .O(\t_V_2_reg_577[10]_i_4_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair149" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \t_V_2_reg_577[1]_i_1 
       (.I0(t_V_2_reg_577_reg__0__0),
        .I1(t_V_2_reg_577_reg__0[1]),
        .O(j_V_fu_942_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair149" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \t_V_2_reg_577[2]_i_1 
       (.I0(t_V_2_reg_577_reg__0[2]),
        .I1(t_V_2_reg_577_reg__0[1]),
        .I2(t_V_2_reg_577_reg__0__0),
        .O(j_V_fu_942_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair133" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \t_V_2_reg_577[3]_i_1 
       (.I0(t_V_2_reg_577_reg__0[3]),
        .I1(t_V_2_reg_577_reg__0[2]),
        .I2(t_V_2_reg_577_reg__0__0),
        .I3(t_V_2_reg_577_reg__0[1]),
        .O(j_V_fu_942_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair133" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \t_V_2_reg_577[4]_i_1 
       (.I0(t_V_2_reg_577_reg__0[4]),
        .I1(t_V_2_reg_577_reg__0[3]),
        .I2(t_V_2_reg_577_reg__0[1]),
        .I3(t_V_2_reg_577_reg__0__0),
        .I4(t_V_2_reg_577_reg__0[2]),
        .O(j_V_fu_942_p2[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \t_V_2_reg_577[5]_i_1 
       (.I0(t_V_2_reg_577_reg__0[5]),
        .I1(t_V_2_reg_577_reg__0[4]),
        .I2(t_V_2_reg_577_reg__0[2]),
        .I3(t_V_2_reg_577_reg__0__0),
        .I4(t_V_2_reg_577_reg__0[1]),
        .I5(t_V_2_reg_577_reg__0[3]),
        .O(j_V_fu_942_p2[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \t_V_2_reg_577[6]_i_1 
       (.I0(t_V_2_reg_577_reg__0[6]),
        .I1(\t_V_2_reg_577[10]_i_4_n_2 ),
        .O(j_V_fu_942_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair147" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \t_V_2_reg_577[7]_i_1 
       (.I0(t_V_2_reg_577_reg__0[7]),
        .I1(t_V_2_reg_577_reg__0[6]),
        .I2(\t_V_2_reg_577[10]_i_4_n_2 ),
        .O(j_V_fu_942_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair132" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \t_V_2_reg_577[8]_i_1 
       (.I0(t_V_2_reg_577_reg__0[8]),
        .I1(t_V_2_reg_577_reg__0[7]),
        .I2(\t_V_2_reg_577[10]_i_4_n_2 ),
        .I3(t_V_2_reg_577_reg__0[6]),
        .O(j_V_fu_942_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair132" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \t_V_2_reg_577[9]_i_1 
       (.I0(t_V_2_reg_577_reg__0[9]),
        .I1(t_V_2_reg_577_reg__0[8]),
        .I2(t_V_2_reg_577_reg__0[6]),
        .I3(\t_V_2_reg_577[10]_i_4_n_2 ),
        .I4(t_V_2_reg_577_reg__0[7]),
        .O(j_V_fu_942_p2[9]));
  FDRE \t_V_2_reg_577_reg[0] 
       (.C(ap_clk),
        .CE(t_V_2_reg_5770),
        .D(\p_assign_2_reg_2622[0]_i_1_n_2 ),
        .Q(t_V_2_reg_577_reg__0__0),
        .R(t_V_2_reg_577));
  FDRE \t_V_2_reg_577_reg[10] 
       (.C(ap_clk),
        .CE(t_V_2_reg_5770),
        .D(j_V_fu_942_p2[10]),
        .Q(t_V_2_reg_577_reg__0[10]),
        .R(t_V_2_reg_577));
  FDRE \t_V_2_reg_577_reg[1] 
       (.C(ap_clk),
        .CE(t_V_2_reg_5770),
        .D(j_V_fu_942_p2[1]),
        .Q(t_V_2_reg_577_reg__0[1]),
        .R(t_V_2_reg_577));
  FDRE \t_V_2_reg_577_reg[2] 
       (.C(ap_clk),
        .CE(t_V_2_reg_5770),
        .D(j_V_fu_942_p2[2]),
        .Q(t_V_2_reg_577_reg__0[2]),
        .R(t_V_2_reg_577));
  FDRE \t_V_2_reg_577_reg[3] 
       (.C(ap_clk),
        .CE(t_V_2_reg_5770),
        .D(j_V_fu_942_p2[3]),
        .Q(t_V_2_reg_577_reg__0[3]),
        .R(t_V_2_reg_577));
  FDRE \t_V_2_reg_577_reg[4] 
       (.C(ap_clk),
        .CE(t_V_2_reg_5770),
        .D(j_V_fu_942_p2[4]),
        .Q(t_V_2_reg_577_reg__0[4]),
        .R(t_V_2_reg_577));
  FDRE \t_V_2_reg_577_reg[5] 
       (.C(ap_clk),
        .CE(t_V_2_reg_5770),
        .D(j_V_fu_942_p2[5]),
        .Q(t_V_2_reg_577_reg__0[5]),
        .R(t_V_2_reg_577));
  FDRE \t_V_2_reg_577_reg[6] 
       (.C(ap_clk),
        .CE(t_V_2_reg_5770),
        .D(j_V_fu_942_p2[6]),
        .Q(t_V_2_reg_577_reg__0[6]),
        .R(t_V_2_reg_577));
  FDRE \t_V_2_reg_577_reg[7] 
       (.C(ap_clk),
        .CE(t_V_2_reg_5770),
        .D(j_V_fu_942_p2[7]),
        .Q(t_V_2_reg_577_reg__0[7]),
        .R(t_V_2_reg_577));
  FDRE \t_V_2_reg_577_reg[8] 
       (.C(ap_clk),
        .CE(t_V_2_reg_5770),
        .D(j_V_fu_942_p2[8]),
        .Q(t_V_2_reg_577_reg__0[8]),
        .R(t_V_2_reg_577));
  FDRE \t_V_2_reg_577_reg[9] 
       (.C(ap_clk),
        .CE(t_V_2_reg_5770),
        .D(j_V_fu_942_p2[9]),
        .Q(t_V_2_reg_577_reg__0[9]),
        .R(t_V_2_reg_577));
  LUT3 #(
    .INIT(8'h20)) 
    \t_V_reg_566[9]_i_1 
       (.I0(ap_CS_fsm_state2),
        .I1(tmp_5_reg_555[0]),
        .I2(tmp_5_reg_555[1]),
        .O(ap_NS_fsm1));
  FDRE \t_V_reg_566_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state11),
        .D(i_V_reg_2495[0]),
        .Q(t_V_reg_566[0]),
        .R(ap_NS_fsm1));
  FDRE \t_V_reg_566_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state11),
        .D(i_V_reg_2495[1]),
        .Q(t_V_reg_566[1]),
        .R(ap_NS_fsm1));
  FDRE \t_V_reg_566_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state11),
        .D(i_V_reg_2495[2]),
        .Q(t_V_reg_566[2]),
        .R(ap_NS_fsm1));
  FDRE \t_V_reg_566_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state11),
        .D(i_V_reg_2495[3]),
        .Q(t_V_reg_566[3]),
        .R(ap_NS_fsm1));
  FDRE \t_V_reg_566_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state11),
        .D(i_V_reg_2495[4]),
        .Q(t_V_reg_566[4]),
        .R(ap_NS_fsm1));
  FDRE \t_V_reg_566_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state11),
        .D(i_V_reg_2495[5]),
        .Q(t_V_reg_566[5]),
        .R(ap_NS_fsm1));
  FDRE \t_V_reg_566_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state11),
        .D(i_V_reg_2495[6]),
        .Q(t_V_reg_566[6]),
        .R(ap_NS_fsm1));
  FDRE \t_V_reg_566_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state11),
        .D(i_V_reg_2495[7]),
        .Q(t_V_reg_566[7]),
        .R(ap_NS_fsm1));
  FDRE \t_V_reg_566_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state11),
        .D(i_V_reg_2495[8]),
        .Q(t_V_reg_566[8]),
        .R(ap_NS_fsm1));
  FDRE \t_V_reg_566_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state11),
        .D(i_V_reg_2495[9]),
        .Q(t_V_reg_566[9]),
        .R(ap_NS_fsm1));
  CARRY4 tmp57_fu_1547_p2_carry
       (.CI(1'b0),
        .CO({tmp57_fu_1547_p2_carry_n_2,tmp57_fu_1547_p2_carry_n_3,tmp57_fu_1547_p2_carry_n_4,tmp57_fu_1547_p2_carry_n_5}),
        .CYINIT(1'b0),
        .DI({r_V_7_0_1_fu_1511_p2[3],tmp57_fu_1547_p2_carry_i_1_n_2,src_kernel_win_0_va_3_fu_258[0],1'b0}),
        .O(tmp57_fu_1547_p2[3:0]),
        .S({tmp57_fu_1547_p2_carry_i_2_n_2,tmp57_fu_1547_p2_carry_i_3_n_2,tmp57_fu_1547_p2_carry_i_4_n_2,src_kernel_win_0_va_1_fu_250[0]}));
  CARRY4 tmp57_fu_1547_p2_carry__0
       (.CI(tmp57_fu_1547_p2_carry_n_2),
        .CO({tmp57_fu_1547_p2_carry__0_n_2,tmp57_fu_1547_p2_carry__0_n_3,tmp57_fu_1547_p2_carry__0_n_4,tmp57_fu_1547_p2_carry__0_n_5}),
        .CYINIT(1'b0),
        .DI({tmp57_fu_1547_p2_carry__0_i_1_n_2,r_V_7_0_1_fu_1511_p2[6:4]}),
        .O(tmp57_fu_1547_p2[7:4]),
        .S({tmp57_fu_1547_p2_carry__0_i_2_n_2,tmp57_fu_1547_p2_carry__0_i_3_n_2,tmp57_fu_1547_p2_carry__0_i_4_n_2,tmp57_fu_1547_p2_carry__0_i_5_n_2}));
  LUT2 #(
    .INIT(4'h6)) 
    tmp57_fu_1547_p2_carry__0_i_1
       (.I0(\tmp_54_reg_2827[7]_i_2_n_2 ),
        .I1(src_kernel_win_0_va_3_fu_258[6]),
        .O(tmp57_fu_1547_p2_carry__0_i_1_n_2));
  LUT5 #(
    .INIT(32'h56A9A956)) 
    tmp57_fu_1547_p2_carry__0_i_2
       (.I0(src_kernel_win_0_va_1_fu_250[7]),
        .I1(\tmp_56_reg_2832[7]_i_2_n_2 ),
        .I2(src_kernel_win_0_va_1_fu_250[6]),
        .I3(src_kernel_win_0_va_3_fu_258[6]),
        .I4(\tmp_54_reg_2827[7]_i_2_n_2 ),
        .O(tmp57_fu_1547_p2_carry__0_i_2_n_2));
  LUT3 #(
    .INIT(8'h96)) 
    tmp57_fu_1547_p2_carry__0_i_3
       (.I0(src_kernel_win_0_va_1_fu_250[6]),
        .I1(\tmp_56_reg_2832[7]_i_2_n_2 ),
        .I2(r_V_7_0_1_fu_1511_p2[6]),
        .O(tmp57_fu_1547_p2_carry__0_i_3_n_2));
  LUT6 #(
    .INIT(64'h9999999999999996)) 
    tmp57_fu_1547_p2_carry__0_i_4
       (.I0(tmp_160_0_2_cast_fu_1539_p1[5]),
        .I1(src_kernel_win_0_va_3_fu_258[4]),
        .I2(src_kernel_win_0_va_3_fu_258[2]),
        .I3(src_kernel_win_0_va_3_fu_258[0]),
        .I4(src_kernel_win_0_va_3_fu_258[1]),
        .I5(src_kernel_win_0_va_3_fu_258[3]),
        .O(tmp57_fu_1547_p2_carry__0_i_4_n_2));
  LUT6 #(
    .INIT(64'hAAAAAAA955555556)) 
    tmp57_fu_1547_p2_carry__0_i_5
       (.I0(src_kernel_win_0_va_1_fu_250[4]),
        .I1(src_kernel_win_0_va_1_fu_250[2]),
        .I2(src_kernel_win_0_va_1_fu_250[0]),
        .I3(src_kernel_win_0_va_1_fu_250[1]),
        .I4(src_kernel_win_0_va_1_fu_250[3]),
        .I5(r_V_7_0_1_fu_1511_p2[4]),
        .O(tmp57_fu_1547_p2_carry__0_i_5_n_2));
  CARRY4 tmp57_fu_1547_p2_carry__1
       (.CI(tmp57_fu_1547_p2_carry__0_n_2),
        .CO({NLW_tmp57_fu_1547_p2_carry__1_CO_UNCONNECTED[3:2],tmp57_fu_1547_p2_carry__1_n_4,tmp57_fu_1547_p2_carry__1_n_5}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,tmp57_fu_1547_p2_carry__1_i_1_n_2,tmp57_fu_1547_p2_carry__1_i_2_n_2}),
        .O({NLW_tmp57_fu_1547_p2_carry__1_O_UNCONNECTED[3],tmp57_fu_1547_p2[10:8]}),
        .S({1'b0,1'b1,tmp57_fu_1547_p2_carry__1_i_3_n_2,tmp57_fu_1547_p2_carry__1_i_4_n_2}));
  LUT3 #(
    .INIT(8'h1E)) 
    tmp57_fu_1547_p2_carry__1_i_1
       (.I0(src_kernel_win_0_va_3_fu_258[6]),
        .I1(\tmp_54_reg_2827[7]_i_2_n_2 ),
        .I2(src_kernel_win_0_va_3_fu_258[7]),
        .O(tmp57_fu_1547_p2_carry__1_i_1_n_2));
  LUT3 #(
    .INIT(8'hA9)) 
    tmp57_fu_1547_p2_carry__1_i_2
       (.I0(src_kernel_win_0_va_3_fu_258[7]),
        .I1(\tmp_54_reg_2827[7]_i_2_n_2 ),
        .I2(src_kernel_win_0_va_3_fu_258[6]),
        .O(tmp57_fu_1547_p2_carry__1_i_2_n_2));
  LUT3 #(
    .INIT(8'h1F)) 
    tmp57_fu_1547_p2_carry__1_i_3
       (.I0(\tmp_54_reg_2827[7]_i_2_n_2 ),
        .I1(src_kernel_win_0_va_3_fu_258[6]),
        .I2(src_kernel_win_0_va_3_fu_258[7]),
        .O(tmp57_fu_1547_p2_carry__1_i_3_n_2));
  LUT6 #(
    .INIT(64'hA9A9A9A9A9A9A956)) 
    tmp57_fu_1547_p2_carry__1_i_4
       (.I0(src_kernel_win_0_va_3_fu_258[7]),
        .I1(\tmp_54_reg_2827[7]_i_2_n_2 ),
        .I2(src_kernel_win_0_va_3_fu_258[6]),
        .I3(src_kernel_win_0_va_1_fu_250[7]),
        .I4(\tmp_56_reg_2832[7]_i_2_n_2 ),
        .I5(src_kernel_win_0_va_1_fu_250[6]),
        .O(tmp57_fu_1547_p2_carry__1_i_4_n_2));
  LUT2 #(
    .INIT(4'h6)) 
    tmp57_fu_1547_p2_carry_i_1
       (.I0(src_kernel_win_0_va_3_fu_258[0]),
        .I1(src_kernel_win_0_va_3_fu_258[1]),
        .O(tmp57_fu_1547_p2_carry_i_1_n_2));
  LUT5 #(
    .INIT(32'hAAA95556)) 
    tmp57_fu_1547_p2_carry_i_2
       (.I0(src_kernel_win_0_va_1_fu_250[3]),
        .I1(src_kernel_win_0_va_1_fu_250[1]),
        .I2(src_kernel_win_0_va_1_fu_250[0]),
        .I3(src_kernel_win_0_va_1_fu_250[2]),
        .I4(r_V_7_0_1_fu_1511_p2[3]),
        .O(tmp57_fu_1547_p2_carry_i_2_n_2));
  LUT5 #(
    .INIT(32'h56A9A956)) 
    tmp57_fu_1547_p2_carry_i_3
       (.I0(src_kernel_win_0_va_1_fu_250[2]),
        .I1(src_kernel_win_0_va_1_fu_250[0]),
        .I2(src_kernel_win_0_va_1_fu_250[1]),
        .I3(src_kernel_win_0_va_3_fu_258[1]),
        .I4(src_kernel_win_0_va_3_fu_258[0]),
        .O(tmp57_fu_1547_p2_carry_i_3_n_2));
  LUT3 #(
    .INIT(8'h96)) 
    tmp57_fu_1547_p2_carry_i_4
       (.I0(src_kernel_win_0_va_1_fu_250[1]),
        .I1(src_kernel_win_0_va_1_fu_250[0]),
        .I2(src_kernel_win_0_va_3_fu_258[0]),
        .O(tmp57_fu_1547_p2_carry_i_4_n_2));
  FDRE \tmp57_reg_2837_reg[0] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(tmp57_fu_1547_p2[0]),
        .Q(tmp57_reg_2837[0]),
        .R(1'b0));
  FDRE \tmp57_reg_2837_reg[10] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(tmp57_fu_1547_p2[10]),
        .Q(tmp57_reg_2837[10]),
        .R(1'b0));
  FDRE \tmp57_reg_2837_reg[1] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(tmp57_fu_1547_p2[1]),
        .Q(tmp57_reg_2837[1]),
        .R(1'b0));
  FDRE \tmp57_reg_2837_reg[2] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(tmp57_fu_1547_p2[2]),
        .Q(tmp57_reg_2837[2]),
        .R(1'b0));
  FDRE \tmp57_reg_2837_reg[3] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(tmp57_fu_1547_p2[3]),
        .Q(tmp57_reg_2837[3]),
        .R(1'b0));
  FDRE \tmp57_reg_2837_reg[4] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(tmp57_fu_1547_p2[4]),
        .Q(tmp57_reg_2837[4]),
        .R(1'b0));
  FDRE \tmp57_reg_2837_reg[5] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(tmp57_fu_1547_p2[5]),
        .Q(tmp57_reg_2837[5]),
        .R(1'b0));
  FDRE \tmp57_reg_2837_reg[6] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(tmp57_fu_1547_p2[6]),
        .Q(tmp57_reg_2837[6]),
        .R(1'b0));
  FDRE \tmp57_reg_2837_reg[7] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(tmp57_fu_1547_p2[7]),
        .Q(tmp57_reg_2837[7]),
        .R(1'b0));
  FDRE \tmp57_reg_2837_reg[8] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(tmp57_fu_1547_p2[8]),
        .Q(tmp57_reg_2837[8]),
        .R(1'b0));
  FDRE \tmp57_reg_2837_reg[9] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(tmp57_fu_1547_p2[9]),
        .Q(tmp57_reg_2837[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair136" *) 
  LUT4 #(
    .INIT(16'hF870)) 
    \tmp_116_0_1_reg_2518[0]_i_1 
       (.I0(ap_CS_fsm_state3),
        .I1(\ap_CS_fsm[3]_i_2__0_n_2 ),
        .I2(\tmp_116_0_1_reg_2518_reg_n_2_[0] ),
        .I3(\tmp_4_reg_2535[10]_i_1_n_2 ),
        .O(\tmp_116_0_1_reg_2518[0]_i_1_n_2 ));
  FDRE \tmp_116_0_1_reg_2518_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_116_0_1_reg_2518[0]_i_1_n_2 ),
        .Q(\tmp_116_0_1_reg_2518_reg_n_2_[0] ),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h15FF15FF15FF55FF)) 
    \tmp_1_reg_2500[0]_i_1 
       (.I0(t_V_reg_566[8]),
        .I1(t_V_reg_566[7]),
        .I2(t_V_reg_566[6]),
        .I3(t_V_reg_566[9]),
        .I4(t_V_reg_566[5]),
        .I5(t_V_reg_566[4]),
        .O(tmp_1_fu_637_p2));
  FDRE \tmp_1_reg_2500_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(tmp_1_fu_637_p2),
        .Q(tmp_1_reg_2500),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h70FA707070707070)) 
    \tmp_2_reg_2514[0]_i_1 
       (.I0(ap_CS_fsm_state3),
        .I1(\ap_CS_fsm[3]_i_2__0_n_2 ),
        .I2(\tmp_2_reg_2514_reg_n_2_[0] ),
        .I3(t_V_reg_566[1]),
        .I4(\icmp_reg_2509[0]_i_2_n_2 ),
        .I5(t_V_reg_566[0]),
        .O(\tmp_2_reg_2514[0]_i_1_n_2 ));
  FDRE \tmp_2_reg_2514_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_2_reg_2514[0]_i_1_n_2 ),
        .Q(\tmp_2_reg_2514_reg_n_2_[0] ),
        .R(1'b0));
  CARRY4 tmp_31_fu_984_p2_carry
       (.CI(1'b0),
        .CO({NLW_tmp_31_fu_984_p2_carry_CO_UNCONNECTED[3:2],tmp_31_fu_984_p2,tmp_31_fu_984_p2_carry_n_5}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,tmp_31_fu_984_p2_carry_i_1_n_2,tmp_31_fu_984_p2_carry_i_2_n_2}),
        .O(NLW_tmp_31_fu_984_p2_carry_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,tmp_31_fu_984_p2_carry_i_3_n_2,tmp_31_fu_984_p2_carry_i_4_n_2}));
  LUT6 #(
    .INIT(64'h5555555555555557)) 
    tmp_31_fu_984_p2_carry_i_1
       (.I0(t_V_2_reg_577_reg__0[10]),
        .I1(t_V_2_reg_577_reg__0[8]),
        .I2(t_V_2_reg_577_reg__0[9]),
        .I3(t_V_2_reg_577_reg__0[7]),
        .I4(t_V_2_reg_577_reg__0[6]),
        .I5(\ImagLoc_x_reg_2592[10]_i_2_n_2 ),
        .O(tmp_31_fu_984_p2_carry_i_1_n_2));
  LUT5 #(
    .INIT(32'h00015554)) 
    tmp_31_fu_984_p2_carry_i_2
       (.I0(t_V_2_reg_577_reg__0[9]),
        .I1(t_V_2_reg_577_reg__0[7]),
        .I2(t_V_2_reg_577_reg__0[6]),
        .I3(\ImagLoc_x_reg_2592[10]_i_2_n_2 ),
        .I4(t_V_2_reg_577_reg__0[8]),
        .O(tmp_31_fu_984_p2_carry_i_2_n_2));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA8)) 
    tmp_31_fu_984_p2_carry_i_3
       (.I0(t_V_2_reg_577_reg__0[10]),
        .I1(t_V_2_reg_577_reg__0[8]),
        .I2(t_V_2_reg_577_reg__0[9]),
        .I3(t_V_2_reg_577_reg__0[7]),
        .I4(t_V_2_reg_577_reg__0[6]),
        .I5(\ImagLoc_x_reg_2592[10]_i_2_n_2 ),
        .O(tmp_31_fu_984_p2_carry_i_3_n_2));
  LUT5 #(
    .INIT(32'h0001FE00)) 
    tmp_31_fu_984_p2_carry_i_4
       (.I0(\ImagLoc_x_reg_2592[10]_i_2_n_2 ),
        .I1(t_V_2_reg_577_reg__0[6]),
        .I2(t_V_2_reg_577_reg__0[7]),
        .I3(t_V_2_reg_577_reg__0[8]),
        .I4(t_V_2_reg_577_reg__0[9]),
        .O(tmp_31_fu_984_p2_carry_i_4_n_2));
  FDRE \tmp_31_reg_2602_reg[0] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(tmp_31_fu_984_p2),
        .Q(tmp_31_reg_2602),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h7F40)) 
    \tmp_32_reg_2572[0]_i_1 
       (.I0(row_assign_9_0_1_t_reg_2558[0]),
        .I1(ap_CS_fsm_state4),
        .I2(tmp_3_reg_2522),
        .I3(tmp_32_reg_2572[0]),
        .O(\tmp_32_reg_2572[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair137" *) 
  LUT4 #(
    .INIT(16'hBF80)) 
    \tmp_32_reg_2572[1]_i_1 
       (.I0(tmp_32_fu_896_p2),
        .I1(ap_CS_fsm_state4),
        .I2(tmp_3_reg_2522),
        .I3(tmp_32_reg_2572[1]),
        .O(\tmp_32_reg_2572[1]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'h0059FF6A)) 
    \tmp_32_reg_2572[1]_i_2 
       (.I0(\tmp_32_reg_2572[1]_i_3_n_2 ),
        .I1(\tmp_4_reg_2535_reg_n_2_[10] ),
        .I2(p_assign_7_reg_2553[1]),
        .I3(or_cond_i425_i_reg_2542),
        .I4(tmp_4_reg_2535),
        .O(tmp_32_fu_896_p2));
  LUT6 #(
    .INIT(64'hFFFFFFFFE2EEEEEE)) 
    \tmp_32_reg_2572[1]_i_3 
       (.I0(\tmp_32_reg_2572[1]_i_4_n_2 ),
        .I1(\tmp_4_reg_2535_reg_n_2_[10] ),
        .I2(\icmp_reg_2509_reg_n_2_[0] ),
        .I3(p_assign_7_reg_2553[9]),
        .I4(\tmp_32_reg_2572[1]_i_5_n_2 ),
        .I5(row_assign_9_0_1_t_reg_2558[0]),
        .O(\tmp_32_reg_2572[1]_i_3_n_2 ));
  LUT6 #(
    .INIT(64'h15151555FFFFFFFF)) 
    \tmp_32_reg_2572[1]_i_4 
       (.I0(\tmp_4_reg_2535_reg_n_2_[8] ),
        .I1(\tmp_4_reg_2535_reg_n_2_[7] ),
        .I2(\tmp_4_reg_2535_reg_n_2_[6] ),
        .I3(\tmp_4_reg_2535_reg_n_2_[5] ),
        .I4(\tmp_4_reg_2535_reg_n_2_[4] ),
        .I5(\tmp_4_reg_2535_reg_n_2_[9] ),
        .O(\tmp_32_reg_2572[1]_i_4_n_2 ));
  LUT5 #(
    .INIT(32'hEAEAEAAA)) 
    \tmp_32_reg_2572[1]_i_5 
       (.I0(p_assign_7_reg_2553[8]),
        .I1(p_assign_7_reg_2553[6]),
        .I2(p_assign_7_reg_2553[7]),
        .I3(p_assign_7_reg_2553[4]),
        .I4(p_assign_7_reg_2553[5]),
        .O(\tmp_32_reg_2572[1]_i_5_n_2 ));
  FDRE \tmp_32_reg_2572_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_32_reg_2572[0]_i_1_n_2 ),
        .Q(tmp_32_reg_2572[0]),
        .R(1'b0));
  FDRE \tmp_32_reg_2572_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_32_reg_2572[1]_i_1_n_2 ),
        .Q(tmp_32_reg_2572[1]),
        .R(1'b0));
  CARRY4 tmp_33_fu_1022_p2_carry
       (.CI(1'b0),
        .CO({NLW_tmp_33_fu_1022_p2_carry_CO_UNCONNECTED[3:2],tmp_33_fu_1022_p2,tmp_33_fu_1022_p2_carry_n_5}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,tmp_31_fu_984_p2_carry_i_1_n_2,tmp_33_fu_1022_p2_carry_i_1_n_2}),
        .O(NLW_tmp_33_fu_1022_p2_carry_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,tmp_33_fu_1022_p2_carry_i_2_n_2,tmp_33_fu_1022_p2_carry_i_3_n_2}));
  LUT6 #(
    .INIT(64'hFFFFFFFF00005556)) 
    tmp_33_fu_1022_p2_carry_i_1
       (.I0(t_V_2_reg_577_reg__0[8]),
        .I1(\ImagLoc_x_reg_2592[10]_i_2_n_2 ),
        .I2(t_V_2_reg_577_reg__0[6]),
        .I3(t_V_2_reg_577_reg__0[7]),
        .I4(t_V_2_reg_577_reg__0[9]),
        .I5(\or_cond_i_reg_2640[0]_i_2_n_2 ),
        .O(tmp_33_fu_1022_p2_carry_i_1_n_2));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA8)) 
    tmp_33_fu_1022_p2_carry_i_2
       (.I0(t_V_2_reg_577_reg__0[10]),
        .I1(t_V_2_reg_577_reg__0[8]),
        .I2(t_V_2_reg_577_reg__0[9]),
        .I3(t_V_2_reg_577_reg__0[7]),
        .I4(t_V_2_reg_577_reg__0[6]),
        .I5(\ImagLoc_x_reg_2592[10]_i_2_n_2 ),
        .O(tmp_33_fu_1022_p2_carry_i_2_n_2));
  LUT5 #(
    .INIT(32'h0001FE00)) 
    tmp_33_fu_1022_p2_carry_i_3
       (.I0(\ImagLoc_x_reg_2592[10]_i_2_n_2 ),
        .I1(t_V_2_reg_577_reg__0[6]),
        .I2(t_V_2_reg_577_reg__0[7]),
        .I3(t_V_2_reg_577_reg__0[8]),
        .I4(t_V_2_reg_577_reg__0[9]),
        .O(tmp_33_fu_1022_p2_carry_i_3_n_2));
  FDRE \tmp_33_reg_2617_reg[0] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(tmp_33_fu_1022_p2),
        .Q(tmp_33_reg_2617),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000A2AA0424)) 
    \tmp_3_reg_2522[0]_i_1 
       (.I0(t_V_reg_566[9]),
        .I1(\tmp_3_reg_2522[0]_i_2_n_2 ),
        .I2(t_V_reg_566[7]),
        .I3(\tmp_3_reg_2522[0]_i_3_n_2 ),
        .I4(t_V_reg_566[8]),
        .I5(\tmp_4_reg_2535[10]_i_1_n_2 ),
        .O(tmp_3_fu_677_p2));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \tmp_3_reg_2522[0]_i_2 
       (.I0(t_V_reg_566[1]),
        .I1(t_V_reg_566[0]),
        .I2(\tmp_4_reg_2535[6]_i_2_n_2 ),
        .I3(t_V_reg_566[5]),
        .I4(t_V_reg_566[4]),
        .I5(t_V_reg_566[6]),
        .O(\tmp_3_reg_2522[0]_i_2_n_2 ));
  LUT6 #(
    .INIT(64'h55555555777E7777)) 
    \tmp_3_reg_2522[0]_i_3 
       (.I0(t_V_reg_566[6]),
        .I1(t_V_reg_566[4]),
        .I2(t_V_reg_566[1]),
        .I3(t_V_reg_566[0]),
        .I4(\tmp_4_reg_2535[6]_i_2_n_2 ),
        .I5(t_V_reg_566[5]),
        .O(\tmp_3_reg_2522[0]_i_3_n_2 ));
  FDRE \tmp_3_reg_2522_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(tmp_3_fu_677_p2),
        .Q(tmp_3_reg_2522),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \tmp_47_reg_2597[0]_i_1 
       (.I0(\ImagLoc_x_reg_2592[10]_i_2_n_2 ),
        .I1(t_V_2_reg_577_reg__0[6]),
        .I2(t_V_2_reg_577_reg__0[7]),
        .I3(t_V_2_reg_577_reg__0[8]),
        .I4(t_V_2_reg_577_reg__0[9]),
        .I5(t_V_2_reg_577_reg__0[10]),
        .O(ImagLoc_x_fu_964_p2));
  FDRE \tmp_47_reg_2597_reg[0] 
       (.C(ap_clk),
        .CE(ImagLoc_x_reg_25920),
        .D(ImagLoc_x_fu_964_p2),
        .Q(tmp_47_reg_2597),
        .R(1'b0));
  FDRE \tmp_49_reg_2644_reg[0] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_addr_reg_26490),
        .D(p_assign_2_reg_2622[0]),
        .Q(tmp_49_reg_2644[0]),
        .R(1'b0));
  FDRE \tmp_49_reg_2644_reg[1] 
       (.C(ap_clk),
        .CE(k_buf_0_val_3_addr_reg_26490),
        .D(k_buf_0_val_5_U_n_19),
        .Q(tmp_49_reg_2644[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair152" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \tmp_4_reg_2535[10]_i_1 
       (.I0(t_V_reg_566[0]),
        .I1(\icmp_reg_2509[0]_i_2_n_2 ),
        .I2(t_V_reg_566[1]),
        .O(\tmp_4_reg_2535[10]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair157" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \tmp_4_reg_2535[1]_i_1 
       (.I0(t_V_reg_566[0]),
        .I1(t_V_reg_566[1]),
        .O(\tmp_4_reg_2535[1]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair135" *) 
  LUT5 #(
    .INIT(32'hAAAAAAA9)) 
    \tmp_4_reg_2535[4]_i_1 
       (.I0(t_V_reg_566[4]),
        .I1(t_V_reg_566[1]),
        .I2(t_V_reg_566[0]),
        .I3(t_V_reg_566[2]),
        .I4(t_V_reg_566[3]),
        .O(\tmp_4_reg_2535[4]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA9)) 
    \tmp_4_reg_2535[5]_i_1 
       (.I0(t_V_reg_566[5]),
        .I1(t_V_reg_566[4]),
        .I2(t_V_reg_566[3]),
        .I3(t_V_reg_566[2]),
        .I4(t_V_reg_566[0]),
        .I5(t_V_reg_566[1]),
        .O(\tmp_4_reg_2535[5]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'hFFFFFEFF00000100)) 
    \tmp_4_reg_2535[6]_i_1 
       (.I0(t_V_reg_566[0]),
        .I1(t_V_reg_566[5]),
        .I2(t_V_reg_566[4]),
        .I3(\tmp_4_reg_2535[6]_i_2_n_2 ),
        .I4(t_V_reg_566[1]),
        .I5(t_V_reg_566[6]),
        .O(\tmp_4_reg_2535[6]_i_1_n_2 ));
  LUT2 #(
    .INIT(4'h1)) 
    \tmp_4_reg_2535[6]_i_2 
       (.I0(t_V_reg_566[2]),
        .I1(t_V_reg_566[3]),
        .O(\tmp_4_reg_2535[6]_i_2_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair134" *) 
  LUT5 #(
    .INIT(32'hAAA9AAAA)) 
    \tmp_4_reg_2535[7]_i_1 
       (.I0(t_V_reg_566[7]),
        .I1(t_V_reg_566[6]),
        .I2(t_V_reg_566[4]),
        .I3(t_V_reg_566[5]),
        .I4(\tmp_4_reg_2535[8]_i_2_n_2 ),
        .O(\tmp_4_reg_2535[7]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'hFFFFFEFF00000100)) 
    \tmp_4_reg_2535[8]_i_1 
       (.I0(t_V_reg_566[6]),
        .I1(t_V_reg_566[4]),
        .I2(t_V_reg_566[5]),
        .I3(\tmp_4_reg_2535[8]_i_2_n_2 ),
        .I4(t_V_reg_566[7]),
        .I5(t_V_reg_566[8]),
        .O(\tmp_4_reg_2535[8]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair127" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \tmp_4_reg_2535[8]_i_2 
       (.I0(t_V_reg_566[3]),
        .I1(t_V_reg_566[2]),
        .I2(t_V_reg_566[0]),
        .I3(t_V_reg_566[1]),
        .O(\tmp_4_reg_2535[8]_i_2_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair125" *) 
  LUT4 #(
    .INIT(16'hFB04)) 
    \tmp_4_reg_2535[9]_i_1 
       (.I0(t_V_reg_566[7]),
        .I1(\tmp_3_reg_2522[0]_i_2_n_2 ),
        .I2(t_V_reg_566[8]),
        .I3(t_V_reg_566[9]),
        .O(\tmp_4_reg_2535[9]_i_1_n_2 ));
  FDRE \tmp_4_reg_2535_reg[10] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\tmp_4_reg_2535[10]_i_1_n_2 ),
        .Q(\tmp_4_reg_2535_reg_n_2_[10] ),
        .R(1'b0));
  FDRE \tmp_4_reg_2535_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\tmp_4_reg_2535[1]_i_1_n_2 ),
        .Q(tmp_4_reg_2535),
        .R(1'b0));
  FDRE \tmp_4_reg_2535_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\tmp_4_reg_2535[4]_i_1_n_2 ),
        .Q(\tmp_4_reg_2535_reg_n_2_[4] ),
        .R(1'b0));
  FDRE \tmp_4_reg_2535_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\tmp_4_reg_2535[5]_i_1_n_2 ),
        .Q(\tmp_4_reg_2535_reg_n_2_[5] ),
        .R(1'b0));
  FDRE \tmp_4_reg_2535_reg[6] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\tmp_4_reg_2535[6]_i_1_n_2 ),
        .Q(\tmp_4_reg_2535_reg_n_2_[6] ),
        .R(1'b0));
  FDRE \tmp_4_reg_2535_reg[7] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\tmp_4_reg_2535[7]_i_1_n_2 ),
        .Q(\tmp_4_reg_2535_reg_n_2_[7] ),
        .R(1'b0));
  FDRE \tmp_4_reg_2535_reg[8] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\tmp_4_reg_2535[8]_i_1_n_2 ),
        .Q(\tmp_4_reg_2535_reg_n_2_[8] ),
        .R(1'b0));
  FDRE \tmp_4_reg_2535_reg[9] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(\tmp_4_reg_2535[9]_i_1_n_2 ),
        .Q(\tmp_4_reg_2535_reg_n_2_[9] ),
        .R(1'b0));
  FDRE \tmp_53_reg_2822_reg[0] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(p_Val2_10_0_0_2_fu_1493_p2_carry_n_9),
        .Q(tmp_53_reg_2822[0]),
        .R(1'b0));
  FDRE \tmp_53_reg_2822_reg[1] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(p_Val2_10_0_0_2_fu_1493_p2_carry_n_8),
        .Q(tmp_53_reg_2822[1]),
        .R(1'b0));
  FDRE \tmp_53_reg_2822_reg[2] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(p_Val2_10_0_0_2_fu_1493_p2_carry_n_7),
        .Q(tmp_53_reg_2822[2]),
        .R(1'b0));
  FDRE \tmp_53_reg_2822_reg[3] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(p_Val2_10_0_0_2_fu_1493_p2_carry_n_6),
        .Q(tmp_53_reg_2822[3]),
        .R(1'b0));
  FDRE \tmp_53_reg_2822_reg[4] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_9),
        .Q(tmp_53_reg_2822[4]),
        .R(1'b0));
  FDRE \tmp_53_reg_2822_reg[5] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_8),
        .Q(tmp_53_reg_2822[5]),
        .R(1'b0));
  FDRE \tmp_53_reg_2822_reg[6] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_7),
        .Q(tmp_53_reg_2822[6]),
        .R(1'b0));
  FDRE \tmp_53_reg_2822_reg[7] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_6),
        .Q(tmp_53_reg_2822[7]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h6)) 
    \tmp_54_reg_2827[2]_i_1 
       (.I0(src_kernel_win_0_va_3_fu_258[0]),
        .I1(src_kernel_win_0_va_3_fu_258[1]),
        .O(r_V_7_0_1_fu_1511_p2[2]));
  LUT3 #(
    .INIT(8'h1E)) 
    \tmp_54_reg_2827[3]_i_1 
       (.I0(src_kernel_win_0_va_3_fu_258[1]),
        .I1(src_kernel_win_0_va_3_fu_258[0]),
        .I2(src_kernel_win_0_va_3_fu_258[2]),
        .O(r_V_7_0_1_fu_1511_p2[3]));
  LUT4 #(
    .INIT(16'h01FE)) 
    \tmp_54_reg_2827[4]_i_1 
       (.I0(src_kernel_win_0_va_3_fu_258[2]),
        .I1(src_kernel_win_0_va_3_fu_258[0]),
        .I2(src_kernel_win_0_va_3_fu_258[1]),
        .I3(src_kernel_win_0_va_3_fu_258[3]),
        .O(r_V_7_0_1_fu_1511_p2[4]));
  LUT5 #(
    .INIT(32'h0001FFFE)) 
    \tmp_54_reg_2827[5]_i_1 
       (.I0(src_kernel_win_0_va_3_fu_258[3]),
        .I1(src_kernel_win_0_va_3_fu_258[1]),
        .I2(src_kernel_win_0_va_3_fu_258[0]),
        .I3(src_kernel_win_0_va_3_fu_258[2]),
        .I4(src_kernel_win_0_va_3_fu_258[4]),
        .O(r_V_7_0_1_fu_1511_p2[5]));
  LUT6 #(
    .INIT(64'h00000001FFFFFFFE)) 
    \tmp_54_reg_2827[6]_i_1 
       (.I0(src_kernel_win_0_va_3_fu_258[4]),
        .I1(src_kernel_win_0_va_3_fu_258[2]),
        .I2(src_kernel_win_0_va_3_fu_258[0]),
        .I3(src_kernel_win_0_va_3_fu_258[1]),
        .I4(src_kernel_win_0_va_3_fu_258[3]),
        .I5(src_kernel_win_0_va_3_fu_258[5]),
        .O(r_V_7_0_1_fu_1511_p2[6]));
  LUT2 #(
    .INIT(4'h6)) 
    \tmp_54_reg_2827[7]_i_1 
       (.I0(\tmp_54_reg_2827[7]_i_2_n_2 ),
        .I1(src_kernel_win_0_va_3_fu_258[6]),
        .O(r_V_7_0_1_fu_1511_p2[7]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \tmp_54_reg_2827[7]_i_2 
       (.I0(src_kernel_win_0_va_3_fu_258[4]),
        .I1(src_kernel_win_0_va_3_fu_258[2]),
        .I2(src_kernel_win_0_va_3_fu_258[0]),
        .I3(src_kernel_win_0_va_3_fu_258[1]),
        .I4(src_kernel_win_0_va_3_fu_258[3]),
        .I5(src_kernel_win_0_va_3_fu_258[5]),
        .O(\tmp_54_reg_2827[7]_i_2_n_2 ));
  FDRE \tmp_54_reg_2827_reg[1] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(src_kernel_win_0_va_3_fu_258[0]),
        .Q(tmp_54_reg_2827[1]),
        .R(1'b0));
  FDRE \tmp_54_reg_2827_reg[2] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(r_V_7_0_1_fu_1511_p2[2]),
        .Q(tmp_54_reg_2827[2]),
        .R(1'b0));
  FDRE \tmp_54_reg_2827_reg[3] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(r_V_7_0_1_fu_1511_p2[3]),
        .Q(tmp_54_reg_2827[3]),
        .R(1'b0));
  FDRE \tmp_54_reg_2827_reg[4] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(r_V_7_0_1_fu_1511_p2[4]),
        .Q(tmp_54_reg_2827[4]),
        .R(1'b0));
  FDRE \tmp_54_reg_2827_reg[5] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(r_V_7_0_1_fu_1511_p2[5]),
        .Q(tmp_54_reg_2827[5]),
        .R(1'b0));
  FDRE \tmp_54_reg_2827_reg[6] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(r_V_7_0_1_fu_1511_p2[6]),
        .Q(tmp_54_reg_2827[6]),
        .R(1'b0));
  FDRE \tmp_54_reg_2827_reg[7] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(r_V_7_0_1_fu_1511_p2[7]),
        .Q(tmp_54_reg_2827[7]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair151" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \tmp_56_reg_2832[1]_i_1 
       (.I0(src_kernel_win_0_va_1_fu_250[0]),
        .I1(src_kernel_win_0_va_1_fu_250[1]),
        .O(tmp_160_0_2_cast_fu_1539_p1[1]));
  (* SOFT_HLUTNM = "soft_lutpair151" *) 
  LUT3 #(
    .INIT(8'h1E)) 
    \tmp_56_reg_2832[2]_i_1 
       (.I0(src_kernel_win_0_va_1_fu_250[1]),
        .I1(src_kernel_win_0_va_1_fu_250[0]),
        .I2(src_kernel_win_0_va_1_fu_250[2]),
        .O(tmp_160_0_2_cast_fu_1539_p1[2]));
  (* SOFT_HLUTNM = "soft_lutpair128" *) 
  LUT4 #(
    .INIT(16'h01FE)) 
    \tmp_56_reg_2832[3]_i_1 
       (.I0(src_kernel_win_0_va_1_fu_250[2]),
        .I1(src_kernel_win_0_va_1_fu_250[0]),
        .I2(src_kernel_win_0_va_1_fu_250[1]),
        .I3(src_kernel_win_0_va_1_fu_250[3]),
        .O(tmp_160_0_2_cast_fu_1539_p1[3]));
  (* SOFT_HLUTNM = "soft_lutpair128" *) 
  LUT5 #(
    .INIT(32'h0001FFFE)) 
    \tmp_56_reg_2832[4]_i_1 
       (.I0(src_kernel_win_0_va_1_fu_250[3]),
        .I1(src_kernel_win_0_va_1_fu_250[1]),
        .I2(src_kernel_win_0_va_1_fu_250[0]),
        .I3(src_kernel_win_0_va_1_fu_250[2]),
        .I4(src_kernel_win_0_va_1_fu_250[4]),
        .O(tmp_160_0_2_cast_fu_1539_p1[4]));
  LUT6 #(
    .INIT(64'h00000001FFFFFFFE)) 
    \tmp_56_reg_2832[5]_i_1 
       (.I0(src_kernel_win_0_va_1_fu_250[4]),
        .I1(src_kernel_win_0_va_1_fu_250[2]),
        .I2(src_kernel_win_0_va_1_fu_250[0]),
        .I3(src_kernel_win_0_va_1_fu_250[1]),
        .I4(src_kernel_win_0_va_1_fu_250[3]),
        .I5(src_kernel_win_0_va_1_fu_250[5]),
        .O(tmp_160_0_2_cast_fu_1539_p1[5]));
  (* SOFT_HLUTNM = "soft_lutpair148" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \tmp_56_reg_2832[6]_i_1 
       (.I0(\tmp_56_reg_2832[7]_i_2_n_2 ),
        .I1(src_kernel_win_0_va_1_fu_250[6]),
        .O(tmp_160_0_2_cast_fu_1539_p1[6]));
  (* SOFT_HLUTNM = "soft_lutpair148" *) 
  LUT3 #(
    .INIT(8'h1E)) 
    \tmp_56_reg_2832[7]_i_1 
       (.I0(src_kernel_win_0_va_1_fu_250[6]),
        .I1(\tmp_56_reg_2832[7]_i_2_n_2 ),
        .I2(src_kernel_win_0_va_1_fu_250[7]),
        .O(tmp_160_0_2_cast_fu_1539_p1[7]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \tmp_56_reg_2832[7]_i_2 
       (.I0(src_kernel_win_0_va_1_fu_250[4]),
        .I1(src_kernel_win_0_va_1_fu_250[2]),
        .I2(src_kernel_win_0_va_1_fu_250[0]),
        .I3(src_kernel_win_0_va_1_fu_250[1]),
        .I4(src_kernel_win_0_va_1_fu_250[3]),
        .I5(src_kernel_win_0_va_1_fu_250[5]),
        .O(\tmp_56_reg_2832[7]_i_2_n_2 ));
  FDRE \tmp_56_reg_2832_reg[0] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(src_kernel_win_0_va_1_fu_250[0]),
        .Q(tmp_56_reg_2832[0]),
        .R(1'b0));
  FDRE \tmp_56_reg_2832_reg[1] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(tmp_160_0_2_cast_fu_1539_p1[1]),
        .Q(tmp_56_reg_2832[1]),
        .R(1'b0));
  FDRE \tmp_56_reg_2832_reg[2] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(tmp_160_0_2_cast_fu_1539_p1[2]),
        .Q(tmp_56_reg_2832[2]),
        .R(1'b0));
  FDRE \tmp_56_reg_2832_reg[3] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(tmp_160_0_2_cast_fu_1539_p1[3]),
        .Q(tmp_56_reg_2832[3]),
        .R(1'b0));
  FDRE \tmp_56_reg_2832_reg[4] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(tmp_160_0_2_cast_fu_1539_p1[4]),
        .Q(tmp_56_reg_2832[4]),
        .R(1'b0));
  FDRE \tmp_56_reg_2832_reg[5] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(tmp_160_0_2_cast_fu_1539_p1[5]),
        .Q(tmp_56_reg_2832[5]),
        .R(1'b0));
  FDRE \tmp_56_reg_2832_reg[6] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(tmp_160_0_2_cast_fu_1539_p1[6]),
        .Q(tmp_56_reg_2832[6]),
        .R(1'b0));
  FDRE \tmp_56_reg_2832_reg[7] 
       (.C(ap_clk),
        .CE(p_Val2_10_0_0_2_reg_28170),
        .D(tmp_160_0_2_cast_fu_1539_p1[7]),
        .Q(tmp_56_reg_2832[7]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    \tmp_57_reg_2927[1]_i_1 
       (.I0(ap_reg_pp0_iter3_or_cond_i_reg_2640),
        .I1(ap_block_pp0_stage0_subdone2_in),
        .O(isneg_1_reg_29320));
  FDRE \tmp_57_reg_2927_reg[0] 
       (.C(ap_clk),
        .CE(isneg_1_reg_29320),
        .D(p_Val2_2_fu_1946_p2[8]),
        .Q(tmp_57_reg_2927[0]),
        .R(1'b0));
  FDRE \tmp_57_reg_2927_reg[1] 
       (.C(ap_clk),
        .CE(isneg_1_reg_29320),
        .D(p_Val2_2_fu_1946_p2[9]),
        .Q(tmp_57_reg_2927[1]),
        .R(1'b0));
  FDRE \tmp_57_reg_2927_reg[2] 
       (.C(ap_clk),
        .CE(isneg_1_reg_29320),
        .D(p_Val2_2_fu_1946_p2[10]),
        .Q(tmp_57_reg_2927[2]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair126" *) 
  LUT5 #(
    .INIT(32'h00262626)) 
    \tmp_5_reg_555[0]_i_1 
       (.I0(tmp_5_reg_555[0]),
        .I1(ap_CS_fsm_state2),
        .I2(tmp_5_reg_555[1]),
        .I3(ap_reg_grp_Filter2D_fu_96_ap_start),
        .I4(\ap_CS_fsm_reg_n_2_[0] ),
        .O(\tmp_5_reg_555[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair126" *) 
  LUT5 #(
    .INIT(32'h006A6A6A)) 
    \tmp_5_reg_555[1]_i_1 
       (.I0(tmp_5_reg_555[1]),
        .I1(tmp_5_reg_555[0]),
        .I2(ap_CS_fsm_state2),
        .I3(ap_reg_grp_Filter2D_fu_96_ap_start),
        .I4(\ap_CS_fsm_reg_n_2_[0] ),
        .O(\tmp_5_reg_555[1]_i_1_n_2 ));
  FDRE \tmp_5_reg_555_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_5_reg_555[0]_i_1_n_2 ),
        .Q(tmp_5_reg_555[0]),
        .R(1'b0));
  FDRE \tmp_5_reg_555_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_5_reg_555[1]_i_1_n_2 ),
        .Q(tmp_5_reg_555[1]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hF0F0F0F0E0000000)) 
    \tmp_72_not_reg_2504[0]_i_1 
       (.I0(t_V_reg_566[4]),
        .I1(t_V_reg_566[5]),
        .I2(t_V_reg_566[9]),
        .I3(t_V_reg_566[6]),
        .I4(t_V_reg_566[7]),
        .I5(t_V_reg_566[8]),
        .O(tmp_72_not_fu_643_p2));
  FDRE \tmp_72_not_reg_2504_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm[3]),
        .D(tmp_72_not_fu_643_p2),
        .Q(tmp_72_not_reg_2504),
        .R(1'b0));
endmodule

module system_edge_detect_0_1_Filter2D_k_buf_0_eOg
   (D,
    ram_reg,
    col_buf_0_val_0_0_reg_27080,
    ap_block_pp0_stage0_subdone2_in,
    \ap_reg_pp0_iter2_reg_588_reg[0] ,
    ap_clk,
    \k_buf_0_val_3_addr_reg_2649_reg[10] ,
    ADDRBWRADDR,
    Q,
    ap_enable_reg_pp0_iter2,
    ap_reg_pp0_iter1_or_cond_i_i_reg_2607,
    \icmp_reg_2509_reg[0] ,
    tmp_1_reg_2500,
    \tmp_2_reg_2514_reg[0] ,
    \ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg[0] ,
    img1_data_stream_0_s_empty_n,
    img1_data_stream_2_s_empty_n,
    img1_data_stream_1_s_empty_n,
    ap_enable_reg_pp0_iter1,
    ap_reg_pp0_iter4_or_cond_i_reg_2640,
    ap_enable_reg_pp0_iter5_reg,
    img2_data_stream_0_s_full_n,
    img2_data_stream_1_s_full_n,
    img2_data_stream_2_s_full_n,
    \exitcond389_i_reg_2583_reg[0] ,
    or_cond_i_i_reg_2607,
    \ap_CS_fsm_reg[4] );
  output [7:0]D;
  output ram_reg;
  output col_buf_0_val_0_0_reg_27080;
  output ap_block_pp0_stage0_subdone2_in;
  output \ap_reg_pp0_iter2_reg_588_reg[0] ;
  input ap_clk;
  input [10:0]\k_buf_0_val_3_addr_reg_2649_reg[10] ;
  input [10:0]ADDRBWRADDR;
  input [7:0]Q;
  input ap_enable_reg_pp0_iter2;
  input ap_reg_pp0_iter1_or_cond_i_i_reg_2607;
  input \icmp_reg_2509_reg[0] ;
  input tmp_1_reg_2500;
  input \tmp_2_reg_2514_reg[0] ;
  input \ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg[0] ;
  input img1_data_stream_0_s_empty_n;
  input img1_data_stream_2_s_empty_n;
  input img1_data_stream_1_s_empty_n;
  input ap_enable_reg_pp0_iter1;
  input ap_reg_pp0_iter4_or_cond_i_reg_2640;
  input ap_enable_reg_pp0_iter5_reg;
  input img2_data_stream_0_s_full_n;
  input img2_data_stream_1_s_full_n;
  input img2_data_stream_2_s_full_n;
  input \exitcond389_i_reg_2583_reg[0] ;
  input or_cond_i_i_reg_2607;
  input [0:0]\ap_CS_fsm_reg[4] ;

  wire [10:0]ADDRBWRADDR;
  wire [7:0]D;
  wire [7:0]Q;
  wire [0:0]\ap_CS_fsm_reg[4] ;
  wire ap_block_pp0_stage0_subdone2_in;
  wire ap_clk;
  wire ap_enable_reg_pp0_iter1;
  wire ap_enable_reg_pp0_iter2;
  wire ap_enable_reg_pp0_iter5_reg;
  wire \ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg[0] ;
  wire ap_reg_pp0_iter1_or_cond_i_i_reg_2607;
  wire \ap_reg_pp0_iter2_reg_588_reg[0] ;
  wire ap_reg_pp0_iter4_or_cond_i_reg_2640;
  wire col_buf_0_val_0_0_reg_27080;
  wire \exitcond389_i_reg_2583_reg[0] ;
  wire \icmp_reg_2509_reg[0] ;
  wire img1_data_stream_0_s_empty_n;
  wire img1_data_stream_1_s_empty_n;
  wire img1_data_stream_2_s_empty_n;
  wire img2_data_stream_0_s_full_n;
  wire img2_data_stream_1_s_full_n;
  wire img2_data_stream_2_s_full_n;
  wire [10:0]\k_buf_0_val_3_addr_reg_2649_reg[10] ;
  wire or_cond_i_i_reg_2607;
  wire ram_reg;
  wire tmp_1_reg_2500;
  wire \tmp_2_reg_2514_reg[0] ;

  system_edge_detect_0_1_Filter2D_k_buf_0_eOg_ram_19 Filter2D_k_buf_0_eOg_ram_U
       (.ADDRBWRADDR(ADDRBWRADDR),
        .D(D),
        .Q(Q),
        .\ap_CS_fsm_reg[4] (\ap_CS_fsm_reg[4] ),
        .ap_block_pp0_stage0_subdone2_in(ap_block_pp0_stage0_subdone2_in),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp0_iter1(ap_enable_reg_pp0_iter1),
        .ap_enable_reg_pp0_iter2(ap_enable_reg_pp0_iter2),
        .ap_enable_reg_pp0_iter5_reg(ap_enable_reg_pp0_iter5_reg),
        .\ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg[0] (\ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg[0] ),
        .ap_reg_pp0_iter1_or_cond_i_i_reg_2607(ap_reg_pp0_iter1_or_cond_i_i_reg_2607),
        .\ap_reg_pp0_iter2_reg_588_reg[0] (\ap_reg_pp0_iter2_reg_588_reg[0] ),
        .ap_reg_pp0_iter4_or_cond_i_reg_2640(ap_reg_pp0_iter4_or_cond_i_reg_2640),
        .col_buf_0_val_0_0_reg_27080(col_buf_0_val_0_0_reg_27080),
        .\exitcond389_i_reg_2583_reg[0] (\exitcond389_i_reg_2583_reg[0] ),
        .\icmp_reg_2509_reg[0] (\icmp_reg_2509_reg[0] ),
        .img1_data_stream_0_s_empty_n(img1_data_stream_0_s_empty_n),
        .img1_data_stream_1_s_empty_n(img1_data_stream_1_s_empty_n),
        .img1_data_stream_2_s_empty_n(img1_data_stream_2_s_empty_n),
        .img2_data_stream_0_s_full_n(img2_data_stream_0_s_full_n),
        .img2_data_stream_1_s_full_n(img2_data_stream_1_s_full_n),
        .img2_data_stream_2_s_full_n(img2_data_stream_2_s_full_n),
        .\k_buf_0_val_3_addr_reg_2649_reg[10] (\k_buf_0_val_3_addr_reg_2649_reg[10] ),
        .or_cond_i_i_reg_2607(or_cond_i_i_reg_2607),
        .ram_reg_0(ram_reg),
        .tmp_1_reg_2500(tmp_1_reg_2500),
        .\tmp_2_reg_2514_reg[0] (\tmp_2_reg_2514_reg[0] ));
endmodule

(* ORIG_REF_NAME = "Filter2D_k_buf_0_eOg" *) 
module system_edge_detect_0_1_Filter2D_k_buf_0_eOg_16
   (D,
    ap_clk,
    \ap_CS_fsm_reg[4] ,
    Q,
    ADDRBWRADDR,
    ap_enable_reg_pp0_iter3,
    ap_block_pp0_stage0_subdone2_in,
    ap_reg_pp0_iter2_or_cond_i_i_reg_2607,
    \icmp_reg_2509_reg[0] ,
    tmp_1_reg_2500,
    \tmp_116_0_1_reg_2518_reg[0] ,
    \k_buf_0_val_3_load_reg_2703_reg[7] ,
    \ap_reg_pp0_iter2_reg_588_reg[7] );
  output [7:0]D;
  input ap_clk;
  input \ap_CS_fsm_reg[4] ;
  input [10:0]Q;
  input [10:0]ADDRBWRADDR;
  input ap_enable_reg_pp0_iter3;
  input ap_block_pp0_stage0_subdone2_in;
  input ap_reg_pp0_iter2_or_cond_i_i_reg_2607;
  input \icmp_reg_2509_reg[0] ;
  input tmp_1_reg_2500;
  input \tmp_116_0_1_reg_2518_reg[0] ;
  input [7:0]\k_buf_0_val_3_load_reg_2703_reg[7] ;
  input [7:0]\ap_reg_pp0_iter2_reg_588_reg[7] ;

  wire [10:0]ADDRBWRADDR;
  wire [7:0]D;
  wire [10:0]Q;
  wire \ap_CS_fsm_reg[4] ;
  wire ap_block_pp0_stage0_subdone2_in;
  wire ap_clk;
  wire ap_enable_reg_pp0_iter3;
  wire ap_reg_pp0_iter2_or_cond_i_i_reg_2607;
  wire [7:0]\ap_reg_pp0_iter2_reg_588_reg[7] ;
  wire \icmp_reg_2509_reg[0] ;
  wire [7:0]\k_buf_0_val_3_load_reg_2703_reg[7] ;
  wire \tmp_116_0_1_reg_2518_reg[0] ;
  wire tmp_1_reg_2500;

  system_edge_detect_0_1_Filter2D_k_buf_0_eOg_ram_18 Filter2D_k_buf_0_eOg_ram_U
       (.ADDRBWRADDR(ADDRBWRADDR),
        .D(D),
        .Q(Q),
        .\ap_CS_fsm_reg[4] (\ap_CS_fsm_reg[4] ),
        .ap_block_pp0_stage0_subdone2_in(ap_block_pp0_stage0_subdone2_in),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp0_iter3(ap_enable_reg_pp0_iter3),
        .ap_reg_pp0_iter2_or_cond_i_i_reg_2607(ap_reg_pp0_iter2_or_cond_i_i_reg_2607),
        .\ap_reg_pp0_iter2_reg_588_reg[7] (\ap_reg_pp0_iter2_reg_588_reg[7] ),
        .\icmp_reg_2509_reg[0] (\icmp_reg_2509_reg[0] ),
        .\k_buf_0_val_3_load_reg_2703_reg[7] (\k_buf_0_val_3_load_reg_2703_reg[7] ),
        .\tmp_116_0_1_reg_2518_reg[0] (\tmp_116_0_1_reg_2518_reg[0] ),
        .tmp_1_reg_2500(tmp_1_reg_2500));
endmodule

(* ORIG_REF_NAME = "Filter2D_k_buf_0_eOg" *) 
module system_edge_detect_0_1_Filter2D_k_buf_0_eOg_17
   (DOBDO,
    ADDRBWRADDR,
    ap_clk,
    \ap_CS_fsm_reg[4] ,
    Q,
    p_assign_2_reg_2622,
    ap_enable_reg_pp0_iter3,
    ap_block_pp0_stage0_subdone2_in,
    ap_reg_pp0_iter2_or_cond_i_i_reg_2607,
    \icmp_reg_2509_reg[0] ,
    tmp_1_reg_2500,
    \tmp_2_reg_2514_reg[0] ,
    \k_buf_0_val_4_load_reg_2716_reg[7] ,
    \ap_reg_pp0_iter2_reg_588_reg[7] ,
    \p_p2_i_i_cast_cast_reg_2612_reg[10] ,
    \ImagLoc_x_reg_2592_reg[10] ,
    or_cond_i_i_reg_2607,
    tmp_33_reg_2617,
    tmp_47_reg_2597,
    tmp_31_reg_2602);
  output [7:0]DOBDO;
  output [9:0]ADDRBWRADDR;
  input ap_clk;
  input \ap_CS_fsm_reg[4] ;
  input [10:0]Q;
  input [10:0]p_assign_2_reg_2622;
  input ap_enable_reg_pp0_iter3;
  input ap_block_pp0_stage0_subdone2_in;
  input ap_reg_pp0_iter2_or_cond_i_i_reg_2607;
  input \icmp_reg_2509_reg[0] ;
  input tmp_1_reg_2500;
  input \tmp_2_reg_2514_reg[0] ;
  input [7:0]\k_buf_0_val_4_load_reg_2716_reg[7] ;
  input [7:0]\ap_reg_pp0_iter2_reg_588_reg[7] ;
  input [9:0]\p_p2_i_i_cast_cast_reg_2612_reg[10] ;
  input [9:0]\ImagLoc_x_reg_2592_reg[10] ;
  input or_cond_i_i_reg_2607;
  input tmp_33_reg_2617;
  input tmp_47_reg_2597;
  input tmp_31_reg_2602;

  wire [9:0]ADDRBWRADDR;
  wire [7:0]DOBDO;
  wire [9:0]\ImagLoc_x_reg_2592_reg[10] ;
  wire [10:0]Q;
  wire \ap_CS_fsm_reg[4] ;
  wire ap_block_pp0_stage0_subdone2_in;
  wire ap_clk;
  wire ap_enable_reg_pp0_iter3;
  wire ap_reg_pp0_iter2_or_cond_i_i_reg_2607;
  wire [7:0]\ap_reg_pp0_iter2_reg_588_reg[7] ;
  wire \icmp_reg_2509_reg[0] ;
  wire [7:0]\k_buf_0_val_4_load_reg_2716_reg[7] ;
  wire or_cond_i_i_reg_2607;
  wire [10:0]p_assign_2_reg_2622;
  wire [9:0]\p_p2_i_i_cast_cast_reg_2612_reg[10] ;
  wire tmp_1_reg_2500;
  wire \tmp_2_reg_2514_reg[0] ;
  wire tmp_31_reg_2602;
  wire tmp_33_reg_2617;
  wire tmp_47_reg_2597;

  system_edge_detect_0_1_Filter2D_k_buf_0_eOg_ram Filter2D_k_buf_0_eOg_ram_U
       (.ADDRBWRADDR(ADDRBWRADDR),
        .DOBDO(DOBDO),
        .\ImagLoc_x_reg_2592_reg[10] (\ImagLoc_x_reg_2592_reg[10] ),
        .Q(Q),
        .\ap_CS_fsm_reg[4] (\ap_CS_fsm_reg[4] ),
        .ap_block_pp0_stage0_subdone2_in(ap_block_pp0_stage0_subdone2_in),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp0_iter3(ap_enable_reg_pp0_iter3),
        .ap_reg_pp0_iter2_or_cond_i_i_reg_2607(ap_reg_pp0_iter2_or_cond_i_i_reg_2607),
        .\ap_reg_pp0_iter2_reg_588_reg[7] (\ap_reg_pp0_iter2_reg_588_reg[7] ),
        .\icmp_reg_2509_reg[0] (\icmp_reg_2509_reg[0] ),
        .\k_buf_0_val_4_load_reg_2716_reg[7] (\k_buf_0_val_4_load_reg_2716_reg[7] ),
        .or_cond_i_i_reg_2607(or_cond_i_i_reg_2607),
        .p_assign_2_reg_2622(p_assign_2_reg_2622),
        .\p_p2_i_i_cast_cast_reg_2612_reg[10] (\p_p2_i_i_cast_cast_reg_2612_reg[10] ),
        .tmp_1_reg_2500(tmp_1_reg_2500),
        .\tmp_2_reg_2514_reg[0] (\tmp_2_reg_2514_reg[0] ),
        .tmp_31_reg_2602(tmp_31_reg_2602),
        .tmp_33_reg_2617(tmp_33_reg_2617),
        .tmp_47_reg_2597(tmp_47_reg_2597));
endmodule

module system_edge_detect_0_1_Filter2D_k_buf_0_eOg_ram
   (DOBDO,
    ADDRBWRADDR,
    ap_clk,
    \ap_CS_fsm_reg[4] ,
    Q,
    p_assign_2_reg_2622,
    ap_enable_reg_pp0_iter3,
    ap_block_pp0_stage0_subdone2_in,
    ap_reg_pp0_iter2_or_cond_i_i_reg_2607,
    \icmp_reg_2509_reg[0] ,
    tmp_1_reg_2500,
    \tmp_2_reg_2514_reg[0] ,
    \k_buf_0_val_4_load_reg_2716_reg[7] ,
    \ap_reg_pp0_iter2_reg_588_reg[7] ,
    \p_p2_i_i_cast_cast_reg_2612_reg[10] ,
    \ImagLoc_x_reg_2592_reg[10] ,
    or_cond_i_i_reg_2607,
    tmp_33_reg_2617,
    tmp_47_reg_2597,
    tmp_31_reg_2602);
  output [7:0]DOBDO;
  output [9:0]ADDRBWRADDR;
  input ap_clk;
  input \ap_CS_fsm_reg[4] ;
  input [10:0]Q;
  input [10:0]p_assign_2_reg_2622;
  input ap_enable_reg_pp0_iter3;
  input ap_block_pp0_stage0_subdone2_in;
  input ap_reg_pp0_iter2_or_cond_i_i_reg_2607;
  input \icmp_reg_2509_reg[0] ;
  input tmp_1_reg_2500;
  input \tmp_2_reg_2514_reg[0] ;
  input [7:0]\k_buf_0_val_4_load_reg_2716_reg[7] ;
  input [7:0]\ap_reg_pp0_iter2_reg_588_reg[7] ;
  input [9:0]\p_p2_i_i_cast_cast_reg_2612_reg[10] ;
  input [9:0]\ImagLoc_x_reg_2592_reg[10] ;
  input or_cond_i_i_reg_2607;
  input tmp_33_reg_2617;
  input tmp_47_reg_2597;
  input tmp_31_reg_2602;

  wire [9:0]ADDRBWRADDR;
  wire [7:0]DOBDO;
  wire [9:0]\ImagLoc_x_reg_2592_reg[10] ;
  wire [10:0]Q;
  wire \ap_CS_fsm_reg[4] ;
  wire ap_block_pp0_stage0_subdone2_in;
  wire ap_clk;
  wire ap_enable_reg_pp0_iter3;
  wire ap_reg_pp0_iter2_or_cond_i_i_reg_2607;
  wire [7:0]\ap_reg_pp0_iter2_reg_588_reg[7] ;
  wire \icmp_reg_2509_reg[0] ;
  wire [7:0]\k_buf_0_val_4_load_reg_2716_reg[7] ;
  wire k_buf_0_val_5_ce1;
  wire or_cond_i_i_reg_2607;
  wire [10:0]p_assign_2_reg_2622;
  wire [9:0]\p_p2_i_i_cast_cast_reg_2612_reg[10] ;
  wire ram_reg_i_13_n_2;
  wire ram_reg_i_2__0_n_2;
  wire ram_reg_i_3__0_n_2;
  wire ram_reg_i_4__0_n_2;
  wire ram_reg_i_5__0_n_2;
  wire ram_reg_i_6__0_n_2;
  wire ram_reg_i_7__0_n_2;
  wire ram_reg_i_8__0_n_2;
  wire ram_reg_i_9__0_n_2;
  wire tmp_1_reg_2500;
  wire \tmp_2_reg_2514_reg[0] ;
  wire tmp_31_reg_2602;
  wire tmp_33_reg_2617;
  wire tmp_47_reg_2597;
  wire [15:0]NLW_ram_reg_DOADO_UNCONNECTED;
  wire [15:8]NLW_ram_reg_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "10240" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    ram_reg
       (.ADDRARDADDR({Q,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({ADDRBWRADDR,p_assign_2_reg_2622[0],1'b1,1'b1,1'b1}),
        .CLKARDCLK(ap_clk),
        .CLKBWRCLK(ap_clk),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,ram_reg_i_2__0_n_2,ram_reg_i_3__0_n_2,ram_reg_i_4__0_n_2,ram_reg_i_5__0_n_2,ram_reg_i_6__0_n_2,ram_reg_i_7__0_n_2,ram_reg_i_8__0_n_2,ram_reg_i_9__0_n_2}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0}),
        .DOADO(NLW_ram_reg_DOADO_UNCONNECTED[15:0]),
        .DOBDO({NLW_ram_reg_DOBDO_UNCONNECTED[15:8],DOBDO}),
        .DOPADOP(NLW_ram_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(k_buf_0_val_5_ce1),
        .ENBWREN(\ap_CS_fsm_reg[4] ),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({k_buf_0_val_5_ce1,k_buf_0_val_5_ce1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ram_reg_i_10
       (.I0(\p_p2_i_i_cast_cast_reg_2612_reg[10] [2]),
        .I1(ram_reg_i_13_n_2),
        .I2(\ImagLoc_x_reg_2592_reg[10] [2]),
        .I3(or_cond_i_i_reg_2607),
        .I4(p_assign_2_reg_2622[3]),
        .O(ADDRBWRADDR[2]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ram_reg_i_11
       (.I0(\p_p2_i_i_cast_cast_reg_2612_reg[10] [1]),
        .I1(ram_reg_i_13_n_2),
        .I2(\ImagLoc_x_reg_2592_reg[10] [1]),
        .I3(or_cond_i_i_reg_2607),
        .I4(p_assign_2_reg_2622[2]),
        .O(ADDRBWRADDR[1]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ram_reg_i_12
       (.I0(\p_p2_i_i_cast_cast_reg_2612_reg[10] [0]),
        .I1(ram_reg_i_13_n_2),
        .I2(\ImagLoc_x_reg_2592_reg[10] [0]),
        .I3(or_cond_i_i_reg_2607),
        .I4(p_assign_2_reg_2622[1]),
        .O(ADDRBWRADDR[0]));
  LUT3 #(
    .INIT(8'h8A)) 
    ram_reg_i_13
       (.I0(tmp_33_reg_2617),
        .I1(tmp_47_reg_2597),
        .I2(tmp_31_reg_2602),
        .O(ram_reg_i_13_n_2));
  LUT6 #(
    .INIT(64'h8080008080000000)) 
    ram_reg_i_1__1
       (.I0(ap_enable_reg_pp0_iter3),
        .I1(ap_block_pp0_stage0_subdone2_in),
        .I2(ap_reg_pp0_iter2_or_cond_i_i_reg_2607),
        .I3(\icmp_reg_2509_reg[0] ),
        .I4(tmp_1_reg_2500),
        .I5(\tmp_2_reg_2514_reg[0] ),
        .O(k_buf_0_val_5_ce1));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_2__0
       (.I0(\k_buf_0_val_4_load_reg_2716_reg[7] [7]),
        .I1(\icmp_reg_2509_reg[0] ),
        .I2(tmp_1_reg_2500),
        .I3(\ap_reg_pp0_iter2_reg_588_reg[7] [7]),
        .O(ram_reg_i_2__0_n_2));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_3__0
       (.I0(\k_buf_0_val_4_load_reg_2716_reg[7] [6]),
        .I1(\icmp_reg_2509_reg[0] ),
        .I2(tmp_1_reg_2500),
        .I3(\ap_reg_pp0_iter2_reg_588_reg[7] [6]),
        .O(ram_reg_i_3__0_n_2));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ram_reg_i_3__1
       (.I0(\p_p2_i_i_cast_cast_reg_2612_reg[10] [9]),
        .I1(ram_reg_i_13_n_2),
        .I2(\ImagLoc_x_reg_2592_reg[10] [9]),
        .I3(or_cond_i_i_reg_2607),
        .I4(p_assign_2_reg_2622[10]),
        .O(ADDRBWRADDR[9]));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_4__0
       (.I0(\k_buf_0_val_4_load_reg_2716_reg[7] [5]),
        .I1(\icmp_reg_2509_reg[0] ),
        .I2(tmp_1_reg_2500),
        .I3(\ap_reg_pp0_iter2_reg_588_reg[7] [5]),
        .O(ram_reg_i_4__0_n_2));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ram_reg_i_4__1
       (.I0(\p_p2_i_i_cast_cast_reg_2612_reg[10] [8]),
        .I1(ram_reg_i_13_n_2),
        .I2(\ImagLoc_x_reg_2592_reg[10] [8]),
        .I3(or_cond_i_i_reg_2607),
        .I4(p_assign_2_reg_2622[9]),
        .O(ADDRBWRADDR[8]));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_5__0
       (.I0(\k_buf_0_val_4_load_reg_2716_reg[7] [4]),
        .I1(\icmp_reg_2509_reg[0] ),
        .I2(tmp_1_reg_2500),
        .I3(\ap_reg_pp0_iter2_reg_588_reg[7] [4]),
        .O(ram_reg_i_5__0_n_2));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ram_reg_i_5__1
       (.I0(\p_p2_i_i_cast_cast_reg_2612_reg[10] [7]),
        .I1(ram_reg_i_13_n_2),
        .I2(\ImagLoc_x_reg_2592_reg[10] [7]),
        .I3(or_cond_i_i_reg_2607),
        .I4(p_assign_2_reg_2622[8]),
        .O(ADDRBWRADDR[7]));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_6__0
       (.I0(\k_buf_0_val_4_load_reg_2716_reg[7] [3]),
        .I1(\icmp_reg_2509_reg[0] ),
        .I2(tmp_1_reg_2500),
        .I3(\ap_reg_pp0_iter2_reg_588_reg[7] [3]),
        .O(ram_reg_i_6__0_n_2));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ram_reg_i_6__1
       (.I0(\p_p2_i_i_cast_cast_reg_2612_reg[10] [6]),
        .I1(ram_reg_i_13_n_2),
        .I2(\ImagLoc_x_reg_2592_reg[10] [6]),
        .I3(or_cond_i_i_reg_2607),
        .I4(p_assign_2_reg_2622[7]),
        .O(ADDRBWRADDR[6]));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_7__0
       (.I0(\k_buf_0_val_4_load_reg_2716_reg[7] [2]),
        .I1(\icmp_reg_2509_reg[0] ),
        .I2(tmp_1_reg_2500),
        .I3(\ap_reg_pp0_iter2_reg_588_reg[7] [2]),
        .O(ram_reg_i_7__0_n_2));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ram_reg_i_7__1
       (.I0(\p_p2_i_i_cast_cast_reg_2612_reg[10] [5]),
        .I1(ram_reg_i_13_n_2),
        .I2(\ImagLoc_x_reg_2592_reg[10] [5]),
        .I3(or_cond_i_i_reg_2607),
        .I4(p_assign_2_reg_2622[6]),
        .O(ADDRBWRADDR[5]));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_8__0
       (.I0(\k_buf_0_val_4_load_reg_2716_reg[7] [1]),
        .I1(\icmp_reg_2509_reg[0] ),
        .I2(tmp_1_reg_2500),
        .I3(\ap_reg_pp0_iter2_reg_588_reg[7] [1]),
        .O(ram_reg_i_8__0_n_2));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ram_reg_i_8__1
       (.I0(\p_p2_i_i_cast_cast_reg_2612_reg[10] [4]),
        .I1(ram_reg_i_13_n_2),
        .I2(\ImagLoc_x_reg_2592_reg[10] [4]),
        .I3(or_cond_i_i_reg_2607),
        .I4(p_assign_2_reg_2622[5]),
        .O(ADDRBWRADDR[4]));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_9__0
       (.I0(\k_buf_0_val_4_load_reg_2716_reg[7] [0]),
        .I1(\icmp_reg_2509_reg[0] ),
        .I2(tmp_1_reg_2500),
        .I3(\ap_reg_pp0_iter2_reg_588_reg[7] [0]),
        .O(ram_reg_i_9__0_n_2));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ram_reg_i_9__1
       (.I0(\p_p2_i_i_cast_cast_reg_2612_reg[10] [3]),
        .I1(ram_reg_i_13_n_2),
        .I2(\ImagLoc_x_reg_2592_reg[10] [3]),
        .I3(or_cond_i_i_reg_2607),
        .I4(p_assign_2_reg_2622[4]),
        .O(ADDRBWRADDR[3]));
endmodule

(* ORIG_REF_NAME = "Filter2D_k_buf_0_eOg_ram" *) 
module system_edge_detect_0_1_Filter2D_k_buf_0_eOg_ram_18
   (D,
    ap_clk,
    \ap_CS_fsm_reg[4] ,
    Q,
    ADDRBWRADDR,
    ap_enable_reg_pp0_iter3,
    ap_block_pp0_stage0_subdone2_in,
    ap_reg_pp0_iter2_or_cond_i_i_reg_2607,
    \icmp_reg_2509_reg[0] ,
    tmp_1_reg_2500,
    \tmp_116_0_1_reg_2518_reg[0] ,
    \k_buf_0_val_3_load_reg_2703_reg[7] ,
    \ap_reg_pp0_iter2_reg_588_reg[7] );
  output [7:0]D;
  input ap_clk;
  input \ap_CS_fsm_reg[4] ;
  input [10:0]Q;
  input [10:0]ADDRBWRADDR;
  input ap_enable_reg_pp0_iter3;
  input ap_block_pp0_stage0_subdone2_in;
  input ap_reg_pp0_iter2_or_cond_i_i_reg_2607;
  input \icmp_reg_2509_reg[0] ;
  input tmp_1_reg_2500;
  input \tmp_116_0_1_reg_2518_reg[0] ;
  input [7:0]\k_buf_0_val_3_load_reg_2703_reg[7] ;
  input [7:0]\ap_reg_pp0_iter2_reg_588_reg[7] ;

  wire [10:0]ADDRBWRADDR;
  wire [7:0]D;
  wire [10:0]Q;
  wire \ap_CS_fsm_reg[4] ;
  wire ap_block_pp0_stage0_subdone2_in;
  wire ap_clk;
  wire ap_enable_reg_pp0_iter3;
  wire ap_reg_pp0_iter2_or_cond_i_i_reg_2607;
  wire [7:0]\ap_reg_pp0_iter2_reg_588_reg[7] ;
  wire \icmp_reg_2509_reg[0] ;
  wire [7:0]\k_buf_0_val_3_load_reg_2703_reg[7] ;
  wire k_buf_0_val_4_ce1;
  wire ram_reg_i_2_n_2;
  wire ram_reg_i_3_n_2;
  wire ram_reg_i_4_n_2;
  wire ram_reg_i_5_n_2;
  wire ram_reg_i_6_n_2;
  wire ram_reg_i_7_n_2;
  wire ram_reg_i_8_n_2;
  wire ram_reg_i_9_n_2;
  wire \tmp_116_0_1_reg_2518_reg[0] ;
  wire tmp_1_reg_2500;
  wire [15:0]NLW_ram_reg_DOADO_UNCONNECTED;
  wire [15:8]NLW_ram_reg_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "10240" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    ram_reg
       (.ADDRARDADDR({Q,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({ADDRBWRADDR,1'b1,1'b1,1'b1}),
        .CLKARDCLK(ap_clk),
        .CLKBWRCLK(ap_clk),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,ram_reg_i_2_n_2,ram_reg_i_3_n_2,ram_reg_i_4_n_2,ram_reg_i_5_n_2,ram_reg_i_6_n_2,ram_reg_i_7_n_2,ram_reg_i_8_n_2,ram_reg_i_9_n_2}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0}),
        .DOADO(NLW_ram_reg_DOADO_UNCONNECTED[15:0]),
        .DOBDO({NLW_ram_reg_DOBDO_UNCONNECTED[15:8],D}),
        .DOPADOP(NLW_ram_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(k_buf_0_val_4_ce1),
        .ENBWREN(\ap_CS_fsm_reg[4] ),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({k_buf_0_val_4_ce1,k_buf_0_val_4_ce1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h8080008080000000)) 
    ram_reg_i_1__0
       (.I0(ap_enable_reg_pp0_iter3),
        .I1(ap_block_pp0_stage0_subdone2_in),
        .I2(ap_reg_pp0_iter2_or_cond_i_i_reg_2607),
        .I3(\icmp_reg_2509_reg[0] ),
        .I4(tmp_1_reg_2500),
        .I5(\tmp_116_0_1_reg_2518_reg[0] ),
        .O(k_buf_0_val_4_ce1));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_2
       (.I0(\k_buf_0_val_3_load_reg_2703_reg[7] [7]),
        .I1(\icmp_reg_2509_reg[0] ),
        .I2(tmp_1_reg_2500),
        .I3(\ap_reg_pp0_iter2_reg_588_reg[7] [7]),
        .O(ram_reg_i_2_n_2));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_3
       (.I0(\k_buf_0_val_3_load_reg_2703_reg[7] [6]),
        .I1(\icmp_reg_2509_reg[0] ),
        .I2(tmp_1_reg_2500),
        .I3(\ap_reg_pp0_iter2_reg_588_reg[7] [6]),
        .O(ram_reg_i_3_n_2));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_4
       (.I0(\k_buf_0_val_3_load_reg_2703_reg[7] [5]),
        .I1(\icmp_reg_2509_reg[0] ),
        .I2(tmp_1_reg_2500),
        .I3(\ap_reg_pp0_iter2_reg_588_reg[7] [5]),
        .O(ram_reg_i_4_n_2));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_5
       (.I0(\k_buf_0_val_3_load_reg_2703_reg[7] [4]),
        .I1(\icmp_reg_2509_reg[0] ),
        .I2(tmp_1_reg_2500),
        .I3(\ap_reg_pp0_iter2_reg_588_reg[7] [4]),
        .O(ram_reg_i_5_n_2));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_6
       (.I0(\k_buf_0_val_3_load_reg_2703_reg[7] [3]),
        .I1(\icmp_reg_2509_reg[0] ),
        .I2(tmp_1_reg_2500),
        .I3(\ap_reg_pp0_iter2_reg_588_reg[7] [3]),
        .O(ram_reg_i_6_n_2));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_7
       (.I0(\k_buf_0_val_3_load_reg_2703_reg[7] [2]),
        .I1(\icmp_reg_2509_reg[0] ),
        .I2(tmp_1_reg_2500),
        .I3(\ap_reg_pp0_iter2_reg_588_reg[7] [2]),
        .O(ram_reg_i_7_n_2));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_8
       (.I0(\k_buf_0_val_3_load_reg_2703_reg[7] [1]),
        .I1(\icmp_reg_2509_reg[0] ),
        .I2(tmp_1_reg_2500),
        .I3(\ap_reg_pp0_iter2_reg_588_reg[7] [1]),
        .O(ram_reg_i_8_n_2));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_9
       (.I0(\k_buf_0_val_3_load_reg_2703_reg[7] [0]),
        .I1(\icmp_reg_2509_reg[0] ),
        .I2(tmp_1_reg_2500),
        .I3(\ap_reg_pp0_iter2_reg_588_reg[7] [0]),
        .O(ram_reg_i_9_n_2));
endmodule

(* ORIG_REF_NAME = "Filter2D_k_buf_0_eOg_ram" *) 
module system_edge_detect_0_1_Filter2D_k_buf_0_eOg_ram_19
   (D,
    ram_reg_0,
    col_buf_0_val_0_0_reg_27080,
    ap_block_pp0_stage0_subdone2_in,
    \ap_reg_pp0_iter2_reg_588_reg[0] ,
    ap_clk,
    \k_buf_0_val_3_addr_reg_2649_reg[10] ,
    ADDRBWRADDR,
    Q,
    ap_enable_reg_pp0_iter2,
    ap_reg_pp0_iter1_or_cond_i_i_reg_2607,
    \icmp_reg_2509_reg[0] ,
    tmp_1_reg_2500,
    \tmp_2_reg_2514_reg[0] ,
    \ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg[0] ,
    img1_data_stream_0_s_empty_n,
    img1_data_stream_2_s_empty_n,
    img1_data_stream_1_s_empty_n,
    ap_enable_reg_pp0_iter1,
    ap_reg_pp0_iter4_or_cond_i_reg_2640,
    ap_enable_reg_pp0_iter5_reg,
    img2_data_stream_0_s_full_n,
    img2_data_stream_1_s_full_n,
    img2_data_stream_2_s_full_n,
    \exitcond389_i_reg_2583_reg[0] ,
    or_cond_i_i_reg_2607,
    \ap_CS_fsm_reg[4] );
  output [7:0]D;
  output ram_reg_0;
  output col_buf_0_val_0_0_reg_27080;
  output ap_block_pp0_stage0_subdone2_in;
  output \ap_reg_pp0_iter2_reg_588_reg[0] ;
  input ap_clk;
  input [10:0]\k_buf_0_val_3_addr_reg_2649_reg[10] ;
  input [10:0]ADDRBWRADDR;
  input [7:0]Q;
  input ap_enable_reg_pp0_iter2;
  input ap_reg_pp0_iter1_or_cond_i_i_reg_2607;
  input \icmp_reg_2509_reg[0] ;
  input tmp_1_reg_2500;
  input \tmp_2_reg_2514_reg[0] ;
  input \ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg[0] ;
  input img1_data_stream_0_s_empty_n;
  input img1_data_stream_2_s_empty_n;
  input img1_data_stream_1_s_empty_n;
  input ap_enable_reg_pp0_iter1;
  input ap_reg_pp0_iter4_or_cond_i_reg_2640;
  input ap_enable_reg_pp0_iter5_reg;
  input img2_data_stream_0_s_full_n;
  input img2_data_stream_1_s_full_n;
  input img2_data_stream_2_s_full_n;
  input \exitcond389_i_reg_2583_reg[0] ;
  input or_cond_i_i_reg_2607;
  input [0:0]\ap_CS_fsm_reg[4] ;

  wire [10:0]ADDRBWRADDR;
  wire [7:0]D;
  wire [7:0]Q;
  wire [0:0]\ap_CS_fsm_reg[4] ;
  wire ap_block_pp0_stage0_subdone2_in;
  wire ap_clk;
  wire ap_enable_reg_pp0_iter1;
  wire ap_enable_reg_pp0_iter1_i_3_n_2;
  wire ap_enable_reg_pp0_iter2;
  wire ap_enable_reg_pp0_iter5_reg;
  wire \ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg[0] ;
  wire ap_reg_pp0_iter1_or_cond_i_i_reg_2607;
  wire \ap_reg_pp0_iter2_reg_588_reg[0] ;
  wire ap_reg_pp0_iter4_or_cond_i_reg_2640;
  wire col_buf_0_val_0_0_reg_27080;
  wire \exitcond389_i_reg_2583_reg[0] ;
  wire \icmp_reg_2509_reg[0] ;
  wire img1_data_stream_0_s_empty_n;
  wire img1_data_stream_1_s_empty_n;
  wire img1_data_stream_2_s_empty_n;
  wire img2_data_stream_0_s_full_n;
  wire img2_data_stream_1_s_full_n;
  wire img2_data_stream_2_s_full_n;
  wire [10:0]\k_buf_0_val_3_addr_reg_2649_reg[10] ;
  wire k_buf_0_val_3_ce1;
  wire or_cond_i_i_reg_2607;
  wire ram_reg_0;
  wire tmp_1_reg_2500;
  wire \tmp_2_reg_2514_reg[0] ;
  wire [15:0]NLW_ram_reg_DOADO_UNCONNECTED;
  wire [15:8]NLW_ram_reg_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_ram_reg_DOPBDOP_UNCONNECTED;

  LUT6 #(
    .INIT(64'h0000000080FFFFFF)) 
    ap_enable_reg_pp0_iter1_i_1__2
       (.I0(img1_data_stream_0_s_empty_n),
        .I1(img1_data_stream_2_s_empty_n),
        .I2(img1_data_stream_1_s_empty_n),
        .I3(ap_enable_reg_pp0_iter1),
        .I4(\ap_reg_pp0_iter2_reg_588_reg[0] ),
        .I5(ap_enable_reg_pp0_iter1_i_3_n_2),
        .O(ap_block_pp0_stage0_subdone2_in));
  LUT4 #(
    .INIT(16'h4044)) 
    ap_enable_reg_pp0_iter1_i_2
       (.I0(\exitcond389_i_reg_2583_reg[0] ),
        .I1(or_cond_i_i_reg_2607),
        .I2(tmp_1_reg_2500),
        .I3(\icmp_reg_2509_reg[0] ),
        .O(\ap_reg_pp0_iter2_reg_588_reg[0] ));
  LUT5 #(
    .INIT(32'h08888888)) 
    ap_enable_reg_pp0_iter1_i_3
       (.I0(ap_reg_pp0_iter4_or_cond_i_reg_2640),
        .I1(ap_enable_reg_pp0_iter5_reg),
        .I2(img2_data_stream_0_s_full_n),
        .I3(img2_data_stream_1_s_full_n),
        .I4(img2_data_stream_2_s_full_n),
        .O(ap_enable_reg_pp0_iter1_i_3_n_2));
  LUT2 #(
    .INIT(4'h2)) 
    \col_buf_0_val_1_0_reg_2721[7]_i_1 
       (.I0(ap_block_pp0_stage0_subdone2_in),
        .I1(\ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg[0] ),
        .O(col_buf_0_val_0_0_reg_27080));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "10240" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "2047" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    ram_reg
       (.ADDRARDADDR({\k_buf_0_val_3_addr_reg_2649_reg[10] ,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({ADDRBWRADDR,1'b1,1'b1,1'b1}),
        .CLKARDCLK(ap_clk),
        .CLKBWRCLK(ap_clk),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0}),
        .DOADO(NLW_ram_reg_DOADO_UNCONNECTED[15:0]),
        .DOBDO({NLW_ram_reg_DOBDO_UNCONNECTED[15:8],D}),
        .DOPADOP(NLW_ram_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_ram_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(k_buf_0_val_3_ce1),
        .ENBWREN(ram_reg_0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({k_buf_0_val_3_ce1,k_buf_0_val_3_ce1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h8080008080000000)) 
    ram_reg_i_1
       (.I0(ap_enable_reg_pp0_iter2),
        .I1(col_buf_0_val_0_0_reg_27080),
        .I2(ap_reg_pp0_iter1_or_cond_i_i_reg_2607),
        .I3(\icmp_reg_2509_reg[0] ),
        .I4(tmp_1_reg_2500),
        .I5(\tmp_2_reg_2514_reg[0] ),
        .O(k_buf_0_val_3_ce1));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_i_2__1
       (.I0(ap_block_pp0_stage0_subdone2_in),
        .I1(\ap_CS_fsm_reg[4] ),
        .I2(ap_enable_reg_pp0_iter1),
        .O(ram_reg_0));
endmodule

module system_edge_detect_0_1_Mat2AXIvideo
   (stream_out_TVALID,
    E,
    \mOutPtr_reg[1] ,
    \mOutPtr_reg[1]_0 ,
    stream_out_TUSER,
    stream_out_TLAST,
    stream_out_TDATA,
    ap_rst_n_inv,
    ap_clk,
    ap_rst_n,
    shiftReg_ce,
    Mat2AXIvideo_U0_ap_start,
    stream_out_TREADY,
    img3_data_stream_0_s_empty_n,
    img3_data_stream_2_s_empty_n,
    img3_data_stream_1_s_empty_n,
    D);
  output stream_out_TVALID;
  output [0:0]E;
  output \mOutPtr_reg[1] ;
  output \mOutPtr_reg[1]_0 ;
  output [0:0]stream_out_TUSER;
  output [0:0]stream_out_TLAST;
  output [23:0]stream_out_TDATA;
  input ap_rst_n_inv;
  input ap_clk;
  input ap_rst_n;
  input shiftReg_ce;
  input Mat2AXIvideo_U0_ap_start;
  input stream_out_TREADY;
  input img3_data_stream_0_s_empty_n;
  input img3_data_stream_2_s_empty_n;
  input img3_data_stream_1_s_empty_n;
  input [23:0]D;

  wire AXI_video_strm_V_data_V_1_ack_in;
  wire AXI_video_strm_V_data_V_1_load_A;
  wire AXI_video_strm_V_data_V_1_load_B;
  wire [23:0]AXI_video_strm_V_data_V_1_payload_A;
  wire [23:0]AXI_video_strm_V_data_V_1_payload_B;
  wire AXI_video_strm_V_data_V_1_sel;
  wire AXI_video_strm_V_data_V_1_sel_rd_i_1_n_2;
  wire AXI_video_strm_V_data_V_1_sel_wr;
  wire AXI_video_strm_V_data_V_1_sel_wr_i_1_n_2;
  wire [1:1]AXI_video_strm_V_data_V_1_state;
  wire \AXI_video_strm_V_data_V_1_state[0]_i_1_n_2 ;
  wire \AXI_video_strm_V_data_V_1_state_reg_n_2_[0] ;
  wire \AXI_video_strm_V_dest_V_1_state[0]_i_1_n_2 ;
  wire \AXI_video_strm_V_dest_V_1_state[1]_i_1_n_2 ;
  wire \AXI_video_strm_V_dest_V_1_state_reg_n_2_[1] ;
  wire \AXI_video_strm_V_id_V_1_state[0]_i_1_n_2 ;
  wire \AXI_video_strm_V_id_V_1_state[1]_i_1_n_2 ;
  wire \AXI_video_strm_V_id_V_1_state_reg_n_2_[0] ;
  wire \AXI_video_strm_V_id_V_1_state_reg_n_2_[1] ;
  wire \AXI_video_strm_V_keep_V_1_state[0]_i_1_n_2 ;
  wire \AXI_video_strm_V_keep_V_1_state[1]_i_1_n_2 ;
  wire \AXI_video_strm_V_keep_V_1_state_reg_n_2_[0] ;
  wire \AXI_video_strm_V_keep_V_1_state_reg_n_2_[1] ;
  wire AXI_video_strm_V_last_V_1_ack_in;
  wire AXI_video_strm_V_last_V_1_payload_A;
  wire \AXI_video_strm_V_last_V_1_payload_A[0]_i_1_n_2 ;
  wire AXI_video_strm_V_last_V_1_payload_B;
  wire \AXI_video_strm_V_last_V_1_payload_B[0]_i_1_n_2 ;
  wire AXI_video_strm_V_last_V_1_sel;
  wire AXI_video_strm_V_last_V_1_sel_rd_i_1_n_2;
  wire AXI_video_strm_V_last_V_1_sel_wr;
  wire AXI_video_strm_V_last_V_1_sel_wr_i_1_n_2;
  wire [1:1]AXI_video_strm_V_last_V_1_state;
  wire \AXI_video_strm_V_last_V_1_state[0]_i_1_n_2 ;
  wire \AXI_video_strm_V_last_V_1_state_reg_n_2_[0] ;
  wire \AXI_video_strm_V_strb_V_1_state[0]_i_1_n_2 ;
  wire \AXI_video_strm_V_strb_V_1_state[1]_i_1_n_2 ;
  wire \AXI_video_strm_V_strb_V_1_state_reg_n_2_[0] ;
  wire \AXI_video_strm_V_strb_V_1_state_reg_n_2_[1] ;
  wire AXI_video_strm_V_user_V_1_ack_in;
  wire AXI_video_strm_V_user_V_1_payload_A;
  wire \AXI_video_strm_V_user_V_1_payload_A[0]_i_1_n_2 ;
  wire AXI_video_strm_V_user_V_1_payload_B;
  wire \AXI_video_strm_V_user_V_1_payload_B[0]_i_1_n_2 ;
  wire AXI_video_strm_V_user_V_1_sel;
  wire AXI_video_strm_V_user_V_1_sel_rd_i_1_n_2;
  wire AXI_video_strm_V_user_V_1_sel_wr;
  wire AXI_video_strm_V_user_V_1_sel_wr_i_1_n_2;
  wire [1:1]AXI_video_strm_V_user_V_1_state;
  wire \AXI_video_strm_V_user_V_1_state[0]_i_1_n_2 ;
  wire \AXI_video_strm_V_user_V_1_state_reg_n_2_[0] ;
  wire [23:0]D;
  wire [0:0]E;
  wire Mat2AXIvideo_U0_ap_start;
  wire \ap_CS_fsm[0]_i_2__2_n_2 ;
  wire \ap_CS_fsm[0]_i_3__0_n_2 ;
  wire \ap_CS_fsm[1]_i_2__0_n_2 ;
  wire \ap_CS_fsm[1]_i_3_n_2 ;
  wire \ap_CS_fsm[2]_i_2__1_n_2 ;
  wire \ap_CS_fsm[3]_i_2__2_n_2 ;
  wire ap_CS_fsm_pp0_stage0;
  wire \ap_CS_fsm_reg_n_2_[0] ;
  wire ap_CS_fsm_state2;
  wire ap_CS_fsm_state6;
  wire [3:0]ap_NS_fsm;
  wire ap_clk;
  wire ap_enable_reg_pp0_iter0;
  wire ap_enable_reg_pp0_iter0_i_1__2_n_2;
  wire ap_enable_reg_pp0_iter1_i_1__1_n_2;
  wire ap_enable_reg_pp0_iter1_reg_n_2;
  wire ap_enable_reg_pp0_iter2_i_1_n_2;
  wire ap_enable_reg_pp0_iter2_reg_n_2;
  wire ap_reg_pp0_iter1_exitcond_reg_270;
  wire \ap_reg_pp0_iter1_exitcond_reg_270[0]_i_1_n_2 ;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire axi_last_V_reg_2790;
  wire \axi_last_V_reg_279[0]_i_1_n_2 ;
  wire \axi_last_V_reg_279[0]_i_2_n_2 ;
  wire \axi_last_V_reg_279_reg_n_2_[0] ;
  wire exitcond_fu_216_p2;
  wire \exitcond_reg_270[0]_i_1_n_2 ;
  wire \exitcond_reg_270[0]_i_3_n_2 ;
  wire \exitcond_reg_270[0]_i_4_n_2 ;
  wire \exitcond_reg_270[0]_i_5_n_2 ;
  wire \exitcond_reg_270[0]_i_6_n_2 ;
  wire \exitcond_reg_270_reg_n_2_[0] ;
  wire [9:0]i_V_fu_210_p2;
  wire [9:0]i_V_reg_265;
  wire i_V_reg_2650;
  wire \i_V_reg_265[9]_i_3_n_2 ;
  wire \i_V_reg_265[9]_i_4_n_2 ;
  wire img3_data_stream_0_s_empty_n;
  wire img3_data_stream_1_s_empty_n;
  wire img3_data_stream_2_s_empty_n;
  wire [10:0]j_V_fu_222_p2;
  wire \mOutPtr_reg[1] ;
  wire \mOutPtr_reg[1]_0 ;
  wire shiftReg_ce;
  wire [23:0]stream_out_TDATA;
  wire [0:0]stream_out_TLAST;
  wire stream_out_TREADY;
  wire [0:0]stream_out_TUSER;
  wire stream_out_TVALID;
  wire t_V_1_reg_188;
  wire t_V_1_reg_1880;
  wire \t_V_1_reg_188[10]_i_5_n_2 ;
  wire [10:0]t_V_1_reg_188_reg__0;
  wire t_V_reg_177;
  wire \t_V_reg_177_reg_n_2_[0] ;
  wire \t_V_reg_177_reg_n_2_[1] ;
  wire \t_V_reg_177_reg_n_2_[2] ;
  wire \t_V_reg_177_reg_n_2_[3] ;
  wire \t_V_reg_177_reg_n_2_[4] ;
  wire \t_V_reg_177_reg_n_2_[5] ;
  wire \t_V_reg_177_reg_n_2_[6] ;
  wire \t_V_reg_177_reg_n_2_[7] ;
  wire \t_V_reg_177_reg_n_2_[8] ;
  wire \t_V_reg_177_reg_n_2_[9] ;
  wire tmp_user_V_fu_126;
  wire \tmp_user_V_fu_126[0]_i_1_n_2 ;

  LUT3 #(
    .INIT(8'h45)) 
    \AXI_video_strm_V_data_V_1_payload_A[23]_i_1 
       (.I0(AXI_video_strm_V_data_V_1_sel_wr),
        .I1(AXI_video_strm_V_data_V_1_ack_in),
        .I2(\AXI_video_strm_V_data_V_1_state_reg_n_2_[0] ),
        .O(AXI_video_strm_V_data_V_1_load_A));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[0]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[0]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[10] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[10]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[10]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[11] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[11]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[11]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[12] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[12]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[12]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[13] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[13]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[13]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[14] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[14]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[14]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[15] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[15]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[15]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[16] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[16]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[16]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[17] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[17]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[17]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[18] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[18]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[18]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[19] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[19]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[19]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[1]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[1]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[20] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[20]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[20]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[21] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[21]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[21]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[22] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[22]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[22]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[23] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[23]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[23]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[2]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[2]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[3]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[3]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[4]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[4]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[5] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[5]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[5]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[6] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[6]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[6]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[7] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[7]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[7]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[8] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[8]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[8]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_A_reg[9] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_A),
        .D(D[9]),
        .Q(AXI_video_strm_V_data_V_1_payload_A[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h8A)) 
    \AXI_video_strm_V_data_V_1_payload_B[23]_i_1 
       (.I0(AXI_video_strm_V_data_V_1_sel_wr),
        .I1(AXI_video_strm_V_data_V_1_ack_in),
        .I2(\AXI_video_strm_V_data_V_1_state_reg_n_2_[0] ),
        .O(AXI_video_strm_V_data_V_1_load_B));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[0]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[0]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[10] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[10]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[10]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[11] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[11]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[11]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[12] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[12]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[12]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[13] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[13]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[13]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[14] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[14]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[14]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[15] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[15]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[15]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[16] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[16]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[16]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[17] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[17]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[17]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[18] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[18]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[18]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[19] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[19]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[19]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[1]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[1]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[20] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[20]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[20]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[21] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[21]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[21]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[22] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[22]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[22]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[23] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[23]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[23]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[2]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[2]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[3]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[3]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[4]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[4]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[5] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[5]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[5]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[6] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[6]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[6]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[7] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[7]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[7]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[8] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[8]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[8]),
        .R(1'b0));
  FDRE \AXI_video_strm_V_data_V_1_payload_B_reg[9] 
       (.C(ap_clk),
        .CE(AXI_video_strm_V_data_V_1_load_B),
        .D(D[9]),
        .Q(AXI_video_strm_V_data_V_1_payload_B[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_data_V_1_sel_rd_i_1
       (.I0(\AXI_video_strm_V_data_V_1_state_reg_n_2_[0] ),
        .I1(stream_out_TREADY),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(AXI_video_strm_V_data_V_1_sel_rd_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_data_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_data_V_1_sel_rd_i_1_n_2),
        .Q(AXI_video_strm_V_data_V_1_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT2 #(
    .INIT(4'h6)) 
    AXI_video_strm_V_data_V_1_sel_wr_i_1
       (.I0(\mOutPtr_reg[1] ),
        .I1(AXI_video_strm_V_data_V_1_sel_wr),
        .O(AXI_video_strm_V_data_V_1_sel_wr_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_data_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_data_V_1_sel_wr_i_1_n_2),
        .Q(AXI_video_strm_V_data_V_1_sel_wr),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT4 #(
    .INIT(16'hAEEE)) 
    \AXI_video_strm_V_data_V_1_state[0]_i_1 
       (.I0(\mOutPtr_reg[1] ),
        .I1(\AXI_video_strm_V_data_V_1_state_reg_n_2_[0] ),
        .I2(stream_out_TREADY),
        .I3(AXI_video_strm_V_data_V_1_ack_in),
        .O(\AXI_video_strm_V_data_V_1_state[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT4 #(
    .INIT(16'hBBFB)) 
    \AXI_video_strm_V_data_V_1_state[1]_i_1 
       (.I0(stream_out_TREADY),
        .I1(\AXI_video_strm_V_data_V_1_state_reg_n_2_[0] ),
        .I2(AXI_video_strm_V_data_V_1_ack_in),
        .I3(\mOutPtr_reg[1] ),
        .O(AXI_video_strm_V_data_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_data_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_data_V_1_state[0]_i_1_n_2 ),
        .Q(\AXI_video_strm_V_data_V_1_state_reg_n_2_[0] ),
        .R(ap_rst_n_inv));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_data_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_data_V_1_state),
        .Q(AXI_video_strm_V_data_V_1_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hB0F0A000)) 
    \AXI_video_strm_V_dest_V_1_state[0]_i_1 
       (.I0(\mOutPtr_reg[1] ),
        .I1(stream_out_TREADY),
        .I2(ap_rst_n),
        .I3(\AXI_video_strm_V_dest_V_1_state_reg_n_2_[1] ),
        .I4(stream_out_TVALID),
        .O(\AXI_video_strm_V_dest_V_1_state[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \AXI_video_strm_V_dest_V_1_state[0]_i_2 
       (.I0(\exitcond_reg_270[0]_i_3_n_2 ),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(\exitcond_reg_270_reg_n_2_[0] ),
        .I3(ap_enable_reg_pp0_iter1_reg_n_2),
        .O(\mOutPtr_reg[1] ));
  LUT4 #(
    .INIT(16'hF4FF)) 
    \AXI_video_strm_V_dest_V_1_state[1]_i_1 
       (.I0(\mOutPtr_reg[1] ),
        .I1(\AXI_video_strm_V_dest_V_1_state_reg_n_2_[1] ),
        .I2(stream_out_TREADY),
        .I3(stream_out_TVALID),
        .O(\AXI_video_strm_V_dest_V_1_state[1]_i_1_n_2 ));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_dest_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_dest_V_1_state[0]_i_1_n_2 ),
        .Q(stream_out_TVALID),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_dest_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_dest_V_1_state[1]_i_1_n_2 ),
        .Q(\AXI_video_strm_V_dest_V_1_state_reg_n_2_[1] ),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hB0F0A000)) 
    \AXI_video_strm_V_id_V_1_state[0]_i_1 
       (.I0(\mOutPtr_reg[1] ),
        .I1(stream_out_TREADY),
        .I2(ap_rst_n),
        .I3(\AXI_video_strm_V_id_V_1_state_reg_n_2_[1] ),
        .I4(\AXI_video_strm_V_id_V_1_state_reg_n_2_[0] ),
        .O(\AXI_video_strm_V_id_V_1_state[0]_i_1_n_2 ));
  LUT4 #(
    .INIT(16'hF4FF)) 
    \AXI_video_strm_V_id_V_1_state[1]_i_1 
       (.I0(\mOutPtr_reg[1] ),
        .I1(\AXI_video_strm_V_id_V_1_state_reg_n_2_[1] ),
        .I2(stream_out_TREADY),
        .I3(\AXI_video_strm_V_id_V_1_state_reg_n_2_[0] ),
        .O(\AXI_video_strm_V_id_V_1_state[1]_i_1_n_2 ));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_id_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_id_V_1_state[0]_i_1_n_2 ),
        .Q(\AXI_video_strm_V_id_V_1_state_reg_n_2_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_id_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_id_V_1_state[1]_i_1_n_2 ),
        .Q(\AXI_video_strm_V_id_V_1_state_reg_n_2_[1] ),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hB0F0A000)) 
    \AXI_video_strm_V_keep_V_1_state[0]_i_1 
       (.I0(\mOutPtr_reg[1] ),
        .I1(stream_out_TREADY),
        .I2(ap_rst_n),
        .I3(\AXI_video_strm_V_keep_V_1_state_reg_n_2_[1] ),
        .I4(\AXI_video_strm_V_keep_V_1_state_reg_n_2_[0] ),
        .O(\AXI_video_strm_V_keep_V_1_state[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT4 #(
    .INIT(16'hF4FF)) 
    \AXI_video_strm_V_keep_V_1_state[1]_i_1 
       (.I0(\mOutPtr_reg[1] ),
        .I1(\AXI_video_strm_V_keep_V_1_state_reg_n_2_[1] ),
        .I2(stream_out_TREADY),
        .I3(\AXI_video_strm_V_keep_V_1_state_reg_n_2_[0] ),
        .O(\AXI_video_strm_V_keep_V_1_state[1]_i_1_n_2 ));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_keep_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_keep_V_1_state[0]_i_1_n_2 ),
        .Q(\AXI_video_strm_V_keep_V_1_state_reg_n_2_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_keep_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_keep_V_1_state[1]_i_1_n_2 ),
        .Q(\AXI_video_strm_V_keep_V_1_state_reg_n_2_[1] ),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hEFEE2022)) 
    \AXI_video_strm_V_last_V_1_payload_A[0]_i_1 
       (.I0(\axi_last_V_reg_279_reg_n_2_[0] ),
        .I1(AXI_video_strm_V_last_V_1_sel_wr),
        .I2(AXI_video_strm_V_last_V_1_ack_in),
        .I3(\AXI_video_strm_V_last_V_1_state_reg_n_2_[0] ),
        .I4(AXI_video_strm_V_last_V_1_payload_A),
        .O(\AXI_video_strm_V_last_V_1_payload_A[0]_i_1_n_2 ));
  FDRE \AXI_video_strm_V_last_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_last_V_1_payload_A[0]_i_1_n_2 ),
        .Q(AXI_video_strm_V_last_V_1_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hBFBB8088)) 
    \AXI_video_strm_V_last_V_1_payload_B[0]_i_1 
       (.I0(\axi_last_V_reg_279_reg_n_2_[0] ),
        .I1(AXI_video_strm_V_last_V_1_sel_wr),
        .I2(AXI_video_strm_V_last_V_1_ack_in),
        .I3(\AXI_video_strm_V_last_V_1_state_reg_n_2_[0] ),
        .I4(AXI_video_strm_V_last_V_1_payload_B),
        .O(\AXI_video_strm_V_last_V_1_payload_B[0]_i_1_n_2 ));
  FDRE \AXI_video_strm_V_last_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_last_V_1_payload_B[0]_i_1_n_2 ),
        .Q(AXI_video_strm_V_last_V_1_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_last_V_1_sel_rd_i_1
       (.I0(\AXI_video_strm_V_last_V_1_state_reg_n_2_[0] ),
        .I1(stream_out_TREADY),
        .I2(AXI_video_strm_V_last_V_1_sel),
        .O(AXI_video_strm_V_last_V_1_sel_rd_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_last_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_last_V_1_sel_rd_i_1_n_2),
        .Q(AXI_video_strm_V_last_V_1_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_last_V_1_sel_wr_i_1
       (.I0(\mOutPtr_reg[1] ),
        .I1(AXI_video_strm_V_last_V_1_ack_in),
        .I2(AXI_video_strm_V_last_V_1_sel_wr),
        .O(AXI_video_strm_V_last_V_1_sel_wr_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_last_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_last_V_1_sel_wr_i_1_n_2),
        .Q(AXI_video_strm_V_last_V_1_sel_wr),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT4 #(
    .INIT(16'hAECC)) 
    \AXI_video_strm_V_last_V_1_state[0]_i_1 
       (.I0(\mOutPtr_reg[1] ),
        .I1(\AXI_video_strm_V_last_V_1_state_reg_n_2_[0] ),
        .I2(stream_out_TREADY),
        .I3(AXI_video_strm_V_last_V_1_ack_in),
        .O(\AXI_video_strm_V_last_V_1_state[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT4 #(
    .INIT(16'hBBFB)) 
    \AXI_video_strm_V_last_V_1_state[1]_i_1 
       (.I0(stream_out_TREADY),
        .I1(\AXI_video_strm_V_last_V_1_state_reg_n_2_[0] ),
        .I2(AXI_video_strm_V_last_V_1_ack_in),
        .I3(\mOutPtr_reg[1] ),
        .O(AXI_video_strm_V_last_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_last_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_last_V_1_state[0]_i_1_n_2 ),
        .Q(\AXI_video_strm_V_last_V_1_state_reg_n_2_[0] ),
        .R(ap_rst_n_inv));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_last_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_last_V_1_state),
        .Q(AXI_video_strm_V_last_V_1_ack_in),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hB0F0A000)) 
    \AXI_video_strm_V_strb_V_1_state[0]_i_1 
       (.I0(\mOutPtr_reg[1] ),
        .I1(stream_out_TREADY),
        .I2(ap_rst_n),
        .I3(\AXI_video_strm_V_strb_V_1_state_reg_n_2_[1] ),
        .I4(\AXI_video_strm_V_strb_V_1_state_reg_n_2_[0] ),
        .O(\AXI_video_strm_V_strb_V_1_state[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT4 #(
    .INIT(16'hF4FF)) 
    \AXI_video_strm_V_strb_V_1_state[1]_i_1 
       (.I0(\mOutPtr_reg[1] ),
        .I1(\AXI_video_strm_V_strb_V_1_state_reg_n_2_[1] ),
        .I2(stream_out_TREADY),
        .I3(\AXI_video_strm_V_strb_V_1_state_reg_n_2_[0] ),
        .O(\AXI_video_strm_V_strb_V_1_state[1]_i_1_n_2 ));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_strb_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_strb_V_1_state[0]_i_1_n_2 ),
        .Q(\AXI_video_strm_V_strb_V_1_state_reg_n_2_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_strb_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_strb_V_1_state[1]_i_1_n_2 ),
        .Q(\AXI_video_strm_V_strb_V_1_state_reg_n_2_[1] ),
        .R(ap_rst_n_inv));
  LUT5 #(
    .INIT(32'hEFEE2022)) 
    \AXI_video_strm_V_user_V_1_payload_A[0]_i_1 
       (.I0(tmp_user_V_fu_126),
        .I1(AXI_video_strm_V_user_V_1_sel_wr),
        .I2(AXI_video_strm_V_user_V_1_ack_in),
        .I3(\AXI_video_strm_V_user_V_1_state_reg_n_2_[0] ),
        .I4(AXI_video_strm_V_user_V_1_payload_A),
        .O(\AXI_video_strm_V_user_V_1_payload_A[0]_i_1_n_2 ));
  FDRE \AXI_video_strm_V_user_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_user_V_1_payload_A[0]_i_1_n_2 ),
        .Q(AXI_video_strm_V_user_V_1_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hBFBB8088)) 
    \AXI_video_strm_V_user_V_1_payload_B[0]_i_1 
       (.I0(tmp_user_V_fu_126),
        .I1(AXI_video_strm_V_user_V_1_sel_wr),
        .I2(AXI_video_strm_V_user_V_1_ack_in),
        .I3(\AXI_video_strm_V_user_V_1_state_reg_n_2_[0] ),
        .I4(AXI_video_strm_V_user_V_1_payload_B),
        .O(\AXI_video_strm_V_user_V_1_payload_B[0]_i_1_n_2 ));
  FDRE \AXI_video_strm_V_user_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_user_V_1_payload_B[0]_i_1_n_2 ),
        .Q(AXI_video_strm_V_user_V_1_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_user_V_1_sel_rd_i_1
       (.I0(\AXI_video_strm_V_user_V_1_state_reg_n_2_[0] ),
        .I1(stream_out_TREADY),
        .I2(AXI_video_strm_V_user_V_1_sel),
        .O(AXI_video_strm_V_user_V_1_sel_rd_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_user_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_user_V_1_sel_rd_i_1_n_2),
        .Q(AXI_video_strm_V_user_V_1_sel),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT3 #(
    .INIT(8'h78)) 
    AXI_video_strm_V_user_V_1_sel_wr_i_1
       (.I0(\mOutPtr_reg[1] ),
        .I1(AXI_video_strm_V_user_V_1_ack_in),
        .I2(AXI_video_strm_V_user_V_1_sel_wr),
        .O(AXI_video_strm_V_user_V_1_sel_wr_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    AXI_video_strm_V_user_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_user_V_1_sel_wr_i_1_n_2),
        .Q(AXI_video_strm_V_user_V_1_sel_wr),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT4 #(
    .INIT(16'hAECC)) 
    \AXI_video_strm_V_user_V_1_state[0]_i_1 
       (.I0(\mOutPtr_reg[1] ),
        .I1(\AXI_video_strm_V_user_V_1_state_reg_n_2_[0] ),
        .I2(stream_out_TREADY),
        .I3(AXI_video_strm_V_user_V_1_ack_in),
        .O(\AXI_video_strm_V_user_V_1_state[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT4 #(
    .INIT(16'hBBFB)) 
    \AXI_video_strm_V_user_V_1_state[1]_i_1 
       (.I0(stream_out_TREADY),
        .I1(\AXI_video_strm_V_user_V_1_state_reg_n_2_[0] ),
        .I2(AXI_video_strm_V_user_V_1_ack_in),
        .I3(\mOutPtr_reg[1] ),
        .O(AXI_video_strm_V_user_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_user_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\AXI_video_strm_V_user_V_1_state[0]_i_1_n_2 ),
        .Q(\AXI_video_strm_V_user_V_1_state_reg_n_2_[0] ),
        .R(ap_rst_n_inv));
  FDRE #(
    .INIT(1'b0)) 
    \AXI_video_strm_V_user_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(AXI_video_strm_V_user_V_1_state),
        .Q(AXI_video_strm_V_user_V_1_ack_in),
        .R(ap_rst_n_inv));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT4 #(
    .INIT(16'h44F4)) 
    \ap_CS_fsm[0]_i_1__4 
       (.I0(\ap_CS_fsm[0]_i_2__2_n_2 ),
        .I1(i_V_reg_2650),
        .I2(\ap_CS_fsm_reg_n_2_[0] ),
        .I3(Mat2AXIvideo_U0_ap_start),
        .O(ap_NS_fsm[0]));
  LUT5 #(
    .INIT(32'hFFFFFBFF)) 
    \ap_CS_fsm[0]_i_2__2 
       (.I0(\t_V_reg_177_reg_n_2_[8] ),
        .I1(\t_V_reg_177_reg_n_2_[4] ),
        .I2(\t_V_reg_177_reg_n_2_[3] ),
        .I3(\t_V_reg_177_reg_n_2_[9] ),
        .I4(\ap_CS_fsm[0]_i_3__0_n_2 ),
        .O(\ap_CS_fsm[0]_i_2__2_n_2 ));
  LUT6 #(
    .INIT(64'hFFFFFEFFFFFFFFFF)) 
    \ap_CS_fsm[0]_i_3__0 
       (.I0(\t_V_reg_177_reg_n_2_[1] ),
        .I1(\t_V_reg_177_reg_n_2_[0] ),
        .I2(\t_V_reg_177_reg_n_2_[5] ),
        .I3(\t_V_reg_177_reg_n_2_[6] ),
        .I4(\t_V_reg_177_reg_n_2_[2] ),
        .I5(\t_V_reg_177_reg_n_2_[7] ),
        .O(\ap_CS_fsm[0]_i_3__0_n_2 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFF007F00)) 
    \ap_CS_fsm[1]_i_1__4 
       (.I0(\AXI_video_strm_V_strb_V_1_state_reg_n_2_[1] ),
        .I1(\AXI_video_strm_V_keep_V_1_state_reg_n_2_[1] ),
        .I2(AXI_video_strm_V_user_V_1_ack_in),
        .I3(ap_CS_fsm_state2),
        .I4(\ap_CS_fsm[1]_i_2__0_n_2 ),
        .I5(\ap_CS_fsm[1]_i_3_n_2 ),
        .O(ap_NS_fsm[1]));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \ap_CS_fsm[1]_i_2__0 
       (.I0(AXI_video_strm_V_last_V_1_ack_in),
        .I1(\AXI_video_strm_V_id_V_1_state_reg_n_2_[1] ),
        .I2(\AXI_video_strm_V_dest_V_1_state_reg_n_2_[1] ),
        .I3(AXI_video_strm_V_data_V_1_ack_in),
        .O(\ap_CS_fsm[1]_i_2__0_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    \ap_CS_fsm[1]_i_3 
       (.I0(ap_CS_fsm_state6),
        .I1(\ap_CS_fsm_reg_n_2_[0] ),
        .I2(Mat2AXIvideo_U0_ap_start),
        .O(\ap_CS_fsm[1]_i_3_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \ap_CS_fsm[2]_i_1__3 
       (.I0(\ap_CS_fsm[2]_i_2__1_n_2 ),
        .I1(\ap_CS_fsm[3]_i_2__2_n_2 ),
        .I2(ap_CS_fsm_pp0_stage0),
        .O(ap_NS_fsm[2]));
  LUT2 #(
    .INIT(4'h8)) 
    \ap_CS_fsm[2]_i_2__1 
       (.I0(\ap_CS_fsm[0]_i_2__2_n_2 ),
        .I1(i_V_reg_2650),
        .O(\ap_CS_fsm[2]_i_2__1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \ap_CS_fsm[3]_i_1__2 
       (.I0(ap_CS_fsm_pp0_stage0),
        .I1(\ap_CS_fsm[3]_i_2__2_n_2 ),
        .O(ap_NS_fsm[3]));
  LUT5 #(
    .INIT(32'h11101010)) 
    \ap_CS_fsm[3]_i_2__2 
       (.I0(\exitcond_reg_270[0]_i_3_n_2 ),
        .I1(ap_enable_reg_pp0_iter1_reg_n_2),
        .I2(ap_enable_reg_pp0_iter2_reg_n_2),
        .I3(ap_enable_reg_pp0_iter0),
        .I4(exitcond_fu_216_p2),
        .O(\ap_CS_fsm[3]_i_2__2_n_2 ));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[0]),
        .Q(\ap_CS_fsm_reg_n_2_[0] ),
        .S(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ap_CS_fsm_state2),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[2]),
        .Q(ap_CS_fsm_pp0_stage0),
        .R(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[3]),
        .Q(ap_CS_fsm_state6),
        .R(ap_rst_n_inv));
  LUT6 #(
    .INIT(64'hB0B0B000F0F0F000)) 
    ap_enable_reg_pp0_iter0_i_1__2
       (.I0(\exitcond_reg_270[0]_i_3_n_2 ),
        .I1(exitcond_fu_216_p2),
        .I2(ap_rst_n),
        .I3(\ap_CS_fsm[2]_i_2__1_n_2 ),
        .I4(ap_enable_reg_pp0_iter0),
        .I5(ap_CS_fsm_pp0_stage0),
        .O(ap_enable_reg_pp0_iter0_i_1__2_n_2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter0_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter0_i_1__2_n_2),
        .Q(ap_enable_reg_pp0_iter0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hC044C000)) 
    ap_enable_reg_pp0_iter1_i_1__1
       (.I0(exitcond_fu_216_p2),
        .I1(ap_rst_n),
        .I2(ap_enable_reg_pp0_iter1_reg_n_2),
        .I3(\exitcond_reg_270[0]_i_3_n_2 ),
        .I4(ap_enable_reg_pp0_iter0),
        .O(ap_enable_reg_pp0_iter1_i_1__1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter1_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter1_i_1__1_n_2),
        .Q(ap_enable_reg_pp0_iter1_reg_n_2),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h40CC4000)) 
    ap_enable_reg_pp0_iter2_i_1
       (.I0(\ap_CS_fsm[2]_i_2__1_n_2 ),
        .I1(ap_rst_n),
        .I2(ap_enable_reg_pp0_iter2_reg_n_2),
        .I3(\exitcond_reg_270[0]_i_3_n_2 ),
        .I4(ap_enable_reg_pp0_iter1_reg_n_2),
        .O(ap_enable_reg_pp0_iter2_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter2_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter2_i_1_n_2),
        .Q(ap_enable_reg_pp0_iter2_reg_n_2),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    \ap_reg_pp0_iter1_exitcond_reg_270[0]_i_1 
       (.I0(\exitcond_reg_270_reg_n_2_[0] ),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(\exitcond_reg_270[0]_i_3_n_2 ),
        .I3(ap_reg_pp0_iter1_exitcond_reg_270),
        .O(\ap_reg_pp0_iter1_exitcond_reg_270[0]_i_1_n_2 ));
  FDRE \ap_reg_pp0_iter1_exitcond_reg_270_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\ap_reg_pp0_iter1_exitcond_reg_270[0]_i_1_n_2 ),
        .Q(ap_reg_pp0_iter1_exitcond_reg_270),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h000C0000AAAAAAAA)) 
    \axi_last_V_reg_279[0]_i_1 
       (.I0(\axi_last_V_reg_279_reg_n_2_[0] ),
        .I1(\axi_last_V_reg_279[0]_i_2_n_2 ),
        .I2(t_V_1_reg_188_reg__0[8]),
        .I3(t_V_1_reg_188_reg__0[9]),
        .I4(t_V_1_reg_188_reg__0[10]),
        .I5(axi_last_V_reg_2790),
        .O(\axi_last_V_reg_279[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \axi_last_V_reg_279[0]_i_2 
       (.I0(t_V_1_reg_188_reg__0[7]),
        .I1(\t_V_1_reg_188[10]_i_5_n_2 ),
        .I2(t_V_1_reg_188_reg__0[6]),
        .O(\axi_last_V_reg_279[0]_i_2_n_2 ));
  FDRE \axi_last_V_reg_279_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\axi_last_V_reg_279[0]_i_1_n_2 ),
        .Q(\axi_last_V_reg_279_reg_n_2_[0] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    \exitcond_reg_270[0]_i_1 
       (.I0(exitcond_fu_216_p2),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(\exitcond_reg_270[0]_i_3_n_2 ),
        .I3(\exitcond_reg_270_reg_n_2_[0] ),
        .O(\exitcond_reg_270[0]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'h00000001)) 
    \exitcond_reg_270[0]_i_2 
       (.I0(t_V_1_reg_188_reg__0[6]),
        .I1(t_V_1_reg_188_reg__0[7]),
        .I2(t_V_1_reg_188_reg__0[9]),
        .I3(\exitcond_reg_270[0]_i_4_n_2 ),
        .I4(\exitcond_reg_270[0]_i_5_n_2 ),
        .O(exitcond_fu_216_p2));
  LUT5 #(
    .INIT(32'h00007FFF)) 
    \exitcond_reg_270[0]_i_3 
       (.I0(AXI_video_strm_V_data_V_1_ack_in),
        .I1(img3_data_stream_0_s_empty_n),
        .I2(img3_data_stream_2_s_empty_n),
        .I3(img3_data_stream_1_s_empty_n),
        .I4(\exitcond_reg_270[0]_i_6_n_2 ),
        .O(\exitcond_reg_270[0]_i_3_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT4 #(
    .INIT(16'hFFEF)) 
    \exitcond_reg_270[0]_i_4 
       (.I0(t_V_1_reg_188_reg__0[1]),
        .I1(t_V_1_reg_188_reg__0[0]),
        .I2(t_V_1_reg_188_reg__0[10]),
        .I3(t_V_1_reg_188_reg__0[5]),
        .O(\exitcond_reg_270[0]_i_4_n_2 ));
  LUT4 #(
    .INIT(16'hFFEF)) 
    \exitcond_reg_270[0]_i_5 
       (.I0(t_V_1_reg_188_reg__0[3]),
        .I1(t_V_1_reg_188_reg__0[2]),
        .I2(t_V_1_reg_188_reg__0[8]),
        .I3(t_V_1_reg_188_reg__0[4]),
        .O(\exitcond_reg_270[0]_i_5_n_2 ));
  LUT5 #(
    .INIT(32'hBBBBB0BB)) 
    \exitcond_reg_270[0]_i_6 
       (.I0(\exitcond_reg_270_reg_n_2_[0] ),
        .I1(ap_enable_reg_pp0_iter1_reg_n_2),
        .I2(AXI_video_strm_V_data_V_1_ack_in),
        .I3(ap_enable_reg_pp0_iter2_reg_n_2),
        .I4(ap_reg_pp0_iter1_exitcond_reg_270),
        .O(\exitcond_reg_270[0]_i_6_n_2 ));
  FDRE \exitcond_reg_270_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\exitcond_reg_270[0]_i_1_n_2 ),
        .Q(\exitcond_reg_270_reg_n_2_[0] ),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \i_V_reg_265[0]_i_1 
       (.I0(\t_V_reg_177_reg_n_2_[0] ),
        .O(i_V_fu_210_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_V_reg_265[1]_i_1 
       (.I0(\t_V_reg_177_reg_n_2_[0] ),
        .I1(\t_V_reg_177_reg_n_2_[1] ),
        .O(i_V_fu_210_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \i_V_reg_265[2]_i_1 
       (.I0(\t_V_reg_177_reg_n_2_[2] ),
        .I1(\t_V_reg_177_reg_n_2_[1] ),
        .I2(\t_V_reg_177_reg_n_2_[0] ),
        .O(i_V_fu_210_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \i_V_reg_265[3]_i_1 
       (.I0(\t_V_reg_177_reg_n_2_[3] ),
        .I1(\t_V_reg_177_reg_n_2_[0] ),
        .I2(\t_V_reg_177_reg_n_2_[1] ),
        .I3(\t_V_reg_177_reg_n_2_[2] ),
        .O(i_V_fu_210_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \i_V_reg_265[4]_i_1 
       (.I0(\t_V_reg_177_reg_n_2_[4] ),
        .I1(\t_V_reg_177_reg_n_2_[2] ),
        .I2(\t_V_reg_177_reg_n_2_[1] ),
        .I3(\t_V_reg_177_reg_n_2_[0] ),
        .I4(\t_V_reg_177_reg_n_2_[3] ),
        .O(i_V_fu_210_p2[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \i_V_reg_265[5]_i_1 
       (.I0(\t_V_reg_177_reg_n_2_[5] ),
        .I1(\t_V_reg_177_reg_n_2_[3] ),
        .I2(\t_V_reg_177_reg_n_2_[0] ),
        .I3(\t_V_reg_177_reg_n_2_[1] ),
        .I4(\t_V_reg_177_reg_n_2_[2] ),
        .I5(\t_V_reg_177_reg_n_2_[4] ),
        .O(i_V_fu_210_p2[5]));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_V_reg_265[6]_i_1 
       (.I0(\t_V_reg_177_reg_n_2_[6] ),
        .I1(\i_V_reg_265[9]_i_4_n_2 ),
        .O(i_V_fu_210_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \i_V_reg_265[7]_i_1 
       (.I0(\t_V_reg_177_reg_n_2_[7] ),
        .I1(\i_V_reg_265[9]_i_4_n_2 ),
        .I2(\t_V_reg_177_reg_n_2_[6] ),
        .O(i_V_fu_210_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \i_V_reg_265[8]_i_1 
       (.I0(\t_V_reg_177_reg_n_2_[8] ),
        .I1(\t_V_reg_177_reg_n_2_[6] ),
        .I2(\i_V_reg_265[9]_i_4_n_2 ),
        .I3(\t_V_reg_177_reg_n_2_[7] ),
        .O(i_V_fu_210_p2[8]));
  LUT5 #(
    .INIT(32'h00008000)) 
    \i_V_reg_265[9]_i_1 
       (.I0(AXI_video_strm_V_data_V_1_ack_in),
        .I1(\AXI_video_strm_V_keep_V_1_state_reg_n_2_[1] ),
        .I2(AXI_video_strm_V_user_V_1_ack_in),
        .I3(\AXI_video_strm_V_dest_V_1_state_reg_n_2_[1] ),
        .I4(\i_V_reg_265[9]_i_3_n_2 ),
        .O(i_V_reg_2650));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \i_V_reg_265[9]_i_2 
       (.I0(\t_V_reg_177_reg_n_2_[9] ),
        .I1(\t_V_reg_177_reg_n_2_[7] ),
        .I2(\i_V_reg_265[9]_i_4_n_2 ),
        .I3(\t_V_reg_177_reg_n_2_[6] ),
        .I4(\t_V_reg_177_reg_n_2_[8] ),
        .O(i_V_fu_210_p2[9]));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \i_V_reg_265[9]_i_3 
       (.I0(ap_CS_fsm_state2),
        .I1(AXI_video_strm_V_last_V_1_ack_in),
        .I2(\AXI_video_strm_V_strb_V_1_state_reg_n_2_[1] ),
        .I3(\AXI_video_strm_V_id_V_1_state_reg_n_2_[1] ),
        .O(\i_V_reg_265[9]_i_3_n_2 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \i_V_reg_265[9]_i_4 
       (.I0(\t_V_reg_177_reg_n_2_[5] ),
        .I1(\t_V_reg_177_reg_n_2_[3] ),
        .I2(\t_V_reg_177_reg_n_2_[0] ),
        .I3(\t_V_reg_177_reg_n_2_[1] ),
        .I4(\t_V_reg_177_reg_n_2_[2] ),
        .I5(\t_V_reg_177_reg_n_2_[4] ),
        .O(\i_V_reg_265[9]_i_4_n_2 ));
  FDRE \i_V_reg_265_reg[0] 
       (.C(ap_clk),
        .CE(i_V_reg_2650),
        .D(i_V_fu_210_p2[0]),
        .Q(i_V_reg_265[0]),
        .R(1'b0));
  FDRE \i_V_reg_265_reg[1] 
       (.C(ap_clk),
        .CE(i_V_reg_2650),
        .D(i_V_fu_210_p2[1]),
        .Q(i_V_reg_265[1]),
        .R(1'b0));
  FDRE \i_V_reg_265_reg[2] 
       (.C(ap_clk),
        .CE(i_V_reg_2650),
        .D(i_V_fu_210_p2[2]),
        .Q(i_V_reg_265[2]),
        .R(1'b0));
  FDRE \i_V_reg_265_reg[3] 
       (.C(ap_clk),
        .CE(i_V_reg_2650),
        .D(i_V_fu_210_p2[3]),
        .Q(i_V_reg_265[3]),
        .R(1'b0));
  FDRE \i_V_reg_265_reg[4] 
       (.C(ap_clk),
        .CE(i_V_reg_2650),
        .D(i_V_fu_210_p2[4]),
        .Q(i_V_reg_265[4]),
        .R(1'b0));
  FDRE \i_V_reg_265_reg[5] 
       (.C(ap_clk),
        .CE(i_V_reg_2650),
        .D(i_V_fu_210_p2[5]),
        .Q(i_V_reg_265[5]),
        .R(1'b0));
  FDRE \i_V_reg_265_reg[6] 
       (.C(ap_clk),
        .CE(i_V_reg_2650),
        .D(i_V_fu_210_p2[6]),
        .Q(i_V_reg_265[6]),
        .R(1'b0));
  FDRE \i_V_reg_265_reg[7] 
       (.C(ap_clk),
        .CE(i_V_reg_2650),
        .D(i_V_fu_210_p2[7]),
        .Q(i_V_reg_265[7]),
        .R(1'b0));
  FDRE \i_V_reg_265_reg[8] 
       (.C(ap_clk),
        .CE(i_V_reg_2650),
        .D(i_V_fu_210_p2[8]),
        .Q(i_V_reg_265[8]),
        .R(1'b0));
  FDRE \i_V_reg_265_reg[9] 
       (.C(ap_clk),
        .CE(i_V_reg_2650),
        .D(i_V_fu_210_p2[9]),
        .Q(i_V_reg_265[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \mOutPtr[1]_i_1__10 
       (.I0(\mOutPtr_reg[1] ),
        .I1(shiftReg_ce),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \mOutPtr[1]_i_2__5 
       (.I0(Mat2AXIvideo_U0_ap_start),
        .I1(i_V_reg_2650),
        .I2(\ap_CS_fsm[0]_i_2__2_n_2 ),
        .O(\mOutPtr_reg[1]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[0]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[0]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[0]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[0]));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[10]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[10]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[10]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[10]));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[11]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[11]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[11]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[11]));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[12]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[12]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[12]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[12]));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[13]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[13]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[13]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[13]));
  (* SOFT_HLUTNM = "soft_lutpair115" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[14]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[14]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[14]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[14]));
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[15]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[15]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[15]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[15]));
  (* SOFT_HLUTNM = "soft_lutpair115" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[16]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[16]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[16]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[16]));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[17]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[17]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[17]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[17]));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[18]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[18]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[18]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[18]));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[19]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[19]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[19]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[19]));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[1]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[1]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[1]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[1]));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[20]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[20]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[20]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[20]));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[21]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[21]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[21]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[21]));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[22]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[22]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[22]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[22]));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[23]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[23]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[23]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[23]));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[2]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[2]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[2]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[2]));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[3]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[3]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[3]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[3]));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[4]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[4]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[4]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[4]));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[5]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[5]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[5]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[5]));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[6]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[6]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[6]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[6]));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[7]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[7]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[7]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[7]));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[8]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[8]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[8]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[8]));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \stream_out_TDATA[9]_INST_0 
       (.I0(AXI_video_strm_V_data_V_1_payload_B[9]),
        .I1(AXI_video_strm_V_data_V_1_payload_A[9]),
        .I2(AXI_video_strm_V_data_V_1_sel),
        .O(stream_out_TDATA[9]));
  LUT3 #(
    .INIT(8'hB8)) 
    \stream_out_TLAST[0]_INST_0 
       (.I0(AXI_video_strm_V_last_V_1_payload_B),
        .I1(AXI_video_strm_V_last_V_1_sel),
        .I2(AXI_video_strm_V_last_V_1_payload_A),
        .O(stream_out_TLAST));
  LUT3 #(
    .INIT(8'hB8)) 
    \stream_out_TUSER[0]_INST_0 
       (.I0(AXI_video_strm_V_user_V_1_payload_B),
        .I1(AXI_video_strm_V_user_V_1_sel),
        .I2(AXI_video_strm_V_user_V_1_payload_A),
        .O(stream_out_TUSER));
  (* SOFT_HLUTNM = "soft_lutpair116" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \t_V_1_reg_188[0]_i_1 
       (.I0(t_V_1_reg_188_reg__0[0]),
        .O(j_V_fu_222_p2[0]));
  LUT3 #(
    .INIT(8'h2A)) 
    \t_V_1_reg_188[10]_i_1 
       (.I0(\ap_CS_fsm[2]_i_2__1_n_2 ),
        .I1(axi_last_V_reg_2790),
        .I2(ap_enable_reg_pp0_iter0),
        .O(t_V_1_reg_188));
  LUT2 #(
    .INIT(4'h8)) 
    \t_V_1_reg_188[10]_i_2 
       (.I0(ap_enable_reg_pp0_iter0),
        .I1(axi_last_V_reg_2790),
        .O(t_V_1_reg_1880));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \t_V_1_reg_188[10]_i_3 
       (.I0(t_V_1_reg_188_reg__0[10]),
        .I1(t_V_1_reg_188_reg__0[8]),
        .I2(t_V_1_reg_188_reg__0[6]),
        .I3(\t_V_1_reg_188[10]_i_5_n_2 ),
        .I4(t_V_1_reg_188_reg__0[7]),
        .I5(t_V_1_reg_188_reg__0[9]),
        .O(j_V_fu_222_p2[10]));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \t_V_1_reg_188[10]_i_4 
       (.I0(\exitcond_reg_270[0]_i_3_n_2 ),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(exitcond_fu_216_p2),
        .O(axi_last_V_reg_2790));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \t_V_1_reg_188[10]_i_5 
       (.I0(t_V_1_reg_188_reg__0[5]),
        .I1(t_V_1_reg_188_reg__0[3]),
        .I2(t_V_1_reg_188_reg__0[0]),
        .I3(t_V_1_reg_188_reg__0[1]),
        .I4(t_V_1_reg_188_reg__0[2]),
        .I5(t_V_1_reg_188_reg__0[4]),
        .O(\t_V_1_reg_188[10]_i_5_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair116" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \t_V_1_reg_188[1]_i_1 
       (.I0(t_V_1_reg_188_reg__0[0]),
        .I1(t_V_1_reg_188_reg__0[1]),
        .O(j_V_fu_222_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \t_V_1_reg_188[2]_i_1 
       (.I0(t_V_1_reg_188_reg__0[2]),
        .I1(t_V_1_reg_188_reg__0[1]),
        .I2(t_V_1_reg_188_reg__0[0]),
        .O(j_V_fu_222_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \t_V_1_reg_188[3]_i_1 
       (.I0(t_V_1_reg_188_reg__0[3]),
        .I1(t_V_1_reg_188_reg__0[0]),
        .I2(t_V_1_reg_188_reg__0[1]),
        .I3(t_V_1_reg_188_reg__0[2]),
        .O(j_V_fu_222_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \t_V_1_reg_188[4]_i_1 
       (.I0(t_V_1_reg_188_reg__0[4]),
        .I1(t_V_1_reg_188_reg__0[2]),
        .I2(t_V_1_reg_188_reg__0[1]),
        .I3(t_V_1_reg_188_reg__0[0]),
        .I4(t_V_1_reg_188_reg__0[3]),
        .O(j_V_fu_222_p2[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \t_V_1_reg_188[5]_i_1 
       (.I0(t_V_1_reg_188_reg__0[5]),
        .I1(t_V_1_reg_188_reg__0[3]),
        .I2(t_V_1_reg_188_reg__0[0]),
        .I3(t_V_1_reg_188_reg__0[1]),
        .I4(t_V_1_reg_188_reg__0[2]),
        .I5(t_V_1_reg_188_reg__0[4]),
        .O(j_V_fu_222_p2[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \t_V_1_reg_188[6]_i_1 
       (.I0(t_V_1_reg_188_reg__0[6]),
        .I1(\t_V_1_reg_188[10]_i_5_n_2 ),
        .O(j_V_fu_222_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \t_V_1_reg_188[7]_i_1 
       (.I0(t_V_1_reg_188_reg__0[7]),
        .I1(\t_V_1_reg_188[10]_i_5_n_2 ),
        .I2(t_V_1_reg_188_reg__0[6]),
        .O(j_V_fu_222_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \t_V_1_reg_188[8]_i_1 
       (.I0(t_V_1_reg_188_reg__0[8]),
        .I1(t_V_1_reg_188_reg__0[6]),
        .I2(\t_V_1_reg_188[10]_i_5_n_2 ),
        .I3(t_V_1_reg_188_reg__0[7]),
        .O(j_V_fu_222_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \t_V_1_reg_188[9]_i_1 
       (.I0(t_V_1_reg_188_reg__0[9]),
        .I1(t_V_1_reg_188_reg__0[7]),
        .I2(\t_V_1_reg_188[10]_i_5_n_2 ),
        .I3(t_V_1_reg_188_reg__0[6]),
        .I4(t_V_1_reg_188_reg__0[8]),
        .O(j_V_fu_222_p2[9]));
  FDRE \t_V_1_reg_188_reg[0] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1880),
        .D(j_V_fu_222_p2[0]),
        .Q(t_V_1_reg_188_reg__0[0]),
        .R(t_V_1_reg_188));
  FDRE \t_V_1_reg_188_reg[10] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1880),
        .D(j_V_fu_222_p2[10]),
        .Q(t_V_1_reg_188_reg__0[10]),
        .R(t_V_1_reg_188));
  FDRE \t_V_1_reg_188_reg[1] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1880),
        .D(j_V_fu_222_p2[1]),
        .Q(t_V_1_reg_188_reg__0[1]),
        .R(t_V_1_reg_188));
  FDRE \t_V_1_reg_188_reg[2] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1880),
        .D(j_V_fu_222_p2[2]),
        .Q(t_V_1_reg_188_reg__0[2]),
        .R(t_V_1_reg_188));
  FDRE \t_V_1_reg_188_reg[3] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1880),
        .D(j_V_fu_222_p2[3]),
        .Q(t_V_1_reg_188_reg__0[3]),
        .R(t_V_1_reg_188));
  FDRE \t_V_1_reg_188_reg[4] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1880),
        .D(j_V_fu_222_p2[4]),
        .Q(t_V_1_reg_188_reg__0[4]),
        .R(t_V_1_reg_188));
  FDRE \t_V_1_reg_188_reg[5] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1880),
        .D(j_V_fu_222_p2[5]),
        .Q(t_V_1_reg_188_reg__0[5]),
        .R(t_V_1_reg_188));
  FDRE \t_V_1_reg_188_reg[6] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1880),
        .D(j_V_fu_222_p2[6]),
        .Q(t_V_1_reg_188_reg__0[6]),
        .R(t_V_1_reg_188));
  FDRE \t_V_1_reg_188_reg[7] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1880),
        .D(j_V_fu_222_p2[7]),
        .Q(t_V_1_reg_188_reg__0[7]),
        .R(t_V_1_reg_188));
  FDRE \t_V_1_reg_188_reg[8] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1880),
        .D(j_V_fu_222_p2[8]),
        .Q(t_V_1_reg_188_reg__0[8]),
        .R(t_V_1_reg_188));
  FDRE \t_V_1_reg_188_reg[9] 
       (.C(ap_clk),
        .CE(t_V_1_reg_1880),
        .D(j_V_fu_222_p2[9]),
        .Q(t_V_1_reg_188_reg__0[9]),
        .R(t_V_1_reg_188));
  LUT3 #(
    .INIT(8'h08)) 
    \t_V_reg_177[9]_i_1 
       (.I0(\ap_CS_fsm_reg_n_2_[0] ),
        .I1(Mat2AXIvideo_U0_ap_start),
        .I2(ap_CS_fsm_state6),
        .O(t_V_reg_177));
  FDRE \t_V_reg_177_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_V_reg_265[0]),
        .Q(\t_V_reg_177_reg_n_2_[0] ),
        .R(t_V_reg_177));
  FDRE \t_V_reg_177_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_V_reg_265[1]),
        .Q(\t_V_reg_177_reg_n_2_[1] ),
        .R(t_V_reg_177));
  FDRE \t_V_reg_177_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_V_reg_265[2]),
        .Q(\t_V_reg_177_reg_n_2_[2] ),
        .R(t_V_reg_177));
  FDRE \t_V_reg_177_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_V_reg_265[3]),
        .Q(\t_V_reg_177_reg_n_2_[3] ),
        .R(t_V_reg_177));
  FDRE \t_V_reg_177_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_V_reg_265[4]),
        .Q(\t_V_reg_177_reg_n_2_[4] ),
        .R(t_V_reg_177));
  FDRE \t_V_reg_177_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_V_reg_265[5]),
        .Q(\t_V_reg_177_reg_n_2_[5] ),
        .R(t_V_reg_177));
  FDRE \t_V_reg_177_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_V_reg_265[6]),
        .Q(\t_V_reg_177_reg_n_2_[6] ),
        .R(t_V_reg_177));
  FDRE \t_V_reg_177_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_V_reg_265[7]),
        .Q(\t_V_reg_177_reg_n_2_[7] ),
        .R(t_V_reg_177));
  FDRE \t_V_reg_177_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_V_reg_265[8]),
        .Q(\t_V_reg_177_reg_n_2_[8] ),
        .R(t_V_reg_177));
  FDRE \t_V_reg_177_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_V_reg_265[9]),
        .Q(\t_V_reg_177_reg_n_2_[9] ),
        .R(t_V_reg_177));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT4 #(
    .INIT(16'h00EA)) 
    \tmp_user_V_fu_126[0]_i_1 
       (.I0(tmp_user_V_fu_126),
        .I1(Mat2AXIvideo_U0_ap_start),
        .I2(\ap_CS_fsm_reg_n_2_[0] ),
        .I3(\mOutPtr_reg[1] ),
        .O(\tmp_user_V_fu_126[0]_i_1_n_2 ));
  FDRE \tmp_user_V_fu_126_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_user_V_fu_126[0]_i_1_n_2 ),
        .Q(tmp_user_V_fu_126),
        .R(1'b0));
endmodule

module system_edge_detect_0_1_Sobel
   (start_once_reg,
    D,
    Q,
    grp_Filter2D_fu_96_p_src_data_stream_2_V_read,
    \mOutPtr_reg[1] ,
    \mOutPtr_reg[1]_0 ,
    E,
    \mOutPtr_reg[0] ,
    shiftReg_ce,
    \ap_CS_fsm_reg[0]_0 ,
    \SRL_SIG_reg[0][7] ,
    \SRL_SIG_reg[0][0] ,
    \SRL_SIG_reg[0][1] ,
    \SRL_SIG_reg[0][2] ,
    \SRL_SIG_reg[0][3] ,
    \SRL_SIG_reg[0][4] ,
    \SRL_SIG_reg[0][5] ,
    \SRL_SIG_reg[0][6] ,
    \SRL_SIG_reg[0][7]_0 ,
    ap_clk,
    ap_rst_n_inv,
    ap_rst_n,
    shiftReg_ce_0,
    \mOutPtr_reg[1]_1 ,
    \mOutPtr_reg[1]_2 ,
    \mOutPtr_reg[1]_3 ,
    shiftReg_ce_1,
    img1_data_stream_0_s_empty_n,
    img1_data_stream_2_s_empty_n,
    img1_data_stream_1_s_empty_n,
    img2_data_stream_0_s_full_n,
    img2_data_stream_1_s_full_n,
    img2_data_stream_2_s_full_n,
    start_for_CvtColor_1_U0_full_n,
    Sobel_U0_ap_start,
    \SRL_SIG_reg[0][7]_1 );
  output start_once_reg;
  output [0:0]D;
  output [0:0]Q;
  output grp_Filter2D_fu_96_p_src_data_stream_2_V_read;
  output [0:0]\mOutPtr_reg[1] ;
  output [0:0]\mOutPtr_reg[1]_0 ;
  output [0:0]E;
  output [0:0]\mOutPtr_reg[0] ;
  output shiftReg_ce;
  output \ap_CS_fsm_reg[0]_0 ;
  output \SRL_SIG_reg[0][7] ;
  output \SRL_SIG_reg[0][0] ;
  output \SRL_SIG_reg[0][1] ;
  output \SRL_SIG_reg[0][2] ;
  output \SRL_SIG_reg[0][3] ;
  output \SRL_SIG_reg[0][4] ;
  output \SRL_SIG_reg[0][5] ;
  output \SRL_SIG_reg[0][6] ;
  output \SRL_SIG_reg[0][7]_0 ;
  input ap_clk;
  input ap_rst_n_inv;
  input ap_rst_n;
  input shiftReg_ce_0;
  input [1:0]\mOutPtr_reg[1]_1 ;
  input [1:0]\mOutPtr_reg[1]_2 ;
  input [1:0]\mOutPtr_reg[1]_3 ;
  input shiftReg_ce_1;
  input img1_data_stream_0_s_empty_n;
  input img1_data_stream_2_s_empty_n;
  input img1_data_stream_1_s_empty_n;
  input img2_data_stream_0_s_full_n;
  input img2_data_stream_1_s_full_n;
  input img2_data_stream_2_s_full_n;
  input start_for_CvtColor_1_U0_full_n;
  input Sobel_U0_ap_start;
  input [7:0]\SRL_SIG_reg[0][7]_1 ;

  wire [0:0]D;
  wire [0:0]E;
  wire [0:0]Q;
  wire \SRL_SIG_reg[0][0] ;
  wire \SRL_SIG_reg[0][1] ;
  wire \SRL_SIG_reg[0][2] ;
  wire \SRL_SIG_reg[0][3] ;
  wire \SRL_SIG_reg[0][4] ;
  wire \SRL_SIG_reg[0][5] ;
  wire \SRL_SIG_reg[0][6] ;
  wire \SRL_SIG_reg[0][7] ;
  wire \SRL_SIG_reg[0][7]_0 ;
  wire [7:0]\SRL_SIG_reg[0][7]_1 ;
  wire Sobel_U0_ap_start;
  wire \ap_CS_fsm_reg[0]_0 ;
  wire \ap_CS_fsm_reg_n_2_[0] ;
  wire ap_clk;
  wire ap_reg_grp_Filter2D_fu_96_ap_start;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire grp_Filter2D_fu_96_n_10;
  wire grp_Filter2D_fu_96_n_12;
  wire grp_Filter2D_fu_96_n_13;
  wire grp_Filter2D_fu_96_n_9;
  wire grp_Filter2D_fu_96_p_src_data_stream_2_V_read;
  wire img1_data_stream_0_s_empty_n;
  wire img1_data_stream_1_s_empty_n;
  wire img1_data_stream_2_s_empty_n;
  wire img2_data_stream_0_s_full_n;
  wire img2_data_stream_1_s_full_n;
  wire img2_data_stream_2_s_full_n;
  wire [0:0]\mOutPtr_reg[0] ;
  wire [0:0]\mOutPtr_reg[1] ;
  wire [0:0]\mOutPtr_reg[1]_0 ;
  wire [1:0]\mOutPtr_reg[1]_1 ;
  wire [1:0]\mOutPtr_reg[1]_2 ;
  wire [1:0]\mOutPtr_reg[1]_3 ;
  wire shiftReg_ce;
  wire shiftReg_ce_0;
  wire shiftReg_ce_1;
  wire start_for_CvtColor_1_U0_full_n;
  wire start_once_reg;

  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(grp_Filter2D_fu_96_n_10),
        .Q(\ap_CS_fsm_reg_n_2_[0] ),
        .S(ap_rst_n_inv));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(grp_Filter2D_fu_96_n_9),
        .Q(Q),
        .R(ap_rst_n_inv));
  FDRE #(
    .INIT(1'b0)) 
    ap_reg_grp_Filter2D_fu_96_ap_start_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(grp_Filter2D_fu_96_n_13),
        .Q(ap_reg_grp_Filter2D_fu_96_ap_start),
        .R(ap_rst_n_inv));
  system_edge_detect_0_1_Filter2D grp_Filter2D_fu_96
       (.D(D),
        .E(grp_Filter2D_fu_96_p_src_data_stream_2_V_read),
        .Q({Q,\ap_CS_fsm_reg_n_2_[0] }),
        .\SRL_SIG_reg[0][0] (\SRL_SIG_reg[0][0] ),
        .\SRL_SIG_reg[0][1] (\SRL_SIG_reg[0][1] ),
        .\SRL_SIG_reg[0][2] (\SRL_SIG_reg[0][2] ),
        .\SRL_SIG_reg[0][3] (\SRL_SIG_reg[0][3] ),
        .\SRL_SIG_reg[0][4] (\SRL_SIG_reg[0][4] ),
        .\SRL_SIG_reg[0][5] (\SRL_SIG_reg[0][5] ),
        .\SRL_SIG_reg[0][6] (\SRL_SIG_reg[0][6] ),
        .\SRL_SIG_reg[0][7] (\SRL_SIG_reg[0][7] ),
        .\SRL_SIG_reg[0][7]_0 (\SRL_SIG_reg[0][7]_0 ),
        .\SRL_SIG_reg[0][7]_1 (\SRL_SIG_reg[0][7]_1 ),
        .Sobel_U0_ap_start(Sobel_U0_ap_start),
        .\ap_CS_fsm_reg[0]_0 (\ap_CS_fsm_reg[0]_0 ),
        .\ap_CS_fsm_reg[1]_0 ({grp_Filter2D_fu_96_n_9,grp_Filter2D_fu_96_n_10}),
        .ap_clk(ap_clk),
        .ap_reg_grp_Filter2D_fu_96_ap_start(ap_reg_grp_Filter2D_fu_96_ap_start),
        .ap_reg_grp_Filter2D_fu_96_ap_start_reg(grp_Filter2D_fu_96_n_13),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .img1_data_stream_0_s_empty_n(img1_data_stream_0_s_empty_n),
        .img1_data_stream_1_s_empty_n(img1_data_stream_1_s_empty_n),
        .img1_data_stream_2_s_empty_n(img1_data_stream_2_s_empty_n),
        .img2_data_stream_0_s_full_n(img2_data_stream_0_s_full_n),
        .img2_data_stream_1_s_full_n(img2_data_stream_1_s_full_n),
        .img2_data_stream_2_s_full_n(img2_data_stream_2_s_full_n),
        .\mOutPtr_reg[0] (E),
        .\mOutPtr_reg[0]_0 (\mOutPtr_reg[0] ),
        .\mOutPtr_reg[1] (\mOutPtr_reg[1] ),
        .\mOutPtr_reg[1]_0 (\mOutPtr_reg[1]_0 ),
        .\mOutPtr_reg[1]_1 (\mOutPtr_reg[1]_1 ),
        .\mOutPtr_reg[1]_2 (\mOutPtr_reg[1]_2 ),
        .\mOutPtr_reg[1]_3 (\mOutPtr_reg[1]_3 ),
        .shiftReg_ce(shiftReg_ce),
        .shiftReg_ce_0(shiftReg_ce_0),
        .shiftReg_ce_1(shiftReg_ce_1),
        .start_for_CvtColor_1_U0_full_n(start_for_CvtColor_1_U0_full_n),
        .start_once_reg_reg(grp_Filter2D_fu_96_n_12),
        .start_once_reg_reg_0(start_once_reg));
  FDRE #(
    .INIT(1'b0)) 
    start_once_reg_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(grp_Filter2D_fu_96_n_12),
        .Q(start_once_reg),
        .R(ap_rst_n_inv));
endmodule

(* hls_module = "yes" *) 
module system_edge_detect_0_1_edge_detect
   (stream_in_TDATA,
    stream_in_TKEEP,
    stream_in_TSTRB,
    stream_in_TUSER,
    stream_in_TLAST,
    stream_in_TID,
    stream_in_TDEST,
    stream_out_TDATA,
    stream_out_TKEEP,
    stream_out_TSTRB,
    stream_out_TUSER,
    stream_out_TLAST,
    stream_out_TID,
    stream_out_TDEST,
    ap_clk,
    ap_rst_n,
    stream_in_TVALID,
    stream_in_TREADY,
    stream_out_TVALID,
    stream_out_TREADY);
  input [23:0]stream_in_TDATA;
  input [2:0]stream_in_TKEEP;
  input [2:0]stream_in_TSTRB;
  input [0:0]stream_in_TUSER;
  input [0:0]stream_in_TLAST;
  input [0:0]stream_in_TID;
  input [0:0]stream_in_TDEST;
  output [23:0]stream_out_TDATA;
  output [2:0]stream_out_TKEEP;
  output [2:0]stream_out_TSTRB;
  output [0:0]stream_out_TUSER;
  output [0:0]stream_out_TLAST;
  output [0:0]stream_out_TID;
  output [0:0]stream_out_TDEST;
  input ap_clk;
  input ap_rst_n;
  input stream_in_TVALID;
  output stream_in_TREADY;
  output stream_out_TVALID;
  input stream_out_TREADY;

  wire \<const0> ;
  wire \<const1> ;
  wire [7:0]AXIvideo2Mat_U0_img_data_stream_0_V_din;
  wire [7:0]AXIvideo2Mat_U0_img_data_stream_1_V_din;
  wire [7:0]AXIvideo2Mat_U0_img_data_stream_2_V_din;
  wire AXIvideo2Mat_U0_n_5;
  wire CvtColor_1_U0_ap_start;
  wire CvtColor_1_U0_n_3;
  wire CvtColor_U0_ap_start;
  wire CvtColor_U0_n_10;
  wire CvtColor_U0_n_11;
  wire CvtColor_U0_n_12;
  wire CvtColor_U0_n_13;
  wire CvtColor_U0_n_14;
  wire CvtColor_U0_n_15;
  wire CvtColor_U0_n_16;
  wire CvtColor_U0_n_3;
  wire CvtColor_U0_n_4;
  wire CvtColor_U0_n_6;
  wire CvtColor_U0_n_8;
  wire Mat2AXIvideo_U0_ap_start;
  wire Mat2AXIvideo_U0_n_3;
  wire Mat2AXIvideo_U0_n_4;
  wire Mat2AXIvideo_U0_n_5;
  wire Sobel_U0_ap_start;
  wire Sobel_U0_n_11;
  wire Sobel_U0_n_12;
  wire Sobel_U0_n_13;
  wire Sobel_U0_n_14;
  wire Sobel_U0_n_15;
  wire Sobel_U0_n_16;
  wire Sobel_U0_n_17;
  wire Sobel_U0_n_18;
  wire Sobel_U0_n_19;
  wire Sobel_U0_n_20;
  wire Sobel_U0_n_3;
  wire Sobel_U0_n_6;
  wire Sobel_U0_n_7;
  wire Sobel_U0_n_8;
  wire Sobel_U0_n_9;
  wire ap_CS_fsm_state2;
  wire ap_clk;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire grp_Filter2D_fu_96_p_src_data_stream_2_V_read;
  wire [7:0]img0_data_stream_0_s_dout;
  wire img0_data_stream_0_s_empty_n;
  wire img0_data_stream_0_s_full_n;
  wire [7:0]img0_data_stream_1_s_dout;
  wire img0_data_stream_1_s_empty_n;
  wire img0_data_stream_1_s_full_n;
  wire [7:0]img0_data_stream_2_s_dout;
  wire img0_data_stream_2_s_empty_n;
  wire img0_data_stream_2_s_full_n;
  wire img1_data_stream_0_s_U_n_4;
  wire img1_data_stream_0_s_U_n_5;
  wire [7:0]img1_data_stream_0_s_dout;
  wire img1_data_stream_0_s_empty_n;
  wire img1_data_stream_0_s_full_n;
  wire img1_data_stream_1_s_U_n_4;
  wire img1_data_stream_1_s_U_n_5;
  wire img1_data_stream_1_s_empty_n;
  wire img1_data_stream_1_s_full_n;
  wire img1_data_stream_2_s_U_n_4;
  wire img1_data_stream_2_s_U_n_5;
  wire img1_data_stream_2_s_empty_n;
  wire img1_data_stream_2_s_full_n;
  wire img2_data_stream_0_s_U_n_3;
  wire [7:0]img2_data_stream_0_s_dout;
  wire img2_data_stream_0_s_full_n;
  wire img2_data_stream_1_s_empty_n;
  wire img2_data_stream_1_s_full_n;
  wire img2_data_stream_2_s_empty_n;
  wire img2_data_stream_2_s_full_n;
  wire img3_data_stream_0_s_U_n_4;
  wire img3_data_stream_0_s_U_n_5;
  wire [7:0]img3_data_stream_0_s_dout;
  wire img3_data_stream_0_s_empty_n;
  wire img3_data_stream_0_s_full_n;
  wire img3_data_stream_1_s_U_n_4;
  wire img3_data_stream_1_s_U_n_5;
  wire [7:0]img3_data_stream_1_s_dout;
  wire img3_data_stream_1_s_empty_n;
  wire img3_data_stream_1_s_full_n;
  wire [7:0]img3_data_stream_2_s_dout;
  wire img3_data_stream_2_s_empty_n;
  wire img3_data_stream_2_s_full_n;
  wire internal_empty_n4_out;
  wire shiftReg_ce;
  wire shiftReg_ce_1;
  wire shiftReg_ce_3;
  wire start_for_CvtColoocq_U_n_4;
  wire start_for_CvtColor_1_U0_full_n;
  wire start_for_CvtColor_U0_full_n;
  wire start_for_Mat2AXIvideo_U0_full_n;
  wire start_for_Sobel_U0_full_n;
  wire start_once_reg;
  wire start_once_reg_0;
  wire start_once_reg_2;
  wire start_once_reg_4;
  wire [23:0]stream_in_TDATA;
  wire [0:0]stream_in_TLAST;
  wire stream_in_TREADY;
  wire [0:0]stream_in_TUSER;
  wire stream_in_TVALID;
  wire [23:0]stream_out_TDATA;
  wire [0:0]stream_out_TLAST;
  wire stream_out_TREADY;
  wire [0:0]stream_out_TUSER;
  wire stream_out_TVALID;
  wire tmp_99_fu_294_p3;

  assign stream_out_TDEST[0] = \<const0> ;
  assign stream_out_TID[0] = \<const0> ;
  assign stream_out_TKEEP[2] = \<const1> ;
  assign stream_out_TKEEP[1] = \<const1> ;
  assign stream_out_TKEEP[0] = \<const1> ;
  assign stream_out_TSTRB[2] = \<const0> ;
  assign stream_out_TSTRB[1] = \<const0> ;
  assign stream_out_TSTRB[0] = \<const0> ;
  system_edge_detect_0_1_AXIvideo2Mat AXIvideo2Mat_U0
       (.D(AXIvideo2Mat_U0_img_data_stream_2_V_din),
        .\SRL_SIG_reg[0][7] (AXIvideo2Mat_U0_img_data_stream_1_V_din),
        .\SRL_SIG_reg[0][7]_0 (AXIvideo2Mat_U0_img_data_stream_0_V_din),
        .\SRL_SIG_reg[1][0] (AXIvideo2Mat_U0_n_5),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .img0_data_stream_0_s_full_n(img0_data_stream_0_s_full_n),
        .img0_data_stream_1_s_full_n(img0_data_stream_1_s_full_n),
        .img0_data_stream_2_s_full_n(img0_data_stream_2_s_full_n),
        .start_for_CvtColor_U0_full_n(start_for_CvtColor_U0_full_n),
        .start_once_reg(start_once_reg),
        .stream_in_TDATA(stream_in_TDATA),
        .stream_in_TLAST(stream_in_TLAST),
        .stream_in_TREADY(stream_in_TREADY),
        .stream_in_TUSER(stream_in_TUSER),
        .stream_in_TVALID(stream_in_TVALID));
  system_edge_detect_0_1_CvtColor_1 CvtColor_1_U0
       (.CvtColor_1_U0_ap_start(CvtColor_1_U0_ap_start),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .internal_empty_n_reg(img2_data_stream_0_s_U_n_3),
        .shiftReg_ce(shiftReg_ce),
        .start_for_Mat2AXIvideo_U0_full_n(start_for_Mat2AXIvideo_U0_full_n),
        .start_once_reg(start_once_reg_0),
        .start_once_reg_reg_0(CvtColor_1_U0_n_3));
  system_edge_detect_0_1_CvtColor CvtColor_U0
       (.B(img0_data_stream_2_s_dout),
        .CvtColor_U0_ap_start(CvtColor_U0_ap_start),
        .D(img0_data_stream_1_s_dout),
        .E(CvtColor_U0_n_3),
        .Q({tmp_99_fu_294_p3,CvtColor_U0_n_10,CvtColor_U0_n_11,CvtColor_U0_n_12,CvtColor_U0_n_13,CvtColor_U0_n_14,CvtColor_U0_n_15,CvtColor_U0_n_16}),
        .\SRL_SIG_reg[0][7] (CvtColor_U0_n_8),
        .\SRL_SIG_reg[0][7]_0 (img0_data_stream_0_s_dout),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .\exitcond_reg_420_reg[0] (AXIvideo2Mat_U0_n_5),
        .img0_data_stream_0_s_empty_n(img0_data_stream_0_s_empty_n),
        .img0_data_stream_1_s_empty_n(img0_data_stream_1_s_empty_n),
        .img0_data_stream_2_s_empty_n(img0_data_stream_2_s_empty_n),
        .img1_data_stream_0_s_full_n(img1_data_stream_0_s_full_n),
        .img1_data_stream_1_s_full_n(img1_data_stream_1_s_full_n),
        .img1_data_stream_2_s_full_n(img1_data_stream_2_s_full_n),
        .internal_empty_n4_out(internal_empty_n4_out),
        .internal_full_n_reg(CvtColor_U0_n_4),
        .shiftReg_ce(shiftReg_ce_1),
        .start_for_Sobel_U0_full_n(start_for_Sobel_U0_full_n),
        .start_once_reg(start_once_reg_2),
        .start_once_reg_reg_0(CvtColor_U0_n_6));
  GND GND
       (.G(\<const0> ));
  system_edge_detect_0_1_Mat2AXIvideo Mat2AXIvideo_U0
       (.D({img3_data_stream_2_s_dout,img3_data_stream_1_s_dout,img3_data_stream_0_s_dout}),
        .E(Mat2AXIvideo_U0_n_3),
        .Mat2AXIvideo_U0_ap_start(Mat2AXIvideo_U0_ap_start),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .img3_data_stream_0_s_empty_n(img3_data_stream_0_s_empty_n),
        .img3_data_stream_1_s_empty_n(img3_data_stream_1_s_empty_n),
        .img3_data_stream_2_s_empty_n(img3_data_stream_2_s_empty_n),
        .\mOutPtr_reg[1] (Mat2AXIvideo_U0_n_4),
        .\mOutPtr_reg[1]_0 (Mat2AXIvideo_U0_n_5),
        .shiftReg_ce(shiftReg_ce),
        .stream_out_TDATA(stream_out_TDATA),
        .stream_out_TLAST(stream_out_TLAST),
        .stream_out_TREADY(stream_out_TREADY),
        .stream_out_TUSER(stream_out_TUSER),
        .stream_out_TVALID(stream_out_TVALID));
  system_edge_detect_0_1_Sobel Sobel_U0
       (.D(Sobel_U0_n_3),
        .E(Sobel_U0_n_8),
        .Q(ap_CS_fsm_state2),
        .\SRL_SIG_reg[0][0] (Sobel_U0_n_13),
        .\SRL_SIG_reg[0][1] (Sobel_U0_n_14),
        .\SRL_SIG_reg[0][2] (Sobel_U0_n_15),
        .\SRL_SIG_reg[0][3] (Sobel_U0_n_16),
        .\SRL_SIG_reg[0][4] (Sobel_U0_n_17),
        .\SRL_SIG_reg[0][5] (Sobel_U0_n_18),
        .\SRL_SIG_reg[0][6] (Sobel_U0_n_19),
        .\SRL_SIG_reg[0][7] (Sobel_U0_n_12),
        .\SRL_SIG_reg[0][7]_0 (Sobel_U0_n_20),
        .\SRL_SIG_reg[0][7]_1 (img1_data_stream_0_s_dout),
        .Sobel_U0_ap_start(Sobel_U0_ap_start),
        .\ap_CS_fsm_reg[0]_0 (Sobel_U0_n_11),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .grp_Filter2D_fu_96_p_src_data_stream_2_V_read(grp_Filter2D_fu_96_p_src_data_stream_2_V_read),
        .img1_data_stream_0_s_empty_n(img1_data_stream_0_s_empty_n),
        .img1_data_stream_1_s_empty_n(img1_data_stream_1_s_empty_n),
        .img1_data_stream_2_s_empty_n(img1_data_stream_2_s_empty_n),
        .img2_data_stream_0_s_full_n(img2_data_stream_0_s_full_n),
        .img2_data_stream_1_s_full_n(img2_data_stream_1_s_full_n),
        .img2_data_stream_2_s_full_n(img2_data_stream_2_s_full_n),
        .\mOutPtr_reg[0] (Sobel_U0_n_9),
        .\mOutPtr_reg[1] (Sobel_U0_n_6),
        .\mOutPtr_reg[1]_0 (Sobel_U0_n_7),
        .\mOutPtr_reg[1]_1 ({img1_data_stream_2_s_U_n_4,img1_data_stream_2_s_U_n_5}),
        .\mOutPtr_reg[1]_2 ({img1_data_stream_1_s_U_n_4,img1_data_stream_1_s_U_n_5}),
        .\mOutPtr_reg[1]_3 ({img1_data_stream_0_s_U_n_4,img1_data_stream_0_s_U_n_5}),
        .shiftReg_ce(shiftReg_ce_3),
        .shiftReg_ce_0(shiftReg_ce_1),
        .shiftReg_ce_1(shiftReg_ce),
        .start_for_CvtColor_1_U0_full_n(start_for_CvtColor_1_U0_full_n),
        .start_once_reg(start_once_reg_4));
  VCC VCC
       (.P(\<const1> ));
  system_edge_detect_0_1_fifo_w8_d1_A img0_data_stream_0_s_U
       (.D(AXIvideo2Mat_U0_img_data_stream_0_V_din),
        .E(CvtColor_U0_n_3),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .\exitcond_reg_420_reg[0] (AXIvideo2Mat_U0_n_5),
        .img0_data_stream_0_s_empty_n(img0_data_stream_0_s_empty_n),
        .img0_data_stream_0_s_full_n(img0_data_stream_0_s_full_n),
        .internal_empty_n4_out(internal_empty_n4_out),
        .\tmp_101_reg_364_reg[7] (img0_data_stream_0_s_dout),
        .\tmp_20_reg_355_reg[0] (CvtColor_U0_n_4));
  system_edge_detect_0_1_fifo_w8_d1_A_0 img0_data_stream_1_s_U
       (.D(img0_data_stream_1_s_dout),
        .E(CvtColor_U0_n_3),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .\axi_data_V_1_reg_236_reg[15] (AXIvideo2Mat_U0_img_data_stream_1_V_din),
        .\exitcond_reg_420_reg[0] (AXIvideo2Mat_U0_n_5),
        .img0_data_stream_1_s_empty_n(img0_data_stream_1_s_empty_n),
        .img0_data_stream_1_s_full_n(img0_data_stream_1_s_full_n),
        .internal_empty_n4_out(internal_empty_n4_out),
        .\tmp_20_reg_355_reg[0] (CvtColor_U0_n_4));
  system_edge_detect_0_1_fifo_w8_d1_A_1 img0_data_stream_2_s_U
       (.B(img0_data_stream_2_s_dout),
        .D(AXIvideo2Mat_U0_img_data_stream_2_V_din),
        .E(CvtColor_U0_n_3),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .\exitcond_reg_420_reg[0] (AXIvideo2Mat_U0_n_5),
        .img0_data_stream_2_s_empty_n(img0_data_stream_2_s_empty_n),
        .img0_data_stream_2_s_full_n(img0_data_stream_2_s_full_n),
        .internal_empty_n4_out(internal_empty_n4_out),
        .\tmp_20_reg_355_reg[0] (CvtColor_U0_n_4));
  system_edge_detect_0_1_fifo_w8_d1_A_2 img1_data_stream_0_s_U
       (.D(Sobel_U0_n_7),
        .E(Sobel_U0_n_8),
        .Q(ap_CS_fsm_state2),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .grp_Filter2D_fu_96_p_src_data_stream_2_V_read(grp_Filter2D_fu_96_p_src_data_stream_2_V_read),
        .img1_data_stream_0_s_empty_n(img1_data_stream_0_s_empty_n),
        .img1_data_stream_0_s_full_n(img1_data_stream_0_s_full_n),
        .\p_Val2_15_reg_394_reg[7] ({tmp_99_fu_294_p3,CvtColor_U0_n_10,CvtColor_U0_n_11,CvtColor_U0_n_12,CvtColor_U0_n_13,CvtColor_U0_n_14,CvtColor_U0_n_15,CvtColor_U0_n_16}),
        .\r_V_1_reg_389_reg[29] (CvtColor_U0_n_8),
        .\reg_588_reg[7] ({img1_data_stream_0_s_U_n_4,img1_data_stream_0_s_U_n_5}),
        .\reg_588_reg[7]_0 (img1_data_stream_0_s_dout),
        .shiftReg_ce(shiftReg_ce_1));
  system_edge_detect_0_1_fifo_w8_d1_A_3 img1_data_stream_1_s_U
       (.D(Sobel_U0_n_6),
        .E(Sobel_U0_n_8),
        .Q(ap_CS_fsm_state2),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .grp_Filter2D_fu_96_p_src_data_stream_2_V_read(grp_Filter2D_fu_96_p_src_data_stream_2_V_read),
        .img1_data_stream_1_s_empty_n(img1_data_stream_1_s_empty_n),
        .img1_data_stream_1_s_full_n(img1_data_stream_1_s_full_n),
        .\mOutPtr_reg[1]_0 ({img1_data_stream_1_s_U_n_4,img1_data_stream_1_s_U_n_5}),
        .shiftReg_ce(shiftReg_ce_1));
  system_edge_detect_0_1_fifo_w8_d1_A_4 img1_data_stream_2_s_U
       (.D(Sobel_U0_n_3),
        .E(Sobel_U0_n_8),
        .Q(ap_CS_fsm_state2),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .grp_Filter2D_fu_96_p_src_data_stream_2_V_read(grp_Filter2D_fu_96_p_src_data_stream_2_V_read),
        .img1_data_stream_2_s_empty_n(img1_data_stream_2_s_empty_n),
        .img1_data_stream_2_s_full_n(img1_data_stream_2_s_full_n),
        .\mOutPtr_reg[1]_0 ({img1_data_stream_2_s_U_n_4,img1_data_stream_2_s_U_n_5}),
        .shiftReg_ce(shiftReg_ce_1));
  system_edge_detect_0_1_fifo_w8_d1_A_5 img2_data_stream_0_s_U
       (.E(Sobel_U0_n_9),
        .\ap_CS_fsm_reg[3] (img2_data_stream_0_s_U_n_3),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .img2_data_stream_0_s_dout(img2_data_stream_0_s_dout),
        .img2_data_stream_0_s_full_n(img2_data_stream_0_s_full_n),
        .img2_data_stream_1_s_empty_n(img2_data_stream_1_s_empty_n),
        .img2_data_stream_2_s_empty_n(img2_data_stream_2_s_empty_n),
        .img3_data_stream_0_s_full_n(img3_data_stream_0_s_full_n),
        .img3_data_stream_1_s_full_n(img3_data_stream_1_s_full_n),
        .img3_data_stream_2_s_full_n(img3_data_stream_2_s_full_n),
        .\p_Val2_1_reg_2922_reg[0] (Sobel_U0_n_13),
        .\p_Val2_1_reg_2922_reg[1] (Sobel_U0_n_14),
        .\p_Val2_1_reg_2922_reg[2] (Sobel_U0_n_15),
        .\p_Val2_1_reg_2922_reg[3] (Sobel_U0_n_16),
        .\p_Val2_1_reg_2922_reg[4] (Sobel_U0_n_17),
        .\p_Val2_1_reg_2922_reg[5] (Sobel_U0_n_18),
        .\p_Val2_1_reg_2922_reg[6] (Sobel_U0_n_19),
        .\p_Val2_1_reg_2922_reg[7] (Sobel_U0_n_20),
        .shiftReg_ce(shiftReg_ce),
        .shiftReg_ce_0(shiftReg_ce_3),
        .\tmp_57_reg_2927_reg[2] (Sobel_U0_n_12));
  system_edge_detect_0_1_fifo_w8_d1_A_6 img2_data_stream_1_s_U
       (.E(Sobel_U0_n_9),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .img2_data_stream_1_s_empty_n(img2_data_stream_1_s_empty_n),
        .img2_data_stream_1_s_full_n(img2_data_stream_1_s_full_n),
        .shiftReg_ce(shiftReg_ce),
        .shiftReg_ce_0(shiftReg_ce_3));
  system_edge_detect_0_1_fifo_w8_d1_A_7 img2_data_stream_2_s_U
       (.E(Sobel_U0_n_9),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .img2_data_stream_2_s_empty_n(img2_data_stream_2_s_empty_n),
        .img2_data_stream_2_s_full_n(img2_data_stream_2_s_full_n),
        .shiftReg_ce(shiftReg_ce),
        .shiftReg_ce_0(shiftReg_ce_3));
  system_edge_detect_0_1_fifo_w8_d1_A_8 img3_data_stream_0_s_U
       (.E(Mat2AXIvideo_U0_n_3),
        .Q({img3_data_stream_0_s_U_n_4,img3_data_stream_0_s_U_n_5}),
        .\ap_CS_fsm_reg[2] (Mat2AXIvideo_U0_n_4),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .img3_data_stream_0_s_empty_n(img3_data_stream_0_s_empty_n),
        .img3_data_stream_0_s_full_n(img3_data_stream_0_s_full_n),
        .shiftReg_ce(shiftReg_ce));
  system_edge_detect_0_1_fifo_w8_d1_A_9 img3_data_stream_1_s_U
       (.E(Mat2AXIvideo_U0_n_3),
        .Q({img3_data_stream_1_s_U_n_4,img3_data_stream_1_s_U_n_5}),
        .\ap_CS_fsm_reg[2] (Mat2AXIvideo_U0_n_4),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .img3_data_stream_1_s_empty_n(img3_data_stream_1_s_empty_n),
        .img3_data_stream_1_s_full_n(img3_data_stream_1_s_full_n),
        .shiftReg_ce(shiftReg_ce));
  system_edge_detect_0_1_fifo_w8_d1_A_10 img3_data_stream_2_s_U
       (.D({img3_data_stream_2_s_dout,img3_data_stream_1_s_dout,img3_data_stream_0_s_dout}),
        .E(Mat2AXIvideo_U0_n_3),
        .Q({img3_data_stream_1_s_U_n_4,img3_data_stream_1_s_U_n_5}),
        .\ap_CS_fsm_reg[2] (Mat2AXIvideo_U0_n_4),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .img2_data_stream_0_s_dout(img2_data_stream_0_s_dout),
        .img3_data_stream_2_s_empty_n(img3_data_stream_2_s_empty_n),
        .img3_data_stream_2_s_full_n(img3_data_stream_2_s_full_n),
        .\mOutPtr_reg[1]_0 ({img3_data_stream_0_s_U_n_4,img3_data_stream_0_s_U_n_5}),
        .shiftReg_ce(shiftReg_ce));
  system_edge_detect_0_1_start_for_CvtColoocq start_for_CvtColoocq_U
       (.CvtColor_U0_ap_start(CvtColor_U0_ap_start),
        .\ap_CS_fsm_reg[1] (CvtColor_U0_n_6),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .\mOutPtr_reg[1]_0 (start_for_CvtColoocq_U_n_4),
        .start_for_CvtColor_U0_full_n(start_for_CvtColor_U0_full_n),
        .start_for_Sobel_U0_full_n(start_for_Sobel_U0_full_n),
        .start_once_reg(start_once_reg_2),
        .start_once_reg_0(start_once_reg));
  system_edge_detect_0_1_start_for_CvtColopcA start_for_CvtColopcA_U
       (.CvtColor_1_U0_ap_start(CvtColor_1_U0_ap_start),
        .Sobel_U0_ap_start(Sobel_U0_ap_start),
        .\ap_CS_fsm_reg[1] (CvtColor_1_U0_n_3),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .start_for_CvtColor_1_U0_full_n(start_for_CvtColor_1_U0_full_n),
        .start_once_reg(start_once_reg_4));
  system_edge_detect_0_1_start_for_Mat2AXIqcK start_for_Mat2AXIqcK_U
       (.CvtColor_1_U0_ap_start(CvtColor_1_U0_ap_start),
        .Mat2AXIvideo_U0_ap_start(Mat2AXIvideo_U0_ap_start),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .internal_empty_n_reg_0(Mat2AXIvideo_U0_n_5),
        .start_for_Mat2AXIvideo_U0_full_n(start_for_Mat2AXIvideo_U0_full_n),
        .start_once_reg(start_once_reg_0));
  system_edge_detect_0_1_start_for_Sobel_U0 start_for_Sobel_U0_U
       (.CvtColor_U0_ap_start(CvtColor_U0_ap_start),
        .Sobel_U0_ap_start(Sobel_U0_ap_start),
        .\ap_CS_fsm_reg[1] (Sobel_U0_n_11),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .ap_rst_n_inv(ap_rst_n_inv),
        .internal_empty_n_reg_0(start_for_CvtColoocq_U_n_4),
        .start_for_Sobel_U0_full_n(start_for_Sobel_U0_full_n),
        .start_once_reg(start_once_reg_2));
endmodule

module system_edge_detect_0_1_edge_detect_mac_mdEe
   (P,
    ap_block_pp0_stage0_subdone3_in,
    \r_V_1_reg_389_reg[29] ,
    ap_clk,
    Q,
    p_Val2_12_reg_384_reg,
    tmp_20_reg_355,
    ap_enable_reg_pp0_iter1_reg,
    img0_data_stream_2_s_empty_n,
    img0_data_stream_0_s_empty_n,
    img0_data_stream_1_s_empty_n,
    ap_reg_pp0_iter4_tmp_20_reg_355,
    ap_enable_reg_pp0_iter5_reg,
    img1_data_stream_0_s_full_n,
    img1_data_stream_1_s_full_n,
    img1_data_stream_2_s_full_n,
    ap_reg_pp0_iter3_tmp_20_reg_355,
    ap_enable_reg_pp0_iter4,
    tmp_98_fu_287_p3);
  output [8:0]P;
  output ap_block_pp0_stage0_subdone3_in;
  output \r_V_1_reg_389_reg[29] ;
  input ap_clk;
  input [7:0]Q;
  input [28:0]p_Val2_12_reg_384_reg;
  input tmp_20_reg_355;
  input ap_enable_reg_pp0_iter1_reg;
  input img0_data_stream_2_s_empty_n;
  input img0_data_stream_0_s_empty_n;
  input img0_data_stream_1_s_empty_n;
  input ap_reg_pp0_iter4_tmp_20_reg_355;
  input ap_enable_reg_pp0_iter5_reg;
  input img1_data_stream_0_s_full_n;
  input img1_data_stream_1_s_full_n;
  input img1_data_stream_2_s_full_n;
  input ap_reg_pp0_iter3_tmp_20_reg_355;
  input ap_enable_reg_pp0_iter4;
  input tmp_98_fu_287_p3;

  wire [8:0]P;
  wire [7:0]Q;
  wire ap_block_pp0_stage0_subdone3_in;
  wire ap_clk;
  wire ap_enable_reg_pp0_iter1_reg;
  wire ap_enable_reg_pp0_iter4;
  wire ap_enable_reg_pp0_iter5_reg;
  wire ap_reg_pp0_iter3_tmp_20_reg_355;
  wire ap_reg_pp0_iter4_tmp_20_reg_355;
  wire img0_data_stream_0_s_empty_n;
  wire img0_data_stream_1_s_empty_n;
  wire img0_data_stream_2_s_empty_n;
  wire img1_data_stream_0_s_full_n;
  wire img1_data_stream_1_s_full_n;
  wire img1_data_stream_2_s_full_n;
  wire [28:0]p_Val2_12_reg_384_reg;
  wire \r_V_1_reg_389_reg[29] ;
  wire tmp_20_reg_355;
  wire tmp_98_fu_287_p3;

  system_edge_detect_0_1_edge_detect_mac_mdEe_DSP48_2 edge_detect_mac_mdEe_DSP48_2_U
       (.P(P),
        .Q(Q),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp0_iter1_reg(ap_enable_reg_pp0_iter1_reg),
        .ap_enable_reg_pp0_iter4(ap_enable_reg_pp0_iter4),
        .ap_enable_reg_pp0_iter5_reg(ap_enable_reg_pp0_iter5_reg),
        .ap_reg_pp0_iter3_tmp_20_reg_355(ap_reg_pp0_iter3_tmp_20_reg_355),
        .ap_reg_pp0_iter4_tmp_20_reg_355(ap_reg_pp0_iter4_tmp_20_reg_355),
        .img0_data_stream_0_s_empty_n(img0_data_stream_0_s_empty_n),
        .img0_data_stream_1_s_empty_n(img0_data_stream_1_s_empty_n),
        .img0_data_stream_2_s_empty_n(img0_data_stream_2_s_empty_n),
        .img1_data_stream_0_s_full_n(img1_data_stream_0_s_full_n),
        .img1_data_stream_1_s_full_n(img1_data_stream_1_s_full_n),
        .img1_data_stream_2_s_full_n(img1_data_stream_2_s_full_n),
        .p_0(ap_block_pp0_stage0_subdone3_in),
        .p_Val2_12_reg_384_reg(p_Val2_12_reg_384_reg),
        .\r_V_1_reg_389_reg[29] (\r_V_1_reg_389_reg[29] ),
        .tmp_20_reg_355(tmp_20_reg_355),
        .tmp_98_fu_287_p3(tmp_98_fu_287_p3));
endmodule

module system_edge_detect_0_1_edge_detect_mac_mdEe_DSP48_2
   (P,
    p_0,
    \r_V_1_reg_389_reg[29] ,
    ap_clk,
    Q,
    p_Val2_12_reg_384_reg,
    tmp_20_reg_355,
    ap_enable_reg_pp0_iter1_reg,
    img0_data_stream_2_s_empty_n,
    img0_data_stream_0_s_empty_n,
    img0_data_stream_1_s_empty_n,
    ap_reg_pp0_iter4_tmp_20_reg_355,
    ap_enable_reg_pp0_iter5_reg,
    img1_data_stream_0_s_full_n,
    img1_data_stream_1_s_full_n,
    img1_data_stream_2_s_full_n,
    ap_reg_pp0_iter3_tmp_20_reg_355,
    ap_enable_reg_pp0_iter4,
    tmp_98_fu_287_p3);
  output [8:0]P;
  output p_0;
  output \r_V_1_reg_389_reg[29] ;
  input ap_clk;
  input [7:0]Q;
  input [28:0]p_Val2_12_reg_384_reg;
  input tmp_20_reg_355;
  input ap_enable_reg_pp0_iter1_reg;
  input img0_data_stream_2_s_empty_n;
  input img0_data_stream_0_s_empty_n;
  input img0_data_stream_1_s_empty_n;
  input ap_reg_pp0_iter4_tmp_20_reg_355;
  input ap_enable_reg_pp0_iter5_reg;
  input img1_data_stream_0_s_full_n;
  input img1_data_stream_1_s_full_n;
  input img1_data_stream_2_s_full_n;
  input ap_reg_pp0_iter3_tmp_20_reg_355;
  input ap_enable_reg_pp0_iter4;
  input tmp_98_fu_287_p3;

  wire [8:0]P;
  wire [7:0]Q;
  wire ap_clk;
  wire ap_enable_reg_pp0_iter1_reg;
  wire ap_enable_reg_pp0_iter4;
  wire ap_enable_reg_pp0_iter5_reg;
  wire ap_reg_pp0_iter3_tmp_20_reg_355;
  wire ap_reg_pp0_iter4_tmp_20_reg_355;
  wire img0_data_stream_0_s_empty_n;
  wire img0_data_stream_1_s_empty_n;
  wire img0_data_stream_2_s_empty_n;
  wire img1_data_stream_0_s_full_n;
  wire img1_data_stream_1_s_full_n;
  wire img1_data_stream_2_s_full_n;
  wire p_0;
  wire [28:0]p_Val2_12_reg_384_reg;
  wire p_Val2_12_reg_384_reg_i_13_n_2;
  wire p_n_100;
  wire p_n_101;
  wire p_n_102;
  wire p_n_103;
  wire p_n_104;
  wire p_n_105;
  wire p_n_106;
  wire p_n_107;
  wire p_n_87;
  wire p_n_88;
  wire p_n_89;
  wire p_n_90;
  wire p_n_91;
  wire p_n_92;
  wire p_n_93;
  wire p_n_94;
  wire p_n_95;
  wire p_n_96;
  wire p_n_97;
  wire p_n_98;
  wire p_n_99;
  wire \r_V_1_reg_389_reg[29] ;
  wire tmp_20_reg_355;
  wire tmp_98_fu_287_p3;
  wire NLW_p_CARRYCASCOUT_UNCONNECTED;
  wire NLW_p_MULTSIGNOUT_UNCONNECTED;
  wire NLW_p_OVERFLOW_UNCONNECTED;
  wire NLW_p_PATTERNBDETECT_UNCONNECTED;
  wire NLW_p_PATTERNDETECT_UNCONNECTED;
  wire NLW_p_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_p_ACOUT_UNCONNECTED;
  wire [17:0]NLW_p_BCOUT_UNCONNECTED;
  wire [3:0]NLW_p_CARRYOUT_UNCONNECTED;
  wire [47:30]NLW_p_P_UNCONNECTED;
  wire [47:0]NLW_p_PCOUT_UNCONNECTED;

  (* METHODOLOGY_DRC_VIOS = "{SYNTH-11 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(2),
    .BREG(2),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(0),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    p
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b0,1'b1,1'b0,1'b1,1'b1,1'b0,1'b0,1'b1,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1,1'b1,1'b0,1'b1,1'b0,1'b0,1'b0}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_p_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_p_BCOUT_UNCONNECTED[17:0]),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,p_Val2_12_reg_384_reg}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_p_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_p_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(p_0),
        .CEB2(p_0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_p_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b1,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_p_OVERFLOW_UNCONNECTED),
        .P({NLW_p_P_UNCONNECTED[47:30],P,p_n_87,p_n_88,p_n_89,p_n_90,p_n_91,p_n_92,p_n_93,p_n_94,p_n_95,p_n_96,p_n_97,p_n_98,p_n_99,p_n_100,p_n_101,p_n_102,p_n_103,p_n_104,p_n_105,p_n_106,p_n_107}),
        .PATTERNBDETECT(NLW_p_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_p_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_p_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_p_UNDERFLOW_UNCONNECTED));
  LUT5 #(
    .INIT(32'h08888888)) 
    p_Val2_12_reg_384_reg_i_13
       (.I0(ap_reg_pp0_iter4_tmp_20_reg_355),
        .I1(ap_enable_reg_pp0_iter5_reg),
        .I2(img1_data_stream_0_s_full_n),
        .I3(img1_data_stream_1_s_full_n),
        .I4(img1_data_stream_2_s_full_n),
        .O(p_Val2_12_reg_384_reg_i_13_n_2));
  LUT6 #(
    .INIT(64'h00000000F7777777)) 
    p_Val2_12_reg_384_reg_i_2
       (.I0(tmp_20_reg_355),
        .I1(ap_enable_reg_pp0_iter1_reg),
        .I2(img0_data_stream_2_s_empty_n),
        .I3(img0_data_stream_0_s_empty_n),
        .I4(img0_data_stream_1_s_empty_n),
        .I5(p_Val2_12_reg_384_reg_i_13_n_2),
        .O(p_0));
  LUT5 #(
    .INIT(32'hBFFF8000)) 
    \r_V_1_reg_389[29]_i_1 
       (.I0(P[8]),
        .I1(p_0),
        .I2(ap_reg_pp0_iter3_tmp_20_reg_355),
        .I3(ap_enable_reg_pp0_iter4),
        .I4(tmp_98_fu_287_p3),
        .O(\r_V_1_reg_389_reg[29] ));
endmodule

module system_edge_detect_0_1_edge_detect_mul_mbkb
   (out,
    Q);
  output [28:0]out;
  input [7:0]Q;

  wire [7:0]Q;
  wire [28:0]out;

  system_edge_detect_0_1_edge_detect_mul_mbkb_DSP48_0 edge_detect_mul_mbkb_DSP48_0_U
       (.Q(Q),
        .out(out));
endmodule

module system_edge_detect_0_1_edge_detect_mul_mbkb_DSP48_0
   (out,
    Q);
  output [28:0]out;
  input [7:0]Q;

  (* RTL_KEEP = "true" *) wire [7:0]Q;
  wire in00_n_78;
  (* RTL_KEEP = "true" *) wire [28:0]out;
  (* RTL_KEEP = "true" *) wire [21:0]p_0_in;
  wire NLW_in00_CARRYCASCOUT_UNCONNECTED;
  wire NLW_in00_MULTSIGNOUT_UNCONNECTED;
  wire NLW_in00_OVERFLOW_UNCONNECTED;
  wire NLW_in00_PATTERNBDETECT_UNCONNECTED;
  wire NLW_in00_PATTERNDETECT_UNCONNECTED;
  wire NLW_in00_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_in00_ACOUT_UNCONNECTED;
  wire [17:0]NLW_in00_BCOUT_UNCONNECTED;
  wire [3:0]NLW_in00_CARRYOUT_UNCONNECTED;
  wire [47:30]NLW_in00_P_UNCONNECTED;
  wire [47:0]NLW_in00_PCOUT_UNCONNECTED;

  LUT1 #(
    .INIT(2'h2)) 
    i_2_0
       (.I0(1'b0),
        .O(p_0_in[21]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2_1
       (.I0(1'b1),
        .O(p_0_in[20]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2_10
       (.I0(1'b0),
        .O(p_0_in[11]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2_11
       (.I0(1'b0),
        .O(p_0_in[10]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2_12
       (.I0(1'b1),
        .O(p_0_in[9]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2_13
       (.I0(1'b0),
        .O(p_0_in[8]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2_14
       (.I0(1'b1),
        .O(p_0_in[7]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2_15
       (.I0(1'b1),
        .O(p_0_in[6]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2_16
       (.I0(1'b0),
        .O(p_0_in[5]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2_17
       (.I0(1'b1),
        .O(p_0_in[4]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2_18
       (.I0(1'b0),
        .O(p_0_in[3]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2_19
       (.I0(1'b0),
        .O(p_0_in[2]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2_2
       (.I0(1'b0),
        .O(p_0_in[19]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2_20
       (.I0(1'b0),
        .O(p_0_in[1]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2_21
       (.I0(1'b0),
        .O(p_0_in[0]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2_3
       (.I0(1'b0),
        .O(p_0_in[18]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2_4
       (.I0(1'b1),
        .O(p_0_in[17]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2_5
       (.I0(1'b1),
        .O(p_0_in[16]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2_6
       (.I0(1'b0),
        .O(p_0_in[15]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2_7
       (.I0(1'b0),
        .O(p_0_in[14]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2_8
       (.I0(1'b1),
        .O(p_0_in[13]));
  LUT1 #(
    .INIT(2'h2)) 
    i_2_9
       (.I0(1'b0),
        .O(p_0_in[12]));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    in00
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,p_0_in}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_in00_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_in00_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_in00_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_in00_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_in00_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_in00_OVERFLOW_UNCONNECTED),
        .P({NLW_in00_P_UNCONNECTED[47:30],in00_n_78,out}),
        .PATTERNBDETECT(NLW_in00_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_in00_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_in00_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_in00_UNDERFLOW_UNCONNECTED));
endmodule

module system_edge_detect_0_1_fifo_w8_d1_A
   (img0_data_stream_0_s_full_n,
    img0_data_stream_0_s_empty_n,
    \tmp_101_reg_364_reg[7] ,
    ap_clk,
    internal_empty_n4_out,
    ap_rst_n,
    \tmp_20_reg_355_reg[0] ,
    ap_rst_n_inv,
    E,
    \exitcond_reg_420_reg[0] ,
    D);
  output img0_data_stream_0_s_full_n;
  output img0_data_stream_0_s_empty_n;
  output [7:0]\tmp_101_reg_364_reg[7] ;
  input ap_clk;
  input internal_empty_n4_out;
  input ap_rst_n;
  input \tmp_20_reg_355_reg[0] ;
  input ap_rst_n_inv;
  input [0:0]E;
  input [0:0]\exitcond_reg_420_reg[0] ;
  input [7:0]D;

  wire [7:0]D;
  wire [0:0]E;
  wire ap_clk;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire [0:0]\exitcond_reg_420_reg[0] ;
  wire img0_data_stream_0_s_empty_n;
  wire img0_data_stream_0_s_full_n;
  wire internal_empty_n4_out;
  wire internal_empty_n_i_1__1_n_2;
  wire internal_full_n_i_1_n_2;
  wire \mOutPtr[0]_i_1_n_2 ;
  wire \mOutPtr[1]_i_2_n_2 ;
  wire \mOutPtr_reg_n_2_[0] ;
  wire \mOutPtr_reg_n_2_[1] ;
  wire [7:0]\tmp_101_reg_364_reg[7] ;
  wire \tmp_20_reg_355_reg[0] ;

  system_edge_detect_0_1_fifo_w8_d1_A_shiftReg_15 U_fifo_w8_d1_A_ram
       (.D(D),
        .Q({\mOutPtr_reg_n_2_[1] ,\mOutPtr_reg_n_2_[0] }),
        .ap_clk(ap_clk),
        .\exitcond_reg_420_reg[0] (\exitcond_reg_420_reg[0] ),
        .\tmp_101_reg_364_reg[7] (\tmp_101_reg_364_reg[7] ));
  LUT6 #(
    .INIT(64'hFD00FD00FD000000)) 
    internal_empty_n_i_1__1
       (.I0(\tmp_20_reg_355_reg[0] ),
        .I1(\mOutPtr_reg_n_2_[1] ),
        .I2(\mOutPtr_reg_n_2_[0] ),
        .I3(ap_rst_n),
        .I4(internal_empty_n4_out),
        .I5(img0_data_stream_0_s_empty_n),
        .O(internal_empty_n_i_1__1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__1_n_2),
        .Q(img0_data_stream_0_s_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFD00FFFF)) 
    internal_full_n_i_1
       (.I0(internal_empty_n4_out),
        .I1(\mOutPtr_reg_n_2_[0] ),
        .I2(\mOutPtr_reg_n_2_[1] ),
        .I3(img0_data_stream_0_s_full_n),
        .I4(ap_rst_n),
        .I5(\tmp_20_reg_355_reg[0] ),
        .O(internal_full_n_i_1_n_2));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1_n_2),
        .Q(img0_data_stream_0_s_full_n),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair160" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \mOutPtr[0]_i_1 
       (.I0(\mOutPtr_reg_n_2_[0] ),
        .O(\mOutPtr[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair160" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \mOutPtr[1]_i_2 
       (.I0(\tmp_20_reg_355_reg[0] ),
        .I1(\mOutPtr_reg_n_2_[0] ),
        .I2(\mOutPtr_reg_n_2_[1] ),
        .O(\mOutPtr[1]_i_2_n_2 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(E),
        .D(\mOutPtr[0]_i_1_n_2 ),
        .Q(\mOutPtr_reg_n_2_[0] ),
        .S(ap_rst_n_inv));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(E),
        .D(\mOutPtr[1]_i_2_n_2 ),
        .Q(\mOutPtr_reg_n_2_[1] ),
        .S(ap_rst_n_inv));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d1_A" *) 
module system_edge_detect_0_1_fifo_w8_d1_A_0
   (img0_data_stream_1_s_full_n,
    img0_data_stream_1_s_empty_n,
    D,
    ap_clk,
    internal_empty_n4_out,
    ap_rst_n,
    \tmp_20_reg_355_reg[0] ,
    ap_rst_n_inv,
    E,
    \exitcond_reg_420_reg[0] ,
    \axi_data_V_1_reg_236_reg[15] );
  output img0_data_stream_1_s_full_n;
  output img0_data_stream_1_s_empty_n;
  output [7:0]D;
  input ap_clk;
  input internal_empty_n4_out;
  input ap_rst_n;
  input \tmp_20_reg_355_reg[0] ;
  input ap_rst_n_inv;
  input [0:0]E;
  input [0:0]\exitcond_reg_420_reg[0] ;
  input [7:0]\axi_data_V_1_reg_236_reg[15] ;

  wire [7:0]D;
  wire [0:0]E;
  wire ap_clk;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire [7:0]\axi_data_V_1_reg_236_reg[15] ;
  wire [0:0]\exitcond_reg_420_reg[0] ;
  wire img0_data_stream_1_s_empty_n;
  wire img0_data_stream_1_s_full_n;
  wire internal_empty_n4_out;
  wire internal_empty_n_i_1__0_n_2;
  wire internal_full_n_i_1__0_n_2;
  wire \mOutPtr[0]_i_1__0_n_2 ;
  wire \mOutPtr[1]_i_1__1_n_2 ;
  wire \mOutPtr_reg_n_2_[0] ;
  wire \mOutPtr_reg_n_2_[1] ;
  wire \tmp_20_reg_355_reg[0] ;

  system_edge_detect_0_1_fifo_w8_d1_A_shiftReg_14 U_fifo_w8_d1_A_ram
       (.D(D),
        .Q({\mOutPtr_reg_n_2_[1] ,\mOutPtr_reg_n_2_[0] }),
        .ap_clk(ap_clk),
        .\axi_data_V_1_reg_236_reg[15] (\axi_data_V_1_reg_236_reg[15] ),
        .\exitcond_reg_420_reg[0] (\exitcond_reg_420_reg[0] ));
  LUT6 #(
    .INIT(64'hFD00FD00FD000000)) 
    internal_empty_n_i_1__0
       (.I0(\tmp_20_reg_355_reg[0] ),
        .I1(\mOutPtr_reg_n_2_[1] ),
        .I2(\mOutPtr_reg_n_2_[0] ),
        .I3(ap_rst_n),
        .I4(internal_empty_n4_out),
        .I5(img0_data_stream_1_s_empty_n),
        .O(internal_empty_n_i_1__0_n_2));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__0_n_2),
        .Q(img0_data_stream_1_s_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFD00FFFF)) 
    internal_full_n_i_1__0
       (.I0(internal_empty_n4_out),
        .I1(\mOutPtr_reg_n_2_[0] ),
        .I2(\mOutPtr_reg_n_2_[1] ),
        .I3(img0_data_stream_1_s_full_n),
        .I4(ap_rst_n),
        .I5(\tmp_20_reg_355_reg[0] ),
        .O(internal_full_n_i_1__0_n_2));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__0_n_2),
        .Q(img0_data_stream_1_s_full_n),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair161" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \mOutPtr[0]_i_1__0 
       (.I0(\mOutPtr_reg_n_2_[0] ),
        .O(\mOutPtr[0]_i_1__0_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair161" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \mOutPtr[1]_i_1__1 
       (.I0(\tmp_20_reg_355_reg[0] ),
        .I1(\mOutPtr_reg_n_2_[0] ),
        .I2(\mOutPtr_reg_n_2_[1] ),
        .O(\mOutPtr[1]_i_1__1_n_2 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(E),
        .D(\mOutPtr[0]_i_1__0_n_2 ),
        .Q(\mOutPtr_reg_n_2_[0] ),
        .S(ap_rst_n_inv));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(E),
        .D(\mOutPtr[1]_i_1__1_n_2 ),
        .Q(\mOutPtr_reg_n_2_[1] ),
        .S(ap_rst_n_inv));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d1_A" *) 
module system_edge_detect_0_1_fifo_w8_d1_A_1
   (img0_data_stream_2_s_full_n,
    img0_data_stream_2_s_empty_n,
    B,
    ap_clk,
    internal_empty_n4_out,
    ap_rst_n,
    \tmp_20_reg_355_reg[0] ,
    ap_rst_n_inv,
    E,
    \exitcond_reg_420_reg[0] ,
    D);
  output img0_data_stream_2_s_full_n;
  output img0_data_stream_2_s_empty_n;
  output [7:0]B;
  input ap_clk;
  input internal_empty_n4_out;
  input ap_rst_n;
  input \tmp_20_reg_355_reg[0] ;
  input ap_rst_n_inv;
  input [0:0]E;
  input \exitcond_reg_420_reg[0] ;
  input [7:0]D;

  wire [7:0]B;
  wire [7:0]D;
  wire [0:0]E;
  wire ap_clk;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire \exitcond_reg_420_reg[0] ;
  wire img0_data_stream_2_s_empty_n;
  wire img0_data_stream_2_s_full_n;
  wire internal_empty_n4_out;
  wire internal_empty_n_i_1_n_2;
  wire internal_full_n_i_1__1_n_2;
  wire \mOutPtr[0]_i_1__1_n_2 ;
  wire \mOutPtr[1]_i_1__0_n_2 ;
  wire \mOutPtr_reg_n_2_[0] ;
  wire \mOutPtr_reg_n_2_[1] ;
  wire \tmp_20_reg_355_reg[0] ;

  system_edge_detect_0_1_fifo_w8_d1_A_shiftReg_13 U_fifo_w8_d1_A_ram
       (.B(B),
        .D(D),
        .Q({\mOutPtr_reg_n_2_[1] ,\mOutPtr_reg_n_2_[0] }),
        .ap_clk(ap_clk),
        .\exitcond_reg_420_reg[0] (\exitcond_reg_420_reg[0] ));
  LUT6 #(
    .INIT(64'hFD00FD00FD000000)) 
    internal_empty_n_i_1
       (.I0(\tmp_20_reg_355_reg[0] ),
        .I1(\mOutPtr_reg_n_2_[1] ),
        .I2(\mOutPtr_reg_n_2_[0] ),
        .I3(ap_rst_n),
        .I4(internal_empty_n4_out),
        .I5(img0_data_stream_2_s_empty_n),
        .O(internal_empty_n_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1_n_2),
        .Q(img0_data_stream_2_s_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFD00FFFF)) 
    internal_full_n_i_1__1
       (.I0(internal_empty_n4_out),
        .I1(\mOutPtr_reg_n_2_[0] ),
        .I2(\mOutPtr_reg_n_2_[1] ),
        .I3(img0_data_stream_2_s_full_n),
        .I4(ap_rst_n),
        .I5(\tmp_20_reg_355_reg[0] ),
        .O(internal_full_n_i_1__1_n_2));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__1_n_2),
        .Q(img0_data_stream_2_s_full_n),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair162" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \mOutPtr[0]_i_1__1 
       (.I0(\mOutPtr_reg_n_2_[0] ),
        .O(\mOutPtr[0]_i_1__1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair162" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \mOutPtr[1]_i_1__0 
       (.I0(\tmp_20_reg_355_reg[0] ),
        .I1(\mOutPtr_reg_n_2_[0] ),
        .I2(\mOutPtr_reg_n_2_[1] ),
        .O(\mOutPtr[1]_i_1__0_n_2 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(E),
        .D(\mOutPtr[0]_i_1__1_n_2 ),
        .Q(\mOutPtr_reg_n_2_[0] ),
        .S(ap_rst_n_inv));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(E),
        .D(\mOutPtr[1]_i_1__0_n_2 ),
        .Q(\mOutPtr_reg_n_2_[1] ),
        .S(ap_rst_n_inv));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d1_A" *) 
module system_edge_detect_0_1_fifo_w8_d1_A_10
   (img3_data_stream_2_s_full_n,
    img3_data_stream_2_s_empty_n,
    D,
    shiftReg_ce,
    img2_data_stream_0_s_dout,
    ap_clk,
    ap_rst_n,
    \ap_CS_fsm_reg[2] ,
    Q,
    \mOutPtr_reg[1]_0 ,
    ap_rst_n_inv,
    E);
  output img3_data_stream_2_s_full_n;
  output img3_data_stream_2_s_empty_n;
  output [23:0]D;
  input shiftReg_ce;
  input [7:0]img2_data_stream_0_s_dout;
  input ap_clk;
  input ap_rst_n;
  input \ap_CS_fsm_reg[2] ;
  input [1:0]Q;
  input [1:0]\mOutPtr_reg[1]_0 ;
  input ap_rst_n_inv;
  input [0:0]E;

  wire [23:0]D;
  wire [0:0]E;
  wire [1:0]Q;
  wire \ap_CS_fsm_reg[2] ;
  wire ap_clk;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire [7:0]img2_data_stream_0_s_dout;
  wire img3_data_stream_2_s_empty_n;
  wire img3_data_stream_2_s_full_n;
  wire internal_empty_n_i_1__13_n_2;
  wire internal_full_n_i_1__14_n_2;
  wire \mOutPtr[0]_i_1__10_n_2 ;
  wire \mOutPtr[1]_i_1__8_n_2 ;
  wire [1:0]\mOutPtr_reg[1]_0 ;
  wire \mOutPtr_reg_n_2_[0] ;
  wire \mOutPtr_reg_n_2_[1] ;
  wire shiftReg_ce;

  system_edge_detect_0_1_fifo_w8_d1_A_shiftReg U_fifo_w8_d1_A_ram
       (.D(D),
        .Q({\mOutPtr_reg_n_2_[1] ,\mOutPtr_reg_n_2_[0] }),
        .ap_clk(ap_clk),
        .img2_data_stream_0_s_dout(img2_data_stream_0_s_dout),
        .\mOutPtr_reg[1] (Q),
        .\mOutPtr_reg[1]_0 (\mOutPtr_reg[1]_0 ),
        .shiftReg_ce(shiftReg_ce));
  LUT6 #(
    .INIT(64'hF0F0E0F000F00000)) 
    internal_empty_n_i_1__13
       (.I0(\mOutPtr_reg_n_2_[1] ),
        .I1(\mOutPtr_reg_n_2_[0] ),
        .I2(ap_rst_n),
        .I3(\ap_CS_fsm_reg[2] ),
        .I4(shiftReg_ce),
        .I5(img3_data_stream_2_s_empty_n),
        .O(internal_empty_n_i_1__13_n_2));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__13_n_2),
        .Q(img3_data_stream_2_s_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hF0FFE0FFFFFFF0FF)) 
    internal_full_n_i_1__14
       (.I0(\mOutPtr_reg_n_2_[0] ),
        .I1(\mOutPtr_reg_n_2_[1] ),
        .I2(img3_data_stream_2_s_full_n),
        .I3(ap_rst_n),
        .I4(\ap_CS_fsm_reg[2] ),
        .I5(shiftReg_ce),
        .O(internal_full_n_i_1__14_n_2));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__14_n_2),
        .Q(img3_data_stream_2_s_full_n),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair171" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \mOutPtr[0]_i_1__10 
       (.I0(\mOutPtr_reg_n_2_[0] ),
        .O(\mOutPtr[0]_i_1__10_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair171" *) 
  LUT4 #(
    .INIT(16'h4BB4)) 
    \mOutPtr[1]_i_1__8 
       (.I0(shiftReg_ce),
        .I1(\ap_CS_fsm_reg[2] ),
        .I2(\mOutPtr_reg_n_2_[0] ),
        .I3(\mOutPtr_reg_n_2_[1] ),
        .O(\mOutPtr[1]_i_1__8_n_2 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(E),
        .D(\mOutPtr[0]_i_1__10_n_2 ),
        .Q(\mOutPtr_reg_n_2_[0] ),
        .S(ap_rst_n_inv));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(E),
        .D(\mOutPtr[1]_i_1__8_n_2 ),
        .Q(\mOutPtr_reg_n_2_[1] ),
        .S(ap_rst_n_inv));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d1_A" *) 
module system_edge_detect_0_1_fifo_w8_d1_A_2
   (img1_data_stream_0_s_full_n,
    img1_data_stream_0_s_empty_n,
    \reg_588_reg[7] ,
    \reg_588_reg[7]_0 ,
    ap_clk,
    ap_rst_n,
    Q,
    grp_Filter2D_fu_96_p_src_data_stream_2_V_read,
    shiftReg_ce,
    ap_rst_n_inv,
    E,
    D,
    \r_V_1_reg_389_reg[29] ,
    \p_Val2_15_reg_394_reg[7] );
  output img1_data_stream_0_s_full_n;
  output img1_data_stream_0_s_empty_n;
  output [1:0]\reg_588_reg[7] ;
  output [7:0]\reg_588_reg[7]_0 ;
  input ap_clk;
  input ap_rst_n;
  input [0:0]Q;
  input grp_Filter2D_fu_96_p_src_data_stream_2_V_read;
  input shiftReg_ce;
  input ap_rst_n_inv;
  input [0:0]E;
  input [0:0]D;
  input \r_V_1_reg_389_reg[29] ;
  input [7:0]\p_Val2_15_reg_394_reg[7] ;

  wire [0:0]D;
  wire [0:0]E;
  wire [0:0]Q;
  wire ap_clk;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire grp_Filter2D_fu_96_p_src_data_stream_2_V_read;
  wire img1_data_stream_0_s_empty_n;
  wire img1_data_stream_0_s_full_n;
  wire internal_empty_n_i_1__4_n_2;
  wire internal_full_n_i_1__4_n_2;
  wire internal_full_n_i_2__3_n_2;
  wire \mOutPtr[0]_i_1__2_n_2 ;
  wire [7:0]\p_Val2_15_reg_394_reg[7] ;
  wire \r_V_1_reg_389_reg[29] ;
  wire [1:0]\reg_588_reg[7] ;
  wire [7:0]\reg_588_reg[7]_0 ;
  wire shiftReg_ce;

  system_edge_detect_0_1_fifo_w8_d1_A_shiftReg_12 U_fifo_w8_d1_A_ram
       (.Q(\reg_588_reg[7] ),
        .ap_clk(ap_clk),
        .\p_Val2_15_reg_394_reg[7] (\p_Val2_15_reg_394_reg[7] ),
        .\r_V_1_reg_389_reg[29] (\r_V_1_reg_389_reg[29] ),
        .\reg_588_reg[7] (\reg_588_reg[7]_0 ),
        .shiftReg_ce(shiftReg_ce));
  LUT6 #(
    .INIT(64'hCCCC8CCC0CCC0000)) 
    internal_empty_n_i_1__4
       (.I0(internal_full_n_i_2__3_n_2),
        .I1(ap_rst_n),
        .I2(Q),
        .I3(grp_Filter2D_fu_96_p_src_data_stream_2_V_read),
        .I4(shiftReg_ce),
        .I5(img1_data_stream_0_s_empty_n),
        .O(internal_empty_n_i_1__4_n_2));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__4_n_2),
        .Q(img1_data_stream_0_s_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hCF8F8F8FFFCFCFCF)) 
    internal_full_n_i_1__4
       (.I0(internal_full_n_i_2__3_n_2),
        .I1(img1_data_stream_0_s_full_n),
        .I2(ap_rst_n),
        .I3(grp_Filter2D_fu_96_p_src_data_stream_2_V_read),
        .I4(Q),
        .I5(shiftReg_ce),
        .O(internal_full_n_i_1__4_n_2));
  (* SOFT_HLUTNM = "soft_lutpair163" *) 
  LUT2 #(
    .INIT(4'hE)) 
    internal_full_n_i_2__3
       (.I0(\reg_588_reg[7] [1]),
        .I1(\reg_588_reg[7] [0]),
        .O(internal_full_n_i_2__3_n_2));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__4_n_2),
        .Q(img1_data_stream_0_s_full_n),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair163" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \mOutPtr[0]_i_1__2 
       (.I0(\reg_588_reg[7] [0]),
        .O(\mOutPtr[0]_i_1__2_n_2 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(E),
        .D(\mOutPtr[0]_i_1__2_n_2 ),
        .Q(\reg_588_reg[7] [0]),
        .S(ap_rst_n_inv));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(E),
        .D(D),
        .Q(\reg_588_reg[7] [1]),
        .S(ap_rst_n_inv));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d1_A" *) 
module system_edge_detect_0_1_fifo_w8_d1_A_3
   (img1_data_stream_1_s_full_n,
    img1_data_stream_1_s_empty_n,
    \mOutPtr_reg[1]_0 ,
    ap_clk,
    ap_rst_n,
    grp_Filter2D_fu_96_p_src_data_stream_2_V_read,
    Q,
    shiftReg_ce,
    ap_rst_n_inv,
    E,
    D);
  output img1_data_stream_1_s_full_n;
  output img1_data_stream_1_s_empty_n;
  output [1:0]\mOutPtr_reg[1]_0 ;
  input ap_clk;
  input ap_rst_n;
  input grp_Filter2D_fu_96_p_src_data_stream_2_V_read;
  input [0:0]Q;
  input shiftReg_ce;
  input ap_rst_n_inv;
  input [0:0]E;
  input [0:0]D;

  wire [0:0]D;
  wire [0:0]E;
  wire [0:0]Q;
  wire ap_clk;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire grp_Filter2D_fu_96_p_src_data_stream_2_V_read;
  wire img1_data_stream_1_s_empty_n;
  wire img1_data_stream_1_s_full_n;
  wire internal_empty_n_i_1__5_n_2;
  wire internal_full_n_i_1__5_n_2;
  wire internal_full_n_i_2__4_n_2;
  wire \mOutPtr[0]_i_1__3_n_2 ;
  wire [1:0]\mOutPtr_reg[1]_0 ;
  wire shiftReg_ce;

  LUT6 #(
    .INIT(64'hCCCC8CCC0CCC0000)) 
    internal_empty_n_i_1__5
       (.I0(internal_full_n_i_2__4_n_2),
        .I1(ap_rst_n),
        .I2(Q),
        .I3(grp_Filter2D_fu_96_p_src_data_stream_2_V_read),
        .I4(shiftReg_ce),
        .I5(img1_data_stream_1_s_empty_n),
        .O(internal_empty_n_i_1__5_n_2));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__5_n_2),
        .Q(img1_data_stream_1_s_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hCF8F8F8FFFCFCFCF)) 
    internal_full_n_i_1__5
       (.I0(internal_full_n_i_2__4_n_2),
        .I1(img1_data_stream_1_s_full_n),
        .I2(ap_rst_n),
        .I3(grp_Filter2D_fu_96_p_src_data_stream_2_V_read),
        .I4(Q),
        .I5(shiftReg_ce),
        .O(internal_full_n_i_1__5_n_2));
  (* SOFT_HLUTNM = "soft_lutpair164" *) 
  LUT2 #(
    .INIT(4'hE)) 
    internal_full_n_i_2__4
       (.I0(\mOutPtr_reg[1]_0 [1]),
        .I1(\mOutPtr_reg[1]_0 [0]),
        .O(internal_full_n_i_2__4_n_2));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__5_n_2),
        .Q(img1_data_stream_1_s_full_n),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair164" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \mOutPtr[0]_i_1__3 
       (.I0(\mOutPtr_reg[1]_0 [0]),
        .O(\mOutPtr[0]_i_1__3_n_2 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(E),
        .D(\mOutPtr[0]_i_1__3_n_2 ),
        .Q(\mOutPtr_reg[1]_0 [0]),
        .S(ap_rst_n_inv));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(E),
        .D(D),
        .Q(\mOutPtr_reg[1]_0 [1]),
        .S(ap_rst_n_inv));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d1_A" *) 
module system_edge_detect_0_1_fifo_w8_d1_A_4
   (img1_data_stream_2_s_full_n,
    img1_data_stream_2_s_empty_n,
    \mOutPtr_reg[1]_0 ,
    ap_clk,
    ap_rst_n,
    grp_Filter2D_fu_96_p_src_data_stream_2_V_read,
    Q,
    shiftReg_ce,
    ap_rst_n_inv,
    E,
    D);
  output img1_data_stream_2_s_full_n;
  output img1_data_stream_2_s_empty_n;
  output [1:0]\mOutPtr_reg[1]_0 ;
  input ap_clk;
  input ap_rst_n;
  input grp_Filter2D_fu_96_p_src_data_stream_2_V_read;
  input [0:0]Q;
  input shiftReg_ce;
  input ap_rst_n_inv;
  input [0:0]E;
  input [0:0]D;

  wire [0:0]D;
  wire [0:0]E;
  wire [0:0]Q;
  wire ap_clk;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire grp_Filter2D_fu_96_p_src_data_stream_2_V_read;
  wire img1_data_stream_2_s_empty_n;
  wire img1_data_stream_2_s_full_n;
  wire internal_empty_n_i_1__6_n_2;
  wire internal_full_n_i_1__6_n_2;
  wire internal_full_n_i_2__5_n_2;
  wire \mOutPtr[0]_i_1__4_n_2 ;
  wire [1:0]\mOutPtr_reg[1]_0 ;
  wire shiftReg_ce;

  LUT6 #(
    .INIT(64'hCCCC8CCC0CCC0000)) 
    internal_empty_n_i_1__6
       (.I0(internal_full_n_i_2__5_n_2),
        .I1(ap_rst_n),
        .I2(Q),
        .I3(grp_Filter2D_fu_96_p_src_data_stream_2_V_read),
        .I4(shiftReg_ce),
        .I5(img1_data_stream_2_s_empty_n),
        .O(internal_empty_n_i_1__6_n_2));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__6_n_2),
        .Q(img1_data_stream_2_s_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hCF8F8F8FFFCFCFCF)) 
    internal_full_n_i_1__6
       (.I0(internal_full_n_i_2__5_n_2),
        .I1(img1_data_stream_2_s_full_n),
        .I2(ap_rst_n),
        .I3(grp_Filter2D_fu_96_p_src_data_stream_2_V_read),
        .I4(Q),
        .I5(shiftReg_ce),
        .O(internal_full_n_i_1__6_n_2));
  (* SOFT_HLUTNM = "soft_lutpair165" *) 
  LUT2 #(
    .INIT(4'hE)) 
    internal_full_n_i_2__5
       (.I0(\mOutPtr_reg[1]_0 [1]),
        .I1(\mOutPtr_reg[1]_0 [0]),
        .O(internal_full_n_i_2__5_n_2));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__6_n_2),
        .Q(img1_data_stream_2_s_full_n),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair165" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \mOutPtr[0]_i_1__4 
       (.I0(\mOutPtr_reg[1]_0 [0]),
        .O(\mOutPtr[0]_i_1__4_n_2 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(E),
        .D(\mOutPtr[0]_i_1__4_n_2 ),
        .Q(\mOutPtr_reg[1]_0 [0]),
        .S(ap_rst_n_inv));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(E),
        .D(D),
        .Q(\mOutPtr_reg[1]_0 [1]),
        .S(ap_rst_n_inv));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d1_A" *) 
module system_edge_detect_0_1_fifo_w8_d1_A_5
   (img2_data_stream_0_s_full_n,
    \ap_CS_fsm_reg[3] ,
    img2_data_stream_0_s_dout,
    ap_clk,
    ap_rst_n,
    shiftReg_ce,
    shiftReg_ce_0,
    img3_data_stream_2_s_full_n,
    img3_data_stream_1_s_full_n,
    img2_data_stream_2_s_empty_n,
    img3_data_stream_0_s_full_n,
    img2_data_stream_1_s_empty_n,
    ap_rst_n_inv,
    E,
    \tmp_57_reg_2927_reg[2] ,
    \p_Val2_1_reg_2922_reg[7] ,
    \p_Val2_1_reg_2922_reg[6] ,
    \p_Val2_1_reg_2922_reg[5] ,
    \p_Val2_1_reg_2922_reg[4] ,
    \p_Val2_1_reg_2922_reg[3] ,
    \p_Val2_1_reg_2922_reg[2] ,
    \p_Val2_1_reg_2922_reg[1] ,
    \p_Val2_1_reg_2922_reg[0] );
  output img2_data_stream_0_s_full_n;
  output \ap_CS_fsm_reg[3] ;
  output [7:0]img2_data_stream_0_s_dout;
  input ap_clk;
  input ap_rst_n;
  input shiftReg_ce;
  input shiftReg_ce_0;
  input img3_data_stream_2_s_full_n;
  input img3_data_stream_1_s_full_n;
  input img2_data_stream_2_s_empty_n;
  input img3_data_stream_0_s_full_n;
  input img2_data_stream_1_s_empty_n;
  input ap_rst_n_inv;
  input [0:0]E;
  input \tmp_57_reg_2927_reg[2] ;
  input \p_Val2_1_reg_2922_reg[7] ;
  input \p_Val2_1_reg_2922_reg[6] ;
  input \p_Val2_1_reg_2922_reg[5] ;
  input \p_Val2_1_reg_2922_reg[4] ;
  input \p_Val2_1_reg_2922_reg[3] ;
  input \p_Val2_1_reg_2922_reg[2] ;
  input \p_Val2_1_reg_2922_reg[1] ;
  input \p_Val2_1_reg_2922_reg[0] ;

  wire [0:0]E;
  wire \ap_CS_fsm_reg[3] ;
  wire ap_clk;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire [7:0]img2_data_stream_0_s_dout;
  wire img2_data_stream_0_s_empty_n;
  wire img2_data_stream_0_s_full_n;
  wire img2_data_stream_1_s_empty_n;
  wire img2_data_stream_2_s_empty_n;
  wire img3_data_stream_0_s_full_n;
  wire img3_data_stream_1_s_full_n;
  wire img3_data_stream_2_s_full_n;
  wire internal_empty_n_i_1__9_n_2;
  wire internal_full_n_i_1__7_n_2;
  wire \mOutPtr[0]_i_1__5_n_2 ;
  wire \mOutPtr[1]_i_2__2_n_2 ;
  wire \mOutPtr_reg_n_2_[0] ;
  wire \mOutPtr_reg_n_2_[1] ;
  wire \p_Val2_1_reg_2922_reg[0] ;
  wire \p_Val2_1_reg_2922_reg[1] ;
  wire \p_Val2_1_reg_2922_reg[2] ;
  wire \p_Val2_1_reg_2922_reg[3] ;
  wire \p_Val2_1_reg_2922_reg[4] ;
  wire \p_Val2_1_reg_2922_reg[5] ;
  wire \p_Val2_1_reg_2922_reg[6] ;
  wire \p_Val2_1_reg_2922_reg[7] ;
  wire shiftReg_ce;
  wire shiftReg_ce_0;
  wire \tmp_57_reg_2927_reg[2] ;

  system_edge_detect_0_1_fifo_w8_d1_A_shiftReg_11 U_fifo_w8_d1_A_ram
       (.Q({\mOutPtr_reg_n_2_[1] ,\mOutPtr_reg_n_2_[0] }),
        .ap_clk(ap_clk),
        .img2_data_stream_0_s_dout(img2_data_stream_0_s_dout),
        .\p_Val2_1_reg_2922_reg[0] (\p_Val2_1_reg_2922_reg[0] ),
        .\p_Val2_1_reg_2922_reg[1] (\p_Val2_1_reg_2922_reg[1] ),
        .\p_Val2_1_reg_2922_reg[2] (\p_Val2_1_reg_2922_reg[2] ),
        .\p_Val2_1_reg_2922_reg[3] (\p_Val2_1_reg_2922_reg[3] ),
        .\p_Val2_1_reg_2922_reg[4] (\p_Val2_1_reg_2922_reg[4] ),
        .\p_Val2_1_reg_2922_reg[5] (\p_Val2_1_reg_2922_reg[5] ),
        .\p_Val2_1_reg_2922_reg[6] (\p_Val2_1_reg_2922_reg[6] ),
        .\p_Val2_1_reg_2922_reg[7] (\p_Val2_1_reg_2922_reg[7] ),
        .shiftReg_ce_0(shiftReg_ce_0),
        .\tmp_57_reg_2927_reg[2] (\tmp_57_reg_2927_reg[2] ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \ap_CS_fsm[3]_i_3__0 
       (.I0(img2_data_stream_0_s_empty_n),
        .I1(img3_data_stream_2_s_full_n),
        .I2(img3_data_stream_1_s_full_n),
        .I3(img2_data_stream_2_s_empty_n),
        .I4(img3_data_stream_0_s_full_n),
        .I5(img2_data_stream_1_s_empty_n),
        .O(\ap_CS_fsm_reg[3] ));
  LUT6 #(
    .INIT(64'hF0F0E0F000F00000)) 
    internal_empty_n_i_1__9
       (.I0(\mOutPtr_reg_n_2_[1] ),
        .I1(\mOutPtr_reg_n_2_[0] ),
        .I2(ap_rst_n),
        .I3(shiftReg_ce),
        .I4(shiftReg_ce_0),
        .I5(img2_data_stream_0_s_empty_n),
        .O(internal_empty_n_i_1__9_n_2));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__9_n_2),
        .Q(img2_data_stream_0_s_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hF0FFE0FFFFFFF0FF)) 
    internal_full_n_i_1__7
       (.I0(\mOutPtr_reg_n_2_[0] ),
        .I1(\mOutPtr_reg_n_2_[1] ),
        .I2(img2_data_stream_0_s_full_n),
        .I3(ap_rst_n),
        .I4(shiftReg_ce),
        .I5(shiftReg_ce_0),
        .O(internal_full_n_i_1__7_n_2));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__7_n_2),
        .Q(img2_data_stream_0_s_full_n),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair166" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \mOutPtr[0]_i_1__5 
       (.I0(\mOutPtr_reg_n_2_[0] ),
        .O(\mOutPtr[0]_i_1__5_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair166" *) 
  LUT4 #(
    .INIT(16'h4BB4)) 
    \mOutPtr[1]_i_2__2 
       (.I0(shiftReg_ce_0),
        .I1(shiftReg_ce),
        .I2(\mOutPtr_reg_n_2_[0] ),
        .I3(\mOutPtr_reg_n_2_[1] ),
        .O(\mOutPtr[1]_i_2__2_n_2 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(E),
        .D(\mOutPtr[0]_i_1__5_n_2 ),
        .Q(\mOutPtr_reg_n_2_[0] ),
        .S(ap_rst_n_inv));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(E),
        .D(\mOutPtr[1]_i_2__2_n_2 ),
        .Q(\mOutPtr_reg_n_2_[1] ),
        .S(ap_rst_n_inv));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d1_A" *) 
module system_edge_detect_0_1_fifo_w8_d1_A_6
   (img2_data_stream_1_s_full_n,
    img2_data_stream_1_s_empty_n,
    ap_clk,
    ap_rst_n,
    shiftReg_ce,
    shiftReg_ce_0,
    ap_rst_n_inv,
    E);
  output img2_data_stream_1_s_full_n;
  output img2_data_stream_1_s_empty_n;
  input ap_clk;
  input ap_rst_n;
  input shiftReg_ce;
  input shiftReg_ce_0;
  input ap_rst_n_inv;
  input [0:0]E;

  wire [0:0]E;
  wire ap_clk;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire img2_data_stream_1_s_empty_n;
  wire img2_data_stream_1_s_full_n;
  wire internal_empty_n_i_1__8_n_2;
  wire internal_full_n_i_1__8_n_2;
  wire \mOutPtr[0]_i_1__6_n_2 ;
  wire \mOutPtr[1]_i_1__7_n_2 ;
  wire \mOutPtr_reg_n_2_[0] ;
  wire \mOutPtr_reg_n_2_[1] ;
  wire shiftReg_ce;
  wire shiftReg_ce_0;

  LUT6 #(
    .INIT(64'hF0F0E0F000F00000)) 
    internal_empty_n_i_1__8
       (.I0(\mOutPtr_reg_n_2_[1] ),
        .I1(\mOutPtr_reg_n_2_[0] ),
        .I2(ap_rst_n),
        .I3(shiftReg_ce),
        .I4(shiftReg_ce_0),
        .I5(img2_data_stream_1_s_empty_n),
        .O(internal_empty_n_i_1__8_n_2));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__8_n_2),
        .Q(img2_data_stream_1_s_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hF0FFE0FFFFFFF0FF)) 
    internal_full_n_i_1__8
       (.I0(\mOutPtr_reg_n_2_[0] ),
        .I1(\mOutPtr_reg_n_2_[1] ),
        .I2(img2_data_stream_1_s_full_n),
        .I3(ap_rst_n),
        .I4(shiftReg_ce),
        .I5(shiftReg_ce_0),
        .O(internal_full_n_i_1__8_n_2));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__8_n_2),
        .Q(img2_data_stream_1_s_full_n),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair167" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \mOutPtr[0]_i_1__6 
       (.I0(\mOutPtr_reg_n_2_[0] ),
        .O(\mOutPtr[0]_i_1__6_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair167" *) 
  LUT4 #(
    .INIT(16'h4BB4)) 
    \mOutPtr[1]_i_1__7 
       (.I0(shiftReg_ce_0),
        .I1(shiftReg_ce),
        .I2(\mOutPtr_reg_n_2_[0] ),
        .I3(\mOutPtr_reg_n_2_[1] ),
        .O(\mOutPtr[1]_i_1__7_n_2 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(E),
        .D(\mOutPtr[0]_i_1__6_n_2 ),
        .Q(\mOutPtr_reg_n_2_[0] ),
        .S(ap_rst_n_inv));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(E),
        .D(\mOutPtr[1]_i_1__7_n_2 ),
        .Q(\mOutPtr_reg_n_2_[1] ),
        .S(ap_rst_n_inv));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d1_A" *) 
module system_edge_detect_0_1_fifo_w8_d1_A_7
   (img2_data_stream_2_s_full_n,
    img2_data_stream_2_s_empty_n,
    ap_clk,
    ap_rst_n,
    shiftReg_ce,
    shiftReg_ce_0,
    ap_rst_n_inv,
    E);
  output img2_data_stream_2_s_full_n;
  output img2_data_stream_2_s_empty_n;
  input ap_clk;
  input ap_rst_n;
  input shiftReg_ce;
  input shiftReg_ce_0;
  input ap_rst_n_inv;
  input [0:0]E;

  wire [0:0]E;
  wire ap_clk;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire img2_data_stream_2_s_empty_n;
  wire img2_data_stream_2_s_full_n;
  wire internal_empty_n_i_1__7_n_2;
  wire internal_full_n_i_1__9_n_2;
  wire \mOutPtr[0]_i_1__7_n_2 ;
  wire \mOutPtr[1]_i_1__6_n_2 ;
  wire \mOutPtr_reg_n_2_[0] ;
  wire \mOutPtr_reg_n_2_[1] ;
  wire shiftReg_ce;
  wire shiftReg_ce_0;

  LUT6 #(
    .INIT(64'hF0F0E0F000F00000)) 
    internal_empty_n_i_1__7
       (.I0(\mOutPtr_reg_n_2_[1] ),
        .I1(\mOutPtr_reg_n_2_[0] ),
        .I2(ap_rst_n),
        .I3(shiftReg_ce),
        .I4(shiftReg_ce_0),
        .I5(img2_data_stream_2_s_empty_n),
        .O(internal_empty_n_i_1__7_n_2));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__7_n_2),
        .Q(img2_data_stream_2_s_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hF0FFE0FFFFFFF0FF)) 
    internal_full_n_i_1__9
       (.I0(\mOutPtr_reg_n_2_[0] ),
        .I1(\mOutPtr_reg_n_2_[1] ),
        .I2(img2_data_stream_2_s_full_n),
        .I3(ap_rst_n),
        .I4(shiftReg_ce),
        .I5(shiftReg_ce_0),
        .O(internal_full_n_i_1__9_n_2));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__9_n_2),
        .Q(img2_data_stream_2_s_full_n),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair168" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \mOutPtr[0]_i_1__7 
       (.I0(\mOutPtr_reg_n_2_[0] ),
        .O(\mOutPtr[0]_i_1__7_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair168" *) 
  LUT4 #(
    .INIT(16'h4BB4)) 
    \mOutPtr[1]_i_1__6 
       (.I0(shiftReg_ce_0),
        .I1(shiftReg_ce),
        .I2(\mOutPtr_reg_n_2_[0] ),
        .I3(\mOutPtr_reg_n_2_[1] ),
        .O(\mOutPtr[1]_i_1__6_n_2 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(E),
        .D(\mOutPtr[0]_i_1__7_n_2 ),
        .Q(\mOutPtr_reg_n_2_[0] ),
        .S(ap_rst_n_inv));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(E),
        .D(\mOutPtr[1]_i_1__6_n_2 ),
        .Q(\mOutPtr_reg_n_2_[1] ),
        .S(ap_rst_n_inv));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d1_A" *) 
module system_edge_detect_0_1_fifo_w8_d1_A_8
   (img3_data_stream_0_s_full_n,
    img3_data_stream_0_s_empty_n,
    Q,
    ap_clk,
    ap_rst_n,
    \ap_CS_fsm_reg[2] ,
    shiftReg_ce,
    ap_rst_n_inv,
    E);
  output img3_data_stream_0_s_full_n;
  output img3_data_stream_0_s_empty_n;
  output [1:0]Q;
  input ap_clk;
  input ap_rst_n;
  input \ap_CS_fsm_reg[2] ;
  input shiftReg_ce;
  input ap_rst_n_inv;
  input [0:0]E;

  wire [0:0]E;
  wire [1:0]Q;
  wire \ap_CS_fsm_reg[2] ;
  wire ap_clk;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire img3_data_stream_0_s_empty_n;
  wire img3_data_stream_0_s_full_n;
  wire internal_empty_n_i_1__12_n_2;
  wire internal_full_n_i_1__12_n_2;
  wire \mOutPtr[0]_i_1__8_n_2 ;
  wire \mOutPtr[1]_i_2__4_n_2 ;
  wire shiftReg_ce;

  LUT6 #(
    .INIT(64'hF0F0E0F000F00000)) 
    internal_empty_n_i_1__12
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(ap_rst_n),
        .I3(\ap_CS_fsm_reg[2] ),
        .I4(shiftReg_ce),
        .I5(img3_data_stream_0_s_empty_n),
        .O(internal_empty_n_i_1__12_n_2));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__12_n_2),
        .Q(img3_data_stream_0_s_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hF0FFE0FFFFFFF0FF)) 
    internal_full_n_i_1__12
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(img3_data_stream_0_s_full_n),
        .I3(ap_rst_n),
        .I4(\ap_CS_fsm_reg[2] ),
        .I5(shiftReg_ce),
        .O(internal_full_n_i_1__12_n_2));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__12_n_2),
        .Q(img3_data_stream_0_s_full_n),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair169" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \mOutPtr[0]_i_1__8 
       (.I0(Q[0]),
        .O(\mOutPtr[0]_i_1__8_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair169" *) 
  LUT4 #(
    .INIT(16'h4BB4)) 
    \mOutPtr[1]_i_2__4 
       (.I0(shiftReg_ce),
        .I1(\ap_CS_fsm_reg[2] ),
        .I2(Q[0]),
        .I3(Q[1]),
        .O(\mOutPtr[1]_i_2__4_n_2 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(E),
        .D(\mOutPtr[0]_i_1__8_n_2 ),
        .Q(Q[0]),
        .S(ap_rst_n_inv));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(E),
        .D(\mOutPtr[1]_i_2__4_n_2 ),
        .Q(Q[1]),
        .S(ap_rst_n_inv));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d1_A" *) 
module system_edge_detect_0_1_fifo_w8_d1_A_9
   (img3_data_stream_1_s_full_n,
    img3_data_stream_1_s_empty_n,
    Q,
    ap_clk,
    ap_rst_n,
    \ap_CS_fsm_reg[2] ,
    shiftReg_ce,
    ap_rst_n_inv,
    E);
  output img3_data_stream_1_s_full_n;
  output img3_data_stream_1_s_empty_n;
  output [1:0]Q;
  input ap_clk;
  input ap_rst_n;
  input \ap_CS_fsm_reg[2] ;
  input shiftReg_ce;
  input ap_rst_n_inv;
  input [0:0]E;

  wire [0:0]E;
  wire [1:0]Q;
  wire \ap_CS_fsm_reg[2] ;
  wire ap_clk;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire img3_data_stream_1_s_empty_n;
  wire img3_data_stream_1_s_full_n;
  wire internal_empty_n_i_1__14_n_2;
  wire internal_full_n_i_1__13_n_2;
  wire \mOutPtr[0]_i_1__9_n_2 ;
  wire \mOutPtr[1]_i_1__9_n_2 ;
  wire shiftReg_ce;

  LUT6 #(
    .INIT(64'hF0F0E0F000F00000)) 
    internal_empty_n_i_1__14
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(ap_rst_n),
        .I3(\ap_CS_fsm_reg[2] ),
        .I4(shiftReg_ce),
        .I5(img3_data_stream_1_s_empty_n),
        .O(internal_empty_n_i_1__14_n_2));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__14_n_2),
        .Q(img3_data_stream_1_s_empty_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hF0FFE0FFFFFFF0FF)) 
    internal_full_n_i_1__13
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(img3_data_stream_1_s_full_n),
        .I3(ap_rst_n),
        .I4(\ap_CS_fsm_reg[2] ),
        .I5(shiftReg_ce),
        .O(internal_full_n_i_1__13_n_2));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__13_n_2),
        .Q(img3_data_stream_1_s_full_n),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair170" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \mOutPtr[0]_i_1__9 
       (.I0(Q[0]),
        .O(\mOutPtr[0]_i_1__9_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair170" *) 
  LUT4 #(
    .INIT(16'h4BB4)) 
    \mOutPtr[1]_i_1__9 
       (.I0(shiftReg_ce),
        .I1(\ap_CS_fsm_reg[2] ),
        .I2(Q[0]),
        .I3(Q[1]),
        .O(\mOutPtr[1]_i_1__9_n_2 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(E),
        .D(\mOutPtr[0]_i_1__9_n_2 ),
        .Q(Q[0]),
        .S(ap_rst_n_inv));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(E),
        .D(\mOutPtr[1]_i_1__9_n_2 ),
        .Q(Q[1]),
        .S(ap_rst_n_inv));
endmodule

module system_edge_detect_0_1_fifo_w8_d1_A_shiftReg
   (D,
    shiftReg_ce,
    img2_data_stream_0_s_dout,
    ap_clk,
    Q,
    \mOutPtr_reg[1] ,
    \mOutPtr_reg[1]_0 );
  output [23:0]D;
  input shiftReg_ce;
  input [7:0]img2_data_stream_0_s_dout;
  input ap_clk;
  input [1:0]Q;
  input [1:0]\mOutPtr_reg[1] ;
  input [1:0]\mOutPtr_reg[1]_0 ;

  wire [23:0]D;
  wire [1:0]Q;
  wire [7:0]\SRL_SIG_reg[0]_0 ;
  wire [7:0]\SRL_SIG_reg[1]_1 ;
  wire ap_clk;
  wire [7:0]img2_data_stream_0_s_dout;
  wire [1:0]\mOutPtr_reg[1] ;
  wire [1:0]\mOutPtr_reg[1]_0 ;
  wire shiftReg_ce;

  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[0]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [0]),
        .I1(\mOutPtr_reg[1]_0 [1]),
        .I2(\mOutPtr_reg[1]_0 [0]),
        .I3(\SRL_SIG_reg[1]_1 [0]),
        .O(D[0]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[10]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [2]),
        .I1(\mOutPtr_reg[1] [1]),
        .I2(\mOutPtr_reg[1] [0]),
        .I3(\SRL_SIG_reg[1]_1 [2]),
        .O(D[10]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[11]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [3]),
        .I1(\mOutPtr_reg[1] [1]),
        .I2(\mOutPtr_reg[1] [0]),
        .I3(\SRL_SIG_reg[1]_1 [3]),
        .O(D[11]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[12]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [4]),
        .I1(\mOutPtr_reg[1] [1]),
        .I2(\mOutPtr_reg[1] [0]),
        .I3(\SRL_SIG_reg[1]_1 [4]),
        .O(D[12]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[13]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [5]),
        .I1(\mOutPtr_reg[1] [1]),
        .I2(\mOutPtr_reg[1] [0]),
        .I3(\SRL_SIG_reg[1]_1 [5]),
        .O(D[13]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[14]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [6]),
        .I1(\mOutPtr_reg[1] [1]),
        .I2(\mOutPtr_reg[1] [0]),
        .I3(\SRL_SIG_reg[1]_1 [6]),
        .O(D[14]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[15]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [7]),
        .I1(\mOutPtr_reg[1] [1]),
        .I2(\mOutPtr_reg[1] [0]),
        .I3(\SRL_SIG_reg[1]_1 [7]),
        .O(D[15]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[16]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [0]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [0]),
        .O(D[16]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[17]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [1]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [1]),
        .O(D[17]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[18]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [2]),
        .O(D[18]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[19]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [3]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [3]),
        .O(D[19]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[1]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [1]),
        .I1(\mOutPtr_reg[1]_0 [1]),
        .I2(\mOutPtr_reg[1]_0 [0]),
        .I3(\SRL_SIG_reg[1]_1 [1]),
        .O(D[1]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[20]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [4]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [4]),
        .O(D[20]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[21]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [5]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [5]),
        .O(D[21]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[22]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [6]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [6]),
        .O(D[22]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[23]_i_2 
       (.I0(\SRL_SIG_reg[0]_0 [7]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [7]),
        .O(D[23]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[2]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [2]),
        .I1(\mOutPtr_reg[1]_0 [1]),
        .I2(\mOutPtr_reg[1]_0 [0]),
        .I3(\SRL_SIG_reg[1]_1 [2]),
        .O(D[2]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[3]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [3]),
        .I1(\mOutPtr_reg[1]_0 [1]),
        .I2(\mOutPtr_reg[1]_0 [0]),
        .I3(\SRL_SIG_reg[1]_1 [3]),
        .O(D[3]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[4]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [4]),
        .I1(\mOutPtr_reg[1]_0 [1]),
        .I2(\mOutPtr_reg[1]_0 [0]),
        .I3(\SRL_SIG_reg[1]_1 [4]),
        .O(D[4]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[5]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [5]),
        .I1(\mOutPtr_reg[1]_0 [1]),
        .I2(\mOutPtr_reg[1]_0 [0]),
        .I3(\SRL_SIG_reg[1]_1 [5]),
        .O(D[5]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[6]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [6]),
        .I1(\mOutPtr_reg[1]_0 [1]),
        .I2(\mOutPtr_reg[1]_0 [0]),
        .I3(\SRL_SIG_reg[1]_1 [6]),
        .O(D[6]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[7]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [7]),
        .I1(\mOutPtr_reg[1]_0 [1]),
        .I2(\mOutPtr_reg[1]_0 [0]),
        .I3(\SRL_SIG_reg[1]_1 [7]),
        .O(D[7]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[8]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [0]),
        .I1(\mOutPtr_reg[1] [1]),
        .I2(\mOutPtr_reg[1] [0]),
        .I3(\SRL_SIG_reg[1]_1 [0]),
        .O(D[8]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \AXI_video_strm_V_data_V_1_payload_A[9]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [1]),
        .I1(\mOutPtr_reg[1] [1]),
        .I2(\mOutPtr_reg[1] [0]),
        .I3(\SRL_SIG_reg[1]_1 [1]),
        .O(D[9]));
  FDRE \SRL_SIG_reg[0][0] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(img2_data_stream_0_s_dout[0]),
        .Q(\SRL_SIG_reg[0]_0 [0]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][1] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(img2_data_stream_0_s_dout[1]),
        .Q(\SRL_SIG_reg[0]_0 [1]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][2] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(img2_data_stream_0_s_dout[2]),
        .Q(\SRL_SIG_reg[0]_0 [2]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][3] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(img2_data_stream_0_s_dout[3]),
        .Q(\SRL_SIG_reg[0]_0 [3]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][4] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(img2_data_stream_0_s_dout[4]),
        .Q(\SRL_SIG_reg[0]_0 [4]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][5] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(img2_data_stream_0_s_dout[5]),
        .Q(\SRL_SIG_reg[0]_0 [5]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][6] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(img2_data_stream_0_s_dout[6]),
        .Q(\SRL_SIG_reg[0]_0 [6]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][7] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(img2_data_stream_0_s_dout[7]),
        .Q(\SRL_SIG_reg[0]_0 [7]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][0] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\SRL_SIG_reg[0]_0 [0]),
        .Q(\SRL_SIG_reg[1]_1 [0]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][1] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\SRL_SIG_reg[0]_0 [1]),
        .Q(\SRL_SIG_reg[1]_1 [1]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][2] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\SRL_SIG_reg[0]_0 [2]),
        .Q(\SRL_SIG_reg[1]_1 [2]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][3] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\SRL_SIG_reg[0]_0 [3]),
        .Q(\SRL_SIG_reg[1]_1 [3]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][4] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\SRL_SIG_reg[0]_0 [4]),
        .Q(\SRL_SIG_reg[1]_1 [4]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][5] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\SRL_SIG_reg[0]_0 [5]),
        .Q(\SRL_SIG_reg[1]_1 [5]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][6] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\SRL_SIG_reg[0]_0 [6]),
        .Q(\SRL_SIG_reg[1]_1 [6]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][7] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\SRL_SIG_reg[0]_0 [7]),
        .Q(\SRL_SIG_reg[1]_1 [7]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d1_A_shiftReg" *) 
module system_edge_detect_0_1_fifo_w8_d1_A_shiftReg_11
   (img2_data_stream_0_s_dout,
    Q,
    \tmp_57_reg_2927_reg[2] ,
    shiftReg_ce_0,
    \p_Val2_1_reg_2922_reg[7] ,
    ap_clk,
    \p_Val2_1_reg_2922_reg[6] ,
    \p_Val2_1_reg_2922_reg[5] ,
    \p_Val2_1_reg_2922_reg[4] ,
    \p_Val2_1_reg_2922_reg[3] ,
    \p_Val2_1_reg_2922_reg[2] ,
    \p_Val2_1_reg_2922_reg[1] ,
    \p_Val2_1_reg_2922_reg[0] );
  output [7:0]img2_data_stream_0_s_dout;
  input [1:0]Q;
  input \tmp_57_reg_2927_reg[2] ;
  input shiftReg_ce_0;
  input \p_Val2_1_reg_2922_reg[7] ;
  input ap_clk;
  input \p_Val2_1_reg_2922_reg[6] ;
  input \p_Val2_1_reg_2922_reg[5] ;
  input \p_Val2_1_reg_2922_reg[4] ;
  input \p_Val2_1_reg_2922_reg[3] ;
  input \p_Val2_1_reg_2922_reg[2] ;
  input \p_Val2_1_reg_2922_reg[1] ;
  input \p_Val2_1_reg_2922_reg[0] ;

  wire [1:0]Q;
  wire [7:0]\SRL_SIG_reg[0]_0 ;
  wire [7:0]\SRL_SIG_reg[1]_1 ;
  wire ap_clk;
  wire [7:0]img2_data_stream_0_s_dout;
  wire \p_Val2_1_reg_2922_reg[0] ;
  wire \p_Val2_1_reg_2922_reg[1] ;
  wire \p_Val2_1_reg_2922_reg[2] ;
  wire \p_Val2_1_reg_2922_reg[3] ;
  wire \p_Val2_1_reg_2922_reg[4] ;
  wire \p_Val2_1_reg_2922_reg[5] ;
  wire \p_Val2_1_reg_2922_reg[6] ;
  wire \p_Val2_1_reg_2922_reg[7] ;
  wire shiftReg_ce_0;
  wire \tmp_57_reg_2927_reg[2] ;

  LUT4 #(
    .INIT(16'hBA8A)) 
    \SRL_SIG[0][0]_i_2 
       (.I0(\SRL_SIG_reg[0]_0 [0]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [0]),
        .O(img2_data_stream_0_s_dout[0]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \SRL_SIG[0][1]_i_1__2 
       (.I0(\SRL_SIG_reg[0]_0 [1]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [1]),
        .O(img2_data_stream_0_s_dout[1]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \SRL_SIG[0][2]_i_1__2 
       (.I0(\SRL_SIG_reg[0]_0 [2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [2]),
        .O(img2_data_stream_0_s_dout[2]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \SRL_SIG[0][3]_i_1__2 
       (.I0(\SRL_SIG_reg[0]_0 [3]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [3]),
        .O(img2_data_stream_0_s_dout[3]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \SRL_SIG[0][4]_i_1__2 
       (.I0(\SRL_SIG_reg[0]_0 [4]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [4]),
        .O(img2_data_stream_0_s_dout[4]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \SRL_SIG[0][5]_i_1__2 
       (.I0(\SRL_SIG_reg[0]_0 [5]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [5]),
        .O(img2_data_stream_0_s_dout[5]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \SRL_SIG[0][6]_i_1__2 
       (.I0(\SRL_SIG_reg[0]_0 [6]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [6]),
        .O(img2_data_stream_0_s_dout[6]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \SRL_SIG[0][7]_i_1__1 
       (.I0(\SRL_SIG_reg[0]_0 [7]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [7]),
        .O(img2_data_stream_0_s_dout[7]));
  FDSE \SRL_SIG_reg[0][0] 
       (.C(ap_clk),
        .CE(shiftReg_ce_0),
        .D(\p_Val2_1_reg_2922_reg[0] ),
        .Q(\SRL_SIG_reg[0]_0 [0]),
        .S(\tmp_57_reg_2927_reg[2] ));
  FDSE \SRL_SIG_reg[0][1] 
       (.C(ap_clk),
        .CE(shiftReg_ce_0),
        .D(\p_Val2_1_reg_2922_reg[1] ),
        .Q(\SRL_SIG_reg[0]_0 [1]),
        .S(\tmp_57_reg_2927_reg[2] ));
  FDSE \SRL_SIG_reg[0][2] 
       (.C(ap_clk),
        .CE(shiftReg_ce_0),
        .D(\p_Val2_1_reg_2922_reg[2] ),
        .Q(\SRL_SIG_reg[0]_0 [2]),
        .S(\tmp_57_reg_2927_reg[2] ));
  FDSE \SRL_SIG_reg[0][3] 
       (.C(ap_clk),
        .CE(shiftReg_ce_0),
        .D(\p_Val2_1_reg_2922_reg[3] ),
        .Q(\SRL_SIG_reg[0]_0 [3]),
        .S(\tmp_57_reg_2927_reg[2] ));
  FDSE \SRL_SIG_reg[0][4] 
       (.C(ap_clk),
        .CE(shiftReg_ce_0),
        .D(\p_Val2_1_reg_2922_reg[4] ),
        .Q(\SRL_SIG_reg[0]_0 [4]),
        .S(\tmp_57_reg_2927_reg[2] ));
  FDSE \SRL_SIG_reg[0][5] 
       (.C(ap_clk),
        .CE(shiftReg_ce_0),
        .D(\p_Val2_1_reg_2922_reg[5] ),
        .Q(\SRL_SIG_reg[0]_0 [5]),
        .S(\tmp_57_reg_2927_reg[2] ));
  FDSE \SRL_SIG_reg[0][6] 
       (.C(ap_clk),
        .CE(shiftReg_ce_0),
        .D(\p_Val2_1_reg_2922_reg[6] ),
        .Q(\SRL_SIG_reg[0]_0 [6]),
        .S(\tmp_57_reg_2927_reg[2] ));
  FDSE \SRL_SIG_reg[0][7] 
       (.C(ap_clk),
        .CE(shiftReg_ce_0),
        .D(\p_Val2_1_reg_2922_reg[7] ),
        .Q(\SRL_SIG_reg[0]_0 [7]),
        .S(\tmp_57_reg_2927_reg[2] ));
  FDRE \SRL_SIG_reg[1][0] 
       (.C(ap_clk),
        .CE(shiftReg_ce_0),
        .D(\SRL_SIG_reg[0]_0 [0]),
        .Q(\SRL_SIG_reg[1]_1 [0]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][1] 
       (.C(ap_clk),
        .CE(shiftReg_ce_0),
        .D(\SRL_SIG_reg[0]_0 [1]),
        .Q(\SRL_SIG_reg[1]_1 [1]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][2] 
       (.C(ap_clk),
        .CE(shiftReg_ce_0),
        .D(\SRL_SIG_reg[0]_0 [2]),
        .Q(\SRL_SIG_reg[1]_1 [2]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][3] 
       (.C(ap_clk),
        .CE(shiftReg_ce_0),
        .D(\SRL_SIG_reg[0]_0 [3]),
        .Q(\SRL_SIG_reg[1]_1 [3]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][4] 
       (.C(ap_clk),
        .CE(shiftReg_ce_0),
        .D(\SRL_SIG_reg[0]_0 [4]),
        .Q(\SRL_SIG_reg[1]_1 [4]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][5] 
       (.C(ap_clk),
        .CE(shiftReg_ce_0),
        .D(\SRL_SIG_reg[0]_0 [5]),
        .Q(\SRL_SIG_reg[1]_1 [5]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][6] 
       (.C(ap_clk),
        .CE(shiftReg_ce_0),
        .D(\SRL_SIG_reg[0]_0 [6]),
        .Q(\SRL_SIG_reg[1]_1 [6]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][7] 
       (.C(ap_clk),
        .CE(shiftReg_ce_0),
        .D(\SRL_SIG_reg[0]_0 [7]),
        .Q(\SRL_SIG_reg[1]_1 [7]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d1_A_shiftReg" *) 
module system_edge_detect_0_1_fifo_w8_d1_A_shiftReg_12
   (\reg_588_reg[7] ,
    Q,
    \r_V_1_reg_389_reg[29] ,
    shiftReg_ce,
    \p_Val2_15_reg_394_reg[7] ,
    ap_clk);
  output [7:0]\reg_588_reg[7] ;
  input [1:0]Q;
  input \r_V_1_reg_389_reg[29] ;
  input shiftReg_ce;
  input [7:0]\p_Val2_15_reg_394_reg[7] ;
  input ap_clk;

  wire [1:0]Q;
  wire [7:0]\SRL_SIG_reg[0]_0 ;
  wire [7:0]\SRL_SIG_reg[1]_1 ;
  wire ap_clk;
  wire [7:0]\p_Val2_15_reg_394_reg[7] ;
  wire \r_V_1_reg_389_reg[29] ;
  wire [7:0]\reg_588_reg[7] ;
  wire shiftReg_ce;

  FDSE \SRL_SIG_reg[0][0] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\p_Val2_15_reg_394_reg[7] [0]),
        .Q(\SRL_SIG_reg[0]_0 [0]),
        .S(\r_V_1_reg_389_reg[29] ));
  FDSE \SRL_SIG_reg[0][1] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\p_Val2_15_reg_394_reg[7] [1]),
        .Q(\SRL_SIG_reg[0]_0 [1]),
        .S(\r_V_1_reg_389_reg[29] ));
  FDSE \SRL_SIG_reg[0][2] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\p_Val2_15_reg_394_reg[7] [2]),
        .Q(\SRL_SIG_reg[0]_0 [2]),
        .S(\r_V_1_reg_389_reg[29] ));
  FDSE \SRL_SIG_reg[0][3] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\p_Val2_15_reg_394_reg[7] [3]),
        .Q(\SRL_SIG_reg[0]_0 [3]),
        .S(\r_V_1_reg_389_reg[29] ));
  FDSE \SRL_SIG_reg[0][4] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\p_Val2_15_reg_394_reg[7] [4]),
        .Q(\SRL_SIG_reg[0]_0 [4]),
        .S(\r_V_1_reg_389_reg[29] ));
  FDSE \SRL_SIG_reg[0][5] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\p_Val2_15_reg_394_reg[7] [5]),
        .Q(\SRL_SIG_reg[0]_0 [5]),
        .S(\r_V_1_reg_389_reg[29] ));
  FDSE \SRL_SIG_reg[0][6] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\p_Val2_15_reg_394_reg[7] [6]),
        .Q(\SRL_SIG_reg[0]_0 [6]),
        .S(\r_V_1_reg_389_reg[29] ));
  FDSE \SRL_SIG_reg[0][7] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\p_Val2_15_reg_394_reg[7] [7]),
        .Q(\SRL_SIG_reg[0]_0 [7]),
        .S(\r_V_1_reg_389_reg[29] ));
  FDRE \SRL_SIG_reg[1][0] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\SRL_SIG_reg[0]_0 [0]),
        .Q(\SRL_SIG_reg[1]_1 [0]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][1] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\SRL_SIG_reg[0]_0 [1]),
        .Q(\SRL_SIG_reg[1]_1 [1]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][2] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\SRL_SIG_reg[0]_0 [2]),
        .Q(\SRL_SIG_reg[1]_1 [2]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][3] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\SRL_SIG_reg[0]_0 [3]),
        .Q(\SRL_SIG_reg[1]_1 [3]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][4] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\SRL_SIG_reg[0]_0 [4]),
        .Q(\SRL_SIG_reg[1]_1 [4]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][5] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\SRL_SIG_reg[0]_0 [5]),
        .Q(\SRL_SIG_reg[1]_1 [5]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][6] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\SRL_SIG_reg[0]_0 [6]),
        .Q(\SRL_SIG_reg[1]_1 [6]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][7] 
       (.C(ap_clk),
        .CE(shiftReg_ce),
        .D(\SRL_SIG_reg[0]_0 [7]),
        .Q(\SRL_SIG_reg[1]_1 [7]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \reg_588[0]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [0]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [0]),
        .O(\reg_588_reg[7] [0]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \reg_588[1]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [1]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [1]),
        .O(\reg_588_reg[7] [1]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \reg_588[2]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [2]),
        .O(\reg_588_reg[7] [2]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \reg_588[3]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [3]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [3]),
        .O(\reg_588_reg[7] [3]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \reg_588[4]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [4]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [4]),
        .O(\reg_588_reg[7] [4]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \reg_588[5]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [5]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [5]),
        .O(\reg_588_reg[7] [5]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \reg_588[6]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [6]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [6]),
        .O(\reg_588_reg[7] [6]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \reg_588[7]_i_2 
       (.I0(\SRL_SIG_reg[0]_0 [7]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [7]),
        .O(\reg_588_reg[7] [7]));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d1_A_shiftReg" *) 
module system_edge_detect_0_1_fifo_w8_d1_A_shiftReg_13
   (B,
    Q,
    \exitcond_reg_420_reg[0] ,
    D,
    ap_clk);
  output [7:0]B;
  input [1:0]Q;
  input \exitcond_reg_420_reg[0] ;
  input [7:0]D;
  input ap_clk;

  wire [7:0]B;
  wire [7:0]D;
  wire [1:0]Q;
  wire [7:0]\SRL_SIG_reg[0]_0 ;
  wire [7:0]\SRL_SIG_reg[1]_1 ;
  wire ap_clk;
  wire \exitcond_reg_420_reg[0] ;

  FDRE \SRL_SIG_reg[0][0] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(D[0]),
        .Q(\SRL_SIG_reg[0]_0 [0]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][1] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(D[1]),
        .Q(\SRL_SIG_reg[0]_0 [1]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][2] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(D[2]),
        .Q(\SRL_SIG_reg[0]_0 [2]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][3] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(D[3]),
        .Q(\SRL_SIG_reg[0]_0 [3]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][4] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(D[4]),
        .Q(\SRL_SIG_reg[0]_0 [4]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][5] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(D[5]),
        .Q(\SRL_SIG_reg[0]_0 [5]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][6] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(D[6]),
        .Q(\SRL_SIG_reg[0]_0 [6]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][7] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(D[7]),
        .Q(\SRL_SIG_reg[0]_0 [7]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][0] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [0]),
        .Q(\SRL_SIG_reg[1]_1 [0]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][1] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [1]),
        .Q(\SRL_SIG_reg[1]_1 [1]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][2] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [2]),
        .Q(\SRL_SIG_reg[1]_1 [2]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][3] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [3]),
        .Q(\SRL_SIG_reg[1]_1 [3]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][4] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [4]),
        .Q(\SRL_SIG_reg[1]_1 [4]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][5] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [5]),
        .Q(\SRL_SIG_reg[1]_1 [5]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][6] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [6]),
        .Q(\SRL_SIG_reg[1]_1 [6]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][7] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [7]),
        .Q(\SRL_SIG_reg[1]_1 [7]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hBA8A)) 
    p_Val2_12_reg_384_reg_i_10
       (.I0(\SRL_SIG_reg[0]_0 [2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [2]),
        .O(B[2]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    p_Val2_12_reg_384_reg_i_11
       (.I0(\SRL_SIG_reg[0]_0 [1]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [1]),
        .O(B[1]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    p_Val2_12_reg_384_reg_i_12
       (.I0(\SRL_SIG_reg[0]_0 [0]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [0]),
        .O(B[0]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    p_Val2_12_reg_384_reg_i_5
       (.I0(\SRL_SIG_reg[0]_0 [7]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [7]),
        .O(B[7]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    p_Val2_12_reg_384_reg_i_6
       (.I0(\SRL_SIG_reg[0]_0 [6]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [6]),
        .O(B[6]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    p_Val2_12_reg_384_reg_i_7
       (.I0(\SRL_SIG_reg[0]_0 [5]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [5]),
        .O(B[5]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    p_Val2_12_reg_384_reg_i_8
       (.I0(\SRL_SIG_reg[0]_0 [4]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [4]),
        .O(B[4]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    p_Val2_12_reg_384_reg_i_9
       (.I0(\SRL_SIG_reg[0]_0 [3]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [3]),
        .O(B[3]));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d1_A_shiftReg" *) 
module system_edge_detect_0_1_fifo_w8_d1_A_shiftReg_14
   (D,
    Q,
    \exitcond_reg_420_reg[0] ,
    \axi_data_V_1_reg_236_reg[15] ,
    ap_clk);
  output [7:0]D;
  input [1:0]Q;
  input [0:0]\exitcond_reg_420_reg[0] ;
  input [7:0]\axi_data_V_1_reg_236_reg[15] ;
  input ap_clk;

  wire [7:0]D;
  wire [1:0]Q;
  wire [7:0]\SRL_SIG_reg[0]_0 ;
  wire [7:0]\SRL_SIG_reg[1]_1 ;
  wire ap_clk;
  wire [7:0]\axi_data_V_1_reg_236_reg[15] ;
  wire [0:0]\exitcond_reg_420_reg[0] ;

  FDRE \SRL_SIG_reg[0][0] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\axi_data_V_1_reg_236_reg[15] [0]),
        .Q(\SRL_SIG_reg[0]_0 [0]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][1] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\axi_data_V_1_reg_236_reg[15] [1]),
        .Q(\SRL_SIG_reg[0]_0 [1]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][2] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\axi_data_V_1_reg_236_reg[15] [2]),
        .Q(\SRL_SIG_reg[0]_0 [2]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][3] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\axi_data_V_1_reg_236_reg[15] [3]),
        .Q(\SRL_SIG_reg[0]_0 [3]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][4] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\axi_data_V_1_reg_236_reg[15] [4]),
        .Q(\SRL_SIG_reg[0]_0 [4]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][5] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\axi_data_V_1_reg_236_reg[15] [5]),
        .Q(\SRL_SIG_reg[0]_0 [5]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][6] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\axi_data_V_1_reg_236_reg[15] [6]),
        .Q(\SRL_SIG_reg[0]_0 [6]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][7] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\axi_data_V_1_reg_236_reg[15] [7]),
        .Q(\SRL_SIG_reg[0]_0 [7]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][0] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [0]),
        .Q(\SRL_SIG_reg[1]_1 [0]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][1] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [1]),
        .Q(\SRL_SIG_reg[1]_1 [1]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][2] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [2]),
        .Q(\SRL_SIG_reg[1]_1 [2]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][3] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [3]),
        .Q(\SRL_SIG_reg[1]_1 [3]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][4] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [4]),
        .Q(\SRL_SIG_reg[1]_1 [4]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][5] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [5]),
        .Q(\SRL_SIG_reg[1]_1 [5]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][6] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [6]),
        .Q(\SRL_SIG_reg[1]_1 [6]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][7] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [7]),
        .Q(\SRL_SIG_reg[1]_1 [7]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \tmp_102_reg_369[0]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [0]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [0]),
        .O(D[0]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \tmp_102_reg_369[1]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [1]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [1]),
        .O(D[1]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \tmp_102_reg_369[2]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [2]),
        .O(D[2]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \tmp_102_reg_369[3]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [3]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [3]),
        .O(D[3]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \tmp_102_reg_369[4]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [4]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [4]),
        .O(D[4]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \tmp_102_reg_369[5]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [5]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [5]),
        .O(D[5]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \tmp_102_reg_369[6]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [6]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [6]),
        .O(D[6]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \tmp_102_reg_369[7]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [7]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [7]),
        .O(D[7]));
endmodule

(* ORIG_REF_NAME = "fifo_w8_d1_A_shiftReg" *) 
module system_edge_detect_0_1_fifo_w8_d1_A_shiftReg_15
   (\tmp_101_reg_364_reg[7] ,
    Q,
    \exitcond_reg_420_reg[0] ,
    D,
    ap_clk);
  output [7:0]\tmp_101_reg_364_reg[7] ;
  input [1:0]Q;
  input [0:0]\exitcond_reg_420_reg[0] ;
  input [7:0]D;
  input ap_clk;

  wire [7:0]D;
  wire [1:0]Q;
  wire [7:0]\SRL_SIG_reg[0]_0 ;
  wire [7:0]\SRL_SIG_reg[1]_1 ;
  wire ap_clk;
  wire [0:0]\exitcond_reg_420_reg[0] ;
  wire [7:0]\tmp_101_reg_364_reg[7] ;

  FDRE \SRL_SIG_reg[0][0] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(D[0]),
        .Q(\SRL_SIG_reg[0]_0 [0]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][1] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(D[1]),
        .Q(\SRL_SIG_reg[0]_0 [1]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][2] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(D[2]),
        .Q(\SRL_SIG_reg[0]_0 [2]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][3] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(D[3]),
        .Q(\SRL_SIG_reg[0]_0 [3]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][4] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(D[4]),
        .Q(\SRL_SIG_reg[0]_0 [4]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][5] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(D[5]),
        .Q(\SRL_SIG_reg[0]_0 [5]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][6] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(D[6]),
        .Q(\SRL_SIG_reg[0]_0 [6]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[0][7] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(D[7]),
        .Q(\SRL_SIG_reg[0]_0 [7]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][0] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [0]),
        .Q(\SRL_SIG_reg[1]_1 [0]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][1] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [1]),
        .Q(\SRL_SIG_reg[1]_1 [1]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][2] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [2]),
        .Q(\SRL_SIG_reg[1]_1 [2]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][3] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [3]),
        .Q(\SRL_SIG_reg[1]_1 [3]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][4] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [4]),
        .Q(\SRL_SIG_reg[1]_1 [4]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][5] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [5]),
        .Q(\SRL_SIG_reg[1]_1 [5]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][6] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [6]),
        .Q(\SRL_SIG_reg[1]_1 [6]),
        .R(1'b0));
  FDRE \SRL_SIG_reg[1][7] 
       (.C(ap_clk),
        .CE(\exitcond_reg_420_reg[0] ),
        .D(\SRL_SIG_reg[0]_0 [7]),
        .Q(\SRL_SIG_reg[1]_1 [7]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \tmp_101_reg_364[0]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [0]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [0]),
        .O(\tmp_101_reg_364_reg[7] [0]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \tmp_101_reg_364[1]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [1]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [1]),
        .O(\tmp_101_reg_364_reg[7] [1]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \tmp_101_reg_364[2]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [2]),
        .O(\tmp_101_reg_364_reg[7] [2]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \tmp_101_reg_364[3]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [3]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [3]),
        .O(\tmp_101_reg_364_reg[7] [3]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \tmp_101_reg_364[4]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [4]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [4]),
        .O(\tmp_101_reg_364_reg[7] [4]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \tmp_101_reg_364[5]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [5]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [5]),
        .O(\tmp_101_reg_364_reg[7] [5]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \tmp_101_reg_364[6]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [6]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [6]),
        .O(\tmp_101_reg_364_reg[7] [6]));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \tmp_101_reg_364[7]_i_1 
       (.I0(\SRL_SIG_reg[0]_0 [7]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\SRL_SIG_reg[1]_1 [7]),
        .O(\tmp_101_reg_364_reg[7] [7]));
endmodule

module system_edge_detect_0_1_start_for_CvtColoocq
   (start_for_CvtColor_U0_full_n,
    CvtColor_U0_ap_start,
    \mOutPtr_reg[1]_0 ,
    ap_clk,
    start_for_Sobel_U0_full_n,
    start_once_reg,
    \ap_CS_fsm_reg[1] ,
    ap_rst_n,
    start_once_reg_0,
    ap_rst_n_inv);
  output start_for_CvtColor_U0_full_n;
  output CvtColor_U0_ap_start;
  output \mOutPtr_reg[1]_0 ;
  input ap_clk;
  input start_for_Sobel_U0_full_n;
  input start_once_reg;
  input \ap_CS_fsm_reg[1] ;
  input ap_rst_n;
  input start_once_reg_0;
  input ap_rst_n_inv;

  wire CvtColor_U0_ap_start;
  wire \ap_CS_fsm_reg[1] ;
  wire ap_clk;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire internal_empty_n_i_1__3_n_2;
  wire internal_empty_n_i_2_n_2;
  wire internal_full_n_i_1__3_n_2;
  wire internal_full_n_i_2__0_n_2;
  wire \mOutPtr[0]_i_1_n_2 ;
  wire \mOutPtr[1]_i_1_n_2 ;
  wire \mOutPtr_reg[1]_0 ;
  wire \mOutPtr_reg_n_2_[0] ;
  wire \mOutPtr_reg_n_2_[1] ;
  wire start_for_CvtColor_U0_full_n;
  wire start_for_Sobel_U0_full_n;
  wire start_once_reg;
  wire start_once_reg_0;

  LUT6 #(
    .INIT(64'hFFEFFF0000000000)) 
    internal_empty_n_i_1__3
       (.I0(\mOutPtr_reg_n_2_[1] ),
        .I1(\mOutPtr_reg_n_2_[0] ),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(internal_empty_n_i_2_n_2),
        .I4(CvtColor_U0_ap_start),
        .I5(ap_rst_n),
        .O(internal_empty_n_i_1__3_n_2));
  LUT2 #(
    .INIT(4'h2)) 
    internal_empty_n_i_2
       (.I0(start_for_CvtColor_U0_full_n),
        .I1(start_once_reg_0),
        .O(internal_empty_n_i_2_n_2));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__3_n_2),
        .Q(CvtColor_U0_ap_start),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFD5555)) 
    internal_full_n_i_1__3
       (.I0(ap_rst_n),
        .I1(\mOutPtr_reg_n_2_[1] ),
        .I2(\mOutPtr_reg_n_2_[0] ),
        .I3(start_once_reg_0),
        .I4(start_for_CvtColor_U0_full_n),
        .I5(internal_full_n_i_2__0_n_2),
        .O(internal_full_n_i_1__3_n_2));
  (* SOFT_HLUTNM = "soft_lutpair172" *) 
  LUT2 #(
    .INIT(4'h8)) 
    internal_full_n_i_2__0
       (.I0(CvtColor_U0_ap_start),
        .I1(\ap_CS_fsm_reg[1] ),
        .O(internal_full_n_i_2__0_n_2));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__3_n_2),
        .Q(start_for_CvtColor_U0_full_n),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair172" *) 
  LUT5 #(
    .INIT(32'h77878878)) 
    \mOutPtr[0]_i_1 
       (.I0(CvtColor_U0_ap_start),
        .I1(\ap_CS_fsm_reg[1] ),
        .I2(start_for_CvtColor_U0_full_n),
        .I3(start_once_reg_0),
        .I4(\mOutPtr_reg_n_2_[0] ),
        .O(\mOutPtr[0]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'hBADFDFDF45202020)) 
    \mOutPtr[1]_i_1 
       (.I0(\mOutPtr_reg_n_2_[0] ),
        .I1(start_once_reg_0),
        .I2(start_for_CvtColor_U0_full_n),
        .I3(\ap_CS_fsm_reg[1] ),
        .I4(CvtColor_U0_ap_start),
        .I5(\mOutPtr_reg_n_2_[1] ),
        .O(\mOutPtr[1]_i_1_n_2 ));
  LUT3 #(
    .INIT(8'hA8)) 
    \mOutPtr[1]_i_2__0 
       (.I0(CvtColor_U0_ap_start),
        .I1(start_for_Sobel_U0_full_n),
        .I2(start_once_reg),
        .O(\mOutPtr_reg[1]_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[0]_i_1_n_2 ),
        .Q(\mOutPtr_reg_n_2_[0] ),
        .S(ap_rst_n_inv));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[1]_i_1_n_2 ),
        .Q(\mOutPtr_reg_n_2_[1] ),
        .S(ap_rst_n_inv));
endmodule

module system_edge_detect_0_1_start_for_CvtColopcA
   (start_for_CvtColor_1_U0_full_n,
    CvtColor_1_U0_ap_start,
    ap_clk,
    \ap_CS_fsm_reg[1] ,
    ap_rst_n,
    Sobel_U0_ap_start,
    start_once_reg,
    ap_rst_n_inv);
  output start_for_CvtColor_1_U0_full_n;
  output CvtColor_1_U0_ap_start;
  input ap_clk;
  input \ap_CS_fsm_reg[1] ;
  input ap_rst_n;
  input Sobel_U0_ap_start;
  input start_once_reg;
  input ap_rst_n_inv;

  wire CvtColor_1_U0_ap_start;
  wire Sobel_U0_ap_start;
  wire \ap_CS_fsm_reg[1] ;
  wire ap_clk;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire internal_empty_n_i_1__10_n_2;
  wire internal_full_n_i_1__10_n_2;
  wire internal_full_n_i_2__1_n_2;
  wire \mOutPtr[0]_i_1_n_2 ;
  wire \mOutPtr[1]_i_1_n_2 ;
  wire \mOutPtr[1]_i_2__3_n_2 ;
  wire \mOutPtr_reg_n_2_[0] ;
  wire \mOutPtr_reg_n_2_[1] ;
  wire start_for_CvtColor_1_U0_full_n;
  wire start_once_reg;

  LUT6 #(
    .INIT(64'hFFEFFF0000000000)) 
    internal_empty_n_i_1__10
       (.I0(\mOutPtr_reg_n_2_[1] ),
        .I1(\mOutPtr_reg_n_2_[0] ),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(internal_full_n_i_2__1_n_2),
        .I4(CvtColor_1_U0_ap_start),
        .I5(ap_rst_n),
        .O(internal_empty_n_i_1__10_n_2));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__10_n_2),
        .Q(CvtColor_1_U0_ap_start),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hDDDDFFFFDDD5DDDD)) 
    internal_full_n_i_1__10
       (.I0(ap_rst_n),
        .I1(start_for_CvtColor_1_U0_full_n),
        .I2(\mOutPtr_reg_n_2_[1] ),
        .I3(\mOutPtr_reg_n_2_[0] ),
        .I4(internal_full_n_i_2__1_n_2),
        .I5(\mOutPtr[1]_i_2__3_n_2 ),
        .O(internal_full_n_i_1__10_n_2));
  LUT3 #(
    .INIT(8'h08)) 
    internal_full_n_i_2__1
       (.I0(start_for_CvtColor_1_U0_full_n),
        .I1(Sobel_U0_ap_start),
        .I2(start_once_reg),
        .O(internal_full_n_i_2__1_n_2));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__10_n_2),
        .Q(start_for_CvtColor_1_U0_full_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h7777877788887888)) 
    \mOutPtr[0]_i_1 
       (.I0(CvtColor_1_U0_ap_start),
        .I1(\ap_CS_fsm_reg[1] ),
        .I2(start_for_CvtColor_1_U0_full_n),
        .I3(Sobel_U0_ap_start),
        .I4(start_once_reg),
        .I5(\mOutPtr_reg_n_2_[0] ),
        .O(\mOutPtr[0]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'hBAAADFFF45552000)) 
    \mOutPtr[1]_i_1 
       (.I0(\mOutPtr_reg_n_2_[0] ),
        .I1(start_once_reg),
        .I2(Sobel_U0_ap_start),
        .I3(start_for_CvtColor_1_U0_full_n),
        .I4(\mOutPtr[1]_i_2__3_n_2 ),
        .I5(\mOutPtr_reg_n_2_[1] ),
        .O(\mOutPtr[1]_i_1_n_2 ));
  LUT2 #(
    .INIT(4'h8)) 
    \mOutPtr[1]_i_2__3 
       (.I0(CvtColor_1_U0_ap_start),
        .I1(\ap_CS_fsm_reg[1] ),
        .O(\mOutPtr[1]_i_2__3_n_2 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[0]_i_1_n_2 ),
        .Q(\mOutPtr_reg_n_2_[0] ),
        .S(ap_rst_n_inv));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[1]_i_1_n_2 ),
        .Q(\mOutPtr_reg_n_2_[1] ),
        .S(ap_rst_n_inv));
endmodule

module system_edge_detect_0_1_start_for_Mat2AXIqcK
   (start_for_Mat2AXIvideo_U0_full_n,
    Mat2AXIvideo_U0_ap_start,
    ap_clk,
    internal_empty_n_reg_0,
    ap_rst_n,
    CvtColor_1_U0_ap_start,
    start_once_reg,
    ap_rst_n_inv);
  output start_for_Mat2AXIvideo_U0_full_n;
  output Mat2AXIvideo_U0_ap_start;
  input ap_clk;
  input internal_empty_n_reg_0;
  input ap_rst_n;
  input CvtColor_1_U0_ap_start;
  input start_once_reg;
  input ap_rst_n_inv;

  wire CvtColor_1_U0_ap_start;
  wire Mat2AXIvideo_U0_ap_start;
  wire ap_clk;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire internal_empty_n_i_1__11_n_2;
  wire internal_empty_n_reg_0;
  wire internal_full_n_i_1__11_n_2;
  wire internal_full_n_i_2__2_n_2;
  wire \mOutPtr[0]_i_1_n_2 ;
  wire \mOutPtr[1]_i_1_n_2 ;
  wire \mOutPtr_reg_n_2_[0] ;
  wire \mOutPtr_reg_n_2_[1] ;
  wire start_for_Mat2AXIvideo_U0_full_n;
  wire start_once_reg;

  LUT6 #(
    .INIT(64'hFFEF0F0000000000)) 
    internal_empty_n_i_1__11
       (.I0(\mOutPtr_reg_n_2_[1] ),
        .I1(\mOutPtr_reg_n_2_[0] ),
        .I2(internal_empty_n_reg_0),
        .I3(internal_full_n_i_2__2_n_2),
        .I4(Mat2AXIvideo_U0_ap_start),
        .I5(ap_rst_n),
        .O(internal_empty_n_i_1__11_n_2));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__11_n_2),
        .Q(Mat2AXIvideo_U0_ap_start),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hDDDDFFFFDDD5DDDD)) 
    internal_full_n_i_1__11
       (.I0(ap_rst_n),
        .I1(start_for_Mat2AXIvideo_U0_full_n),
        .I2(\mOutPtr_reg_n_2_[1] ),
        .I3(\mOutPtr_reg_n_2_[0] ),
        .I4(internal_full_n_i_2__2_n_2),
        .I5(internal_empty_n_reg_0),
        .O(internal_full_n_i_1__11_n_2));
  (* SOFT_HLUTNM = "soft_lutpair173" *) 
  LUT3 #(
    .INIT(8'h08)) 
    internal_full_n_i_2__2
       (.I0(start_for_Mat2AXIvideo_U0_full_n),
        .I1(CvtColor_1_U0_ap_start),
        .I2(start_once_reg),
        .O(internal_full_n_i_2__2_n_2));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__11_n_2),
        .Q(start_for_Mat2AXIvideo_U0_full_n),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair173" *) 
  LUT5 #(
    .INIT(32'h5595AA6A)) 
    \mOutPtr[0]_i_1 
       (.I0(internal_empty_n_reg_0),
        .I1(start_for_Mat2AXIvideo_U0_full_n),
        .I2(CvtColor_1_U0_ap_start),
        .I3(start_once_reg),
        .I4(\mOutPtr_reg_n_2_[0] ),
        .O(\mOutPtr[0]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'hBAAADFFF45552000)) 
    \mOutPtr[1]_i_1 
       (.I0(\mOutPtr_reg_n_2_[0] ),
        .I1(start_once_reg),
        .I2(CvtColor_1_U0_ap_start),
        .I3(start_for_Mat2AXIvideo_U0_full_n),
        .I4(internal_empty_n_reg_0),
        .I5(\mOutPtr_reg_n_2_[1] ),
        .O(\mOutPtr[1]_i_1_n_2 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[0]_i_1_n_2 ),
        .Q(\mOutPtr_reg_n_2_[0] ),
        .S(ap_rst_n_inv));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[1]_i_1_n_2 ),
        .Q(\mOutPtr_reg_n_2_[1] ),
        .S(ap_rst_n_inv));
endmodule

module system_edge_detect_0_1_start_for_Sobel_U0
   (start_for_Sobel_U0_full_n,
    Sobel_U0_ap_start,
    ap_clk,
    \ap_CS_fsm_reg[1] ,
    ap_rst_n,
    CvtColor_U0_ap_start,
    start_once_reg,
    internal_empty_n_reg_0,
    ap_rst_n_inv);
  output start_for_Sobel_U0_full_n;
  output Sobel_U0_ap_start;
  input ap_clk;
  input \ap_CS_fsm_reg[1] ;
  input ap_rst_n;
  input CvtColor_U0_ap_start;
  input start_once_reg;
  input internal_empty_n_reg_0;
  input ap_rst_n_inv;

  wire CvtColor_U0_ap_start;
  wire Sobel_U0_ap_start;
  wire \ap_CS_fsm_reg[1] ;
  wire ap_clk;
  wire ap_rst_n;
  wire ap_rst_n_inv;
  wire internal_empty_n_i_1__2_n_2;
  wire internal_empty_n_reg_0;
  wire internal_full_n_i_1__2_n_2;
  wire internal_full_n_i_2__6_n_2;
  wire internal_full_n_i_3_n_2;
  wire \mOutPtr[0]_i_1_n_2 ;
  wire \mOutPtr[1]_i_1_n_2 ;
  wire \mOutPtr_reg_n_2_[0] ;
  wire \mOutPtr_reg_n_2_[1] ;
  wire start_for_Sobel_U0_full_n;
  wire start_once_reg;

  LUT6 #(
    .INIT(64'hFFEFFF0000000000)) 
    internal_empty_n_i_1__2
       (.I0(\mOutPtr_reg_n_2_[1] ),
        .I1(\mOutPtr_reg_n_2_[0] ),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(internal_full_n_i_3_n_2),
        .I4(Sobel_U0_ap_start),
        .I5(ap_rst_n),
        .O(internal_empty_n_i_1__2_n_2));
  FDRE #(
    .INIT(1'b0)) 
    internal_empty_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_empty_n_i_1__2_n_2),
        .Q(Sobel_U0_ap_start),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hDDFFD5DDD5DDD5DD)) 
    internal_full_n_i_1__2
       (.I0(ap_rst_n),
        .I1(start_for_Sobel_U0_full_n),
        .I2(internal_full_n_i_2__6_n_2),
        .I3(internal_full_n_i_3_n_2),
        .I4(\ap_CS_fsm_reg[1] ),
        .I5(Sobel_U0_ap_start),
        .O(internal_full_n_i_1__2_n_2));
  LUT2 #(
    .INIT(4'hE)) 
    internal_full_n_i_2__6
       (.I0(\mOutPtr_reg_n_2_[1] ),
        .I1(\mOutPtr_reg_n_2_[0] ),
        .O(internal_full_n_i_2__6_n_2));
  LUT3 #(
    .INIT(8'h08)) 
    internal_full_n_i_3
       (.I0(start_for_Sobel_U0_full_n),
        .I1(CvtColor_U0_ap_start),
        .I2(start_once_reg),
        .O(internal_full_n_i_3_n_2));
  FDRE #(
    .INIT(1'b1)) 
    internal_full_n_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(internal_full_n_i_1__2_n_2),
        .Q(start_for_Sobel_U0_full_n),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h7777877788887888)) 
    \mOutPtr[0]_i_1 
       (.I0(Sobel_U0_ap_start),
        .I1(\ap_CS_fsm_reg[1] ),
        .I2(start_for_Sobel_U0_full_n),
        .I3(CvtColor_U0_ap_start),
        .I4(start_once_reg),
        .I5(\mOutPtr_reg_n_2_[0] ),
        .O(\mOutPtr[0]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'hBADFDFDF45202020)) 
    \mOutPtr[1]_i_1 
       (.I0(\mOutPtr_reg_n_2_[0] ),
        .I1(start_once_reg),
        .I2(internal_empty_n_reg_0),
        .I3(\ap_CS_fsm_reg[1] ),
        .I4(Sobel_U0_ap_start),
        .I5(\mOutPtr_reg_n_2_[1] ),
        .O(\mOutPtr[1]_i_1_n_2 ));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[0]_i_1_n_2 ),
        .Q(\mOutPtr_reg_n_2_[0] ),
        .S(ap_rst_n_inv));
  FDSE #(
    .INIT(1'b1)) 
    \mOutPtr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\mOutPtr[1]_i_1_n_2 ),
        .Q(\mOutPtr_reg_n_2_[1] ),
        .S(ap_rst_n_inv));
endmodule

(* CHECK_LICENSE_TYPE = "system_edge_detect_0_0,edge_detect,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "edge_detect,Vivado 2017.4" *) 
(* hls_module = "yes" *) 
(* NotValidForBitStream *)
module system_edge_detect_0_1
   (stream_in_TVALID,
    stream_in_TREADY,
    stream_in_TDATA,
    stream_in_TKEEP,
    stream_in_TSTRB,
    stream_in_TUSER,
    stream_in_TLAST,
    stream_in_TID,
    stream_in_TDEST,
    stream_out_TVALID,
    stream_out_TREADY,
    stream_out_TDATA,
    stream_out_TKEEP,
    stream_out_TSTRB,
    stream_out_TUSER,
    stream_out_TLAST,
    stream_out_TID,
    stream_out_TDEST,
    ap_clk,
    ap_rst_n);
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 stream_in TVALID" *) input stream_in_TVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 stream_in TREADY" *) output stream_in_TREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 stream_in TDATA" *) input [23:0]stream_in_TDATA;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 stream_in TKEEP" *) input [2:0]stream_in_TKEEP;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 stream_in TSTRB" *) input [2:0]stream_in_TSTRB;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 stream_in TUSER" *) input [0:0]stream_in_TUSER;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 stream_in TLAST" *) input [0:0]stream_in_TLAST;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 stream_in TID" *) input [0:0]stream_in_TID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 stream_in TDEST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME stream_in, TDATA_NUM_BYTES 3, TDEST_WIDTH 1, TID_WIDTH 1, TUSER_WIDTH 1, LAYERED_METADATA undef, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1" *) input [0:0]stream_in_TDEST;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 stream_out TVALID" *) output stream_out_TVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 stream_out TREADY" *) input stream_out_TREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 stream_out TDATA" *) output [23:0]stream_out_TDATA;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 stream_out TKEEP" *) output [2:0]stream_out_TKEEP;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 stream_out TSTRB" *) output [2:0]stream_out_TSTRB;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 stream_out TUSER" *) output [0:0]stream_out_TUSER;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 stream_out TLAST" *) output [0:0]stream_out_TLAST;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 stream_out TID" *) output [0:0]stream_out_TID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 stream_out TDEST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME stream_out, TDATA_NUM_BYTES 3, TDEST_WIDTH 1, TID_WIDTH 1, TUSER_WIDTH 1, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} TDATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 24} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TDATA_WIDTH 24 TUSER {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TUSER_WIDTH 1}, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1" *) output [0:0]stream_out_TDEST;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 ap_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF stream_in:stream_out, ASSOCIATED_RESET ap_rst_n, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1" *) input ap_clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 ap_rst_n RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {RST {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}" *) input ap_rst_n;

  wire ap_clk;
  wire ap_rst_n;
  wire [23:0]stream_in_TDATA;
  wire [0:0]stream_in_TDEST;
  wire [0:0]stream_in_TID;
  wire [2:0]stream_in_TKEEP;
  wire [0:0]stream_in_TLAST;
  wire stream_in_TREADY;
  wire [2:0]stream_in_TSTRB;
  wire [0:0]stream_in_TUSER;
  wire stream_in_TVALID;
  wire [23:0]stream_out_TDATA;
  wire [0:0]stream_out_TDEST;
  wire [0:0]stream_out_TID;
  wire [2:0]stream_out_TKEEP;
  wire [0:0]stream_out_TLAST;
  wire stream_out_TREADY;
  wire [2:0]stream_out_TSTRB;
  wire [0:0]stream_out_TUSER;
  wire stream_out_TVALID;

  system_edge_detect_0_1_edge_detect inst
       (.ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .stream_in_TDATA(stream_in_TDATA),
        .stream_in_TDEST(stream_in_TDEST),
        .stream_in_TID(stream_in_TID),
        .stream_in_TKEEP(stream_in_TKEEP),
        .stream_in_TLAST(stream_in_TLAST),
        .stream_in_TREADY(stream_in_TREADY),
        .stream_in_TSTRB(stream_in_TSTRB),
        .stream_in_TUSER(stream_in_TUSER),
        .stream_in_TVALID(stream_in_TVALID),
        .stream_out_TDATA(stream_out_TDATA),
        .stream_out_TDEST(stream_out_TDEST),
        .stream_out_TID(stream_out_TID),
        .stream_out_TKEEP(stream_out_TKEEP),
        .stream_out_TLAST(stream_out_TLAST),
        .stream_out_TREADY(stream_out_TREADY),
        .stream_out_TSTRB(stream_out_TSTRB),
        .stream_out_TUSER(stream_out_TUSER),
        .stream_out_TVALID(stream_out_TVALID));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
