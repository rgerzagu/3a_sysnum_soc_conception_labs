-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
-- Date        : Wed Mar 28 23:51:38 2018
-- Host        : DLT02-RO running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_edge_detect_0_0_sim_netlist.vhdl
-- Design      : system_edge_detect_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AXIvideo2Mat is
  port (
    ap_rst_n_inv : out STD_LOGIC;
    stream_in_TREADY : out STD_LOGIC;
    start_once_reg : out STD_LOGIC;
    \SRL_SIG_reg[1][0]\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \SRL_SIG_reg[0][7]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \SRL_SIG_reg[0][7]_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    start_for_CvtColor_U0_full_n : in STD_LOGIC;
    stream_in_TVALID : in STD_LOGIC;
    img0_data_stream_2_s_full_n : in STD_LOGIC;
    img0_data_stream_1_s_full_n : in STD_LOGIC;
    img0_data_stream_0_s_full_n : in STD_LOGIC;
    stream_in_TLAST : in STD_LOGIC_VECTOR ( 0 to 0 );
    stream_in_TUSER : in STD_LOGIC_VECTOR ( 0 to 0 );
    stream_in_TDATA : in STD_LOGIC_VECTOR ( 23 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AXIvideo2Mat;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AXIvideo2Mat is
  signal AXI_video_strm_V_data_V_0_ack_in : STD_LOGIC;
  signal AXI_video_strm_V_data_V_0_data_out : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal AXI_video_strm_V_data_V_0_load_A : STD_LOGIC;
  signal AXI_video_strm_V_data_V_0_load_B : STD_LOGIC;
  signal AXI_video_strm_V_data_V_0_payload_A : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal AXI_video_strm_V_data_V_0_payload_B : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal AXI_video_strm_V_data_V_0_sel : STD_LOGIC;
  signal AXI_video_strm_V_data_V_0_sel2 : STD_LOGIC;
  signal AXI_video_strm_V_data_V_0_sel_rd_i_1_n_2 : STD_LOGIC;
  signal AXI_video_strm_V_data_V_0_sel_wr : STD_LOGIC;
  signal AXI_video_strm_V_data_V_0_sel_wr_i_1_n_2 : STD_LOGIC;
  signal AXI_video_strm_V_data_V_0_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_data_V_0_state[0]_i_1_n_2\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_0_state_reg_n_2_[0]\ : STD_LOGIC;
  signal AXI_video_strm_V_dest_V_0_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_dest_V_0_state[0]_i_1_n_2\ : STD_LOGIC;
  signal \AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2\ : STD_LOGIC;
  signal \AXI_video_strm_V_dest_V_0_state[1]_i_4_n_2\ : STD_LOGIC;
  signal \AXI_video_strm_V_dest_V_0_state_reg_n_2_[0]\ : STD_LOGIC;
  signal AXI_video_strm_V_last_V_0_ack_in : STD_LOGIC;
  signal AXI_video_strm_V_last_V_0_data_out : STD_LOGIC;
  signal AXI_video_strm_V_last_V_0_payload_A : STD_LOGIC;
  signal \AXI_video_strm_V_last_V_0_payload_A[0]_i_1_n_2\ : STD_LOGIC;
  signal AXI_video_strm_V_last_V_0_payload_B : STD_LOGIC;
  signal \AXI_video_strm_V_last_V_0_payload_B[0]_i_1_n_2\ : STD_LOGIC;
  signal AXI_video_strm_V_last_V_0_sel : STD_LOGIC;
  signal AXI_video_strm_V_last_V_0_sel_rd_i_1_n_2 : STD_LOGIC;
  signal AXI_video_strm_V_last_V_0_sel_wr : STD_LOGIC;
  signal AXI_video_strm_V_last_V_0_sel_wr_i_1_n_2 : STD_LOGIC;
  signal AXI_video_strm_V_last_V_0_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_last_V_0_state[0]_i_1_n_2\ : STD_LOGIC;
  signal \AXI_video_strm_V_last_V_0_state_reg_n_2_[0]\ : STD_LOGIC;
  signal AXI_video_strm_V_user_V_0_ack_in : STD_LOGIC;
  signal AXI_video_strm_V_user_V_0_payload_A : STD_LOGIC;
  signal \AXI_video_strm_V_user_V_0_payload_A[0]_i_1_n_2\ : STD_LOGIC;
  signal AXI_video_strm_V_user_V_0_payload_B : STD_LOGIC;
  signal \AXI_video_strm_V_user_V_0_payload_B[0]_i_1_n_2\ : STD_LOGIC;
  signal AXI_video_strm_V_user_V_0_sel : STD_LOGIC;
  signal AXI_video_strm_V_user_V_0_sel_rd_i_1_n_2 : STD_LOGIC;
  signal AXI_video_strm_V_user_V_0_sel_wr : STD_LOGIC;
  signal AXI_video_strm_V_user_V_0_sel_wr_i_1_n_2 : STD_LOGIC;
  signal AXI_video_strm_V_user_V_0_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_user_V_0_state[0]_i_1_n_2\ : STD_LOGIC;
  signal \AXI_video_strm_V_user_V_0_state_reg_n_2_[0]\ : STD_LOGIC;
  signal \^srl_sig_reg[1][0]\ : STD_LOGIC;
  signal \ap_CS_fsm[0]_i_2_n_2\ : STD_LOGIC;
  signal \ap_CS_fsm[0]_i_3_n_2\ : STD_LOGIC;
  signal \ap_CS_fsm[0]_i_4_n_2\ : STD_LOGIC;
  signal \ap_CS_fsm[5]_i_2_n_2\ : STD_LOGIC;
  signal ap_CS_fsm_pp1_stage0 : STD_LOGIC;
  signal ap_CS_fsm_pp2_stage0 : STD_LOGIC;
  signal \ap_CS_fsm_reg_n_2_[0]\ : STD_LOGIC;
  signal ap_CS_fsm_state10 : STD_LOGIC;
  signal ap_CS_fsm_state2 : STD_LOGIC;
  signal ap_CS_fsm_state3 : STD_LOGIC;
  signal ap_CS_fsm_state4 : STD_LOGIC;
  signal ap_CS_fsm_state7 : STD_LOGIC;
  signal ap_NS_fsm : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal ap_enable_reg_pp1_iter0 : STD_LOGIC;
  signal ap_enable_reg_pp1_iter0_i_1_n_2 : STD_LOGIC;
  signal ap_enable_reg_pp1_iter0_i_3_n_2 : STD_LOGIC;
  signal ap_enable_reg_pp1_iter0_i_4_n_2 : STD_LOGIC;
  signal ap_enable_reg_pp1_iter1_i_1_n_2 : STD_LOGIC;
  signal ap_enable_reg_pp1_iter1_reg_n_2 : STD_LOGIC;
  signal ap_enable_reg_pp2_iter0 : STD_LOGIC;
  signal ap_enable_reg_pp2_iter0_i_1_n_2 : STD_LOGIC;
  signal ap_enable_reg_pp2_iter0_i_2_n_2 : STD_LOGIC;
  signal ap_enable_reg_pp2_iter1_i_1_n_2 : STD_LOGIC;
  signal ap_enable_reg_pp2_iter1_reg_n_2 : STD_LOGIC;
  signal \^ap_rst_n_inv\ : STD_LOGIC;
  signal axi_data_V1_reg_181 : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \axi_data_V1_reg_181[0]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[10]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[11]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[12]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[13]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[14]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[15]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[16]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[17]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[18]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[19]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[1]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[20]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[21]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[22]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[23]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[2]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[3]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[4]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[5]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[6]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[7]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[8]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V1_reg_181[9]_i_1_n_2\ : STD_LOGIC;
  signal axi_data_V_1_reg_236 : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \axi_data_V_1_reg_236[0]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[10]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[11]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[12]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[13]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[14]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[15]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[16]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[17]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[18]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[19]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[1]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[20]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[21]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[22]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[23]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[2]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[3]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[4]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[5]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[6]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[7]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[8]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_1_reg_236[9]_i_1_n_2\ : STD_LOGIC;
  signal axi_data_V_3_reg_295 : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \axi_data_V_3_reg_295[0]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[10]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[11]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[12]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[13]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[14]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[15]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[16]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[17]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[18]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[19]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[1]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[20]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[21]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[22]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[23]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[2]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[3]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[4]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[5]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[6]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[7]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[8]_i_1_n_2\ : STD_LOGIC;
  signal \axi_data_V_3_reg_295[9]_i_1_n_2\ : STD_LOGIC;
  signal axi_last_V1_reg_171 : STD_LOGIC;
  signal \axi_last_V1_reg_171[0]_i_1_n_2\ : STD_LOGIC;
  signal axi_last_V_3_reg_283 : STD_LOGIC;
  signal \axi_last_V_3_reg_283[0]_i_1_n_2\ : STD_LOGIC;
  signal brmerge_reg_429 : STD_LOGIC;
  signal brmerge_reg_4290 : STD_LOGIC;
  signal \brmerge_reg_429[0]_i_1_n_2\ : STD_LOGIC;
  signal \brmerge_reg_429[0]_i_2_n_2\ : STD_LOGIC;
  signal \brmerge_reg_429[0]_i_3_n_2\ : STD_LOGIC;
  signal eol_1_reg_225 : STD_LOGIC;
  signal \eol_1_reg_225[0]_i_2_n_2\ : STD_LOGIC;
  signal \eol_2_reg_272[0]_i_1_n_2\ : STD_LOGIC;
  signal \eol_2_reg_272[0]_i_2_n_2\ : STD_LOGIC;
  signal \eol_2_reg_272_reg_n_2_[0]\ : STD_LOGIC;
  signal eol_reg_213 : STD_LOGIC;
  signal \eol_reg_213[0]_i_1_n_2\ : STD_LOGIC;
  signal \eol_reg_213[0]_i_2_n_2\ : STD_LOGIC;
  signal \eol_reg_213_reg_n_2_[0]\ : STD_LOGIC;
  signal exitcond_fu_338_p2 : STD_LOGIC;
  signal exitcond_reg_4200 : STD_LOGIC;
  signal \exitcond_reg_420[0]_i_1_n_2\ : STD_LOGIC;
  signal \exitcond_reg_420_reg_n_2_[0]\ : STD_LOGIC;
  signal i_V_fu_332_p2 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal i_V_reg_415 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \i_V_reg_415[9]_i_2_n_2\ : STD_LOGIC;
  signal j_V_fu_344_p2 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal p_1_in : STD_LOGIC;
  signal sof_1_fu_128 : STD_LOGIC;
  signal sof_1_fu_1280 : STD_LOGIC;
  signal \sof_1_fu_128[0]_i_1_n_2\ : STD_LOGIC;
  signal \^start_once_reg\ : STD_LOGIC;
  signal \start_once_reg_i_1__2_n_2\ : STD_LOGIC;
  signal \^stream_in_tready\ : STD_LOGIC;
  signal t_V_3_reg_202 : STD_LOGIC;
  signal \t_V_3_reg_202[10]_i_5_n_2\ : STD_LOGIC;
  signal \t_V_3_reg_202_reg__0\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal t_V_reg_191 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal tmp_data_V_reg_391 : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal tmp_last_V_reg_399 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of AXI_video_strm_V_data_V_0_sel_rd_i_1 : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of AXI_video_strm_V_data_V_0_sel_wr_i_1 : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_data_V_0_state[0]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_data_V_0_state[1]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_dest_V_0_state[0]_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_dest_V_0_state[1]_i_2\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_dest_V_0_state[1]_i_4\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of AXI_video_strm_V_last_V_0_sel_wr_i_1 : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_last_V_0_state[0]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_last_V_0_state[1]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of AXI_video_strm_V_user_V_0_sel_rd_i_1 : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_user_V_0_state[0]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_user_V_0_state[1]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \SRL_SIG[0][0]_i_1__1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \SRL_SIG[0][0]_i_1__2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \SRL_SIG[0][1]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \SRL_SIG[0][1]_i_1__0\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \SRL_SIG[0][2]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \SRL_SIG[0][2]_i_1__0\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \SRL_SIG[0][3]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \SRL_SIG[0][3]_i_1__1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \SRL_SIG[0][4]_i_1__0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \SRL_SIG[0][4]_i_1__1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \SRL_SIG[0][5]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \SRL_SIG[0][5]_i_1__0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \SRL_SIG[0][5]_i_1__1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \SRL_SIG[0][6]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \SRL_SIG[0][6]_i_1__0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \SRL_SIG[0][7]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \SRL_SIG[0][7]_i_1__2\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \SRL_SIG[0][7]_i_2__1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \ap_CS_fsm[0]_i_1__2\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \ap_CS_fsm[0]_i_3\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \ap_CS_fsm[0]_i_4\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \ap_CS_fsm[4]_i_2\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \ap_CS_fsm[4]_i_3\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \ap_CS_fsm[5]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \ap_CS_fsm[7]_i_1\ : label is "soft_lutpair27";
  attribute FSM_ENCODING : string;
  attribute FSM_ENCODING of \ap_CS_fsm_reg[0]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[1]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[2]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[3]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[4]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[5]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[6]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[7]\ : label is "none";
  attribute SOFT_HLUTNM of ap_enable_reg_pp1_iter0_i_1 : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of ap_enable_reg_pp1_iter0_i_3 : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of ap_enable_reg_pp1_iter0_i_4 : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[0]_i_1\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[10]_i_1\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[11]_i_1\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[12]_i_1\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[13]_i_1\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[14]_i_1\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[15]_i_1\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[16]_i_1\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[17]_i_1\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[18]_i_1\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[19]_i_1\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[20]_i_1\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[21]_i_1\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[22]_i_1\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[23]_i_1\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[2]_i_1\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[3]_i_1\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[4]_i_1\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[5]_i_1\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[6]_i_1\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[7]_i_1\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[8]_i_1\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \axi_data_V1_reg_181[9]_i_1\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \axi_data_V_3_reg_295[11]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \axi_data_V_3_reg_295[15]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \axi_data_V_3_reg_295[16]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \axi_data_V_3_reg_295[1]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \axi_data_V_3_reg_295[20]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \axi_data_V_3_reg_295[2]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \axi_data_V_3_reg_295[6]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \axi_last_V1_reg_171[0]_i_1\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \brmerge_reg_429[0]_i_2\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \brmerge_reg_429[0]_i_3\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \i_V_reg_415[0]_i_1\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \i_V_reg_415[1]_i_1\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \i_V_reg_415[2]_i_1\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \i_V_reg_415[3]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \i_V_reg_415[4]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \i_V_reg_415[7]_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \i_V_reg_415[8]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \i_V_reg_415[9]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \t_V_3_reg_202[0]_i_1\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \t_V_3_reg_202[10]_i_4\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \t_V_3_reg_202[1]_i_1\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \t_V_3_reg_202[2]_i_1\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \t_V_3_reg_202[3]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \t_V_3_reg_202[4]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \t_V_3_reg_202[7]_i_1\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \t_V_3_reg_202[8]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \t_V_3_reg_202[9]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[0]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[10]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[11]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[12]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[13]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[14]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[15]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[16]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[17]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[18]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[19]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[1]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[20]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[21]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[22]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[23]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[2]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[3]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[4]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[5]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[6]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[7]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[8]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \tmp_data_V_reg_391[9]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \tmp_last_V_reg_399[0]_i_2\ : label is "soft_lutpair15";
begin
  \SRL_SIG_reg[1][0]\ <= \^srl_sig_reg[1][0]\;
  ap_rst_n_inv <= \^ap_rst_n_inv\;
  start_once_reg <= \^start_once_reg\;
  stream_in_TREADY <= \^stream_in_tready\;
\AXI_video_strm_V_data_V_0_payload_A[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_sel_wr,
      I1 => AXI_video_strm_V_data_V_0_ack_in,
      I2 => \AXI_video_strm_V_data_V_0_state_reg_n_2_[0]\,
      O => AXI_video_strm_V_data_V_0_load_A
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(0),
      Q => AXI_video_strm_V_data_V_0_payload_A(0),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(10),
      Q => AXI_video_strm_V_data_V_0_payload_A(10),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(11),
      Q => AXI_video_strm_V_data_V_0_payload_A(11),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(12),
      Q => AXI_video_strm_V_data_V_0_payload_A(12),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(13),
      Q => AXI_video_strm_V_data_V_0_payload_A(13),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(14),
      Q => AXI_video_strm_V_data_V_0_payload_A(14),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(15),
      Q => AXI_video_strm_V_data_V_0_payload_A(15),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(16),
      Q => AXI_video_strm_V_data_V_0_payload_A(16),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(17),
      Q => AXI_video_strm_V_data_V_0_payload_A(17),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(18),
      Q => AXI_video_strm_V_data_V_0_payload_A(18),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(19),
      Q => AXI_video_strm_V_data_V_0_payload_A(19),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(1),
      Q => AXI_video_strm_V_data_V_0_payload_A(1),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(20),
      Q => AXI_video_strm_V_data_V_0_payload_A(20),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(21),
      Q => AXI_video_strm_V_data_V_0_payload_A(21),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(22),
      Q => AXI_video_strm_V_data_V_0_payload_A(22),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(23),
      Q => AXI_video_strm_V_data_V_0_payload_A(23),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(2),
      Q => AXI_video_strm_V_data_V_0_payload_A(2),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(3),
      Q => AXI_video_strm_V_data_V_0_payload_A(3),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(4),
      Q => AXI_video_strm_V_data_V_0_payload_A(4),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(5),
      Q => AXI_video_strm_V_data_V_0_payload_A(5),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(6),
      Q => AXI_video_strm_V_data_V_0_payload_A(6),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(7),
      Q => AXI_video_strm_V_data_V_0_payload_A(7),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(8),
      Q => AXI_video_strm_V_data_V_0_payload_A(8),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_A_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_A,
      D => stream_in_TDATA(9),
      Q => AXI_video_strm_V_data_V_0_payload_A(9),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_sel_wr,
      I1 => AXI_video_strm_V_data_V_0_ack_in,
      I2 => \AXI_video_strm_V_data_V_0_state_reg_n_2_[0]\,
      O => AXI_video_strm_V_data_V_0_load_B
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(0),
      Q => AXI_video_strm_V_data_V_0_payload_B(0),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(10),
      Q => AXI_video_strm_V_data_V_0_payload_B(10),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(11),
      Q => AXI_video_strm_V_data_V_0_payload_B(11),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(12),
      Q => AXI_video_strm_V_data_V_0_payload_B(12),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(13),
      Q => AXI_video_strm_V_data_V_0_payload_B(13),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(14),
      Q => AXI_video_strm_V_data_V_0_payload_B(14),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(15),
      Q => AXI_video_strm_V_data_V_0_payload_B(15),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(16),
      Q => AXI_video_strm_V_data_V_0_payload_B(16),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(17),
      Q => AXI_video_strm_V_data_V_0_payload_B(17),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(18),
      Q => AXI_video_strm_V_data_V_0_payload_B(18),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(19),
      Q => AXI_video_strm_V_data_V_0_payload_B(19),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(1),
      Q => AXI_video_strm_V_data_V_0_payload_B(1),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(20),
      Q => AXI_video_strm_V_data_V_0_payload_B(20),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(21),
      Q => AXI_video_strm_V_data_V_0_payload_B(21),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(22),
      Q => AXI_video_strm_V_data_V_0_payload_B(22),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(23),
      Q => AXI_video_strm_V_data_V_0_payload_B(23),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(2),
      Q => AXI_video_strm_V_data_V_0_payload_B(2),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(3),
      Q => AXI_video_strm_V_data_V_0_payload_B(3),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(4),
      Q => AXI_video_strm_V_data_V_0_payload_B(4),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(5),
      Q => AXI_video_strm_V_data_V_0_payload_B(5),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(6),
      Q => AXI_video_strm_V_data_V_0_payload_B(6),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(7),
      Q => AXI_video_strm_V_data_V_0_payload_B(7),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(8),
      Q => AXI_video_strm_V_data_V_0_payload_B(8),
      R => '0'
    );
\AXI_video_strm_V_data_V_0_payload_B_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_load_B,
      D => stream_in_TDATA(9),
      Q => AXI_video_strm_V_data_V_0_payload_B(9),
      R => '0'
    );
AXI_video_strm_V_data_V_0_sel_rd_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2\,
      I1 => AXI_video_strm_V_data_V_0_sel,
      O => AXI_video_strm_V_data_V_0_sel_rd_i_1_n_2
    );
AXI_video_strm_V_data_V_0_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_data_V_0_sel_rd_i_1_n_2,
      Q => AXI_video_strm_V_data_V_0_sel,
      R => \^ap_rst_n_inv\
    );
AXI_video_strm_V_data_V_0_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_ack_in,
      I1 => stream_in_TVALID,
      I2 => AXI_video_strm_V_data_V_0_sel_wr,
      O => AXI_video_strm_V_data_V_0_sel_wr_i_1_n_2
    );
AXI_video_strm_V_data_V_0_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_data_V_0_sel_wr_i_1_n_2,
      Q => AXI_video_strm_V_data_V_0_sel_wr,
      R => \^ap_rst_n_inv\
    );
\AXI_video_strm_V_data_V_0_state[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FD88"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_ack_in,
      I1 => stream_in_TVALID,
      I2 => \AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2\,
      I3 => \AXI_video_strm_V_data_V_0_state_reg_n_2_[0]\,
      O => \AXI_video_strm_V_data_V_0_state[0]_i_1_n_2\
    );
\AXI_video_strm_V_data_V_0_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F77"
    )
        port map (
      I0 => \AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2\,
      I1 => \AXI_video_strm_V_data_V_0_state_reg_n_2_[0]\,
      I2 => stream_in_TVALID,
      I3 => AXI_video_strm_V_data_V_0_ack_in,
      O => AXI_video_strm_V_data_V_0_state(1)
    );
\AXI_video_strm_V_data_V_0_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_data_V_0_state[0]_i_1_n_2\,
      Q => \AXI_video_strm_V_data_V_0_state_reg_n_2_[0]\,
      R => \^ap_rst_n_inv\
    );
\AXI_video_strm_V_data_V_0_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_data_V_0_state(1),
      Q => AXI_video_strm_V_data_V_0_ack_in,
      R => \^ap_rst_n_inv\
    );
\AXI_video_strm_V_dest_V_0_state[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FD88"
    )
        port map (
      I0 => \^stream_in_tready\,
      I1 => stream_in_TVALID,
      I2 => \AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2\,
      I3 => \AXI_video_strm_V_dest_V_0_state_reg_n_2_[0]\,
      O => \AXI_video_strm_V_dest_V_0_state[0]_i_1_n_2\
    );
\AXI_video_strm_V_dest_V_0_state[1]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ap_rst_n,
      O => \^ap_rst_n_inv\
    );
\AXI_video_strm_V_dest_V_0_state[1]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F77"
    )
        port map (
      I0 => \AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2\,
      I1 => \AXI_video_strm_V_dest_V_0_state_reg_n_2_[0]\,
      I2 => stream_in_TVALID,
      I3 => \^stream_in_tready\,
      O => AXI_video_strm_V_dest_V_0_state(1)
    );
\AXI_video_strm_V_dest_V_0_state[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000BBB"
    )
        port map (
      I0 => brmerge_reg_429,
      I1 => \^srl_sig_reg[1][0]\,
      I2 => \AXI_video_strm_V_data_V_0_state_reg_n_2_[0]\,
      I3 => ap_CS_fsm_state2,
      I4 => \AXI_video_strm_V_dest_V_0_state[1]_i_4_n_2\,
      O => \AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2\
    );
\AXI_video_strm_V_dest_V_0_state[1]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_0_state_reg_n_2_[0]\,
      I1 => ap_CS_fsm_pp2_stage0,
      I2 => \eol_2_reg_272_reg_n_2_[0]\,
      I3 => ap_enable_reg_pp2_iter1_reg_n_2,
      O => \AXI_video_strm_V_dest_V_0_state[1]_i_4_n_2\
    );
\AXI_video_strm_V_dest_V_0_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_dest_V_0_state[0]_i_1_n_2\,
      Q => \AXI_video_strm_V_dest_V_0_state_reg_n_2_[0]\,
      R => \^ap_rst_n_inv\
    );
\AXI_video_strm_V_dest_V_0_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_dest_V_0_state(1),
      Q => \^stream_in_tready\,
      R => \^ap_rst_n_inv\
    );
\AXI_video_strm_V_last_V_0_payload_A[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEE2022"
    )
        port map (
      I0 => stream_in_TLAST(0),
      I1 => AXI_video_strm_V_last_V_0_sel_wr,
      I2 => AXI_video_strm_V_last_V_0_ack_in,
      I3 => \AXI_video_strm_V_last_V_0_state_reg_n_2_[0]\,
      I4 => AXI_video_strm_V_last_V_0_payload_A,
      O => \AXI_video_strm_V_last_V_0_payload_A[0]_i_1_n_2\
    );
\AXI_video_strm_V_last_V_0_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_last_V_0_payload_A[0]_i_1_n_2\,
      Q => AXI_video_strm_V_last_V_0_payload_A,
      R => '0'
    );
\AXI_video_strm_V_last_V_0_payload_B[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFBB8088"
    )
        port map (
      I0 => stream_in_TLAST(0),
      I1 => AXI_video_strm_V_last_V_0_sel_wr,
      I2 => AXI_video_strm_V_last_V_0_ack_in,
      I3 => \AXI_video_strm_V_last_V_0_state_reg_n_2_[0]\,
      I4 => AXI_video_strm_V_last_V_0_payload_B,
      O => \AXI_video_strm_V_last_V_0_payload_B[0]_i_1_n_2\
    );
\AXI_video_strm_V_last_V_0_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_last_V_0_payload_B[0]_i_1_n_2\,
      Q => AXI_video_strm_V_last_V_0_payload_B,
      R => '0'
    );
AXI_video_strm_V_last_V_0_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B4"
    )
        port map (
      I0 => \AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2\,
      I1 => \AXI_video_strm_V_last_V_0_state_reg_n_2_[0]\,
      I2 => AXI_video_strm_V_last_V_0_sel,
      O => AXI_video_strm_V_last_V_0_sel_rd_i_1_n_2
    );
AXI_video_strm_V_last_V_0_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_last_V_0_sel_rd_i_1_n_2,
      Q => AXI_video_strm_V_last_V_0_sel,
      R => \^ap_rst_n_inv\
    );
AXI_video_strm_V_last_V_0_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => AXI_video_strm_V_last_V_0_ack_in,
      I1 => stream_in_TVALID,
      I2 => AXI_video_strm_V_last_V_0_sel_wr,
      O => AXI_video_strm_V_last_V_0_sel_wr_i_1_n_2
    );
AXI_video_strm_V_last_V_0_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_last_V_0_sel_wr_i_1_n_2,
      Q => AXI_video_strm_V_last_V_0_sel_wr,
      R => \^ap_rst_n_inv\
    );
\AXI_video_strm_V_last_V_0_state[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FD88"
    )
        port map (
      I0 => AXI_video_strm_V_last_V_0_ack_in,
      I1 => stream_in_TVALID,
      I2 => \AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2\,
      I3 => \AXI_video_strm_V_last_V_0_state_reg_n_2_[0]\,
      O => \AXI_video_strm_V_last_V_0_state[0]_i_1_n_2\
    );
\AXI_video_strm_V_last_V_0_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F77"
    )
        port map (
      I0 => \AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2\,
      I1 => \AXI_video_strm_V_last_V_0_state_reg_n_2_[0]\,
      I2 => stream_in_TVALID,
      I3 => AXI_video_strm_V_last_V_0_ack_in,
      O => AXI_video_strm_V_last_V_0_state(1)
    );
\AXI_video_strm_V_last_V_0_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_last_V_0_state[0]_i_1_n_2\,
      Q => \AXI_video_strm_V_last_V_0_state_reg_n_2_[0]\,
      R => \^ap_rst_n_inv\
    );
\AXI_video_strm_V_last_V_0_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_last_V_0_state(1),
      Q => AXI_video_strm_V_last_V_0_ack_in,
      R => \^ap_rst_n_inv\
    );
\AXI_video_strm_V_user_V_0_payload_A[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEE2022"
    )
        port map (
      I0 => stream_in_TUSER(0),
      I1 => AXI_video_strm_V_user_V_0_sel_wr,
      I2 => AXI_video_strm_V_user_V_0_ack_in,
      I3 => \AXI_video_strm_V_user_V_0_state_reg_n_2_[0]\,
      I4 => AXI_video_strm_V_user_V_0_payload_A,
      O => \AXI_video_strm_V_user_V_0_payload_A[0]_i_1_n_2\
    );
\AXI_video_strm_V_user_V_0_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_user_V_0_payload_A[0]_i_1_n_2\,
      Q => AXI_video_strm_V_user_V_0_payload_A,
      R => '0'
    );
\AXI_video_strm_V_user_V_0_payload_B[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFBB8088"
    )
        port map (
      I0 => stream_in_TUSER(0),
      I1 => AXI_video_strm_V_user_V_0_sel_wr,
      I2 => AXI_video_strm_V_user_V_0_ack_in,
      I3 => \AXI_video_strm_V_user_V_0_state_reg_n_2_[0]\,
      I4 => AXI_video_strm_V_user_V_0_payload_B,
      O => \AXI_video_strm_V_user_V_0_payload_B[0]_i_1_n_2\
    );
\AXI_video_strm_V_user_V_0_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_user_V_0_payload_B[0]_i_1_n_2\,
      Q => AXI_video_strm_V_user_V_0_payload_B,
      R => '0'
    );
AXI_video_strm_V_user_V_0_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B4"
    )
        port map (
      I0 => \AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2\,
      I1 => \AXI_video_strm_V_user_V_0_state_reg_n_2_[0]\,
      I2 => AXI_video_strm_V_user_V_0_sel,
      O => AXI_video_strm_V_user_V_0_sel_rd_i_1_n_2
    );
AXI_video_strm_V_user_V_0_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_user_V_0_sel_rd_i_1_n_2,
      Q => AXI_video_strm_V_user_V_0_sel,
      R => \^ap_rst_n_inv\
    );
AXI_video_strm_V_user_V_0_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => AXI_video_strm_V_user_V_0_ack_in,
      I1 => stream_in_TVALID,
      I2 => AXI_video_strm_V_user_V_0_sel_wr,
      O => AXI_video_strm_V_user_V_0_sel_wr_i_1_n_2
    );
AXI_video_strm_V_user_V_0_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_user_V_0_sel_wr_i_1_n_2,
      Q => AXI_video_strm_V_user_V_0_sel_wr,
      R => \^ap_rst_n_inv\
    );
\AXI_video_strm_V_user_V_0_state[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FD88"
    )
        port map (
      I0 => AXI_video_strm_V_user_V_0_ack_in,
      I1 => stream_in_TVALID,
      I2 => \AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2\,
      I3 => \AXI_video_strm_V_user_V_0_state_reg_n_2_[0]\,
      O => \AXI_video_strm_V_user_V_0_state[0]_i_1_n_2\
    );
\AXI_video_strm_V_user_V_0_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F77"
    )
        port map (
      I0 => \AXI_video_strm_V_dest_V_0_state[1]_i_3_n_2\,
      I1 => \AXI_video_strm_V_user_V_0_state_reg_n_2_[0]\,
      I2 => stream_in_TVALID,
      I3 => AXI_video_strm_V_user_V_0_ack_in,
      O => AXI_video_strm_V_user_V_0_state(1)
    );
\AXI_video_strm_V_user_V_0_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_user_V_0_state[0]_i_1_n_2\,
      Q => \AXI_video_strm_V_user_V_0_state_reg_n_2_[0]\,
      R => \^ap_rst_n_inv\
    );
\AXI_video_strm_V_user_V_0_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_user_V_0_state(1),
      Q => AXI_video_strm_V_user_V_0_ack_in,
      R => \^ap_rst_n_inv\
    );
\SRL_SIG[0][0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(16),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(16),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(16),
      O => D(0)
    );
\SRL_SIG[0][0]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(8),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(8),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(8),
      O => \SRL_SIG_reg[0][7]\(0)
    );
\SRL_SIG[0][0]_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(0),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(0),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(0),
      O => \SRL_SIG_reg[0][7]_0\(0)
    );
\SRL_SIG[0][1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(17),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(17),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(17),
      O => D(1)
    );
\SRL_SIG[0][1]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(9),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(9),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(9),
      O => \SRL_SIG_reg[0][7]\(1)
    );
\SRL_SIG[0][1]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(1),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(1),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(1),
      O => \SRL_SIG_reg[0][7]_0\(1)
    );
\SRL_SIG[0][2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(18),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(18),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(18),
      O => D(2)
    );
\SRL_SIG[0][2]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(10),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(10),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(10),
      O => \SRL_SIG_reg[0][7]\(2)
    );
\SRL_SIG[0][2]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(2),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(2),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(2),
      O => \SRL_SIG_reg[0][7]_0\(2)
    );
\SRL_SIG[0][3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(19),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(19),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(19),
      O => D(3)
    );
\SRL_SIG[0][3]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(11),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(11),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(11),
      O => \SRL_SIG_reg[0][7]\(3)
    );
\SRL_SIG[0][3]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(3),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(3),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(3),
      O => \SRL_SIG_reg[0][7]_0\(3)
    );
\SRL_SIG[0][4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(20),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(20),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(20),
      O => D(4)
    );
\SRL_SIG[0][4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(12),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(12),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(12),
      O => \SRL_SIG_reg[0][7]\(4)
    );
\SRL_SIG[0][4]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(4),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(4),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(4),
      O => \SRL_SIG_reg[0][7]_0\(4)
    );
\SRL_SIG[0][5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(21),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(21),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(21),
      O => D(5)
    );
\SRL_SIG[0][5]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(13),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(13),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(13),
      O => \SRL_SIG_reg[0][7]\(5)
    );
\SRL_SIG[0][5]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(5),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(5),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(5),
      O => \SRL_SIG_reg[0][7]_0\(5)
    );
\SRL_SIG[0][6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(22),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(22),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(22),
      O => D(6)
    );
\SRL_SIG[0][6]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(14),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(14),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(14),
      O => \SRL_SIG_reg[0][7]\(6)
    );
\SRL_SIG[0][6]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(6),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(6),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(6),
      O => \SRL_SIG_reg[0][7]_0\(6)
    );
\SRL_SIG[0][7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(23),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(23),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(23),
      O => D(7)
    );
\SRL_SIG[0][7]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(15),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(15),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(15),
      O => \SRL_SIG_reg[0][7]\(7)
    );
\SRL_SIG[0][7]_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2000"
    )
        port map (
      I0 => \ap_CS_fsm[5]_i_2_n_2\,
      I1 => \exitcond_reg_420_reg_n_2_[0]\,
      I2 => ap_enable_reg_pp1_iter1_reg_n_2,
      I3 => ap_CS_fsm_pp1_stage0,
      O => \^srl_sig_reg[1][0]\
    );
\SRL_SIG[0][7]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(7),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_payload_B(7),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(7),
      O => \SRL_SIG_reg[0][7]_0\(7)
    );
\ap_CS_fsm[0]_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"222F2222"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => \ap_CS_fsm[0]_i_2_n_2\,
      I2 => \^start_once_reg\,
      I3 => start_for_CvtColor_U0_full_n,
      I4 => \ap_CS_fsm_reg_n_2_[0]\,
      O => ap_NS_fsm(0)
    );
\ap_CS_fsm[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => t_V_reg_191(8),
      I1 => t_V_reg_191(3),
      I2 => \ap_CS_fsm[0]_i_3_n_2\,
      I3 => \ap_CS_fsm[0]_i_4_n_2\,
      O => \ap_CS_fsm[0]_i_2_n_2\
    );
\ap_CS_fsm[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => t_V_reg_191(1),
      I1 => t_V_reg_191(0),
      I2 => t_V_reg_191(5),
      I3 => t_V_reg_191(2),
      O => \ap_CS_fsm[0]_i_3_n_2\
    );
\ap_CS_fsm[0]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => t_V_reg_191(9),
      I1 => t_V_reg_191(4),
      I2 => t_V_reg_191(7),
      I3 => t_V_reg_191(6),
      O => \ap_CS_fsm[0]_i_4_n_2\
    );
\ap_CS_fsm[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF44444"
    )
        port map (
      I0 => ap_NS_fsm(2),
      I1 => ap_CS_fsm_state2,
      I2 => \^start_once_reg\,
      I3 => start_for_CvtColor_U0_full_n,
      I4 => \ap_CS_fsm_reg_n_2_[0]\,
      O => ap_NS_fsm(1)
    );
\ap_CS_fsm[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88800080"
    )
        port map (
      I0 => ap_CS_fsm_state2,
      I1 => \AXI_video_strm_V_data_V_0_state_reg_n_2_[0]\,
      I2 => AXI_video_strm_V_user_V_0_payload_A,
      I3 => AXI_video_strm_V_user_V_0_sel,
      I4 => AXI_video_strm_V_user_V_0_payload_B,
      O => ap_NS_fsm(2)
    );
\ap_CS_fsm[3]_i_1__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => ap_CS_fsm_state3,
      I1 => ap_CS_fsm_state10,
      O => ap_NS_fsm(3)
    );
\ap_CS_fsm[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBFAAAA"
    )
        port map (
      I0 => p_1_in,
      I1 => exitcond_reg_4200,
      I2 => ap_enable_reg_pp1_iter1_reg_n_2,
      I3 => ap_enable_reg_pp1_iter0,
      I4 => ap_CS_fsm_pp1_stage0,
      O => ap_NS_fsm(4)
    );
\ap_CS_fsm[4]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => \ap_CS_fsm[0]_i_2_n_2\,
      O => p_1_in
    );
\ap_CS_fsm[4]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AA8A"
    )
        port map (
      I0 => ap_CS_fsm_pp1_stage0,
      I1 => \exitcond_reg_420_reg_n_2_[0]\,
      I2 => ap_enable_reg_pp1_iter1_reg_n_2,
      I3 => \ap_CS_fsm[5]_i_2_n_2\,
      O => exitcond_reg_4200
    );
\ap_CS_fsm[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000E000"
    )
        port map (
      I0 => \ap_CS_fsm[5]_i_2_n_2\,
      I1 => \exitcond_reg_420_reg_n_2_[0]\,
      I2 => ap_CS_fsm_pp1_stage0,
      I3 => ap_enable_reg_pp1_iter1_reg_n_2,
      I4 => ap_enable_reg_pp1_iter0,
      O => ap_NS_fsm(5)
    );
\ap_CS_fsm[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80808000"
    )
        port map (
      I0 => img0_data_stream_0_s_full_n,
      I1 => img0_data_stream_1_s_full_n,
      I2 => img0_data_stream_2_s_full_n,
      I3 => brmerge_reg_429,
      I4 => \AXI_video_strm_V_data_V_0_state_reg_n_2_[0]\,
      O => \ap_CS_fsm[5]_i_2_n_2\
    );
\ap_CS_fsm[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFAAFFAAABAAFFAA"
    )
        port map (
      I0 => ap_CS_fsm_state7,
      I1 => \AXI_video_strm_V_data_V_0_state_reg_n_2_[0]\,
      I2 => \eol_2_reg_272_reg_n_2_[0]\,
      I3 => ap_CS_fsm_pp2_stage0,
      I4 => ap_enable_reg_pp2_iter1_reg_n_2,
      I5 => ap_enable_reg_pp2_iter0,
      O => ap_NS_fsm(6)
    );
\ap_CS_fsm[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000E000"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_0_state_reg_n_2_[0]\,
      I1 => \eol_2_reg_272_reg_n_2_[0]\,
      I2 => ap_CS_fsm_pp2_stage0,
      I3 => ap_enable_reg_pp2_iter1_reg_n_2,
      I4 => ap_enable_reg_pp2_iter0,
      O => ap_NS_fsm(7)
    );
\ap_CS_fsm_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(0),
      Q => \ap_CS_fsm_reg_n_2_[0]\,
      S => \^ap_rst_n_inv\
    );
\ap_CS_fsm_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(1),
      Q => ap_CS_fsm_state2,
      R => \^ap_rst_n_inv\
    );
\ap_CS_fsm_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(2),
      Q => ap_CS_fsm_state3,
      R => \^ap_rst_n_inv\
    );
\ap_CS_fsm_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(3),
      Q => ap_CS_fsm_state4,
      R => \^ap_rst_n_inv\
    );
\ap_CS_fsm_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(4),
      Q => ap_CS_fsm_pp1_stage0,
      R => \^ap_rst_n_inv\
    );
\ap_CS_fsm_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(5),
      Q => ap_CS_fsm_state7,
      R => \^ap_rst_n_inv\
    );
\ap_CS_fsm_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(6),
      Q => ap_CS_fsm_pp2_stage0,
      R => \^ap_rst_n_inv\
    );
\ap_CS_fsm_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(7),
      Q => ap_CS_fsm_state10,
      R => \^ap_rst_n_inv\
    );
ap_enable_reg_pp1_iter0_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"70707000"
    )
        port map (
      I0 => exitcond_fu_338_p2,
      I1 => exitcond_reg_4200,
      I2 => ap_rst_n,
      I3 => p_1_in,
      I4 => ap_enable_reg_pp1_iter0,
      O => ap_enable_reg_pp1_iter0_i_1_n_2
    );
ap_enable_reg_pp1_iter0_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04000000"
    )
        port map (
      I0 => \t_V_3_reg_202_reg__0\(9),
      I1 => \t_V_3_reg_202_reg__0\(8),
      I2 => \t_V_3_reg_202_reg__0\(2),
      I3 => ap_enable_reg_pp1_iter0_i_3_n_2,
      I4 => ap_enable_reg_pp1_iter0_i_4_n_2,
      O => exitcond_fu_338_p2
    );
ap_enable_reg_pp1_iter0_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => \t_V_3_reg_202_reg__0\(10),
      I1 => \t_V_3_reg_202_reg__0\(3),
      I2 => \t_V_3_reg_202_reg__0\(4),
      I3 => \t_V_3_reg_202_reg__0\(1),
      O => ap_enable_reg_pp1_iter0_i_3_n_2
    );
ap_enable_reg_pp1_iter0_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \t_V_3_reg_202_reg__0\(7),
      I1 => \t_V_3_reg_202_reg__0\(6),
      I2 => \t_V_3_reg_202_reg__0\(5),
      I3 => \t_V_3_reg_202_reg__0\(0),
      O => ap_enable_reg_pp1_iter0_i_4_n_2
    );
ap_enable_reg_pp1_iter0_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp1_iter0_i_1_n_2,
      Q => ap_enable_reg_pp1_iter0,
      R => '0'
    );
ap_enable_reg_pp1_iter1_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCCC4CC00000400"
    )
        port map (
      I0 => p_1_in,
      I1 => ap_rst_n,
      I2 => \ap_CS_fsm[5]_i_2_n_2\,
      I3 => ap_enable_reg_pp1_iter1_reg_n_2,
      I4 => \exitcond_reg_420_reg_n_2_[0]\,
      I5 => ap_enable_reg_pp1_iter0,
      O => ap_enable_reg_pp1_iter1_i_1_n_2
    );
ap_enable_reg_pp1_iter1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp1_iter1_i_1_n_2,
      Q => ap_enable_reg_pp1_iter1_reg_n_2,
      R => '0'
    );
ap_enable_reg_pp2_iter0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000057777777"
    )
        port map (
      I0 => ap_CS_fsm_pp2_stage0,
      I1 => \eol_2_reg_272_reg_n_2_[0]\,
      I2 => ap_enable_reg_pp2_iter1_reg_n_2,
      I3 => \AXI_video_strm_V_data_V_0_state_reg_n_2_[0]\,
      I4 => AXI_video_strm_V_last_V_0_data_out,
      I5 => ap_enable_reg_pp2_iter0_i_2_n_2,
      O => ap_enable_reg_pp2_iter0_i_1_n_2
    );
ap_enable_reg_pp2_iter0_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1F"
    )
        port map (
      I0 => ap_enable_reg_pp2_iter0,
      I1 => ap_CS_fsm_state7,
      I2 => ap_rst_n,
      O => ap_enable_reg_pp2_iter0_i_2_n_2
    );
ap_enable_reg_pp2_iter0_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp2_iter0_i_1_n_2,
      Q => ap_enable_reg_pp2_iter0,
      R => '0'
    );
ap_enable_reg_pp2_iter1_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCCC4CC00000400"
    )
        port map (
      I0 => ap_CS_fsm_state7,
      I1 => ap_rst_n,
      I2 => \AXI_video_strm_V_data_V_0_state_reg_n_2_[0]\,
      I3 => ap_enable_reg_pp2_iter1_reg_n_2,
      I4 => \eol_2_reg_272_reg_n_2_[0]\,
      I5 => ap_enable_reg_pp2_iter0,
      O => ap_enable_reg_pp2_iter1_i_1_n_2
    );
ap_enable_reg_pp2_iter1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp2_iter1_i_1_n_2,
      Q => ap_enable_reg_pp2_iter1_reg_n_2,
      R => '0'
    );
\axi_data_V1_reg_181[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(0),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(0),
      O => \axi_data_V1_reg_181[0]_i_1_n_2\
    );
\axi_data_V1_reg_181[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(10),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(10),
      O => \axi_data_V1_reg_181[10]_i_1_n_2\
    );
\axi_data_V1_reg_181[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(11),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(11),
      O => \axi_data_V1_reg_181[11]_i_1_n_2\
    );
\axi_data_V1_reg_181[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(12),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(12),
      O => \axi_data_V1_reg_181[12]_i_1_n_2\
    );
\axi_data_V1_reg_181[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(13),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(13),
      O => \axi_data_V1_reg_181[13]_i_1_n_2\
    );
\axi_data_V1_reg_181[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(14),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(14),
      O => \axi_data_V1_reg_181[14]_i_1_n_2\
    );
\axi_data_V1_reg_181[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(15),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(15),
      O => \axi_data_V1_reg_181[15]_i_1_n_2\
    );
\axi_data_V1_reg_181[16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(16),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(16),
      O => \axi_data_V1_reg_181[16]_i_1_n_2\
    );
\axi_data_V1_reg_181[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(17),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(17),
      O => \axi_data_V1_reg_181[17]_i_1_n_2\
    );
\axi_data_V1_reg_181[18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(18),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(18),
      O => \axi_data_V1_reg_181[18]_i_1_n_2\
    );
\axi_data_V1_reg_181[19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(19),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(19),
      O => \axi_data_V1_reg_181[19]_i_1_n_2\
    );
\axi_data_V1_reg_181[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(1),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(1),
      O => \axi_data_V1_reg_181[1]_i_1_n_2\
    );
\axi_data_V1_reg_181[20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(20),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(20),
      O => \axi_data_V1_reg_181[20]_i_1_n_2\
    );
\axi_data_V1_reg_181[21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(21),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(21),
      O => \axi_data_V1_reg_181[21]_i_1_n_2\
    );
\axi_data_V1_reg_181[22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(22),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(22),
      O => \axi_data_V1_reg_181[22]_i_1_n_2\
    );
\axi_data_V1_reg_181[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(23),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(23),
      O => \axi_data_V1_reg_181[23]_i_1_n_2\
    );
\axi_data_V1_reg_181[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(2),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(2),
      O => \axi_data_V1_reg_181[2]_i_1_n_2\
    );
\axi_data_V1_reg_181[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(3),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(3),
      O => \axi_data_V1_reg_181[3]_i_1_n_2\
    );
\axi_data_V1_reg_181[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(4),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(4),
      O => \axi_data_V1_reg_181[4]_i_1_n_2\
    );
\axi_data_V1_reg_181[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(5),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(5),
      O => \axi_data_V1_reg_181[5]_i_1_n_2\
    );
\axi_data_V1_reg_181[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(6),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(6),
      O => \axi_data_V1_reg_181[6]_i_1_n_2\
    );
\axi_data_V1_reg_181[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(7),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(7),
      O => \axi_data_V1_reg_181[7]_i_1_n_2\
    );
\axi_data_V1_reg_181[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(8),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(8),
      O => \axi_data_V1_reg_181[8]_i_1_n_2\
    );
\axi_data_V1_reg_181[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_data_V_reg_391(9),
      I1 => ap_CS_fsm_state3,
      I2 => axi_data_V_3_reg_295(9),
      O => \axi_data_V1_reg_181[9]_i_1_n_2\
    );
\axi_data_V1_reg_181_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[0]_i_1_n_2\,
      Q => axi_data_V1_reg_181(0),
      R => '0'
    );
\axi_data_V1_reg_181_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[10]_i_1_n_2\,
      Q => axi_data_V1_reg_181(10),
      R => '0'
    );
\axi_data_V1_reg_181_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[11]_i_1_n_2\,
      Q => axi_data_V1_reg_181(11),
      R => '0'
    );
\axi_data_V1_reg_181_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[12]_i_1_n_2\,
      Q => axi_data_V1_reg_181(12),
      R => '0'
    );
\axi_data_V1_reg_181_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[13]_i_1_n_2\,
      Q => axi_data_V1_reg_181(13),
      R => '0'
    );
\axi_data_V1_reg_181_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[14]_i_1_n_2\,
      Q => axi_data_V1_reg_181(14),
      R => '0'
    );
\axi_data_V1_reg_181_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[15]_i_1_n_2\,
      Q => axi_data_V1_reg_181(15),
      R => '0'
    );
\axi_data_V1_reg_181_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[16]_i_1_n_2\,
      Q => axi_data_V1_reg_181(16),
      R => '0'
    );
\axi_data_V1_reg_181_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[17]_i_1_n_2\,
      Q => axi_data_V1_reg_181(17),
      R => '0'
    );
\axi_data_V1_reg_181_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[18]_i_1_n_2\,
      Q => axi_data_V1_reg_181(18),
      R => '0'
    );
\axi_data_V1_reg_181_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[19]_i_1_n_2\,
      Q => axi_data_V1_reg_181(19),
      R => '0'
    );
\axi_data_V1_reg_181_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[1]_i_1_n_2\,
      Q => axi_data_V1_reg_181(1),
      R => '0'
    );
\axi_data_V1_reg_181_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[20]_i_1_n_2\,
      Q => axi_data_V1_reg_181(20),
      R => '0'
    );
\axi_data_V1_reg_181_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[21]_i_1_n_2\,
      Q => axi_data_V1_reg_181(21),
      R => '0'
    );
\axi_data_V1_reg_181_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[22]_i_1_n_2\,
      Q => axi_data_V1_reg_181(22),
      R => '0'
    );
\axi_data_V1_reg_181_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[23]_i_1_n_2\,
      Q => axi_data_V1_reg_181(23),
      R => '0'
    );
\axi_data_V1_reg_181_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[2]_i_1_n_2\,
      Q => axi_data_V1_reg_181(2),
      R => '0'
    );
\axi_data_V1_reg_181_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[3]_i_1_n_2\,
      Q => axi_data_V1_reg_181(3),
      R => '0'
    );
\axi_data_V1_reg_181_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[4]_i_1_n_2\,
      Q => axi_data_V1_reg_181(4),
      R => '0'
    );
\axi_data_V1_reg_181_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[5]_i_1_n_2\,
      Q => axi_data_V1_reg_181(5),
      R => '0'
    );
\axi_data_V1_reg_181_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[6]_i_1_n_2\,
      Q => axi_data_V1_reg_181(6),
      R => '0'
    );
\axi_data_V1_reg_181_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[7]_i_1_n_2\,
      Q => axi_data_V1_reg_181(7),
      R => '0'
    );
\axi_data_V1_reg_181_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[8]_i_1_n_2\,
      Q => axi_data_V1_reg_181(8),
      R => '0'
    );
\axi_data_V1_reg_181_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_data_V1_reg_181[9]_i_1_n_2\,
      Q => axi_data_V1_reg_181(9),
      R => '0'
    );
\axi_data_V_1_reg_236[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(0),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(0),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(0),
      O => \axi_data_V_1_reg_236[0]_i_1_n_2\
    );
\axi_data_V_1_reg_236[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(10),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(10),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(10),
      O => \axi_data_V_1_reg_236[10]_i_1_n_2\
    );
\axi_data_V_1_reg_236[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(11),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(11),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(11),
      O => \axi_data_V_1_reg_236[11]_i_1_n_2\
    );
\axi_data_V_1_reg_236[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(12),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(12),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(12),
      O => \axi_data_V_1_reg_236[12]_i_1_n_2\
    );
\axi_data_V_1_reg_236[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(13),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(13),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(13),
      O => \axi_data_V_1_reg_236[13]_i_1_n_2\
    );
\axi_data_V_1_reg_236[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(14),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(14),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(14),
      O => \axi_data_V_1_reg_236[14]_i_1_n_2\
    );
\axi_data_V_1_reg_236[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(15),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(15),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(15),
      O => \axi_data_V_1_reg_236[15]_i_1_n_2\
    );
\axi_data_V_1_reg_236[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(16),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(16),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(16),
      O => \axi_data_V_1_reg_236[16]_i_1_n_2\
    );
\axi_data_V_1_reg_236[17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(17),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(17),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(17),
      O => \axi_data_V_1_reg_236[17]_i_1_n_2\
    );
\axi_data_V_1_reg_236[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(18),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(18),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(18),
      O => \axi_data_V_1_reg_236[18]_i_1_n_2\
    );
\axi_data_V_1_reg_236[19]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(19),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(19),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(19),
      O => \axi_data_V_1_reg_236[19]_i_1_n_2\
    );
\axi_data_V_1_reg_236[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(1),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(1),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(1),
      O => \axi_data_V_1_reg_236[1]_i_1_n_2\
    );
\axi_data_V_1_reg_236[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(20),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(20),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(20),
      O => \axi_data_V_1_reg_236[20]_i_1_n_2\
    );
\axi_data_V_1_reg_236[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(21),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(21),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(21),
      O => \axi_data_V_1_reg_236[21]_i_1_n_2\
    );
\axi_data_V_1_reg_236[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(22),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(22),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(22),
      O => \axi_data_V_1_reg_236[22]_i_1_n_2\
    );
\axi_data_V_1_reg_236[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(23),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(23),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(23),
      O => \axi_data_V_1_reg_236[23]_i_1_n_2\
    );
\axi_data_V_1_reg_236[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(2),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(2),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(2),
      O => \axi_data_V_1_reg_236[2]_i_1_n_2\
    );
\axi_data_V_1_reg_236[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(3),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(3),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(3),
      O => \axi_data_V_1_reg_236[3]_i_1_n_2\
    );
\axi_data_V_1_reg_236[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(4),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(4),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(4),
      O => \axi_data_V_1_reg_236[4]_i_1_n_2\
    );
\axi_data_V_1_reg_236[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(5),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(5),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(5),
      O => \axi_data_V_1_reg_236[5]_i_1_n_2\
    );
\axi_data_V_1_reg_236[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(6),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(6),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(6),
      O => \axi_data_V_1_reg_236[6]_i_1_n_2\
    );
\axi_data_V_1_reg_236[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(7),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(7),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(7),
      O => \axi_data_V_1_reg_236[7]_i_1_n_2\
    );
\axi_data_V_1_reg_236[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(8),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(8),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(8),
      O => \axi_data_V_1_reg_236[8]_i_1_n_2\
    );
\axi_data_V_1_reg_236[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => axi_data_V_1_reg_236(9),
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_data_V_0_data_out(9),
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_data_V1_reg_181(9),
      O => \axi_data_V_1_reg_236[9]_i_1_n_2\
    );
\axi_data_V_1_reg_236_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[0]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(0),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[10]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(10),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[11]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(11),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[12]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(12),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[13]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(13),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[14]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(14),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[15]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(15),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[16]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(16),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[17]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(17),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[18]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(18),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[19]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(19),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[1]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(1),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[20]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(20),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[21]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(21),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[22]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(22),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[23]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(23),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[2]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(2),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[3]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(3),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[4]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(4),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[5]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(5),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[6]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(6),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[7]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(7),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[8]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(8),
      R => '0'
    );
\axi_data_V_1_reg_236_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \axi_data_V_1_reg_236[9]_i_1_n_2\,
      Q => axi_data_V_1_reg_236(9),
      R => '0'
    );
\axi_data_V_3_reg_295[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(0),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(0),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(0),
      O => \axi_data_V_3_reg_295[0]_i_1_n_2\
    );
\axi_data_V_3_reg_295[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(10),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(10),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(10),
      O => \axi_data_V_3_reg_295[10]_i_1_n_2\
    );
\axi_data_V_3_reg_295[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(11),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(11),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(11),
      O => \axi_data_V_3_reg_295[11]_i_1_n_2\
    );
\axi_data_V_3_reg_295[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(12),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(12),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(12),
      O => \axi_data_V_3_reg_295[12]_i_1_n_2\
    );
\axi_data_V_3_reg_295[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(13),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(13),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(13),
      O => \axi_data_V_3_reg_295[13]_i_1_n_2\
    );
\axi_data_V_3_reg_295[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(14),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(14),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(14),
      O => \axi_data_V_3_reg_295[14]_i_1_n_2\
    );
\axi_data_V_3_reg_295[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(15),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(15),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(15),
      O => \axi_data_V_3_reg_295[15]_i_1_n_2\
    );
\axi_data_V_3_reg_295[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(16),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(16),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(16),
      O => \axi_data_V_3_reg_295[16]_i_1_n_2\
    );
\axi_data_V_3_reg_295[17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(17),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(17),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(17),
      O => \axi_data_V_3_reg_295[17]_i_1_n_2\
    );
\axi_data_V_3_reg_295[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(18),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(18),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(18),
      O => \axi_data_V_3_reg_295[18]_i_1_n_2\
    );
\axi_data_V_3_reg_295[19]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(19),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(19),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(19),
      O => \axi_data_V_3_reg_295[19]_i_1_n_2\
    );
\axi_data_V_3_reg_295[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(1),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(1),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(1),
      O => \axi_data_V_3_reg_295[1]_i_1_n_2\
    );
\axi_data_V_3_reg_295[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(20),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(20),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(20),
      O => \axi_data_V_3_reg_295[20]_i_1_n_2\
    );
\axi_data_V_3_reg_295[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(21),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(21),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(21),
      O => \axi_data_V_3_reg_295[21]_i_1_n_2\
    );
\axi_data_V_3_reg_295[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(22),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(22),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(22),
      O => \axi_data_V_3_reg_295[22]_i_1_n_2\
    );
\axi_data_V_3_reg_295[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(23),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(23),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(23),
      O => \axi_data_V_3_reg_295[23]_i_1_n_2\
    );
\axi_data_V_3_reg_295[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(2),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(2),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(2),
      O => \axi_data_V_3_reg_295[2]_i_1_n_2\
    );
\axi_data_V_3_reg_295[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(3),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(3),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(3),
      O => \axi_data_V_3_reg_295[3]_i_1_n_2\
    );
\axi_data_V_3_reg_295[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(4),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(4),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(4),
      O => \axi_data_V_3_reg_295[4]_i_1_n_2\
    );
\axi_data_V_3_reg_295[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(5),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(5),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(5),
      O => \axi_data_V_3_reg_295[5]_i_1_n_2\
    );
\axi_data_V_3_reg_295[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(6),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(6),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(6),
      O => \axi_data_V_3_reg_295[6]_i_1_n_2\
    );
\axi_data_V_3_reg_295[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(7),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(7),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(7),
      O => \axi_data_V_3_reg_295[7]_i_1_n_2\
    );
\axi_data_V_3_reg_295[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(8),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(8),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(8),
      O => \axi_data_V_3_reg_295[8]_i_1_n_2\
    );
\axi_data_V_3_reg_295[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => axi_data_V_1_reg_236(9),
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_data_V_0_payload_B(9),
      I3 => AXI_video_strm_V_data_V_0_sel,
      I4 => AXI_video_strm_V_data_V_0_payload_A(9),
      O => \axi_data_V_3_reg_295[9]_i_1_n_2\
    );
\axi_data_V_3_reg_295_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[0]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(0),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[10]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(10),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[11]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(11),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[12]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(12),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[13]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(13),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[14]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(14),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[15]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(15),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[16]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(16),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[17]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(17),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[18]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(18),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[19]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(19),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[1]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(1),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[20]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(20),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[21]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(21),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[22]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(22),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[23]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(23),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[2]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(2),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[3]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(3),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[4]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(4),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[5]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(5),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[6]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(6),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[7]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(7),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[8]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(8),
      R => '0'
    );
\axi_data_V_3_reg_295_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_data_V_3_reg_295[9]_i_1_n_2\,
      Q => axi_data_V_3_reg_295(9),
      R => '0'
    );
\axi_last_V1_reg_171[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => tmp_last_V_reg_399,
      I1 => ap_CS_fsm_state3,
      I2 => axi_last_V_3_reg_283,
      O => \axi_last_V1_reg_171[0]_i_1_n_2\
    );
\axi_last_V1_reg_171_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \axi_last_V1_reg_171[0]_i_1_n_2\,
      Q => axi_last_V1_reg_171,
      R => '0'
    );
\axi_last_V_3_reg_283[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => eol_1_reg_225,
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_last_V_0_payload_B,
      I3 => AXI_video_strm_V_last_V_0_sel,
      I4 => AXI_video_strm_V_last_V_0_payload_A,
      O => \axi_last_V_3_reg_283[0]_i_1_n_2\
    );
\axi_last_V_3_reg_283_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \axi_last_V_3_reg_283[0]_i_1_n_2\,
      Q => axi_last_V_3_reg_283,
      R => '0'
    );
\brmerge_reg_429[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFACFFFFFFAC0000"
    )
        port map (
      I0 => \brmerge_reg_429[0]_i_2_n_2\,
      I1 => \eol_reg_213_reg_n_2_[0]\,
      I2 => \brmerge_reg_429[0]_i_3_n_2\,
      I3 => sof_1_fu_128,
      I4 => brmerge_reg_4290,
      I5 => brmerge_reg_429,
      O => \brmerge_reg_429[0]_i_1_n_2\
    );
\brmerge_reg_429[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => eol_1_reg_225,
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_last_V_0_payload_B,
      I3 => AXI_video_strm_V_last_V_0_sel,
      I4 => AXI_video_strm_V_last_V_0_payload_A,
      O => \brmerge_reg_429[0]_i_2_n_2\
    );
\brmerge_reg_429[0]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => ap_CS_fsm_pp1_stage0,
      I1 => ap_enable_reg_pp1_iter1_reg_n_2,
      I2 => \exitcond_reg_420_reg_n_2_[0]\,
      O => \brmerge_reg_429[0]_i_3_n_2\
    );
\brmerge_reg_429_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \brmerge_reg_429[0]_i_1_n_2\,
      Q => brmerge_reg_429,
      R => '0'
    );
\eol_1_reg_225[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => p_1_in,
      I1 => \^srl_sig_reg[1][0]\,
      O => eol_reg_213
    );
\eol_1_reg_225[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => eol_1_reg_225,
      I1 => brmerge_reg_429,
      I2 => AXI_video_strm_V_last_V_0_data_out,
      I3 => \^srl_sig_reg[1][0]\,
      I4 => axi_last_V1_reg_171,
      O => \eol_1_reg_225[0]_i_2_n_2\
    );
\eol_1_reg_225_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \eol_1_reg_225[0]_i_2_n_2\,
      Q => eol_1_reg_225,
      R => '0'
    );
\eol_2_reg_272[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AEAAAAAA"
    )
        port map (
      I0 => ap_CS_fsm_state7,
      I1 => ap_enable_reg_pp2_iter1_reg_n_2,
      I2 => \eol_2_reg_272_reg_n_2_[0]\,
      I3 => ap_CS_fsm_pp2_stage0,
      I4 => \AXI_video_strm_V_data_V_0_state_reg_n_2_[0]\,
      O => \eol_2_reg_272[0]_i_1_n_2\
    );
\eol_2_reg_272[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \eol_reg_213_reg_n_2_[0]\,
      I1 => ap_CS_fsm_state7,
      I2 => AXI_video_strm_V_last_V_0_payload_B,
      I3 => AXI_video_strm_V_last_V_0_sel,
      I4 => AXI_video_strm_V_last_V_0_payload_A,
      O => \eol_2_reg_272[0]_i_2_n_2\
    );
\eol_2_reg_272_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \eol_2_reg_272[0]_i_1_n_2\,
      D => \eol_2_reg_272[0]_i_2_n_2\,
      Q => \eol_2_reg_272_reg_n_2_[0]\,
      R => '0'
    );
\eol_reg_213[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E000000000000000"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_0_state_reg_n_2_[0]\,
      I1 => brmerge_reg_429,
      I2 => img0_data_stream_2_s_full_n,
      I3 => img0_data_stream_1_s_full_n,
      I4 => img0_data_stream_0_s_full_n,
      I5 => \eol_reg_213[0]_i_2_n_2\,
      O => \eol_reg_213[0]_i_1_n_2\
    );
\eol_reg_213[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2000000000000"
    )
        port map (
      I0 => AXI_video_strm_V_last_V_0_data_out,
      I1 => brmerge_reg_429,
      I2 => eol_1_reg_225,
      I3 => \exitcond_reg_420_reg_n_2_[0]\,
      I4 => ap_enable_reg_pp1_iter1_reg_n_2,
      I5 => ap_CS_fsm_pp1_stage0,
      O => \eol_reg_213[0]_i_2_n_2\
    );
\eol_reg_213_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => eol_reg_213,
      D => \eol_reg_213[0]_i_1_n_2\,
      Q => \eol_reg_213_reg_n_2_[0]\,
      R => '0'
    );
\exitcond_reg_420[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => exitcond_fu_338_p2,
      I1 => exitcond_reg_4200,
      I2 => \exitcond_reg_420_reg_n_2_[0]\,
      O => \exitcond_reg_420[0]_i_1_n_2\
    );
\exitcond_reg_420_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \exitcond_reg_420[0]_i_1_n_2\,
      Q => \exitcond_reg_420_reg_n_2_[0]\,
      R => '0'
    );
\i_V_reg_415[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => t_V_reg_191(0),
      O => i_V_fu_332_p2(0)
    );
\i_V_reg_415[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => t_V_reg_191(0),
      I1 => t_V_reg_191(1),
      O => i_V_fu_332_p2(1)
    );
\i_V_reg_415[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => t_V_reg_191(2),
      I1 => t_V_reg_191(1),
      I2 => t_V_reg_191(0),
      O => i_V_fu_332_p2(2)
    );
\i_V_reg_415[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => t_V_reg_191(3),
      I1 => t_V_reg_191(0),
      I2 => t_V_reg_191(1),
      I3 => t_V_reg_191(2),
      O => i_V_fu_332_p2(3)
    );
\i_V_reg_415[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => t_V_reg_191(4),
      I1 => t_V_reg_191(2),
      I2 => t_V_reg_191(1),
      I3 => t_V_reg_191(0),
      I4 => t_V_reg_191(3),
      O => i_V_fu_332_p2(4)
    );
\i_V_reg_415[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => t_V_reg_191(5),
      I1 => t_V_reg_191(3),
      I2 => t_V_reg_191(0),
      I3 => t_V_reg_191(1),
      I4 => t_V_reg_191(2),
      I5 => t_V_reg_191(4),
      O => i_V_fu_332_p2(5)
    );
\i_V_reg_415[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => t_V_reg_191(6),
      I1 => \i_V_reg_415[9]_i_2_n_2\,
      O => i_V_fu_332_p2(6)
    );
\i_V_reg_415[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => t_V_reg_191(7),
      I1 => \i_V_reg_415[9]_i_2_n_2\,
      I2 => t_V_reg_191(6),
      O => i_V_fu_332_p2(7)
    );
\i_V_reg_415[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => t_V_reg_191(8),
      I1 => t_V_reg_191(6),
      I2 => \i_V_reg_415[9]_i_2_n_2\,
      I3 => t_V_reg_191(7),
      O => i_V_fu_332_p2(8)
    );
\i_V_reg_415[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => t_V_reg_191(9),
      I1 => t_V_reg_191(7),
      I2 => \i_V_reg_415[9]_i_2_n_2\,
      I3 => t_V_reg_191(6),
      I4 => t_V_reg_191(8),
      O => i_V_fu_332_p2(9)
    );
\i_V_reg_415[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => t_V_reg_191(5),
      I1 => t_V_reg_191(3),
      I2 => t_V_reg_191(0),
      I3 => t_V_reg_191(1),
      I4 => t_V_reg_191(2),
      I5 => t_V_reg_191(4),
      O => \i_V_reg_415[9]_i_2_n_2\
    );
\i_V_reg_415_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state4,
      D => i_V_fu_332_p2(0),
      Q => i_V_reg_415(0),
      R => '0'
    );
\i_V_reg_415_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state4,
      D => i_V_fu_332_p2(1),
      Q => i_V_reg_415(1),
      R => '0'
    );
\i_V_reg_415_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state4,
      D => i_V_fu_332_p2(2),
      Q => i_V_reg_415(2),
      R => '0'
    );
\i_V_reg_415_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state4,
      D => i_V_fu_332_p2(3),
      Q => i_V_reg_415(3),
      R => '0'
    );
\i_V_reg_415_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state4,
      D => i_V_fu_332_p2(4),
      Q => i_V_reg_415(4),
      R => '0'
    );
\i_V_reg_415_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state4,
      D => i_V_fu_332_p2(5),
      Q => i_V_reg_415(5),
      R => '0'
    );
\i_V_reg_415_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state4,
      D => i_V_fu_332_p2(6),
      Q => i_V_reg_415(6),
      R => '0'
    );
\i_V_reg_415_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state4,
      D => i_V_fu_332_p2(7),
      Q => i_V_reg_415(7),
      R => '0'
    );
\i_V_reg_415_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state4,
      D => i_V_fu_332_p2(8),
      Q => i_V_reg_415(8),
      R => '0'
    );
\i_V_reg_415_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state4,
      D => i_V_fu_332_p2(9),
      Q => i_V_reg_415(9),
      R => '0'
    );
\sof_1_fu_128[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0EEE"
    )
        port map (
      I0 => sof_1_fu_128,
      I1 => ap_CS_fsm_state3,
      I2 => brmerge_reg_4290,
      I3 => ap_enable_reg_pp1_iter0,
      O => \sof_1_fu_128[0]_i_1_n_2\
    );
\sof_1_fu_128_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \sof_1_fu_128[0]_i_1_n_2\,
      Q => sof_1_fu_128,
      R => '0'
    );
\start_once_reg_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DDD0"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => \ap_CS_fsm[0]_i_2_n_2\,
      I2 => start_for_CvtColor_U0_full_n,
      I3 => \^start_once_reg\,
      O => \start_once_reg_i_1__2_n_2\
    );
start_once_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \start_once_reg_i_1__2_n_2\,
      Q => \^start_once_reg\,
      R => \^ap_rst_n_inv\
    );
\t_V_3_reg_202[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \t_V_3_reg_202_reg__0\(0),
      O => j_V_fu_344_p2(0)
    );
\t_V_3_reg_202[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => p_1_in,
      I1 => brmerge_reg_4290,
      I2 => ap_enable_reg_pp1_iter0,
      O => t_V_3_reg_202
    );
\t_V_3_reg_202[10]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ap_enable_reg_pp1_iter0,
      I1 => brmerge_reg_4290,
      O => sof_1_fu_1280
    );
\t_V_3_reg_202[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \t_V_3_reg_202_reg__0\(10),
      I1 => \t_V_3_reg_202_reg__0\(8),
      I2 => \t_V_3_reg_202_reg__0\(6),
      I3 => \t_V_3_reg_202[10]_i_5_n_2\,
      I4 => \t_V_3_reg_202_reg__0\(7),
      I5 => \t_V_3_reg_202_reg__0\(9),
      O => j_V_fu_344_p2(10)
    );
\t_V_3_reg_202[10]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => exitcond_reg_4200,
      I1 => exitcond_fu_338_p2,
      O => brmerge_reg_4290
    );
\t_V_3_reg_202[10]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \t_V_3_reg_202_reg__0\(5),
      I1 => \t_V_3_reg_202_reg__0\(3),
      I2 => \t_V_3_reg_202_reg__0\(2),
      I3 => \t_V_3_reg_202_reg__0\(0),
      I4 => \t_V_3_reg_202_reg__0\(1),
      I5 => \t_V_3_reg_202_reg__0\(4),
      O => \t_V_3_reg_202[10]_i_5_n_2\
    );
\t_V_3_reg_202[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \t_V_3_reg_202_reg__0\(0),
      I1 => \t_V_3_reg_202_reg__0\(1),
      O => j_V_fu_344_p2(1)
    );
\t_V_3_reg_202[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \t_V_3_reg_202_reg__0\(2),
      I1 => \t_V_3_reg_202_reg__0\(1),
      I2 => \t_V_3_reg_202_reg__0\(0),
      O => j_V_fu_344_p2(2)
    );
\t_V_3_reg_202[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \t_V_3_reg_202_reg__0\(3),
      I1 => \t_V_3_reg_202_reg__0\(2),
      I2 => \t_V_3_reg_202_reg__0\(0),
      I3 => \t_V_3_reg_202_reg__0\(1),
      O => j_V_fu_344_p2(3)
    );
\t_V_3_reg_202[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \t_V_3_reg_202_reg__0\(4),
      I1 => \t_V_3_reg_202_reg__0\(1),
      I2 => \t_V_3_reg_202_reg__0\(0),
      I3 => \t_V_3_reg_202_reg__0\(2),
      I4 => \t_V_3_reg_202_reg__0\(3),
      O => j_V_fu_344_p2(4)
    );
\t_V_3_reg_202[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \t_V_3_reg_202_reg__0\(5),
      I1 => \t_V_3_reg_202_reg__0\(3),
      I2 => \t_V_3_reg_202_reg__0\(2),
      I3 => \t_V_3_reg_202_reg__0\(0),
      I4 => \t_V_3_reg_202_reg__0\(1),
      I5 => \t_V_3_reg_202_reg__0\(4),
      O => j_V_fu_344_p2(5)
    );
\t_V_3_reg_202[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \t_V_3_reg_202_reg__0\(6),
      I1 => \t_V_3_reg_202[10]_i_5_n_2\,
      O => j_V_fu_344_p2(6)
    );
\t_V_3_reg_202[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \t_V_3_reg_202_reg__0\(7),
      I1 => \t_V_3_reg_202[10]_i_5_n_2\,
      I2 => \t_V_3_reg_202_reg__0\(6),
      O => j_V_fu_344_p2(7)
    );
\t_V_3_reg_202[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \t_V_3_reg_202_reg__0\(8),
      I1 => \t_V_3_reg_202_reg__0\(6),
      I2 => \t_V_3_reg_202[10]_i_5_n_2\,
      I3 => \t_V_3_reg_202_reg__0\(7),
      O => j_V_fu_344_p2(8)
    );
\t_V_3_reg_202[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \t_V_3_reg_202_reg__0\(9),
      I1 => \t_V_3_reg_202_reg__0\(7),
      I2 => \t_V_3_reg_202[10]_i_5_n_2\,
      I3 => \t_V_3_reg_202_reg__0\(6),
      I4 => \t_V_3_reg_202_reg__0\(8),
      O => j_V_fu_344_p2(9)
    );
\t_V_3_reg_202_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1280,
      D => j_V_fu_344_p2(0),
      Q => \t_V_3_reg_202_reg__0\(0),
      R => t_V_3_reg_202
    );
\t_V_3_reg_202_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1280,
      D => j_V_fu_344_p2(10),
      Q => \t_V_3_reg_202_reg__0\(10),
      R => t_V_3_reg_202
    );
\t_V_3_reg_202_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1280,
      D => j_V_fu_344_p2(1),
      Q => \t_V_3_reg_202_reg__0\(1),
      R => t_V_3_reg_202
    );
\t_V_3_reg_202_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1280,
      D => j_V_fu_344_p2(2),
      Q => \t_V_3_reg_202_reg__0\(2),
      R => t_V_3_reg_202
    );
\t_V_3_reg_202_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1280,
      D => j_V_fu_344_p2(3),
      Q => \t_V_3_reg_202_reg__0\(3),
      R => t_V_3_reg_202
    );
\t_V_3_reg_202_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1280,
      D => j_V_fu_344_p2(4),
      Q => \t_V_3_reg_202_reg__0\(4),
      R => t_V_3_reg_202
    );
\t_V_3_reg_202_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1280,
      D => j_V_fu_344_p2(5),
      Q => \t_V_3_reg_202_reg__0\(5),
      R => t_V_3_reg_202
    );
\t_V_3_reg_202_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1280,
      D => j_V_fu_344_p2(6),
      Q => \t_V_3_reg_202_reg__0\(6),
      R => t_V_3_reg_202
    );
\t_V_3_reg_202_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1280,
      D => j_V_fu_344_p2(7),
      Q => \t_V_3_reg_202_reg__0\(7),
      R => t_V_3_reg_202
    );
\t_V_3_reg_202_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1280,
      D => j_V_fu_344_p2(8),
      Q => \t_V_3_reg_202_reg__0\(8),
      R => t_V_3_reg_202
    );
\t_V_3_reg_202_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => sof_1_fu_1280,
      D => j_V_fu_344_p2(9),
      Q => \t_V_3_reg_202_reg__0\(9),
      R => t_V_3_reg_202
    );
\t_V_reg_191_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state10,
      D => i_V_reg_415(0),
      Q => t_V_reg_191(0),
      R => ap_CS_fsm_state3
    );
\t_V_reg_191_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state10,
      D => i_V_reg_415(1),
      Q => t_V_reg_191(1),
      R => ap_CS_fsm_state3
    );
\t_V_reg_191_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state10,
      D => i_V_reg_415(2),
      Q => t_V_reg_191(2),
      R => ap_CS_fsm_state3
    );
\t_V_reg_191_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state10,
      D => i_V_reg_415(3),
      Q => t_V_reg_191(3),
      R => ap_CS_fsm_state3
    );
\t_V_reg_191_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state10,
      D => i_V_reg_415(4),
      Q => t_V_reg_191(4),
      R => ap_CS_fsm_state3
    );
\t_V_reg_191_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state10,
      D => i_V_reg_415(5),
      Q => t_V_reg_191(5),
      R => ap_CS_fsm_state3
    );
\t_V_reg_191_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state10,
      D => i_V_reg_415(6),
      Q => t_V_reg_191(6),
      R => ap_CS_fsm_state3
    );
\t_V_reg_191_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state10,
      D => i_V_reg_415(7),
      Q => t_V_reg_191(7),
      R => ap_CS_fsm_state3
    );
\t_V_reg_191_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state10,
      D => i_V_reg_415(8),
      Q => t_V_reg_191(8),
      R => ap_CS_fsm_state3
    );
\t_V_reg_191_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state10,
      D => i_V_reg_415(9),
      Q => t_V_reg_191(9),
      R => ap_CS_fsm_state3
    );
\tmp_data_V_reg_391[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(0),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(0),
      O => AXI_video_strm_V_data_V_0_data_out(0)
    );
\tmp_data_V_reg_391[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(10),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(10),
      O => AXI_video_strm_V_data_V_0_data_out(10)
    );
\tmp_data_V_reg_391[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(11),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(11),
      O => AXI_video_strm_V_data_V_0_data_out(11)
    );
\tmp_data_V_reg_391[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(12),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(12),
      O => AXI_video_strm_V_data_V_0_data_out(12)
    );
\tmp_data_V_reg_391[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(13),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(13),
      O => AXI_video_strm_V_data_V_0_data_out(13)
    );
\tmp_data_V_reg_391[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(14),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(14),
      O => AXI_video_strm_V_data_V_0_data_out(14)
    );
\tmp_data_V_reg_391[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(15),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(15),
      O => AXI_video_strm_V_data_V_0_data_out(15)
    );
\tmp_data_V_reg_391[16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(16),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(16),
      O => AXI_video_strm_V_data_V_0_data_out(16)
    );
\tmp_data_V_reg_391[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(17),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(17),
      O => AXI_video_strm_V_data_V_0_data_out(17)
    );
\tmp_data_V_reg_391[18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(18),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(18),
      O => AXI_video_strm_V_data_V_0_data_out(18)
    );
\tmp_data_V_reg_391[19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(19),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(19),
      O => AXI_video_strm_V_data_V_0_data_out(19)
    );
\tmp_data_V_reg_391[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(1),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(1),
      O => AXI_video_strm_V_data_V_0_data_out(1)
    );
\tmp_data_V_reg_391[20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(20),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(20),
      O => AXI_video_strm_V_data_V_0_data_out(20)
    );
\tmp_data_V_reg_391[21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(21),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(21),
      O => AXI_video_strm_V_data_V_0_data_out(21)
    );
\tmp_data_V_reg_391[22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(22),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(22),
      O => AXI_video_strm_V_data_V_0_data_out(22)
    );
\tmp_data_V_reg_391[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(23),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(23),
      O => AXI_video_strm_V_data_V_0_data_out(23)
    );
\tmp_data_V_reg_391[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(2),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(2),
      O => AXI_video_strm_V_data_V_0_data_out(2)
    );
\tmp_data_V_reg_391[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(3),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(3),
      O => AXI_video_strm_V_data_V_0_data_out(3)
    );
\tmp_data_V_reg_391[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(4),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(4),
      O => AXI_video_strm_V_data_V_0_data_out(4)
    );
\tmp_data_V_reg_391[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(5),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(5),
      O => AXI_video_strm_V_data_V_0_data_out(5)
    );
\tmp_data_V_reg_391[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(6),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(6),
      O => AXI_video_strm_V_data_V_0_data_out(6)
    );
\tmp_data_V_reg_391[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(7),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(7),
      O => AXI_video_strm_V_data_V_0_data_out(7)
    );
\tmp_data_V_reg_391[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(8),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(8),
      O => AXI_video_strm_V_data_V_0_data_out(8)
    );
\tmp_data_V_reg_391[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_0_payload_B(9),
      I1 => AXI_video_strm_V_data_V_0_sel,
      I2 => AXI_video_strm_V_data_V_0_payload_A(9),
      O => AXI_video_strm_V_data_V_0_data_out(9)
    );
\tmp_data_V_reg_391_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(0),
      Q => tmp_data_V_reg_391(0),
      R => '0'
    );
\tmp_data_V_reg_391_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(10),
      Q => tmp_data_V_reg_391(10),
      R => '0'
    );
\tmp_data_V_reg_391_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(11),
      Q => tmp_data_V_reg_391(11),
      R => '0'
    );
\tmp_data_V_reg_391_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(12),
      Q => tmp_data_V_reg_391(12),
      R => '0'
    );
\tmp_data_V_reg_391_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(13),
      Q => tmp_data_V_reg_391(13),
      R => '0'
    );
\tmp_data_V_reg_391_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(14),
      Q => tmp_data_V_reg_391(14),
      R => '0'
    );
\tmp_data_V_reg_391_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(15),
      Q => tmp_data_V_reg_391(15),
      R => '0'
    );
\tmp_data_V_reg_391_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(16),
      Q => tmp_data_V_reg_391(16),
      R => '0'
    );
\tmp_data_V_reg_391_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(17),
      Q => tmp_data_V_reg_391(17),
      R => '0'
    );
\tmp_data_V_reg_391_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(18),
      Q => tmp_data_V_reg_391(18),
      R => '0'
    );
\tmp_data_V_reg_391_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(19),
      Q => tmp_data_V_reg_391(19),
      R => '0'
    );
\tmp_data_V_reg_391_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(1),
      Q => tmp_data_V_reg_391(1),
      R => '0'
    );
\tmp_data_V_reg_391_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(20),
      Q => tmp_data_V_reg_391(20),
      R => '0'
    );
\tmp_data_V_reg_391_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(21),
      Q => tmp_data_V_reg_391(21),
      R => '0'
    );
\tmp_data_V_reg_391_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(22),
      Q => tmp_data_V_reg_391(22),
      R => '0'
    );
\tmp_data_V_reg_391_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(23),
      Q => tmp_data_V_reg_391(23),
      R => '0'
    );
\tmp_data_V_reg_391_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(2),
      Q => tmp_data_V_reg_391(2),
      R => '0'
    );
\tmp_data_V_reg_391_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(3),
      Q => tmp_data_V_reg_391(3),
      R => '0'
    );
\tmp_data_V_reg_391_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(4),
      Q => tmp_data_V_reg_391(4),
      R => '0'
    );
\tmp_data_V_reg_391_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(5),
      Q => tmp_data_V_reg_391(5),
      R => '0'
    );
\tmp_data_V_reg_391_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(6),
      Q => tmp_data_V_reg_391(6),
      R => '0'
    );
\tmp_data_V_reg_391_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(7),
      Q => tmp_data_V_reg_391(7),
      R => '0'
    );
\tmp_data_V_reg_391_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(8),
      Q => tmp_data_V_reg_391(8),
      R => '0'
    );
\tmp_data_V_reg_391_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_data_V_0_data_out(9),
      Q => tmp_data_V_reg_391(9),
      R => '0'
    );
\tmp_last_V_reg_399[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_0_state_reg_n_2_[0]\,
      I1 => ap_CS_fsm_state2,
      O => AXI_video_strm_V_data_V_0_sel2
    );
\tmp_last_V_reg_399[0]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_last_V_0_payload_B,
      I1 => AXI_video_strm_V_last_V_0_sel,
      I2 => AXI_video_strm_V_last_V_0_payload_A,
      O => AXI_video_strm_V_last_V_0_data_out
    );
\tmp_last_V_reg_399_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_0_sel2,
      D => AXI_video_strm_V_last_V_0_data_out,
      Q => tmp_last_V_reg_399,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_CvtColor_1 is
  port (
    start_once_reg : out STD_LOGIC;
    start_once_reg_reg_0 : out STD_LOGIC;
    shiftReg_ce : out STD_LOGIC;
    ap_rst_n_inv : in STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    CvtColor_1_U0_ap_start : in STD_LOGIC;
    start_for_Mat2AXIvideo_U0_full_n : in STD_LOGIC;
    internal_empty_n_reg : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_CvtColor_1;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_CvtColor_1 is
  signal \ap_CS_fsm[2]_i_3__0_n_2\ : STD_LOGIC;
  signal \ap_CS_fsm[2]_i_4_n_2\ : STD_LOGIC;
  signal \ap_CS_fsm[3]_i_2__1_n_2\ : STD_LOGIC;
  signal ap_CS_fsm_pp0_stage0 : STD_LOGIC;
  signal \ap_CS_fsm_reg_n_2_[0]\ : STD_LOGIC;
  signal ap_CS_fsm_state2 : STD_LOGIC;
  signal ap_CS_fsm_state5 : STD_LOGIC;
  signal ap_NS_fsm : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal ap_enable_reg_pp0_iter0 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter00 : STD_LOGIC;
  signal \ap_enable_reg_pp0_iter0_i_1__1_n_2\ : STD_LOGIC;
  signal \ap_enable_reg_pp0_iter1_i_1__0_n_2\ : STD_LOGIC;
  signal ap_enable_reg_pp0_iter1_reg_n_2 : STD_LOGIC;
  signal i_1_fu_210_p2 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal i_1_reg_232 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \i_1_reg_232[9]_i_2_n_2\ : STD_LOGIC;
  signal i_reg_182 : STD_LOGIC;
  signal \i_reg_182_reg_n_2_[0]\ : STD_LOGIC;
  signal \i_reg_182_reg_n_2_[1]\ : STD_LOGIC;
  signal \i_reg_182_reg_n_2_[2]\ : STD_LOGIC;
  signal \i_reg_182_reg_n_2_[3]\ : STD_LOGIC;
  signal \i_reg_182_reg_n_2_[4]\ : STD_LOGIC;
  signal \i_reg_182_reg_n_2_[5]\ : STD_LOGIC;
  signal \i_reg_182_reg_n_2_[6]\ : STD_LOGIC;
  signal \i_reg_182_reg_n_2_[7]\ : STD_LOGIC;
  signal \i_reg_182_reg_n_2_[8]\ : STD_LOGIC;
  signal \i_reg_182_reg_n_2_[9]\ : STD_LOGIC;
  signal j_1_fu_222_p2 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal j_reg_193 : STD_LOGIC;
  signal j_reg_1930 : STD_LOGIC;
  signal \j_reg_193[10]_i_4_n_2\ : STD_LOGIC;
  signal \j_reg_193_reg__0\ : STD_LOGIC_VECTOR ( 10 downto 8 );
  signal \j_reg_193_reg_n_2_[0]\ : STD_LOGIC;
  signal \j_reg_193_reg_n_2_[1]\ : STD_LOGIC;
  signal \j_reg_193_reg_n_2_[2]\ : STD_LOGIC;
  signal \j_reg_193_reg_n_2_[3]\ : STD_LOGIC;
  signal \j_reg_193_reg_n_2_[4]\ : STD_LOGIC;
  signal \j_reg_193_reg_n_2_[5]\ : STD_LOGIC;
  signal \j_reg_193_reg_n_2_[6]\ : STD_LOGIC;
  signal \j_reg_193_reg_n_2_[7]\ : STD_LOGIC;
  signal \^start_once_reg\ : STD_LOGIC;
  signal \start_once_reg_i_1__1_n_2\ : STD_LOGIC;
  signal \^start_once_reg_reg_0\ : STD_LOGIC;
  signal tmp_21_fu_216_p2 : STD_LOGIC;
  signal tmp_21_reg_237 : STD_LOGIC;
  signal \tmp_21_reg_237[0]_i_1_n_2\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \SRL_SIG[0][0]_i_1\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \ap_CS_fsm[0]_i_2__1\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \ap_CS_fsm[2]_i_2__0\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \ap_CS_fsm[2]_i_3__0\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \ap_CS_fsm[3]_i_2__1\ : label is "soft_lutpair63";
  attribute FSM_ENCODING : string;
  attribute FSM_ENCODING of \ap_CS_fsm_reg[0]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[1]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[2]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[3]\ : label is "none";
  attribute SOFT_HLUTNM of \ap_enable_reg_pp0_iter1_i_2__0\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \i_1_reg_232[1]_i_1\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \i_1_reg_232[2]_i_1\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \i_1_reg_232[3]_i_1\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \i_1_reg_232[4]_i_1\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \i_1_reg_232[6]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \i_1_reg_232[7]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \i_1_reg_232[8]_i_1\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \i_1_reg_232[9]_i_1\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \j_reg_193[1]_i_1\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \j_reg_193[2]_i_1\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \j_reg_193[3]_i_1\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \j_reg_193[4]_i_1\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \j_reg_193[6]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \j_reg_193[7]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \j_reg_193[8]_i_1\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \j_reg_193[9]_i_1\ : label is "soft_lutpair58";
begin
  start_once_reg <= \^start_once_reg\;
  start_once_reg_reg_0 <= \^start_once_reg_reg_0\;
\SRL_SIG[0][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => tmp_21_reg_237,
      I1 => ap_enable_reg_pp0_iter1_reg_n_2,
      I2 => \ap_CS_fsm[3]_i_2__1_n_2\,
      I3 => ap_CS_fsm_pp0_stage0,
      O => shiftReg_ce
    );
\ap_CS_fsm[0]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBBFAAAA"
    )
        port map (
      I0 => \^start_once_reg_reg_0\,
      I1 => CvtColor_1_U0_ap_start,
      I2 => start_for_Mat2AXIvideo_U0_full_n,
      I3 => \^start_once_reg\,
      I4 => \ap_CS_fsm_reg_n_2_[0]\,
      O => ap_NS_fsm(0)
    );
\ap_CS_fsm[0]_i_2__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ap_CS_fsm_state2,
      I1 => \ap_CS_fsm[2]_i_4_n_2\,
      O => \^start_once_reg_reg_0\
    );
\ap_CS_fsm[1]_i_1__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEEAAAAA"
    )
        port map (
      I0 => ap_CS_fsm_state5,
      I1 => CvtColor_1_U0_ap_start,
      I2 => start_for_Mat2AXIvideo_U0_full_n,
      I3 => \^start_once_reg\,
      I4 => \ap_CS_fsm_reg_n_2_[0]\,
      O => ap_NS_fsm(1)
    );
\ap_CS_fsm[2]_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BFAA"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter00,
      I1 => ap_enable_reg_pp0_iter0,
      I2 => \ap_CS_fsm[2]_i_3__0_n_2\,
      I3 => ap_CS_fsm_pp0_stage0,
      O => ap_NS_fsm(2)
    );
\ap_CS_fsm[2]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ap_CS_fsm_state2,
      I1 => \ap_CS_fsm[2]_i_4_n_2\,
      O => ap_enable_reg_pp0_iter00
    );
\ap_CS_fsm[2]_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5400"
    )
        port map (
      I0 => \ap_CS_fsm[3]_i_2__1_n_2\,
      I1 => \j_reg_193_reg__0\(8),
      I2 => \j_reg_193_reg__0\(9),
      I3 => \j_reg_193_reg__0\(10),
      O => \ap_CS_fsm[2]_i_3__0_n_2\
    );
\ap_CS_fsm[2]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555555557FFFFFF"
    )
        port map (
      I0 => \i_reg_182_reg_n_2_[9]\,
      I1 => \i_reg_182_reg_n_2_[5]\,
      I2 => \i_reg_182_reg_n_2_[4]\,
      I3 => \i_reg_182_reg_n_2_[7]\,
      I4 => \i_reg_182_reg_n_2_[6]\,
      I5 => \i_reg_182_reg_n_2_[8]\,
      O => \ap_CS_fsm[2]_i_4_n_2\
    );
\ap_CS_fsm[3]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2220000000000000"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter0,
      I1 => \ap_CS_fsm[3]_i_2__1_n_2\,
      I2 => \j_reg_193_reg__0\(8),
      I3 => \j_reg_193_reg__0\(9),
      I4 => \j_reg_193_reg__0\(10),
      I5 => ap_CS_fsm_pp0_stage0,
      O => ap_NS_fsm(3)
    );
\ap_CS_fsm[3]_i_2__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => tmp_21_reg_237,
      I1 => ap_enable_reg_pp0_iter1_reg_n_2,
      I2 => internal_empty_n_reg,
      O => \ap_CS_fsm[3]_i_2__1_n_2\
    );
\ap_CS_fsm_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(0),
      Q => \ap_CS_fsm_reg_n_2_[0]\,
      S => ap_rst_n_inv
    );
\ap_CS_fsm_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(1),
      Q => ap_CS_fsm_state2,
      R => ap_rst_n_inv
    );
\ap_CS_fsm_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(2),
      Q => ap_CS_fsm_pp0_stage0,
      R => ap_rst_n_inv
    );
\ap_CS_fsm_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(3),
      Q => ap_CS_fsm_state5,
      R => ap_rst_n_inv
    );
\ap_enable_reg_pp0_iter0_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"70707000"
    )
        port map (
      I0 => \ap_CS_fsm[2]_i_3__0_n_2\,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => ap_rst_n,
      I3 => ap_enable_reg_pp0_iter00,
      I4 => ap_enable_reg_pp0_iter0,
      O => \ap_enable_reg_pp0_iter0_i_1__1_n_2\
    );
ap_enable_reg_pp0_iter0_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \ap_enable_reg_pp0_iter0_i_1__1_n_2\,
      Q => ap_enable_reg_pp0_iter0,
      R => '0'
    );
\ap_enable_reg_pp0_iter1_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"40CC400040004000"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter00,
      I1 => ap_rst_n,
      I2 => ap_enable_reg_pp0_iter1_reg_n_2,
      I3 => \ap_CS_fsm[3]_i_2__1_n_2\,
      I4 => ap_enable_reg_pp0_iter0,
      I5 => tmp_21_fu_216_p2,
      O => \ap_enable_reg_pp0_iter1_i_1__0_n_2\
    );
\ap_enable_reg_pp0_iter1_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"57"
    )
        port map (
      I0 => \j_reg_193_reg__0\(10),
      I1 => \j_reg_193_reg__0\(9),
      I2 => \j_reg_193_reg__0\(8),
      O => tmp_21_fu_216_p2
    );
ap_enable_reg_pp0_iter1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \ap_enable_reg_pp0_iter1_i_1__0_n_2\,
      Q => ap_enable_reg_pp0_iter1_reg_n_2,
      R => '0'
    );
\i_1_reg_232[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \i_reg_182_reg_n_2_[0]\,
      O => i_1_fu_210_p2(0)
    );
\i_1_reg_232[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \i_reg_182_reg_n_2_[0]\,
      I1 => \i_reg_182_reg_n_2_[1]\,
      O => i_1_fu_210_p2(1)
    );
\i_1_reg_232[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \i_reg_182_reg_n_2_[2]\,
      I1 => \i_reg_182_reg_n_2_[1]\,
      I2 => \i_reg_182_reg_n_2_[0]\,
      O => i_1_fu_210_p2(2)
    );
\i_1_reg_232[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \i_reg_182_reg_n_2_[3]\,
      I1 => \i_reg_182_reg_n_2_[0]\,
      I2 => \i_reg_182_reg_n_2_[1]\,
      I3 => \i_reg_182_reg_n_2_[2]\,
      O => i_1_fu_210_p2(3)
    );
\i_1_reg_232[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \i_reg_182_reg_n_2_[4]\,
      I1 => \i_reg_182_reg_n_2_[2]\,
      I2 => \i_reg_182_reg_n_2_[1]\,
      I3 => \i_reg_182_reg_n_2_[0]\,
      I4 => \i_reg_182_reg_n_2_[3]\,
      O => i_1_fu_210_p2(4)
    );
\i_1_reg_232[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \i_reg_182_reg_n_2_[5]\,
      I1 => \i_reg_182_reg_n_2_[3]\,
      I2 => \i_reg_182_reg_n_2_[0]\,
      I3 => \i_reg_182_reg_n_2_[1]\,
      I4 => \i_reg_182_reg_n_2_[2]\,
      I5 => \i_reg_182_reg_n_2_[4]\,
      O => i_1_fu_210_p2(5)
    );
\i_1_reg_232[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \i_reg_182_reg_n_2_[6]\,
      I1 => \i_1_reg_232[9]_i_2_n_2\,
      O => i_1_fu_210_p2(6)
    );
\i_1_reg_232[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \i_reg_182_reg_n_2_[7]\,
      I1 => \i_1_reg_232[9]_i_2_n_2\,
      I2 => \i_reg_182_reg_n_2_[6]\,
      O => i_1_fu_210_p2(7)
    );
\i_1_reg_232[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \i_reg_182_reg_n_2_[8]\,
      I1 => \i_reg_182_reg_n_2_[6]\,
      I2 => \i_reg_182_reg_n_2_[7]\,
      I3 => \i_1_reg_232[9]_i_2_n_2\,
      O => i_1_fu_210_p2(8)
    );
\i_1_reg_232[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \i_reg_182_reg_n_2_[9]\,
      I1 => \i_1_reg_232[9]_i_2_n_2\,
      I2 => \i_reg_182_reg_n_2_[7]\,
      I3 => \i_reg_182_reg_n_2_[6]\,
      I4 => \i_reg_182_reg_n_2_[8]\,
      O => i_1_fu_210_p2(9)
    );
\i_1_reg_232[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \i_reg_182_reg_n_2_[5]\,
      I1 => \i_reg_182_reg_n_2_[3]\,
      I2 => \i_reg_182_reg_n_2_[0]\,
      I3 => \i_reg_182_reg_n_2_[1]\,
      I4 => \i_reg_182_reg_n_2_[2]\,
      I5 => \i_reg_182_reg_n_2_[4]\,
      O => \i_1_reg_232[9]_i_2_n_2\
    );
\i_1_reg_232_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_1_fu_210_p2(0),
      Q => i_1_reg_232(0),
      R => '0'
    );
\i_1_reg_232_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_1_fu_210_p2(1),
      Q => i_1_reg_232(1),
      R => '0'
    );
\i_1_reg_232_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_1_fu_210_p2(2),
      Q => i_1_reg_232(2),
      R => '0'
    );
\i_1_reg_232_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_1_fu_210_p2(3),
      Q => i_1_reg_232(3),
      R => '0'
    );
\i_1_reg_232_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_1_fu_210_p2(4),
      Q => i_1_reg_232(4),
      R => '0'
    );
\i_1_reg_232_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_1_fu_210_p2(5),
      Q => i_1_reg_232(5),
      R => '0'
    );
\i_1_reg_232_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_1_fu_210_p2(6),
      Q => i_1_reg_232(6),
      R => '0'
    );
\i_1_reg_232_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_1_fu_210_p2(7),
      Q => i_1_reg_232(7),
      R => '0'
    );
\i_1_reg_232_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_1_fu_210_p2(8),
      Q => i_1_reg_232(8),
      R => '0'
    );
\i_1_reg_232_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_1_fu_210_p2(9),
      Q => i_1_reg_232(9),
      R => '0'
    );
\i_reg_182[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000A800"
    )
        port map (
      I0 => CvtColor_1_U0_ap_start,
      I1 => start_for_Mat2AXIvideo_U0_full_n,
      I2 => \^start_once_reg\,
      I3 => \ap_CS_fsm_reg_n_2_[0]\,
      I4 => ap_CS_fsm_state5,
      O => i_reg_182
    );
\i_reg_182_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state5,
      D => i_1_reg_232(0),
      Q => \i_reg_182_reg_n_2_[0]\,
      R => i_reg_182
    );
\i_reg_182_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state5,
      D => i_1_reg_232(1),
      Q => \i_reg_182_reg_n_2_[1]\,
      R => i_reg_182
    );
\i_reg_182_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state5,
      D => i_1_reg_232(2),
      Q => \i_reg_182_reg_n_2_[2]\,
      R => i_reg_182
    );
\i_reg_182_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state5,
      D => i_1_reg_232(3),
      Q => \i_reg_182_reg_n_2_[3]\,
      R => i_reg_182
    );
\i_reg_182_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state5,
      D => i_1_reg_232(4),
      Q => \i_reg_182_reg_n_2_[4]\,
      R => i_reg_182
    );
\i_reg_182_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state5,
      D => i_1_reg_232(5),
      Q => \i_reg_182_reg_n_2_[5]\,
      R => i_reg_182
    );
\i_reg_182_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state5,
      D => i_1_reg_232(6),
      Q => \i_reg_182_reg_n_2_[6]\,
      R => i_reg_182
    );
\i_reg_182_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state5,
      D => i_1_reg_232(7),
      Q => \i_reg_182_reg_n_2_[7]\,
      R => i_reg_182
    );
\i_reg_182_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state5,
      D => i_1_reg_232(8),
      Q => \i_reg_182_reg_n_2_[8]\,
      R => i_reg_182
    );
\i_reg_182_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state5,
      D => i_1_reg_232(9),
      Q => \i_reg_182_reg_n_2_[9]\,
      R => i_reg_182
    );
\j_reg_193[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \j_reg_193_reg_n_2_[0]\,
      O => j_1_fu_222_p2(0)
    );
\j_reg_193[10]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter00,
      I1 => j_reg_1930,
      O => j_reg_193
    );
\j_reg_193[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0404044400000000"
    )
        port map (
      I0 => \ap_CS_fsm[3]_i_2__1_n_2\,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => \j_reg_193_reg__0\(10),
      I3 => \j_reg_193_reg__0\(9),
      I4 => \j_reg_193_reg__0\(8),
      I5 => ap_enable_reg_pp0_iter0,
      O => j_reg_1930
    );
\j_reg_193[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \j_reg_193_reg__0\(10),
      I1 => \j_reg_193_reg__0\(8),
      I2 => \j_reg_193_reg_n_2_[6]\,
      I3 => \j_reg_193[10]_i_4_n_2\,
      I4 => \j_reg_193_reg_n_2_[7]\,
      I5 => \j_reg_193_reg__0\(9),
      O => j_1_fu_222_p2(10)
    );
\j_reg_193[10]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \j_reg_193_reg_n_2_[5]\,
      I1 => \j_reg_193_reg_n_2_[3]\,
      I2 => \j_reg_193_reg_n_2_[0]\,
      I3 => \j_reg_193_reg_n_2_[1]\,
      I4 => \j_reg_193_reg_n_2_[2]\,
      I5 => \j_reg_193_reg_n_2_[4]\,
      O => \j_reg_193[10]_i_4_n_2\
    );
\j_reg_193[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \j_reg_193_reg_n_2_[0]\,
      I1 => \j_reg_193_reg_n_2_[1]\,
      O => j_1_fu_222_p2(1)
    );
\j_reg_193[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \j_reg_193_reg_n_2_[2]\,
      I1 => \j_reg_193_reg_n_2_[1]\,
      I2 => \j_reg_193_reg_n_2_[0]\,
      O => j_1_fu_222_p2(2)
    );
\j_reg_193[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \j_reg_193_reg_n_2_[3]\,
      I1 => \j_reg_193_reg_n_2_[0]\,
      I2 => \j_reg_193_reg_n_2_[1]\,
      I3 => \j_reg_193_reg_n_2_[2]\,
      O => j_1_fu_222_p2(3)
    );
\j_reg_193[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \j_reg_193_reg_n_2_[4]\,
      I1 => \j_reg_193_reg_n_2_[2]\,
      I2 => \j_reg_193_reg_n_2_[1]\,
      I3 => \j_reg_193_reg_n_2_[0]\,
      I4 => \j_reg_193_reg_n_2_[3]\,
      O => j_1_fu_222_p2(4)
    );
\j_reg_193[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \j_reg_193_reg_n_2_[5]\,
      I1 => \j_reg_193_reg_n_2_[3]\,
      I2 => \j_reg_193_reg_n_2_[0]\,
      I3 => \j_reg_193_reg_n_2_[1]\,
      I4 => \j_reg_193_reg_n_2_[2]\,
      I5 => \j_reg_193_reg_n_2_[4]\,
      O => j_1_fu_222_p2(5)
    );
\j_reg_193[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \j_reg_193_reg_n_2_[6]\,
      I1 => \j_reg_193[10]_i_4_n_2\,
      O => j_1_fu_222_p2(6)
    );
\j_reg_193[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \j_reg_193_reg_n_2_[7]\,
      I1 => \j_reg_193[10]_i_4_n_2\,
      I2 => \j_reg_193_reg_n_2_[6]\,
      O => j_1_fu_222_p2(7)
    );
\j_reg_193[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \j_reg_193_reg__0\(8),
      I1 => \j_reg_193_reg_n_2_[6]\,
      I2 => \j_reg_193[10]_i_4_n_2\,
      I3 => \j_reg_193_reg_n_2_[7]\,
      O => j_1_fu_222_p2(8)
    );
\j_reg_193[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \j_reg_193_reg__0\(9),
      I1 => \j_reg_193_reg_n_2_[7]\,
      I2 => \j_reg_193[10]_i_4_n_2\,
      I3 => \j_reg_193_reg_n_2_[6]\,
      I4 => \j_reg_193_reg__0\(8),
      O => j_1_fu_222_p2(9)
    );
\j_reg_193_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_1930,
      D => j_1_fu_222_p2(0),
      Q => \j_reg_193_reg_n_2_[0]\,
      R => j_reg_193
    );
\j_reg_193_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_1930,
      D => j_1_fu_222_p2(10),
      Q => \j_reg_193_reg__0\(10),
      R => j_reg_193
    );
\j_reg_193_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_1930,
      D => j_1_fu_222_p2(1),
      Q => \j_reg_193_reg_n_2_[1]\,
      R => j_reg_193
    );
\j_reg_193_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_1930,
      D => j_1_fu_222_p2(2),
      Q => \j_reg_193_reg_n_2_[2]\,
      R => j_reg_193
    );
\j_reg_193_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_1930,
      D => j_1_fu_222_p2(3),
      Q => \j_reg_193_reg_n_2_[3]\,
      R => j_reg_193
    );
\j_reg_193_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_1930,
      D => j_1_fu_222_p2(4),
      Q => \j_reg_193_reg_n_2_[4]\,
      R => j_reg_193
    );
\j_reg_193_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_1930,
      D => j_1_fu_222_p2(5),
      Q => \j_reg_193_reg_n_2_[5]\,
      R => j_reg_193
    );
\j_reg_193_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_1930,
      D => j_1_fu_222_p2(6),
      Q => \j_reg_193_reg_n_2_[6]\,
      R => j_reg_193
    );
\j_reg_193_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_1930,
      D => j_1_fu_222_p2(7),
      Q => \j_reg_193_reg_n_2_[7]\,
      R => j_reg_193
    );
\j_reg_193_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_1930,
      D => j_1_fu_222_p2(8),
      Q => \j_reg_193_reg__0\(8),
      R => j_reg_193
    );
\j_reg_193_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_1930,
      D => j_1_fu_222_p2(9),
      Q => \j_reg_193_reg__0\(9),
      R => j_reg_193
    );
\start_once_reg_i_1__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5444"
    )
        port map (
      I0 => \^start_once_reg_reg_0\,
      I1 => \^start_once_reg\,
      I2 => start_for_Mat2AXIvideo_U0_full_n,
      I3 => CvtColor_1_U0_ap_start,
      O => \start_once_reg_i_1__1_n_2\
    );
start_once_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \start_once_reg_i_1__1_n_2\,
      Q => \^start_once_reg\,
      R => ap_rst_n_inv
    );
\tmp_21_reg_237[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF57FF00005700"
    )
        port map (
      I0 => \j_reg_193_reg__0\(10),
      I1 => \j_reg_193_reg__0\(9),
      I2 => \j_reg_193_reg__0\(8),
      I3 => ap_CS_fsm_pp0_stage0,
      I4 => \ap_CS_fsm[3]_i_2__1_n_2\,
      I5 => tmp_21_reg_237,
      O => \tmp_21_reg_237[0]_i_1_n_2\
    );
\tmp_21_reg_237_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_21_reg_237[0]_i_1_n_2\,
      Q => tmp_21_reg_237,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_ram is
  port (
    DOBDO : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ADDRBWRADDR : out STD_LOGIC_VECTOR ( 9 downto 0 );
    ap_clk : in STD_LOGIC;
    \ap_CS_fsm_reg[4]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 10 downto 0 );
    p_assign_2_reg_2622 : in STD_LOGIC_VECTOR ( 10 downto 0 );
    ap_enable_reg_pp0_iter3 : in STD_LOGIC;
    ap_block_pp0_stage0_subdone2_in : in STD_LOGIC;
    ap_reg_pp0_iter2_or_cond_i_i_reg_2607 : in STD_LOGIC;
    \icmp_reg_2509_reg[0]\ : in STD_LOGIC;
    tmp_1_reg_2500 : in STD_LOGIC;
    \tmp_2_reg_2514_reg[0]\ : in STD_LOGIC;
    \k_buf_0_val_4_load_reg_2716_reg[7]\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \ap_reg_pp0_iter2_reg_588_reg[7]\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \p_p2_i_i_cast_cast_reg_2612_reg[10]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \ImagLoc_x_reg_2592_reg[10]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    or_cond_i_i_reg_2607 : in STD_LOGIC;
    tmp_33_reg_2617 : in STD_LOGIC;
    tmp_47_reg_2597 : in STD_LOGIC;
    tmp_31_reg_2602 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_ram;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_ram is
  signal \^addrbwraddr\ : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal k_buf_0_val_5_ce1 : STD_LOGIC;
  signal ram_reg_i_13_n_2 : STD_LOGIC;
  signal \ram_reg_i_2__0_n_2\ : STD_LOGIC;
  signal \ram_reg_i_3__0_n_2\ : STD_LOGIC;
  signal \ram_reg_i_4__0_n_2\ : STD_LOGIC;
  signal \ram_reg_i_5__0_n_2\ : STD_LOGIC;
  signal \ram_reg_i_6__0_n_2\ : STD_LOGIC;
  signal \ram_reg_i_7__0_n_2\ : STD_LOGIC;
  signal \ram_reg_i_8__0_n_2\ : STD_LOGIC;
  signal \ram_reg_i_9__0_n_2\ : STD_LOGIC;
  signal NLW_ram_reg_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 8 );
  signal NLW_ram_reg_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg : label is "p0_d8";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of ram_reg : label is "p0_d8";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg : label is 10240;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg : label is 7;
begin
  ADDRBWRADDR(9 downto 0) <= \^addrbwraddr\(9 downto 0);
ram_reg: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(13 downto 3) => Q(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 4) => \^addrbwraddr\(9 downto 0),
      ADDRBWRADDR(3) => p_assign_2_reg_2622(0),
      ADDRBWRADDR(2 downto 0) => B"111",
      CLKARDCLK => ap_clk,
      CLKBWRCLK => ap_clk,
      DIADI(15 downto 8) => B"00000000",
      DIADI(7) => \ram_reg_i_2__0_n_2\,
      DIADI(6) => \ram_reg_i_3__0_n_2\,
      DIADI(5) => \ram_reg_i_4__0_n_2\,
      DIADI(4) => \ram_reg_i_5__0_n_2\,
      DIADI(3) => \ram_reg_i_6__0_n_2\,
      DIADI(2) => \ram_reg_i_7__0_n_2\,
      DIADI(1) => \ram_reg_i_8__0_n_2\,
      DIADI(0) => \ram_reg_i_9__0_n_2\,
      DIBDI(15 downto 0) => B"0000000011111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"00",
      DOADO(15 downto 0) => NLW_ram_reg_DOADO_UNCONNECTED(15 downto 0),
      DOBDO(15 downto 8) => NLW_ram_reg_DOBDO_UNCONNECTED(15 downto 8),
      DOBDO(7 downto 0) => DOBDO(7 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => k_buf_0_val_5_ce1,
      ENBWREN => \ap_CS_fsm_reg[4]\,
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => k_buf_0_val_5_ce1,
      WEA(0) => k_buf_0_val_5_ce1,
      WEBWE(3 downto 0) => B"0000"
    );
ram_reg_i_10: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \p_p2_i_i_cast_cast_reg_2612_reg[10]\(2),
      I1 => ram_reg_i_13_n_2,
      I2 => \ImagLoc_x_reg_2592_reg[10]\(2),
      I3 => or_cond_i_i_reg_2607,
      I4 => p_assign_2_reg_2622(3),
      O => \^addrbwraddr\(2)
    );
ram_reg_i_11: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \p_p2_i_i_cast_cast_reg_2612_reg[10]\(1),
      I1 => ram_reg_i_13_n_2,
      I2 => \ImagLoc_x_reg_2592_reg[10]\(1),
      I3 => or_cond_i_i_reg_2607,
      I4 => p_assign_2_reg_2622(2),
      O => \^addrbwraddr\(1)
    );
ram_reg_i_12: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \p_p2_i_i_cast_cast_reg_2612_reg[10]\(0),
      I1 => ram_reg_i_13_n_2,
      I2 => \ImagLoc_x_reg_2592_reg[10]\(0),
      I3 => or_cond_i_i_reg_2607,
      I4 => p_assign_2_reg_2622(1),
      O => \^addrbwraddr\(0)
    );
ram_reg_i_13: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => tmp_33_reg_2617,
      I1 => tmp_47_reg_2597,
      I2 => tmp_31_reg_2602,
      O => ram_reg_i_13_n_2
    );
\ram_reg_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8080008080000000"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter3,
      I1 => ap_block_pp0_stage0_subdone2_in,
      I2 => ap_reg_pp0_iter2_or_cond_i_i_reg_2607,
      I3 => \icmp_reg_2509_reg[0]\,
      I4 => tmp_1_reg_2500,
      I5 => \tmp_2_reg_2514_reg[0]\,
      O => k_buf_0_val_5_ce1
    );
\ram_reg_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \k_buf_0_val_4_load_reg_2716_reg[7]\(7),
      I1 => \icmp_reg_2509_reg[0]\,
      I2 => tmp_1_reg_2500,
      I3 => \ap_reg_pp0_iter2_reg_588_reg[7]\(7),
      O => \ram_reg_i_2__0_n_2\
    );
\ram_reg_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \k_buf_0_val_4_load_reg_2716_reg[7]\(6),
      I1 => \icmp_reg_2509_reg[0]\,
      I2 => tmp_1_reg_2500,
      I3 => \ap_reg_pp0_iter2_reg_588_reg[7]\(6),
      O => \ram_reg_i_3__0_n_2\
    );
\ram_reg_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \p_p2_i_i_cast_cast_reg_2612_reg[10]\(9),
      I1 => ram_reg_i_13_n_2,
      I2 => \ImagLoc_x_reg_2592_reg[10]\(9),
      I3 => or_cond_i_i_reg_2607,
      I4 => p_assign_2_reg_2622(10),
      O => \^addrbwraddr\(9)
    );
\ram_reg_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \k_buf_0_val_4_load_reg_2716_reg[7]\(5),
      I1 => \icmp_reg_2509_reg[0]\,
      I2 => tmp_1_reg_2500,
      I3 => \ap_reg_pp0_iter2_reg_588_reg[7]\(5),
      O => \ram_reg_i_4__0_n_2\
    );
\ram_reg_i_4__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \p_p2_i_i_cast_cast_reg_2612_reg[10]\(8),
      I1 => ram_reg_i_13_n_2,
      I2 => \ImagLoc_x_reg_2592_reg[10]\(8),
      I3 => or_cond_i_i_reg_2607,
      I4 => p_assign_2_reg_2622(9),
      O => \^addrbwraddr\(8)
    );
\ram_reg_i_5__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \k_buf_0_val_4_load_reg_2716_reg[7]\(4),
      I1 => \icmp_reg_2509_reg[0]\,
      I2 => tmp_1_reg_2500,
      I3 => \ap_reg_pp0_iter2_reg_588_reg[7]\(4),
      O => \ram_reg_i_5__0_n_2\
    );
\ram_reg_i_5__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \p_p2_i_i_cast_cast_reg_2612_reg[10]\(7),
      I1 => ram_reg_i_13_n_2,
      I2 => \ImagLoc_x_reg_2592_reg[10]\(7),
      I3 => or_cond_i_i_reg_2607,
      I4 => p_assign_2_reg_2622(8),
      O => \^addrbwraddr\(7)
    );
\ram_reg_i_6__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \k_buf_0_val_4_load_reg_2716_reg[7]\(3),
      I1 => \icmp_reg_2509_reg[0]\,
      I2 => tmp_1_reg_2500,
      I3 => \ap_reg_pp0_iter2_reg_588_reg[7]\(3),
      O => \ram_reg_i_6__0_n_2\
    );
\ram_reg_i_6__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \p_p2_i_i_cast_cast_reg_2612_reg[10]\(6),
      I1 => ram_reg_i_13_n_2,
      I2 => \ImagLoc_x_reg_2592_reg[10]\(6),
      I3 => or_cond_i_i_reg_2607,
      I4 => p_assign_2_reg_2622(7),
      O => \^addrbwraddr\(6)
    );
\ram_reg_i_7__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \k_buf_0_val_4_load_reg_2716_reg[7]\(2),
      I1 => \icmp_reg_2509_reg[0]\,
      I2 => tmp_1_reg_2500,
      I3 => \ap_reg_pp0_iter2_reg_588_reg[7]\(2),
      O => \ram_reg_i_7__0_n_2\
    );
\ram_reg_i_7__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \p_p2_i_i_cast_cast_reg_2612_reg[10]\(5),
      I1 => ram_reg_i_13_n_2,
      I2 => \ImagLoc_x_reg_2592_reg[10]\(5),
      I3 => or_cond_i_i_reg_2607,
      I4 => p_assign_2_reg_2622(6),
      O => \^addrbwraddr\(5)
    );
\ram_reg_i_8__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \k_buf_0_val_4_load_reg_2716_reg[7]\(1),
      I1 => \icmp_reg_2509_reg[0]\,
      I2 => tmp_1_reg_2500,
      I3 => \ap_reg_pp0_iter2_reg_588_reg[7]\(1),
      O => \ram_reg_i_8__0_n_2\
    );
\ram_reg_i_8__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \p_p2_i_i_cast_cast_reg_2612_reg[10]\(4),
      I1 => ram_reg_i_13_n_2,
      I2 => \ImagLoc_x_reg_2592_reg[10]\(4),
      I3 => or_cond_i_i_reg_2607,
      I4 => p_assign_2_reg_2622(5),
      O => \^addrbwraddr\(4)
    );
\ram_reg_i_9__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \k_buf_0_val_4_load_reg_2716_reg[7]\(0),
      I1 => \icmp_reg_2509_reg[0]\,
      I2 => tmp_1_reg_2500,
      I3 => \ap_reg_pp0_iter2_reg_588_reg[7]\(0),
      O => \ram_reg_i_9__0_n_2\
    );
\ram_reg_i_9__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \p_p2_i_i_cast_cast_reg_2612_reg[10]\(3),
      I1 => ram_reg_i_13_n_2,
      I2 => \ImagLoc_x_reg_2592_reg[10]\(3),
      I3 => or_cond_i_i_reg_2607,
      I4 => p_assign_2_reg_2622(4),
      O => \^addrbwraddr\(3)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_ram_18 is
  port (
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC;
    \ap_CS_fsm_reg[4]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 10 downto 0 );
    ADDRBWRADDR : in STD_LOGIC_VECTOR ( 10 downto 0 );
    ap_enable_reg_pp0_iter3 : in STD_LOGIC;
    ap_block_pp0_stage0_subdone2_in : in STD_LOGIC;
    ap_reg_pp0_iter2_or_cond_i_i_reg_2607 : in STD_LOGIC;
    \icmp_reg_2509_reg[0]\ : in STD_LOGIC;
    tmp_1_reg_2500 : in STD_LOGIC;
    \tmp_116_0_1_reg_2518_reg[0]\ : in STD_LOGIC;
    \k_buf_0_val_3_load_reg_2703_reg[7]\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \ap_reg_pp0_iter2_reg_588_reg[7]\ : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_ram_18 : entity is "Filter2D_k_buf_0_eOg_ram";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_ram_18;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_ram_18 is
  signal k_buf_0_val_4_ce1 : STD_LOGIC;
  signal ram_reg_i_2_n_2 : STD_LOGIC;
  signal ram_reg_i_3_n_2 : STD_LOGIC;
  signal ram_reg_i_4_n_2 : STD_LOGIC;
  signal ram_reg_i_5_n_2 : STD_LOGIC;
  signal ram_reg_i_6_n_2 : STD_LOGIC;
  signal ram_reg_i_7_n_2 : STD_LOGIC;
  signal ram_reg_i_8_n_2 : STD_LOGIC;
  signal ram_reg_i_9_n_2 : STD_LOGIC;
  signal NLW_ram_reg_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 8 );
  signal NLW_ram_reg_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg : label is "p0_d8";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of ram_reg : label is "p0_d8";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg : label is 10240;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg : label is 7;
begin
ram_reg: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(13 downto 3) => Q(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 3) => ADDRBWRADDR(10 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      CLKARDCLK => ap_clk,
      CLKBWRCLK => ap_clk,
      DIADI(15 downto 8) => B"00000000",
      DIADI(7) => ram_reg_i_2_n_2,
      DIADI(6) => ram_reg_i_3_n_2,
      DIADI(5) => ram_reg_i_4_n_2,
      DIADI(4) => ram_reg_i_5_n_2,
      DIADI(3) => ram_reg_i_6_n_2,
      DIADI(2) => ram_reg_i_7_n_2,
      DIADI(1) => ram_reg_i_8_n_2,
      DIADI(0) => ram_reg_i_9_n_2,
      DIBDI(15 downto 0) => B"0000000011111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"00",
      DOADO(15 downto 0) => NLW_ram_reg_DOADO_UNCONNECTED(15 downto 0),
      DOBDO(15 downto 8) => NLW_ram_reg_DOBDO_UNCONNECTED(15 downto 8),
      DOBDO(7 downto 0) => D(7 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => k_buf_0_val_4_ce1,
      ENBWREN => \ap_CS_fsm_reg[4]\,
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => k_buf_0_val_4_ce1,
      WEA(0) => k_buf_0_val_4_ce1,
      WEBWE(3 downto 0) => B"0000"
    );
\ram_reg_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8080008080000000"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter3,
      I1 => ap_block_pp0_stage0_subdone2_in,
      I2 => ap_reg_pp0_iter2_or_cond_i_i_reg_2607,
      I3 => \icmp_reg_2509_reg[0]\,
      I4 => tmp_1_reg_2500,
      I5 => \tmp_116_0_1_reg_2518_reg[0]\,
      O => k_buf_0_val_4_ce1
    );
ram_reg_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \k_buf_0_val_3_load_reg_2703_reg[7]\(7),
      I1 => \icmp_reg_2509_reg[0]\,
      I2 => tmp_1_reg_2500,
      I3 => \ap_reg_pp0_iter2_reg_588_reg[7]\(7),
      O => ram_reg_i_2_n_2
    );
ram_reg_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \k_buf_0_val_3_load_reg_2703_reg[7]\(6),
      I1 => \icmp_reg_2509_reg[0]\,
      I2 => tmp_1_reg_2500,
      I3 => \ap_reg_pp0_iter2_reg_588_reg[7]\(6),
      O => ram_reg_i_3_n_2
    );
ram_reg_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \k_buf_0_val_3_load_reg_2703_reg[7]\(5),
      I1 => \icmp_reg_2509_reg[0]\,
      I2 => tmp_1_reg_2500,
      I3 => \ap_reg_pp0_iter2_reg_588_reg[7]\(5),
      O => ram_reg_i_4_n_2
    );
ram_reg_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \k_buf_0_val_3_load_reg_2703_reg[7]\(4),
      I1 => \icmp_reg_2509_reg[0]\,
      I2 => tmp_1_reg_2500,
      I3 => \ap_reg_pp0_iter2_reg_588_reg[7]\(4),
      O => ram_reg_i_5_n_2
    );
ram_reg_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \k_buf_0_val_3_load_reg_2703_reg[7]\(3),
      I1 => \icmp_reg_2509_reg[0]\,
      I2 => tmp_1_reg_2500,
      I3 => \ap_reg_pp0_iter2_reg_588_reg[7]\(3),
      O => ram_reg_i_6_n_2
    );
ram_reg_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \k_buf_0_val_3_load_reg_2703_reg[7]\(2),
      I1 => \icmp_reg_2509_reg[0]\,
      I2 => tmp_1_reg_2500,
      I3 => \ap_reg_pp0_iter2_reg_588_reg[7]\(2),
      O => ram_reg_i_7_n_2
    );
ram_reg_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \k_buf_0_val_3_load_reg_2703_reg[7]\(1),
      I1 => \icmp_reg_2509_reg[0]\,
      I2 => tmp_1_reg_2500,
      I3 => \ap_reg_pp0_iter2_reg_588_reg[7]\(1),
      O => ram_reg_i_8_n_2
    );
ram_reg_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \k_buf_0_val_3_load_reg_2703_reg[7]\(0),
      I1 => \icmp_reg_2509_reg[0]\,
      I2 => tmp_1_reg_2500,
      I3 => \ap_reg_pp0_iter2_reg_588_reg[7]\(0),
      O => ram_reg_i_9_n_2
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_ram_19 is
  port (
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ram_reg_0 : out STD_LOGIC;
    col_buf_0_val_0_0_reg_27080 : out STD_LOGIC;
    ap_block_pp0_stage0_subdone2_in : out STD_LOGIC;
    \ap_reg_pp0_iter2_reg_588_reg[0]\ : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    \k_buf_0_val_3_addr_reg_2649_reg[10]\ : in STD_LOGIC_VECTOR ( 10 downto 0 );
    ADDRBWRADDR : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_enable_reg_pp0_iter2 : in STD_LOGIC;
    ap_reg_pp0_iter1_or_cond_i_i_reg_2607 : in STD_LOGIC;
    \icmp_reg_2509_reg[0]\ : in STD_LOGIC;
    tmp_1_reg_2500 : in STD_LOGIC;
    \tmp_2_reg_2514_reg[0]\ : in STD_LOGIC;
    \ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg[0]\ : in STD_LOGIC;
    img1_data_stream_0_s_empty_n : in STD_LOGIC;
    img1_data_stream_2_s_empty_n : in STD_LOGIC;
    img1_data_stream_1_s_empty_n : in STD_LOGIC;
    ap_enable_reg_pp0_iter1 : in STD_LOGIC;
    ap_reg_pp0_iter4_or_cond_i_reg_2640 : in STD_LOGIC;
    ap_enable_reg_pp0_iter5_reg : in STD_LOGIC;
    img2_data_stream_0_s_full_n : in STD_LOGIC;
    img2_data_stream_1_s_full_n : in STD_LOGIC;
    img2_data_stream_2_s_full_n : in STD_LOGIC;
    \exitcond389_i_reg_2583_reg[0]\ : in STD_LOGIC;
    or_cond_i_i_reg_2607 : in STD_LOGIC;
    \ap_CS_fsm_reg[4]\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_ram_19 : entity is "Filter2D_k_buf_0_eOg_ram";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_ram_19;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_ram_19 is
  signal \^ap_block_pp0_stage0_subdone2_in\ : STD_LOGIC;
  signal ap_enable_reg_pp0_iter1_i_3_n_2 : STD_LOGIC;
  signal \^ap_reg_pp0_iter2_reg_588_reg[0]\ : STD_LOGIC;
  signal \^col_buf_0_val_0_0_reg_27080\ : STD_LOGIC;
  signal k_buf_0_val_3_ce1 : STD_LOGIC;
  signal \^ram_reg_0\ : STD_LOGIC;
  signal NLW_ram_reg_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_ram_reg_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 8 );
  signal NLW_ram_reg_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg : label is "p0_d8";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of ram_reg : label is "p0_d8";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg : label is 10240;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg : label is 2047;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg : label is 7;
begin
  ap_block_pp0_stage0_subdone2_in <= \^ap_block_pp0_stage0_subdone2_in\;
  \ap_reg_pp0_iter2_reg_588_reg[0]\ <= \^ap_reg_pp0_iter2_reg_588_reg[0]\;
  col_buf_0_val_0_0_reg_27080 <= \^col_buf_0_val_0_0_reg_27080\;
  ram_reg_0 <= \^ram_reg_0\;
\ap_enable_reg_pp0_iter1_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080FFFFFF"
    )
        port map (
      I0 => img1_data_stream_0_s_empty_n,
      I1 => img1_data_stream_2_s_empty_n,
      I2 => img1_data_stream_1_s_empty_n,
      I3 => ap_enable_reg_pp0_iter1,
      I4 => \^ap_reg_pp0_iter2_reg_588_reg[0]\,
      I5 => ap_enable_reg_pp0_iter1_i_3_n_2,
      O => \^ap_block_pp0_stage0_subdone2_in\
    );
ap_enable_reg_pp0_iter1_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4044"
    )
        port map (
      I0 => \exitcond389_i_reg_2583_reg[0]\,
      I1 => or_cond_i_i_reg_2607,
      I2 => tmp_1_reg_2500,
      I3 => \icmp_reg_2509_reg[0]\,
      O => \^ap_reg_pp0_iter2_reg_588_reg[0]\
    );
ap_enable_reg_pp0_iter1_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08888888"
    )
        port map (
      I0 => ap_reg_pp0_iter4_or_cond_i_reg_2640,
      I1 => ap_enable_reg_pp0_iter5_reg,
      I2 => img2_data_stream_0_s_full_n,
      I3 => img2_data_stream_1_s_full_n,
      I4 => img2_data_stream_2_s_full_n,
      O => ap_enable_reg_pp0_iter1_i_3_n_2
    );
\col_buf_0_val_1_0_reg_2721[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^ap_block_pp0_stage0_subdone2_in\,
      I1 => \ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg[0]\,
      O => \^col_buf_0_val_0_0_reg_27080\
    );
ram_reg: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 9,
      READ_WIDTH_B => 9,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 9,
      WRITE_WIDTH_B => 9
    )
        port map (
      ADDRARDADDR(13 downto 3) => \k_buf_0_val_3_addr_reg_2649_reg[10]\(10 downto 0),
      ADDRARDADDR(2 downto 0) => B"111",
      ADDRBWRADDR(13 downto 3) => ADDRBWRADDR(10 downto 0),
      ADDRBWRADDR(2 downto 0) => B"111",
      CLKARDCLK => ap_clk,
      CLKBWRCLK => ap_clk,
      DIADI(15 downto 8) => B"00000000",
      DIADI(7 downto 0) => Q(7 downto 0),
      DIBDI(15 downto 0) => B"0000000011111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"00",
      DOADO(15 downto 0) => NLW_ram_reg_DOADO_UNCONNECTED(15 downto 0),
      DOBDO(15 downto 8) => NLW_ram_reg_DOBDO_UNCONNECTED(15 downto 8),
      DOBDO(7 downto 0) => D(7 downto 0),
      DOPADOP(1 downto 0) => NLW_ram_reg_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_ram_reg_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => k_buf_0_val_3_ce1,
      ENBWREN => \^ram_reg_0\,
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => k_buf_0_val_3_ce1,
      WEA(0) => k_buf_0_val_3_ce1,
      WEBWE(3 downto 0) => B"0000"
    );
ram_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8080008080000000"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter2,
      I1 => \^col_buf_0_val_0_0_reg_27080\,
      I2 => ap_reg_pp0_iter1_or_cond_i_i_reg_2607,
      I3 => \icmp_reg_2509_reg[0]\,
      I4 => tmp_1_reg_2500,
      I5 => \tmp_2_reg_2514_reg[0]\,
      O => k_buf_0_val_3_ce1
    );
\ram_reg_i_2__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^ap_block_pp0_stage0_subdone2_in\,
      I1 => \ap_CS_fsm_reg[4]\(0),
      I2 => ap_enable_reg_pp0_iter1,
      O => \^ram_reg_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mat2AXIvideo is
  port (
    stream_out_TVALID : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \mOutPtr_reg[1]\ : out STD_LOGIC;
    \mOutPtr_reg[1]_0\ : out STD_LOGIC;
    stream_out_TUSER : out STD_LOGIC_VECTOR ( 0 to 0 );
    stream_out_TLAST : out STD_LOGIC_VECTOR ( 0 to 0 );
    stream_out_TDATA : out STD_LOGIC_VECTOR ( 23 downto 0 );
    ap_rst_n_inv : in STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    shiftReg_ce : in STD_LOGIC;
    Mat2AXIvideo_U0_ap_start : in STD_LOGIC;
    stream_out_TREADY : in STD_LOGIC;
    img3_data_stream_0_s_empty_n : in STD_LOGIC;
    img3_data_stream_2_s_empty_n : in STD_LOGIC;
    img3_data_stream_1_s_empty_n : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 23 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mat2AXIvideo;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mat2AXIvideo is
  signal AXI_video_strm_V_data_V_1_ack_in : STD_LOGIC;
  signal AXI_video_strm_V_data_V_1_load_A : STD_LOGIC;
  signal AXI_video_strm_V_data_V_1_load_B : STD_LOGIC;
  signal AXI_video_strm_V_data_V_1_payload_A : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal AXI_video_strm_V_data_V_1_payload_B : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal AXI_video_strm_V_data_V_1_sel : STD_LOGIC;
  signal AXI_video_strm_V_data_V_1_sel_rd_i_1_n_2 : STD_LOGIC;
  signal AXI_video_strm_V_data_V_1_sel_wr : STD_LOGIC;
  signal AXI_video_strm_V_data_V_1_sel_wr_i_1_n_2 : STD_LOGIC;
  signal AXI_video_strm_V_data_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_data_V_1_state[0]_i_1_n_2\ : STD_LOGIC;
  signal \AXI_video_strm_V_data_V_1_state_reg_n_2_[0]\ : STD_LOGIC;
  signal \AXI_video_strm_V_dest_V_1_state[0]_i_1_n_2\ : STD_LOGIC;
  signal \AXI_video_strm_V_dest_V_1_state[1]_i_1_n_2\ : STD_LOGIC;
  signal \AXI_video_strm_V_dest_V_1_state_reg_n_2_[1]\ : STD_LOGIC;
  signal \AXI_video_strm_V_id_V_1_state[0]_i_1_n_2\ : STD_LOGIC;
  signal \AXI_video_strm_V_id_V_1_state[1]_i_1_n_2\ : STD_LOGIC;
  signal \AXI_video_strm_V_id_V_1_state_reg_n_2_[0]\ : STD_LOGIC;
  signal \AXI_video_strm_V_id_V_1_state_reg_n_2_[1]\ : STD_LOGIC;
  signal \AXI_video_strm_V_keep_V_1_state[0]_i_1_n_2\ : STD_LOGIC;
  signal \AXI_video_strm_V_keep_V_1_state[1]_i_1_n_2\ : STD_LOGIC;
  signal \AXI_video_strm_V_keep_V_1_state_reg_n_2_[0]\ : STD_LOGIC;
  signal \AXI_video_strm_V_keep_V_1_state_reg_n_2_[1]\ : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_ack_in : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_payload_A : STD_LOGIC;
  signal \AXI_video_strm_V_last_V_1_payload_A[0]_i_1_n_2\ : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_payload_B : STD_LOGIC;
  signal \AXI_video_strm_V_last_V_1_payload_B[0]_i_1_n_2\ : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_sel : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_sel_rd_i_1_n_2 : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_sel_wr : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_sel_wr_i_1_n_2 : STD_LOGIC;
  signal AXI_video_strm_V_last_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_last_V_1_state[0]_i_1_n_2\ : STD_LOGIC;
  signal \AXI_video_strm_V_last_V_1_state_reg_n_2_[0]\ : STD_LOGIC;
  signal \AXI_video_strm_V_strb_V_1_state[0]_i_1_n_2\ : STD_LOGIC;
  signal \AXI_video_strm_V_strb_V_1_state[1]_i_1_n_2\ : STD_LOGIC;
  signal \AXI_video_strm_V_strb_V_1_state_reg_n_2_[0]\ : STD_LOGIC;
  signal \AXI_video_strm_V_strb_V_1_state_reg_n_2_[1]\ : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_ack_in : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_payload_A : STD_LOGIC;
  signal \AXI_video_strm_V_user_V_1_payload_A[0]_i_1_n_2\ : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_payload_B : STD_LOGIC;
  signal \AXI_video_strm_V_user_V_1_payload_B[0]_i_1_n_2\ : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_sel : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_sel_rd_i_1_n_2 : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_sel_wr : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_sel_wr_i_1_n_2 : STD_LOGIC;
  signal AXI_video_strm_V_user_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \AXI_video_strm_V_user_V_1_state[0]_i_1_n_2\ : STD_LOGIC;
  signal \AXI_video_strm_V_user_V_1_state_reg_n_2_[0]\ : STD_LOGIC;
  signal \ap_CS_fsm[0]_i_2__2_n_2\ : STD_LOGIC;
  signal \ap_CS_fsm[0]_i_3__0_n_2\ : STD_LOGIC;
  signal \ap_CS_fsm[1]_i_2__0_n_2\ : STD_LOGIC;
  signal \ap_CS_fsm[1]_i_3_n_2\ : STD_LOGIC;
  signal \ap_CS_fsm[2]_i_2__1_n_2\ : STD_LOGIC;
  signal \ap_CS_fsm[3]_i_2__2_n_2\ : STD_LOGIC;
  signal ap_CS_fsm_pp0_stage0 : STD_LOGIC;
  signal \ap_CS_fsm_reg_n_2_[0]\ : STD_LOGIC;
  signal ap_CS_fsm_state2 : STD_LOGIC;
  signal ap_CS_fsm_state6 : STD_LOGIC;
  signal ap_NS_fsm : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal ap_enable_reg_pp0_iter0 : STD_LOGIC;
  signal \ap_enable_reg_pp0_iter0_i_1__2_n_2\ : STD_LOGIC;
  signal \ap_enable_reg_pp0_iter1_i_1__1_n_2\ : STD_LOGIC;
  signal ap_enable_reg_pp0_iter1_reg_n_2 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter2_i_1_n_2 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter2_reg_n_2 : STD_LOGIC;
  signal ap_reg_pp0_iter1_exitcond_reg_270 : STD_LOGIC;
  signal \ap_reg_pp0_iter1_exitcond_reg_270[0]_i_1_n_2\ : STD_LOGIC;
  signal axi_last_V_reg_2790 : STD_LOGIC;
  signal \axi_last_V_reg_279[0]_i_1_n_2\ : STD_LOGIC;
  signal \axi_last_V_reg_279[0]_i_2_n_2\ : STD_LOGIC;
  signal \axi_last_V_reg_279_reg_n_2_[0]\ : STD_LOGIC;
  signal exitcond_fu_216_p2 : STD_LOGIC;
  signal \exitcond_reg_270[0]_i_1_n_2\ : STD_LOGIC;
  signal \exitcond_reg_270[0]_i_3_n_2\ : STD_LOGIC;
  signal \exitcond_reg_270[0]_i_4_n_2\ : STD_LOGIC;
  signal \exitcond_reg_270[0]_i_5_n_2\ : STD_LOGIC;
  signal \exitcond_reg_270[0]_i_6_n_2\ : STD_LOGIC;
  signal \exitcond_reg_270_reg_n_2_[0]\ : STD_LOGIC;
  signal i_V_fu_210_p2 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal i_V_reg_265 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal i_V_reg_2650 : STD_LOGIC;
  signal \i_V_reg_265[9]_i_3_n_2\ : STD_LOGIC;
  signal \i_V_reg_265[9]_i_4_n_2\ : STD_LOGIC;
  signal j_V_fu_222_p2 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \^moutptr_reg[1]\ : STD_LOGIC;
  signal \^stream_out_tvalid\ : STD_LOGIC;
  signal t_V_1_reg_188 : STD_LOGIC;
  signal t_V_1_reg_1880 : STD_LOGIC;
  signal \t_V_1_reg_188[10]_i_5_n_2\ : STD_LOGIC;
  signal \t_V_1_reg_188_reg__0\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal t_V_reg_177 : STD_LOGIC;
  signal \t_V_reg_177_reg_n_2_[0]\ : STD_LOGIC;
  signal \t_V_reg_177_reg_n_2_[1]\ : STD_LOGIC;
  signal \t_V_reg_177_reg_n_2_[2]\ : STD_LOGIC;
  signal \t_V_reg_177_reg_n_2_[3]\ : STD_LOGIC;
  signal \t_V_reg_177_reg_n_2_[4]\ : STD_LOGIC;
  signal \t_V_reg_177_reg_n_2_[5]\ : STD_LOGIC;
  signal \t_V_reg_177_reg_n_2_[6]\ : STD_LOGIC;
  signal \t_V_reg_177_reg_n_2_[7]\ : STD_LOGIC;
  signal \t_V_reg_177_reg_n_2_[8]\ : STD_LOGIC;
  signal \t_V_reg_177_reg_n_2_[9]\ : STD_LOGIC;
  signal tmp_user_V_fu_126 : STD_LOGIC;
  signal \tmp_user_V_fu_126[0]_i_1_n_2\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of AXI_video_strm_V_data_V_1_sel_rd_i_1 : label is "soft_lutpair102";
  attribute SOFT_HLUTNM of AXI_video_strm_V_data_V_1_sel_wr_i_1 : label is "soft_lutpair94";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_data_V_1_state[0]_i_1\ : label is "soft_lutpair89";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_data_V_1_state[1]_i_1\ : label is "soft_lutpair89";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_dest_V_1_state[0]_i_2\ : label is "soft_lutpair95";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_keep_V_1_state[1]_i_1\ : label is "soft_lutpair91";
  attribute SOFT_HLUTNM of AXI_video_strm_V_last_V_1_sel_rd_i_1 : label is "soft_lutpair112";
  attribute SOFT_HLUTNM of AXI_video_strm_V_last_V_1_sel_wr_i_1 : label is "soft_lutpair100";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_last_V_1_state[0]_i_1\ : label is "soft_lutpair90";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_last_V_1_state[1]_i_1\ : label is "soft_lutpair90";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_strb_V_1_state[1]_i_1\ : label is "soft_lutpair94";
  attribute SOFT_HLUTNM of AXI_video_strm_V_user_V_1_sel_rd_i_1 : label is "soft_lutpair112";
  attribute SOFT_HLUTNM of AXI_video_strm_V_user_V_1_sel_wr_i_1 : label is "soft_lutpair100";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_user_V_1_state[0]_i_1\ : label is "soft_lutpair92";
  attribute SOFT_HLUTNM of \AXI_video_strm_V_user_V_1_state[1]_i_1\ : label is "soft_lutpair92";
  attribute SOFT_HLUTNM of \ap_CS_fsm[0]_i_1__4\ : label is "soft_lutpair97";
  attribute SOFT_HLUTNM of \ap_CS_fsm[1]_i_3\ : label is "soft_lutpair93";
  attribute SOFT_HLUTNM of \ap_CS_fsm[2]_i_1__3\ : label is "soft_lutpair101";
  attribute SOFT_HLUTNM of \ap_CS_fsm[3]_i_1__2\ : label is "soft_lutpair101";
  attribute FSM_ENCODING : string;
  attribute FSM_ENCODING of \ap_CS_fsm_reg[0]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[1]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[2]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[3]\ : label is "none";
  attribute SOFT_HLUTNM of \ap_reg_pp0_iter1_exitcond_reg_270[0]_i_1\ : label is "soft_lutpair95";
  attribute SOFT_HLUTNM of \axi_last_V_reg_279[0]_i_2\ : label is "soft_lutpair98";
  attribute SOFT_HLUTNM of \exitcond_reg_270[0]_i_1\ : label is "soft_lutpair96";
  attribute SOFT_HLUTNM of \exitcond_reg_270[0]_i_4\ : label is "soft_lutpair88";
  attribute SOFT_HLUTNM of \i_V_reg_265[1]_i_1\ : label is "soft_lutpair103";
  attribute SOFT_HLUTNM of \i_V_reg_265[2]_i_1\ : label is "soft_lutpair103";
  attribute SOFT_HLUTNM of \i_V_reg_265[3]_i_1\ : label is "soft_lutpair86";
  attribute SOFT_HLUTNM of \i_V_reg_265[4]_i_1\ : label is "soft_lutpair86";
  attribute SOFT_HLUTNM of \i_V_reg_265[6]_i_1\ : label is "soft_lutpair99";
  attribute SOFT_HLUTNM of \i_V_reg_265[7]_i_1\ : label is "soft_lutpair99";
  attribute SOFT_HLUTNM of \i_V_reg_265[8]_i_1\ : label is "soft_lutpair87";
  attribute SOFT_HLUTNM of \i_V_reg_265[9]_i_2\ : label is "soft_lutpair87";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_1__10\ : label is "soft_lutpair91";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_2__5\ : label is "soft_lutpair97";
  attribute SOFT_HLUTNM of \stream_out_TDATA[0]_INST_0\ : label is "soft_lutpair104";
  attribute SOFT_HLUTNM of \stream_out_TDATA[10]_INST_0\ : label is "soft_lutpair111";
  attribute SOFT_HLUTNM of \stream_out_TDATA[11]_INST_0\ : label is "soft_lutpair113";
  attribute SOFT_HLUTNM of \stream_out_TDATA[12]_INST_0\ : label is "soft_lutpair113";
  attribute SOFT_HLUTNM of \stream_out_TDATA[13]_INST_0\ : label is "soft_lutpair114";
  attribute SOFT_HLUTNM of \stream_out_TDATA[14]_INST_0\ : label is "soft_lutpair115";
  attribute SOFT_HLUTNM of \stream_out_TDATA[16]_INST_0\ : label is "soft_lutpair115";
  attribute SOFT_HLUTNM of \stream_out_TDATA[17]_INST_0\ : label is "soft_lutpair114";
  attribute SOFT_HLUTNM of \stream_out_TDATA[18]_INST_0\ : label is "soft_lutpair111";
  attribute SOFT_HLUTNM of \stream_out_TDATA[19]_INST_0\ : label is "soft_lutpair110";
  attribute SOFT_HLUTNM of \stream_out_TDATA[1]_INST_0\ : label is "soft_lutpair105";
  attribute SOFT_HLUTNM of \stream_out_TDATA[20]_INST_0\ : label is "soft_lutpair109";
  attribute SOFT_HLUTNM of \stream_out_TDATA[21]_INST_0\ : label is "soft_lutpair107";
  attribute SOFT_HLUTNM of \stream_out_TDATA[22]_INST_0\ : label is "soft_lutpair108";
  attribute SOFT_HLUTNM of \stream_out_TDATA[23]_INST_0\ : label is "soft_lutpair102";
  attribute SOFT_HLUTNM of \stream_out_TDATA[2]_INST_0\ : label is "soft_lutpair106";
  attribute SOFT_HLUTNM of \stream_out_TDATA[3]_INST_0\ : label is "soft_lutpair106";
  attribute SOFT_HLUTNM of \stream_out_TDATA[4]_INST_0\ : label is "soft_lutpair104";
  attribute SOFT_HLUTNM of \stream_out_TDATA[5]_INST_0\ : label is "soft_lutpair105";
  attribute SOFT_HLUTNM of \stream_out_TDATA[6]_INST_0\ : label is "soft_lutpair107";
  attribute SOFT_HLUTNM of \stream_out_TDATA[7]_INST_0\ : label is "soft_lutpair108";
  attribute SOFT_HLUTNM of \stream_out_TDATA[8]_INST_0\ : label is "soft_lutpair109";
  attribute SOFT_HLUTNM of \stream_out_TDATA[9]_INST_0\ : label is "soft_lutpair110";
  attribute SOFT_HLUTNM of \t_V_1_reg_188[0]_i_1\ : label is "soft_lutpair116";
  attribute SOFT_HLUTNM of \t_V_1_reg_188[10]_i_4\ : label is "soft_lutpair96";
  attribute SOFT_HLUTNM of \t_V_1_reg_188[1]_i_1\ : label is "soft_lutpair116";
  attribute SOFT_HLUTNM of \t_V_1_reg_188[2]_i_1\ : label is "soft_lutpair88";
  attribute SOFT_HLUTNM of \t_V_1_reg_188[3]_i_1\ : label is "soft_lutpair85";
  attribute SOFT_HLUTNM of \t_V_1_reg_188[4]_i_1\ : label is "soft_lutpair85";
  attribute SOFT_HLUTNM of \t_V_1_reg_188[7]_i_1\ : label is "soft_lutpair98";
  attribute SOFT_HLUTNM of \t_V_1_reg_188[8]_i_1\ : label is "soft_lutpair84";
  attribute SOFT_HLUTNM of \t_V_1_reg_188[9]_i_1\ : label is "soft_lutpair84";
  attribute SOFT_HLUTNM of \tmp_user_V_fu_126[0]_i_1\ : label is "soft_lutpair93";
begin
  \mOutPtr_reg[1]\ <= \^moutptr_reg[1]\;
  stream_out_TVALID <= \^stream_out_tvalid\;
\AXI_video_strm_V_data_V_1_payload_A[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_sel_wr,
      I1 => AXI_video_strm_V_data_V_1_ack_in,
      I2 => \AXI_video_strm_V_data_V_1_state_reg_n_2_[0]\,
      O => AXI_video_strm_V_data_V_1_load_A
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(0),
      Q => AXI_video_strm_V_data_V_1_payload_A(0),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(10),
      Q => AXI_video_strm_V_data_V_1_payload_A(10),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(11),
      Q => AXI_video_strm_V_data_V_1_payload_A(11),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(12),
      Q => AXI_video_strm_V_data_V_1_payload_A(12),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(13),
      Q => AXI_video_strm_V_data_V_1_payload_A(13),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(14),
      Q => AXI_video_strm_V_data_V_1_payload_A(14),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(15),
      Q => AXI_video_strm_V_data_V_1_payload_A(15),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(16),
      Q => AXI_video_strm_V_data_V_1_payload_A(16),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(17),
      Q => AXI_video_strm_V_data_V_1_payload_A(17),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(18),
      Q => AXI_video_strm_V_data_V_1_payload_A(18),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(19),
      Q => AXI_video_strm_V_data_V_1_payload_A(19),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(1),
      Q => AXI_video_strm_V_data_V_1_payload_A(1),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(20),
      Q => AXI_video_strm_V_data_V_1_payload_A(20),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(21),
      Q => AXI_video_strm_V_data_V_1_payload_A(21),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(22),
      Q => AXI_video_strm_V_data_V_1_payload_A(22),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(23),
      Q => AXI_video_strm_V_data_V_1_payload_A(23),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(2),
      Q => AXI_video_strm_V_data_V_1_payload_A(2),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(3),
      Q => AXI_video_strm_V_data_V_1_payload_A(3),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(4),
      Q => AXI_video_strm_V_data_V_1_payload_A(4),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(5),
      Q => AXI_video_strm_V_data_V_1_payload_A(5),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(6),
      Q => AXI_video_strm_V_data_V_1_payload_A(6),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(7),
      Q => AXI_video_strm_V_data_V_1_payload_A(7),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(8),
      Q => AXI_video_strm_V_data_V_1_payload_A(8),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_A_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_A,
      D => D(9),
      Q => AXI_video_strm_V_data_V_1_payload_A(9),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_sel_wr,
      I1 => AXI_video_strm_V_data_V_1_ack_in,
      I2 => \AXI_video_strm_V_data_V_1_state_reg_n_2_[0]\,
      O => AXI_video_strm_V_data_V_1_load_B
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(0),
      Q => AXI_video_strm_V_data_V_1_payload_B(0),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(10),
      Q => AXI_video_strm_V_data_V_1_payload_B(10),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(11),
      Q => AXI_video_strm_V_data_V_1_payload_B(11),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(12),
      Q => AXI_video_strm_V_data_V_1_payload_B(12),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(13),
      Q => AXI_video_strm_V_data_V_1_payload_B(13),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(14),
      Q => AXI_video_strm_V_data_V_1_payload_B(14),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(15),
      Q => AXI_video_strm_V_data_V_1_payload_B(15),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(16),
      Q => AXI_video_strm_V_data_V_1_payload_B(16),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(17),
      Q => AXI_video_strm_V_data_V_1_payload_B(17),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(18),
      Q => AXI_video_strm_V_data_V_1_payload_B(18),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(19),
      Q => AXI_video_strm_V_data_V_1_payload_B(19),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(1),
      Q => AXI_video_strm_V_data_V_1_payload_B(1),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(20),
      Q => AXI_video_strm_V_data_V_1_payload_B(20),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(21),
      Q => AXI_video_strm_V_data_V_1_payload_B(21),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(22),
      Q => AXI_video_strm_V_data_V_1_payload_B(22),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(23),
      Q => AXI_video_strm_V_data_V_1_payload_B(23),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(2),
      Q => AXI_video_strm_V_data_V_1_payload_B(2),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(3),
      Q => AXI_video_strm_V_data_V_1_payload_B(3),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(4),
      Q => AXI_video_strm_V_data_V_1_payload_B(4),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(5),
      Q => AXI_video_strm_V_data_V_1_payload_B(5),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(6),
      Q => AXI_video_strm_V_data_V_1_payload_B(6),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(7),
      Q => AXI_video_strm_V_data_V_1_payload_B(7),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(8),
      Q => AXI_video_strm_V_data_V_1_payload_B(8),
      R => '0'
    );
\AXI_video_strm_V_data_V_1_payload_B_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => AXI_video_strm_V_data_V_1_load_B,
      D => D(9),
      Q => AXI_video_strm_V_data_V_1_payload_B(9),
      R => '0'
    );
AXI_video_strm_V_data_V_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \AXI_video_strm_V_data_V_1_state_reg_n_2_[0]\,
      I1 => stream_out_TREADY,
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => AXI_video_strm_V_data_V_1_sel_rd_i_1_n_2
    );
AXI_video_strm_V_data_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_data_V_1_sel_rd_i_1_n_2,
      Q => AXI_video_strm_V_data_V_1_sel,
      R => ap_rst_n_inv
    );
AXI_video_strm_V_data_V_1_sel_wr_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^moutptr_reg[1]\,
      I1 => AXI_video_strm_V_data_V_1_sel_wr,
      O => AXI_video_strm_V_data_V_1_sel_wr_i_1_n_2
    );
AXI_video_strm_V_data_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_data_V_1_sel_wr_i_1_n_2,
      Q => AXI_video_strm_V_data_V_1_sel_wr,
      R => ap_rst_n_inv
    );
\AXI_video_strm_V_data_V_1_state[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AEEE"
    )
        port map (
      I0 => \^moutptr_reg[1]\,
      I1 => \AXI_video_strm_V_data_V_1_state_reg_n_2_[0]\,
      I2 => stream_out_TREADY,
      I3 => AXI_video_strm_V_data_V_1_ack_in,
      O => \AXI_video_strm_V_data_V_1_state[0]_i_1_n_2\
    );
\AXI_video_strm_V_data_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BBFB"
    )
        port map (
      I0 => stream_out_TREADY,
      I1 => \AXI_video_strm_V_data_V_1_state_reg_n_2_[0]\,
      I2 => AXI_video_strm_V_data_V_1_ack_in,
      I3 => \^moutptr_reg[1]\,
      O => AXI_video_strm_V_data_V_1_state(1)
    );
\AXI_video_strm_V_data_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_data_V_1_state[0]_i_1_n_2\,
      Q => \AXI_video_strm_V_data_V_1_state_reg_n_2_[0]\,
      R => ap_rst_n_inv
    );
\AXI_video_strm_V_data_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_data_V_1_state(1),
      Q => AXI_video_strm_V_data_V_1_ack_in,
      R => ap_rst_n_inv
    );
\AXI_video_strm_V_dest_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B0F0A000"
    )
        port map (
      I0 => \^moutptr_reg[1]\,
      I1 => stream_out_TREADY,
      I2 => ap_rst_n,
      I3 => \AXI_video_strm_V_dest_V_1_state_reg_n_2_[1]\,
      I4 => \^stream_out_tvalid\,
      O => \AXI_video_strm_V_dest_V_1_state[0]_i_1_n_2\
    );
\AXI_video_strm_V_dest_V_1_state[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => \exitcond_reg_270[0]_i_3_n_2\,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => \exitcond_reg_270_reg_n_2_[0]\,
      I3 => ap_enable_reg_pp0_iter1_reg_n_2,
      O => \^moutptr_reg[1]\
    );
\AXI_video_strm_V_dest_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F4FF"
    )
        port map (
      I0 => \^moutptr_reg[1]\,
      I1 => \AXI_video_strm_V_dest_V_1_state_reg_n_2_[1]\,
      I2 => stream_out_TREADY,
      I3 => \^stream_out_tvalid\,
      O => \AXI_video_strm_V_dest_V_1_state[1]_i_1_n_2\
    );
\AXI_video_strm_V_dest_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_dest_V_1_state[0]_i_1_n_2\,
      Q => \^stream_out_tvalid\,
      R => '0'
    );
\AXI_video_strm_V_dest_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_dest_V_1_state[1]_i_1_n_2\,
      Q => \AXI_video_strm_V_dest_V_1_state_reg_n_2_[1]\,
      R => ap_rst_n_inv
    );
\AXI_video_strm_V_id_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B0F0A000"
    )
        port map (
      I0 => \^moutptr_reg[1]\,
      I1 => stream_out_TREADY,
      I2 => ap_rst_n,
      I3 => \AXI_video_strm_V_id_V_1_state_reg_n_2_[1]\,
      I4 => \AXI_video_strm_V_id_V_1_state_reg_n_2_[0]\,
      O => \AXI_video_strm_V_id_V_1_state[0]_i_1_n_2\
    );
\AXI_video_strm_V_id_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F4FF"
    )
        port map (
      I0 => \^moutptr_reg[1]\,
      I1 => \AXI_video_strm_V_id_V_1_state_reg_n_2_[1]\,
      I2 => stream_out_TREADY,
      I3 => \AXI_video_strm_V_id_V_1_state_reg_n_2_[0]\,
      O => \AXI_video_strm_V_id_V_1_state[1]_i_1_n_2\
    );
\AXI_video_strm_V_id_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_id_V_1_state[0]_i_1_n_2\,
      Q => \AXI_video_strm_V_id_V_1_state_reg_n_2_[0]\,
      R => '0'
    );
\AXI_video_strm_V_id_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_id_V_1_state[1]_i_1_n_2\,
      Q => \AXI_video_strm_V_id_V_1_state_reg_n_2_[1]\,
      R => ap_rst_n_inv
    );
\AXI_video_strm_V_keep_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B0F0A000"
    )
        port map (
      I0 => \^moutptr_reg[1]\,
      I1 => stream_out_TREADY,
      I2 => ap_rst_n,
      I3 => \AXI_video_strm_V_keep_V_1_state_reg_n_2_[1]\,
      I4 => \AXI_video_strm_V_keep_V_1_state_reg_n_2_[0]\,
      O => \AXI_video_strm_V_keep_V_1_state[0]_i_1_n_2\
    );
\AXI_video_strm_V_keep_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F4FF"
    )
        port map (
      I0 => \^moutptr_reg[1]\,
      I1 => \AXI_video_strm_V_keep_V_1_state_reg_n_2_[1]\,
      I2 => stream_out_TREADY,
      I3 => \AXI_video_strm_V_keep_V_1_state_reg_n_2_[0]\,
      O => \AXI_video_strm_V_keep_V_1_state[1]_i_1_n_2\
    );
\AXI_video_strm_V_keep_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_keep_V_1_state[0]_i_1_n_2\,
      Q => \AXI_video_strm_V_keep_V_1_state_reg_n_2_[0]\,
      R => '0'
    );
\AXI_video_strm_V_keep_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_keep_V_1_state[1]_i_1_n_2\,
      Q => \AXI_video_strm_V_keep_V_1_state_reg_n_2_[1]\,
      R => ap_rst_n_inv
    );
\AXI_video_strm_V_last_V_1_payload_A[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEE2022"
    )
        port map (
      I0 => \axi_last_V_reg_279_reg_n_2_[0]\,
      I1 => AXI_video_strm_V_last_V_1_sel_wr,
      I2 => AXI_video_strm_V_last_V_1_ack_in,
      I3 => \AXI_video_strm_V_last_V_1_state_reg_n_2_[0]\,
      I4 => AXI_video_strm_V_last_V_1_payload_A,
      O => \AXI_video_strm_V_last_V_1_payload_A[0]_i_1_n_2\
    );
\AXI_video_strm_V_last_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_last_V_1_payload_A[0]_i_1_n_2\,
      Q => AXI_video_strm_V_last_V_1_payload_A,
      R => '0'
    );
\AXI_video_strm_V_last_V_1_payload_B[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFBB8088"
    )
        port map (
      I0 => \axi_last_V_reg_279_reg_n_2_[0]\,
      I1 => AXI_video_strm_V_last_V_1_sel_wr,
      I2 => AXI_video_strm_V_last_V_1_ack_in,
      I3 => \AXI_video_strm_V_last_V_1_state_reg_n_2_[0]\,
      I4 => AXI_video_strm_V_last_V_1_payload_B,
      O => \AXI_video_strm_V_last_V_1_payload_B[0]_i_1_n_2\
    );
\AXI_video_strm_V_last_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_last_V_1_payload_B[0]_i_1_n_2\,
      Q => AXI_video_strm_V_last_V_1_payload_B,
      R => '0'
    );
AXI_video_strm_V_last_V_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \AXI_video_strm_V_last_V_1_state_reg_n_2_[0]\,
      I1 => stream_out_TREADY,
      I2 => AXI_video_strm_V_last_V_1_sel,
      O => AXI_video_strm_V_last_V_1_sel_rd_i_1_n_2
    );
AXI_video_strm_V_last_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_last_V_1_sel_rd_i_1_n_2,
      Q => AXI_video_strm_V_last_V_1_sel,
      R => ap_rst_n_inv
    );
AXI_video_strm_V_last_V_1_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^moutptr_reg[1]\,
      I1 => AXI_video_strm_V_last_V_1_ack_in,
      I2 => AXI_video_strm_V_last_V_1_sel_wr,
      O => AXI_video_strm_V_last_V_1_sel_wr_i_1_n_2
    );
AXI_video_strm_V_last_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_last_V_1_sel_wr_i_1_n_2,
      Q => AXI_video_strm_V_last_V_1_sel_wr,
      R => ap_rst_n_inv
    );
\AXI_video_strm_V_last_V_1_state[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AECC"
    )
        port map (
      I0 => \^moutptr_reg[1]\,
      I1 => \AXI_video_strm_V_last_V_1_state_reg_n_2_[0]\,
      I2 => stream_out_TREADY,
      I3 => AXI_video_strm_V_last_V_1_ack_in,
      O => \AXI_video_strm_V_last_V_1_state[0]_i_1_n_2\
    );
\AXI_video_strm_V_last_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BBFB"
    )
        port map (
      I0 => stream_out_TREADY,
      I1 => \AXI_video_strm_V_last_V_1_state_reg_n_2_[0]\,
      I2 => AXI_video_strm_V_last_V_1_ack_in,
      I3 => \^moutptr_reg[1]\,
      O => AXI_video_strm_V_last_V_1_state(1)
    );
\AXI_video_strm_V_last_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_last_V_1_state[0]_i_1_n_2\,
      Q => \AXI_video_strm_V_last_V_1_state_reg_n_2_[0]\,
      R => ap_rst_n_inv
    );
\AXI_video_strm_V_last_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_last_V_1_state(1),
      Q => AXI_video_strm_V_last_V_1_ack_in,
      R => ap_rst_n_inv
    );
\AXI_video_strm_V_strb_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B0F0A000"
    )
        port map (
      I0 => \^moutptr_reg[1]\,
      I1 => stream_out_TREADY,
      I2 => ap_rst_n,
      I3 => \AXI_video_strm_V_strb_V_1_state_reg_n_2_[1]\,
      I4 => \AXI_video_strm_V_strb_V_1_state_reg_n_2_[0]\,
      O => \AXI_video_strm_V_strb_V_1_state[0]_i_1_n_2\
    );
\AXI_video_strm_V_strb_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F4FF"
    )
        port map (
      I0 => \^moutptr_reg[1]\,
      I1 => \AXI_video_strm_V_strb_V_1_state_reg_n_2_[1]\,
      I2 => stream_out_TREADY,
      I3 => \AXI_video_strm_V_strb_V_1_state_reg_n_2_[0]\,
      O => \AXI_video_strm_V_strb_V_1_state[1]_i_1_n_2\
    );
\AXI_video_strm_V_strb_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_strb_V_1_state[0]_i_1_n_2\,
      Q => \AXI_video_strm_V_strb_V_1_state_reg_n_2_[0]\,
      R => '0'
    );
\AXI_video_strm_V_strb_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_strb_V_1_state[1]_i_1_n_2\,
      Q => \AXI_video_strm_V_strb_V_1_state_reg_n_2_[1]\,
      R => ap_rst_n_inv
    );
\AXI_video_strm_V_user_V_1_payload_A[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEE2022"
    )
        port map (
      I0 => tmp_user_V_fu_126,
      I1 => AXI_video_strm_V_user_V_1_sel_wr,
      I2 => AXI_video_strm_V_user_V_1_ack_in,
      I3 => \AXI_video_strm_V_user_V_1_state_reg_n_2_[0]\,
      I4 => AXI_video_strm_V_user_V_1_payload_A,
      O => \AXI_video_strm_V_user_V_1_payload_A[0]_i_1_n_2\
    );
\AXI_video_strm_V_user_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_user_V_1_payload_A[0]_i_1_n_2\,
      Q => AXI_video_strm_V_user_V_1_payload_A,
      R => '0'
    );
\AXI_video_strm_V_user_V_1_payload_B[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFBB8088"
    )
        port map (
      I0 => tmp_user_V_fu_126,
      I1 => AXI_video_strm_V_user_V_1_sel_wr,
      I2 => AXI_video_strm_V_user_V_1_ack_in,
      I3 => \AXI_video_strm_V_user_V_1_state_reg_n_2_[0]\,
      I4 => AXI_video_strm_V_user_V_1_payload_B,
      O => \AXI_video_strm_V_user_V_1_payload_B[0]_i_1_n_2\
    );
\AXI_video_strm_V_user_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_user_V_1_payload_B[0]_i_1_n_2\,
      Q => AXI_video_strm_V_user_V_1_payload_B,
      R => '0'
    );
AXI_video_strm_V_user_V_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \AXI_video_strm_V_user_V_1_state_reg_n_2_[0]\,
      I1 => stream_out_TREADY,
      I2 => AXI_video_strm_V_user_V_1_sel,
      O => AXI_video_strm_V_user_V_1_sel_rd_i_1_n_2
    );
AXI_video_strm_V_user_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_user_V_1_sel_rd_i_1_n_2,
      Q => AXI_video_strm_V_user_V_1_sel,
      R => ap_rst_n_inv
    );
AXI_video_strm_V_user_V_1_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^moutptr_reg[1]\,
      I1 => AXI_video_strm_V_user_V_1_ack_in,
      I2 => AXI_video_strm_V_user_V_1_sel_wr,
      O => AXI_video_strm_V_user_V_1_sel_wr_i_1_n_2
    );
AXI_video_strm_V_user_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_user_V_1_sel_wr_i_1_n_2,
      Q => AXI_video_strm_V_user_V_1_sel_wr,
      R => ap_rst_n_inv
    );
\AXI_video_strm_V_user_V_1_state[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AECC"
    )
        port map (
      I0 => \^moutptr_reg[1]\,
      I1 => \AXI_video_strm_V_user_V_1_state_reg_n_2_[0]\,
      I2 => stream_out_TREADY,
      I3 => AXI_video_strm_V_user_V_1_ack_in,
      O => \AXI_video_strm_V_user_V_1_state[0]_i_1_n_2\
    );
\AXI_video_strm_V_user_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BBFB"
    )
        port map (
      I0 => stream_out_TREADY,
      I1 => \AXI_video_strm_V_user_V_1_state_reg_n_2_[0]\,
      I2 => AXI_video_strm_V_user_V_1_ack_in,
      I3 => \^moutptr_reg[1]\,
      O => AXI_video_strm_V_user_V_1_state(1)
    );
\AXI_video_strm_V_user_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \AXI_video_strm_V_user_V_1_state[0]_i_1_n_2\,
      Q => \AXI_video_strm_V_user_V_1_state_reg_n_2_[0]\,
      R => ap_rst_n_inv
    );
\AXI_video_strm_V_user_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => AXI_video_strm_V_user_V_1_state(1),
      Q => AXI_video_strm_V_user_V_1_ack_in,
      R => ap_rst_n_inv
    );
\ap_CS_fsm[0]_i_1__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44F4"
    )
        port map (
      I0 => \ap_CS_fsm[0]_i_2__2_n_2\,
      I1 => i_V_reg_2650,
      I2 => \ap_CS_fsm_reg_n_2_[0]\,
      I3 => Mat2AXIvideo_U0_ap_start,
      O => ap_NS_fsm(0)
    );
\ap_CS_fsm[0]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFBFF"
    )
        port map (
      I0 => \t_V_reg_177_reg_n_2_[8]\,
      I1 => \t_V_reg_177_reg_n_2_[4]\,
      I2 => \t_V_reg_177_reg_n_2_[3]\,
      I3 => \t_V_reg_177_reg_n_2_[9]\,
      I4 => \ap_CS_fsm[0]_i_3__0_n_2\,
      O => \ap_CS_fsm[0]_i_2__2_n_2\
    );
\ap_CS_fsm[0]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEFFFFFFFFFF"
    )
        port map (
      I0 => \t_V_reg_177_reg_n_2_[1]\,
      I1 => \t_V_reg_177_reg_n_2_[0]\,
      I2 => \t_V_reg_177_reg_n_2_[5]\,
      I3 => \t_V_reg_177_reg_n_2_[6]\,
      I4 => \t_V_reg_177_reg_n_2_[2]\,
      I5 => \t_V_reg_177_reg_n_2_[7]\,
      O => \ap_CS_fsm[0]_i_3__0_n_2\
    );
\ap_CS_fsm[1]_i_1__4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFF007F00"
    )
        port map (
      I0 => \AXI_video_strm_V_strb_V_1_state_reg_n_2_[1]\,
      I1 => \AXI_video_strm_V_keep_V_1_state_reg_n_2_[1]\,
      I2 => AXI_video_strm_V_user_V_1_ack_in,
      I3 => ap_CS_fsm_state2,
      I4 => \ap_CS_fsm[1]_i_2__0_n_2\,
      I5 => \ap_CS_fsm[1]_i_3_n_2\,
      O => ap_NS_fsm(1)
    );
\ap_CS_fsm[1]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => AXI_video_strm_V_last_V_1_ack_in,
      I1 => \AXI_video_strm_V_id_V_1_state_reg_n_2_[1]\,
      I2 => \AXI_video_strm_V_dest_V_1_state_reg_n_2_[1]\,
      I3 => AXI_video_strm_V_data_V_1_ack_in,
      O => \ap_CS_fsm[1]_i_2__0_n_2\
    );
\ap_CS_fsm[1]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => ap_CS_fsm_state6,
      I1 => \ap_CS_fsm_reg_n_2_[0]\,
      I2 => Mat2AXIvideo_U0_ap_start,
      O => \ap_CS_fsm[1]_i_3_n_2\
    );
\ap_CS_fsm[2]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \ap_CS_fsm[2]_i_2__1_n_2\,
      I1 => \ap_CS_fsm[3]_i_2__2_n_2\,
      I2 => ap_CS_fsm_pp0_stage0,
      O => ap_NS_fsm(2)
    );
\ap_CS_fsm[2]_i_2__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ap_CS_fsm[0]_i_2__2_n_2\,
      I1 => i_V_reg_2650,
      O => \ap_CS_fsm[2]_i_2__1_n_2\
    );
\ap_CS_fsm[3]_i_1__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ap_CS_fsm_pp0_stage0,
      I1 => \ap_CS_fsm[3]_i_2__2_n_2\,
      O => ap_NS_fsm(3)
    );
\ap_CS_fsm[3]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"11101010"
    )
        port map (
      I0 => \exitcond_reg_270[0]_i_3_n_2\,
      I1 => ap_enable_reg_pp0_iter1_reg_n_2,
      I2 => ap_enable_reg_pp0_iter2_reg_n_2,
      I3 => ap_enable_reg_pp0_iter0,
      I4 => exitcond_fu_216_p2,
      O => \ap_CS_fsm[3]_i_2__2_n_2\
    );
\ap_CS_fsm_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(0),
      Q => \ap_CS_fsm_reg_n_2_[0]\,
      S => ap_rst_n_inv
    );
\ap_CS_fsm_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(1),
      Q => ap_CS_fsm_state2,
      R => ap_rst_n_inv
    );
\ap_CS_fsm_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(2),
      Q => ap_CS_fsm_pp0_stage0,
      R => ap_rst_n_inv
    );
\ap_CS_fsm_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(3),
      Q => ap_CS_fsm_state6,
      R => ap_rst_n_inv
    );
\ap_enable_reg_pp0_iter0_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B0B0B000F0F0F000"
    )
        port map (
      I0 => \exitcond_reg_270[0]_i_3_n_2\,
      I1 => exitcond_fu_216_p2,
      I2 => ap_rst_n,
      I3 => \ap_CS_fsm[2]_i_2__1_n_2\,
      I4 => ap_enable_reg_pp0_iter0,
      I5 => ap_CS_fsm_pp0_stage0,
      O => \ap_enable_reg_pp0_iter0_i_1__2_n_2\
    );
ap_enable_reg_pp0_iter0_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \ap_enable_reg_pp0_iter0_i_1__2_n_2\,
      Q => ap_enable_reg_pp0_iter0,
      R => '0'
    );
\ap_enable_reg_pp0_iter1_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C044C000"
    )
        port map (
      I0 => exitcond_fu_216_p2,
      I1 => ap_rst_n,
      I2 => ap_enable_reg_pp0_iter1_reg_n_2,
      I3 => \exitcond_reg_270[0]_i_3_n_2\,
      I4 => ap_enable_reg_pp0_iter0,
      O => \ap_enable_reg_pp0_iter1_i_1__1_n_2\
    );
ap_enable_reg_pp0_iter1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \ap_enable_reg_pp0_iter1_i_1__1_n_2\,
      Q => ap_enable_reg_pp0_iter1_reg_n_2,
      R => '0'
    );
ap_enable_reg_pp0_iter2_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"40CC4000"
    )
        port map (
      I0 => \ap_CS_fsm[2]_i_2__1_n_2\,
      I1 => ap_rst_n,
      I2 => ap_enable_reg_pp0_iter2_reg_n_2,
      I3 => \exitcond_reg_270[0]_i_3_n_2\,
      I4 => ap_enable_reg_pp0_iter1_reg_n_2,
      O => ap_enable_reg_pp0_iter2_i_1_n_2
    );
ap_enable_reg_pp0_iter2_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp0_iter2_i_1_n_2,
      Q => ap_enable_reg_pp0_iter2_reg_n_2,
      R => '0'
    );
\ap_reg_pp0_iter1_exitcond_reg_270[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \exitcond_reg_270_reg_n_2_[0]\,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => \exitcond_reg_270[0]_i_3_n_2\,
      I3 => ap_reg_pp0_iter1_exitcond_reg_270,
      O => \ap_reg_pp0_iter1_exitcond_reg_270[0]_i_1_n_2\
    );
\ap_reg_pp0_iter1_exitcond_reg_270_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \ap_reg_pp0_iter1_exitcond_reg_270[0]_i_1_n_2\,
      Q => ap_reg_pp0_iter1_exitcond_reg_270,
      R => '0'
    );
\axi_last_V_reg_279[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000C0000AAAAAAAA"
    )
        port map (
      I0 => \axi_last_V_reg_279_reg_n_2_[0]\,
      I1 => \axi_last_V_reg_279[0]_i_2_n_2\,
      I2 => \t_V_1_reg_188_reg__0\(8),
      I3 => \t_V_1_reg_188_reg__0\(9),
      I4 => \t_V_1_reg_188_reg__0\(10),
      I5 => axi_last_V_reg_2790,
      O => \axi_last_V_reg_279[0]_i_1_n_2\
    );
\axi_last_V_reg_279[0]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \t_V_1_reg_188_reg__0\(7),
      I1 => \t_V_1_reg_188[10]_i_5_n_2\,
      I2 => \t_V_1_reg_188_reg__0\(6),
      O => \axi_last_V_reg_279[0]_i_2_n_2\
    );
\axi_last_V_reg_279_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \axi_last_V_reg_279[0]_i_1_n_2\,
      Q => \axi_last_V_reg_279_reg_n_2_[0]\,
      R => '0'
    );
\exitcond_reg_270[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => exitcond_fu_216_p2,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => \exitcond_reg_270[0]_i_3_n_2\,
      I3 => \exitcond_reg_270_reg_n_2_[0]\,
      O => \exitcond_reg_270[0]_i_1_n_2\
    );
\exitcond_reg_270[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => \t_V_1_reg_188_reg__0\(6),
      I1 => \t_V_1_reg_188_reg__0\(7),
      I2 => \t_V_1_reg_188_reg__0\(9),
      I3 => \exitcond_reg_270[0]_i_4_n_2\,
      I4 => \exitcond_reg_270[0]_i_5_n_2\,
      O => exitcond_fu_216_p2
    );
\exitcond_reg_270[0]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00007FFF"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_ack_in,
      I1 => img3_data_stream_0_s_empty_n,
      I2 => img3_data_stream_2_s_empty_n,
      I3 => img3_data_stream_1_s_empty_n,
      I4 => \exitcond_reg_270[0]_i_6_n_2\,
      O => \exitcond_reg_270[0]_i_3_n_2\
    );
\exitcond_reg_270[0]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => \t_V_1_reg_188_reg__0\(1),
      I1 => \t_V_1_reg_188_reg__0\(0),
      I2 => \t_V_1_reg_188_reg__0\(10),
      I3 => \t_V_1_reg_188_reg__0\(5),
      O => \exitcond_reg_270[0]_i_4_n_2\
    );
\exitcond_reg_270[0]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => \t_V_1_reg_188_reg__0\(3),
      I1 => \t_V_1_reg_188_reg__0\(2),
      I2 => \t_V_1_reg_188_reg__0\(8),
      I3 => \t_V_1_reg_188_reg__0\(4),
      O => \exitcond_reg_270[0]_i_5_n_2\
    );
\exitcond_reg_270[0]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBBBB0BB"
    )
        port map (
      I0 => \exitcond_reg_270_reg_n_2_[0]\,
      I1 => ap_enable_reg_pp0_iter1_reg_n_2,
      I2 => AXI_video_strm_V_data_V_1_ack_in,
      I3 => ap_enable_reg_pp0_iter2_reg_n_2,
      I4 => ap_reg_pp0_iter1_exitcond_reg_270,
      O => \exitcond_reg_270[0]_i_6_n_2\
    );
\exitcond_reg_270_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \exitcond_reg_270[0]_i_1_n_2\,
      Q => \exitcond_reg_270_reg_n_2_[0]\,
      R => '0'
    );
\i_V_reg_265[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \t_V_reg_177_reg_n_2_[0]\,
      O => i_V_fu_210_p2(0)
    );
\i_V_reg_265[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \t_V_reg_177_reg_n_2_[0]\,
      I1 => \t_V_reg_177_reg_n_2_[1]\,
      O => i_V_fu_210_p2(1)
    );
\i_V_reg_265[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \t_V_reg_177_reg_n_2_[2]\,
      I1 => \t_V_reg_177_reg_n_2_[1]\,
      I2 => \t_V_reg_177_reg_n_2_[0]\,
      O => i_V_fu_210_p2(2)
    );
\i_V_reg_265[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \t_V_reg_177_reg_n_2_[3]\,
      I1 => \t_V_reg_177_reg_n_2_[0]\,
      I2 => \t_V_reg_177_reg_n_2_[1]\,
      I3 => \t_V_reg_177_reg_n_2_[2]\,
      O => i_V_fu_210_p2(3)
    );
\i_V_reg_265[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \t_V_reg_177_reg_n_2_[4]\,
      I1 => \t_V_reg_177_reg_n_2_[2]\,
      I2 => \t_V_reg_177_reg_n_2_[1]\,
      I3 => \t_V_reg_177_reg_n_2_[0]\,
      I4 => \t_V_reg_177_reg_n_2_[3]\,
      O => i_V_fu_210_p2(4)
    );
\i_V_reg_265[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \t_V_reg_177_reg_n_2_[5]\,
      I1 => \t_V_reg_177_reg_n_2_[3]\,
      I2 => \t_V_reg_177_reg_n_2_[0]\,
      I3 => \t_V_reg_177_reg_n_2_[1]\,
      I4 => \t_V_reg_177_reg_n_2_[2]\,
      I5 => \t_V_reg_177_reg_n_2_[4]\,
      O => i_V_fu_210_p2(5)
    );
\i_V_reg_265[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \t_V_reg_177_reg_n_2_[6]\,
      I1 => \i_V_reg_265[9]_i_4_n_2\,
      O => i_V_fu_210_p2(6)
    );
\i_V_reg_265[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \t_V_reg_177_reg_n_2_[7]\,
      I1 => \i_V_reg_265[9]_i_4_n_2\,
      I2 => \t_V_reg_177_reg_n_2_[6]\,
      O => i_V_fu_210_p2(7)
    );
\i_V_reg_265[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \t_V_reg_177_reg_n_2_[8]\,
      I1 => \t_V_reg_177_reg_n_2_[6]\,
      I2 => \i_V_reg_265[9]_i_4_n_2\,
      I3 => \t_V_reg_177_reg_n_2_[7]\,
      O => i_V_fu_210_p2(8)
    );
\i_V_reg_265[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00008000"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_ack_in,
      I1 => \AXI_video_strm_V_keep_V_1_state_reg_n_2_[1]\,
      I2 => AXI_video_strm_V_user_V_1_ack_in,
      I3 => \AXI_video_strm_V_dest_V_1_state_reg_n_2_[1]\,
      I4 => \i_V_reg_265[9]_i_3_n_2\,
      O => i_V_reg_2650
    );
\i_V_reg_265[9]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \t_V_reg_177_reg_n_2_[9]\,
      I1 => \t_V_reg_177_reg_n_2_[7]\,
      I2 => \i_V_reg_265[9]_i_4_n_2\,
      I3 => \t_V_reg_177_reg_n_2_[6]\,
      I4 => \t_V_reg_177_reg_n_2_[8]\,
      O => i_V_fu_210_p2(9)
    );
\i_V_reg_265[9]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => ap_CS_fsm_state2,
      I1 => AXI_video_strm_V_last_V_1_ack_in,
      I2 => \AXI_video_strm_V_strb_V_1_state_reg_n_2_[1]\,
      I3 => \AXI_video_strm_V_id_V_1_state_reg_n_2_[1]\,
      O => \i_V_reg_265[9]_i_3_n_2\
    );
\i_V_reg_265[9]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \t_V_reg_177_reg_n_2_[5]\,
      I1 => \t_V_reg_177_reg_n_2_[3]\,
      I2 => \t_V_reg_177_reg_n_2_[0]\,
      I3 => \t_V_reg_177_reg_n_2_[1]\,
      I4 => \t_V_reg_177_reg_n_2_[2]\,
      I5 => \t_V_reg_177_reg_n_2_[4]\,
      O => \i_V_reg_265[9]_i_4_n_2\
    );
\i_V_reg_265_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2650,
      D => i_V_fu_210_p2(0),
      Q => i_V_reg_265(0),
      R => '0'
    );
\i_V_reg_265_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2650,
      D => i_V_fu_210_p2(1),
      Q => i_V_reg_265(1),
      R => '0'
    );
\i_V_reg_265_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2650,
      D => i_V_fu_210_p2(2),
      Q => i_V_reg_265(2),
      R => '0'
    );
\i_V_reg_265_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2650,
      D => i_V_fu_210_p2(3),
      Q => i_V_reg_265(3),
      R => '0'
    );
\i_V_reg_265_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2650,
      D => i_V_fu_210_p2(4),
      Q => i_V_reg_265(4),
      R => '0'
    );
\i_V_reg_265_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2650,
      D => i_V_fu_210_p2(5),
      Q => i_V_reg_265(5),
      R => '0'
    );
\i_V_reg_265_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2650,
      D => i_V_fu_210_p2(6),
      Q => i_V_reg_265(6),
      R => '0'
    );
\i_V_reg_265_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2650,
      D => i_V_fu_210_p2(7),
      Q => i_V_reg_265(7),
      R => '0'
    );
\i_V_reg_265_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2650,
      D => i_V_fu_210_p2(8),
      Q => i_V_reg_265(8),
      R => '0'
    );
\i_V_reg_265_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_V_reg_2650,
      D => i_V_fu_210_p2(9),
      Q => i_V_reg_265(9),
      R => '0'
    );
\mOutPtr[1]_i_1__10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^moutptr_reg[1]\,
      I1 => shiftReg_ce,
      O => E(0)
    );
\mOutPtr[1]_i_2__5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => Mat2AXIvideo_U0_ap_start,
      I1 => i_V_reg_2650,
      I2 => \ap_CS_fsm[0]_i_2__2_n_2\,
      O => \mOutPtr_reg[1]_0\
    );
\stream_out_TDATA[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(0),
      I1 => AXI_video_strm_V_data_V_1_payload_A(0),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(0)
    );
\stream_out_TDATA[10]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(10),
      I1 => AXI_video_strm_V_data_V_1_payload_A(10),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(10)
    );
\stream_out_TDATA[11]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(11),
      I1 => AXI_video_strm_V_data_V_1_payload_A(11),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(11)
    );
\stream_out_TDATA[12]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(12),
      I1 => AXI_video_strm_V_data_V_1_payload_A(12),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(12)
    );
\stream_out_TDATA[13]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(13),
      I1 => AXI_video_strm_V_data_V_1_payload_A(13),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(13)
    );
\stream_out_TDATA[14]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(14),
      I1 => AXI_video_strm_V_data_V_1_payload_A(14),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(14)
    );
\stream_out_TDATA[15]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(15),
      I1 => AXI_video_strm_V_data_V_1_payload_A(15),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(15)
    );
\stream_out_TDATA[16]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(16),
      I1 => AXI_video_strm_V_data_V_1_payload_A(16),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(16)
    );
\stream_out_TDATA[17]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(17),
      I1 => AXI_video_strm_V_data_V_1_payload_A(17),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(17)
    );
\stream_out_TDATA[18]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(18),
      I1 => AXI_video_strm_V_data_V_1_payload_A(18),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(18)
    );
\stream_out_TDATA[19]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(19),
      I1 => AXI_video_strm_V_data_V_1_payload_A(19),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(19)
    );
\stream_out_TDATA[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(1),
      I1 => AXI_video_strm_V_data_V_1_payload_A(1),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(1)
    );
\stream_out_TDATA[20]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(20),
      I1 => AXI_video_strm_V_data_V_1_payload_A(20),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(20)
    );
\stream_out_TDATA[21]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(21),
      I1 => AXI_video_strm_V_data_V_1_payload_A(21),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(21)
    );
\stream_out_TDATA[22]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(22),
      I1 => AXI_video_strm_V_data_V_1_payload_A(22),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(22)
    );
\stream_out_TDATA[23]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(23),
      I1 => AXI_video_strm_V_data_V_1_payload_A(23),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(23)
    );
\stream_out_TDATA[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(2),
      I1 => AXI_video_strm_V_data_V_1_payload_A(2),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(2)
    );
\stream_out_TDATA[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(3),
      I1 => AXI_video_strm_V_data_V_1_payload_A(3),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(3)
    );
\stream_out_TDATA[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(4),
      I1 => AXI_video_strm_V_data_V_1_payload_A(4),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(4)
    );
\stream_out_TDATA[5]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(5),
      I1 => AXI_video_strm_V_data_V_1_payload_A(5),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(5)
    );
\stream_out_TDATA[6]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(6),
      I1 => AXI_video_strm_V_data_V_1_payload_A(6),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(6)
    );
\stream_out_TDATA[7]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(7),
      I1 => AXI_video_strm_V_data_V_1_payload_A(7),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(7)
    );
\stream_out_TDATA[8]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(8),
      I1 => AXI_video_strm_V_data_V_1_payload_A(8),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(8)
    );
\stream_out_TDATA[9]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI_video_strm_V_data_V_1_payload_B(9),
      I1 => AXI_video_strm_V_data_V_1_payload_A(9),
      I2 => AXI_video_strm_V_data_V_1_sel,
      O => stream_out_TDATA(9)
    );
\stream_out_TLAST[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_last_V_1_payload_B,
      I1 => AXI_video_strm_V_last_V_1_sel,
      I2 => AXI_video_strm_V_last_V_1_payload_A,
      O => stream_out_TLAST(0)
    );
\stream_out_TUSER[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI_video_strm_V_user_V_1_payload_B,
      I1 => AXI_video_strm_V_user_V_1_sel,
      I2 => AXI_video_strm_V_user_V_1_payload_A,
      O => stream_out_TUSER(0)
    );
\t_V_1_reg_188[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \t_V_1_reg_188_reg__0\(0),
      O => j_V_fu_222_p2(0)
    );
\t_V_1_reg_188[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => \ap_CS_fsm[2]_i_2__1_n_2\,
      I1 => axi_last_V_reg_2790,
      I2 => ap_enable_reg_pp0_iter0,
      O => t_V_1_reg_188
    );
\t_V_1_reg_188[10]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter0,
      I1 => axi_last_V_reg_2790,
      O => t_V_1_reg_1880
    );
\t_V_1_reg_188[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \t_V_1_reg_188_reg__0\(10),
      I1 => \t_V_1_reg_188_reg__0\(8),
      I2 => \t_V_1_reg_188_reg__0\(6),
      I3 => \t_V_1_reg_188[10]_i_5_n_2\,
      I4 => \t_V_1_reg_188_reg__0\(7),
      I5 => \t_V_1_reg_188_reg__0\(9),
      O => j_V_fu_222_p2(10)
    );
\t_V_1_reg_188[10]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \exitcond_reg_270[0]_i_3_n_2\,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => exitcond_fu_216_p2,
      O => axi_last_V_reg_2790
    );
\t_V_1_reg_188[10]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \t_V_1_reg_188_reg__0\(5),
      I1 => \t_V_1_reg_188_reg__0\(3),
      I2 => \t_V_1_reg_188_reg__0\(0),
      I3 => \t_V_1_reg_188_reg__0\(1),
      I4 => \t_V_1_reg_188_reg__0\(2),
      I5 => \t_V_1_reg_188_reg__0\(4),
      O => \t_V_1_reg_188[10]_i_5_n_2\
    );
\t_V_1_reg_188[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \t_V_1_reg_188_reg__0\(0),
      I1 => \t_V_1_reg_188_reg__0\(1),
      O => j_V_fu_222_p2(1)
    );
\t_V_1_reg_188[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \t_V_1_reg_188_reg__0\(2),
      I1 => \t_V_1_reg_188_reg__0\(1),
      I2 => \t_V_1_reg_188_reg__0\(0),
      O => j_V_fu_222_p2(2)
    );
\t_V_1_reg_188[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \t_V_1_reg_188_reg__0\(3),
      I1 => \t_V_1_reg_188_reg__0\(0),
      I2 => \t_V_1_reg_188_reg__0\(1),
      I3 => \t_V_1_reg_188_reg__0\(2),
      O => j_V_fu_222_p2(3)
    );
\t_V_1_reg_188[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \t_V_1_reg_188_reg__0\(4),
      I1 => \t_V_1_reg_188_reg__0\(2),
      I2 => \t_V_1_reg_188_reg__0\(1),
      I3 => \t_V_1_reg_188_reg__0\(0),
      I4 => \t_V_1_reg_188_reg__0\(3),
      O => j_V_fu_222_p2(4)
    );
\t_V_1_reg_188[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \t_V_1_reg_188_reg__0\(5),
      I1 => \t_V_1_reg_188_reg__0\(3),
      I2 => \t_V_1_reg_188_reg__0\(0),
      I3 => \t_V_1_reg_188_reg__0\(1),
      I4 => \t_V_1_reg_188_reg__0\(2),
      I5 => \t_V_1_reg_188_reg__0\(4),
      O => j_V_fu_222_p2(5)
    );
\t_V_1_reg_188[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \t_V_1_reg_188_reg__0\(6),
      I1 => \t_V_1_reg_188[10]_i_5_n_2\,
      O => j_V_fu_222_p2(6)
    );
\t_V_1_reg_188[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \t_V_1_reg_188_reg__0\(7),
      I1 => \t_V_1_reg_188[10]_i_5_n_2\,
      I2 => \t_V_1_reg_188_reg__0\(6),
      O => j_V_fu_222_p2(7)
    );
\t_V_1_reg_188[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \t_V_1_reg_188_reg__0\(8),
      I1 => \t_V_1_reg_188_reg__0\(6),
      I2 => \t_V_1_reg_188[10]_i_5_n_2\,
      I3 => \t_V_1_reg_188_reg__0\(7),
      O => j_V_fu_222_p2(8)
    );
\t_V_1_reg_188[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \t_V_1_reg_188_reg__0\(9),
      I1 => \t_V_1_reg_188_reg__0\(7),
      I2 => \t_V_1_reg_188[10]_i_5_n_2\,
      I3 => \t_V_1_reg_188_reg__0\(6),
      I4 => \t_V_1_reg_188_reg__0\(8),
      O => j_V_fu_222_p2(9)
    );
\t_V_1_reg_188_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1880,
      D => j_V_fu_222_p2(0),
      Q => \t_V_1_reg_188_reg__0\(0),
      R => t_V_1_reg_188
    );
\t_V_1_reg_188_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1880,
      D => j_V_fu_222_p2(10),
      Q => \t_V_1_reg_188_reg__0\(10),
      R => t_V_1_reg_188
    );
\t_V_1_reg_188_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1880,
      D => j_V_fu_222_p2(1),
      Q => \t_V_1_reg_188_reg__0\(1),
      R => t_V_1_reg_188
    );
\t_V_1_reg_188_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1880,
      D => j_V_fu_222_p2(2),
      Q => \t_V_1_reg_188_reg__0\(2),
      R => t_V_1_reg_188
    );
\t_V_1_reg_188_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1880,
      D => j_V_fu_222_p2(3),
      Q => \t_V_1_reg_188_reg__0\(3),
      R => t_V_1_reg_188
    );
\t_V_1_reg_188_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1880,
      D => j_V_fu_222_p2(4),
      Q => \t_V_1_reg_188_reg__0\(4),
      R => t_V_1_reg_188
    );
\t_V_1_reg_188_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1880,
      D => j_V_fu_222_p2(5),
      Q => \t_V_1_reg_188_reg__0\(5),
      R => t_V_1_reg_188
    );
\t_V_1_reg_188_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1880,
      D => j_V_fu_222_p2(6),
      Q => \t_V_1_reg_188_reg__0\(6),
      R => t_V_1_reg_188
    );
\t_V_1_reg_188_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1880,
      D => j_V_fu_222_p2(7),
      Q => \t_V_1_reg_188_reg__0\(7),
      R => t_V_1_reg_188
    );
\t_V_1_reg_188_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1880,
      D => j_V_fu_222_p2(8),
      Q => \t_V_1_reg_188_reg__0\(8),
      R => t_V_1_reg_188
    );
\t_V_1_reg_188_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_1_reg_1880,
      D => j_V_fu_222_p2(9),
      Q => \t_V_1_reg_188_reg__0\(9),
      R => t_V_1_reg_188
    );
\t_V_reg_177[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \ap_CS_fsm_reg_n_2_[0]\,
      I1 => Mat2AXIvideo_U0_ap_start,
      I2 => ap_CS_fsm_state6,
      O => t_V_reg_177
    );
\t_V_reg_177_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => i_V_reg_265(0),
      Q => \t_V_reg_177_reg_n_2_[0]\,
      R => t_V_reg_177
    );
\t_V_reg_177_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => i_V_reg_265(1),
      Q => \t_V_reg_177_reg_n_2_[1]\,
      R => t_V_reg_177
    );
\t_V_reg_177_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => i_V_reg_265(2),
      Q => \t_V_reg_177_reg_n_2_[2]\,
      R => t_V_reg_177
    );
\t_V_reg_177_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => i_V_reg_265(3),
      Q => \t_V_reg_177_reg_n_2_[3]\,
      R => t_V_reg_177
    );
\t_V_reg_177_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => i_V_reg_265(4),
      Q => \t_V_reg_177_reg_n_2_[4]\,
      R => t_V_reg_177
    );
\t_V_reg_177_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => i_V_reg_265(5),
      Q => \t_V_reg_177_reg_n_2_[5]\,
      R => t_V_reg_177
    );
\t_V_reg_177_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => i_V_reg_265(6),
      Q => \t_V_reg_177_reg_n_2_[6]\,
      R => t_V_reg_177
    );
\t_V_reg_177_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => i_V_reg_265(7),
      Q => \t_V_reg_177_reg_n_2_[7]\,
      R => t_V_reg_177
    );
\t_V_reg_177_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => i_V_reg_265(8),
      Q => \t_V_reg_177_reg_n_2_[8]\,
      R => t_V_reg_177
    );
\t_V_reg_177_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => i_V_reg_265(9),
      Q => \t_V_reg_177_reg_n_2_[9]\,
      R => t_V_reg_177
    );
\tmp_user_V_fu_126[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00EA"
    )
        port map (
      I0 => tmp_user_V_fu_126,
      I1 => Mat2AXIvideo_U0_ap_start,
      I2 => \ap_CS_fsm_reg_n_2_[0]\,
      I3 => \^moutptr_reg[1]\,
      O => \tmp_user_V_fu_126[0]_i_1_n_2\
    );
\tmp_user_V_fu_126_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_user_V_fu_126[0]_i_1_n_2\,
      Q => tmp_user_V_fu_126,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_edge_detect_mac_mdEe_DSP48_2 is
  port (
    P : out STD_LOGIC_VECTOR ( 8 downto 0 );
    p_0 : out STD_LOGIC;
    \r_V_1_reg_389_reg[29]\ : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 7 downto 0 );
    p_Val2_12_reg_384_reg : in STD_LOGIC_VECTOR ( 28 downto 0 );
    tmp_20_reg_355 : in STD_LOGIC;
    ap_enable_reg_pp0_iter1_reg : in STD_LOGIC;
    img0_data_stream_2_s_empty_n : in STD_LOGIC;
    img0_data_stream_0_s_empty_n : in STD_LOGIC;
    img0_data_stream_1_s_empty_n : in STD_LOGIC;
    ap_reg_pp0_iter4_tmp_20_reg_355 : in STD_LOGIC;
    ap_enable_reg_pp0_iter5_reg : in STD_LOGIC;
    img1_data_stream_0_s_full_n : in STD_LOGIC;
    img1_data_stream_1_s_full_n : in STD_LOGIC;
    img1_data_stream_2_s_full_n : in STD_LOGIC;
    ap_reg_pp0_iter3_tmp_20_reg_355 : in STD_LOGIC;
    ap_enable_reg_pp0_iter4 : in STD_LOGIC;
    tmp_98_fu_287_p3 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_edge_detect_mac_mdEe_DSP48_2;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_edge_detect_mac_mdEe_DSP48_2 is
  signal \^p\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \^p_0\ : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_i_13_n_2 : STD_LOGIC;
  signal p_n_100 : STD_LOGIC;
  signal p_n_101 : STD_LOGIC;
  signal p_n_102 : STD_LOGIC;
  signal p_n_103 : STD_LOGIC;
  signal p_n_104 : STD_LOGIC;
  signal p_n_105 : STD_LOGIC;
  signal p_n_106 : STD_LOGIC;
  signal p_n_107 : STD_LOGIC;
  signal p_n_87 : STD_LOGIC;
  signal p_n_88 : STD_LOGIC;
  signal p_n_89 : STD_LOGIC;
  signal p_n_90 : STD_LOGIC;
  signal p_n_91 : STD_LOGIC;
  signal p_n_92 : STD_LOGIC;
  signal p_n_93 : STD_LOGIC;
  signal p_n_94 : STD_LOGIC;
  signal p_n_95 : STD_LOGIC;
  signal p_n_96 : STD_LOGIC;
  signal p_n_97 : STD_LOGIC;
  signal p_n_98 : STD_LOGIC;
  signal p_n_99 : STD_LOGIC;
  signal NLW_p_RnM_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_p_RnM_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_p_RnM_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_p_RnM_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_p_RnM_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_p_RnM_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_p_RnM_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_p_RnM_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_p_RnM_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_p_RnM_P_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 30 );
  signal NLW_p_RnM_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of p_RnM : label is "{SYNTH-11 {cell *THIS*}}";
begin
  P(8 downto 0) <= \^p\(8 downto 0);
  p_0 <= \^p_0\;
p_RnM: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 0,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 0,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 2,
      BREG => 2,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 0,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 0,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29 downto 0) => B"000000001001011001000101101000",
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_p_RnM_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17 downto 8) => B"0000000000",
      B(7 downto 0) => Q(7 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_p_RnM_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 29) => B"0000000000000000000",
      C(28 downto 0) => p_Val2_12_reg_384_reg(28 downto 0),
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_p_RnM_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_p_RnM_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '0',
      CEA2 => '0',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => \^p_0\,
      CEB2 => \^p_0\,
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '0',
      CLK => ap_clk,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_p_RnM_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"0110101",
      OVERFLOW => NLW_p_RnM_OVERFLOW_UNCONNECTED,
      P(47 downto 30) => NLW_p_RnM_P_UNCONNECTED(47 downto 30),
      P(29 downto 21) => \^p\(8 downto 0),
      P(20) => p_n_87,
      P(19) => p_n_88,
      P(18) => p_n_89,
      P(17) => p_n_90,
      P(16) => p_n_91,
      P(15) => p_n_92,
      P(14) => p_n_93,
      P(13) => p_n_94,
      P(12) => p_n_95,
      P(11) => p_n_96,
      P(10) => p_n_97,
      P(9) => p_n_98,
      P(8) => p_n_99,
      P(7) => p_n_100,
      P(6) => p_n_101,
      P(5) => p_n_102,
      P(4) => p_n_103,
      P(3) => p_n_104,
      P(2) => p_n_105,
      P(1) => p_n_106,
      P(0) => p_n_107,
      PATTERNBDETECT => NLW_p_RnM_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_p_RnM_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47 downto 0) => NLW_p_RnM_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_p_RnM_UNDERFLOW_UNCONNECTED
    );
p_Val2_12_reg_384_reg_i_13: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08888888"
    )
        port map (
      I0 => ap_reg_pp0_iter4_tmp_20_reg_355,
      I1 => ap_enable_reg_pp0_iter5_reg,
      I2 => img1_data_stream_0_s_full_n,
      I3 => img1_data_stream_1_s_full_n,
      I4 => img1_data_stream_2_s_full_n,
      O => p_Val2_12_reg_384_reg_i_13_n_2
    );
p_Val2_12_reg_384_reg_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000F7777777"
    )
        port map (
      I0 => tmp_20_reg_355,
      I1 => ap_enable_reg_pp0_iter1_reg,
      I2 => img0_data_stream_2_s_empty_n,
      I3 => img0_data_stream_0_s_empty_n,
      I4 => img0_data_stream_1_s_empty_n,
      I5 => p_Val2_12_reg_384_reg_i_13_n_2,
      O => \^p_0\
    );
\r_V_1_reg_389[29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFFF8000"
    )
        port map (
      I0 => \^p\(8),
      I1 => \^p_0\,
      I2 => ap_reg_pp0_iter3_tmp_20_reg_355,
      I3 => ap_enable_reg_pp0_iter4,
      I4 => tmp_98_fu_287_p3,
      O => \r_V_1_reg_389_reg[29]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_edge_detect_mul_mbkb_DSP48_0 is
  port (
    \out\ : out STD_LOGIC_VECTOR ( 28 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_edge_detect_mul_mbkb_DSP48_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_edge_detect_mul_mbkb_DSP48_0 is
  signal in00_n_78 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 21 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of p_0_in : signal is "true";
  signal NLW_in00_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_in00_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_in00_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_in00_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_in00_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_in00_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_in00_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_in00_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_in00_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_in00_P_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 30 );
  signal NLW_in00_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of in00 : label is "{SYNTH-13 {cell *THIS*}}";
begin
i_2_0: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => p_0_in(21)
    );
i_2_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '1',
      O => p_0_in(20)
    );
i_2_10: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => p_0_in(11)
    );
i_2_11: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => p_0_in(10)
    );
i_2_12: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '1',
      O => p_0_in(9)
    );
i_2_13: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => p_0_in(8)
    );
i_2_14: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '1',
      O => p_0_in(7)
    );
i_2_15: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '1',
      O => p_0_in(6)
    );
i_2_16: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => p_0_in(5)
    );
i_2_17: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '1',
      O => p_0_in(4)
    );
i_2_18: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => p_0_in(3)
    );
i_2_19: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => p_0_in(2)
    );
i_2_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => p_0_in(19)
    );
i_2_20: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => p_0_in(1)
    );
i_2_21: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => p_0_in(0)
    );
i_2_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => p_0_in(18)
    );
i_2_4: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '1',
      O => p_0_in(17)
    );
i_2_5: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '1',
      O => p_0_in(16)
    );
i_2_6: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => p_0_in(15)
    );
i_2_7: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => p_0_in(14)
    );
i_2_8: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '1',
      O => p_0_in(13)
    );
i_2_9: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => p_0_in(12)
    );
in00: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 0,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 0,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 0,
      BREG => 0,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 0,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29 downto 22) => B"00000000",
      A(21 downto 0) => p_0_in(21 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_in00_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17 downto 8) => B"0000000000",
      B(7 downto 0) => Q(7 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_in00_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_in00_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_in00_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '0',
      CEA2 => '0',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => '0',
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '0',
      CLK => '0',
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_in00_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"0000101",
      OVERFLOW => NLW_in00_OVERFLOW_UNCONNECTED,
      P(47 downto 30) => NLW_in00_P_UNCONNECTED(47 downto 30),
      P(29) => in00_n_78,
      P(28 downto 0) => \out\(28 downto 0),
      PATTERNBDETECT => NLW_in00_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_in00_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47 downto 0) => NLW_in00_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_in00_UNDERFLOW_UNCONNECTED
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_3 is
  port (
    img1_data_stream_1_s_full_n : out STD_LOGIC;
    img1_data_stream_1_s_empty_n : out STD_LOGIC;
    \mOutPtr_reg[1]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    grp_Filter2D_fu_96_p_src_data_stream_2_V_read : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    shiftReg_ce : in STD_LOGIC;
    ap_rst_n_inv : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_3 : entity is "fifo_w8_d1_A";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_3;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_3 is
  signal \^img1_data_stream_1_s_empty_n\ : STD_LOGIC;
  signal \^img1_data_stream_1_s_full_n\ : STD_LOGIC;
  signal \internal_empty_n_i_1__5_n_2\ : STD_LOGIC;
  signal \internal_full_n_i_1__5_n_2\ : STD_LOGIC;
  signal \internal_full_n_i_2__4_n_2\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1__3_n_2\ : STD_LOGIC;
  signal \^moutptr_reg[1]_0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \internal_full_n_i_2__4\ : label is "soft_lutpair164";
  attribute SOFT_HLUTNM of \mOutPtr[0]_i_1__3\ : label is "soft_lutpair164";
begin
  img1_data_stream_1_s_empty_n <= \^img1_data_stream_1_s_empty_n\;
  img1_data_stream_1_s_full_n <= \^img1_data_stream_1_s_full_n\;
  \mOutPtr_reg[1]_0\(1 downto 0) <= \^moutptr_reg[1]_0\(1 downto 0);
\internal_empty_n_i_1__5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCC8CCC0CCC0000"
    )
        port map (
      I0 => \internal_full_n_i_2__4_n_2\,
      I1 => ap_rst_n,
      I2 => Q(0),
      I3 => grp_Filter2D_fu_96_p_src_data_stream_2_V_read,
      I4 => shiftReg_ce,
      I5 => \^img1_data_stream_1_s_empty_n\,
      O => \internal_empty_n_i_1__5_n_2\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__5_n_2\,
      Q => \^img1_data_stream_1_s_empty_n\,
      R => '0'
    );
\internal_full_n_i_1__5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CF8F8F8FFFCFCFCF"
    )
        port map (
      I0 => \internal_full_n_i_2__4_n_2\,
      I1 => \^img1_data_stream_1_s_full_n\,
      I2 => ap_rst_n,
      I3 => grp_Filter2D_fu_96_p_src_data_stream_2_V_read,
      I4 => Q(0),
      I5 => shiftReg_ce,
      O => \internal_full_n_i_1__5_n_2\
    );
\internal_full_n_i_2__4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^moutptr_reg[1]_0\(1),
      I1 => \^moutptr_reg[1]_0\(0),
      O => \internal_full_n_i_2__4_n_2\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__5_n_2\,
      Q => \^img1_data_stream_1_s_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^moutptr_reg[1]_0\(0),
      O => \mOutPtr[0]_i_1__3_n_2\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => \mOutPtr[0]_i_1__3_n_2\,
      Q => \^moutptr_reg[1]_0\(0),
      S => ap_rst_n_inv
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => D(0),
      Q => \^moutptr_reg[1]_0\(1),
      S => ap_rst_n_inv
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_4 is
  port (
    img1_data_stream_2_s_full_n : out STD_LOGIC;
    img1_data_stream_2_s_empty_n : out STD_LOGIC;
    \mOutPtr_reg[1]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    grp_Filter2D_fu_96_p_src_data_stream_2_V_read : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    shiftReg_ce : in STD_LOGIC;
    ap_rst_n_inv : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_4 : entity is "fifo_w8_d1_A";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_4;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_4 is
  signal \^img1_data_stream_2_s_empty_n\ : STD_LOGIC;
  signal \^img1_data_stream_2_s_full_n\ : STD_LOGIC;
  signal \internal_empty_n_i_1__6_n_2\ : STD_LOGIC;
  signal \internal_full_n_i_1__6_n_2\ : STD_LOGIC;
  signal \internal_full_n_i_2__5_n_2\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1__4_n_2\ : STD_LOGIC;
  signal \^moutptr_reg[1]_0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \internal_full_n_i_2__5\ : label is "soft_lutpair165";
  attribute SOFT_HLUTNM of \mOutPtr[0]_i_1__4\ : label is "soft_lutpair165";
begin
  img1_data_stream_2_s_empty_n <= \^img1_data_stream_2_s_empty_n\;
  img1_data_stream_2_s_full_n <= \^img1_data_stream_2_s_full_n\;
  \mOutPtr_reg[1]_0\(1 downto 0) <= \^moutptr_reg[1]_0\(1 downto 0);
\internal_empty_n_i_1__6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCC8CCC0CCC0000"
    )
        port map (
      I0 => \internal_full_n_i_2__5_n_2\,
      I1 => ap_rst_n,
      I2 => Q(0),
      I3 => grp_Filter2D_fu_96_p_src_data_stream_2_V_read,
      I4 => shiftReg_ce,
      I5 => \^img1_data_stream_2_s_empty_n\,
      O => \internal_empty_n_i_1__6_n_2\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__6_n_2\,
      Q => \^img1_data_stream_2_s_empty_n\,
      R => '0'
    );
\internal_full_n_i_1__6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CF8F8F8FFFCFCFCF"
    )
        port map (
      I0 => \internal_full_n_i_2__5_n_2\,
      I1 => \^img1_data_stream_2_s_full_n\,
      I2 => ap_rst_n,
      I3 => grp_Filter2D_fu_96_p_src_data_stream_2_V_read,
      I4 => Q(0),
      I5 => shiftReg_ce,
      O => \internal_full_n_i_1__6_n_2\
    );
\internal_full_n_i_2__5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^moutptr_reg[1]_0\(1),
      I1 => \^moutptr_reg[1]_0\(0),
      O => \internal_full_n_i_2__5_n_2\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__6_n_2\,
      Q => \^img1_data_stream_2_s_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1__4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^moutptr_reg[1]_0\(0),
      O => \mOutPtr[0]_i_1__4_n_2\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => \mOutPtr[0]_i_1__4_n_2\,
      Q => \^moutptr_reg[1]_0\(0),
      S => ap_rst_n_inv
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => D(0),
      Q => \^moutptr_reg[1]_0\(1),
      S => ap_rst_n_inv
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_6 is
  port (
    img2_data_stream_1_s_full_n : out STD_LOGIC;
    img2_data_stream_1_s_empty_n : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    shiftReg_ce : in STD_LOGIC;
    shiftReg_ce_0 : in STD_LOGIC;
    ap_rst_n_inv : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_6 : entity is "fifo_w8_d1_A";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_6;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_6 is
  signal \^img2_data_stream_1_s_empty_n\ : STD_LOGIC;
  signal \^img2_data_stream_1_s_full_n\ : STD_LOGIC;
  signal \internal_empty_n_i_1__8_n_2\ : STD_LOGIC;
  signal \internal_full_n_i_1__8_n_2\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1__6_n_2\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1__7_n_2\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[0]\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[1]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \mOutPtr[0]_i_1__6\ : label is "soft_lutpair167";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_1__7\ : label is "soft_lutpair167";
begin
  img2_data_stream_1_s_empty_n <= \^img2_data_stream_1_s_empty_n\;
  img2_data_stream_1_s_full_n <= \^img2_data_stream_1_s_full_n\;
\internal_empty_n_i_1__8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0F0E0F000F00000"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[1]\,
      I1 => \mOutPtr_reg_n_2_[0]\,
      I2 => ap_rst_n,
      I3 => shiftReg_ce,
      I4 => shiftReg_ce_0,
      I5 => \^img2_data_stream_1_s_empty_n\,
      O => \internal_empty_n_i_1__8_n_2\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__8_n_2\,
      Q => \^img2_data_stream_1_s_empty_n\,
      R => '0'
    );
\internal_full_n_i_1__8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFE0FFFFFFF0FF"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[0]\,
      I1 => \mOutPtr_reg_n_2_[1]\,
      I2 => \^img2_data_stream_1_s_full_n\,
      I3 => ap_rst_n,
      I4 => shiftReg_ce,
      I5 => shiftReg_ce_0,
      O => \internal_full_n_i_1__8_n_2\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__8_n_2\,
      Q => \^img2_data_stream_1_s_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1__6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[0]\,
      O => \mOutPtr[0]_i_1__6_n_2\
    );
\mOutPtr[1]_i_1__7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => shiftReg_ce_0,
      I1 => shiftReg_ce,
      I2 => \mOutPtr_reg_n_2_[0]\,
      I3 => \mOutPtr_reg_n_2_[1]\,
      O => \mOutPtr[1]_i_1__7_n_2\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => \mOutPtr[0]_i_1__6_n_2\,
      Q => \mOutPtr_reg_n_2_[0]\,
      S => ap_rst_n_inv
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => \mOutPtr[1]_i_1__7_n_2\,
      Q => \mOutPtr_reg_n_2_[1]\,
      S => ap_rst_n_inv
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_7 is
  port (
    img2_data_stream_2_s_full_n : out STD_LOGIC;
    img2_data_stream_2_s_empty_n : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    shiftReg_ce : in STD_LOGIC;
    shiftReg_ce_0 : in STD_LOGIC;
    ap_rst_n_inv : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_7 : entity is "fifo_w8_d1_A";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_7;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_7 is
  signal \^img2_data_stream_2_s_empty_n\ : STD_LOGIC;
  signal \^img2_data_stream_2_s_full_n\ : STD_LOGIC;
  signal \internal_empty_n_i_1__7_n_2\ : STD_LOGIC;
  signal \internal_full_n_i_1__9_n_2\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1__7_n_2\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1__6_n_2\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[0]\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[1]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \mOutPtr[0]_i_1__7\ : label is "soft_lutpair168";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_1__6\ : label is "soft_lutpair168";
begin
  img2_data_stream_2_s_empty_n <= \^img2_data_stream_2_s_empty_n\;
  img2_data_stream_2_s_full_n <= \^img2_data_stream_2_s_full_n\;
\internal_empty_n_i_1__7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0F0E0F000F00000"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[1]\,
      I1 => \mOutPtr_reg_n_2_[0]\,
      I2 => ap_rst_n,
      I3 => shiftReg_ce,
      I4 => shiftReg_ce_0,
      I5 => \^img2_data_stream_2_s_empty_n\,
      O => \internal_empty_n_i_1__7_n_2\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__7_n_2\,
      Q => \^img2_data_stream_2_s_empty_n\,
      R => '0'
    );
\internal_full_n_i_1__9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFE0FFFFFFF0FF"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[0]\,
      I1 => \mOutPtr_reg_n_2_[1]\,
      I2 => \^img2_data_stream_2_s_full_n\,
      I3 => ap_rst_n,
      I4 => shiftReg_ce,
      I5 => shiftReg_ce_0,
      O => \internal_full_n_i_1__9_n_2\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__9_n_2\,
      Q => \^img2_data_stream_2_s_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1__7\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[0]\,
      O => \mOutPtr[0]_i_1__7_n_2\
    );
\mOutPtr[1]_i_1__6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => shiftReg_ce_0,
      I1 => shiftReg_ce,
      I2 => \mOutPtr_reg_n_2_[0]\,
      I3 => \mOutPtr_reg_n_2_[1]\,
      O => \mOutPtr[1]_i_1__6_n_2\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => \mOutPtr[0]_i_1__7_n_2\,
      Q => \mOutPtr_reg_n_2_[0]\,
      S => ap_rst_n_inv
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => \mOutPtr[1]_i_1__6_n_2\,
      Q => \mOutPtr_reg_n_2_[1]\,
      S => ap_rst_n_inv
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_8 is
  port (
    img3_data_stream_0_s_full_n : out STD_LOGIC;
    img3_data_stream_0_s_empty_n : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    \ap_CS_fsm_reg[2]\ : in STD_LOGIC;
    shiftReg_ce : in STD_LOGIC;
    ap_rst_n_inv : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_8 : entity is "fifo_w8_d1_A";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_8;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_8 is
  signal \^q\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^img3_data_stream_0_s_empty_n\ : STD_LOGIC;
  signal \^img3_data_stream_0_s_full_n\ : STD_LOGIC;
  signal \internal_empty_n_i_1__12_n_2\ : STD_LOGIC;
  signal \internal_full_n_i_1__12_n_2\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1__8_n_2\ : STD_LOGIC;
  signal \mOutPtr[1]_i_2__4_n_2\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \mOutPtr[0]_i_1__8\ : label is "soft_lutpair169";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_2__4\ : label is "soft_lutpair169";
begin
  Q(1 downto 0) <= \^q\(1 downto 0);
  img3_data_stream_0_s_empty_n <= \^img3_data_stream_0_s_empty_n\;
  img3_data_stream_0_s_full_n <= \^img3_data_stream_0_s_full_n\;
\internal_empty_n_i_1__12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0F0E0F000F00000"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => ap_rst_n,
      I3 => \ap_CS_fsm_reg[2]\,
      I4 => shiftReg_ce,
      I5 => \^img3_data_stream_0_s_empty_n\,
      O => \internal_empty_n_i_1__12_n_2\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__12_n_2\,
      Q => \^img3_data_stream_0_s_empty_n\,
      R => '0'
    );
\internal_full_n_i_1__12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFE0FFFFFFF0FF"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^img3_data_stream_0_s_full_n\,
      I3 => ap_rst_n,
      I4 => \ap_CS_fsm_reg[2]\,
      I5 => shiftReg_ce,
      O => \internal_full_n_i_1__12_n_2\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__12_n_2\,
      Q => \^img3_data_stream_0_s_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1__8\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => \mOutPtr[0]_i_1__8_n_2\
    );
\mOutPtr[1]_i_2__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => shiftReg_ce,
      I1 => \ap_CS_fsm_reg[2]\,
      I2 => \^q\(0),
      I3 => \^q\(1),
      O => \mOutPtr[1]_i_2__4_n_2\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => \mOutPtr[0]_i_1__8_n_2\,
      Q => \^q\(0),
      S => ap_rst_n_inv
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => \mOutPtr[1]_i_2__4_n_2\,
      Q => \^q\(1),
      S => ap_rst_n_inv
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_9 is
  port (
    img3_data_stream_1_s_full_n : out STD_LOGIC;
    img3_data_stream_1_s_empty_n : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    \ap_CS_fsm_reg[2]\ : in STD_LOGIC;
    shiftReg_ce : in STD_LOGIC;
    ap_rst_n_inv : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_9 : entity is "fifo_w8_d1_A";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_9;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_9 is
  signal \^q\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^img3_data_stream_1_s_empty_n\ : STD_LOGIC;
  signal \^img3_data_stream_1_s_full_n\ : STD_LOGIC;
  signal \internal_empty_n_i_1__14_n_2\ : STD_LOGIC;
  signal \internal_full_n_i_1__13_n_2\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1__9_n_2\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1__9_n_2\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \mOutPtr[0]_i_1__9\ : label is "soft_lutpair170";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_1__9\ : label is "soft_lutpair170";
begin
  Q(1 downto 0) <= \^q\(1 downto 0);
  img3_data_stream_1_s_empty_n <= \^img3_data_stream_1_s_empty_n\;
  img3_data_stream_1_s_full_n <= \^img3_data_stream_1_s_full_n\;
\internal_empty_n_i_1__14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0F0E0F000F00000"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => ap_rst_n,
      I3 => \ap_CS_fsm_reg[2]\,
      I4 => shiftReg_ce,
      I5 => \^img3_data_stream_1_s_empty_n\,
      O => \internal_empty_n_i_1__14_n_2\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__14_n_2\,
      Q => \^img3_data_stream_1_s_empty_n\,
      R => '0'
    );
\internal_full_n_i_1__13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFE0FFFFFFF0FF"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^img3_data_stream_1_s_full_n\,
      I3 => ap_rst_n,
      I4 => \ap_CS_fsm_reg[2]\,
      I5 => shiftReg_ce,
      O => \internal_full_n_i_1__13_n_2\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__13_n_2\,
      Q => \^img3_data_stream_1_s_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1__9\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => \mOutPtr[0]_i_1__9_n_2\
    );
\mOutPtr[1]_i_1__9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => shiftReg_ce,
      I1 => \ap_CS_fsm_reg[2]\,
      I2 => \^q\(0),
      I3 => \^q\(1),
      O => \mOutPtr[1]_i_1__9_n_2\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => \mOutPtr[0]_i_1__9_n_2\,
      Q => \^q\(0),
      S => ap_rst_n_inv
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => \mOutPtr[1]_i_1__9_n_2\,
      Q => \^q\(1),
      S => ap_rst_n_inv
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg is
  port (
    D : out STD_LOGIC_VECTOR ( 23 downto 0 );
    shiftReg_ce : in STD_LOGIC;
    img2_data_stream_0_s_dout : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \mOutPtr_reg[1]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \mOutPtr_reg[1]_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg is
  signal \SRL_SIG_reg[0]_0\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \SRL_SIG_reg[1]_1\ : STD_LOGIC_VECTOR ( 7 downto 0 );
begin
\AXI_video_strm_V_data_V_1_payload_A[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(0),
      I1 => \mOutPtr_reg[1]_0\(1),
      I2 => \mOutPtr_reg[1]_0\(0),
      I3 => \SRL_SIG_reg[1]_1\(0),
      O => D(0)
    );
\AXI_video_strm_V_data_V_1_payload_A[10]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(2),
      I1 => \mOutPtr_reg[1]\(1),
      I2 => \mOutPtr_reg[1]\(0),
      I3 => \SRL_SIG_reg[1]_1\(2),
      O => D(10)
    );
\AXI_video_strm_V_data_V_1_payload_A[11]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(3),
      I1 => \mOutPtr_reg[1]\(1),
      I2 => \mOutPtr_reg[1]\(0),
      I3 => \SRL_SIG_reg[1]_1\(3),
      O => D(11)
    );
\AXI_video_strm_V_data_V_1_payload_A[12]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(4),
      I1 => \mOutPtr_reg[1]\(1),
      I2 => \mOutPtr_reg[1]\(0),
      I3 => \SRL_SIG_reg[1]_1\(4),
      O => D(12)
    );
\AXI_video_strm_V_data_V_1_payload_A[13]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(5),
      I1 => \mOutPtr_reg[1]\(1),
      I2 => \mOutPtr_reg[1]\(0),
      I3 => \SRL_SIG_reg[1]_1\(5),
      O => D(13)
    );
\AXI_video_strm_V_data_V_1_payload_A[14]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(6),
      I1 => \mOutPtr_reg[1]\(1),
      I2 => \mOutPtr_reg[1]\(0),
      I3 => \SRL_SIG_reg[1]_1\(6),
      O => D(14)
    );
\AXI_video_strm_V_data_V_1_payload_A[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(7),
      I1 => \mOutPtr_reg[1]\(1),
      I2 => \mOutPtr_reg[1]\(0),
      I3 => \SRL_SIG_reg[1]_1\(7),
      O => D(15)
    );
\AXI_video_strm_V_data_V_1_payload_A[16]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(0),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(0),
      O => D(16)
    );
\AXI_video_strm_V_data_V_1_payload_A[17]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(1),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(1),
      O => D(17)
    );
\AXI_video_strm_V_data_V_1_payload_A[18]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(2),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(2),
      O => D(18)
    );
\AXI_video_strm_V_data_V_1_payload_A[19]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(3),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(3),
      O => D(19)
    );
\AXI_video_strm_V_data_V_1_payload_A[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(1),
      I1 => \mOutPtr_reg[1]_0\(1),
      I2 => \mOutPtr_reg[1]_0\(0),
      I3 => \SRL_SIG_reg[1]_1\(1),
      O => D(1)
    );
\AXI_video_strm_V_data_V_1_payload_A[20]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(4),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(4),
      O => D(20)
    );
\AXI_video_strm_V_data_V_1_payload_A[21]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(5),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(5),
      O => D(21)
    );
\AXI_video_strm_V_data_V_1_payload_A[22]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(6),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(6),
      O => D(22)
    );
\AXI_video_strm_V_data_V_1_payload_A[23]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(7),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(7),
      O => D(23)
    );
\AXI_video_strm_V_data_V_1_payload_A[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(2),
      I1 => \mOutPtr_reg[1]_0\(1),
      I2 => \mOutPtr_reg[1]_0\(0),
      I3 => \SRL_SIG_reg[1]_1\(2),
      O => D(2)
    );
\AXI_video_strm_V_data_V_1_payload_A[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(3),
      I1 => \mOutPtr_reg[1]_0\(1),
      I2 => \mOutPtr_reg[1]_0\(0),
      I3 => \SRL_SIG_reg[1]_1\(3),
      O => D(3)
    );
\AXI_video_strm_V_data_V_1_payload_A[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(4),
      I1 => \mOutPtr_reg[1]_0\(1),
      I2 => \mOutPtr_reg[1]_0\(0),
      I3 => \SRL_SIG_reg[1]_1\(4),
      O => D(4)
    );
\AXI_video_strm_V_data_V_1_payload_A[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(5),
      I1 => \mOutPtr_reg[1]_0\(1),
      I2 => \mOutPtr_reg[1]_0\(0),
      I3 => \SRL_SIG_reg[1]_1\(5),
      O => D(5)
    );
\AXI_video_strm_V_data_V_1_payload_A[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(6),
      I1 => \mOutPtr_reg[1]_0\(1),
      I2 => \mOutPtr_reg[1]_0\(0),
      I3 => \SRL_SIG_reg[1]_1\(6),
      O => D(6)
    );
\AXI_video_strm_V_data_V_1_payload_A[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(7),
      I1 => \mOutPtr_reg[1]_0\(1),
      I2 => \mOutPtr_reg[1]_0\(0),
      I3 => \SRL_SIG_reg[1]_1\(7),
      O => D(7)
    );
\AXI_video_strm_V_data_V_1_payload_A[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(0),
      I1 => \mOutPtr_reg[1]\(1),
      I2 => \mOutPtr_reg[1]\(0),
      I3 => \SRL_SIG_reg[1]_1\(0),
      O => D(8)
    );
\AXI_video_strm_V_data_V_1_payload_A[9]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(1),
      I1 => \mOutPtr_reg[1]\(1),
      I2 => \mOutPtr_reg[1]\(0),
      I3 => \SRL_SIG_reg[1]_1\(1),
      O => D(9)
    );
\SRL_SIG_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => img2_data_stream_0_s_dout(0),
      Q => \SRL_SIG_reg[0]_0\(0),
      R => '0'
    );
\SRL_SIG_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => img2_data_stream_0_s_dout(1),
      Q => \SRL_SIG_reg[0]_0\(1),
      R => '0'
    );
\SRL_SIG_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => img2_data_stream_0_s_dout(2),
      Q => \SRL_SIG_reg[0]_0\(2),
      R => '0'
    );
\SRL_SIG_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => img2_data_stream_0_s_dout(3),
      Q => \SRL_SIG_reg[0]_0\(3),
      R => '0'
    );
\SRL_SIG_reg[0][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => img2_data_stream_0_s_dout(4),
      Q => \SRL_SIG_reg[0]_0\(4),
      R => '0'
    );
\SRL_SIG_reg[0][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => img2_data_stream_0_s_dout(5),
      Q => \SRL_SIG_reg[0]_0\(5),
      R => '0'
    );
\SRL_SIG_reg[0][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => img2_data_stream_0_s_dout(6),
      Q => \SRL_SIG_reg[0]_0\(6),
      R => '0'
    );
\SRL_SIG_reg[0][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => img2_data_stream_0_s_dout(7),
      Q => \SRL_SIG_reg[0]_0\(7),
      R => '0'
    );
\SRL_SIG_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \SRL_SIG_reg[0]_0\(0),
      Q => \SRL_SIG_reg[1]_1\(0),
      R => '0'
    );
\SRL_SIG_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \SRL_SIG_reg[0]_0\(1),
      Q => \SRL_SIG_reg[1]_1\(1),
      R => '0'
    );
\SRL_SIG_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \SRL_SIG_reg[0]_0\(2),
      Q => \SRL_SIG_reg[1]_1\(2),
      R => '0'
    );
\SRL_SIG_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \SRL_SIG_reg[0]_0\(3),
      Q => \SRL_SIG_reg[1]_1\(3),
      R => '0'
    );
\SRL_SIG_reg[1][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \SRL_SIG_reg[0]_0\(4),
      Q => \SRL_SIG_reg[1]_1\(4),
      R => '0'
    );
\SRL_SIG_reg[1][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \SRL_SIG_reg[0]_0\(5),
      Q => \SRL_SIG_reg[1]_1\(5),
      R => '0'
    );
\SRL_SIG_reg[1][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \SRL_SIG_reg[0]_0\(6),
      Q => \SRL_SIG_reg[1]_1\(6),
      R => '0'
    );
\SRL_SIG_reg[1][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \SRL_SIG_reg[0]_0\(7),
      Q => \SRL_SIG_reg[1]_1\(7),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_11 is
  port (
    img2_data_stream_0_s_dout : out STD_LOGIC_VECTOR ( 7 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \tmp_57_reg_2927_reg[2]\ : in STD_LOGIC;
    shiftReg_ce_0 : in STD_LOGIC;
    \p_Val2_1_reg_2922_reg[7]\ : in STD_LOGIC;
    ap_clk : in STD_LOGIC;
    \p_Val2_1_reg_2922_reg[6]\ : in STD_LOGIC;
    \p_Val2_1_reg_2922_reg[5]\ : in STD_LOGIC;
    \p_Val2_1_reg_2922_reg[4]\ : in STD_LOGIC;
    \p_Val2_1_reg_2922_reg[3]\ : in STD_LOGIC;
    \p_Val2_1_reg_2922_reg[2]\ : in STD_LOGIC;
    \p_Val2_1_reg_2922_reg[1]\ : in STD_LOGIC;
    \p_Val2_1_reg_2922_reg[0]\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_11 : entity is "fifo_w8_d1_A_shiftReg";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_11;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_11 is
  signal \SRL_SIG_reg[0]_0\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \SRL_SIG_reg[1]_1\ : STD_LOGIC_VECTOR ( 7 downto 0 );
begin
\SRL_SIG[0][0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(0),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(0),
      O => img2_data_stream_0_s_dout(0)
    );
\SRL_SIG[0][1]_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(1),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(1),
      O => img2_data_stream_0_s_dout(1)
    );
\SRL_SIG[0][2]_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(2),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(2),
      O => img2_data_stream_0_s_dout(2)
    );
\SRL_SIG[0][3]_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(3),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(3),
      O => img2_data_stream_0_s_dout(3)
    );
\SRL_SIG[0][4]_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(4),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(4),
      O => img2_data_stream_0_s_dout(4)
    );
\SRL_SIG[0][5]_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(5),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(5),
      O => img2_data_stream_0_s_dout(5)
    );
\SRL_SIG[0][6]_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(6),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(6),
      O => img2_data_stream_0_s_dout(6)
    );
\SRL_SIG[0][7]_i_1__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(7),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(7),
      O => img2_data_stream_0_s_dout(7)
    );
\SRL_SIG_reg[0][0]\: unisim.vcomponents.FDSE
     port map (
      C => ap_clk,
      CE => shiftReg_ce_0,
      D => \p_Val2_1_reg_2922_reg[0]\,
      Q => \SRL_SIG_reg[0]_0\(0),
      S => \tmp_57_reg_2927_reg[2]\
    );
\SRL_SIG_reg[0][1]\: unisim.vcomponents.FDSE
     port map (
      C => ap_clk,
      CE => shiftReg_ce_0,
      D => \p_Val2_1_reg_2922_reg[1]\,
      Q => \SRL_SIG_reg[0]_0\(1),
      S => \tmp_57_reg_2927_reg[2]\
    );
\SRL_SIG_reg[0][2]\: unisim.vcomponents.FDSE
     port map (
      C => ap_clk,
      CE => shiftReg_ce_0,
      D => \p_Val2_1_reg_2922_reg[2]\,
      Q => \SRL_SIG_reg[0]_0\(2),
      S => \tmp_57_reg_2927_reg[2]\
    );
\SRL_SIG_reg[0][3]\: unisim.vcomponents.FDSE
     port map (
      C => ap_clk,
      CE => shiftReg_ce_0,
      D => \p_Val2_1_reg_2922_reg[3]\,
      Q => \SRL_SIG_reg[0]_0\(3),
      S => \tmp_57_reg_2927_reg[2]\
    );
\SRL_SIG_reg[0][4]\: unisim.vcomponents.FDSE
     port map (
      C => ap_clk,
      CE => shiftReg_ce_0,
      D => \p_Val2_1_reg_2922_reg[4]\,
      Q => \SRL_SIG_reg[0]_0\(4),
      S => \tmp_57_reg_2927_reg[2]\
    );
\SRL_SIG_reg[0][5]\: unisim.vcomponents.FDSE
     port map (
      C => ap_clk,
      CE => shiftReg_ce_0,
      D => \p_Val2_1_reg_2922_reg[5]\,
      Q => \SRL_SIG_reg[0]_0\(5),
      S => \tmp_57_reg_2927_reg[2]\
    );
\SRL_SIG_reg[0][6]\: unisim.vcomponents.FDSE
     port map (
      C => ap_clk,
      CE => shiftReg_ce_0,
      D => \p_Val2_1_reg_2922_reg[6]\,
      Q => \SRL_SIG_reg[0]_0\(6),
      S => \tmp_57_reg_2927_reg[2]\
    );
\SRL_SIG_reg[0][7]\: unisim.vcomponents.FDSE
     port map (
      C => ap_clk,
      CE => shiftReg_ce_0,
      D => \p_Val2_1_reg_2922_reg[7]\,
      Q => \SRL_SIG_reg[0]_0\(7),
      S => \tmp_57_reg_2927_reg[2]\
    );
\SRL_SIG_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce_0,
      D => \SRL_SIG_reg[0]_0\(0),
      Q => \SRL_SIG_reg[1]_1\(0),
      R => '0'
    );
\SRL_SIG_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce_0,
      D => \SRL_SIG_reg[0]_0\(1),
      Q => \SRL_SIG_reg[1]_1\(1),
      R => '0'
    );
\SRL_SIG_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce_0,
      D => \SRL_SIG_reg[0]_0\(2),
      Q => \SRL_SIG_reg[1]_1\(2),
      R => '0'
    );
\SRL_SIG_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce_0,
      D => \SRL_SIG_reg[0]_0\(3),
      Q => \SRL_SIG_reg[1]_1\(3),
      R => '0'
    );
\SRL_SIG_reg[1][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce_0,
      D => \SRL_SIG_reg[0]_0\(4),
      Q => \SRL_SIG_reg[1]_1\(4),
      R => '0'
    );
\SRL_SIG_reg[1][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce_0,
      D => \SRL_SIG_reg[0]_0\(5),
      Q => \SRL_SIG_reg[1]_1\(5),
      R => '0'
    );
\SRL_SIG_reg[1][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce_0,
      D => \SRL_SIG_reg[0]_0\(6),
      Q => \SRL_SIG_reg[1]_1\(6),
      R => '0'
    );
\SRL_SIG_reg[1][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce_0,
      D => \SRL_SIG_reg[0]_0\(7),
      Q => \SRL_SIG_reg[1]_1\(7),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_12 is
  port (
    \reg_588_reg[7]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \r_V_1_reg_389_reg[29]\ : in STD_LOGIC;
    shiftReg_ce : in STD_LOGIC;
    \p_Val2_15_reg_394_reg[7]\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_12 : entity is "fifo_w8_d1_A_shiftReg";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_12;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_12 is
  signal \SRL_SIG_reg[0]_0\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \SRL_SIG_reg[1]_1\ : STD_LOGIC_VECTOR ( 7 downto 0 );
begin
\SRL_SIG_reg[0][0]\: unisim.vcomponents.FDSE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \p_Val2_15_reg_394_reg[7]\(0),
      Q => \SRL_SIG_reg[0]_0\(0),
      S => \r_V_1_reg_389_reg[29]\
    );
\SRL_SIG_reg[0][1]\: unisim.vcomponents.FDSE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \p_Val2_15_reg_394_reg[7]\(1),
      Q => \SRL_SIG_reg[0]_0\(1),
      S => \r_V_1_reg_389_reg[29]\
    );
\SRL_SIG_reg[0][2]\: unisim.vcomponents.FDSE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \p_Val2_15_reg_394_reg[7]\(2),
      Q => \SRL_SIG_reg[0]_0\(2),
      S => \r_V_1_reg_389_reg[29]\
    );
\SRL_SIG_reg[0][3]\: unisim.vcomponents.FDSE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \p_Val2_15_reg_394_reg[7]\(3),
      Q => \SRL_SIG_reg[0]_0\(3),
      S => \r_V_1_reg_389_reg[29]\
    );
\SRL_SIG_reg[0][4]\: unisim.vcomponents.FDSE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \p_Val2_15_reg_394_reg[7]\(4),
      Q => \SRL_SIG_reg[0]_0\(4),
      S => \r_V_1_reg_389_reg[29]\
    );
\SRL_SIG_reg[0][5]\: unisim.vcomponents.FDSE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \p_Val2_15_reg_394_reg[7]\(5),
      Q => \SRL_SIG_reg[0]_0\(5),
      S => \r_V_1_reg_389_reg[29]\
    );
\SRL_SIG_reg[0][6]\: unisim.vcomponents.FDSE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \p_Val2_15_reg_394_reg[7]\(6),
      Q => \SRL_SIG_reg[0]_0\(6),
      S => \r_V_1_reg_389_reg[29]\
    );
\SRL_SIG_reg[0][7]\: unisim.vcomponents.FDSE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \p_Val2_15_reg_394_reg[7]\(7),
      Q => \SRL_SIG_reg[0]_0\(7),
      S => \r_V_1_reg_389_reg[29]\
    );
\SRL_SIG_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \SRL_SIG_reg[0]_0\(0),
      Q => \SRL_SIG_reg[1]_1\(0),
      R => '0'
    );
\SRL_SIG_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \SRL_SIG_reg[0]_0\(1),
      Q => \SRL_SIG_reg[1]_1\(1),
      R => '0'
    );
\SRL_SIG_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \SRL_SIG_reg[0]_0\(2),
      Q => \SRL_SIG_reg[1]_1\(2),
      R => '0'
    );
\SRL_SIG_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \SRL_SIG_reg[0]_0\(3),
      Q => \SRL_SIG_reg[1]_1\(3),
      R => '0'
    );
\SRL_SIG_reg[1][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \SRL_SIG_reg[0]_0\(4),
      Q => \SRL_SIG_reg[1]_1\(4),
      R => '0'
    );
\SRL_SIG_reg[1][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \SRL_SIG_reg[0]_0\(5),
      Q => \SRL_SIG_reg[1]_1\(5),
      R => '0'
    );
\SRL_SIG_reg[1][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \SRL_SIG_reg[0]_0\(6),
      Q => \SRL_SIG_reg[1]_1\(6),
      R => '0'
    );
\SRL_SIG_reg[1][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => shiftReg_ce,
      D => \SRL_SIG_reg[0]_0\(7),
      Q => \SRL_SIG_reg[1]_1\(7),
      R => '0'
    );
\reg_588[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(0),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(0),
      O => \reg_588_reg[7]\(0)
    );
\reg_588[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(1),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(1),
      O => \reg_588_reg[7]\(1)
    );
\reg_588[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(2),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(2),
      O => \reg_588_reg[7]\(2)
    );
\reg_588[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(3),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(3),
      O => \reg_588_reg[7]\(3)
    );
\reg_588[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(4),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(4),
      O => \reg_588_reg[7]\(4)
    );
\reg_588[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(5),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(5),
      O => \reg_588_reg[7]\(5)
    );
\reg_588[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(6),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(6),
      O => \reg_588_reg[7]\(6)
    );
\reg_588[7]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(7),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(7),
      O => \reg_588_reg[7]\(7)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_13 is
  port (
    B : out STD_LOGIC_VECTOR ( 7 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \exitcond_reg_420_reg[0]\ : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_13 : entity is "fifo_w8_d1_A_shiftReg";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_13;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_13 is
  signal \SRL_SIG_reg[0]_0\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \SRL_SIG_reg[1]_1\ : STD_LOGIC_VECTOR ( 7 downto 0 );
begin
\SRL_SIG_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\,
      D => D(0),
      Q => \SRL_SIG_reg[0]_0\(0),
      R => '0'
    );
\SRL_SIG_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\,
      D => D(1),
      Q => \SRL_SIG_reg[0]_0\(1),
      R => '0'
    );
\SRL_SIG_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\,
      D => D(2),
      Q => \SRL_SIG_reg[0]_0\(2),
      R => '0'
    );
\SRL_SIG_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\,
      D => D(3),
      Q => \SRL_SIG_reg[0]_0\(3),
      R => '0'
    );
\SRL_SIG_reg[0][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\,
      D => D(4),
      Q => \SRL_SIG_reg[0]_0\(4),
      R => '0'
    );
\SRL_SIG_reg[0][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\,
      D => D(5),
      Q => \SRL_SIG_reg[0]_0\(5),
      R => '0'
    );
\SRL_SIG_reg[0][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\,
      D => D(6),
      Q => \SRL_SIG_reg[0]_0\(6),
      R => '0'
    );
\SRL_SIG_reg[0][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\,
      D => D(7),
      Q => \SRL_SIG_reg[0]_0\(7),
      R => '0'
    );
\SRL_SIG_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\,
      D => \SRL_SIG_reg[0]_0\(0),
      Q => \SRL_SIG_reg[1]_1\(0),
      R => '0'
    );
\SRL_SIG_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\,
      D => \SRL_SIG_reg[0]_0\(1),
      Q => \SRL_SIG_reg[1]_1\(1),
      R => '0'
    );
\SRL_SIG_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\,
      D => \SRL_SIG_reg[0]_0\(2),
      Q => \SRL_SIG_reg[1]_1\(2),
      R => '0'
    );
\SRL_SIG_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\,
      D => \SRL_SIG_reg[0]_0\(3),
      Q => \SRL_SIG_reg[1]_1\(3),
      R => '0'
    );
\SRL_SIG_reg[1][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\,
      D => \SRL_SIG_reg[0]_0\(4),
      Q => \SRL_SIG_reg[1]_1\(4),
      R => '0'
    );
\SRL_SIG_reg[1][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\,
      D => \SRL_SIG_reg[0]_0\(5),
      Q => \SRL_SIG_reg[1]_1\(5),
      R => '0'
    );
\SRL_SIG_reg[1][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\,
      D => \SRL_SIG_reg[0]_0\(6),
      Q => \SRL_SIG_reg[1]_1\(6),
      R => '0'
    );
\SRL_SIG_reg[1][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\,
      D => \SRL_SIG_reg[0]_0\(7),
      Q => \SRL_SIG_reg[1]_1\(7),
      R => '0'
    );
p_Val2_12_reg_384_reg_i_10: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(2),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(2),
      O => B(2)
    );
p_Val2_12_reg_384_reg_i_11: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(1),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(1),
      O => B(1)
    );
p_Val2_12_reg_384_reg_i_12: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(0),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(0),
      O => B(0)
    );
p_Val2_12_reg_384_reg_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(7),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(7),
      O => B(7)
    );
p_Val2_12_reg_384_reg_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(6),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(6),
      O => B(6)
    );
p_Val2_12_reg_384_reg_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(5),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(5),
      O => B(5)
    );
p_Val2_12_reg_384_reg_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(4),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(4),
      O => B(4)
    );
p_Val2_12_reg_384_reg_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(3),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(3),
      O => B(3)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_14 is
  port (
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \exitcond_reg_420_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \axi_data_V_1_reg_236_reg[15]\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_14 : entity is "fifo_w8_d1_A_shiftReg";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_14;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_14 is
  signal \SRL_SIG_reg[0]_0\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \SRL_SIG_reg[1]_1\ : STD_LOGIC_VECTOR ( 7 downto 0 );
begin
\SRL_SIG_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \axi_data_V_1_reg_236_reg[15]\(0),
      Q => \SRL_SIG_reg[0]_0\(0),
      R => '0'
    );
\SRL_SIG_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \axi_data_V_1_reg_236_reg[15]\(1),
      Q => \SRL_SIG_reg[0]_0\(1),
      R => '0'
    );
\SRL_SIG_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \axi_data_V_1_reg_236_reg[15]\(2),
      Q => \SRL_SIG_reg[0]_0\(2),
      R => '0'
    );
\SRL_SIG_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \axi_data_V_1_reg_236_reg[15]\(3),
      Q => \SRL_SIG_reg[0]_0\(3),
      R => '0'
    );
\SRL_SIG_reg[0][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \axi_data_V_1_reg_236_reg[15]\(4),
      Q => \SRL_SIG_reg[0]_0\(4),
      R => '0'
    );
\SRL_SIG_reg[0][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \axi_data_V_1_reg_236_reg[15]\(5),
      Q => \SRL_SIG_reg[0]_0\(5),
      R => '0'
    );
\SRL_SIG_reg[0][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \axi_data_V_1_reg_236_reg[15]\(6),
      Q => \SRL_SIG_reg[0]_0\(6),
      R => '0'
    );
\SRL_SIG_reg[0][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \axi_data_V_1_reg_236_reg[15]\(7),
      Q => \SRL_SIG_reg[0]_0\(7),
      R => '0'
    );
\SRL_SIG_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \SRL_SIG_reg[0]_0\(0),
      Q => \SRL_SIG_reg[1]_1\(0),
      R => '0'
    );
\SRL_SIG_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \SRL_SIG_reg[0]_0\(1),
      Q => \SRL_SIG_reg[1]_1\(1),
      R => '0'
    );
\SRL_SIG_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \SRL_SIG_reg[0]_0\(2),
      Q => \SRL_SIG_reg[1]_1\(2),
      R => '0'
    );
\SRL_SIG_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \SRL_SIG_reg[0]_0\(3),
      Q => \SRL_SIG_reg[1]_1\(3),
      R => '0'
    );
\SRL_SIG_reg[1][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \SRL_SIG_reg[0]_0\(4),
      Q => \SRL_SIG_reg[1]_1\(4),
      R => '0'
    );
\SRL_SIG_reg[1][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \SRL_SIG_reg[0]_0\(5),
      Q => \SRL_SIG_reg[1]_1\(5),
      R => '0'
    );
\SRL_SIG_reg[1][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \SRL_SIG_reg[0]_0\(6),
      Q => \SRL_SIG_reg[1]_1\(6),
      R => '0'
    );
\SRL_SIG_reg[1][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \SRL_SIG_reg[0]_0\(7),
      Q => \SRL_SIG_reg[1]_1\(7),
      R => '0'
    );
\tmp_102_reg_369[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(0),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(0),
      O => D(0)
    );
\tmp_102_reg_369[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(1),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(1),
      O => D(1)
    );
\tmp_102_reg_369[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(2),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(2),
      O => D(2)
    );
\tmp_102_reg_369[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(3),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(3),
      O => D(3)
    );
\tmp_102_reg_369[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(4),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(4),
      O => D(4)
    );
\tmp_102_reg_369[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(5),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(5),
      O => D(5)
    );
\tmp_102_reg_369[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(6),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(6),
      O => D(6)
    );
\tmp_102_reg_369[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(7),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(7),
      O => D(7)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_15 is
  port (
    \tmp_101_reg_364_reg[7]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \exitcond_reg_420_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_15 : entity is "fifo_w8_d1_A_shiftReg";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_15;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_15 is
  signal \SRL_SIG_reg[0]_0\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \SRL_SIG_reg[1]_1\ : STD_LOGIC_VECTOR ( 7 downto 0 );
begin
\SRL_SIG_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => D(0),
      Q => \SRL_SIG_reg[0]_0\(0),
      R => '0'
    );
\SRL_SIG_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => D(1),
      Q => \SRL_SIG_reg[0]_0\(1),
      R => '0'
    );
\SRL_SIG_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => D(2),
      Q => \SRL_SIG_reg[0]_0\(2),
      R => '0'
    );
\SRL_SIG_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => D(3),
      Q => \SRL_SIG_reg[0]_0\(3),
      R => '0'
    );
\SRL_SIG_reg[0][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => D(4),
      Q => \SRL_SIG_reg[0]_0\(4),
      R => '0'
    );
\SRL_SIG_reg[0][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => D(5),
      Q => \SRL_SIG_reg[0]_0\(5),
      R => '0'
    );
\SRL_SIG_reg[0][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => D(6),
      Q => \SRL_SIG_reg[0]_0\(6),
      R => '0'
    );
\SRL_SIG_reg[0][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => D(7),
      Q => \SRL_SIG_reg[0]_0\(7),
      R => '0'
    );
\SRL_SIG_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \SRL_SIG_reg[0]_0\(0),
      Q => \SRL_SIG_reg[1]_1\(0),
      R => '0'
    );
\SRL_SIG_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \SRL_SIG_reg[0]_0\(1),
      Q => \SRL_SIG_reg[1]_1\(1),
      R => '0'
    );
\SRL_SIG_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \SRL_SIG_reg[0]_0\(2),
      Q => \SRL_SIG_reg[1]_1\(2),
      R => '0'
    );
\SRL_SIG_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \SRL_SIG_reg[0]_0\(3),
      Q => \SRL_SIG_reg[1]_1\(3),
      R => '0'
    );
\SRL_SIG_reg[1][4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \SRL_SIG_reg[0]_0\(4),
      Q => \SRL_SIG_reg[1]_1\(4),
      R => '0'
    );
\SRL_SIG_reg[1][5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \SRL_SIG_reg[0]_0\(5),
      Q => \SRL_SIG_reg[1]_1\(5),
      R => '0'
    );
\SRL_SIG_reg[1][6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \SRL_SIG_reg[0]_0\(6),
      Q => \SRL_SIG_reg[1]_1\(6),
      R => '0'
    );
\SRL_SIG_reg[1][7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \exitcond_reg_420_reg[0]\(0),
      D => \SRL_SIG_reg[0]_0\(7),
      Q => \SRL_SIG_reg[1]_1\(7),
      R => '0'
    );
\tmp_101_reg_364[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(0),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(0),
      O => \tmp_101_reg_364_reg[7]\(0)
    );
\tmp_101_reg_364[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(1),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(1),
      O => \tmp_101_reg_364_reg[7]\(1)
    );
\tmp_101_reg_364[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(2),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(2),
      O => \tmp_101_reg_364_reg[7]\(2)
    );
\tmp_101_reg_364[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(3),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(3),
      O => \tmp_101_reg_364_reg[7]\(3)
    );
\tmp_101_reg_364[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(4),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(4),
      O => \tmp_101_reg_364_reg[7]\(4)
    );
\tmp_101_reg_364[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(5),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(5),
      O => \tmp_101_reg_364_reg[7]\(5)
    );
\tmp_101_reg_364[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(6),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(6),
      O => \tmp_101_reg_364_reg[7]\(6)
    );
\tmp_101_reg_364[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA8A"
    )
        port map (
      I0 => \SRL_SIG_reg[0]_0\(7),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \SRL_SIG_reg[1]_1\(7),
      O => \tmp_101_reg_364_reg[7]\(7)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_CvtColoocq is
  port (
    start_for_CvtColor_U0_full_n : out STD_LOGIC;
    CvtColor_U0_ap_start : out STD_LOGIC;
    \mOutPtr_reg[1]_0\ : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    start_for_Sobel_U0_full_n : in STD_LOGIC;
    start_once_reg : in STD_LOGIC;
    \ap_CS_fsm_reg[1]\ : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    start_once_reg_0 : in STD_LOGIC;
    ap_rst_n_inv : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_CvtColoocq;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_CvtColoocq is
  signal \^cvtcolor_u0_ap_start\ : STD_LOGIC;
  signal \internal_empty_n_i_1__3_n_2\ : STD_LOGIC;
  signal internal_empty_n_i_2_n_2 : STD_LOGIC;
  signal \internal_full_n_i_1__3_n_2\ : STD_LOGIC;
  signal \internal_full_n_i_2__0_n_2\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1_n_2\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1_n_2\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[0]\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[1]\ : STD_LOGIC;
  signal \^start_for_cvtcolor_u0_full_n\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \internal_full_n_i_2__0\ : label is "soft_lutpair172";
  attribute SOFT_HLUTNM of \mOutPtr[0]_i_1\ : label is "soft_lutpair172";
begin
  CvtColor_U0_ap_start <= \^cvtcolor_u0_ap_start\;
  start_for_CvtColor_U0_full_n <= \^start_for_cvtcolor_u0_full_n\;
\internal_empty_n_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEFFF0000000000"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[1]\,
      I1 => \mOutPtr_reg_n_2_[0]\,
      I2 => \ap_CS_fsm_reg[1]\,
      I3 => internal_empty_n_i_2_n_2,
      I4 => \^cvtcolor_u0_ap_start\,
      I5 => ap_rst_n,
      O => \internal_empty_n_i_1__3_n_2\
    );
internal_empty_n_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^start_for_cvtcolor_u0_full_n\,
      I1 => start_once_reg_0,
      O => internal_empty_n_i_2_n_2
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__3_n_2\,
      Q => \^cvtcolor_u0_ap_start\,
      R => '0'
    );
\internal_full_n_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFD5555"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \mOutPtr_reg_n_2_[1]\,
      I2 => \mOutPtr_reg_n_2_[0]\,
      I3 => start_once_reg_0,
      I4 => \^start_for_cvtcolor_u0_full_n\,
      I5 => \internal_full_n_i_2__0_n_2\,
      O => \internal_full_n_i_1__3_n_2\
    );
\internal_full_n_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^cvtcolor_u0_ap_start\,
      I1 => \ap_CS_fsm_reg[1]\,
      O => \internal_full_n_i_2__0_n_2\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__3_n_2\,
      Q => \^start_for_cvtcolor_u0_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"77878878"
    )
        port map (
      I0 => \^cvtcolor_u0_ap_start\,
      I1 => \ap_CS_fsm_reg[1]\,
      I2 => \^start_for_cvtcolor_u0_full_n\,
      I3 => start_once_reg_0,
      I4 => \mOutPtr_reg_n_2_[0]\,
      O => \mOutPtr[0]_i_1_n_2\
    );
\mOutPtr[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BADFDFDF45202020"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[0]\,
      I1 => start_once_reg_0,
      I2 => \^start_for_cvtcolor_u0_full_n\,
      I3 => \ap_CS_fsm_reg[1]\,
      I4 => \^cvtcolor_u0_ap_start\,
      I5 => \mOutPtr_reg_n_2_[1]\,
      O => \mOutPtr[1]_i_1_n_2\
    );
\mOutPtr[1]_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A8"
    )
        port map (
      I0 => \^cvtcolor_u0_ap_start\,
      I1 => start_for_Sobel_U0_full_n,
      I2 => start_once_reg,
      O => \mOutPtr_reg[1]_0\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[0]_i_1_n_2\,
      Q => \mOutPtr_reg_n_2_[0]\,
      S => ap_rst_n_inv
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[1]_i_1_n_2\,
      Q => \mOutPtr_reg_n_2_[1]\,
      S => ap_rst_n_inv
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_CvtColopcA is
  port (
    start_for_CvtColor_1_U0_full_n : out STD_LOGIC;
    CvtColor_1_U0_ap_start : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    \ap_CS_fsm_reg[1]\ : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    Sobel_U0_ap_start : in STD_LOGIC;
    start_once_reg : in STD_LOGIC;
    ap_rst_n_inv : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_CvtColopcA;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_CvtColopcA is
  signal \^cvtcolor_1_u0_ap_start\ : STD_LOGIC;
  signal \internal_empty_n_i_1__10_n_2\ : STD_LOGIC;
  signal \internal_full_n_i_1__10_n_2\ : STD_LOGIC;
  signal \internal_full_n_i_2__1_n_2\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1_n_2\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1_n_2\ : STD_LOGIC;
  signal \mOutPtr[1]_i_2__3_n_2\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[0]\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[1]\ : STD_LOGIC;
  signal \^start_for_cvtcolor_1_u0_full_n\ : STD_LOGIC;
begin
  CvtColor_1_U0_ap_start <= \^cvtcolor_1_u0_ap_start\;
  start_for_CvtColor_1_U0_full_n <= \^start_for_cvtcolor_1_u0_full_n\;
\internal_empty_n_i_1__10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEFFF0000000000"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[1]\,
      I1 => \mOutPtr_reg_n_2_[0]\,
      I2 => \ap_CS_fsm_reg[1]\,
      I3 => \internal_full_n_i_2__1_n_2\,
      I4 => \^cvtcolor_1_u0_ap_start\,
      I5 => ap_rst_n,
      O => \internal_empty_n_i_1__10_n_2\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__10_n_2\,
      Q => \^cvtcolor_1_u0_ap_start\,
      R => '0'
    );
\internal_full_n_i_1__10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDDDFFFFDDD5DDDD"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \^start_for_cvtcolor_1_u0_full_n\,
      I2 => \mOutPtr_reg_n_2_[1]\,
      I3 => \mOutPtr_reg_n_2_[0]\,
      I4 => \internal_full_n_i_2__1_n_2\,
      I5 => \mOutPtr[1]_i_2__3_n_2\,
      O => \internal_full_n_i_1__10_n_2\
    );
\internal_full_n_i_2__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^start_for_cvtcolor_1_u0_full_n\,
      I1 => Sobel_U0_ap_start,
      I2 => start_once_reg,
      O => \internal_full_n_i_2__1_n_2\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__10_n_2\,
      Q => \^start_for_cvtcolor_1_u0_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7777877788887888"
    )
        port map (
      I0 => \^cvtcolor_1_u0_ap_start\,
      I1 => \ap_CS_fsm_reg[1]\,
      I2 => \^start_for_cvtcolor_1_u0_full_n\,
      I3 => Sobel_U0_ap_start,
      I4 => start_once_reg,
      I5 => \mOutPtr_reg_n_2_[0]\,
      O => \mOutPtr[0]_i_1_n_2\
    );
\mOutPtr[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BAAADFFF45552000"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[0]\,
      I1 => start_once_reg,
      I2 => Sobel_U0_ap_start,
      I3 => \^start_for_cvtcolor_1_u0_full_n\,
      I4 => \mOutPtr[1]_i_2__3_n_2\,
      I5 => \mOutPtr_reg_n_2_[1]\,
      O => \mOutPtr[1]_i_1_n_2\
    );
\mOutPtr[1]_i_2__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^cvtcolor_1_u0_ap_start\,
      I1 => \ap_CS_fsm_reg[1]\,
      O => \mOutPtr[1]_i_2__3_n_2\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[0]_i_1_n_2\,
      Q => \mOutPtr_reg_n_2_[0]\,
      S => ap_rst_n_inv
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[1]_i_1_n_2\,
      Q => \mOutPtr_reg_n_2_[1]\,
      S => ap_rst_n_inv
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Mat2AXIqcK is
  port (
    start_for_Mat2AXIvideo_U0_full_n : out STD_LOGIC;
    Mat2AXIvideo_U0_ap_start : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    internal_empty_n_reg_0 : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    CvtColor_1_U0_ap_start : in STD_LOGIC;
    start_once_reg : in STD_LOGIC;
    ap_rst_n_inv : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Mat2AXIqcK;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Mat2AXIqcK is
  signal \^mat2axivideo_u0_ap_start\ : STD_LOGIC;
  signal \internal_empty_n_i_1__11_n_2\ : STD_LOGIC;
  signal \internal_full_n_i_1__11_n_2\ : STD_LOGIC;
  signal \internal_full_n_i_2__2_n_2\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1_n_2\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1_n_2\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[0]\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[1]\ : STD_LOGIC;
  signal \^start_for_mat2axivideo_u0_full_n\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \internal_full_n_i_2__2\ : label is "soft_lutpair173";
  attribute SOFT_HLUTNM of \mOutPtr[0]_i_1\ : label is "soft_lutpair173";
begin
  Mat2AXIvideo_U0_ap_start <= \^mat2axivideo_u0_ap_start\;
  start_for_Mat2AXIvideo_U0_full_n <= \^start_for_mat2axivideo_u0_full_n\;
\internal_empty_n_i_1__11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F0000000000"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[1]\,
      I1 => \mOutPtr_reg_n_2_[0]\,
      I2 => internal_empty_n_reg_0,
      I3 => \internal_full_n_i_2__2_n_2\,
      I4 => \^mat2axivideo_u0_ap_start\,
      I5 => ap_rst_n,
      O => \internal_empty_n_i_1__11_n_2\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__11_n_2\,
      Q => \^mat2axivideo_u0_ap_start\,
      R => '0'
    );
\internal_full_n_i_1__11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDDDFFFFDDD5DDDD"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \^start_for_mat2axivideo_u0_full_n\,
      I2 => \mOutPtr_reg_n_2_[1]\,
      I3 => \mOutPtr_reg_n_2_[0]\,
      I4 => \internal_full_n_i_2__2_n_2\,
      I5 => internal_empty_n_reg_0,
      O => \internal_full_n_i_1__11_n_2\
    );
\internal_full_n_i_2__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^start_for_mat2axivideo_u0_full_n\,
      I1 => CvtColor_1_U0_ap_start,
      I2 => start_once_reg,
      O => \internal_full_n_i_2__2_n_2\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__11_n_2\,
      Q => \^start_for_mat2axivideo_u0_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5595AA6A"
    )
        port map (
      I0 => internal_empty_n_reg_0,
      I1 => \^start_for_mat2axivideo_u0_full_n\,
      I2 => CvtColor_1_U0_ap_start,
      I3 => start_once_reg,
      I4 => \mOutPtr_reg_n_2_[0]\,
      O => \mOutPtr[0]_i_1_n_2\
    );
\mOutPtr[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BAAADFFF45552000"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[0]\,
      I1 => start_once_reg,
      I2 => CvtColor_1_U0_ap_start,
      I3 => \^start_for_mat2axivideo_u0_full_n\,
      I4 => internal_empty_n_reg_0,
      I5 => \mOutPtr_reg_n_2_[1]\,
      O => \mOutPtr[1]_i_1_n_2\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[0]_i_1_n_2\,
      Q => \mOutPtr_reg_n_2_[0]\,
      S => ap_rst_n_inv
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[1]_i_1_n_2\,
      Q => \mOutPtr_reg_n_2_[1]\,
      S => ap_rst_n_inv
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Sobel_U0 is
  port (
    start_for_Sobel_U0_full_n : out STD_LOGIC;
    Sobel_U0_ap_start : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    \ap_CS_fsm_reg[1]\ : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    CvtColor_U0_ap_start : in STD_LOGIC;
    start_once_reg : in STD_LOGIC;
    internal_empty_n_reg_0 : in STD_LOGIC;
    ap_rst_n_inv : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Sobel_U0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Sobel_U0 is
  signal \^sobel_u0_ap_start\ : STD_LOGIC;
  signal \internal_empty_n_i_1__2_n_2\ : STD_LOGIC;
  signal \internal_full_n_i_1__2_n_2\ : STD_LOGIC;
  signal \internal_full_n_i_2__6_n_2\ : STD_LOGIC;
  signal internal_full_n_i_3_n_2 : STD_LOGIC;
  signal \mOutPtr[0]_i_1_n_2\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1_n_2\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[0]\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[1]\ : STD_LOGIC;
  signal \^start_for_sobel_u0_full_n\ : STD_LOGIC;
begin
  Sobel_U0_ap_start <= \^sobel_u0_ap_start\;
  start_for_Sobel_U0_full_n <= \^start_for_sobel_u0_full_n\;
\internal_empty_n_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEFFF0000000000"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[1]\,
      I1 => \mOutPtr_reg_n_2_[0]\,
      I2 => \ap_CS_fsm_reg[1]\,
      I3 => internal_full_n_i_3_n_2,
      I4 => \^sobel_u0_ap_start\,
      I5 => ap_rst_n,
      O => \internal_empty_n_i_1__2_n_2\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__2_n_2\,
      Q => \^sobel_u0_ap_start\,
      R => '0'
    );
\internal_full_n_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDFFD5DDD5DDD5DD"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \^start_for_sobel_u0_full_n\,
      I2 => \internal_full_n_i_2__6_n_2\,
      I3 => internal_full_n_i_3_n_2,
      I4 => \ap_CS_fsm_reg[1]\,
      I5 => \^sobel_u0_ap_start\,
      O => \internal_full_n_i_1__2_n_2\
    );
\internal_full_n_i_2__6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[1]\,
      I1 => \mOutPtr_reg_n_2_[0]\,
      O => \internal_full_n_i_2__6_n_2\
    );
internal_full_n_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^start_for_sobel_u0_full_n\,
      I1 => CvtColor_U0_ap_start,
      I2 => start_once_reg,
      O => internal_full_n_i_3_n_2
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__2_n_2\,
      Q => \^start_for_sobel_u0_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7777877788887888"
    )
        port map (
      I0 => \^sobel_u0_ap_start\,
      I1 => \ap_CS_fsm_reg[1]\,
      I2 => \^start_for_sobel_u0_full_n\,
      I3 => CvtColor_U0_ap_start,
      I4 => start_once_reg,
      I5 => \mOutPtr_reg_n_2_[0]\,
      O => \mOutPtr[0]_i_1_n_2\
    );
\mOutPtr[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BADFDFDF45202020"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[0]\,
      I1 => start_once_reg,
      I2 => internal_empty_n_reg_0,
      I3 => \ap_CS_fsm_reg[1]\,
      I4 => \^sobel_u0_ap_start\,
      I5 => \mOutPtr_reg_n_2_[1]\,
      O => \mOutPtr[1]_i_1_n_2\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[0]_i_1_n_2\,
      Q => \mOutPtr_reg_n_2_[0]\,
      S => ap_rst_n_inv
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \mOutPtr[1]_i_1_n_2\,
      Q => \mOutPtr_reg_n_2_[1]\,
      S => ap_rst_n_inv
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg is
  port (
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ram_reg : out STD_LOGIC;
    col_buf_0_val_0_0_reg_27080 : out STD_LOGIC;
    ap_block_pp0_stage0_subdone2_in : out STD_LOGIC;
    \ap_reg_pp0_iter2_reg_588_reg[0]\ : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    \k_buf_0_val_3_addr_reg_2649_reg[10]\ : in STD_LOGIC_VECTOR ( 10 downto 0 );
    ADDRBWRADDR : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_enable_reg_pp0_iter2 : in STD_LOGIC;
    ap_reg_pp0_iter1_or_cond_i_i_reg_2607 : in STD_LOGIC;
    \icmp_reg_2509_reg[0]\ : in STD_LOGIC;
    tmp_1_reg_2500 : in STD_LOGIC;
    \tmp_2_reg_2514_reg[0]\ : in STD_LOGIC;
    \ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg[0]\ : in STD_LOGIC;
    img1_data_stream_0_s_empty_n : in STD_LOGIC;
    img1_data_stream_2_s_empty_n : in STD_LOGIC;
    img1_data_stream_1_s_empty_n : in STD_LOGIC;
    ap_enable_reg_pp0_iter1 : in STD_LOGIC;
    ap_reg_pp0_iter4_or_cond_i_reg_2640 : in STD_LOGIC;
    ap_enable_reg_pp0_iter5_reg : in STD_LOGIC;
    img2_data_stream_0_s_full_n : in STD_LOGIC;
    img2_data_stream_1_s_full_n : in STD_LOGIC;
    img2_data_stream_2_s_full_n : in STD_LOGIC;
    \exitcond389_i_reg_2583_reg[0]\ : in STD_LOGIC;
    or_cond_i_i_reg_2607 : in STD_LOGIC;
    \ap_CS_fsm_reg[4]\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg is
begin
Filter2D_k_buf_0_eOg_ram_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_ram_19
     port map (
      ADDRBWRADDR(10 downto 0) => ADDRBWRADDR(10 downto 0),
      D(7 downto 0) => D(7 downto 0),
      Q(7 downto 0) => Q(7 downto 0),
      \ap_CS_fsm_reg[4]\(0) => \ap_CS_fsm_reg[4]\(0),
      ap_block_pp0_stage0_subdone2_in => ap_block_pp0_stage0_subdone2_in,
      ap_clk => ap_clk,
      ap_enable_reg_pp0_iter1 => ap_enable_reg_pp0_iter1,
      ap_enable_reg_pp0_iter2 => ap_enable_reg_pp0_iter2,
      ap_enable_reg_pp0_iter5_reg => ap_enable_reg_pp0_iter5_reg,
      \ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg[0]\ => \ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg[0]\,
      ap_reg_pp0_iter1_or_cond_i_i_reg_2607 => ap_reg_pp0_iter1_or_cond_i_i_reg_2607,
      \ap_reg_pp0_iter2_reg_588_reg[0]\ => \ap_reg_pp0_iter2_reg_588_reg[0]\,
      ap_reg_pp0_iter4_or_cond_i_reg_2640 => ap_reg_pp0_iter4_or_cond_i_reg_2640,
      col_buf_0_val_0_0_reg_27080 => col_buf_0_val_0_0_reg_27080,
      \exitcond389_i_reg_2583_reg[0]\ => \exitcond389_i_reg_2583_reg[0]\,
      \icmp_reg_2509_reg[0]\ => \icmp_reg_2509_reg[0]\,
      img1_data_stream_0_s_empty_n => img1_data_stream_0_s_empty_n,
      img1_data_stream_1_s_empty_n => img1_data_stream_1_s_empty_n,
      img1_data_stream_2_s_empty_n => img1_data_stream_2_s_empty_n,
      img2_data_stream_0_s_full_n => img2_data_stream_0_s_full_n,
      img2_data_stream_1_s_full_n => img2_data_stream_1_s_full_n,
      img2_data_stream_2_s_full_n => img2_data_stream_2_s_full_n,
      \k_buf_0_val_3_addr_reg_2649_reg[10]\(10 downto 0) => \k_buf_0_val_3_addr_reg_2649_reg[10]\(10 downto 0),
      or_cond_i_i_reg_2607 => or_cond_i_i_reg_2607,
      ram_reg_0 => ram_reg,
      tmp_1_reg_2500 => tmp_1_reg_2500,
      \tmp_2_reg_2514_reg[0]\ => \tmp_2_reg_2514_reg[0]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_16 is
  port (
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC;
    \ap_CS_fsm_reg[4]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 10 downto 0 );
    ADDRBWRADDR : in STD_LOGIC_VECTOR ( 10 downto 0 );
    ap_enable_reg_pp0_iter3 : in STD_LOGIC;
    ap_block_pp0_stage0_subdone2_in : in STD_LOGIC;
    ap_reg_pp0_iter2_or_cond_i_i_reg_2607 : in STD_LOGIC;
    \icmp_reg_2509_reg[0]\ : in STD_LOGIC;
    tmp_1_reg_2500 : in STD_LOGIC;
    \tmp_116_0_1_reg_2518_reg[0]\ : in STD_LOGIC;
    \k_buf_0_val_3_load_reg_2703_reg[7]\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \ap_reg_pp0_iter2_reg_588_reg[7]\ : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_16 : entity is "Filter2D_k_buf_0_eOg";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_16;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_16 is
begin
Filter2D_k_buf_0_eOg_ram_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_ram_18
     port map (
      ADDRBWRADDR(10 downto 0) => ADDRBWRADDR(10 downto 0),
      D(7 downto 0) => D(7 downto 0),
      Q(10 downto 0) => Q(10 downto 0),
      \ap_CS_fsm_reg[4]\ => \ap_CS_fsm_reg[4]\,
      ap_block_pp0_stage0_subdone2_in => ap_block_pp0_stage0_subdone2_in,
      ap_clk => ap_clk,
      ap_enable_reg_pp0_iter3 => ap_enable_reg_pp0_iter3,
      ap_reg_pp0_iter2_or_cond_i_i_reg_2607 => ap_reg_pp0_iter2_or_cond_i_i_reg_2607,
      \ap_reg_pp0_iter2_reg_588_reg[7]\(7 downto 0) => \ap_reg_pp0_iter2_reg_588_reg[7]\(7 downto 0),
      \icmp_reg_2509_reg[0]\ => \icmp_reg_2509_reg[0]\,
      \k_buf_0_val_3_load_reg_2703_reg[7]\(7 downto 0) => \k_buf_0_val_3_load_reg_2703_reg[7]\(7 downto 0),
      \tmp_116_0_1_reg_2518_reg[0]\ => \tmp_116_0_1_reg_2518_reg[0]\,
      tmp_1_reg_2500 => tmp_1_reg_2500
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_17 is
  port (
    DOBDO : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ADDRBWRADDR : out STD_LOGIC_VECTOR ( 9 downto 0 );
    ap_clk : in STD_LOGIC;
    \ap_CS_fsm_reg[4]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 10 downto 0 );
    p_assign_2_reg_2622 : in STD_LOGIC_VECTOR ( 10 downto 0 );
    ap_enable_reg_pp0_iter3 : in STD_LOGIC;
    ap_block_pp0_stage0_subdone2_in : in STD_LOGIC;
    ap_reg_pp0_iter2_or_cond_i_i_reg_2607 : in STD_LOGIC;
    \icmp_reg_2509_reg[0]\ : in STD_LOGIC;
    tmp_1_reg_2500 : in STD_LOGIC;
    \tmp_2_reg_2514_reg[0]\ : in STD_LOGIC;
    \k_buf_0_val_4_load_reg_2716_reg[7]\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \ap_reg_pp0_iter2_reg_588_reg[7]\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \p_p2_i_i_cast_cast_reg_2612_reg[10]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \ImagLoc_x_reg_2592_reg[10]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    or_cond_i_i_reg_2607 : in STD_LOGIC;
    tmp_33_reg_2617 : in STD_LOGIC;
    tmp_47_reg_2597 : in STD_LOGIC;
    tmp_31_reg_2602 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_17 : entity is "Filter2D_k_buf_0_eOg";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_17;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_17 is
begin
Filter2D_k_buf_0_eOg_ram_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_ram
     port map (
      ADDRBWRADDR(9 downto 0) => ADDRBWRADDR(9 downto 0),
      DOBDO(7 downto 0) => DOBDO(7 downto 0),
      \ImagLoc_x_reg_2592_reg[10]\(9 downto 0) => \ImagLoc_x_reg_2592_reg[10]\(9 downto 0),
      Q(10 downto 0) => Q(10 downto 0),
      \ap_CS_fsm_reg[4]\ => \ap_CS_fsm_reg[4]\,
      ap_block_pp0_stage0_subdone2_in => ap_block_pp0_stage0_subdone2_in,
      ap_clk => ap_clk,
      ap_enable_reg_pp0_iter3 => ap_enable_reg_pp0_iter3,
      ap_reg_pp0_iter2_or_cond_i_i_reg_2607 => ap_reg_pp0_iter2_or_cond_i_i_reg_2607,
      \ap_reg_pp0_iter2_reg_588_reg[7]\(7 downto 0) => \ap_reg_pp0_iter2_reg_588_reg[7]\(7 downto 0),
      \icmp_reg_2509_reg[0]\ => \icmp_reg_2509_reg[0]\,
      \k_buf_0_val_4_load_reg_2716_reg[7]\(7 downto 0) => \k_buf_0_val_4_load_reg_2716_reg[7]\(7 downto 0),
      or_cond_i_i_reg_2607 => or_cond_i_i_reg_2607,
      p_assign_2_reg_2622(10 downto 0) => p_assign_2_reg_2622(10 downto 0),
      \p_p2_i_i_cast_cast_reg_2612_reg[10]\(9 downto 0) => \p_p2_i_i_cast_cast_reg_2612_reg[10]\(9 downto 0),
      tmp_1_reg_2500 => tmp_1_reg_2500,
      \tmp_2_reg_2514_reg[0]\ => \tmp_2_reg_2514_reg[0]\,
      tmp_31_reg_2602 => tmp_31_reg_2602,
      tmp_33_reg_2617 => tmp_33_reg_2617,
      tmp_47_reg_2597 => tmp_47_reg_2597
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_edge_detect_mac_mdEe is
  port (
    P : out STD_LOGIC_VECTOR ( 8 downto 0 );
    ap_block_pp0_stage0_subdone3_in : out STD_LOGIC;
    \r_V_1_reg_389_reg[29]\ : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 7 downto 0 );
    p_Val2_12_reg_384_reg : in STD_LOGIC_VECTOR ( 28 downto 0 );
    tmp_20_reg_355 : in STD_LOGIC;
    ap_enable_reg_pp0_iter1_reg : in STD_LOGIC;
    img0_data_stream_2_s_empty_n : in STD_LOGIC;
    img0_data_stream_0_s_empty_n : in STD_LOGIC;
    img0_data_stream_1_s_empty_n : in STD_LOGIC;
    ap_reg_pp0_iter4_tmp_20_reg_355 : in STD_LOGIC;
    ap_enable_reg_pp0_iter5_reg : in STD_LOGIC;
    img1_data_stream_0_s_full_n : in STD_LOGIC;
    img1_data_stream_1_s_full_n : in STD_LOGIC;
    img1_data_stream_2_s_full_n : in STD_LOGIC;
    ap_reg_pp0_iter3_tmp_20_reg_355 : in STD_LOGIC;
    ap_enable_reg_pp0_iter4 : in STD_LOGIC;
    tmp_98_fu_287_p3 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_edge_detect_mac_mdEe;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_edge_detect_mac_mdEe is
begin
edge_detect_mac_mdEe_DSP48_2_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_edge_detect_mac_mdEe_DSP48_2
     port map (
      P(8 downto 0) => P(8 downto 0),
      Q(7 downto 0) => Q(7 downto 0),
      ap_clk => ap_clk,
      ap_enable_reg_pp0_iter1_reg => ap_enable_reg_pp0_iter1_reg,
      ap_enable_reg_pp0_iter4 => ap_enable_reg_pp0_iter4,
      ap_enable_reg_pp0_iter5_reg => ap_enable_reg_pp0_iter5_reg,
      ap_reg_pp0_iter3_tmp_20_reg_355 => ap_reg_pp0_iter3_tmp_20_reg_355,
      ap_reg_pp0_iter4_tmp_20_reg_355 => ap_reg_pp0_iter4_tmp_20_reg_355,
      img0_data_stream_0_s_empty_n => img0_data_stream_0_s_empty_n,
      img0_data_stream_1_s_empty_n => img0_data_stream_1_s_empty_n,
      img0_data_stream_2_s_empty_n => img0_data_stream_2_s_empty_n,
      img1_data_stream_0_s_full_n => img1_data_stream_0_s_full_n,
      img1_data_stream_1_s_full_n => img1_data_stream_1_s_full_n,
      img1_data_stream_2_s_full_n => img1_data_stream_2_s_full_n,
      p_0 => ap_block_pp0_stage0_subdone3_in,
      p_Val2_12_reg_384_reg(28 downto 0) => p_Val2_12_reg_384_reg(28 downto 0),
      \r_V_1_reg_389_reg[29]\ => \r_V_1_reg_389_reg[29]\,
      tmp_20_reg_355 => tmp_20_reg_355,
      tmp_98_fu_287_p3 => tmp_98_fu_287_p3
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_edge_detect_mul_mbkb is
  port (
    \out\ : out STD_LOGIC_VECTOR ( 28 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_edge_detect_mul_mbkb;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_edge_detect_mul_mbkb is
begin
edge_detect_mul_mbkb_DSP48_0_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_edge_detect_mul_mbkb_DSP48_0
     port map (
      Q(7 downto 0) => Q(7 downto 0),
      \out\(28 downto 0) => \out\(28 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A is
  port (
    img0_data_stream_0_s_full_n : out STD_LOGIC;
    img0_data_stream_0_s_empty_n : out STD_LOGIC;
    \tmp_101_reg_364_reg[7]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC;
    internal_empty_n4_out : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    \tmp_20_reg_355_reg[0]\ : in STD_LOGIC;
    ap_rst_n_inv : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \exitcond_reg_420_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A is
  signal \^img0_data_stream_0_s_empty_n\ : STD_LOGIC;
  signal \^img0_data_stream_0_s_full_n\ : STD_LOGIC;
  signal \internal_empty_n_i_1__1_n_2\ : STD_LOGIC;
  signal internal_full_n_i_1_n_2 : STD_LOGIC;
  signal \mOutPtr[0]_i_1_n_2\ : STD_LOGIC;
  signal \mOutPtr[1]_i_2_n_2\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[0]\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[1]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \mOutPtr[0]_i_1\ : label is "soft_lutpair160";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_2\ : label is "soft_lutpair160";
begin
  img0_data_stream_0_s_empty_n <= \^img0_data_stream_0_s_empty_n\;
  img0_data_stream_0_s_full_n <= \^img0_data_stream_0_s_full_n\;
U_fifo_w8_d1_A_ram: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_15
     port map (
      D(7 downto 0) => D(7 downto 0),
      Q(1) => \mOutPtr_reg_n_2_[1]\,
      Q(0) => \mOutPtr_reg_n_2_[0]\,
      ap_clk => ap_clk,
      \exitcond_reg_420_reg[0]\(0) => \exitcond_reg_420_reg[0]\(0),
      \tmp_101_reg_364_reg[7]\(7 downto 0) => \tmp_101_reg_364_reg[7]\(7 downto 0)
    );
\internal_empty_n_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FD00FD00FD000000"
    )
        port map (
      I0 => \tmp_20_reg_355_reg[0]\,
      I1 => \mOutPtr_reg_n_2_[1]\,
      I2 => \mOutPtr_reg_n_2_[0]\,
      I3 => ap_rst_n,
      I4 => internal_empty_n4_out,
      I5 => \^img0_data_stream_0_s_empty_n\,
      O => \internal_empty_n_i_1__1_n_2\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__1_n_2\,
      Q => \^img0_data_stream_0_s_empty_n\,
      R => '0'
    );
internal_full_n_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFD00FFFF"
    )
        port map (
      I0 => internal_empty_n4_out,
      I1 => \mOutPtr_reg_n_2_[0]\,
      I2 => \mOutPtr_reg_n_2_[1]\,
      I3 => \^img0_data_stream_0_s_full_n\,
      I4 => ap_rst_n,
      I5 => \tmp_20_reg_355_reg[0]\,
      O => internal_full_n_i_1_n_2
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => internal_full_n_i_1_n_2,
      Q => \^img0_data_stream_0_s_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[0]\,
      O => \mOutPtr[0]_i_1_n_2\
    );
\mOutPtr[1]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \tmp_20_reg_355_reg[0]\,
      I1 => \mOutPtr_reg_n_2_[0]\,
      I2 => \mOutPtr_reg_n_2_[1]\,
      O => \mOutPtr[1]_i_2_n_2\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => \mOutPtr[0]_i_1_n_2\,
      Q => \mOutPtr_reg_n_2_[0]\,
      S => ap_rst_n_inv
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => \mOutPtr[1]_i_2_n_2\,
      Q => \mOutPtr_reg_n_2_[1]\,
      S => ap_rst_n_inv
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_0 is
  port (
    img0_data_stream_1_s_full_n : out STD_LOGIC;
    img0_data_stream_1_s_empty_n : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC;
    internal_empty_n4_out : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    \tmp_20_reg_355_reg[0]\ : in STD_LOGIC;
    ap_rst_n_inv : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \exitcond_reg_420_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \axi_data_V_1_reg_236_reg[15]\ : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_0 : entity is "fifo_w8_d1_A";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_0 is
  signal \^img0_data_stream_1_s_empty_n\ : STD_LOGIC;
  signal \^img0_data_stream_1_s_full_n\ : STD_LOGIC;
  signal \internal_empty_n_i_1__0_n_2\ : STD_LOGIC;
  signal \internal_full_n_i_1__0_n_2\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1__0_n_2\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1__1_n_2\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[0]\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[1]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \mOutPtr[0]_i_1__0\ : label is "soft_lutpair161";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_1__1\ : label is "soft_lutpair161";
begin
  img0_data_stream_1_s_empty_n <= \^img0_data_stream_1_s_empty_n\;
  img0_data_stream_1_s_full_n <= \^img0_data_stream_1_s_full_n\;
U_fifo_w8_d1_A_ram: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_14
     port map (
      D(7 downto 0) => D(7 downto 0),
      Q(1) => \mOutPtr_reg_n_2_[1]\,
      Q(0) => \mOutPtr_reg_n_2_[0]\,
      ap_clk => ap_clk,
      \axi_data_V_1_reg_236_reg[15]\(7 downto 0) => \axi_data_V_1_reg_236_reg[15]\(7 downto 0),
      \exitcond_reg_420_reg[0]\(0) => \exitcond_reg_420_reg[0]\(0)
    );
\internal_empty_n_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FD00FD00FD000000"
    )
        port map (
      I0 => \tmp_20_reg_355_reg[0]\,
      I1 => \mOutPtr_reg_n_2_[1]\,
      I2 => \mOutPtr_reg_n_2_[0]\,
      I3 => ap_rst_n,
      I4 => internal_empty_n4_out,
      I5 => \^img0_data_stream_1_s_empty_n\,
      O => \internal_empty_n_i_1__0_n_2\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__0_n_2\,
      Q => \^img0_data_stream_1_s_empty_n\,
      R => '0'
    );
\internal_full_n_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFD00FFFF"
    )
        port map (
      I0 => internal_empty_n4_out,
      I1 => \mOutPtr_reg_n_2_[0]\,
      I2 => \mOutPtr_reg_n_2_[1]\,
      I3 => \^img0_data_stream_1_s_full_n\,
      I4 => ap_rst_n,
      I5 => \tmp_20_reg_355_reg[0]\,
      O => \internal_full_n_i_1__0_n_2\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__0_n_2\,
      Q => \^img0_data_stream_1_s_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[0]\,
      O => \mOutPtr[0]_i_1__0_n_2\
    );
\mOutPtr[1]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \tmp_20_reg_355_reg[0]\,
      I1 => \mOutPtr_reg_n_2_[0]\,
      I2 => \mOutPtr_reg_n_2_[1]\,
      O => \mOutPtr[1]_i_1__1_n_2\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => \mOutPtr[0]_i_1__0_n_2\,
      Q => \mOutPtr_reg_n_2_[0]\,
      S => ap_rst_n_inv
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => \mOutPtr[1]_i_1__1_n_2\,
      Q => \mOutPtr_reg_n_2_[1]\,
      S => ap_rst_n_inv
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_1 is
  port (
    img0_data_stream_2_s_full_n : out STD_LOGIC;
    img0_data_stream_2_s_empty_n : out STD_LOGIC;
    B : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC;
    internal_empty_n4_out : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    \tmp_20_reg_355_reg[0]\ : in STD_LOGIC;
    ap_rst_n_inv : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \exitcond_reg_420_reg[0]\ : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_1 : entity is "fifo_w8_d1_A";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_1;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_1 is
  signal \^img0_data_stream_2_s_empty_n\ : STD_LOGIC;
  signal \^img0_data_stream_2_s_full_n\ : STD_LOGIC;
  signal internal_empty_n_i_1_n_2 : STD_LOGIC;
  signal \internal_full_n_i_1__1_n_2\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1__1_n_2\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1__0_n_2\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[0]\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[1]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \mOutPtr[0]_i_1__1\ : label is "soft_lutpair162";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_1__0\ : label is "soft_lutpair162";
begin
  img0_data_stream_2_s_empty_n <= \^img0_data_stream_2_s_empty_n\;
  img0_data_stream_2_s_full_n <= \^img0_data_stream_2_s_full_n\;
U_fifo_w8_d1_A_ram: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_13
     port map (
      B(7 downto 0) => B(7 downto 0),
      D(7 downto 0) => D(7 downto 0),
      Q(1) => \mOutPtr_reg_n_2_[1]\,
      Q(0) => \mOutPtr_reg_n_2_[0]\,
      ap_clk => ap_clk,
      \exitcond_reg_420_reg[0]\ => \exitcond_reg_420_reg[0]\
    );
internal_empty_n_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FD00FD00FD000000"
    )
        port map (
      I0 => \tmp_20_reg_355_reg[0]\,
      I1 => \mOutPtr_reg_n_2_[1]\,
      I2 => \mOutPtr_reg_n_2_[0]\,
      I3 => ap_rst_n,
      I4 => internal_empty_n4_out,
      I5 => \^img0_data_stream_2_s_empty_n\,
      O => internal_empty_n_i_1_n_2
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => internal_empty_n_i_1_n_2,
      Q => \^img0_data_stream_2_s_empty_n\,
      R => '0'
    );
\internal_full_n_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFD00FFFF"
    )
        port map (
      I0 => internal_empty_n4_out,
      I1 => \mOutPtr_reg_n_2_[0]\,
      I2 => \mOutPtr_reg_n_2_[1]\,
      I3 => \^img0_data_stream_2_s_full_n\,
      I4 => ap_rst_n,
      I5 => \tmp_20_reg_355_reg[0]\,
      O => \internal_full_n_i_1__1_n_2\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__1_n_2\,
      Q => \^img0_data_stream_2_s_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[0]\,
      O => \mOutPtr[0]_i_1__1_n_2\
    );
\mOutPtr[1]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \tmp_20_reg_355_reg[0]\,
      I1 => \mOutPtr_reg_n_2_[0]\,
      I2 => \mOutPtr_reg_n_2_[1]\,
      O => \mOutPtr[1]_i_1__0_n_2\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => \mOutPtr[0]_i_1__1_n_2\,
      Q => \mOutPtr_reg_n_2_[0]\,
      S => ap_rst_n_inv
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => \mOutPtr[1]_i_1__0_n_2\,
      Q => \mOutPtr_reg_n_2_[1]\,
      S => ap_rst_n_inv
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_10 is
  port (
    img3_data_stream_2_s_full_n : out STD_LOGIC;
    img3_data_stream_2_s_empty_n : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 23 downto 0 );
    shiftReg_ce : in STD_LOGIC;
    img2_data_stream_0_s_dout : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    \ap_CS_fsm_reg[2]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \mOutPtr_reg[1]_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ap_rst_n_inv : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_10 : entity is "fifo_w8_d1_A";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_10;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_10 is
  signal \^img3_data_stream_2_s_empty_n\ : STD_LOGIC;
  signal \^img3_data_stream_2_s_full_n\ : STD_LOGIC;
  signal \internal_empty_n_i_1__13_n_2\ : STD_LOGIC;
  signal \internal_full_n_i_1__14_n_2\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1__10_n_2\ : STD_LOGIC;
  signal \mOutPtr[1]_i_1__8_n_2\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[0]\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[1]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \mOutPtr[0]_i_1__10\ : label is "soft_lutpair171";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_1__8\ : label is "soft_lutpair171";
begin
  img3_data_stream_2_s_empty_n <= \^img3_data_stream_2_s_empty_n\;
  img3_data_stream_2_s_full_n <= \^img3_data_stream_2_s_full_n\;
U_fifo_w8_d1_A_ram: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg
     port map (
      D(23 downto 0) => D(23 downto 0),
      Q(1) => \mOutPtr_reg_n_2_[1]\,
      Q(0) => \mOutPtr_reg_n_2_[0]\,
      ap_clk => ap_clk,
      img2_data_stream_0_s_dout(7 downto 0) => img2_data_stream_0_s_dout(7 downto 0),
      \mOutPtr_reg[1]\(1 downto 0) => Q(1 downto 0),
      \mOutPtr_reg[1]_0\(1 downto 0) => \mOutPtr_reg[1]_0\(1 downto 0),
      shiftReg_ce => shiftReg_ce
    );
\internal_empty_n_i_1__13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0F0E0F000F00000"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[1]\,
      I1 => \mOutPtr_reg_n_2_[0]\,
      I2 => ap_rst_n,
      I3 => \ap_CS_fsm_reg[2]\,
      I4 => shiftReg_ce,
      I5 => \^img3_data_stream_2_s_empty_n\,
      O => \internal_empty_n_i_1__13_n_2\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__13_n_2\,
      Q => \^img3_data_stream_2_s_empty_n\,
      R => '0'
    );
\internal_full_n_i_1__14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFE0FFFFFFF0FF"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[0]\,
      I1 => \mOutPtr_reg_n_2_[1]\,
      I2 => \^img3_data_stream_2_s_full_n\,
      I3 => ap_rst_n,
      I4 => \ap_CS_fsm_reg[2]\,
      I5 => shiftReg_ce,
      O => \internal_full_n_i_1__14_n_2\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__14_n_2\,
      Q => \^img3_data_stream_2_s_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1__10\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[0]\,
      O => \mOutPtr[0]_i_1__10_n_2\
    );
\mOutPtr[1]_i_1__8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => shiftReg_ce,
      I1 => \ap_CS_fsm_reg[2]\,
      I2 => \mOutPtr_reg_n_2_[0]\,
      I3 => \mOutPtr_reg_n_2_[1]\,
      O => \mOutPtr[1]_i_1__8_n_2\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => \mOutPtr[0]_i_1__10_n_2\,
      Q => \mOutPtr_reg_n_2_[0]\,
      S => ap_rst_n_inv
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => \mOutPtr[1]_i_1__8_n_2\,
      Q => \mOutPtr_reg_n_2_[1]\,
      S => ap_rst_n_inv
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_2 is
  port (
    img1_data_stream_0_s_full_n : out STD_LOGIC;
    img1_data_stream_0_s_empty_n : out STD_LOGIC;
    \reg_588_reg[7]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \reg_588_reg[7]_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    grp_Filter2D_fu_96_p_src_data_stream_2_V_read : in STD_LOGIC;
    shiftReg_ce : in STD_LOGIC;
    ap_rst_n_inv : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    \r_V_1_reg_389_reg[29]\ : in STD_LOGIC;
    \p_Val2_15_reg_394_reg[7]\ : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_2 : entity is "fifo_w8_d1_A";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_2;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_2 is
  signal \^img1_data_stream_0_s_empty_n\ : STD_LOGIC;
  signal \^img1_data_stream_0_s_full_n\ : STD_LOGIC;
  signal \internal_empty_n_i_1__4_n_2\ : STD_LOGIC;
  signal \internal_full_n_i_1__4_n_2\ : STD_LOGIC;
  signal \internal_full_n_i_2__3_n_2\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1__2_n_2\ : STD_LOGIC;
  signal \^reg_588_reg[7]\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \internal_full_n_i_2__3\ : label is "soft_lutpair163";
  attribute SOFT_HLUTNM of \mOutPtr[0]_i_1__2\ : label is "soft_lutpair163";
begin
  img1_data_stream_0_s_empty_n <= \^img1_data_stream_0_s_empty_n\;
  img1_data_stream_0_s_full_n <= \^img1_data_stream_0_s_full_n\;
  \reg_588_reg[7]\(1 downto 0) <= \^reg_588_reg[7]\(1 downto 0);
U_fifo_w8_d1_A_ram: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_12
     port map (
      Q(1 downto 0) => \^reg_588_reg[7]\(1 downto 0),
      ap_clk => ap_clk,
      \p_Val2_15_reg_394_reg[7]\(7 downto 0) => \p_Val2_15_reg_394_reg[7]\(7 downto 0),
      \r_V_1_reg_389_reg[29]\ => \r_V_1_reg_389_reg[29]\,
      \reg_588_reg[7]\(7 downto 0) => \reg_588_reg[7]_0\(7 downto 0),
      shiftReg_ce => shiftReg_ce
    );
\internal_empty_n_i_1__4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCC8CCC0CCC0000"
    )
        port map (
      I0 => \internal_full_n_i_2__3_n_2\,
      I1 => ap_rst_n,
      I2 => Q(0),
      I3 => grp_Filter2D_fu_96_p_src_data_stream_2_V_read,
      I4 => shiftReg_ce,
      I5 => \^img1_data_stream_0_s_empty_n\,
      O => \internal_empty_n_i_1__4_n_2\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__4_n_2\,
      Q => \^img1_data_stream_0_s_empty_n\,
      R => '0'
    );
\internal_full_n_i_1__4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CF8F8F8FFFCFCFCF"
    )
        port map (
      I0 => \internal_full_n_i_2__3_n_2\,
      I1 => \^img1_data_stream_0_s_full_n\,
      I2 => ap_rst_n,
      I3 => grp_Filter2D_fu_96_p_src_data_stream_2_V_read,
      I4 => Q(0),
      I5 => shiftReg_ce,
      O => \internal_full_n_i_1__4_n_2\
    );
\internal_full_n_i_2__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^reg_588_reg[7]\(1),
      I1 => \^reg_588_reg[7]\(0),
      O => \internal_full_n_i_2__3_n_2\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__4_n_2\,
      Q => \^img1_data_stream_0_s_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^reg_588_reg[7]\(0),
      O => \mOutPtr[0]_i_1__2_n_2\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => \mOutPtr[0]_i_1__2_n_2\,
      Q => \^reg_588_reg[7]\(0),
      S => ap_rst_n_inv
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => D(0),
      Q => \^reg_588_reg[7]\(1),
      S => ap_rst_n_inv
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_5 is
  port (
    img2_data_stream_0_s_full_n : out STD_LOGIC;
    \ap_CS_fsm_reg[3]\ : out STD_LOGIC;
    img2_data_stream_0_s_dout : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    shiftReg_ce : in STD_LOGIC;
    shiftReg_ce_0 : in STD_LOGIC;
    img3_data_stream_2_s_full_n : in STD_LOGIC;
    img3_data_stream_1_s_full_n : in STD_LOGIC;
    img2_data_stream_2_s_empty_n : in STD_LOGIC;
    img3_data_stream_0_s_full_n : in STD_LOGIC;
    img2_data_stream_1_s_empty_n : in STD_LOGIC;
    ap_rst_n_inv : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \tmp_57_reg_2927_reg[2]\ : in STD_LOGIC;
    \p_Val2_1_reg_2922_reg[7]\ : in STD_LOGIC;
    \p_Val2_1_reg_2922_reg[6]\ : in STD_LOGIC;
    \p_Val2_1_reg_2922_reg[5]\ : in STD_LOGIC;
    \p_Val2_1_reg_2922_reg[4]\ : in STD_LOGIC;
    \p_Val2_1_reg_2922_reg[3]\ : in STD_LOGIC;
    \p_Val2_1_reg_2922_reg[2]\ : in STD_LOGIC;
    \p_Val2_1_reg_2922_reg[1]\ : in STD_LOGIC;
    \p_Val2_1_reg_2922_reg[0]\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_5 : entity is "fifo_w8_d1_A";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_5;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_5 is
  signal img2_data_stream_0_s_empty_n : STD_LOGIC;
  signal \^img2_data_stream_0_s_full_n\ : STD_LOGIC;
  signal \internal_empty_n_i_1__9_n_2\ : STD_LOGIC;
  signal \internal_full_n_i_1__7_n_2\ : STD_LOGIC;
  signal \mOutPtr[0]_i_1__5_n_2\ : STD_LOGIC;
  signal \mOutPtr[1]_i_2__2_n_2\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[0]\ : STD_LOGIC;
  signal \mOutPtr_reg_n_2_[1]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \mOutPtr[0]_i_1__5\ : label is "soft_lutpair166";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_2__2\ : label is "soft_lutpair166";
begin
  img2_data_stream_0_s_full_n <= \^img2_data_stream_0_s_full_n\;
U_fifo_w8_d1_A_ram: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_shiftReg_11
     port map (
      Q(1) => \mOutPtr_reg_n_2_[1]\,
      Q(0) => \mOutPtr_reg_n_2_[0]\,
      ap_clk => ap_clk,
      img2_data_stream_0_s_dout(7 downto 0) => img2_data_stream_0_s_dout(7 downto 0),
      \p_Val2_1_reg_2922_reg[0]\ => \p_Val2_1_reg_2922_reg[0]\,
      \p_Val2_1_reg_2922_reg[1]\ => \p_Val2_1_reg_2922_reg[1]\,
      \p_Val2_1_reg_2922_reg[2]\ => \p_Val2_1_reg_2922_reg[2]\,
      \p_Val2_1_reg_2922_reg[3]\ => \p_Val2_1_reg_2922_reg[3]\,
      \p_Val2_1_reg_2922_reg[4]\ => \p_Val2_1_reg_2922_reg[4]\,
      \p_Val2_1_reg_2922_reg[5]\ => \p_Val2_1_reg_2922_reg[5]\,
      \p_Val2_1_reg_2922_reg[6]\ => \p_Val2_1_reg_2922_reg[6]\,
      \p_Val2_1_reg_2922_reg[7]\ => \p_Val2_1_reg_2922_reg[7]\,
      shiftReg_ce_0 => shiftReg_ce_0,
      \tmp_57_reg_2927_reg[2]\ => \tmp_57_reg_2927_reg[2]\
    );
\ap_CS_fsm[3]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => img2_data_stream_0_s_empty_n,
      I1 => img3_data_stream_2_s_full_n,
      I2 => img3_data_stream_1_s_full_n,
      I3 => img2_data_stream_2_s_empty_n,
      I4 => img3_data_stream_0_s_full_n,
      I5 => img2_data_stream_1_s_empty_n,
      O => \ap_CS_fsm_reg[3]\
    );
\internal_empty_n_i_1__9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0F0E0F000F00000"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[1]\,
      I1 => \mOutPtr_reg_n_2_[0]\,
      I2 => ap_rst_n,
      I3 => shiftReg_ce,
      I4 => shiftReg_ce_0,
      I5 => img2_data_stream_0_s_empty_n,
      O => \internal_empty_n_i_1__9_n_2\
    );
internal_empty_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_empty_n_i_1__9_n_2\,
      Q => img2_data_stream_0_s_empty_n,
      R => '0'
    );
\internal_full_n_i_1__7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFE0FFFFFFF0FF"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[0]\,
      I1 => \mOutPtr_reg_n_2_[1]\,
      I2 => \^img2_data_stream_0_s_full_n\,
      I3 => ap_rst_n,
      I4 => shiftReg_ce,
      I5 => shiftReg_ce_0,
      O => \internal_full_n_i_1__7_n_2\
    );
internal_full_n_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \internal_full_n_i_1__7_n_2\,
      Q => \^img2_data_stream_0_s_full_n\,
      R => '0'
    );
\mOutPtr[0]_i_1__5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \mOutPtr_reg_n_2_[0]\,
      O => \mOutPtr[0]_i_1__5_n_2\
    );
\mOutPtr[1]_i_2__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => shiftReg_ce_0,
      I1 => shiftReg_ce,
      I2 => \mOutPtr_reg_n_2_[0]\,
      I3 => \mOutPtr_reg_n_2_[1]\,
      O => \mOutPtr[1]_i_2__2_n_2\
    );
\mOutPtr_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => \mOutPtr[0]_i_1__5_n_2\,
      Q => \mOutPtr_reg_n_2_[0]\,
      S => ap_rst_n_inv
    );
\mOutPtr_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => E(0),
      D => \mOutPtr[1]_i_2__2_n_2\,
      Q => \mOutPtr_reg_n_2_[1]\,
      S => ap_rst_n_inv
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_CvtColor is
  port (
    start_once_reg : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    internal_full_n_reg : out STD_LOGIC;
    internal_empty_n4_out : out STD_LOGIC;
    start_once_reg_reg_0 : out STD_LOGIC;
    shiftReg_ce : out STD_LOGIC;
    \SRL_SIG_reg[0][7]\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_clk : in STD_LOGIC;
    B : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ap_rst_n_inv : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    \exitcond_reg_420_reg[0]\ : in STD_LOGIC;
    CvtColor_U0_ap_start : in STD_LOGIC;
    start_for_Sobel_U0_full_n : in STD_LOGIC;
    img0_data_stream_2_s_empty_n : in STD_LOGIC;
    img0_data_stream_0_s_empty_n : in STD_LOGIC;
    img0_data_stream_1_s_empty_n : in STD_LOGIC;
    img1_data_stream_0_s_full_n : in STD_LOGIC;
    img1_data_stream_1_s_full_n : in STD_LOGIC;
    img1_data_stream_2_s_full_n : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \SRL_SIG_reg[0][7]_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_CvtColor;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_CvtColor is
  signal \^q\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \ap_CS_fsm[2]_i_3_n_2\ : STD_LOGIC;
  signal \ap_CS_fsm[3]_i_2_n_2\ : STD_LOGIC;
  signal ap_CS_fsm_pp0_stage0 : STD_LOGIC;
  signal \ap_CS_fsm_reg_n_2_[0]\ : STD_LOGIC;
  signal ap_CS_fsm_state2 : STD_LOGIC;
  signal ap_CS_fsm_state9 : STD_LOGIC;
  signal ap_NS_fsm : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal ap_block_pp0_stage0_subdone3_in : STD_LOGIC;
  signal ap_enable_reg_pp0_iter0 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter00 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter0_i_1_n_2 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter1_i_1_n_2 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter1_reg_n_2 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter2 : STD_LOGIC;
  signal \ap_enable_reg_pp0_iter2_i_1__0_n_2\ : STD_LOGIC;
  signal ap_enable_reg_pp0_iter3 : STD_LOGIC;
  signal \ap_enable_reg_pp0_iter3_i_1__0_n_2\ : STD_LOGIC;
  signal ap_enable_reg_pp0_iter4 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter4_i_1_n_2 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter5_i_1_n_2 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter5_reg_n_2 : STD_LOGIC;
  signal ap_reg_pp0_iter1_tmp_20_reg_355 : STD_LOGIC;
  signal \ap_reg_pp0_iter1_tmp_20_reg_355[0]_i_1_n_2\ : STD_LOGIC;
  signal ap_reg_pp0_iter2_tmp_20_reg_355 : STD_LOGIC;
  signal \ap_reg_pp0_iter2_tmp_20_reg_355[0]_i_1_n_2\ : STD_LOGIC;
  signal ap_reg_pp0_iter3_tmp_20_reg_355 : STD_LOGIC;
  signal \ap_reg_pp0_iter3_tmp_20_reg_355[0]_i_1_n_2\ : STD_LOGIC;
  signal ap_reg_pp0_iter4_tmp_20_reg_355 : STD_LOGIC;
  signal \ap_reg_pp0_iter4_tmp_20_reg_355[0]_i_1_n_2\ : STD_LOGIC;
  signal edge_detect_mac_mdEe_U13_n_12 : STD_LOGIC;
  signal i_1_fu_231_p2 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal i_1_reg_350 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \i_1_reg_350[9]_i_2_n_2\ : STD_LOGIC;
  signal i_reg_203 : STD_LOGIC;
  signal \i_reg_203_reg_n_2_[0]\ : STD_LOGIC;
  signal \i_reg_203_reg_n_2_[1]\ : STD_LOGIC;
  signal \i_reg_203_reg_n_2_[2]\ : STD_LOGIC;
  signal \i_reg_203_reg_n_2_[3]\ : STD_LOGIC;
  signal \i_reg_203_reg_n_2_[4]\ : STD_LOGIC;
  signal \i_reg_203_reg_n_2_[5]\ : STD_LOGIC;
  signal \i_reg_203_reg_n_2_[6]\ : STD_LOGIC;
  signal \i_reg_203_reg_n_2_[7]\ : STD_LOGIC;
  signal \i_reg_203_reg_n_2_[8]\ : STD_LOGIC;
  signal \i_reg_203_reg_n_2_[9]\ : STD_LOGIC;
  signal j_1_fu_243_p2 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal j_reg_214 : STD_LOGIC;
  signal j_reg_2140 : STD_LOGIC;
  signal \j_reg_214[10]_i_4_n_2\ : STD_LOGIC;
  signal \j_reg_214_reg__0\ : STD_LOGIC_VECTOR ( 10 downto 8 );
  signal \j_reg_214_reg_n_2_[0]\ : STD_LOGIC;
  signal \j_reg_214_reg_n_2_[1]\ : STD_LOGIC;
  signal \j_reg_214_reg_n_2_[2]\ : STD_LOGIC;
  signal \j_reg_214_reg_n_2_[3]\ : STD_LOGIC;
  signal \j_reg_214_reg_n_2_[4]\ : STD_LOGIC;
  signal \j_reg_214_reg_n_2_[5]\ : STD_LOGIC;
  signal \j_reg_214_reg_n_2_[6]\ : STD_LOGIC;
  signal \j_reg_214_reg_n_2_[7]\ : STD_LOGIC;
  signal p_Val2_12_reg_3840 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_100 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_101 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_102 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_103 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_104 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_105 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_106 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_107 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_79 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_80 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_81 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_82 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_83 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_84 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_85 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_86 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_87 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_88 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_89 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_90 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_91 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_92 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_93 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_94 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_95 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_96 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_97 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_98 : STD_LOGIC;
  signal p_Val2_12_reg_384_reg_n_99 : STD_LOGIC;
  signal p_Val2_14_fu_261_p4 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \p_Val2_15_fu_281_p2__0\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal p_Val2_15_reg_3940 : STD_LOGIC;
  signal \p_Val2_15_reg_394[7]_i_3_n_2\ : STD_LOGIC;
  signal r_V_i_fu_323_p2 : STD_LOGIC_VECTOR ( 28 downto 0 );
  signal r_V_i_reg_3790 : STD_LOGIC;
  signal \^shiftreg_ce\ : STD_LOGIC;
  signal \^start_once_reg\ : STD_LOGIC;
  signal start_once_reg_i_1_n_2 : STD_LOGIC;
  signal \^start_once_reg_reg_0\ : STD_LOGIC;
  signal tmp_101_reg_364 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal tmp_101_reg_3640 : STD_LOGIC;
  signal tmp_102_reg_369 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal tmp_20_fu_237_p2 : STD_LOGIC;
  signal tmp_20_reg_355 : STD_LOGIC;
  signal \tmp_20_reg_355[0]_i_1_n_2\ : STD_LOGIC;
  signal tmp_3_i_i_i_fu_277_p1 : STD_LOGIC;
  signal tmp_98_fu_287_p3 : STD_LOGIC;
  signal NLW_p_Val2_12_reg_384_reg_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_p_Val2_12_reg_384_reg_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_p_Val2_12_reg_384_reg_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_p_Val2_12_reg_384_reg_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_p_Val2_12_reg_384_reg_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_p_Val2_12_reg_384_reg_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_p_Val2_12_reg_384_reg_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_p_Val2_12_reg_384_reg_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_p_Val2_12_reg_384_reg_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_p_Val2_12_reg_384_reg_P_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 29 );
  signal NLW_p_Val2_12_reg_384_reg_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \SRL_SIG[0][7]_i_2\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \ap_CS_fsm[0]_i_2__0\ : label is "soft_lutpair83";
  attribute SOFT_HLUTNM of \ap_CS_fsm[2]_i_1__0\ : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of \ap_CS_fsm[2]_i_2\ : label is "soft_lutpair83";
  attribute SOFT_HLUTNM of \ap_CS_fsm[3]_i_1\ : label is "soft_lutpair79";
  attribute FSM_ENCODING : string;
  attribute FSM_ENCODING of \ap_CS_fsm_reg[0]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[1]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[2]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[3]\ : label is "none";
  attribute SOFT_HLUTNM of \ap_enable_reg_pp0_iter2_i_1__0\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \ap_enable_reg_pp0_iter3_i_1__0\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of ap_enable_reg_pp0_iter4_i_1 : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \ap_reg_pp0_iter1_tmp_20_reg_355[0]_i_1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \ap_reg_pp0_iter2_tmp_20_reg_355[0]_i_1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \ap_reg_pp0_iter3_tmp_20_reg_355[0]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \ap_reg_pp0_iter4_tmp_20_reg_355[0]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \i_1_reg_350[1]_i_1\ : label is "soft_lutpair81";
  attribute SOFT_HLUTNM of \i_1_reg_350[2]_i_1\ : label is "soft_lutpair81";
  attribute SOFT_HLUTNM of \i_1_reg_350[3]_i_1\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \i_1_reg_350[4]_i_1\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \i_1_reg_350[6]_i_1\ : label is "soft_lutpair82";
  attribute SOFT_HLUTNM of \i_1_reg_350[7]_i_1\ : label is "soft_lutpair82";
  attribute SOFT_HLUTNM of \i_1_reg_350[8]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \i_1_reg_350[9]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \j_reg_214[1]_i_1\ : label is "soft_lutpair80";
  attribute SOFT_HLUTNM of \j_reg_214[2]_i_1\ : label is "soft_lutpair80";
  attribute SOFT_HLUTNM of \j_reg_214[3]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \j_reg_214[4]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \j_reg_214[6]_i_1\ : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \j_reg_214[7]_i_1\ : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \j_reg_214[8]_i_1\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \j_reg_214[9]_i_1\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_3\ : label is "soft_lutpair69";
begin
  Q(7 downto 0) <= \^q\(7 downto 0);
  shiftReg_ce <= \^shiftreg_ce\;
  start_once_reg <= \^start_once_reg\;
  start_once_reg_reg_0 <= \^start_once_reg_reg_0\;
\SRL_SIG[0][7]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^shiftreg_ce\,
      I1 => tmp_98_fu_287_p3,
      I2 => \^q\(7),
      O => \SRL_SIG_reg[0][7]\
    );
\SRL_SIG[0][7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => ap_reg_pp0_iter4_tmp_20_reg_355,
      I1 => ap_block_pp0_stage0_subdone3_in,
      I2 => ap_enable_reg_pp0_iter5_reg_n_2,
      O => \^shiftreg_ce\
    );
\ap_CS_fsm[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBBFAAAA"
    )
        port map (
      I0 => \^start_once_reg_reg_0\,
      I1 => CvtColor_U0_ap_start,
      I2 => start_for_Sobel_U0_full_n,
      I3 => \^start_once_reg\,
      I4 => \ap_CS_fsm_reg_n_2_[0]\,
      O => ap_NS_fsm(0)
    );
\ap_CS_fsm[0]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ap_CS_fsm_state2,
      I1 => \ap_CS_fsm[2]_i_3_n_2\,
      O => \^start_once_reg_reg_0\
    );
\ap_CS_fsm[1]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEEAAAAA"
    )
        port map (
      I0 => ap_CS_fsm_state9,
      I1 => CvtColor_U0_ap_start,
      I2 => start_for_Sobel_U0_full_n,
      I3 => \^start_once_reg\,
      I4 => \ap_CS_fsm_reg_n_2_[0]\,
      O => ap_NS_fsm(1)
    );
\ap_CS_fsm[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter00,
      I1 => \ap_CS_fsm[3]_i_2_n_2\,
      I2 => ap_CS_fsm_pp0_stage0,
      O => ap_NS_fsm(2)
    );
\ap_CS_fsm[2]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ap_CS_fsm_state2,
      I1 => \ap_CS_fsm[2]_i_3_n_2\,
      O => ap_enable_reg_pp0_iter00
    );
\ap_CS_fsm[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555555557FFFFFF"
    )
        port map (
      I0 => \i_reg_203_reg_n_2_[9]\,
      I1 => \i_reg_203_reg_n_2_[5]\,
      I2 => \i_reg_203_reg_n_2_[4]\,
      I3 => \i_reg_203_reg_n_2_[7]\,
      I4 => \i_reg_203_reg_n_2_[6]\,
      I5 => \i_reg_203_reg_n_2_[8]\,
      O => \ap_CS_fsm[2]_i_3_n_2\
    );
\ap_CS_fsm[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ap_CS_fsm_pp0_stage0,
      I1 => \ap_CS_fsm[3]_i_2_n_2\,
      O => ap_NS_fsm(3)
    );
\ap_CS_fsm[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDDDFFFFD0DDFFFF"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter5_reg_n_2,
      I1 => ap_enable_reg_pp0_iter4,
      I2 => ap_enable_reg_pp0_iter1_reg_n_2,
      I3 => ap_enable_reg_pp0_iter0,
      I4 => ap_block_pp0_stage0_subdone3_in,
      I5 => tmp_20_fu_237_p2,
      O => \ap_CS_fsm[3]_i_2_n_2\
    );
\ap_CS_fsm_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(0),
      Q => \ap_CS_fsm_reg_n_2_[0]\,
      S => ap_rst_n_inv
    );
\ap_CS_fsm_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(1),
      Q => ap_CS_fsm_state2,
      R => ap_rst_n_inv
    );
\ap_CS_fsm_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(2),
      Q => ap_CS_fsm_pp0_stage0,
      R => ap_rst_n_inv
    );
\ap_CS_fsm_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(3),
      Q => ap_CS_fsm_state9,
      R => ap_rst_n_inv
    );
ap_enable_reg_pp0_iter0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F700F700F7000000"
    )
        port map (
      I0 => ap_CS_fsm_pp0_stage0,
      I1 => ap_block_pp0_stage0_subdone3_in,
      I2 => tmp_20_fu_237_p2,
      I3 => ap_rst_n,
      I4 => ap_enable_reg_pp0_iter00,
      I5 => ap_enable_reg_pp0_iter0,
      O => ap_enable_reg_pp0_iter0_i_1_n_2
    );
ap_enable_reg_pp0_iter0_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"57"
    )
        port map (
      I0 => \j_reg_214_reg__0\(10),
      I1 => \j_reg_214_reg__0\(9),
      I2 => \j_reg_214_reg__0\(8),
      O => tmp_20_fu_237_p2
    );
ap_enable_reg_pp0_iter0_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp0_iter0_i_1_n_2,
      Q => ap_enable_reg_pp0_iter0,
      R => '0'
    );
ap_enable_reg_pp0_iter1_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8800A0A0"
    )
        port map (
      I0 => ap_rst_n,
      I1 => ap_enable_reg_pp0_iter0,
      I2 => ap_enable_reg_pp0_iter1_reg_n_2,
      I3 => tmp_20_fu_237_p2,
      I4 => ap_block_pp0_stage0_subdone3_in,
      O => ap_enable_reg_pp0_iter1_i_1_n_2
    );
ap_enable_reg_pp0_iter1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp0_iter1_i_1_n_2,
      Q => ap_enable_reg_pp0_iter1_reg_n_2,
      R => '0'
    );
\ap_enable_reg_pp0_iter2_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter1_reg_n_2,
      I1 => ap_block_pp0_stage0_subdone3_in,
      I2 => ap_enable_reg_pp0_iter2,
      O => \ap_enable_reg_pp0_iter2_i_1__0_n_2\
    );
ap_enable_reg_pp0_iter2_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \ap_enable_reg_pp0_iter2_i_1__0_n_2\,
      Q => ap_enable_reg_pp0_iter2,
      R => ap_rst_n_inv
    );
\ap_enable_reg_pp0_iter3_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter2,
      I1 => ap_block_pp0_stage0_subdone3_in,
      I2 => ap_enable_reg_pp0_iter3,
      O => \ap_enable_reg_pp0_iter3_i_1__0_n_2\
    );
ap_enable_reg_pp0_iter3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \ap_enable_reg_pp0_iter3_i_1__0_n_2\,
      Q => ap_enable_reg_pp0_iter3,
      R => ap_rst_n_inv
    );
ap_enable_reg_pp0_iter4_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter3,
      I1 => ap_block_pp0_stage0_subdone3_in,
      I2 => ap_enable_reg_pp0_iter4,
      O => ap_enable_reg_pp0_iter4_i_1_n_2
    );
ap_enable_reg_pp0_iter4_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp0_iter4_i_1_n_2,
      Q => ap_enable_reg_pp0_iter4,
      R => ap_rst_n_inv
    );
ap_enable_reg_pp0_iter5_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C044C000"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter00,
      I1 => ap_rst_n,
      I2 => ap_enable_reg_pp0_iter4,
      I3 => ap_block_pp0_stage0_subdone3_in,
      I4 => ap_enable_reg_pp0_iter5_reg_n_2,
      O => ap_enable_reg_pp0_iter5_i_1_n_2
    );
ap_enable_reg_pp0_iter5_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp0_iter5_i_1_n_2,
      Q => ap_enable_reg_pp0_iter5_reg_n_2,
      R => '0'
    );
\ap_reg_pp0_iter1_tmp_20_reg_355[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => tmp_20_reg_355,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => ap_block_pp0_stage0_subdone3_in,
      I3 => ap_reg_pp0_iter1_tmp_20_reg_355,
      O => \ap_reg_pp0_iter1_tmp_20_reg_355[0]_i_1_n_2\
    );
\ap_reg_pp0_iter1_tmp_20_reg_355_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \ap_reg_pp0_iter1_tmp_20_reg_355[0]_i_1_n_2\,
      Q => ap_reg_pp0_iter1_tmp_20_reg_355,
      R => '0'
    );
\ap_reg_pp0_iter2_tmp_20_reg_355[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ap_reg_pp0_iter1_tmp_20_reg_355,
      I1 => ap_block_pp0_stage0_subdone3_in,
      I2 => ap_reg_pp0_iter2_tmp_20_reg_355,
      O => \ap_reg_pp0_iter2_tmp_20_reg_355[0]_i_1_n_2\
    );
\ap_reg_pp0_iter2_tmp_20_reg_355_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \ap_reg_pp0_iter2_tmp_20_reg_355[0]_i_1_n_2\,
      Q => ap_reg_pp0_iter2_tmp_20_reg_355,
      R => '0'
    );
\ap_reg_pp0_iter3_tmp_20_reg_355[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ap_reg_pp0_iter2_tmp_20_reg_355,
      I1 => ap_block_pp0_stage0_subdone3_in,
      I2 => ap_reg_pp0_iter3_tmp_20_reg_355,
      O => \ap_reg_pp0_iter3_tmp_20_reg_355[0]_i_1_n_2\
    );
\ap_reg_pp0_iter3_tmp_20_reg_355_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \ap_reg_pp0_iter3_tmp_20_reg_355[0]_i_1_n_2\,
      Q => ap_reg_pp0_iter3_tmp_20_reg_355,
      R => '0'
    );
\ap_reg_pp0_iter4_tmp_20_reg_355[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ap_reg_pp0_iter3_tmp_20_reg_355,
      I1 => ap_block_pp0_stage0_subdone3_in,
      I2 => ap_reg_pp0_iter4_tmp_20_reg_355,
      O => \ap_reg_pp0_iter4_tmp_20_reg_355[0]_i_1_n_2\
    );
\ap_reg_pp0_iter4_tmp_20_reg_355_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \ap_reg_pp0_iter4_tmp_20_reg_355[0]_i_1_n_2\,
      Q => ap_reg_pp0_iter4_tmp_20_reg_355,
      R => '0'
    );
edge_detect_mac_mdEe_U13: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_edge_detect_mac_mdEe
     port map (
      P(8 downto 1) => p_Val2_14_fu_261_p4(7 downto 0),
      P(0) => tmp_3_i_i_i_fu_277_p1,
      Q(7 downto 0) => tmp_102_reg_369(7 downto 0),
      ap_block_pp0_stage0_subdone3_in => ap_block_pp0_stage0_subdone3_in,
      ap_clk => ap_clk,
      ap_enable_reg_pp0_iter1_reg => ap_enable_reg_pp0_iter1_reg_n_2,
      ap_enable_reg_pp0_iter4 => ap_enable_reg_pp0_iter4,
      ap_enable_reg_pp0_iter5_reg => ap_enable_reg_pp0_iter5_reg_n_2,
      ap_reg_pp0_iter3_tmp_20_reg_355 => ap_reg_pp0_iter3_tmp_20_reg_355,
      ap_reg_pp0_iter4_tmp_20_reg_355 => ap_reg_pp0_iter4_tmp_20_reg_355,
      img0_data_stream_0_s_empty_n => img0_data_stream_0_s_empty_n,
      img0_data_stream_1_s_empty_n => img0_data_stream_1_s_empty_n,
      img0_data_stream_2_s_empty_n => img0_data_stream_2_s_empty_n,
      img1_data_stream_0_s_full_n => img1_data_stream_0_s_full_n,
      img1_data_stream_1_s_full_n => img1_data_stream_1_s_full_n,
      img1_data_stream_2_s_full_n => img1_data_stream_2_s_full_n,
      p_Val2_12_reg_384_reg(28) => p_Val2_12_reg_384_reg_n_79,
      p_Val2_12_reg_384_reg(27) => p_Val2_12_reg_384_reg_n_80,
      p_Val2_12_reg_384_reg(26) => p_Val2_12_reg_384_reg_n_81,
      p_Val2_12_reg_384_reg(25) => p_Val2_12_reg_384_reg_n_82,
      p_Val2_12_reg_384_reg(24) => p_Val2_12_reg_384_reg_n_83,
      p_Val2_12_reg_384_reg(23) => p_Val2_12_reg_384_reg_n_84,
      p_Val2_12_reg_384_reg(22) => p_Val2_12_reg_384_reg_n_85,
      p_Val2_12_reg_384_reg(21) => p_Val2_12_reg_384_reg_n_86,
      p_Val2_12_reg_384_reg(20) => p_Val2_12_reg_384_reg_n_87,
      p_Val2_12_reg_384_reg(19) => p_Val2_12_reg_384_reg_n_88,
      p_Val2_12_reg_384_reg(18) => p_Val2_12_reg_384_reg_n_89,
      p_Val2_12_reg_384_reg(17) => p_Val2_12_reg_384_reg_n_90,
      p_Val2_12_reg_384_reg(16) => p_Val2_12_reg_384_reg_n_91,
      p_Val2_12_reg_384_reg(15) => p_Val2_12_reg_384_reg_n_92,
      p_Val2_12_reg_384_reg(14) => p_Val2_12_reg_384_reg_n_93,
      p_Val2_12_reg_384_reg(13) => p_Val2_12_reg_384_reg_n_94,
      p_Val2_12_reg_384_reg(12) => p_Val2_12_reg_384_reg_n_95,
      p_Val2_12_reg_384_reg(11) => p_Val2_12_reg_384_reg_n_96,
      p_Val2_12_reg_384_reg(10) => p_Val2_12_reg_384_reg_n_97,
      p_Val2_12_reg_384_reg(9) => p_Val2_12_reg_384_reg_n_98,
      p_Val2_12_reg_384_reg(8) => p_Val2_12_reg_384_reg_n_99,
      p_Val2_12_reg_384_reg(7) => p_Val2_12_reg_384_reg_n_100,
      p_Val2_12_reg_384_reg(6) => p_Val2_12_reg_384_reg_n_101,
      p_Val2_12_reg_384_reg(5) => p_Val2_12_reg_384_reg_n_102,
      p_Val2_12_reg_384_reg(4) => p_Val2_12_reg_384_reg_n_103,
      p_Val2_12_reg_384_reg(3) => p_Val2_12_reg_384_reg_n_104,
      p_Val2_12_reg_384_reg(2) => p_Val2_12_reg_384_reg_n_105,
      p_Val2_12_reg_384_reg(1) => p_Val2_12_reg_384_reg_n_106,
      p_Val2_12_reg_384_reg(0) => p_Val2_12_reg_384_reg_n_107,
      \r_V_1_reg_389_reg[29]\ => edge_detect_mac_mdEe_U13_n_12,
      tmp_20_reg_355 => tmp_20_reg_355,
      tmp_98_fu_287_p3 => tmp_98_fu_287_p3
    );
edge_detect_mul_mbkb_U11: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_edge_detect_mul_mbkb
     port map (
      Q(7 downto 0) => tmp_101_reg_364(7 downto 0),
      \out\(28 downto 0) => r_V_i_fu_323_p2(28 downto 0)
    );
\i_1_reg_350[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \i_reg_203_reg_n_2_[0]\,
      O => i_1_fu_231_p2(0)
    );
\i_1_reg_350[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \i_reg_203_reg_n_2_[0]\,
      I1 => \i_reg_203_reg_n_2_[1]\,
      O => i_1_fu_231_p2(1)
    );
\i_1_reg_350[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \i_reg_203_reg_n_2_[2]\,
      I1 => \i_reg_203_reg_n_2_[1]\,
      I2 => \i_reg_203_reg_n_2_[0]\,
      O => i_1_fu_231_p2(2)
    );
\i_1_reg_350[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \i_reg_203_reg_n_2_[3]\,
      I1 => \i_reg_203_reg_n_2_[0]\,
      I2 => \i_reg_203_reg_n_2_[1]\,
      I3 => \i_reg_203_reg_n_2_[2]\,
      O => i_1_fu_231_p2(3)
    );
\i_1_reg_350[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \i_reg_203_reg_n_2_[4]\,
      I1 => \i_reg_203_reg_n_2_[2]\,
      I2 => \i_reg_203_reg_n_2_[1]\,
      I3 => \i_reg_203_reg_n_2_[0]\,
      I4 => \i_reg_203_reg_n_2_[3]\,
      O => i_1_fu_231_p2(4)
    );
\i_1_reg_350[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \i_reg_203_reg_n_2_[5]\,
      I1 => \i_reg_203_reg_n_2_[3]\,
      I2 => \i_reg_203_reg_n_2_[0]\,
      I3 => \i_reg_203_reg_n_2_[1]\,
      I4 => \i_reg_203_reg_n_2_[2]\,
      I5 => \i_reg_203_reg_n_2_[4]\,
      O => i_1_fu_231_p2(5)
    );
\i_1_reg_350[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \i_reg_203_reg_n_2_[6]\,
      I1 => \i_1_reg_350[9]_i_2_n_2\,
      O => i_1_fu_231_p2(6)
    );
\i_1_reg_350[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \i_reg_203_reg_n_2_[7]\,
      I1 => \i_1_reg_350[9]_i_2_n_2\,
      I2 => \i_reg_203_reg_n_2_[6]\,
      O => i_1_fu_231_p2(7)
    );
\i_1_reg_350[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \i_reg_203_reg_n_2_[8]\,
      I1 => \i_reg_203_reg_n_2_[6]\,
      I2 => \i_reg_203_reg_n_2_[7]\,
      I3 => \i_1_reg_350[9]_i_2_n_2\,
      O => i_1_fu_231_p2(8)
    );
\i_1_reg_350[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \i_reg_203_reg_n_2_[9]\,
      I1 => \i_1_reg_350[9]_i_2_n_2\,
      I2 => \i_reg_203_reg_n_2_[7]\,
      I3 => \i_reg_203_reg_n_2_[6]\,
      I4 => \i_reg_203_reg_n_2_[8]\,
      O => i_1_fu_231_p2(9)
    );
\i_1_reg_350[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \i_reg_203_reg_n_2_[5]\,
      I1 => \i_reg_203_reg_n_2_[3]\,
      I2 => \i_reg_203_reg_n_2_[0]\,
      I3 => \i_reg_203_reg_n_2_[1]\,
      I4 => \i_reg_203_reg_n_2_[2]\,
      I5 => \i_reg_203_reg_n_2_[4]\,
      O => \i_1_reg_350[9]_i_2_n_2\
    );
\i_1_reg_350_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_1_fu_231_p2(0),
      Q => i_1_reg_350(0),
      R => '0'
    );
\i_1_reg_350_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_1_fu_231_p2(1),
      Q => i_1_reg_350(1),
      R => '0'
    );
\i_1_reg_350_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_1_fu_231_p2(2),
      Q => i_1_reg_350(2),
      R => '0'
    );
\i_1_reg_350_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_1_fu_231_p2(3),
      Q => i_1_reg_350(3),
      R => '0'
    );
\i_1_reg_350_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_1_fu_231_p2(4),
      Q => i_1_reg_350(4),
      R => '0'
    );
\i_1_reg_350_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_1_fu_231_p2(5),
      Q => i_1_reg_350(5),
      R => '0'
    );
\i_1_reg_350_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_1_fu_231_p2(6),
      Q => i_1_reg_350(6),
      R => '0'
    );
\i_1_reg_350_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_1_fu_231_p2(7),
      Q => i_1_reg_350(7),
      R => '0'
    );
\i_1_reg_350_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_1_fu_231_p2(8),
      Q => i_1_reg_350(8),
      R => '0'
    );
\i_1_reg_350_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => i_1_fu_231_p2(9),
      Q => i_1_reg_350(9),
      R => '0'
    );
\i_reg_203[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000A800"
    )
        port map (
      I0 => CvtColor_U0_ap_start,
      I1 => start_for_Sobel_U0_full_n,
      I2 => \^start_once_reg\,
      I3 => \ap_CS_fsm_reg_n_2_[0]\,
      I4 => ap_CS_fsm_state9,
      O => i_reg_203
    );
\i_reg_203_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state9,
      D => i_1_reg_350(0),
      Q => \i_reg_203_reg_n_2_[0]\,
      R => i_reg_203
    );
\i_reg_203_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state9,
      D => i_1_reg_350(1),
      Q => \i_reg_203_reg_n_2_[1]\,
      R => i_reg_203
    );
\i_reg_203_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state9,
      D => i_1_reg_350(2),
      Q => \i_reg_203_reg_n_2_[2]\,
      R => i_reg_203
    );
\i_reg_203_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state9,
      D => i_1_reg_350(3),
      Q => \i_reg_203_reg_n_2_[3]\,
      R => i_reg_203
    );
\i_reg_203_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state9,
      D => i_1_reg_350(4),
      Q => \i_reg_203_reg_n_2_[4]\,
      R => i_reg_203
    );
\i_reg_203_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state9,
      D => i_1_reg_350(5),
      Q => \i_reg_203_reg_n_2_[5]\,
      R => i_reg_203
    );
\i_reg_203_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state9,
      D => i_1_reg_350(6),
      Q => \i_reg_203_reg_n_2_[6]\,
      R => i_reg_203
    );
\i_reg_203_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state9,
      D => i_1_reg_350(7),
      Q => \i_reg_203_reg_n_2_[7]\,
      R => i_reg_203
    );
\i_reg_203_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state9,
      D => i_1_reg_350(8),
      Q => \i_reg_203_reg_n_2_[8]\,
      R => i_reg_203
    );
\i_reg_203_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state9,
      D => i_1_reg_350(9),
      Q => \i_reg_203_reg_n_2_[9]\,
      R => i_reg_203
    );
internal_full_n_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2AAAAAAA"
    )
        port map (
      I0 => \exitcond_reg_420_reg[0]\,
      I1 => tmp_20_reg_355,
      I2 => ap_enable_reg_pp0_iter1_reg_n_2,
      I3 => ap_CS_fsm_pp0_stage0,
      I4 => ap_block_pp0_stage0_subdone3_in,
      O => internal_empty_n4_out
    );
\j_reg_214[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \j_reg_214_reg_n_2_[0]\,
      O => j_1_fu_243_p2(0)
    );
\j_reg_214[10]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter00,
      I1 => j_reg_2140,
      O => j_reg_214
    );
\j_reg_214[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0808088800000000"
    )
        port map (
      I0 => ap_block_pp0_stage0_subdone3_in,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => \j_reg_214_reg__0\(10),
      I3 => \j_reg_214_reg__0\(9),
      I4 => \j_reg_214_reg__0\(8),
      I5 => ap_enable_reg_pp0_iter0,
      O => j_reg_2140
    );
\j_reg_214[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \j_reg_214_reg__0\(10),
      I1 => \j_reg_214_reg__0\(8),
      I2 => \j_reg_214_reg_n_2_[6]\,
      I3 => \j_reg_214[10]_i_4_n_2\,
      I4 => \j_reg_214_reg_n_2_[7]\,
      I5 => \j_reg_214_reg__0\(9),
      O => j_1_fu_243_p2(10)
    );
\j_reg_214[10]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \j_reg_214_reg_n_2_[5]\,
      I1 => \j_reg_214_reg_n_2_[3]\,
      I2 => \j_reg_214_reg_n_2_[0]\,
      I3 => \j_reg_214_reg_n_2_[1]\,
      I4 => \j_reg_214_reg_n_2_[2]\,
      I5 => \j_reg_214_reg_n_2_[4]\,
      O => \j_reg_214[10]_i_4_n_2\
    );
\j_reg_214[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \j_reg_214_reg_n_2_[0]\,
      I1 => \j_reg_214_reg_n_2_[1]\,
      O => j_1_fu_243_p2(1)
    );
\j_reg_214[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \j_reg_214_reg_n_2_[2]\,
      I1 => \j_reg_214_reg_n_2_[1]\,
      I2 => \j_reg_214_reg_n_2_[0]\,
      O => j_1_fu_243_p2(2)
    );
\j_reg_214[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \j_reg_214_reg_n_2_[3]\,
      I1 => \j_reg_214_reg_n_2_[0]\,
      I2 => \j_reg_214_reg_n_2_[1]\,
      I3 => \j_reg_214_reg_n_2_[2]\,
      O => j_1_fu_243_p2(3)
    );
\j_reg_214[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \j_reg_214_reg_n_2_[4]\,
      I1 => \j_reg_214_reg_n_2_[2]\,
      I2 => \j_reg_214_reg_n_2_[1]\,
      I3 => \j_reg_214_reg_n_2_[0]\,
      I4 => \j_reg_214_reg_n_2_[3]\,
      O => j_1_fu_243_p2(4)
    );
\j_reg_214[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \j_reg_214_reg_n_2_[5]\,
      I1 => \j_reg_214_reg_n_2_[3]\,
      I2 => \j_reg_214_reg_n_2_[0]\,
      I3 => \j_reg_214_reg_n_2_[1]\,
      I4 => \j_reg_214_reg_n_2_[2]\,
      I5 => \j_reg_214_reg_n_2_[4]\,
      O => j_1_fu_243_p2(5)
    );
\j_reg_214[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \j_reg_214_reg_n_2_[6]\,
      I1 => \j_reg_214[10]_i_4_n_2\,
      O => j_1_fu_243_p2(6)
    );
\j_reg_214[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \j_reg_214_reg_n_2_[7]\,
      I1 => \j_reg_214[10]_i_4_n_2\,
      I2 => \j_reg_214_reg_n_2_[6]\,
      O => j_1_fu_243_p2(7)
    );
\j_reg_214[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \j_reg_214_reg__0\(8),
      I1 => \j_reg_214_reg_n_2_[6]\,
      I2 => \j_reg_214[10]_i_4_n_2\,
      I3 => \j_reg_214_reg_n_2_[7]\,
      O => j_1_fu_243_p2(8)
    );
\j_reg_214[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \j_reg_214_reg__0\(9),
      I1 => \j_reg_214_reg_n_2_[7]\,
      I2 => \j_reg_214[10]_i_4_n_2\,
      I3 => \j_reg_214_reg_n_2_[6]\,
      I4 => \j_reg_214_reg__0\(8),
      O => j_1_fu_243_p2(9)
    );
\j_reg_214_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_2140,
      D => j_1_fu_243_p2(0),
      Q => \j_reg_214_reg_n_2_[0]\,
      R => j_reg_214
    );
\j_reg_214_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_2140,
      D => j_1_fu_243_p2(10),
      Q => \j_reg_214_reg__0\(10),
      R => j_reg_214
    );
\j_reg_214_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_2140,
      D => j_1_fu_243_p2(1),
      Q => \j_reg_214_reg_n_2_[1]\,
      R => j_reg_214
    );
\j_reg_214_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_2140,
      D => j_1_fu_243_p2(2),
      Q => \j_reg_214_reg_n_2_[2]\,
      R => j_reg_214
    );
\j_reg_214_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_2140,
      D => j_1_fu_243_p2(3),
      Q => \j_reg_214_reg_n_2_[3]\,
      R => j_reg_214
    );
\j_reg_214_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_2140,
      D => j_1_fu_243_p2(4),
      Q => \j_reg_214_reg_n_2_[4]\,
      R => j_reg_214
    );
\j_reg_214_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_2140,
      D => j_1_fu_243_p2(5),
      Q => \j_reg_214_reg_n_2_[5]\,
      R => j_reg_214
    );
\j_reg_214_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_2140,
      D => j_1_fu_243_p2(6),
      Q => \j_reg_214_reg_n_2_[6]\,
      R => j_reg_214
    );
\j_reg_214_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_2140,
      D => j_1_fu_243_p2(7),
      Q => \j_reg_214_reg_n_2_[7]\,
      R => j_reg_214
    );
\j_reg_214_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_2140,
      D => j_1_fu_243_p2(8),
      Q => \j_reg_214_reg__0\(8),
      R => j_reg_214
    );
\j_reg_214_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => j_reg_2140,
      D => j_1_fu_243_p2(9),
      Q => \j_reg_214_reg__0\(9),
      R => j_reg_214
    );
\mOutPtr[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \exitcond_reg_420_reg[0]\,
      I1 => tmp_20_reg_355,
      I2 => ap_enable_reg_pp0_iter1_reg_n_2,
      I3 => ap_CS_fsm_pp0_stage0,
      I4 => ap_block_pp0_stage0_subdone3_in,
      O => E(0)
    );
\mOutPtr[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00008000"
    )
        port map (
      I0 => tmp_20_reg_355,
      I1 => ap_enable_reg_pp0_iter1_reg_n_2,
      I2 => ap_CS_fsm_pp0_stage0,
      I3 => ap_block_pp0_stage0_subdone3_in,
      I4 => \exitcond_reg_420_reg[0]\,
      O => internal_full_n_reg
    );
p_Val2_12_reg_384_reg: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 0,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 0,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 2,
      BREG => 2,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 1,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29 downto 0) => B"000000000001110100101111000110",
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_p_Val2_12_reg_384_reg_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17 downto 8) => B"0000000000",
      B(7 downto 0) => B(7 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_p_Val2_12_reg_384_reg_BCOUT_UNCONNECTED(17 downto 0),
      C(47) => r_V_i_fu_323_p2(28),
      C(46) => r_V_i_fu_323_p2(28),
      C(45) => r_V_i_fu_323_p2(28),
      C(44) => r_V_i_fu_323_p2(28),
      C(43) => r_V_i_fu_323_p2(28),
      C(42) => r_V_i_fu_323_p2(28),
      C(41) => r_V_i_fu_323_p2(28),
      C(40) => r_V_i_fu_323_p2(28),
      C(39) => r_V_i_fu_323_p2(28),
      C(38) => r_V_i_fu_323_p2(28),
      C(37) => r_V_i_fu_323_p2(28),
      C(36) => r_V_i_fu_323_p2(28),
      C(35) => r_V_i_fu_323_p2(28),
      C(34) => r_V_i_fu_323_p2(28),
      C(33) => r_V_i_fu_323_p2(28),
      C(32) => r_V_i_fu_323_p2(28),
      C(31) => r_V_i_fu_323_p2(28),
      C(30) => r_V_i_fu_323_p2(28),
      C(29) => r_V_i_fu_323_p2(28),
      C(28 downto 0) => r_V_i_fu_323_p2(28 downto 0),
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_p_Val2_12_reg_384_reg_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_p_Val2_12_reg_384_reg_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '0',
      CEA2 => '0',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => tmp_101_reg_3640,
      CEB2 => ap_block_pp0_stage0_subdone3_in,
      CEC => r_V_i_reg_3790,
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => p_Val2_12_reg_3840,
      CLK => ap_clk,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_p_Val2_12_reg_384_reg_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"0110101",
      OVERFLOW => NLW_p_Val2_12_reg_384_reg_OVERFLOW_UNCONNECTED,
      P(47 downto 29) => NLW_p_Val2_12_reg_384_reg_P_UNCONNECTED(47 downto 29),
      P(28) => p_Val2_12_reg_384_reg_n_79,
      P(27) => p_Val2_12_reg_384_reg_n_80,
      P(26) => p_Val2_12_reg_384_reg_n_81,
      P(25) => p_Val2_12_reg_384_reg_n_82,
      P(24) => p_Val2_12_reg_384_reg_n_83,
      P(23) => p_Val2_12_reg_384_reg_n_84,
      P(22) => p_Val2_12_reg_384_reg_n_85,
      P(21) => p_Val2_12_reg_384_reg_n_86,
      P(20) => p_Val2_12_reg_384_reg_n_87,
      P(19) => p_Val2_12_reg_384_reg_n_88,
      P(18) => p_Val2_12_reg_384_reg_n_89,
      P(17) => p_Val2_12_reg_384_reg_n_90,
      P(16) => p_Val2_12_reg_384_reg_n_91,
      P(15) => p_Val2_12_reg_384_reg_n_92,
      P(14) => p_Val2_12_reg_384_reg_n_93,
      P(13) => p_Val2_12_reg_384_reg_n_94,
      P(12) => p_Val2_12_reg_384_reg_n_95,
      P(11) => p_Val2_12_reg_384_reg_n_96,
      P(10) => p_Val2_12_reg_384_reg_n_97,
      P(9) => p_Val2_12_reg_384_reg_n_98,
      P(8) => p_Val2_12_reg_384_reg_n_99,
      P(7) => p_Val2_12_reg_384_reg_n_100,
      P(6) => p_Val2_12_reg_384_reg_n_101,
      P(5) => p_Val2_12_reg_384_reg_n_102,
      P(4) => p_Val2_12_reg_384_reg_n_103,
      P(3) => p_Val2_12_reg_384_reg_n_104,
      P(2) => p_Val2_12_reg_384_reg_n_105,
      P(1) => p_Val2_12_reg_384_reg_n_106,
      P(0) => p_Val2_12_reg_384_reg_n_107,
      PATTERNBDETECT => NLW_p_Val2_12_reg_384_reg_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_p_Val2_12_reg_384_reg_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47 downto 0) => NLW_p_Val2_12_reg_384_reg_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_p_Val2_12_reg_384_reg_UNDERFLOW_UNCONNECTED
    );
p_Val2_12_reg_384_reg_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => tmp_20_reg_355,
      I1 => ap_block_pp0_stage0_subdone3_in,
      I2 => ap_CS_fsm_pp0_stage0,
      O => tmp_101_reg_3640
    );
p_Val2_12_reg_384_reg_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ap_reg_pp0_iter1_tmp_20_reg_355,
      I1 => ap_block_pp0_stage0_subdone3_in,
      O => r_V_i_reg_3790
    );
p_Val2_12_reg_384_reg_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => ap_block_pp0_stage0_subdone3_in,
      I1 => ap_enable_reg_pp0_iter3,
      I2 => ap_reg_pp0_iter2_tmp_20_reg_355,
      O => p_Val2_12_reg_3840
    );
\p_Val2_15_reg_394[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_Val2_14_fu_261_p4(0),
      I1 => tmp_3_i_i_i_fu_277_p1,
      O => \p_Val2_15_fu_281_p2__0\(0)
    );
\p_Val2_15_reg_394[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => p_Val2_14_fu_261_p4(0),
      I1 => tmp_3_i_i_i_fu_277_p1,
      I2 => p_Val2_14_fu_261_p4(1),
      O => \p_Val2_15_fu_281_p2__0\(1)
    );
\p_Val2_15_reg_394[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => tmp_3_i_i_i_fu_277_p1,
      I1 => p_Val2_14_fu_261_p4(0),
      I2 => p_Val2_14_fu_261_p4(1),
      I3 => p_Val2_14_fu_261_p4(2),
      O => \p_Val2_15_fu_281_p2__0\(2)
    );
\p_Val2_15_reg_394[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => p_Val2_14_fu_261_p4(1),
      I1 => p_Val2_14_fu_261_p4(0),
      I2 => tmp_3_i_i_i_fu_277_p1,
      I3 => p_Val2_14_fu_261_p4(2),
      I4 => p_Val2_14_fu_261_p4(3),
      O => \p_Val2_15_fu_281_p2__0\(3)
    );
\p_Val2_15_reg_394[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => p_Val2_14_fu_261_p4(2),
      I1 => tmp_3_i_i_i_fu_277_p1,
      I2 => p_Val2_14_fu_261_p4(0),
      I3 => p_Val2_14_fu_261_p4(1),
      I4 => p_Val2_14_fu_261_p4(3),
      I5 => p_Val2_14_fu_261_p4(4),
      O => \p_Val2_15_fu_281_p2__0\(4)
    );
\p_Val2_15_reg_394[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \p_Val2_15_reg_394[7]_i_3_n_2\,
      I1 => p_Val2_14_fu_261_p4(5),
      O => \p_Val2_15_fu_281_p2__0\(5)
    );
\p_Val2_15_reg_394[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \p_Val2_15_reg_394[7]_i_3_n_2\,
      I1 => p_Val2_14_fu_261_p4(5),
      I2 => p_Val2_14_fu_261_p4(6),
      O => \p_Val2_15_fu_281_p2__0\(6)
    );
\p_Val2_15_reg_394[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ap_reg_pp0_iter3_tmp_20_reg_355,
      I1 => ap_block_pp0_stage0_subdone3_in,
      O => p_Val2_15_reg_3940
    );
\p_Val2_15_reg_394[7]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => p_Val2_14_fu_261_p4(5),
      I1 => \p_Val2_15_reg_394[7]_i_3_n_2\,
      I2 => p_Val2_14_fu_261_p4(6),
      I3 => p_Val2_14_fu_261_p4(7),
      O => \p_Val2_15_fu_281_p2__0\(7)
    );
\p_Val2_15_reg_394[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => p_Val2_14_fu_261_p4(4),
      I1 => p_Val2_14_fu_261_p4(2),
      I2 => tmp_3_i_i_i_fu_277_p1,
      I3 => p_Val2_14_fu_261_p4(0),
      I4 => p_Val2_14_fu_261_p4(1),
      I5 => p_Val2_14_fu_261_p4(3),
      O => \p_Val2_15_reg_394[7]_i_3_n_2\
    );
\p_Val2_15_reg_394_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_15_reg_3940,
      D => \p_Val2_15_fu_281_p2__0\(0),
      Q => \^q\(0),
      R => '0'
    );
\p_Val2_15_reg_394_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_15_reg_3940,
      D => \p_Val2_15_fu_281_p2__0\(1),
      Q => \^q\(1),
      R => '0'
    );
\p_Val2_15_reg_394_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_15_reg_3940,
      D => \p_Val2_15_fu_281_p2__0\(2),
      Q => \^q\(2),
      R => '0'
    );
\p_Val2_15_reg_394_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_15_reg_3940,
      D => \p_Val2_15_fu_281_p2__0\(3),
      Q => \^q\(3),
      R => '0'
    );
\p_Val2_15_reg_394_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_15_reg_3940,
      D => \p_Val2_15_fu_281_p2__0\(4),
      Q => \^q\(4),
      R => '0'
    );
\p_Val2_15_reg_394_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_15_reg_3940,
      D => \p_Val2_15_fu_281_p2__0\(5),
      Q => \^q\(5),
      R => '0'
    );
\p_Val2_15_reg_394_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_15_reg_3940,
      D => \p_Val2_15_fu_281_p2__0\(6),
      Q => \^q\(6),
      R => '0'
    );
\p_Val2_15_reg_394_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_15_reg_3940,
      D => \p_Val2_15_fu_281_p2__0\(7),
      Q => \^q\(7),
      R => '0'
    );
\r_V_1_reg_389_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => edge_detect_mac_mdEe_U13_n_12,
      Q => tmp_98_fu_287_p3,
      R => '0'
    );
start_once_reg_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5444"
    )
        port map (
      I0 => \^start_once_reg_reg_0\,
      I1 => \^start_once_reg\,
      I2 => start_for_Sobel_U0_full_n,
      I3 => CvtColor_U0_ap_start,
      O => start_once_reg_i_1_n_2
    );
start_once_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => start_once_reg_i_1_n_2,
      Q => \^start_once_reg\,
      R => ap_rst_n_inv
    );
\tmp_101_reg_364_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => tmp_101_reg_3640,
      D => \SRL_SIG_reg[0][7]_0\(0),
      Q => tmp_101_reg_364(0),
      R => '0'
    );
\tmp_101_reg_364_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => tmp_101_reg_3640,
      D => \SRL_SIG_reg[0][7]_0\(1),
      Q => tmp_101_reg_364(1),
      R => '0'
    );
\tmp_101_reg_364_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => tmp_101_reg_3640,
      D => \SRL_SIG_reg[0][7]_0\(2),
      Q => tmp_101_reg_364(2),
      R => '0'
    );
\tmp_101_reg_364_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => tmp_101_reg_3640,
      D => \SRL_SIG_reg[0][7]_0\(3),
      Q => tmp_101_reg_364(3),
      R => '0'
    );
\tmp_101_reg_364_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => tmp_101_reg_3640,
      D => \SRL_SIG_reg[0][7]_0\(4),
      Q => tmp_101_reg_364(4),
      R => '0'
    );
\tmp_101_reg_364_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => tmp_101_reg_3640,
      D => \SRL_SIG_reg[0][7]_0\(5),
      Q => tmp_101_reg_364(5),
      R => '0'
    );
\tmp_101_reg_364_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => tmp_101_reg_3640,
      D => \SRL_SIG_reg[0][7]_0\(6),
      Q => tmp_101_reg_364(6),
      R => '0'
    );
\tmp_101_reg_364_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => tmp_101_reg_3640,
      D => \SRL_SIG_reg[0][7]_0\(7),
      Q => tmp_101_reg_364(7),
      R => '0'
    );
\tmp_102_reg_369_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => tmp_101_reg_3640,
      D => D(0),
      Q => tmp_102_reg_369(0),
      R => '0'
    );
\tmp_102_reg_369_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => tmp_101_reg_3640,
      D => D(1),
      Q => tmp_102_reg_369(1),
      R => '0'
    );
\tmp_102_reg_369_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => tmp_101_reg_3640,
      D => D(2),
      Q => tmp_102_reg_369(2),
      R => '0'
    );
\tmp_102_reg_369_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => tmp_101_reg_3640,
      D => D(3),
      Q => tmp_102_reg_369(3),
      R => '0'
    );
\tmp_102_reg_369_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => tmp_101_reg_3640,
      D => D(4),
      Q => tmp_102_reg_369(4),
      R => '0'
    );
\tmp_102_reg_369_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => tmp_101_reg_3640,
      D => D(5),
      Q => tmp_102_reg_369(5),
      R => '0'
    );
\tmp_102_reg_369_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => tmp_101_reg_3640,
      D => D(6),
      Q => tmp_102_reg_369(6),
      R => '0'
    );
\tmp_102_reg_369_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => tmp_101_reg_3640,
      D => D(7),
      Q => tmp_102_reg_369(7),
      R => '0'
    );
\tmp_20_reg_355[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"57FFFFFF57000000"
    )
        port map (
      I0 => \j_reg_214_reg__0\(10),
      I1 => \j_reg_214_reg__0\(9),
      I2 => \j_reg_214_reg__0\(8),
      I3 => ap_CS_fsm_pp0_stage0,
      I4 => ap_block_pp0_stage0_subdone3_in,
      I5 => tmp_20_reg_355,
      O => \tmp_20_reg_355[0]_i_1_n_2\
    );
\tmp_20_reg_355_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_20_reg_355[0]_i_1_n_2\,
      Q => tmp_20_reg_355,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D is
  port (
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \mOutPtr_reg[1]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \mOutPtr_reg[1]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \mOutPtr_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \mOutPtr_reg[0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    shiftReg_ce : out STD_LOGIC;
    \ap_CS_fsm_reg[1]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \ap_CS_fsm_reg[0]_0\ : out STD_LOGIC;
    start_once_reg_reg : out STD_LOGIC;
    ap_reg_grp_Filter2D_fu_96_ap_start_reg : out STD_LOGIC;
    \SRL_SIG_reg[0][7]\ : out STD_LOGIC;
    \SRL_SIG_reg[0][0]\ : out STD_LOGIC;
    \SRL_SIG_reg[0][1]\ : out STD_LOGIC;
    \SRL_SIG_reg[0][2]\ : out STD_LOGIC;
    \SRL_SIG_reg[0][3]\ : out STD_LOGIC;
    \SRL_SIG_reg[0][4]\ : out STD_LOGIC;
    \SRL_SIG_reg[0][5]\ : out STD_LOGIC;
    \SRL_SIG_reg[0][6]\ : out STD_LOGIC;
    \SRL_SIG_reg[0][7]_0\ : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_rst_n_inv : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    shiftReg_ce_0 : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \mOutPtr_reg[1]_1\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \mOutPtr_reg[1]_2\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \mOutPtr_reg[1]_3\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ap_reg_grp_Filter2D_fu_96_ap_start : in STD_LOGIC;
    shiftReg_ce_1 : in STD_LOGIC;
    img1_data_stream_0_s_empty_n : in STD_LOGIC;
    img1_data_stream_2_s_empty_n : in STD_LOGIC;
    img1_data_stream_1_s_empty_n : in STD_LOGIC;
    img2_data_stream_0_s_full_n : in STD_LOGIC;
    img2_data_stream_1_s_full_n : in STD_LOGIC;
    img2_data_stream_2_s_full_n : in STD_LOGIC;
    start_once_reg_reg_0 : in STD_LOGIC;
    start_for_CvtColor_1_U0_full_n : in STD_LOGIC;
    Sobel_U0_ap_start : in STD_LOGIC;
    \SRL_SIG_reg[0][7]_1\ : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D is
  signal \^e\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal ImagLoc_x_fu_964_p2 : STD_LOGIC_VECTOR ( 11 to 11 );
  signal ImagLoc_x_reg_2592 : STD_LOGIC_VECTOR ( 10 downto 1 );
  signal ImagLoc_x_reg_25920 : STD_LOGIC;
  signal \ImagLoc_x_reg_2592[10]_i_1_n_2\ : STD_LOGIC;
  signal \ImagLoc_x_reg_2592[10]_i_2_n_2\ : STD_LOGIC;
  signal \ImagLoc_x_reg_2592[1]_i_1_n_2\ : STD_LOGIC;
  signal \ImagLoc_x_reg_2592[2]_i_1_n_2\ : STD_LOGIC;
  signal \ImagLoc_x_reg_2592[3]_i_1_n_2\ : STD_LOGIC;
  signal \ImagLoc_x_reg_2592[4]_i_1_n_2\ : STD_LOGIC;
  signal \ImagLoc_x_reg_2592[5]_i_1_n_2\ : STD_LOGIC;
  signal \ImagLoc_x_reg_2592[6]_i_1_n_2\ : STD_LOGIC;
  signal \ImagLoc_x_reg_2592[7]_i_1_n_2\ : STD_LOGIC;
  signal \ImagLoc_x_reg_2592[8]_i_1_n_2\ : STD_LOGIC;
  signal \ImagLoc_x_reg_2592[9]_i_1_n_2\ : STD_LOGIC;
  signal \ap_CS_fsm[3]_i_2__0_n_2\ : STD_LOGIC;
  signal \ap_CS_fsm[3]_i_3_n_2\ : STD_LOGIC;
  signal \ap_CS_fsm[5]_i_2__0_n_2\ : STD_LOGIC;
  signal ap_CS_fsm_pp0_stage0 : STD_LOGIC;
  signal \^ap_cs_fsm_reg[0]_0\ : STD_LOGIC;
  signal \ap_CS_fsm_reg_n_2_[0]\ : STD_LOGIC;
  signal ap_CS_fsm_state11 : STD_LOGIC;
  signal ap_CS_fsm_state2 : STD_LOGIC;
  signal ap_CS_fsm_state3 : STD_LOGIC;
  signal ap_CS_fsm_state4 : STD_LOGIC;
  signal ap_NS_fsm : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal ap_NS_fsm1 : STD_LOGIC;
  signal ap_block_pp0_stage0_subdone2_in : STD_LOGIC;
  signal ap_enable_reg_pp0_iter0 : STD_LOGIC;
  signal \ap_enable_reg_pp0_iter0_i_1__0_n_2\ : STD_LOGIC;
  signal ap_enable_reg_pp0_iter1 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter2 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter3 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter3_i_1_n_2 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter4 : STD_LOGIC;
  signal \ap_enable_reg_pp0_iter5_i_1__0_n_2\ : STD_LOGIC;
  signal ap_enable_reg_pp0_iter5_reg_n_2 : STD_LOGIC;
  signal ap_reg_grp_Filter2D_fu_96_ap_start_i_2_n_2 : STD_LOGIC;
  signal ap_reg_pp0_iter1_brmerge_reg_2627 : STD_LOGIC;
  signal ap_reg_pp0_iter1_brmerge_reg_26270 : STD_LOGIC;
  signal \ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg_n_2_[0]\ : STD_LOGIC;
  signal ap_reg_pp0_iter1_or_cond_i_i_reg_2607 : STD_LOGIC;
  signal ap_reg_pp0_iter1_or_cond_i_reg_2640 : STD_LOGIC;
  signal ap_reg_pp0_iter2_exitcond389_i_reg_2583 : STD_LOGIC;
  signal ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal ap_reg_pp0_iter2_or_cond_i_i_reg_2607 : STD_LOGIC;
  signal ap_reg_pp0_iter2_or_cond_i_reg_2640 : STD_LOGIC;
  signal ap_reg_pp0_iter2_reg_588 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal ap_reg_pp0_iter3_or_cond_i_reg_2640 : STD_LOGIC;
  signal ap_reg_pp0_iter4_or_cond_i_reg_2640 : STD_LOGIC;
  signal brmerge_fu_1034_p2 : STD_LOGIC;
  signal brmerge_reg_2627 : STD_LOGIC;
  signal ce119_out : STD_LOGIC;
  signal col_buf_0_val_0_0_fu_1140_p3 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal col_buf_0_val_0_0_reg_2708 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal col_buf_0_val_0_0_reg_27080 : STD_LOGIC;
  signal col_buf_0_val_1_0_fu_1159_p3 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal col_buf_0_val_1_0_reg_2721 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal col_buf_0_val_2_0_fu_1178_p3 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal col_buf_0_val_2_0_reg_2729 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal exitcond389_i_fu_936_p2 : STD_LOGIC;
  signal \exitcond389_i_reg_2583[0]_i_3_n_2\ : STD_LOGIC;
  signal \exitcond389_i_reg_2583[0]_i_4_n_2\ : STD_LOGIC;
  signal \exitcond389_i_reg_2583[0]_i_5_n_2\ : STD_LOGIC;
  signal \exitcond389_i_reg_2583_reg_n_2_[0]\ : STD_LOGIC;
  signal i_V_fu_631_p2 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal i_V_reg_2495 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \i_V_reg_2495[9]_i_2_n_2\ : STD_LOGIC;
  signal \icmp_reg_2509[0]_i_1_n_2\ : STD_LOGIC;
  signal \icmp_reg_2509[0]_i_2_n_2\ : STD_LOGIC;
  signal \icmp_reg_2509[0]_i_3_n_2\ : STD_LOGIC;
  signal \icmp_reg_2509_reg_n_2_[0]\ : STD_LOGIC;
  signal isneg_1_reg_29320 : STD_LOGIC;
  signal j_V_fu_942_p2 : STD_LOGIC_VECTOR ( 10 downto 1 );
  signal k_buf_0_val_3_U_n_10 : STD_LOGIC;
  signal k_buf_0_val_3_U_n_13 : STD_LOGIC;
  signal k_buf_0_val_3_addr_reg_26490 : STD_LOGIC;
  signal k_buf_0_val_3_load_reg_2703 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal k_buf_0_val_3_load_reg_27030 : STD_LOGIC;
  signal k_buf_0_val_3_q0 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal k_buf_0_val_4_load_reg_2716 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal k_buf_0_val_4_q0 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal k_buf_0_val_5_U_n_10 : STD_LOGIC;
  signal k_buf_0_val_5_U_n_11 : STD_LOGIC;
  signal k_buf_0_val_5_U_n_12 : STD_LOGIC;
  signal k_buf_0_val_5_U_n_13 : STD_LOGIC;
  signal k_buf_0_val_5_U_n_14 : STD_LOGIC;
  signal k_buf_0_val_5_U_n_15 : STD_LOGIC;
  signal k_buf_0_val_5_U_n_16 : STD_LOGIC;
  signal k_buf_0_val_5_U_n_17 : STD_LOGIC;
  signal k_buf_0_val_5_U_n_18 : STD_LOGIC;
  signal k_buf_0_val_5_U_n_19 : STD_LOGIC;
  signal k_buf_0_val_5_q0 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal k_buf_2_val_5_addr_reg_2697 : STD_LOGIC_VECTOR ( 10 downto 2 );
  signal or_cond_i425_i_fu_709_p2 : STD_LOGIC;
  signal or_cond_i425_i_reg_2542 : STD_LOGIC;
  signal or_cond_i_fu_1039_p2 : STD_LOGIC;
  signal or_cond_i_i_fu_990_p2 : STD_LOGIC;
  signal or_cond_i_i_reg_2607 : STD_LOGIC;
  signal or_cond_i_reg_2640 : STD_LOGIC;
  signal \or_cond_i_reg_2640[0]_i_2_n_2\ : STD_LOGIC;
  signal \or_cond_i_reg_2640[0]_i_3_n_2\ : STD_LOGIC;
  signal \p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_1_n_2\ : STD_LOGIC;
  signal \p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_2_n_2\ : STD_LOGIC;
  signal \p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_3_n_2\ : STD_LOGIC;
  signal \p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_4_n_2\ : STD_LOGIC;
  signal \p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_2\ : STD_LOGIC;
  signal \p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_3\ : STD_LOGIC;
  signal \p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_4\ : STD_LOGIC;
  signal \p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_5\ : STD_LOGIC;
  signal \p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_6\ : STD_LOGIC;
  signal \p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_7\ : STD_LOGIC;
  signal \p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_8\ : STD_LOGIC;
  signal \p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_9\ : STD_LOGIC;
  signal \p_Val2_10_0_0_2_fu_1493_p2_carry__1_n_9\ : STD_LOGIC;
  signal p_Val2_10_0_0_2_fu_1493_p2_carry_i_1_n_2 : STD_LOGIC;
  signal p_Val2_10_0_0_2_fu_1493_p2_carry_i_2_n_2 : STD_LOGIC;
  signal p_Val2_10_0_0_2_fu_1493_p2_carry_i_3_n_2 : STD_LOGIC;
  signal p_Val2_10_0_0_2_fu_1493_p2_carry_i_4_n_2 : STD_LOGIC;
  signal p_Val2_10_0_0_2_fu_1493_p2_carry_i_5_n_2 : STD_LOGIC;
  signal p_Val2_10_0_0_2_fu_1493_p2_carry_n_2 : STD_LOGIC;
  signal p_Val2_10_0_0_2_fu_1493_p2_carry_n_3 : STD_LOGIC;
  signal p_Val2_10_0_0_2_fu_1493_p2_carry_n_4 : STD_LOGIC;
  signal p_Val2_10_0_0_2_fu_1493_p2_carry_n_5 : STD_LOGIC;
  signal p_Val2_10_0_0_2_fu_1493_p2_carry_n_6 : STD_LOGIC;
  signal p_Val2_10_0_0_2_fu_1493_p2_carry_n_7 : STD_LOGIC;
  signal p_Val2_10_0_0_2_fu_1493_p2_carry_n_8 : STD_LOGIC;
  signal p_Val2_10_0_0_2_fu_1493_p2_carry_n_9 : STD_LOGIC;
  signal p_Val2_10_0_0_2_reg_2817 : STD_LOGIC_VECTOR ( 8 to 8 );
  signal p_Val2_10_0_0_2_reg_28170 : STD_LOGIC;
  signal p_Val2_1_fu_1973_p2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \p_Val2_1_fu_1973_p2__1_carry__0_i_1_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry__0_i_2_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry__0_i_3_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry__0_i_4_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry__0_i_5_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry__0_n_4\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry__0_n_5\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry__0_n_7\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry__0_n_8\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry__0_n_9\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry_i_1_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry_i_2_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry_i_3_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry_i_4_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry_i_5_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry_i_6_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry_i_7_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry_n_3\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry_n_4\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry_n_5\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry_n_6\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry_n_7\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__1_carry_n_8\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__20_carry__0_i_1_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__20_carry__0_i_2_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__20_carry__0_i_3_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__20_carry__0_i_4_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__20_carry__0_i_5_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__20_carry__0_i_6_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__20_carry__0_i_7_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__20_carry__0_n_3\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__20_carry__0_n_4\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__20_carry__0_n_5\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__20_carry_i_1_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__20_carry_i_2_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__20_carry_i_3_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__20_carry_i_4_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__20_carry_i_5_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__20_carry_i_6_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__20_carry_i_7_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__20_carry_n_2\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__20_carry_n_3\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__20_carry_n_4\ : STD_LOGIC;
  signal \p_Val2_1_fu_1973_p2__20_carry_n_5\ : STD_LOGIC;
  signal p_Val2_1_reg_2922 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal p_Val2_2_fu_1946_p2 : STD_LOGIC_VECTOR ( 10 downto 8 );
  signal \p_Val2_2_fu_1946_p2__27_carry__0_i_1_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry__0_i_2_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry__0_i_3_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry__0_i_4_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry__0_i_5_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry__0_i_6_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry__0_i_7_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry__0_i_8_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry__0_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry__0_n_3\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry__0_n_4\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry__0_n_5\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry__1_i_1_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry__1_i_2_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry__1_i_3_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry__1_i_4_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry__1_i_5_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry__1_n_4\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry__1_n_5\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry_i_1_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry_i_2_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry_i_3_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry_i_4_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry_i_5_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry_i_6_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry_n_3\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry_n_4\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2__27_carry_n_5\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2_carry__0_i_1_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2_carry__0_i_2_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2_carry__0_i_3_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2_carry__0_i_4_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2_carry__0_n_2\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2_carry__0_n_3\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2_carry__0_n_4\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2_carry__0_n_5\ : STD_LOGIC;
  signal \p_Val2_2_fu_1946_p2_carry__1_n_4\ : STD_LOGIC;
  signal p_Val2_2_fu_1946_p2_carry_i_1_n_2 : STD_LOGIC;
  signal p_Val2_2_fu_1946_p2_carry_i_2_n_2 : STD_LOGIC;
  signal p_Val2_2_fu_1946_p2_carry_i_3_n_2 : STD_LOGIC;
  signal p_Val2_2_fu_1946_p2_carry_i_4_n_2 : STD_LOGIC;
  signal p_Val2_2_fu_1946_p2_carry_n_2 : STD_LOGIC;
  signal p_Val2_2_fu_1946_p2_carry_n_3 : STD_LOGIC;
  signal p_Val2_2_fu_1946_p2_carry_n_4 : STD_LOGIC;
  signal p_Val2_2_fu_1946_p2_carry_n_5 : STD_LOGIC;
  signal p_assign_2_fu_1028_p2 : STD_LOGIC_VECTOR ( 8 downto 1 );
  signal p_assign_2_reg_2622 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \p_assign_2_reg_2622[0]_i_1_n_2\ : STD_LOGIC;
  signal \p_assign_2_reg_2622[10]_i_1_n_2\ : STD_LOGIC;
  signal \p_assign_2_reg_2622[10]_i_2_n_2\ : STD_LOGIC;
  signal \p_assign_2_reg_2622[10]_i_3_n_2\ : STD_LOGIC;
  signal \p_assign_2_reg_2622[10]_i_4_n_2\ : STD_LOGIC;
  signal \p_assign_2_reg_2622[10]_i_5_n_2\ : STD_LOGIC;
  signal \p_assign_2_reg_2622[10]_i_6_n_2\ : STD_LOGIC;
  signal \p_assign_2_reg_2622[10]_i_7_n_2\ : STD_LOGIC;
  signal \p_assign_2_reg_2622[10]_i_8_n_2\ : STD_LOGIC;
  signal p_assign_7_fu_723_p2 : STD_LOGIC_VECTOR ( 7 to 7 );
  signal p_assign_7_reg_2553 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal \p_assign_7_reg_2553[4]_i_1_n_2\ : STD_LOGIC;
  signal \p_assign_7_reg_2553[5]_i_1_n_2\ : STD_LOGIC;
  signal \p_assign_7_reg_2553[6]_i_1_n_2\ : STD_LOGIC;
  signal \p_assign_7_reg_2553[8]_i_1_n_2\ : STD_LOGIC;
  signal \p_assign_7_reg_2553[8]_i_2_n_2\ : STD_LOGIC;
  signal \p_assign_7_reg_2553[9]_i_1_n_2\ : STD_LOGIC;
  signal \p_assign_7_reg_2553[9]_i_2_n_2\ : STD_LOGIC;
  signal \p_p2_i_i_cast_cast_reg_2612[10]_i_1_n_2\ : STD_LOGIC;
  signal \p_p2_i_i_cast_cast_reg_2612[1]_i_1_n_2\ : STD_LOGIC;
  signal \p_p2_i_i_cast_cast_reg_2612[2]_i_1_n_2\ : STD_LOGIC;
  signal \p_p2_i_i_cast_cast_reg_2612[3]_i_1_n_2\ : STD_LOGIC;
  signal \p_p2_i_i_cast_cast_reg_2612[4]_i_1_n_2\ : STD_LOGIC;
  signal \p_p2_i_i_cast_cast_reg_2612[5]_i_1_n_2\ : STD_LOGIC;
  signal \p_p2_i_i_cast_cast_reg_2612[5]_i_2_n_2\ : STD_LOGIC;
  signal \p_p2_i_i_cast_cast_reg_2612[6]_i_1_n_2\ : STD_LOGIC;
  signal \p_p2_i_i_cast_cast_reg_2612[7]_i_1_n_2\ : STD_LOGIC;
  signal \p_p2_i_i_cast_cast_reg_2612[8]_i_1_n_2\ : STD_LOGIC;
  signal \p_p2_i_i_cast_cast_reg_2612[9]_i_1_n_2\ : STD_LOGIC;
  signal p_p2_i_i_cast_cast_reg_2612_reg : STD_LOGIC_VECTOR ( 10 downto 1 );
  signal r_V_7_0_1_fu_1511_p2 : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal reg_588 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal right_border_buf_0_1_fu_322 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal right_border_buf_0_2_fu_330 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal right_border_buf_0_3_fu_334 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal right_border_buf_0_4_fu_342 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal right_border_buf_0_5_fu_346 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal right_border_buf_0_s_fu_318 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal row_assign_9_0_1_t_fu_825_p2 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal row_assign_9_0_1_t_reg_2558 : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal row_assign_9_0_2_t_fu_839_p2 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal row_assign_9_0_2_t_reg_2565 : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^shiftreg_ce\ : STD_LOGIC;
  signal src_kernel_win_0_va_1_fu_250 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal src_kernel_win_0_va_1_fu_2500 : STD_LOGIC;
  signal src_kernel_win_0_va_2_fu_254 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal src_kernel_win_0_va_3_fu_258 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal src_kernel_win_0_va_4_fu_262 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal src_kernel_win_0_va_5_fu_266 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal src_kernel_win_0_va_6_fu_1442_p3 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal src_kernel_win_0_va_6_reg_2805 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal src_kernel_win_0_va_7_fu_1456_p3 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal src_kernel_win_0_va_7_reg_2811 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal src_kernel_win_0_va_fu_246 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal t_V_2_reg_577 : STD_LOGIC;
  signal t_V_2_reg_5770 : STD_LOGIC;
  signal \t_V_2_reg_577[10]_i_4_n_2\ : STD_LOGIC;
  signal \t_V_2_reg_577_reg__0\ : STD_LOGIC_VECTOR ( 10 downto 1 );
  signal \t_V_2_reg_577_reg__0__0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal t_V_reg_566 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal tmp57_fu_1547_p2 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \tmp57_fu_1547_p2_carry__0_i_1_n_2\ : STD_LOGIC;
  signal \tmp57_fu_1547_p2_carry__0_i_2_n_2\ : STD_LOGIC;
  signal \tmp57_fu_1547_p2_carry__0_i_3_n_2\ : STD_LOGIC;
  signal \tmp57_fu_1547_p2_carry__0_i_4_n_2\ : STD_LOGIC;
  signal \tmp57_fu_1547_p2_carry__0_i_5_n_2\ : STD_LOGIC;
  signal \tmp57_fu_1547_p2_carry__0_n_2\ : STD_LOGIC;
  signal \tmp57_fu_1547_p2_carry__0_n_3\ : STD_LOGIC;
  signal \tmp57_fu_1547_p2_carry__0_n_4\ : STD_LOGIC;
  signal \tmp57_fu_1547_p2_carry__0_n_5\ : STD_LOGIC;
  signal \tmp57_fu_1547_p2_carry__1_i_1_n_2\ : STD_LOGIC;
  signal \tmp57_fu_1547_p2_carry__1_i_2_n_2\ : STD_LOGIC;
  signal \tmp57_fu_1547_p2_carry__1_i_3_n_2\ : STD_LOGIC;
  signal \tmp57_fu_1547_p2_carry__1_i_4_n_2\ : STD_LOGIC;
  signal \tmp57_fu_1547_p2_carry__1_n_4\ : STD_LOGIC;
  signal \tmp57_fu_1547_p2_carry__1_n_5\ : STD_LOGIC;
  signal tmp57_fu_1547_p2_carry_i_1_n_2 : STD_LOGIC;
  signal tmp57_fu_1547_p2_carry_i_2_n_2 : STD_LOGIC;
  signal tmp57_fu_1547_p2_carry_i_3_n_2 : STD_LOGIC;
  signal tmp57_fu_1547_p2_carry_i_4_n_2 : STD_LOGIC;
  signal tmp57_fu_1547_p2_carry_n_2 : STD_LOGIC;
  signal tmp57_fu_1547_p2_carry_n_3 : STD_LOGIC;
  signal tmp57_fu_1547_p2_carry_n_4 : STD_LOGIC;
  signal tmp57_fu_1547_p2_carry_n_5 : STD_LOGIC;
  signal tmp57_reg_2837 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal tmp69_cast_fu_1936_p1 : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \tmp_116_0_1_reg_2518[0]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_116_0_1_reg_2518_reg_n_2_[0]\ : STD_LOGIC;
  signal tmp_160_0_0_2_cast_fu_1489_p1 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal tmp_160_0_2_cast_fu_1539_p1 : STD_LOGIC_VECTOR ( 7 downto 1 );
  signal tmp_1_fu_637_p2 : STD_LOGIC;
  signal tmp_1_reg_2500 : STD_LOGIC;
  signal \tmp_2_reg_2514[0]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_2_reg_2514_reg_n_2_[0]\ : STD_LOGIC;
  signal tmp_31_fu_984_p2 : STD_LOGIC;
  signal tmp_31_fu_984_p2_carry_i_1_n_2 : STD_LOGIC;
  signal tmp_31_fu_984_p2_carry_i_2_n_2 : STD_LOGIC;
  signal tmp_31_fu_984_p2_carry_i_3_n_2 : STD_LOGIC;
  signal tmp_31_fu_984_p2_carry_i_4_n_2 : STD_LOGIC;
  signal tmp_31_fu_984_p2_carry_n_5 : STD_LOGIC;
  signal tmp_31_reg_2602 : STD_LOGIC;
  signal tmp_32_fu_896_p2 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal tmp_32_reg_2572 : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \tmp_32_reg_2572[0]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_32_reg_2572[1]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_32_reg_2572[1]_i_3_n_2\ : STD_LOGIC;
  signal \tmp_32_reg_2572[1]_i_4_n_2\ : STD_LOGIC;
  signal \tmp_32_reg_2572[1]_i_5_n_2\ : STD_LOGIC;
  signal tmp_33_fu_1022_p2 : STD_LOGIC;
  signal tmp_33_fu_1022_p2_carry_i_1_n_2 : STD_LOGIC;
  signal tmp_33_fu_1022_p2_carry_i_2_n_2 : STD_LOGIC;
  signal tmp_33_fu_1022_p2_carry_i_3_n_2 : STD_LOGIC;
  signal tmp_33_fu_1022_p2_carry_n_5 : STD_LOGIC;
  signal tmp_33_reg_2617 : STD_LOGIC;
  signal tmp_3_fu_677_p2 : STD_LOGIC;
  signal tmp_3_reg_2522 : STD_LOGIC;
  signal \tmp_3_reg_2522[0]_i_2_n_2\ : STD_LOGIC;
  signal \tmp_3_reg_2522[0]_i_3_n_2\ : STD_LOGIC;
  signal tmp_47_reg_2597 : STD_LOGIC;
  signal tmp_49_reg_2644 : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal tmp_4_reg_2535 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \tmp_4_reg_2535[10]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_4_reg_2535[1]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_4_reg_2535[4]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_4_reg_2535[5]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_4_reg_2535[6]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_4_reg_2535[6]_i_2_n_2\ : STD_LOGIC;
  signal \tmp_4_reg_2535[7]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_4_reg_2535[8]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_4_reg_2535[8]_i_2_n_2\ : STD_LOGIC;
  signal \tmp_4_reg_2535[9]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_4_reg_2535_reg_n_2_[10]\ : STD_LOGIC;
  signal \tmp_4_reg_2535_reg_n_2_[4]\ : STD_LOGIC;
  signal \tmp_4_reg_2535_reg_n_2_[5]\ : STD_LOGIC;
  signal \tmp_4_reg_2535_reg_n_2_[6]\ : STD_LOGIC;
  signal \tmp_4_reg_2535_reg_n_2_[7]\ : STD_LOGIC;
  signal \tmp_4_reg_2535_reg_n_2_[8]\ : STD_LOGIC;
  signal \tmp_4_reg_2535_reg_n_2_[9]\ : STD_LOGIC;
  signal tmp_53_reg_2822 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal tmp_54_reg_2827 : STD_LOGIC_VECTOR ( 7 downto 1 );
  signal \tmp_54_reg_2827[7]_i_2_n_2\ : STD_LOGIC;
  signal tmp_56_reg_2832 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \tmp_56_reg_2832[7]_i_2_n_2\ : STD_LOGIC;
  signal tmp_57_reg_2927 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal tmp_5_reg_555 : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \tmp_5_reg_555[0]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_5_reg_555[1]_i_1_n_2\ : STD_LOGIC;
  signal tmp_72_not_fu_643_p2 : STD_LOGIC;
  signal tmp_72_not_reg_2504 : STD_LOGIC;
  signal \NLW_p_Val2_10_0_0_2_fu_1493_p2_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_p_Val2_10_0_0_2_fu_1493_p2_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_p_Val2_1_fu_1973_p2__1_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_p_Val2_1_fu_1973_p2__1_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_p_Val2_1_fu_1973_p2__1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_p_Val2_1_fu_1973_p2__20_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_p_Val2_2_fu_1946_p2__27_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_p_Val2_2_fu_1946_p2__27_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_p_Val2_2_fu_1946_p2__27_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_p_Val2_2_fu_1946_p2__27_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_p_Val2_2_fu_1946_p2_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_p_Val2_2_fu_1946_p2_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_tmp57_fu_1547_p2_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_tmp57_fu_1547_p2_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_tmp_31_fu_984_p2_carry_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_tmp_31_fu_984_p2_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_tmp_33_fu_1022_p2_carry_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_tmp_33_fu_1022_p2_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \ImagLoc_x_reg_2592[1]_i_1\ : label is "soft_lutpair156";
  attribute SOFT_HLUTNM of \ImagLoc_x_reg_2592[2]_i_1\ : label is "soft_lutpair140";
  attribute SOFT_HLUTNM of \ImagLoc_x_reg_2592[3]_i_1\ : label is "soft_lutpair146";
  attribute SOFT_HLUTNM of \ImagLoc_x_reg_2592[4]_i_1\ : label is "soft_lutpair129";
  attribute SOFT_HLUTNM of \ImagLoc_x_reg_2592[6]_i_1\ : label is "soft_lutpair158";
  attribute SOFT_HLUTNM of \ImagLoc_x_reg_2592[7]_i_1\ : label is "soft_lutpair154";
  attribute SOFT_HLUTNM of \ImagLoc_x_reg_2592[8]_i_1\ : label is "soft_lutpair118";
  attribute SOFT_HLUTNM of \ImagLoc_x_reg_2592[9]_i_1\ : label is "soft_lutpair118";
  attribute SOFT_HLUTNM of \SRL_SIG[0][0]_i_1__3\ : label is "soft_lutpair141";
  attribute SOFT_HLUTNM of \SRL_SIG[0][1]_i_1__3\ : label is "soft_lutpair142";
  attribute SOFT_HLUTNM of \SRL_SIG[0][2]_i_1__3\ : label is "soft_lutpair143";
  attribute SOFT_HLUTNM of \SRL_SIG[0][3]_i_1__3\ : label is "soft_lutpair144";
  attribute SOFT_HLUTNM of \SRL_SIG[0][4]_i_1__3\ : label is "soft_lutpair142";
  attribute SOFT_HLUTNM of \SRL_SIG[0][5]_i_1__3\ : label is "soft_lutpair144";
  attribute SOFT_HLUTNM of \SRL_SIG[0][7]_i_1__4\ : label is "soft_lutpair141";
  attribute SOFT_HLUTNM of \SRL_SIG[0][7]_i_2__0\ : label is "soft_lutpair130";
  attribute SOFT_HLUTNM of \SRL_SIG[0][7]_i_3\ : label is "soft_lutpair143";
  attribute SOFT_HLUTNM of \ap_CS_fsm[0]_i_1__0\ : label is "soft_lutpair117";
  attribute SOFT_HLUTNM of \ap_CS_fsm[0]_i_1__3\ : label is "soft_lutpair119";
  attribute SOFT_HLUTNM of \ap_CS_fsm[1]_i_1__2\ : label is "soft_lutpair117";
  attribute SOFT_HLUTNM of \ap_CS_fsm[1]_i_2\ : label is "soft_lutpair119";
  attribute SOFT_HLUTNM of \ap_CS_fsm[3]_i_3\ : label is "soft_lutpair138";
  attribute SOFT_HLUTNM of \ap_CS_fsm[4]_i_1__0\ : label is "soft_lutpair150";
  attribute SOFT_HLUTNM of \ap_CS_fsm[5]_i_1__0\ : label is "soft_lutpair150";
  attribute FSM_ENCODING : string;
  attribute FSM_ENCODING of \ap_CS_fsm_reg[0]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[1]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[2]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[3]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[4]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[5]\ : label is "none";
  attribute SOFT_HLUTNM of ap_reg_grp_Filter2D_fu_96_ap_start_i_2 : label is "soft_lutpair136";
  attribute SOFT_HLUTNM of \brmerge_reg_2627[0]_i_1\ : label is "soft_lutpair159";
  attribute SOFT_HLUTNM of \exitcond389_i_reg_2583[0]_i_3\ : label is "soft_lutpair122";
  attribute SOFT_HLUTNM of \exitcond389_i_reg_2583[0]_i_4\ : label is "soft_lutpair147";
  attribute SOFT_HLUTNM of \exitcond389_i_reg_2583[0]_i_5\ : label is "soft_lutpair139";
  attribute SOFT_HLUTNM of \i_V_reg_2495[0]_i_1\ : label is "soft_lutpair157";
  attribute SOFT_HLUTNM of \i_V_reg_2495[2]_i_1\ : label is "soft_lutpair145";
  attribute SOFT_HLUTNM of \i_V_reg_2495[3]_i_1\ : label is "soft_lutpair145";
  attribute SOFT_HLUTNM of \i_V_reg_2495[4]_i_1\ : label is "soft_lutpair127";
  attribute SOFT_HLUTNM of \i_V_reg_2495[7]_i_1\ : label is "soft_lutpair138";
  attribute SOFT_HLUTNM of \i_V_reg_2495[8]_i_1\ : label is "soft_lutpair131";
  attribute SOFT_HLUTNM of \i_V_reg_2495[9]_i_1\ : label is "soft_lutpair131";
  attribute SOFT_HLUTNM of \icmp_reg_2509[0]_i_1\ : label is "soft_lutpair155";
  attribute SOFT_HLUTNM of \icmp_reg_2509[0]_i_3\ : label is "soft_lutpair134";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_1__4\ : label is "soft_lutpair123";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_1__5\ : label is "soft_lutpair130";
  attribute SOFT_HLUTNM of \mOutPtr[1]_i_2__1\ : label is "soft_lutpair123";
  attribute SOFT_HLUTNM of \or_cond_i425_i_reg_2542[0]_i_1\ : label is "soft_lutpair125";
  attribute SOFT_HLUTNM of \or_cond_i_i_reg_2607[0]_i_2\ : label is "soft_lutpair159";
  attribute SOFT_HLUTNM of \or_cond_i_reg_2640[0]_i_3\ : label is "soft_lutpair124";
  attribute SOFT_HLUTNM of p_Val2_10_0_0_2_fu_1493_p2_carry_i_5 : label is "soft_lutpair137";
  attribute HLUTNM : string;
  attribute HLUTNM of \p_Val2_1_fu_1973_p2__1_carry__0_i_1\ : label is "lutpair3";
  attribute HLUTNM of \p_Val2_1_fu_1973_p2__1_carry__0_i_5\ : label is "lutpair3";
  attribute HLUTNM of \p_Val2_1_fu_1973_p2__20_carry__0_i_3\ : label is "lutpair6";
  attribute HLUTNM of \p_Val2_1_fu_1973_p2__20_carry_i_1\ : label is "lutpair5";
  attribute HLUTNM of \p_Val2_1_fu_1973_p2__20_carry_i_3\ : label is "lutpair4";
  attribute HLUTNM of \p_Val2_1_fu_1973_p2__20_carry_i_4\ : label is "lutpair6";
  attribute HLUTNM of \p_Val2_1_fu_1973_p2__20_carry_i_5\ : label is "lutpair5";
  attribute HLUTNM of \p_Val2_1_fu_1973_p2__20_carry_i_7\ : label is "lutpair4";
  attribute HLUTNM of \p_Val2_2_fu_1946_p2__27_carry__0_i_4\ : label is "lutpair1";
  attribute HLUTNM of \p_Val2_2_fu_1946_p2__27_carry__0_i_5\ : label is "lutpair2";
  attribute HLUTNM of \p_Val2_2_fu_1946_p2__27_carry__1_i_2\ : label is "lutpair2";
  attribute HLUTNM of \p_Val2_2_fu_1946_p2__27_carry_i_1\ : label is "lutpair0";
  attribute HLUTNM of \p_Val2_2_fu_1946_p2__27_carry_i_3\ : label is "lutpair1";
  attribute HLUTNM of \p_Val2_2_fu_1946_p2__27_carry_i_4\ : label is "lutpair0";
  attribute SOFT_HLUTNM of \p_assign_2_reg_2622[10]_i_2\ : label is "soft_lutpair154";
  attribute SOFT_HLUTNM of \p_assign_2_reg_2622[10]_i_4\ : label is "soft_lutpair129";
  attribute SOFT_HLUTNM of \p_assign_2_reg_2622[10]_i_6\ : label is "soft_lutpair121";
  attribute SOFT_HLUTNM of \p_assign_2_reg_2622[10]_i_8\ : label is "soft_lutpair153";
  attribute SOFT_HLUTNM of \p_assign_2_reg_2622[1]_i_1\ : label is "soft_lutpair146";
  attribute SOFT_HLUTNM of \p_assign_2_reg_2622[5]_i_1\ : label is "soft_lutpair124";
  attribute SOFT_HLUTNM of \p_assign_2_reg_2622[6]_i_1\ : label is "soft_lutpair158";
  attribute SOFT_HLUTNM of \p_assign_2_reg_2622[8]_i_1\ : label is "soft_lutpair153";
  attribute SOFT_HLUTNM of \p_assign_7_reg_2553[4]_i_1\ : label is "soft_lutpair120";
  attribute SOFT_HLUTNM of \p_assign_7_reg_2553[5]_i_1\ : label is "soft_lutpair120";
  attribute SOFT_HLUTNM of \p_assign_7_reg_2553[8]_i_2\ : label is "soft_lutpair135";
  attribute SOFT_HLUTNM of \p_p2_i_i_cast_cast_reg_2612[1]_i_1\ : label is "soft_lutpair156";
  attribute SOFT_HLUTNM of \p_p2_i_i_cast_cast_reg_2612[2]_i_1\ : label is "soft_lutpair140";
  attribute SOFT_HLUTNM of \p_p2_i_i_cast_cast_reg_2612[3]_i_1\ : label is "soft_lutpair121";
  attribute SOFT_HLUTNM of \p_p2_i_i_cast_cast_reg_2612[5]_i_1\ : label is "soft_lutpair122";
  attribute SOFT_HLUTNM of \p_p2_i_i_cast_cast_reg_2612[5]_i_2\ : label is "soft_lutpair139";
  attribute SOFT_HLUTNM of \row_assign_9_0_1_t_reg_2558[1]_i_1\ : label is "soft_lutpair152";
  attribute SOFT_HLUTNM of \row_assign_9_0_2_t_reg_2565[1]_i_1\ : label is "soft_lutpair155";
  attribute SOFT_HLUTNM of \t_V_2_reg_577[1]_i_1\ : label is "soft_lutpair149";
  attribute SOFT_HLUTNM of \t_V_2_reg_577[2]_i_1\ : label is "soft_lutpair149";
  attribute SOFT_HLUTNM of \t_V_2_reg_577[3]_i_1\ : label is "soft_lutpair133";
  attribute SOFT_HLUTNM of \t_V_2_reg_577[4]_i_1\ : label is "soft_lutpair133";
  attribute SOFT_HLUTNM of \t_V_2_reg_577[7]_i_1\ : label is "soft_lutpair147";
  attribute SOFT_HLUTNM of \t_V_2_reg_577[8]_i_1\ : label is "soft_lutpair132";
  attribute SOFT_HLUTNM of \t_V_2_reg_577[9]_i_1\ : label is "soft_lutpair132";
  attribute SOFT_HLUTNM of \tmp_116_0_1_reg_2518[0]_i_1\ : label is "soft_lutpair136";
  attribute SOFT_HLUTNM of \tmp_32_reg_2572[1]_i_1\ : label is "soft_lutpair137";
  attribute SOFT_HLUTNM of \tmp_4_reg_2535[10]_i_1\ : label is "soft_lutpair152";
  attribute SOFT_HLUTNM of \tmp_4_reg_2535[1]_i_1\ : label is "soft_lutpair157";
  attribute SOFT_HLUTNM of \tmp_4_reg_2535[4]_i_1\ : label is "soft_lutpair135";
  attribute SOFT_HLUTNM of \tmp_4_reg_2535[7]_i_1\ : label is "soft_lutpair134";
  attribute SOFT_HLUTNM of \tmp_4_reg_2535[8]_i_2\ : label is "soft_lutpair127";
  attribute SOFT_HLUTNM of \tmp_4_reg_2535[9]_i_1\ : label is "soft_lutpair125";
  attribute SOFT_HLUTNM of \tmp_56_reg_2832[1]_i_1\ : label is "soft_lutpair151";
  attribute SOFT_HLUTNM of \tmp_56_reg_2832[2]_i_1\ : label is "soft_lutpair151";
  attribute SOFT_HLUTNM of \tmp_56_reg_2832[3]_i_1\ : label is "soft_lutpair128";
  attribute SOFT_HLUTNM of \tmp_56_reg_2832[4]_i_1\ : label is "soft_lutpair128";
  attribute SOFT_HLUTNM of \tmp_56_reg_2832[6]_i_1\ : label is "soft_lutpair148";
  attribute SOFT_HLUTNM of \tmp_56_reg_2832[7]_i_1\ : label is "soft_lutpair148";
  attribute SOFT_HLUTNM of \tmp_5_reg_555[0]_i_1\ : label is "soft_lutpair126";
  attribute SOFT_HLUTNM of \tmp_5_reg_555[1]_i_1\ : label is "soft_lutpair126";
begin
  E(0) <= \^e\(0);
  \ap_CS_fsm_reg[0]_0\ <= \^ap_cs_fsm_reg[0]_0\;
  shiftReg_ce <= \^shiftreg_ce\;
\ImagLoc_x_reg_2592[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAAA9"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(10),
      I1 => \t_V_2_reg_577_reg__0\(8),
      I2 => \t_V_2_reg_577_reg__0\(9),
      I3 => \t_V_2_reg_577_reg__0\(7),
      I4 => \t_V_2_reg_577_reg__0\(6),
      I5 => \ImagLoc_x_reg_2592[10]_i_2_n_2\,
      O => \ImagLoc_x_reg_2592[10]_i_1_n_2\
    );
\ImagLoc_x_reg_2592[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(5),
      I1 => \t_V_2_reg_577_reg__0\(2),
      I2 => \t_V_2_reg_577_reg__0\(1),
      I3 => \t_V_2_reg_577_reg__0__0\(0),
      I4 => \t_V_2_reg_577_reg__0\(3),
      I5 => \t_V_2_reg_577_reg__0\(4),
      O => \ImagLoc_x_reg_2592[10]_i_2_n_2\
    );
\ImagLoc_x_reg_2592[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(1),
      I1 => \t_V_2_reg_577_reg__0__0\(0),
      O => \ImagLoc_x_reg_2592[1]_i_1_n_2\
    );
\ImagLoc_x_reg_2592[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0__0\(0),
      I1 => \t_V_2_reg_577_reg__0\(1),
      I2 => \t_V_2_reg_577_reg__0\(2),
      O => \ImagLoc_x_reg_2592[2]_i_1_n_2\
    );
\ImagLoc_x_reg_2592[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE01"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(2),
      I1 => \t_V_2_reg_577_reg__0\(1),
      I2 => \t_V_2_reg_577_reg__0__0\(0),
      I3 => \t_V_2_reg_577_reg__0\(3),
      O => \ImagLoc_x_reg_2592[3]_i_1_n_2\
    );
\ImagLoc_x_reg_2592[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0001"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(3),
      I1 => \t_V_2_reg_577_reg__0__0\(0),
      I2 => \t_V_2_reg_577_reg__0\(1),
      I3 => \t_V_2_reg_577_reg__0\(2),
      I4 => \t_V_2_reg_577_reg__0\(4),
      O => \ImagLoc_x_reg_2592[4]_i_1_n_2\
    );
\ImagLoc_x_reg_2592[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000001"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(4),
      I1 => \t_V_2_reg_577_reg__0\(3),
      I2 => \t_V_2_reg_577_reg__0__0\(0),
      I3 => \t_V_2_reg_577_reg__0\(1),
      I4 => \t_V_2_reg_577_reg__0\(2),
      I5 => \t_V_2_reg_577_reg__0\(5),
      O => \ImagLoc_x_reg_2592[5]_i_1_n_2\
    );
\ImagLoc_x_reg_2592[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \ImagLoc_x_reg_2592[10]_i_2_n_2\,
      I1 => \t_V_2_reg_577_reg__0\(6),
      O => \ImagLoc_x_reg_2592[6]_i_1_n_2\
    );
\ImagLoc_x_reg_2592[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(6),
      I1 => \ImagLoc_x_reg_2592[10]_i_2_n_2\,
      I2 => \t_V_2_reg_577_reg__0\(7),
      O => \ImagLoc_x_reg_2592[7]_i_1_n_2\
    );
\ImagLoc_x_reg_2592[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAA9"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(8),
      I1 => \ImagLoc_x_reg_2592[10]_i_2_n_2\,
      I2 => \t_V_2_reg_577_reg__0\(6),
      I3 => \t_V_2_reg_577_reg__0\(7),
      O => \ImagLoc_x_reg_2592[8]_i_1_n_2\
    );
\ImagLoc_x_reg_2592[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAAAA9"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(9),
      I1 => \t_V_2_reg_577_reg__0\(8),
      I2 => \t_V_2_reg_577_reg__0\(7),
      I3 => \t_V_2_reg_577_reg__0\(6),
      I4 => \ImagLoc_x_reg_2592[10]_i_2_n_2\,
      O => \ImagLoc_x_reg_2592[9]_i_1_n_2\
    );
\ImagLoc_x_reg_2592_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \ImagLoc_x_reg_2592[10]_i_1_n_2\,
      Q => ImagLoc_x_reg_2592(10),
      R => '0'
    );
\ImagLoc_x_reg_2592_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \ImagLoc_x_reg_2592[1]_i_1_n_2\,
      Q => ImagLoc_x_reg_2592(1),
      R => '0'
    );
\ImagLoc_x_reg_2592_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \ImagLoc_x_reg_2592[2]_i_1_n_2\,
      Q => ImagLoc_x_reg_2592(2),
      R => '0'
    );
\ImagLoc_x_reg_2592_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \ImagLoc_x_reg_2592[3]_i_1_n_2\,
      Q => ImagLoc_x_reg_2592(3),
      R => '0'
    );
\ImagLoc_x_reg_2592_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \ImagLoc_x_reg_2592[4]_i_1_n_2\,
      Q => ImagLoc_x_reg_2592(4),
      R => '0'
    );
\ImagLoc_x_reg_2592_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \ImagLoc_x_reg_2592[5]_i_1_n_2\,
      Q => ImagLoc_x_reg_2592(5),
      R => '0'
    );
\ImagLoc_x_reg_2592_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \ImagLoc_x_reg_2592[6]_i_1_n_2\,
      Q => ImagLoc_x_reg_2592(6),
      R => '0'
    );
\ImagLoc_x_reg_2592_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \ImagLoc_x_reg_2592[7]_i_1_n_2\,
      Q => ImagLoc_x_reg_2592(7),
      R => '0'
    );
\ImagLoc_x_reg_2592_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \ImagLoc_x_reg_2592[8]_i_1_n_2\,
      Q => ImagLoc_x_reg_2592(8),
      R => '0'
    );
\ImagLoc_x_reg_2592_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \ImagLoc_x_reg_2592[9]_i_1_n_2\,
      Q => ImagLoc_x_reg_2592(9),
      R => '0'
    );
\SRL_SIG[0][0]_i_1__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => p_Val2_1_reg_2922(0),
      I1 => tmp_57_reg_2927(0),
      I2 => tmp_57_reg_2927(1),
      I3 => tmp_57_reg_2927(2),
      O => \SRL_SIG_reg[0][0]\
    );
\SRL_SIG[0][1]_i_1__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => p_Val2_1_reg_2922(1),
      I1 => tmp_57_reg_2927(0),
      I2 => tmp_57_reg_2927(1),
      I3 => tmp_57_reg_2927(2),
      O => \SRL_SIG_reg[0][1]\
    );
\SRL_SIG[0][2]_i_1__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => p_Val2_1_reg_2922(2),
      I1 => tmp_57_reg_2927(0),
      I2 => tmp_57_reg_2927(1),
      I3 => tmp_57_reg_2927(2),
      O => \SRL_SIG_reg[0][2]\
    );
\SRL_SIG[0][3]_i_1__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => p_Val2_1_reg_2922(3),
      I1 => tmp_57_reg_2927(0),
      I2 => tmp_57_reg_2927(1),
      I3 => tmp_57_reg_2927(2),
      O => \SRL_SIG_reg[0][3]\
    );
\SRL_SIG[0][4]_i_1__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => p_Val2_1_reg_2922(4),
      I1 => tmp_57_reg_2927(0),
      I2 => tmp_57_reg_2927(1),
      I3 => tmp_57_reg_2927(2),
      O => \SRL_SIG_reg[0][4]\
    );
\SRL_SIG[0][5]_i_1__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => p_Val2_1_reg_2922(5),
      I1 => tmp_57_reg_2927(0),
      I2 => tmp_57_reg_2927(1),
      I3 => tmp_57_reg_2927(2),
      O => \SRL_SIG_reg[0][5]\
    );
\SRL_SIG[0][6]_i_1__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => p_Val2_1_reg_2922(6),
      I1 => tmp_57_reg_2927(0),
      I2 => tmp_57_reg_2927(1),
      I3 => tmp_57_reg_2927(2),
      O => \SRL_SIG_reg[0][6]\
    );
\SRL_SIG[0][7]_i_1__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2220"
    )
        port map (
      I0 => \^shiftreg_ce\,
      I1 => tmp_57_reg_2927(2),
      I2 => tmp_57_reg_2927(1),
      I3 => tmp_57_reg_2927(0),
      O => \SRL_SIG_reg[0][7]\
    );
\SRL_SIG[0][7]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => ap_block_pp0_stage0_subdone2_in,
      I1 => ap_enable_reg_pp0_iter5_reg_n_2,
      I2 => ap_reg_pp0_iter4_or_cond_i_reg_2640,
      I3 => Q(1),
      O => \^shiftreg_ce\
    );
\SRL_SIG[0][7]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => p_Val2_1_reg_2922(7),
      I1 => tmp_57_reg_2927(0),
      I2 => tmp_57_reg_2927(1),
      I3 => tmp_57_reg_2927(2),
      O => \SRL_SIG_reg[0][7]_0\
    );
\ap_CS_fsm[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5757FF00"
    )
        port map (
      I0 => Sobel_U0_ap_start,
      I1 => start_for_CvtColor_1_U0_full_n,
      I2 => start_once_reg_reg_0,
      I3 => \^ap_cs_fsm_reg[0]_0\,
      I4 => Q(0),
      O => \ap_CS_fsm_reg[1]_0\(0)
    );
\ap_CS_fsm[0]_i_1__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \ap_CS_fsm[3]_i_2__0_n_2\,
      I1 => ap_CS_fsm_state3,
      I2 => ap_reg_grp_Filter2D_fu_96_ap_start,
      I3 => \ap_CS_fsm_reg_n_2_[0]\,
      O => ap_NS_fsm(0)
    );
\ap_CS_fsm[1]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF8F8888"
    )
        port map (
      I0 => ap_reg_grp_Filter2D_fu_96_ap_start,
      I1 => \ap_CS_fsm_reg_n_2_[0]\,
      I2 => tmp_5_reg_555(1),
      I3 => tmp_5_reg_555(0),
      I4 => ap_CS_fsm_state2,
      O => ap_NS_fsm(1)
    );
\ap_CS_fsm[1]_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBB11111"
    )
        port map (
      I0 => Q(0),
      I1 => \^ap_cs_fsm_reg[0]_0\,
      I2 => start_once_reg_reg_0,
      I3 => start_for_CvtColor_1_U0_full_n,
      I4 => Sobel_U0_ap_start,
      O => \ap_CS_fsm_reg[1]_0\(1)
    );
\ap_CS_fsm[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0808AA08"
    )
        port map (
      I0 => Q(1),
      I1 => \ap_CS_fsm_reg_n_2_[0]\,
      I2 => ap_reg_grp_Filter2D_fu_96_ap_start,
      I3 => ap_CS_fsm_state3,
      I4 => \ap_CS_fsm[3]_i_2__0_n_2\,
      O => \^ap_cs_fsm_reg[0]_0\
    );
\ap_CS_fsm[2]_i_1__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AEAA"
    )
        port map (
      I0 => ap_CS_fsm_state11,
      I1 => tmp_5_reg_555(1),
      I2 => tmp_5_reg_555(0),
      I3 => ap_CS_fsm_state2,
      O => ap_NS_fsm(2)
    );
\ap_CS_fsm[3]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ap_CS_fsm_state3,
      I1 => \ap_CS_fsm[3]_i_2__0_n_2\,
      O => ap_NS_fsm(3)
    );
\ap_CS_fsm[3]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFFFFFFFFFF"
    )
        port map (
      I0 => \ap_CS_fsm[3]_i_3_n_2\,
      I1 => t_V_reg_566(9),
      I2 => t_V_reg_566(4),
      I3 => t_V_reg_566(1),
      I4 => t_V_reg_566(8),
      I5 => \tmp_4_reg_2535[6]_i_2_n_2\,
      O => \ap_CS_fsm[3]_i_2__0_n_2\
    );
\ap_CS_fsm[3]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFF7"
    )
        port map (
      I0 => t_V_reg_566(7),
      I1 => t_V_reg_566(6),
      I2 => t_V_reg_566(0),
      I3 => t_V_reg_566(5),
      O => \ap_CS_fsm[3]_i_3_n_2\
    );
\ap_CS_fsm[4]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => \ap_CS_fsm[5]_i_2__0_n_2\,
      I2 => ap_CS_fsm_pp0_stage0,
      O => ap_NS_fsm(4)
    );
\ap_CS_fsm[5]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ap_CS_fsm_pp0_stage0,
      I1 => \ap_CS_fsm[5]_i_2__0_n_2\,
      O => ap_NS_fsm(5)
    );
\ap_CS_fsm[5]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB00FBFBFFFFFFFF"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter1,
      I1 => ap_enable_reg_pp0_iter2,
      I2 => ap_enable_reg_pp0_iter3,
      I3 => ap_enable_reg_pp0_iter4,
      I4 => ap_enable_reg_pp0_iter5_reg_n_2,
      I5 => ap_block_pp0_stage0_subdone2_in,
      O => \ap_CS_fsm[5]_i_2__0_n_2\
    );
\ap_CS_fsm_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(0),
      Q => \ap_CS_fsm_reg_n_2_[0]\,
      S => ap_rst_n_inv
    );
\ap_CS_fsm_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(1),
      Q => ap_CS_fsm_state2,
      R => ap_rst_n_inv
    );
\ap_CS_fsm_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(2),
      Q => ap_CS_fsm_state3,
      R => ap_rst_n_inv
    );
\ap_CS_fsm_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(3),
      Q => ap_CS_fsm_state4,
      R => ap_rst_n_inv
    );
\ap_CS_fsm_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(4),
      Q => ap_CS_fsm_pp0_stage0,
      R => ap_rst_n_inv
    );
\ap_CS_fsm_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(5),
      Q => ap_CS_fsm_state11,
      R => ap_rst_n_inv
    );
\ap_enable_reg_pp0_iter0_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F007F007F000000"
    )
        port map (
      I0 => exitcond389_i_fu_936_p2,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => ap_block_pp0_stage0_subdone2_in,
      I3 => ap_rst_n,
      I4 => ap_CS_fsm_state4,
      I5 => ap_enable_reg_pp0_iter0,
      O => \ap_enable_reg_pp0_iter0_i_1__0_n_2\
    );
ap_enable_reg_pp0_iter0_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \ap_enable_reg_pp0_iter0_i_1__0_n_2\,
      Q => ap_enable_reg_pp0_iter0,
      R => '0'
    );
ap_enable_reg_pp0_iter1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => ap_enable_reg_pp0_iter0,
      Q => ap_enable_reg_pp0_iter1,
      R => ap_rst_n_inv
    );
ap_enable_reg_pp0_iter2_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => ap_enable_reg_pp0_iter1,
      Q => ap_enable_reg_pp0_iter2,
      R => ap_rst_n_inv
    );
ap_enable_reg_pp0_iter3_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter2,
      I1 => ap_enable_reg_pp0_iter1,
      O => ap_enable_reg_pp0_iter3_i_1_n_2
    );
ap_enable_reg_pp0_iter3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => ap_enable_reg_pp0_iter3_i_1_n_2,
      Q => ap_enable_reg_pp0_iter3,
      R => ap_rst_n_inv
    );
ap_enable_reg_pp0_iter4_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => ap_enable_reg_pp0_iter3,
      Q => ap_enable_reg_pp0_iter4,
      R => ap_rst_n_inv
    );
\ap_enable_reg_pp0_iter5_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C044C000"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => ap_rst_n,
      I2 => ap_enable_reg_pp0_iter4,
      I3 => ap_block_pp0_stage0_subdone2_in,
      I4 => ap_enable_reg_pp0_iter5_reg_n_2,
      O => \ap_enable_reg_pp0_iter5_i_1__0_n_2\
    );
ap_enable_reg_pp0_iter5_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \ap_enable_reg_pp0_iter5_i_1__0_n_2\,
      Q => ap_enable_reg_pp0_iter5_reg_n_2,
      R => '0'
    );
ap_reg_grp_Filter2D_fu_96_ap_start_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D5D5D555C0C0C000"
    )
        port map (
      I0 => ap_reg_grp_Filter2D_fu_96_ap_start_i_2_n_2,
      I1 => Q(0),
      I2 => Sobel_U0_ap_start,
      I3 => start_for_CvtColor_1_U0_full_n,
      I4 => start_once_reg_reg_0,
      I5 => ap_reg_grp_Filter2D_fu_96_ap_start,
      O => ap_reg_grp_Filter2D_fu_96_ap_start_reg
    );
ap_reg_grp_Filter2D_fu_96_ap_start_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ap_CS_fsm_state3,
      I1 => \ap_CS_fsm[3]_i_2__0_n_2\,
      O => ap_reg_grp_Filter2D_fu_96_ap_start_i_2_n_2
    );
\ap_reg_pp0_iter1_brmerge_reg_2627_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_reg_pp0_iter1_brmerge_reg_26270,
      D => brmerge_reg_2627,
      Q => ap_reg_pp0_iter1_brmerge_reg_2627,
      R => '0'
    );
\ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_reg_pp0_iter1_brmerge_reg_26270,
      D => \exitcond389_i_reg_2583_reg_n_2_[0]\,
      Q => \ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg_n_2_[0]\,
      R => '0'
    );
\ap_reg_pp0_iter1_or_cond_i_i_reg_2607_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_reg_pp0_iter1_brmerge_reg_26270,
      D => or_cond_i_i_reg_2607,
      Q => ap_reg_pp0_iter1_or_cond_i_i_reg_2607,
      R => '0'
    );
\ap_reg_pp0_iter1_or_cond_i_reg_2640_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_reg_pp0_iter1_brmerge_reg_26270,
      D => or_cond_i_reg_2640,
      Q => ap_reg_pp0_iter1_or_cond_i_reg_2640,
      R => '0'
    );
\ap_reg_pp0_iter2_exitcond389_i_reg_2583_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => \ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg_n_2_[0]\,
      Q => ap_reg_pp0_iter2_exitcond389_i_reg_2583,
      R => '0'
    );
\ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => tmp_49_reg_2644(0),
      Q => ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697(0),
      R => '0'
    );
\ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => k_buf_2_val_5_addr_reg_2697(10),
      Q => ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697(10),
      R => '0'
    );
\ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => tmp_49_reg_2644(1),
      Q => ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697(1),
      R => '0'
    );
\ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => k_buf_2_val_5_addr_reg_2697(2),
      Q => ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697(2),
      R => '0'
    );
\ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => k_buf_2_val_5_addr_reg_2697(3),
      Q => ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697(3),
      R => '0'
    );
\ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => k_buf_2_val_5_addr_reg_2697(4),
      Q => ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697(4),
      R => '0'
    );
\ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => k_buf_2_val_5_addr_reg_2697(5),
      Q => ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697(5),
      R => '0'
    );
\ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => k_buf_2_val_5_addr_reg_2697(6),
      Q => ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697(6),
      R => '0'
    );
\ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => k_buf_2_val_5_addr_reg_2697(7),
      Q => ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697(7),
      R => '0'
    );
\ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => k_buf_2_val_5_addr_reg_2697(8),
      Q => ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697(8),
      R => '0'
    );
\ap_reg_pp0_iter2_k_buf_0_val_4_addr_reg_2655_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => k_buf_2_val_5_addr_reg_2697(9),
      Q => ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697(9),
      R => '0'
    );
\ap_reg_pp0_iter2_or_cond_i_i_reg_2607_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => ap_reg_pp0_iter1_or_cond_i_i_reg_2607,
      Q => ap_reg_pp0_iter2_or_cond_i_i_reg_2607,
      R => '0'
    );
\ap_reg_pp0_iter2_or_cond_i_reg_2640_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => ap_reg_pp0_iter1_or_cond_i_reg_2640,
      Q => ap_reg_pp0_iter2_or_cond_i_reg_2640,
      R => '0'
    );
\ap_reg_pp0_iter2_reg_588_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => reg_588(0),
      Q => ap_reg_pp0_iter2_reg_588(0),
      R => '0'
    );
\ap_reg_pp0_iter2_reg_588_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => reg_588(1),
      Q => ap_reg_pp0_iter2_reg_588(1),
      R => '0'
    );
\ap_reg_pp0_iter2_reg_588_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => reg_588(2),
      Q => ap_reg_pp0_iter2_reg_588(2),
      R => '0'
    );
\ap_reg_pp0_iter2_reg_588_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => reg_588(3),
      Q => ap_reg_pp0_iter2_reg_588(3),
      R => '0'
    );
\ap_reg_pp0_iter2_reg_588_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => reg_588(4),
      Q => ap_reg_pp0_iter2_reg_588(4),
      R => '0'
    );
\ap_reg_pp0_iter2_reg_588_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => reg_588(5),
      Q => ap_reg_pp0_iter2_reg_588(5),
      R => '0'
    );
\ap_reg_pp0_iter2_reg_588_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => reg_588(6),
      Q => ap_reg_pp0_iter2_reg_588(6),
      R => '0'
    );
\ap_reg_pp0_iter2_reg_588_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => reg_588(7),
      Q => ap_reg_pp0_iter2_reg_588(7),
      R => '0'
    );
\ap_reg_pp0_iter3_or_cond_i_reg_2640_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => ap_reg_pp0_iter2_or_cond_i_reg_2640,
      Q => ap_reg_pp0_iter3_or_cond_i_reg_2640,
      R => '0'
    );
\ap_reg_pp0_iter4_or_cond_i_reg_2640_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => ap_reg_pp0_iter3_or_cond_i_reg_2640,
      Q => ap_reg_pp0_iter4_or_cond_i_reg_2640,
      R => '0'
    );
\brmerge_reg_2627[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => tmp_31_fu_984_p2,
      I1 => tmp_72_not_reg_2504,
      O => brmerge_fu_1034_p2
    );
\brmerge_reg_2627_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => brmerge_fu_1034_p2,
      Q => brmerge_reg_2627,
      R => '0'
    );
\col_buf_0_val_0_0_reg_2708_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_0_0_fu_1140_p3(0),
      Q => col_buf_0_val_0_0_reg_2708(0),
      R => '0'
    );
\col_buf_0_val_0_0_reg_2708_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_0_0_fu_1140_p3(1),
      Q => col_buf_0_val_0_0_reg_2708(1),
      R => '0'
    );
\col_buf_0_val_0_0_reg_2708_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_0_0_fu_1140_p3(2),
      Q => col_buf_0_val_0_0_reg_2708(2),
      R => '0'
    );
\col_buf_0_val_0_0_reg_2708_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_0_0_fu_1140_p3(3),
      Q => col_buf_0_val_0_0_reg_2708(3),
      R => '0'
    );
\col_buf_0_val_0_0_reg_2708_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_0_0_fu_1140_p3(4),
      Q => col_buf_0_val_0_0_reg_2708(4),
      R => '0'
    );
\col_buf_0_val_0_0_reg_2708_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_0_0_fu_1140_p3(5),
      Q => col_buf_0_val_0_0_reg_2708(5),
      R => '0'
    );
\col_buf_0_val_0_0_reg_2708_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_0_0_fu_1140_p3(6),
      Q => col_buf_0_val_0_0_reg_2708(6),
      R => '0'
    );
\col_buf_0_val_0_0_reg_2708_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_0_0_fu_1140_p3(7),
      Q => col_buf_0_val_0_0_reg_2708(7),
      R => '0'
    );
\col_buf_0_val_1_0_reg_2721_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_1_0_fu_1159_p3(0),
      Q => col_buf_0_val_1_0_reg_2721(0),
      R => '0'
    );
\col_buf_0_val_1_0_reg_2721_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_1_0_fu_1159_p3(1),
      Q => col_buf_0_val_1_0_reg_2721(1),
      R => '0'
    );
\col_buf_0_val_1_0_reg_2721_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_1_0_fu_1159_p3(2),
      Q => col_buf_0_val_1_0_reg_2721(2),
      R => '0'
    );
\col_buf_0_val_1_0_reg_2721_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_1_0_fu_1159_p3(3),
      Q => col_buf_0_val_1_0_reg_2721(3),
      R => '0'
    );
\col_buf_0_val_1_0_reg_2721_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_1_0_fu_1159_p3(4),
      Q => col_buf_0_val_1_0_reg_2721(4),
      R => '0'
    );
\col_buf_0_val_1_0_reg_2721_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_1_0_fu_1159_p3(5),
      Q => col_buf_0_val_1_0_reg_2721(5),
      R => '0'
    );
\col_buf_0_val_1_0_reg_2721_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_1_0_fu_1159_p3(6),
      Q => col_buf_0_val_1_0_reg_2721(6),
      R => '0'
    );
\col_buf_0_val_1_0_reg_2721_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_1_0_fu_1159_p3(7),
      Q => col_buf_0_val_1_0_reg_2721(7),
      R => '0'
    );
\col_buf_0_val_2_0_reg_2729_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_2_0_fu_1178_p3(0),
      Q => col_buf_0_val_2_0_reg_2729(0),
      R => '0'
    );
\col_buf_0_val_2_0_reg_2729_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_2_0_fu_1178_p3(1),
      Q => col_buf_0_val_2_0_reg_2729(1),
      R => '0'
    );
\col_buf_0_val_2_0_reg_2729_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_2_0_fu_1178_p3(2),
      Q => col_buf_0_val_2_0_reg_2729(2),
      R => '0'
    );
\col_buf_0_val_2_0_reg_2729_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_2_0_fu_1178_p3(3),
      Q => col_buf_0_val_2_0_reg_2729(3),
      R => '0'
    );
\col_buf_0_val_2_0_reg_2729_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_2_0_fu_1178_p3(4),
      Q => col_buf_0_val_2_0_reg_2729(4),
      R => '0'
    );
\col_buf_0_val_2_0_reg_2729_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_2_0_fu_1178_p3(5),
      Q => col_buf_0_val_2_0_reg_2729(5),
      R => '0'
    );
\col_buf_0_val_2_0_reg_2729_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_2_0_fu_1178_p3(6),
      Q => col_buf_0_val_2_0_reg_2729(6),
      R => '0'
    );
\col_buf_0_val_2_0_reg_2729_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => col_buf_0_val_0_0_reg_27080,
      D => col_buf_0_val_2_0_fu_1178_p3(7),
      Q => col_buf_0_val_2_0_reg_2729(7),
      R => '0'
    );
\exitcond389_i_reg_2583[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ap_CS_fsm_pp0_stage0,
      I1 => ap_block_pp0_stage0_subdone2_in,
      O => ap_reg_pp0_iter1_brmerge_reg_26270
    );
\exitcond389_i_reg_2583[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000100000"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0__0\(0),
      I1 => \t_V_2_reg_577_reg__0\(9),
      I2 => \t_V_2_reg_577_reg__0\(8),
      I3 => \exitcond389_i_reg_2583[0]_i_3_n_2\,
      I4 => \exitcond389_i_reg_2583[0]_i_4_n_2\,
      I5 => \exitcond389_i_reg_2583[0]_i_5_n_2\,
      O => exitcond389_i_fu_936_p2
    );
\exitcond389_i_reg_2583[0]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(3),
      I1 => \t_V_2_reg_577_reg__0\(4),
      O => \exitcond389_i_reg_2583[0]_i_3_n_2\
    );
\exitcond389_i_reg_2583[0]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(6),
      I1 => \t_V_2_reg_577_reg__0\(7),
      O => \exitcond389_i_reg_2583[0]_i_4_n_2\
    );
\exitcond389_i_reg_2583[0]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFDF"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(10),
      I1 => \t_V_2_reg_577_reg__0\(5),
      I2 => \t_V_2_reg_577_reg__0\(1),
      I3 => \t_V_2_reg_577_reg__0\(2),
      O => \exitcond389_i_reg_2583[0]_i_5_n_2\
    );
\exitcond389_i_reg_2583_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_reg_pp0_iter1_brmerge_reg_26270,
      D => exitcond389_i_fu_936_p2,
      Q => \exitcond389_i_reg_2583_reg_n_2_[0]\,
      R => '0'
    );
\i_V_reg_2495[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => t_V_reg_566(0),
      O => i_V_fu_631_p2(0)
    );
\i_V_reg_2495[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => t_V_reg_566(1),
      I1 => t_V_reg_566(0),
      O => i_V_fu_631_p2(1)
    );
\i_V_reg_2495[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => t_V_reg_566(2),
      I1 => t_V_reg_566(0),
      I2 => t_V_reg_566(1),
      O => i_V_fu_631_p2(2)
    );
\i_V_reg_2495[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => t_V_reg_566(3),
      I1 => t_V_reg_566(1),
      I2 => t_V_reg_566(0),
      I3 => t_V_reg_566(2),
      O => i_V_fu_631_p2(3)
    );
\i_V_reg_2495[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => t_V_reg_566(4),
      I1 => t_V_reg_566(2),
      I2 => t_V_reg_566(0),
      I3 => t_V_reg_566(1),
      I4 => t_V_reg_566(3),
      O => i_V_fu_631_p2(4)
    );
\i_V_reg_2495[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => t_V_reg_566(5),
      I1 => t_V_reg_566(4),
      I2 => t_V_reg_566(3),
      I3 => t_V_reg_566(1),
      I4 => t_V_reg_566(0),
      I5 => t_V_reg_566(2),
      O => i_V_fu_631_p2(5)
    );
\i_V_reg_2495[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => t_V_reg_566(6),
      I1 => \i_V_reg_2495[9]_i_2_n_2\,
      O => i_V_fu_631_p2(6)
    );
\i_V_reg_2495[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => t_V_reg_566(7),
      I1 => t_V_reg_566(6),
      I2 => \i_V_reg_2495[9]_i_2_n_2\,
      O => i_V_fu_631_p2(7)
    );
\i_V_reg_2495[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => t_V_reg_566(8),
      I1 => \i_V_reg_2495[9]_i_2_n_2\,
      I2 => t_V_reg_566(6),
      I3 => t_V_reg_566(7),
      O => i_V_fu_631_p2(8)
    );
\i_V_reg_2495[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => t_V_reg_566(9),
      I1 => t_V_reg_566(8),
      I2 => t_V_reg_566(7),
      I3 => t_V_reg_566(6),
      I4 => \i_V_reg_2495[9]_i_2_n_2\,
      O => i_V_fu_631_p2(9)
    );
\i_V_reg_2495[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => t_V_reg_566(4),
      I1 => t_V_reg_566(3),
      I2 => t_V_reg_566(1),
      I3 => t_V_reg_566(0),
      I4 => t_V_reg_566(2),
      I5 => t_V_reg_566(5),
      O => \i_V_reg_2495[9]_i_2_n_2\
    );
\i_V_reg_2495_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state3,
      D => i_V_fu_631_p2(0),
      Q => i_V_reg_2495(0),
      R => '0'
    );
\i_V_reg_2495_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state3,
      D => i_V_fu_631_p2(1),
      Q => i_V_reg_2495(1),
      R => '0'
    );
\i_V_reg_2495_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state3,
      D => i_V_fu_631_p2(2),
      Q => i_V_reg_2495(2),
      R => '0'
    );
\i_V_reg_2495_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state3,
      D => i_V_fu_631_p2(3),
      Q => i_V_reg_2495(3),
      R => '0'
    );
\i_V_reg_2495_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state3,
      D => i_V_fu_631_p2(4),
      Q => i_V_reg_2495(4),
      R => '0'
    );
\i_V_reg_2495_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state3,
      D => i_V_fu_631_p2(5),
      Q => i_V_reg_2495(5),
      R => '0'
    );
\i_V_reg_2495_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state3,
      D => i_V_fu_631_p2(6),
      Q => i_V_reg_2495(6),
      R => '0'
    );
\i_V_reg_2495_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state3,
      D => i_V_fu_631_p2(7),
      Q => i_V_reg_2495(7),
      R => '0'
    );
\i_V_reg_2495_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state3,
      D => i_V_fu_631_p2(8),
      Q => i_V_reg_2495(8),
      R => '0'
    );
\i_V_reg_2495_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state3,
      D => i_V_fu_631_p2(9),
      Q => i_V_reg_2495(9),
      R => '0'
    );
\icmp_reg_2509[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => t_V_reg_566(1),
      I1 => \icmp_reg_2509[0]_i_2_n_2\,
      O => \icmp_reg_2509[0]_i_1_n_2\
    );
\icmp_reg_2509[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \icmp_reg_2509[0]_i_3_n_2\,
      I1 => t_V_reg_566(2),
      I2 => t_V_reg_566(3),
      I3 => t_V_reg_566(7),
      I4 => t_V_reg_566(9),
      I5 => t_V_reg_566(8),
      O => \icmp_reg_2509[0]_i_2_n_2\
    );
\icmp_reg_2509[0]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => t_V_reg_566(6),
      I1 => t_V_reg_566(4),
      I2 => t_V_reg_566(5),
      O => \icmp_reg_2509[0]_i_3_n_2\
    );
\icmp_reg_2509_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \icmp_reg_2509[0]_i_1_n_2\,
      Q => \icmp_reg_2509_reg_n_2_[0]\,
      R => '0'
    );
k_buf_0_val_3_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg
     port map (
      ADDRBWRADDR(10) => k_buf_0_val_5_U_n_10,
      ADDRBWRADDR(9) => k_buf_0_val_5_U_n_11,
      ADDRBWRADDR(8) => k_buf_0_val_5_U_n_12,
      ADDRBWRADDR(7) => k_buf_0_val_5_U_n_13,
      ADDRBWRADDR(6) => k_buf_0_val_5_U_n_14,
      ADDRBWRADDR(5) => k_buf_0_val_5_U_n_15,
      ADDRBWRADDR(4) => k_buf_0_val_5_U_n_16,
      ADDRBWRADDR(3) => k_buf_0_val_5_U_n_17,
      ADDRBWRADDR(2) => k_buf_0_val_5_U_n_18,
      ADDRBWRADDR(1) => k_buf_0_val_5_U_n_19,
      ADDRBWRADDR(0) => p_assign_2_reg_2622(0),
      D(7 downto 0) => k_buf_0_val_3_q0(7 downto 0),
      Q(7 downto 0) => reg_588(7 downto 0),
      \ap_CS_fsm_reg[4]\(0) => ap_CS_fsm_pp0_stage0,
      ap_block_pp0_stage0_subdone2_in => ap_block_pp0_stage0_subdone2_in,
      ap_clk => ap_clk,
      ap_enable_reg_pp0_iter1 => ap_enable_reg_pp0_iter1,
      ap_enable_reg_pp0_iter2 => ap_enable_reg_pp0_iter2,
      ap_enable_reg_pp0_iter5_reg => ap_enable_reg_pp0_iter5_reg_n_2,
      \ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg[0]\ => \ap_reg_pp0_iter1_exitcond389_i_reg_2583_reg_n_2_[0]\,
      ap_reg_pp0_iter1_or_cond_i_i_reg_2607 => ap_reg_pp0_iter1_or_cond_i_i_reg_2607,
      \ap_reg_pp0_iter2_reg_588_reg[0]\ => k_buf_0_val_3_U_n_13,
      ap_reg_pp0_iter4_or_cond_i_reg_2640 => ap_reg_pp0_iter4_or_cond_i_reg_2640,
      col_buf_0_val_0_0_reg_27080 => col_buf_0_val_0_0_reg_27080,
      \exitcond389_i_reg_2583_reg[0]\ => \exitcond389_i_reg_2583_reg_n_2_[0]\,
      \icmp_reg_2509_reg[0]\ => \icmp_reg_2509_reg_n_2_[0]\,
      img1_data_stream_0_s_empty_n => img1_data_stream_0_s_empty_n,
      img1_data_stream_1_s_empty_n => img1_data_stream_1_s_empty_n,
      img1_data_stream_2_s_empty_n => img1_data_stream_2_s_empty_n,
      img2_data_stream_0_s_full_n => img2_data_stream_0_s_full_n,
      img2_data_stream_1_s_full_n => img2_data_stream_1_s_full_n,
      img2_data_stream_2_s_full_n => img2_data_stream_2_s_full_n,
      \k_buf_0_val_3_addr_reg_2649_reg[10]\(10 downto 2) => k_buf_2_val_5_addr_reg_2697(10 downto 2),
      \k_buf_0_val_3_addr_reg_2649_reg[10]\(1 downto 0) => tmp_49_reg_2644(1 downto 0),
      or_cond_i_i_reg_2607 => or_cond_i_i_reg_2607,
      ram_reg => k_buf_0_val_3_U_n_10,
      tmp_1_reg_2500 => tmp_1_reg_2500,
      \tmp_2_reg_2514_reg[0]\ => \tmp_2_reg_2514_reg_n_2_[0]\
    );
\k_buf_0_val_3_addr_reg_2649[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => ap_block_pp0_stage0_subdone2_in,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => \exitcond389_i_reg_2583_reg_n_2_[0]\,
      O => k_buf_0_val_3_addr_reg_26490
    );
\k_buf_0_val_3_addr_reg_2649_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_addr_reg_26490,
      D => k_buf_0_val_5_U_n_10,
      Q => k_buf_2_val_5_addr_reg_2697(10),
      R => '0'
    );
\k_buf_0_val_3_addr_reg_2649_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_addr_reg_26490,
      D => k_buf_0_val_5_U_n_18,
      Q => k_buf_2_val_5_addr_reg_2697(2),
      R => '0'
    );
\k_buf_0_val_3_addr_reg_2649_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_addr_reg_26490,
      D => k_buf_0_val_5_U_n_17,
      Q => k_buf_2_val_5_addr_reg_2697(3),
      R => '0'
    );
\k_buf_0_val_3_addr_reg_2649_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_addr_reg_26490,
      D => k_buf_0_val_5_U_n_16,
      Q => k_buf_2_val_5_addr_reg_2697(4),
      R => '0'
    );
\k_buf_0_val_3_addr_reg_2649_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_addr_reg_26490,
      D => k_buf_0_val_5_U_n_15,
      Q => k_buf_2_val_5_addr_reg_2697(5),
      R => '0'
    );
\k_buf_0_val_3_addr_reg_2649_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_addr_reg_26490,
      D => k_buf_0_val_5_U_n_14,
      Q => k_buf_2_val_5_addr_reg_2697(6),
      R => '0'
    );
\k_buf_0_val_3_addr_reg_2649_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_addr_reg_26490,
      D => k_buf_0_val_5_U_n_13,
      Q => k_buf_2_val_5_addr_reg_2697(7),
      R => '0'
    );
\k_buf_0_val_3_addr_reg_2649_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_addr_reg_26490,
      D => k_buf_0_val_5_U_n_12,
      Q => k_buf_2_val_5_addr_reg_2697(8),
      R => '0'
    );
\k_buf_0_val_3_addr_reg_2649_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_addr_reg_26490,
      D => k_buf_0_val_5_U_n_11,
      Q => k_buf_2_val_5_addr_reg_2697(9),
      R => '0'
    );
\k_buf_0_val_3_load_reg_2703_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_load_reg_27030,
      D => k_buf_0_val_3_q0(0),
      Q => k_buf_0_val_3_load_reg_2703(0),
      R => '0'
    );
\k_buf_0_val_3_load_reg_2703_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_load_reg_27030,
      D => k_buf_0_val_3_q0(1),
      Q => k_buf_0_val_3_load_reg_2703(1),
      R => '0'
    );
\k_buf_0_val_3_load_reg_2703_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_load_reg_27030,
      D => k_buf_0_val_3_q0(2),
      Q => k_buf_0_val_3_load_reg_2703(2),
      R => '0'
    );
\k_buf_0_val_3_load_reg_2703_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_load_reg_27030,
      D => k_buf_0_val_3_q0(3),
      Q => k_buf_0_val_3_load_reg_2703(3),
      R => '0'
    );
\k_buf_0_val_3_load_reg_2703_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_load_reg_27030,
      D => k_buf_0_val_3_q0(4),
      Q => k_buf_0_val_3_load_reg_2703(4),
      R => '0'
    );
\k_buf_0_val_3_load_reg_2703_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_load_reg_27030,
      D => k_buf_0_val_3_q0(5),
      Q => k_buf_0_val_3_load_reg_2703(5),
      R => '0'
    );
\k_buf_0_val_3_load_reg_2703_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_load_reg_27030,
      D => k_buf_0_val_3_q0(6),
      Q => k_buf_0_val_3_load_reg_2703(6),
      R => '0'
    );
\k_buf_0_val_3_load_reg_2703_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_load_reg_27030,
      D => k_buf_0_val_3_q0(7),
      Q => k_buf_0_val_3_load_reg_2703(7),
      R => '0'
    );
k_buf_0_val_4_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_16
     port map (
      ADDRBWRADDR(10) => k_buf_0_val_5_U_n_10,
      ADDRBWRADDR(9) => k_buf_0_val_5_U_n_11,
      ADDRBWRADDR(8) => k_buf_0_val_5_U_n_12,
      ADDRBWRADDR(7) => k_buf_0_val_5_U_n_13,
      ADDRBWRADDR(6) => k_buf_0_val_5_U_n_14,
      ADDRBWRADDR(5) => k_buf_0_val_5_U_n_15,
      ADDRBWRADDR(4) => k_buf_0_val_5_U_n_16,
      ADDRBWRADDR(3) => k_buf_0_val_5_U_n_17,
      ADDRBWRADDR(2) => k_buf_0_val_5_U_n_18,
      ADDRBWRADDR(1) => k_buf_0_val_5_U_n_19,
      ADDRBWRADDR(0) => p_assign_2_reg_2622(0),
      D(7 downto 0) => k_buf_0_val_4_q0(7 downto 0),
      Q(10 downto 0) => ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697(10 downto 0),
      \ap_CS_fsm_reg[4]\ => k_buf_0_val_3_U_n_10,
      ap_block_pp0_stage0_subdone2_in => ap_block_pp0_stage0_subdone2_in,
      ap_clk => ap_clk,
      ap_enable_reg_pp0_iter3 => ap_enable_reg_pp0_iter3,
      ap_reg_pp0_iter2_or_cond_i_i_reg_2607 => ap_reg_pp0_iter2_or_cond_i_i_reg_2607,
      \ap_reg_pp0_iter2_reg_588_reg[7]\(7 downto 0) => ap_reg_pp0_iter2_reg_588(7 downto 0),
      \icmp_reg_2509_reg[0]\ => \icmp_reg_2509_reg_n_2_[0]\,
      \k_buf_0_val_3_load_reg_2703_reg[7]\(7 downto 0) => k_buf_0_val_3_load_reg_2703(7 downto 0),
      \tmp_116_0_1_reg_2518_reg[0]\ => \tmp_116_0_1_reg_2518_reg_n_2_[0]\,
      tmp_1_reg_2500 => tmp_1_reg_2500
    );
\k_buf_0_val_4_load_reg_2716[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter2,
      I1 => col_buf_0_val_0_0_reg_27080,
      O => k_buf_0_val_3_load_reg_27030
    );
\k_buf_0_val_4_load_reg_2716_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_load_reg_27030,
      D => k_buf_0_val_4_q0(0),
      Q => k_buf_0_val_4_load_reg_2716(0),
      R => '0'
    );
\k_buf_0_val_4_load_reg_2716_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_load_reg_27030,
      D => k_buf_0_val_4_q0(1),
      Q => k_buf_0_val_4_load_reg_2716(1),
      R => '0'
    );
\k_buf_0_val_4_load_reg_2716_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_load_reg_27030,
      D => k_buf_0_val_4_q0(2),
      Q => k_buf_0_val_4_load_reg_2716(2),
      R => '0'
    );
\k_buf_0_val_4_load_reg_2716_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_load_reg_27030,
      D => k_buf_0_val_4_q0(3),
      Q => k_buf_0_val_4_load_reg_2716(3),
      R => '0'
    );
\k_buf_0_val_4_load_reg_2716_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_load_reg_27030,
      D => k_buf_0_val_4_q0(4),
      Q => k_buf_0_val_4_load_reg_2716(4),
      R => '0'
    );
\k_buf_0_val_4_load_reg_2716_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_load_reg_27030,
      D => k_buf_0_val_4_q0(5),
      Q => k_buf_0_val_4_load_reg_2716(5),
      R => '0'
    );
\k_buf_0_val_4_load_reg_2716_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_load_reg_27030,
      D => k_buf_0_val_4_q0(6),
      Q => k_buf_0_val_4_load_reg_2716(6),
      R => '0'
    );
\k_buf_0_val_4_load_reg_2716_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_load_reg_27030,
      D => k_buf_0_val_4_q0(7),
      Q => k_buf_0_val_4_load_reg_2716(7),
      R => '0'
    );
k_buf_0_val_5_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D_k_buf_0_eOg_17
     port map (
      ADDRBWRADDR(9) => k_buf_0_val_5_U_n_10,
      ADDRBWRADDR(8) => k_buf_0_val_5_U_n_11,
      ADDRBWRADDR(7) => k_buf_0_val_5_U_n_12,
      ADDRBWRADDR(6) => k_buf_0_val_5_U_n_13,
      ADDRBWRADDR(5) => k_buf_0_val_5_U_n_14,
      ADDRBWRADDR(4) => k_buf_0_val_5_U_n_15,
      ADDRBWRADDR(3) => k_buf_0_val_5_U_n_16,
      ADDRBWRADDR(2) => k_buf_0_val_5_U_n_17,
      ADDRBWRADDR(1) => k_buf_0_val_5_U_n_18,
      ADDRBWRADDR(0) => k_buf_0_val_5_U_n_19,
      DOBDO(7 downto 0) => k_buf_0_val_5_q0(7 downto 0),
      \ImagLoc_x_reg_2592_reg[10]\(9 downto 0) => ImagLoc_x_reg_2592(10 downto 1),
      Q(10 downto 0) => ap_reg_pp0_iter2_k_buf_2_val_5_addr_reg_2697(10 downto 0),
      \ap_CS_fsm_reg[4]\ => k_buf_0_val_3_U_n_10,
      ap_block_pp0_stage0_subdone2_in => ap_block_pp0_stage0_subdone2_in,
      ap_clk => ap_clk,
      ap_enable_reg_pp0_iter3 => ap_enable_reg_pp0_iter3,
      ap_reg_pp0_iter2_or_cond_i_i_reg_2607 => ap_reg_pp0_iter2_or_cond_i_i_reg_2607,
      \ap_reg_pp0_iter2_reg_588_reg[7]\(7 downto 0) => ap_reg_pp0_iter2_reg_588(7 downto 0),
      \icmp_reg_2509_reg[0]\ => \icmp_reg_2509_reg_n_2_[0]\,
      \k_buf_0_val_4_load_reg_2716_reg[7]\(7 downto 0) => k_buf_0_val_4_load_reg_2716(7 downto 0),
      or_cond_i_i_reg_2607 => or_cond_i_i_reg_2607,
      p_assign_2_reg_2622(10 downto 0) => p_assign_2_reg_2622(10 downto 0),
      \p_p2_i_i_cast_cast_reg_2612_reg[10]\(9 downto 0) => p_p2_i_i_cast_cast_reg_2612_reg(10 downto 1),
      tmp_1_reg_2500 => tmp_1_reg_2500,
      \tmp_2_reg_2514_reg[0]\ => \tmp_2_reg_2514_reg_n_2_[0]\,
      tmp_31_reg_2602 => tmp_31_reg_2602,
      tmp_33_reg_2617 => tmp_33_reg_2617,
      tmp_47_reg_2597 => tmp_47_reg_2597
    );
\mOutPtr[1]_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"40BFBF40"
    )
        port map (
      I0 => shiftReg_ce_0,
      I1 => Q(1),
      I2 => \^e\(0),
      I3 => \mOutPtr_reg[1]_1\(0),
      I4 => \mOutPtr_reg[1]_1\(1),
      O => D(0)
    );
\mOutPtr[1]_i_1__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"40BFBF40"
    )
        port map (
      I0 => shiftReg_ce_0,
      I1 => Q(1),
      I2 => \^e\(0),
      I3 => \mOutPtr_reg[1]_2\(0),
      I4 => \mOutPtr_reg[1]_2\(1),
      O => \mOutPtr_reg[1]\(0)
    );
\mOutPtr[1]_i_1__4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => shiftReg_ce_0,
      I1 => \^e\(0),
      I2 => Q(1),
      O => \mOutPtr_reg[0]\(0)
    );
\mOutPtr[1]_i_1__5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => Q(1),
      I1 => ap_reg_pp0_iter4_or_cond_i_reg_2640,
      I2 => ap_enable_reg_pp0_iter5_reg_n_2,
      I3 => ap_block_pp0_stage0_subdone2_in,
      I4 => shiftReg_ce_1,
      O => \mOutPtr_reg[0]_0\(0)
    );
\mOutPtr[1]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"40BFBF40"
    )
        port map (
      I0 => shiftReg_ce_0,
      I1 => Q(1),
      I2 => \^e\(0),
      I3 => \mOutPtr_reg[1]_3\(0),
      I4 => \mOutPtr_reg[1]_3\(1),
      O => \mOutPtr_reg[1]_0\(0)
    );
\or_cond_i425_i_reg_2542[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5D55FBDB"
    )
        port map (
      I0 => t_V_reg_566(9),
      I1 => \tmp_3_reg_2522[0]_i_2_n_2\,
      I2 => t_V_reg_566(7),
      I3 => \tmp_3_reg_2522[0]_i_3_n_2\,
      I4 => t_V_reg_566(8),
      O => or_cond_i425_i_fu_709_p2
    );
\or_cond_i425_i_reg_2542_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => or_cond_i425_i_fu_709_p2,
      Q => or_cond_i425_i_reg_2542,
      R => '0'
    );
\or_cond_i_i_reg_2607[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => ap_block_pp0_stage0_subdone2_in,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => exitcond389_i_fu_936_p2,
      O => ImagLoc_x_reg_25920
    );
\or_cond_i_i_reg_2607[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_31_fu_984_p2,
      I1 => ImagLoc_x_fu_964_p2(11),
      O => or_cond_i_i_fu_990_p2
    );
\or_cond_i_i_reg_2607_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => or_cond_i_i_fu_990_p2,
      Q => or_cond_i_i_reg_2607,
      R => '0'
    );
\or_cond_i_reg_2640[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \icmp_reg_2509_reg_n_2_[0]\,
      I1 => \or_cond_i_reg_2640[0]_i_2_n_2\,
      O => or_cond_i_fu_1039_p2
    );
\or_cond_i_reg_2640[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => \or_cond_i_reg_2640[0]_i_3_n_2\,
      I1 => \t_V_2_reg_577_reg__0\(6),
      I2 => \t_V_2_reg_577_reg__0\(7),
      I3 => \t_V_2_reg_577_reg__0\(8),
      I4 => \t_V_2_reg_577_reg__0\(9),
      I5 => \t_V_2_reg_577_reg__0\(10),
      O => \or_cond_i_reg_2640[0]_i_2_n_2\
    );
\or_cond_i_reg_2640[0]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(5),
      I1 => \t_V_2_reg_577_reg__0\(1),
      I2 => \t_V_2_reg_577_reg__0\(2),
      I3 => \t_V_2_reg_577_reg__0\(3),
      I4 => \t_V_2_reg_577_reg__0\(4),
      O => \or_cond_i_reg_2640[0]_i_3_n_2\
    );
\or_cond_i_reg_2640_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => or_cond_i_fu_1039_p2,
      Q => or_cond_i_reg_2640,
      R => '0'
    );
p_Val2_10_0_0_2_fu_1493_p2_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => p_Val2_10_0_0_2_fu_1493_p2_carry_n_2,
      CO(2) => p_Val2_10_0_0_2_fu_1493_p2_carry_n_3,
      CO(1) => p_Val2_10_0_0_2_fu_1493_p2_carry_n_4,
      CO(0) => p_Val2_10_0_0_2_fu_1493_p2_carry_n_5,
      CYINIT => '1',
      DI(3 downto 0) => tmp_160_0_0_2_cast_fu_1489_p1(3 downto 0),
      O(3) => p_Val2_10_0_0_2_fu_1493_p2_carry_n_6,
      O(2) => p_Val2_10_0_0_2_fu_1493_p2_carry_n_7,
      O(1) => p_Val2_10_0_0_2_fu_1493_p2_carry_n_8,
      O(0) => p_Val2_10_0_0_2_fu_1493_p2_carry_n_9,
      S(3) => p_Val2_10_0_0_2_fu_1493_p2_carry_i_1_n_2,
      S(2) => p_Val2_10_0_0_2_fu_1493_p2_carry_i_2_n_2,
      S(1) => p_Val2_10_0_0_2_fu_1493_p2_carry_i_3_n_2,
      S(0) => p_Val2_10_0_0_2_fu_1493_p2_carry_i_4_n_2
    );
\p_Val2_10_0_0_2_fu_1493_p2_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => p_Val2_10_0_0_2_fu_1493_p2_carry_n_2,
      CO(3) => \p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_2\,
      CO(2) => \p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_3\,
      CO(1) => \p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_4\,
      CO(0) => \p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_5\,
      CYINIT => '0',
      DI(3 downto 0) => tmp_160_0_0_2_cast_fu_1489_p1(7 downto 4),
      O(3) => \p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_6\,
      O(2) => \p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_7\,
      O(1) => \p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_8\,
      O(0) => \p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_9\,
      S(3) => \p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_1_n_2\,
      S(2) => \p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_2_n_2\,
      S(1) => \p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_3_n_2\,
      S(0) => \p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_4_n_2\
    );
\p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8FFB800470047FF"
    )
        port map (
      I0 => col_buf_0_val_1_0_reg_2721(7),
      I1 => row_assign_9_0_2_t_reg_2565(0),
      I2 => col_buf_0_val_0_0_reg_2708(7),
      I3 => p_Val2_10_0_0_2_fu_1493_p2_carry_i_5_n_2,
      I4 => col_buf_0_val_2_0_reg_2729(7),
      I5 => src_kernel_win_0_va_5_fu_266(7),
      O => \p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_1_n_2\
    );
\p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E2FFE2001D001DFF"
    )
        port map (
      I0 => col_buf_0_val_0_0_reg_2708(6),
      I1 => row_assign_9_0_2_t_reg_2565(0),
      I2 => col_buf_0_val_1_0_reg_2721(6),
      I3 => p_Val2_10_0_0_2_fu_1493_p2_carry_i_5_n_2,
      I4 => col_buf_0_val_2_0_reg_2729(6),
      I5 => src_kernel_win_0_va_5_fu_266(6),
      O => \p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_2_n_2\
    );
\p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEE222E2111DDD1D"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(5),
      I1 => p_Val2_10_0_0_2_fu_1493_p2_carry_i_5_n_2,
      I2 => col_buf_0_val_0_0_reg_2708(5),
      I3 => row_assign_9_0_2_t_reg_2565(0),
      I4 => col_buf_0_val_1_0_reg_2721(5),
      I5 => src_kernel_win_0_va_5_fu_266(5),
      O => \p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_3_n_2\
    );
\p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E2FFE2001D001DFF"
    )
        port map (
      I0 => col_buf_0_val_0_0_reg_2708(4),
      I1 => row_assign_9_0_2_t_reg_2565(0),
      I2 => col_buf_0_val_1_0_reg_2721(4),
      I3 => p_Val2_10_0_0_2_fu_1493_p2_carry_i_5_n_2,
      I4 => col_buf_0_val_2_0_reg_2729(4),
      I5 => src_kernel_win_0_va_5_fu_266(4),
      O => \p_Val2_10_0_0_2_fu_1493_p2_carry__0_i_4_n_2\
    );
\p_Val2_10_0_0_2_fu_1493_p2_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_2\,
      CO(3 downto 0) => \NLW_p_Val2_10_0_0_2_fu_1493_p2_carry__1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_p_Val2_10_0_0_2_fu_1493_p2_carry__1_O_UNCONNECTED\(3 downto 1),
      O(0) => \p_Val2_10_0_0_2_fu_1493_p2_carry__1_n_9\,
      S(3 downto 0) => B"0001"
    );
p_Val2_10_0_0_2_fu_1493_p2_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8FFB800470047FF"
    )
        port map (
      I0 => col_buf_0_val_1_0_reg_2721(3),
      I1 => row_assign_9_0_2_t_reg_2565(0),
      I2 => col_buf_0_val_0_0_reg_2708(3),
      I3 => p_Val2_10_0_0_2_fu_1493_p2_carry_i_5_n_2,
      I4 => col_buf_0_val_2_0_reg_2729(3),
      I5 => src_kernel_win_0_va_5_fu_266(3),
      O => p_Val2_10_0_0_2_fu_1493_p2_carry_i_1_n_2
    );
p_Val2_10_0_0_2_fu_1493_p2_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEE222E2111DDD1D"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(2),
      I1 => p_Val2_10_0_0_2_fu_1493_p2_carry_i_5_n_2,
      I2 => col_buf_0_val_0_0_reg_2708(2),
      I3 => row_assign_9_0_2_t_reg_2565(0),
      I4 => col_buf_0_val_1_0_reg_2721(2),
      I5 => src_kernel_win_0_va_5_fu_266(2),
      O => p_Val2_10_0_0_2_fu_1493_p2_carry_i_2_n_2
    );
p_Val2_10_0_0_2_fu_1493_p2_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEE222E2111DDD1D"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(1),
      I1 => p_Val2_10_0_0_2_fu_1493_p2_carry_i_5_n_2,
      I2 => col_buf_0_val_0_0_reg_2708(1),
      I3 => row_assign_9_0_2_t_reg_2565(0),
      I4 => col_buf_0_val_1_0_reg_2721(1),
      I5 => src_kernel_win_0_va_5_fu_266(1),
      O => p_Val2_10_0_0_2_fu_1493_p2_carry_i_3_n_2
    );
p_Val2_10_0_0_2_fu_1493_p2_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEE222E2111DDD1D"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(0),
      I1 => p_Val2_10_0_0_2_fu_1493_p2_carry_i_5_n_2,
      I2 => col_buf_0_val_0_0_reg_2708(0),
      I3 => row_assign_9_0_2_t_reg_2565(0),
      I4 => col_buf_0_val_1_0_reg_2721(0),
      I5 => src_kernel_win_0_va_5_fu_266(0),
      O => p_Val2_10_0_0_2_fu_1493_p2_carry_i_4_n_2
    );
p_Val2_10_0_0_2_fu_1493_p2_carry_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tmp_3_reg_2522,
      I1 => row_assign_9_0_2_t_reg_2565(1),
      O => p_Val2_10_0_0_2_fu_1493_p2_carry_i_5_n_2
    );
\p_Val2_10_0_0_2_reg_2817[8]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ap_reg_pp0_iter2_or_cond_i_reg_2640,
      I1 => ap_block_pp0_stage0_subdone2_in,
      O => p_Val2_10_0_0_2_reg_28170
    );
\p_Val2_10_0_0_2_reg_2817_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => \p_Val2_10_0_0_2_fu_1493_p2_carry__1_n_9\,
      Q => p_Val2_10_0_0_2_reg_2817(8),
      R => '0'
    );
\p_Val2_1_fu_1973_p2__1_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \p_Val2_1_fu_1973_p2__1_carry_n_2\,
      CO(2) => \p_Val2_1_fu_1973_p2__1_carry_n_3\,
      CO(1) => \p_Val2_1_fu_1973_p2__1_carry_n_4\,
      CO(0) => \p_Val2_1_fu_1973_p2__1_carry_n_5\,
      CYINIT => '0',
      DI(3) => \p_Val2_1_fu_1973_p2__1_carry_i_1_n_2\,
      DI(2) => \p_Val2_1_fu_1973_p2__1_carry_i_2_n_2\,
      DI(1) => \p_Val2_1_fu_1973_p2__1_carry_i_3_n_2\,
      DI(0) => tmp_54_reg_2827(1),
      O(3) => \p_Val2_1_fu_1973_p2__1_carry_n_6\,
      O(2) => \p_Val2_1_fu_1973_p2__1_carry_n_7\,
      O(1) => \p_Val2_1_fu_1973_p2__1_carry_n_8\,
      O(0) => \NLW_p_Val2_1_fu_1973_p2__1_carry_O_UNCONNECTED\(0),
      S(3) => \p_Val2_1_fu_1973_p2__1_carry_i_4_n_2\,
      S(2) => \p_Val2_1_fu_1973_p2__1_carry_i_5_n_2\,
      S(1) => \p_Val2_1_fu_1973_p2__1_carry_i_6_n_2\,
      S(0) => \p_Val2_1_fu_1973_p2__1_carry_i_7_n_2\
    );
\p_Val2_1_fu_1973_p2__1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \p_Val2_1_fu_1973_p2__1_carry_n_2\,
      CO(3 downto 2) => \NLW_p_Val2_1_fu_1973_p2__1_carry__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \p_Val2_1_fu_1973_p2__1_carry__0_n_4\,
      CO(0) => \p_Val2_1_fu_1973_p2__1_carry__0_n_5\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \p_Val2_1_fu_1973_p2__1_carry__0_i_1_n_2\,
      DI(0) => \p_Val2_1_fu_1973_p2__1_carry__0_i_2_n_2\,
      O(3) => \NLW_p_Val2_1_fu_1973_p2__1_carry__0_O_UNCONNECTED\(3),
      O(2) => \p_Val2_1_fu_1973_p2__1_carry__0_n_7\,
      O(1) => \p_Val2_1_fu_1973_p2__1_carry__0_n_8\,
      O(0) => \p_Val2_1_fu_1973_p2__1_carry__0_n_9\,
      S(3) => '0',
      S(2) => \p_Val2_1_fu_1973_p2__1_carry__0_i_3_n_2\,
      S(1) => \p_Val2_1_fu_1973_p2__1_carry__0_i_4_n_2\,
      S(0) => \p_Val2_1_fu_1973_p2__1_carry__0_i_5_n_2\
    );
\p_Val2_1_fu_1973_p2__1_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => tmp_53_reg_2822(5),
      I1 => src_kernel_win_0_va_7_reg_2811(4),
      I2 => tmp_54_reg_2827(5),
      O => \p_Val2_1_fu_1973_p2__1_carry__0_i_1_n_2\
    );
\p_Val2_1_fu_1973_p2__1_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => tmp_53_reg_2822(4),
      I1 => src_kernel_win_0_va_7_reg_2811(3),
      I2 => tmp_54_reg_2827(4),
      O => \p_Val2_1_fu_1973_p2__1_carry__0_i_2_n_2\
    );
\p_Val2_1_fu_1973_p2__1_carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => tmp_54_reg_2827(6),
      I1 => src_kernel_win_0_va_7_reg_2811(5),
      I2 => tmp_53_reg_2822(6),
      I3 => src_kernel_win_0_va_7_reg_2811(6),
      I4 => tmp_53_reg_2822(7),
      I5 => tmp_54_reg_2827(7),
      O => \p_Val2_1_fu_1973_p2__1_carry__0_i_3_n_2\
    );
\p_Val2_1_fu_1973_p2__1_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \p_Val2_1_fu_1973_p2__1_carry__0_i_1_n_2\,
      I1 => src_kernel_win_0_va_7_reg_2811(5),
      I2 => tmp_53_reg_2822(6),
      I3 => tmp_54_reg_2827(6),
      O => \p_Val2_1_fu_1973_p2__1_carry__0_i_4_n_2\
    );
\p_Val2_1_fu_1973_p2__1_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => tmp_53_reg_2822(5),
      I1 => src_kernel_win_0_va_7_reg_2811(4),
      I2 => tmp_54_reg_2827(5),
      I3 => \p_Val2_1_fu_1973_p2__1_carry__0_i_2_n_2\,
      O => \p_Val2_1_fu_1973_p2__1_carry__0_i_5_n_2\
    );
\p_Val2_1_fu_1973_p2__1_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => tmp_53_reg_2822(3),
      I1 => src_kernel_win_0_va_7_reg_2811(2),
      I2 => tmp_54_reg_2827(3),
      O => \p_Val2_1_fu_1973_p2__1_carry_i_1_n_2\
    );
\p_Val2_1_fu_1973_p2__1_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => tmp_53_reg_2822(2),
      I1 => src_kernel_win_0_va_7_reg_2811(1),
      I2 => tmp_54_reg_2827(2),
      O => \p_Val2_1_fu_1973_p2__1_carry_i_2_n_2\
    );
\p_Val2_1_fu_1973_p2__1_carry_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => tmp_54_reg_2827(2),
      I1 => tmp_53_reg_2822(2),
      I2 => src_kernel_win_0_va_7_reg_2811(1),
      O => \p_Val2_1_fu_1973_p2__1_carry_i_3_n_2\
    );
\p_Val2_1_fu_1973_p2__1_carry_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => tmp_53_reg_2822(4),
      I1 => src_kernel_win_0_va_7_reg_2811(3),
      I2 => tmp_54_reg_2827(4),
      I3 => \p_Val2_1_fu_1973_p2__1_carry_i_1_n_2\,
      O => \p_Val2_1_fu_1973_p2__1_carry_i_4_n_2\
    );
\p_Val2_1_fu_1973_p2__1_carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => tmp_53_reg_2822(3),
      I1 => src_kernel_win_0_va_7_reg_2811(2),
      I2 => tmp_54_reg_2827(3),
      I3 => \p_Val2_1_fu_1973_p2__1_carry_i_2_n_2\,
      O => \p_Val2_1_fu_1973_p2__1_carry_i_5_n_2\
    );
\p_Val2_1_fu_1973_p2__1_carry_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => tmp_53_reg_2822(2),
      I1 => src_kernel_win_0_va_7_reg_2811(1),
      I2 => tmp_54_reg_2827(2),
      I3 => src_kernel_win_0_va_7_reg_2811(0),
      I4 => tmp_53_reg_2822(1),
      O => \p_Val2_1_fu_1973_p2__1_carry_i_6_n_2\
    );
\p_Val2_1_fu_1973_p2__1_carry_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => tmp_53_reg_2822(1),
      I1 => src_kernel_win_0_va_7_reg_2811(0),
      I2 => tmp_54_reg_2827(1),
      O => \p_Val2_1_fu_1973_p2__1_carry_i_7_n_2\
    );
\p_Val2_1_fu_1973_p2__20_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \p_Val2_1_fu_1973_p2__20_carry_n_2\,
      CO(2) => \p_Val2_1_fu_1973_p2__20_carry_n_3\,
      CO(1) => \p_Val2_1_fu_1973_p2__20_carry_n_4\,
      CO(0) => \p_Val2_1_fu_1973_p2__20_carry_n_5\,
      CYINIT => '0',
      DI(3) => \p_Val2_1_fu_1973_p2__20_carry_i_1_n_2\,
      DI(2) => \p_Val2_1_fu_1973_p2__20_carry_i_2_n_2\,
      DI(1) => \p_Val2_1_fu_1973_p2__20_carry_i_3_n_2\,
      DI(0) => '0',
      O(3 downto 0) => p_Val2_1_fu_1973_p2(3 downto 0),
      S(3) => \p_Val2_1_fu_1973_p2__20_carry_i_4_n_2\,
      S(2) => \p_Val2_1_fu_1973_p2__20_carry_i_5_n_2\,
      S(1) => \p_Val2_1_fu_1973_p2__20_carry_i_6_n_2\,
      S(0) => \p_Val2_1_fu_1973_p2__20_carry_i_7_n_2\
    );
\p_Val2_1_fu_1973_p2__20_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \p_Val2_1_fu_1973_p2__20_carry_n_2\,
      CO(3) => \NLW_p_Val2_1_fu_1973_p2__20_carry__0_CO_UNCONNECTED\(3),
      CO(2) => \p_Val2_1_fu_1973_p2__20_carry__0_n_3\,
      CO(1) => \p_Val2_1_fu_1973_p2__20_carry__0_n_4\,
      CO(0) => \p_Val2_1_fu_1973_p2__20_carry__0_n_5\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \p_Val2_1_fu_1973_p2__20_carry__0_i_1_n_2\,
      DI(1) => \p_Val2_1_fu_1973_p2__20_carry__0_i_2_n_2\,
      DI(0) => \p_Val2_1_fu_1973_p2__20_carry__0_i_3_n_2\,
      O(3 downto 0) => p_Val2_1_fu_1973_p2(7 downto 4),
      S(3) => \p_Val2_1_fu_1973_p2__20_carry__0_i_4_n_2\,
      S(2) => \p_Val2_1_fu_1973_p2__20_carry__0_i_5_n_2\,
      S(1) => \p_Val2_1_fu_1973_p2__20_carry__0_i_6_n_2\,
      S(0) => \p_Val2_1_fu_1973_p2__20_carry__0_i_7_n_2\
    );
\p_Val2_1_fu_1973_p2__20_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \p_Val2_1_fu_1973_p2__1_carry__0_n_9\,
      I1 => tmp_56_reg_2832(5),
      I2 => src_kernel_win_0_va_6_reg_2805(5),
      O => \p_Val2_1_fu_1973_p2__20_carry__0_i_1_n_2\
    );
\p_Val2_1_fu_1973_p2__20_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \p_Val2_1_fu_1973_p2__1_carry_n_6\,
      I1 => tmp_56_reg_2832(4),
      I2 => src_kernel_win_0_va_6_reg_2805(4),
      O => \p_Val2_1_fu_1973_p2__20_carry__0_i_2_n_2\
    );
\p_Val2_1_fu_1973_p2__20_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \p_Val2_1_fu_1973_p2__1_carry_n_7\,
      I1 => tmp_56_reg_2832(3),
      I2 => src_kernel_win_0_va_6_reg_2805(3),
      O => \p_Val2_1_fu_1973_p2__20_carry__0_i_3_n_2\
    );
\p_Val2_1_fu_1973_p2__20_carry__0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => src_kernel_win_0_va_6_reg_2805(6),
      I1 => tmp_56_reg_2832(6),
      I2 => \p_Val2_1_fu_1973_p2__1_carry__0_n_8\,
      I3 => tmp_56_reg_2832(7),
      I4 => \p_Val2_1_fu_1973_p2__1_carry__0_n_7\,
      I5 => src_kernel_win_0_va_6_reg_2805(7),
      O => \p_Val2_1_fu_1973_p2__20_carry__0_i_4_n_2\
    );
\p_Val2_1_fu_1973_p2__20_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \p_Val2_1_fu_1973_p2__20_carry__0_i_1_n_2\,
      I1 => tmp_56_reg_2832(6),
      I2 => \p_Val2_1_fu_1973_p2__1_carry__0_n_8\,
      I3 => src_kernel_win_0_va_6_reg_2805(6),
      O => \p_Val2_1_fu_1973_p2__20_carry__0_i_5_n_2\
    );
\p_Val2_1_fu_1973_p2__20_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \p_Val2_1_fu_1973_p2__1_carry__0_n_9\,
      I1 => tmp_56_reg_2832(5),
      I2 => src_kernel_win_0_va_6_reg_2805(5),
      I3 => \p_Val2_1_fu_1973_p2__20_carry__0_i_2_n_2\,
      O => \p_Val2_1_fu_1973_p2__20_carry__0_i_6_n_2\
    );
\p_Val2_1_fu_1973_p2__20_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \p_Val2_1_fu_1973_p2__1_carry_n_6\,
      I1 => tmp_56_reg_2832(4),
      I2 => src_kernel_win_0_va_6_reg_2805(4),
      I3 => \p_Val2_1_fu_1973_p2__20_carry__0_i_3_n_2\,
      O => \p_Val2_1_fu_1973_p2__20_carry__0_i_7_n_2\
    );
\p_Val2_1_fu_1973_p2__20_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \p_Val2_1_fu_1973_p2__1_carry_n_8\,
      I1 => tmp_56_reg_2832(2),
      I2 => src_kernel_win_0_va_6_reg_2805(2),
      O => \p_Val2_1_fu_1973_p2__20_carry_i_1_n_2\
    );
\p_Val2_1_fu_1973_p2__20_carry_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF969600"
    )
        port map (
      I0 => tmp_54_reg_2827(1),
      I1 => src_kernel_win_0_va_7_reg_2811(0),
      I2 => tmp_53_reg_2822(1),
      I3 => tmp_56_reg_2832(1),
      I4 => src_kernel_win_0_va_6_reg_2805(1),
      O => \p_Val2_1_fu_1973_p2__20_carry_i_2_n_2\
    );
\p_Val2_1_fu_1973_p2__20_carry_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => tmp_53_reg_2822(0),
      I1 => tmp_56_reg_2832(0),
      I2 => src_kernel_win_0_va_6_reg_2805(0),
      O => \p_Val2_1_fu_1973_p2__20_carry_i_3_n_2\
    );
\p_Val2_1_fu_1973_p2__20_carry_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \p_Val2_1_fu_1973_p2__1_carry_n_7\,
      I1 => tmp_56_reg_2832(3),
      I2 => src_kernel_win_0_va_6_reg_2805(3),
      I3 => \p_Val2_1_fu_1973_p2__20_carry_i_1_n_2\,
      O => \p_Val2_1_fu_1973_p2__20_carry_i_4_n_2\
    );
\p_Val2_1_fu_1973_p2__20_carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \p_Val2_1_fu_1973_p2__1_carry_n_8\,
      I1 => tmp_56_reg_2832(2),
      I2 => src_kernel_win_0_va_6_reg_2805(2),
      I3 => \p_Val2_1_fu_1973_p2__20_carry_i_2_n_2\,
      O => \p_Val2_1_fu_1973_p2__20_carry_i_5_n_2\
    );
\p_Val2_1_fu_1973_p2__20_carry_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996966996696996"
    )
        port map (
      I0 => \p_Val2_1_fu_1973_p2__20_carry_i_3_n_2\,
      I1 => tmp_56_reg_2832(1),
      I2 => tmp_54_reg_2827(1),
      I3 => src_kernel_win_0_va_7_reg_2811(0),
      I4 => tmp_53_reg_2822(1),
      I5 => src_kernel_win_0_va_6_reg_2805(1),
      O => \p_Val2_1_fu_1973_p2__20_carry_i_6_n_2\
    );
\p_Val2_1_fu_1973_p2__20_carry_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => tmp_53_reg_2822(0),
      I1 => tmp_56_reg_2832(0),
      I2 => src_kernel_win_0_va_6_reg_2805(0),
      O => \p_Val2_1_fu_1973_p2__20_carry_i_7_n_2\
    );
\p_Val2_1_reg_2922_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => isneg_1_reg_29320,
      D => p_Val2_1_fu_1973_p2(0),
      Q => p_Val2_1_reg_2922(0),
      R => '0'
    );
\p_Val2_1_reg_2922_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => isneg_1_reg_29320,
      D => p_Val2_1_fu_1973_p2(1),
      Q => p_Val2_1_reg_2922(1),
      R => '0'
    );
\p_Val2_1_reg_2922_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => isneg_1_reg_29320,
      D => p_Val2_1_fu_1973_p2(2),
      Q => p_Val2_1_reg_2922(2),
      R => '0'
    );
\p_Val2_1_reg_2922_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => isneg_1_reg_29320,
      D => p_Val2_1_fu_1973_p2(3),
      Q => p_Val2_1_reg_2922(3),
      R => '0'
    );
\p_Val2_1_reg_2922_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => isneg_1_reg_29320,
      D => p_Val2_1_fu_1973_p2(4),
      Q => p_Val2_1_reg_2922(4),
      R => '0'
    );
\p_Val2_1_reg_2922_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => isneg_1_reg_29320,
      D => p_Val2_1_fu_1973_p2(5),
      Q => p_Val2_1_reg_2922(5),
      R => '0'
    );
\p_Val2_1_reg_2922_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => isneg_1_reg_29320,
      D => p_Val2_1_fu_1973_p2(6),
      Q => p_Val2_1_reg_2922(6),
      R => '0'
    );
\p_Val2_1_reg_2922_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => isneg_1_reg_29320,
      D => p_Val2_1_fu_1973_p2(7),
      Q => p_Val2_1_reg_2922(7),
      R => '0'
    );
\p_Val2_2_fu_1946_p2__27_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \p_Val2_2_fu_1946_p2__27_carry_n_2\,
      CO(2) => \p_Val2_2_fu_1946_p2__27_carry_n_3\,
      CO(1) => \p_Val2_2_fu_1946_p2__27_carry_n_4\,
      CO(0) => \p_Val2_2_fu_1946_p2__27_carry_n_5\,
      CYINIT => '0',
      DI(3) => \p_Val2_2_fu_1946_p2__27_carry_i_1_n_2\,
      DI(2) => \p_Val2_2_fu_1946_p2__27_carry_i_2_n_2\,
      DI(1 downto 0) => tmp57_reg_2837(1 downto 0),
      O(3 downto 0) => \NLW_p_Val2_2_fu_1946_p2__27_carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \p_Val2_2_fu_1946_p2__27_carry_i_3_n_2\,
      S(2) => \p_Val2_2_fu_1946_p2__27_carry_i_4_n_2\,
      S(1) => \p_Val2_2_fu_1946_p2__27_carry_i_5_n_2\,
      S(0) => \p_Val2_2_fu_1946_p2__27_carry_i_6_n_2\
    );
\p_Val2_2_fu_1946_p2__27_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \p_Val2_2_fu_1946_p2__27_carry_n_2\,
      CO(3) => \p_Val2_2_fu_1946_p2__27_carry__0_n_2\,
      CO(2) => \p_Val2_2_fu_1946_p2__27_carry__0_n_3\,
      CO(1) => \p_Val2_2_fu_1946_p2__27_carry__0_n_4\,
      CO(0) => \p_Val2_2_fu_1946_p2__27_carry__0_n_5\,
      CYINIT => '0',
      DI(3) => \p_Val2_2_fu_1946_p2__27_carry__0_i_1_n_2\,
      DI(2) => \p_Val2_2_fu_1946_p2__27_carry__0_i_2_n_2\,
      DI(1) => \p_Val2_2_fu_1946_p2__27_carry__0_i_3_n_2\,
      DI(0) => \p_Val2_2_fu_1946_p2__27_carry__0_i_4_n_2\,
      O(3 downto 0) => \NLW_p_Val2_2_fu_1946_p2__27_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \p_Val2_2_fu_1946_p2__27_carry__0_i_5_n_2\,
      S(2) => \p_Val2_2_fu_1946_p2__27_carry__0_i_6_n_2\,
      S(1) => \p_Val2_2_fu_1946_p2__27_carry__0_i_7_n_2\,
      S(0) => \p_Val2_2_fu_1946_p2__27_carry__0_i_8_n_2\
    );
\p_Val2_2_fu_1946_p2__27_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => tmp69_cast_fu_1936_p1(6),
      I1 => src_kernel_win_0_va_7_reg_2811(5),
      I2 => tmp57_reg_2837(6),
      O => \p_Val2_2_fu_1946_p2__27_carry__0_i_1_n_2\
    );
\p_Val2_2_fu_1946_p2__27_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => tmp69_cast_fu_1936_p1(5),
      I1 => src_kernel_win_0_va_7_reg_2811(4),
      I2 => tmp57_reg_2837(5),
      O => \p_Val2_2_fu_1946_p2__27_carry__0_i_2_n_2\
    );
\p_Val2_2_fu_1946_p2__27_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => tmp69_cast_fu_1936_p1(4),
      I1 => src_kernel_win_0_va_7_reg_2811(3),
      I2 => tmp57_reg_2837(4),
      O => \p_Val2_2_fu_1946_p2__27_carry__0_i_3_n_2\
    );
\p_Val2_2_fu_1946_p2__27_carry__0_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => tmp69_cast_fu_1936_p1(3),
      I1 => src_kernel_win_0_va_7_reg_2811(2),
      I2 => tmp57_reg_2837(3),
      O => \p_Val2_2_fu_1946_p2__27_carry__0_i_4_n_2\
    );
\p_Val2_2_fu_1946_p2__27_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => tmp69_cast_fu_1936_p1(7),
      I1 => src_kernel_win_0_va_7_reg_2811(6),
      I2 => tmp57_reg_2837(7),
      I3 => \p_Val2_2_fu_1946_p2__27_carry__0_i_1_n_2\,
      O => \p_Val2_2_fu_1946_p2__27_carry__0_i_5_n_2\
    );
\p_Val2_2_fu_1946_p2__27_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => tmp69_cast_fu_1936_p1(6),
      I1 => src_kernel_win_0_va_7_reg_2811(5),
      I2 => tmp57_reg_2837(6),
      I3 => \p_Val2_2_fu_1946_p2__27_carry__0_i_2_n_2\,
      O => \p_Val2_2_fu_1946_p2__27_carry__0_i_6_n_2\
    );
\p_Val2_2_fu_1946_p2__27_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => tmp69_cast_fu_1936_p1(5),
      I1 => src_kernel_win_0_va_7_reg_2811(4),
      I2 => tmp57_reg_2837(5),
      I3 => \p_Val2_2_fu_1946_p2__27_carry__0_i_3_n_2\,
      O => \p_Val2_2_fu_1946_p2__27_carry__0_i_7_n_2\
    );
\p_Val2_2_fu_1946_p2__27_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => tmp69_cast_fu_1936_p1(4),
      I1 => src_kernel_win_0_va_7_reg_2811(3),
      I2 => tmp57_reg_2837(4),
      I3 => \p_Val2_2_fu_1946_p2__27_carry__0_i_4_n_2\,
      O => \p_Val2_2_fu_1946_p2__27_carry__0_i_8_n_2\
    );
\p_Val2_2_fu_1946_p2__27_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \p_Val2_2_fu_1946_p2__27_carry__0_n_2\,
      CO(3 downto 2) => \NLW_p_Val2_2_fu_1946_p2__27_carry__1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \p_Val2_2_fu_1946_p2__27_carry__1_n_4\,
      CO(0) => \p_Val2_2_fu_1946_p2__27_carry__1_n_5\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \p_Val2_2_fu_1946_p2__27_carry__1_i_1_n_2\,
      DI(0) => \p_Val2_2_fu_1946_p2__27_carry__1_i_2_n_2\,
      O(3) => \NLW_p_Val2_2_fu_1946_p2__27_carry__1_O_UNCONNECTED\(3),
      O(2 downto 0) => p_Val2_2_fu_1946_p2(10 downto 8),
      S(3) => '0',
      S(2) => \p_Val2_2_fu_1946_p2__27_carry__1_i_3_n_2\,
      S(1) => \p_Val2_2_fu_1946_p2__27_carry__1_i_4_n_2\,
      S(0) => \p_Val2_2_fu_1946_p2__27_carry__1_i_5_n_2\
    );
\p_Val2_2_fu_1946_p2__27_carry__1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => tmp69_cast_fu_1936_p1(8),
      I1 => src_kernel_win_0_va_7_reg_2811(7),
      I2 => tmp57_reg_2837(8),
      O => \p_Val2_2_fu_1946_p2__27_carry__1_i_1_n_2\
    );
\p_Val2_2_fu_1946_p2__27_carry__1_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => tmp69_cast_fu_1936_p1(7),
      I1 => src_kernel_win_0_va_7_reg_2811(6),
      I2 => tmp57_reg_2837(7),
      O => \p_Val2_2_fu_1946_p2__27_carry__1_i_2_n_2\
    );
\p_Val2_2_fu_1946_p2__27_carry__1_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => \p_Val2_2_fu_1946_p2_carry__1_n_4\,
      I1 => tmp57_reg_2837(9),
      I2 => tmp57_reg_2837(10),
      O => \p_Val2_2_fu_1946_p2__27_carry__1_i_3_n_2\
    );
\p_Val2_2_fu_1946_p2__27_carry__1_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"17E8E817"
    )
        port map (
      I0 => tmp57_reg_2837(8),
      I1 => src_kernel_win_0_va_7_reg_2811(7),
      I2 => tmp69_cast_fu_1936_p1(8),
      I3 => \p_Val2_2_fu_1946_p2_carry__1_n_4\,
      I4 => tmp57_reg_2837(9),
      O => \p_Val2_2_fu_1946_p2__27_carry__1_i_4_n_2\
    );
\p_Val2_2_fu_1946_p2__27_carry__1_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \p_Val2_2_fu_1946_p2__27_carry__1_i_2_n_2\,
      I1 => src_kernel_win_0_va_7_reg_2811(7),
      I2 => tmp69_cast_fu_1936_p1(8),
      I3 => tmp57_reg_2837(8),
      O => \p_Val2_2_fu_1946_p2__27_carry__1_i_5_n_2\
    );
\p_Val2_2_fu_1946_p2__27_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => tmp69_cast_fu_1936_p1(2),
      I1 => src_kernel_win_0_va_7_reg_2811(1),
      I2 => tmp57_reg_2837(2),
      O => \p_Val2_2_fu_1946_p2__27_carry_i_1_n_2\
    );
\p_Val2_2_fu_1946_p2__27_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => tmp57_reg_2837(2),
      I1 => tmp69_cast_fu_1936_p1(2),
      I2 => src_kernel_win_0_va_7_reg_2811(1),
      O => \p_Val2_2_fu_1946_p2__27_carry_i_2_n_2\
    );
\p_Val2_2_fu_1946_p2__27_carry_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => tmp69_cast_fu_1936_p1(3),
      I1 => src_kernel_win_0_va_7_reg_2811(2),
      I2 => tmp57_reg_2837(3),
      I3 => \p_Val2_2_fu_1946_p2__27_carry_i_1_n_2\,
      O => \p_Val2_2_fu_1946_p2__27_carry_i_3_n_2\
    );
\p_Val2_2_fu_1946_p2__27_carry_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => tmp69_cast_fu_1936_p1(2),
      I1 => src_kernel_win_0_va_7_reg_2811(1),
      I2 => tmp57_reg_2837(2),
      I3 => src_kernel_win_0_va_7_reg_2811(0),
      I4 => tmp69_cast_fu_1936_p1(1),
      O => \p_Val2_2_fu_1946_p2__27_carry_i_4_n_2\
    );
\p_Val2_2_fu_1946_p2__27_carry_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => tmp69_cast_fu_1936_p1(1),
      I1 => src_kernel_win_0_va_7_reg_2811(0),
      I2 => tmp57_reg_2837(1),
      O => \p_Val2_2_fu_1946_p2__27_carry_i_5_n_2\
    );
\p_Val2_2_fu_1946_p2__27_carry_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => tmp57_reg_2837(0),
      I1 => tmp69_cast_fu_1936_p1(0),
      O => \p_Val2_2_fu_1946_p2__27_carry_i_6_n_2\
    );
p_Val2_2_fu_1946_p2_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => p_Val2_2_fu_1946_p2_carry_n_2,
      CO(2) => p_Val2_2_fu_1946_p2_carry_n_3,
      CO(1) => p_Val2_2_fu_1946_p2_carry_n_4,
      CO(0) => p_Val2_2_fu_1946_p2_carry_n_5,
      CYINIT => '0',
      DI(3 downto 0) => src_kernel_win_0_va_6_reg_2805(3 downto 0),
      O(3 downto 0) => tmp69_cast_fu_1936_p1(3 downto 0),
      S(3) => p_Val2_2_fu_1946_p2_carry_i_1_n_2,
      S(2) => p_Val2_2_fu_1946_p2_carry_i_2_n_2,
      S(1) => p_Val2_2_fu_1946_p2_carry_i_3_n_2,
      S(0) => p_Val2_2_fu_1946_p2_carry_i_4_n_2
    );
\p_Val2_2_fu_1946_p2_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => p_Val2_2_fu_1946_p2_carry_n_2,
      CO(3) => \p_Val2_2_fu_1946_p2_carry__0_n_2\,
      CO(2) => \p_Val2_2_fu_1946_p2_carry__0_n_3\,
      CO(1) => \p_Val2_2_fu_1946_p2_carry__0_n_4\,
      CO(0) => \p_Val2_2_fu_1946_p2_carry__0_n_5\,
      CYINIT => '0',
      DI(3 downto 0) => src_kernel_win_0_va_6_reg_2805(7 downto 4),
      O(3 downto 0) => tmp69_cast_fu_1936_p1(7 downto 4),
      S(3) => \p_Val2_2_fu_1946_p2_carry__0_i_1_n_2\,
      S(2) => \p_Val2_2_fu_1946_p2_carry__0_i_2_n_2\,
      S(1) => \p_Val2_2_fu_1946_p2_carry__0_i_3_n_2\,
      S(0) => \p_Val2_2_fu_1946_p2_carry__0_i_4_n_2\
    );
\p_Val2_2_fu_1946_p2_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => src_kernel_win_0_va_6_reg_2805(7),
      I1 => tmp_53_reg_2822(7),
      O => \p_Val2_2_fu_1946_p2_carry__0_i_1_n_2\
    );
\p_Val2_2_fu_1946_p2_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => src_kernel_win_0_va_6_reg_2805(6),
      I1 => tmp_53_reg_2822(6),
      O => \p_Val2_2_fu_1946_p2_carry__0_i_2_n_2\
    );
\p_Val2_2_fu_1946_p2_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => src_kernel_win_0_va_6_reg_2805(5),
      I1 => tmp_53_reg_2822(5),
      O => \p_Val2_2_fu_1946_p2_carry__0_i_3_n_2\
    );
\p_Val2_2_fu_1946_p2_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => src_kernel_win_0_va_6_reg_2805(4),
      I1 => tmp_53_reg_2822(4),
      O => \p_Val2_2_fu_1946_p2_carry__0_i_4_n_2\
    );
\p_Val2_2_fu_1946_p2_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \p_Val2_2_fu_1946_p2_carry__0_n_2\,
      CO(3 downto 2) => \NLW_p_Val2_2_fu_1946_p2_carry__1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \p_Val2_2_fu_1946_p2_carry__1_n_4\,
      CO(0) => \NLW_p_Val2_2_fu_1946_p2_carry__1_CO_UNCONNECTED\(0),
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3 downto 1) => \NLW_p_Val2_2_fu_1946_p2_carry__1_O_UNCONNECTED\(3 downto 1),
      O(0) => tmp69_cast_fu_1936_p1(8),
      S(3 downto 1) => B"001",
      S(0) => p_Val2_10_0_0_2_reg_2817(8)
    );
p_Val2_2_fu_1946_p2_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => src_kernel_win_0_va_6_reg_2805(3),
      I1 => tmp_53_reg_2822(3),
      O => p_Val2_2_fu_1946_p2_carry_i_1_n_2
    );
p_Val2_2_fu_1946_p2_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => src_kernel_win_0_va_6_reg_2805(2),
      I1 => tmp_53_reg_2822(2),
      O => p_Val2_2_fu_1946_p2_carry_i_2_n_2
    );
p_Val2_2_fu_1946_p2_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => src_kernel_win_0_va_6_reg_2805(1),
      I1 => tmp_53_reg_2822(1),
      O => p_Val2_2_fu_1946_p2_carry_i_3_n_2
    );
p_Val2_2_fu_1946_p2_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => src_kernel_win_0_va_6_reg_2805(0),
      I1 => tmp_53_reg_2822(0),
      O => p_Val2_2_fu_1946_p2_carry_i_4_n_2
    );
\p_assign_2_reg_2622[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0__0\(0),
      O => \p_assign_2_reg_2622[0]_i_1_n_2\
    );
\p_assign_2_reg_2622[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"66566A666A666A66"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(10),
      I1 => \t_V_2_reg_577_reg__0\(9),
      I2 => \t_V_2_reg_577_reg__0\(8),
      I3 => \p_assign_2_reg_2622[10]_i_2_n_2\,
      I4 => \p_assign_2_reg_2622[10]_i_3_n_2\,
      I5 => \p_p2_i_i_cast_cast_reg_2612[7]_i_1_n_2\,
      O => \p_assign_2_reg_2622[10]_i_1_n_2\
    );
\p_assign_2_reg_2622[10]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(7),
      I1 => \t_V_2_reg_577_reg__0\(6),
      I2 => \ImagLoc_x_reg_2592[10]_i_2_n_2\,
      O => \p_assign_2_reg_2622[10]_i_2_n_2\
    );
\p_assign_2_reg_2622[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => \p_p2_i_i_cast_cast_reg_2612[6]_i_1_n_2\,
      I1 => \p_assign_2_reg_2622[10]_i_4_n_2\,
      I2 => \p_assign_2_reg_2622[10]_i_5_n_2\,
      I3 => ImagLoc_x_fu_964_p2(11),
      I4 => \p_assign_2_reg_2622[10]_i_6_n_2\,
      I5 => \p_assign_2_reg_2622[10]_i_7_n_2\,
      O => \p_assign_2_reg_2622[10]_i_3_n_2\
    );
\p_assign_2_reg_2622[10]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"55555556"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(4),
      I1 => \t_V_2_reg_577_reg__0\(2),
      I2 => \t_V_2_reg_577_reg__0\(1),
      I3 => \t_V_2_reg_577_reg__0__0\(0),
      I4 => \t_V_2_reg_577_reg__0\(3),
      O => \p_assign_2_reg_2622[10]_i_4_n_2\
    );
\p_assign_2_reg_2622[10]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEFEFEFEFEFFFEFE"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(2),
      I1 => \t_V_2_reg_577_reg__0\(1),
      I2 => \t_V_2_reg_577_reg__0__0\(0),
      I3 => \p_assign_2_reg_2622[10]_i_8_n_2\,
      I4 => \exitcond389_i_reg_2583[0]_i_4_n_2\,
      I5 => \ImagLoc_x_reg_2592[10]_i_2_n_2\,
      O => \p_assign_2_reg_2622[10]_i_5_n_2\
    );
\p_assign_2_reg_2622[10]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5556"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(3),
      I1 => \t_V_2_reg_577_reg__0__0\(0),
      I2 => \t_V_2_reg_577_reg__0\(1),
      I3 => \t_V_2_reg_577_reg__0\(2),
      O => \p_assign_2_reg_2622[10]_i_6_n_2\
    );
\p_assign_2_reg_2622[10]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555555555555556"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(5),
      I1 => \t_V_2_reg_577_reg__0\(2),
      I2 => \t_V_2_reg_577_reg__0\(1),
      I3 => \t_V_2_reg_577_reg__0__0\(0),
      I4 => \t_V_2_reg_577_reg__0\(3),
      I5 => \t_V_2_reg_577_reg__0\(4),
      O => \p_assign_2_reg_2622[10]_i_7_n_2\
    );
\p_assign_2_reg_2622[10]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(10),
      I1 => \t_V_2_reg_577_reg__0\(9),
      I2 => \t_V_2_reg_577_reg__0\(8),
      O => \p_assign_2_reg_2622[10]_i_8_n_2\
    );
\p_assign_2_reg_2622[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(1),
      I1 => \t_V_2_reg_577_reg__0__0\(0),
      I2 => ImagLoc_x_fu_964_p2(11),
      O => p_assign_2_fu_1028_p2(1)
    );
\p_assign_2_reg_2622[2]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(2),
      O => p_assign_2_fu_1028_p2(2)
    );
\p_assign_2_reg_2622[3]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(3),
      O => p_assign_2_fu_1028_p2(3)
    );
\p_assign_2_reg_2622[4]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(4),
      O => p_assign_2_fu_1028_p2(4)
    );
\p_assign_2_reg_2622[5]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(5),
      O => p_assign_2_fu_1028_p2(5)
    );
\p_assign_2_reg_2622[6]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(6),
      O => p_assign_2_fu_1028_p2(6)
    );
\p_assign_2_reg_2622[7]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(7),
      O => p_assign_2_fu_1028_p2(7)
    );
\p_assign_2_reg_2622[8]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(8),
      O => p_assign_2_fu_1028_p2(8)
    );
\p_assign_2_reg_2622_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \p_assign_2_reg_2622[0]_i_1_n_2\,
      Q => p_assign_2_reg_2622(0),
      R => '0'
    );
\p_assign_2_reg_2622_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \p_assign_2_reg_2622[10]_i_1_n_2\,
      Q => p_assign_2_reg_2622(10),
      R => '0'
    );
\p_assign_2_reg_2622_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => p_assign_2_fu_1028_p2(1),
      Q => p_assign_2_reg_2622(1),
      R => '0'
    );
\p_assign_2_reg_2622_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => p_assign_2_fu_1028_p2(2),
      Q => p_assign_2_reg_2622(2),
      R => '0'
    );
\p_assign_2_reg_2622_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => p_assign_2_fu_1028_p2(3),
      Q => p_assign_2_reg_2622(3),
      R => '0'
    );
\p_assign_2_reg_2622_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => p_assign_2_fu_1028_p2(4),
      Q => p_assign_2_reg_2622(4),
      R => '0'
    );
\p_assign_2_reg_2622_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => p_assign_2_fu_1028_p2(5),
      Q => p_assign_2_reg_2622(5),
      R => '0'
    );
\p_assign_2_reg_2622_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => p_assign_2_fu_1028_p2(6),
      Q => p_assign_2_reg_2622(6),
      R => '0'
    );
\p_assign_2_reg_2622_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => p_assign_2_fu_1028_p2(7),
      Q => p_assign_2_reg_2622(7),
      R => '0'
    );
\p_assign_2_reg_2622_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => p_assign_2_fu_1028_p2(8),
      Q => p_assign_2_reg_2622(8),
      R => '0'
    );
\p_assign_2_reg_2622_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \t_V_2_reg_577_reg__0\(9),
      Q => p_assign_2_reg_2622(9),
      R => '0'
    );
\p_assign_7_reg_2553[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5556"
    )
        port map (
      I0 => t_V_reg_566(4),
      I1 => t_V_reg_566(1),
      I2 => t_V_reg_566(2),
      I3 => t_V_reg_566(3),
      O => \p_assign_7_reg_2553[4]_i_1_n_2\
    );
\p_assign_7_reg_2553[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"55555556"
    )
        port map (
      I0 => t_V_reg_566(5),
      I1 => t_V_reg_566(4),
      I2 => t_V_reg_566(3),
      I3 => t_V_reg_566(2),
      I4 => t_V_reg_566(1),
      O => \p_assign_7_reg_2553[5]_i_1_n_2\
    );
\p_assign_7_reg_2553[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555555555555556"
    )
        port map (
      I0 => t_V_reg_566(6),
      I1 => t_V_reg_566(5),
      I2 => t_V_reg_566(4),
      I3 => t_V_reg_566(3),
      I4 => t_V_reg_566(2),
      I5 => t_V_reg_566(1),
      O => \p_assign_7_reg_2553[6]_i_1_n_2\
    );
\p_assign_7_reg_2553[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000010FFFFFFEF"
    )
        port map (
      I0 => t_V_reg_566(5),
      I1 => t_V_reg_566(4),
      I2 => \tmp_4_reg_2535[6]_i_2_n_2\,
      I3 => t_V_reg_566(1),
      I4 => t_V_reg_566(6),
      I5 => t_V_reg_566(7),
      O => p_assign_7_fu_723_p2(7)
    );
\p_assign_7_reg_2553[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555555555565555"
    )
        port map (
      I0 => t_V_reg_566(8),
      I1 => t_V_reg_566(7),
      I2 => t_V_reg_566(5),
      I3 => t_V_reg_566(4),
      I4 => \p_assign_7_reg_2553[8]_i_2_n_2\,
      I5 => t_V_reg_566(6),
      O => \p_assign_7_reg_2553[8]_i_1_n_2\
    );
\p_assign_7_reg_2553[8]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => t_V_reg_566(3),
      I1 => t_V_reg_566(2),
      I2 => t_V_reg_566(1),
      O => \p_assign_7_reg_2553[8]_i_2_n_2\
    );
\p_assign_7_reg_2553[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"65"
    )
        port map (
      I0 => t_V_reg_566(9),
      I1 => t_V_reg_566(8),
      I2 => \p_assign_7_reg_2553[9]_i_2_n_2\,
      O => \p_assign_7_reg_2553[9]_i_1_n_2\
    );
\p_assign_7_reg_2553[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000010"
    )
        port map (
      I0 => t_V_reg_566(6),
      I1 => t_V_reg_566(1),
      I2 => \tmp_4_reg_2535[6]_i_2_n_2\,
      I3 => t_V_reg_566(4),
      I4 => t_V_reg_566(5),
      I5 => t_V_reg_566(7),
      O => \p_assign_7_reg_2553[9]_i_2_n_2\
    );
\p_assign_7_reg_2553_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => t_V_reg_566(1),
      Q => p_assign_7_reg_2553(1),
      R => '0'
    );
\p_assign_7_reg_2553_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \p_assign_7_reg_2553[4]_i_1_n_2\,
      Q => p_assign_7_reg_2553(4),
      R => '0'
    );
\p_assign_7_reg_2553_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \p_assign_7_reg_2553[5]_i_1_n_2\,
      Q => p_assign_7_reg_2553(5),
      R => '0'
    );
\p_assign_7_reg_2553_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \p_assign_7_reg_2553[6]_i_1_n_2\,
      Q => p_assign_7_reg_2553(6),
      R => '0'
    );
\p_assign_7_reg_2553_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => p_assign_7_fu_723_p2(7),
      Q => p_assign_7_reg_2553(7),
      R => '0'
    );
\p_assign_7_reg_2553_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \p_assign_7_reg_2553[8]_i_1_n_2\,
      Q => p_assign_7_reg_2553(8),
      R => '0'
    );
\p_assign_7_reg_2553_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \p_assign_7_reg_2553[9]_i_1_n_2\,
      Q => p_assign_7_reg_2553(9),
      R => '0'
    );
\p_p2_i_i_cast_cast_reg_2612[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAAA8"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(10),
      I1 => \t_V_2_reg_577_reg__0\(8),
      I2 => \t_V_2_reg_577_reg__0\(9),
      I3 => \t_V_2_reg_577_reg__0\(7),
      I4 => \t_V_2_reg_577_reg__0\(6),
      I5 => \ImagLoc_x_reg_2592[10]_i_2_n_2\,
      O => \p_p2_i_i_cast_cast_reg_2612[10]_i_1_n_2\
    );
\p_p2_i_i_cast_cast_reg_2612[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"41"
    )
        port map (
      I0 => ImagLoc_x_fu_964_p2(11),
      I1 => \t_V_2_reg_577_reg__0__0\(0),
      I2 => \t_V_2_reg_577_reg__0\(1),
      O => \p_p2_i_i_cast_cast_reg_2612[1]_i_1_n_2\
    );
\p_p2_i_i_cast_cast_reg_2612[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00A9"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(2),
      I1 => \t_V_2_reg_577_reg__0\(1),
      I2 => \t_V_2_reg_577_reg__0__0\(0),
      I3 => ImagLoc_x_fu_964_p2(11),
      O => \p_p2_i_i_cast_cast_reg_2612[2]_i_1_n_2\
    );
\p_p2_i_i_cast_cast_reg_2612[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000AAA9"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(3),
      I1 => \t_V_2_reg_577_reg__0__0\(0),
      I2 => \t_V_2_reg_577_reg__0\(1),
      I3 => \t_V_2_reg_577_reg__0\(2),
      I4 => ImagLoc_x_fu_964_p2(11),
      O => \p_p2_i_i_cast_cast_reg_2612[3]_i_1_n_2\
    );
\p_p2_i_i_cast_cast_reg_2612[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAAAAAA9"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(4),
      I1 => \t_V_2_reg_577_reg__0\(2),
      I2 => \t_V_2_reg_577_reg__0\(1),
      I3 => \t_V_2_reg_577_reg__0__0\(0),
      I4 => \t_V_2_reg_577_reg__0\(3),
      I5 => ImagLoc_x_fu_964_p2(11),
      O => \p_p2_i_i_cast_cast_reg_2612[4]_i_1_n_2\
    );
\p_p2_i_i_cast_cast_reg_2612[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000AAA9"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(5),
      I1 => \p_p2_i_i_cast_cast_reg_2612[5]_i_2_n_2\,
      I2 => \t_V_2_reg_577_reg__0\(3),
      I3 => \t_V_2_reg_577_reg__0\(4),
      I4 => ImagLoc_x_fu_964_p2(11),
      O => \p_p2_i_i_cast_cast_reg_2612[5]_i_1_n_2\
    );
\p_p2_i_i_cast_cast_reg_2612[5]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(2),
      I1 => \t_V_2_reg_577_reg__0\(1),
      I2 => \t_V_2_reg_577_reg__0__0\(0),
      O => \p_p2_i_i_cast_cast_reg_2612[5]_i_2_n_2\
    );
\p_p2_i_i_cast_cast_reg_2612[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF00000000FFFE"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(10),
      I1 => \t_V_2_reg_577_reg__0\(9),
      I2 => \t_V_2_reg_577_reg__0\(8),
      I3 => \t_V_2_reg_577_reg__0\(7),
      I4 => \t_V_2_reg_577_reg__0\(6),
      I5 => \ImagLoc_x_reg_2592[10]_i_2_n_2\,
      O => \p_p2_i_i_cast_cast_reg_2612[6]_i_1_n_2\
    );
\p_p2_i_i_cast_cast_reg_2612[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF00000000FE"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(10),
      I1 => \t_V_2_reg_577_reg__0\(9),
      I2 => \t_V_2_reg_577_reg__0\(8),
      I3 => \t_V_2_reg_577_reg__0\(6),
      I4 => \ImagLoc_x_reg_2592[10]_i_2_n_2\,
      I5 => \t_V_2_reg_577_reg__0\(7),
      O => \p_p2_i_i_cast_cast_reg_2612[7]_i_1_n_2\
    );
\p_p2_i_i_cast_cast_reg_2612[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0F0F0F0F0F0F00E"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(10),
      I1 => \t_V_2_reg_577_reg__0\(9),
      I2 => \t_V_2_reg_577_reg__0\(8),
      I3 => \t_V_2_reg_577_reg__0\(7),
      I4 => \t_V_2_reg_577_reg__0\(6),
      I5 => \ImagLoc_x_reg_2592[10]_i_2_n_2\,
      O => \p_p2_i_i_cast_cast_reg_2612[8]_i_1_n_2\
    );
\p_p2_i_i_cast_cast_reg_2612[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCCCCCCCCCCCCC2"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(10),
      I1 => \t_V_2_reg_577_reg__0\(9),
      I2 => \t_V_2_reg_577_reg__0\(8),
      I3 => \t_V_2_reg_577_reg__0\(7),
      I4 => \t_V_2_reg_577_reg__0\(6),
      I5 => \ImagLoc_x_reg_2592[10]_i_2_n_2\,
      O => \p_p2_i_i_cast_cast_reg_2612[9]_i_1_n_2\
    );
\p_p2_i_i_cast_cast_reg_2612_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \p_p2_i_i_cast_cast_reg_2612[10]_i_1_n_2\,
      Q => p_p2_i_i_cast_cast_reg_2612_reg(10),
      R => '0'
    );
\p_p2_i_i_cast_cast_reg_2612_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \p_p2_i_i_cast_cast_reg_2612[1]_i_1_n_2\,
      Q => p_p2_i_i_cast_cast_reg_2612_reg(1),
      R => '0'
    );
\p_p2_i_i_cast_cast_reg_2612_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \p_p2_i_i_cast_cast_reg_2612[2]_i_1_n_2\,
      Q => p_p2_i_i_cast_cast_reg_2612_reg(2),
      R => '0'
    );
\p_p2_i_i_cast_cast_reg_2612_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \p_p2_i_i_cast_cast_reg_2612[3]_i_1_n_2\,
      Q => p_p2_i_i_cast_cast_reg_2612_reg(3),
      R => '0'
    );
\p_p2_i_i_cast_cast_reg_2612_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \p_p2_i_i_cast_cast_reg_2612[4]_i_1_n_2\,
      Q => p_p2_i_i_cast_cast_reg_2612_reg(4),
      R => '0'
    );
\p_p2_i_i_cast_cast_reg_2612_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \p_p2_i_i_cast_cast_reg_2612[5]_i_1_n_2\,
      Q => p_p2_i_i_cast_cast_reg_2612_reg(5),
      R => '0'
    );
\p_p2_i_i_cast_cast_reg_2612_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \p_p2_i_i_cast_cast_reg_2612[6]_i_1_n_2\,
      Q => p_p2_i_i_cast_cast_reg_2612_reg(6),
      R => '0'
    );
\p_p2_i_i_cast_cast_reg_2612_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \p_p2_i_i_cast_cast_reg_2612[7]_i_1_n_2\,
      Q => p_p2_i_i_cast_cast_reg_2612_reg(7),
      R => '0'
    );
\p_p2_i_i_cast_cast_reg_2612_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \p_p2_i_i_cast_cast_reg_2612[8]_i_1_n_2\,
      Q => p_p2_i_i_cast_cast_reg_2612_reg(8),
      R => '0'
    );
\p_p2_i_i_cast_cast_reg_2612_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => \p_p2_i_i_cast_cast_reg_2612[9]_i_1_n_2\,
      Q => p_p2_i_i_cast_cast_reg_2612_reg(9),
      R => '0'
    );
\reg_588[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => k_buf_0_val_3_U_n_13,
      I1 => ap_enable_reg_pp0_iter1,
      I2 => ap_CS_fsm_pp0_stage0,
      I3 => ap_block_pp0_stage0_subdone2_in,
      O => \^e\(0)
    );
\reg_588_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \^e\(0),
      D => \SRL_SIG_reg[0][7]_1\(0),
      Q => reg_588(0),
      R => '0'
    );
\reg_588_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \^e\(0),
      D => \SRL_SIG_reg[0][7]_1\(1),
      Q => reg_588(1),
      R => '0'
    );
\reg_588_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \^e\(0),
      D => \SRL_SIG_reg[0][7]_1\(2),
      Q => reg_588(2),
      R => '0'
    );
\reg_588_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \^e\(0),
      D => \SRL_SIG_reg[0][7]_1\(3),
      Q => reg_588(3),
      R => '0'
    );
\reg_588_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \^e\(0),
      D => \SRL_SIG_reg[0][7]_1\(4),
      Q => reg_588(4),
      R => '0'
    );
\reg_588_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \^e\(0),
      D => \SRL_SIG_reg[0][7]_1\(5),
      Q => reg_588(5),
      R => '0'
    );
\reg_588_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \^e\(0),
      D => \SRL_SIG_reg[0][7]_1\(6),
      Q => reg_588(6),
      R => '0'
    );
\reg_588_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \^e\(0),
      D => \SRL_SIG_reg[0][7]_1\(7),
      Q => reg_588(7),
      R => '0'
    );
\right_border_buf_0_1_fu_322_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_s_fu_318(0),
      Q => right_border_buf_0_1_fu_322(0),
      R => '0'
    );
\right_border_buf_0_1_fu_322_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_s_fu_318(1),
      Q => right_border_buf_0_1_fu_322(1),
      R => '0'
    );
\right_border_buf_0_1_fu_322_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_s_fu_318(2),
      Q => right_border_buf_0_1_fu_322(2),
      R => '0'
    );
\right_border_buf_0_1_fu_322_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_s_fu_318(3),
      Q => right_border_buf_0_1_fu_322(3),
      R => '0'
    );
\right_border_buf_0_1_fu_322_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_s_fu_318(4),
      Q => right_border_buf_0_1_fu_322(4),
      R => '0'
    );
\right_border_buf_0_1_fu_322_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_s_fu_318(5),
      Q => right_border_buf_0_1_fu_322(5),
      R => '0'
    );
\right_border_buf_0_1_fu_322_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_s_fu_318(6),
      Q => right_border_buf_0_1_fu_322(6),
      R => '0'
    );
\right_border_buf_0_1_fu_322_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_s_fu_318(7),
      Q => right_border_buf_0_1_fu_322(7),
      R => '0'
    );
\right_border_buf_0_2_fu_330[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_4_q0(0),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_3_fu_334(0),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_2_fu_330(0),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_1_0_fu_1159_p3(0)
    );
\right_border_buf_0_2_fu_330[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_4_q0(1),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_3_fu_334(1),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_2_fu_330(1),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_1_0_fu_1159_p3(1)
    );
\right_border_buf_0_2_fu_330[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_4_q0(2),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_3_fu_334(2),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_2_fu_330(2),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_1_0_fu_1159_p3(2)
    );
\right_border_buf_0_2_fu_330[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_4_q0(3),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_3_fu_334(3),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_2_fu_330(3),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_1_0_fu_1159_p3(3)
    );
\right_border_buf_0_2_fu_330[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_4_q0(4),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_3_fu_334(4),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_2_fu_330(4),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_1_0_fu_1159_p3(4)
    );
\right_border_buf_0_2_fu_330[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_4_q0(5),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_3_fu_334(5),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_2_fu_330(5),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_1_0_fu_1159_p3(5)
    );
\right_border_buf_0_2_fu_330[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_4_q0(6),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_3_fu_334(6),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_2_fu_330(6),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_1_0_fu_1159_p3(6)
    );
\right_border_buf_0_2_fu_330[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => tmp_1_reg_2500,
      I1 => \icmp_reg_2509_reg_n_2_[0]\,
      I2 => ap_enable_reg_pp0_iter2,
      I3 => col_buf_0_val_0_0_reg_27080,
      I4 => ap_reg_pp0_iter1_or_cond_i_i_reg_2607,
      O => ce119_out
    );
\right_border_buf_0_2_fu_330[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_4_q0(7),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_3_fu_334(7),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_2_fu_330(7),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_1_0_fu_1159_p3(7)
    );
\right_border_buf_0_2_fu_330_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_1_0_fu_1159_p3(0),
      Q => right_border_buf_0_2_fu_330(0),
      R => '0'
    );
\right_border_buf_0_2_fu_330_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_1_0_fu_1159_p3(1),
      Q => right_border_buf_0_2_fu_330(1),
      R => '0'
    );
\right_border_buf_0_2_fu_330_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_1_0_fu_1159_p3(2),
      Q => right_border_buf_0_2_fu_330(2),
      R => '0'
    );
\right_border_buf_0_2_fu_330_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_1_0_fu_1159_p3(3),
      Q => right_border_buf_0_2_fu_330(3),
      R => '0'
    );
\right_border_buf_0_2_fu_330_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_1_0_fu_1159_p3(4),
      Q => right_border_buf_0_2_fu_330(4),
      R => '0'
    );
\right_border_buf_0_2_fu_330_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_1_0_fu_1159_p3(5),
      Q => right_border_buf_0_2_fu_330(5),
      R => '0'
    );
\right_border_buf_0_2_fu_330_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_1_0_fu_1159_p3(6),
      Q => right_border_buf_0_2_fu_330(6),
      R => '0'
    );
\right_border_buf_0_2_fu_330_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_1_0_fu_1159_p3(7),
      Q => right_border_buf_0_2_fu_330(7),
      R => '0'
    );
\right_border_buf_0_3_fu_334_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_2_fu_330(0),
      Q => right_border_buf_0_3_fu_334(0),
      R => '0'
    );
\right_border_buf_0_3_fu_334_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_2_fu_330(1),
      Q => right_border_buf_0_3_fu_334(1),
      R => '0'
    );
\right_border_buf_0_3_fu_334_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_2_fu_330(2),
      Q => right_border_buf_0_3_fu_334(2),
      R => '0'
    );
\right_border_buf_0_3_fu_334_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_2_fu_330(3),
      Q => right_border_buf_0_3_fu_334(3),
      R => '0'
    );
\right_border_buf_0_3_fu_334_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_2_fu_330(4),
      Q => right_border_buf_0_3_fu_334(4),
      R => '0'
    );
\right_border_buf_0_3_fu_334_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_2_fu_330(5),
      Q => right_border_buf_0_3_fu_334(5),
      R => '0'
    );
\right_border_buf_0_3_fu_334_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_2_fu_330(6),
      Q => right_border_buf_0_3_fu_334(6),
      R => '0'
    );
\right_border_buf_0_3_fu_334_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_2_fu_330(7),
      Q => right_border_buf_0_3_fu_334(7),
      R => '0'
    );
\right_border_buf_0_4_fu_342[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_5_q0(0),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_5_fu_346(0),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_4_fu_342(0),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_2_0_fu_1178_p3(0)
    );
\right_border_buf_0_4_fu_342[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_5_q0(1),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_5_fu_346(1),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_4_fu_342(1),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_2_0_fu_1178_p3(1)
    );
\right_border_buf_0_4_fu_342[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_5_q0(2),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_5_fu_346(2),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_4_fu_342(2),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_2_0_fu_1178_p3(2)
    );
\right_border_buf_0_4_fu_342[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_5_q0(3),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_5_fu_346(3),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_4_fu_342(3),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_2_0_fu_1178_p3(3)
    );
\right_border_buf_0_4_fu_342[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_5_q0(4),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_5_fu_346(4),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_4_fu_342(4),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_2_0_fu_1178_p3(4)
    );
\right_border_buf_0_4_fu_342[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_5_q0(5),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_5_fu_346(5),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_4_fu_342(5),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_2_0_fu_1178_p3(5)
    );
\right_border_buf_0_4_fu_342[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_5_q0(6),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_5_fu_346(6),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_4_fu_342(6),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_2_0_fu_1178_p3(6)
    );
\right_border_buf_0_4_fu_342[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_5_q0(7),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_5_fu_346(7),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_4_fu_342(7),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_2_0_fu_1178_p3(7)
    );
\right_border_buf_0_4_fu_342_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_2_0_fu_1178_p3(0),
      Q => right_border_buf_0_4_fu_342(0),
      R => '0'
    );
\right_border_buf_0_4_fu_342_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_2_0_fu_1178_p3(1),
      Q => right_border_buf_0_4_fu_342(1),
      R => '0'
    );
\right_border_buf_0_4_fu_342_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_2_0_fu_1178_p3(2),
      Q => right_border_buf_0_4_fu_342(2),
      R => '0'
    );
\right_border_buf_0_4_fu_342_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_2_0_fu_1178_p3(3),
      Q => right_border_buf_0_4_fu_342(3),
      R => '0'
    );
\right_border_buf_0_4_fu_342_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_2_0_fu_1178_p3(4),
      Q => right_border_buf_0_4_fu_342(4),
      R => '0'
    );
\right_border_buf_0_4_fu_342_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_2_0_fu_1178_p3(5),
      Q => right_border_buf_0_4_fu_342(5),
      R => '0'
    );
\right_border_buf_0_4_fu_342_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_2_0_fu_1178_p3(6),
      Q => right_border_buf_0_4_fu_342(6),
      R => '0'
    );
\right_border_buf_0_4_fu_342_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_2_0_fu_1178_p3(7),
      Q => right_border_buf_0_4_fu_342(7),
      R => '0'
    );
\right_border_buf_0_5_fu_346_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_4_fu_342(0),
      Q => right_border_buf_0_5_fu_346(0),
      R => '0'
    );
\right_border_buf_0_5_fu_346_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_4_fu_342(1),
      Q => right_border_buf_0_5_fu_346(1),
      R => '0'
    );
\right_border_buf_0_5_fu_346_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_4_fu_342(2),
      Q => right_border_buf_0_5_fu_346(2),
      R => '0'
    );
\right_border_buf_0_5_fu_346_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_4_fu_342(3),
      Q => right_border_buf_0_5_fu_346(3),
      R => '0'
    );
\right_border_buf_0_5_fu_346_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_4_fu_342(4),
      Q => right_border_buf_0_5_fu_346(4),
      R => '0'
    );
\right_border_buf_0_5_fu_346_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_4_fu_342(5),
      Q => right_border_buf_0_5_fu_346(5),
      R => '0'
    );
\right_border_buf_0_5_fu_346_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_4_fu_342(6),
      Q => right_border_buf_0_5_fu_346(6),
      R => '0'
    );
\right_border_buf_0_5_fu_346_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => right_border_buf_0_4_fu_342(7),
      Q => right_border_buf_0_5_fu_346(7),
      R => '0'
    );
\right_border_buf_0_s_fu_318[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_3_q0(0),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_1_fu_322(0),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_s_fu_318(0),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_0_0_fu_1140_p3(0)
    );
\right_border_buf_0_s_fu_318[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_3_q0(1),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_1_fu_322(1),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_s_fu_318(1),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_0_0_fu_1140_p3(1)
    );
\right_border_buf_0_s_fu_318[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_3_q0(2),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_1_fu_322(2),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_s_fu_318(2),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_0_0_fu_1140_p3(2)
    );
\right_border_buf_0_s_fu_318[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_3_q0(3),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_1_fu_322(3),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_s_fu_318(3),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_0_0_fu_1140_p3(3)
    );
\right_border_buf_0_s_fu_318[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_3_q0(4),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_1_fu_322(4),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_s_fu_318(4),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_0_0_fu_1140_p3(4)
    );
\right_border_buf_0_s_fu_318[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_3_q0(5),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_1_fu_322(5),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_s_fu_318(5),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_0_0_fu_1140_p3(5)
    );
\right_border_buf_0_s_fu_318[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_3_q0(6),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_1_fu_322(6),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_s_fu_318(6),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_0_0_fu_1140_p3(6)
    );
\right_border_buf_0_s_fu_318[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB888B888888888"
    )
        port map (
      I0 => k_buf_0_val_3_q0(7),
      I1 => ap_reg_pp0_iter1_brmerge_reg_2627,
      I2 => right_border_buf_0_1_fu_322(7),
      I3 => tmp_49_reg_2644(0),
      I4 => right_border_buf_0_s_fu_318(7),
      I5 => tmp_49_reg_2644(1),
      O => col_buf_0_val_0_0_fu_1140_p3(7)
    );
\right_border_buf_0_s_fu_318_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_0_0_fu_1140_p3(0),
      Q => right_border_buf_0_s_fu_318(0),
      R => '0'
    );
\right_border_buf_0_s_fu_318_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_0_0_fu_1140_p3(1),
      Q => right_border_buf_0_s_fu_318(1),
      R => '0'
    );
\right_border_buf_0_s_fu_318_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_0_0_fu_1140_p3(2),
      Q => right_border_buf_0_s_fu_318(2),
      R => '0'
    );
\right_border_buf_0_s_fu_318_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_0_0_fu_1140_p3(3),
      Q => right_border_buf_0_s_fu_318(3),
      R => '0'
    );
\right_border_buf_0_s_fu_318_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_0_0_fu_1140_p3(4),
      Q => right_border_buf_0_s_fu_318(4),
      R => '0'
    );
\right_border_buf_0_s_fu_318_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_0_0_fu_1140_p3(5),
      Q => right_border_buf_0_s_fu_318(5),
      R => '0'
    );
\right_border_buf_0_s_fu_318_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_0_0_fu_1140_p3(6),
      Q => right_border_buf_0_s_fu_318(6),
      R => '0'
    );
\right_border_buf_0_s_fu_318_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce119_out,
      D => col_buf_0_val_0_0_fu_1140_p3(7),
      Q => right_border_buf_0_s_fu_318(7),
      R => '0'
    );
\row_assign_9_0_1_t_reg_2558[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => t_V_reg_566(1),
      I1 => t_V_reg_566(0),
      I2 => \icmp_reg_2509[0]_i_2_n_2\,
      O => row_assign_9_0_1_t_fu_825_p2(1)
    );
\row_assign_9_0_1_t_reg_2558_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => i_V_fu_631_p2(0),
      Q => row_assign_9_0_1_t_reg_2558(0),
      R => '0'
    );
\row_assign_9_0_1_t_reg_2558_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => row_assign_9_0_1_t_fu_825_p2(1),
      Q => row_assign_9_0_1_t_reg_2558(1),
      R => '0'
    );
\row_assign_9_0_2_t_reg_2565[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => \icmp_reg_2509[0]_i_2_n_2\,
      I1 => t_V_reg_566(0),
      I2 => t_V_reg_566(1),
      O => row_assign_9_0_2_t_fu_839_p2(1)
    );
\row_assign_9_0_2_t_reg_2565_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => t_V_reg_566(0),
      Q => row_assign_9_0_2_t_reg_2565(0),
      R => '0'
    );
\row_assign_9_0_2_t_reg_2565_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => row_assign_9_0_2_t_fu_839_p2(1),
      Q => row_assign_9_0_2_t_reg_2565(1),
      R => '0'
    );
\src_kernel_win_0_va_1_fu_250_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_fu_246(0),
      Q => src_kernel_win_0_va_1_fu_250(0),
      R => '0'
    );
\src_kernel_win_0_va_1_fu_250_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_fu_246(1),
      Q => src_kernel_win_0_va_1_fu_250(1),
      R => '0'
    );
\src_kernel_win_0_va_1_fu_250_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_fu_246(2),
      Q => src_kernel_win_0_va_1_fu_250(2),
      R => '0'
    );
\src_kernel_win_0_va_1_fu_250_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_fu_246(3),
      Q => src_kernel_win_0_va_1_fu_250(3),
      R => '0'
    );
\src_kernel_win_0_va_1_fu_250_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_fu_246(4),
      Q => src_kernel_win_0_va_1_fu_250(4),
      R => '0'
    );
\src_kernel_win_0_va_1_fu_250_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_fu_246(5),
      Q => src_kernel_win_0_va_1_fu_250(5),
      R => '0'
    );
\src_kernel_win_0_va_1_fu_250_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_fu_246(6),
      Q => src_kernel_win_0_va_1_fu_250(6),
      R => '0'
    );
\src_kernel_win_0_va_1_fu_250_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_fu_246(7),
      Q => src_kernel_win_0_va_1_fu_250(7),
      R => '0'
    );
\src_kernel_win_0_va_2_fu_254_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_7_fu_1456_p3(0),
      Q => src_kernel_win_0_va_2_fu_254(0),
      R => '0'
    );
\src_kernel_win_0_va_2_fu_254_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_7_fu_1456_p3(1),
      Q => src_kernel_win_0_va_2_fu_254(1),
      R => '0'
    );
\src_kernel_win_0_va_2_fu_254_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_7_fu_1456_p3(2),
      Q => src_kernel_win_0_va_2_fu_254(2),
      R => '0'
    );
\src_kernel_win_0_va_2_fu_254_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_7_fu_1456_p3(3),
      Q => src_kernel_win_0_va_2_fu_254(3),
      R => '0'
    );
\src_kernel_win_0_va_2_fu_254_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_7_fu_1456_p3(4),
      Q => src_kernel_win_0_va_2_fu_254(4),
      R => '0'
    );
\src_kernel_win_0_va_2_fu_254_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_7_fu_1456_p3(5),
      Q => src_kernel_win_0_va_2_fu_254(5),
      R => '0'
    );
\src_kernel_win_0_va_2_fu_254_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_7_fu_1456_p3(6),
      Q => src_kernel_win_0_va_2_fu_254(6),
      R => '0'
    );
\src_kernel_win_0_va_2_fu_254_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_7_fu_1456_p3(7),
      Q => src_kernel_win_0_va_2_fu_254(7),
      R => '0'
    );
\src_kernel_win_0_va_3_fu_258_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_2_fu_254(0),
      Q => src_kernel_win_0_va_3_fu_258(0),
      R => '0'
    );
\src_kernel_win_0_va_3_fu_258_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_2_fu_254(1),
      Q => src_kernel_win_0_va_3_fu_258(1),
      R => '0'
    );
\src_kernel_win_0_va_3_fu_258_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_2_fu_254(2),
      Q => src_kernel_win_0_va_3_fu_258(2),
      R => '0'
    );
\src_kernel_win_0_va_3_fu_258_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_2_fu_254(3),
      Q => src_kernel_win_0_va_3_fu_258(3),
      R => '0'
    );
\src_kernel_win_0_va_3_fu_258_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_2_fu_254(4),
      Q => src_kernel_win_0_va_3_fu_258(4),
      R => '0'
    );
\src_kernel_win_0_va_3_fu_258_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_2_fu_254(5),
      Q => src_kernel_win_0_va_3_fu_258(5),
      R => '0'
    );
\src_kernel_win_0_va_3_fu_258_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_2_fu_254(6),
      Q => src_kernel_win_0_va_3_fu_258(6),
      R => '0'
    );
\src_kernel_win_0_va_3_fu_258_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_2_fu_254(7),
      Q => src_kernel_win_0_va_3_fu_258(7),
      R => '0'
    );
\src_kernel_win_0_va_4_fu_262[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFB8FF0000B800"
    )
        port map (
      I0 => col_buf_0_val_1_0_reg_2721(0),
      I1 => row_assign_9_0_2_t_reg_2565(0),
      I2 => col_buf_0_val_0_0_reg_2708(0),
      I3 => tmp_3_reg_2522,
      I4 => row_assign_9_0_2_t_reg_2565(1),
      I5 => col_buf_0_val_2_0_reg_2729(0),
      O => tmp_160_0_0_2_cast_fu_1489_p1(0)
    );
\src_kernel_win_0_va_4_fu_262[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFB8FF0000B800"
    )
        port map (
      I0 => col_buf_0_val_1_0_reg_2721(1),
      I1 => row_assign_9_0_2_t_reg_2565(0),
      I2 => col_buf_0_val_0_0_reg_2708(1),
      I3 => tmp_3_reg_2522,
      I4 => row_assign_9_0_2_t_reg_2565(1),
      I5 => col_buf_0_val_2_0_reg_2729(1),
      O => tmp_160_0_0_2_cast_fu_1489_p1(1)
    );
\src_kernel_win_0_va_4_fu_262[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFB8FF0000B800"
    )
        port map (
      I0 => col_buf_0_val_1_0_reg_2721(2),
      I1 => row_assign_9_0_2_t_reg_2565(0),
      I2 => col_buf_0_val_0_0_reg_2708(2),
      I3 => tmp_3_reg_2522,
      I4 => row_assign_9_0_2_t_reg_2565(1),
      I5 => col_buf_0_val_2_0_reg_2729(2),
      O => tmp_160_0_0_2_cast_fu_1489_p1(2)
    );
\src_kernel_win_0_va_4_fu_262[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BABABA8A8A8ABA8A"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(3),
      I1 => row_assign_9_0_2_t_reg_2565(1),
      I2 => tmp_3_reg_2522,
      I3 => col_buf_0_val_0_0_reg_2708(3),
      I4 => row_assign_9_0_2_t_reg_2565(0),
      I5 => col_buf_0_val_1_0_reg_2721(3),
      O => tmp_160_0_0_2_cast_fu_1489_p1(3)
    );
\src_kernel_win_0_va_4_fu_262[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(4),
      I1 => row_assign_9_0_2_t_reg_2565(1),
      I2 => tmp_3_reg_2522,
      I3 => col_buf_0_val_1_0_reg_2721(4),
      I4 => row_assign_9_0_2_t_reg_2565(0),
      I5 => col_buf_0_val_0_0_reg_2708(4),
      O => tmp_160_0_0_2_cast_fu_1489_p1(4)
    );
\src_kernel_win_0_va_4_fu_262[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFB8FF0000B800"
    )
        port map (
      I0 => col_buf_0_val_1_0_reg_2721(5),
      I1 => row_assign_9_0_2_t_reg_2565(0),
      I2 => col_buf_0_val_0_0_reg_2708(5),
      I3 => tmp_3_reg_2522,
      I4 => row_assign_9_0_2_t_reg_2565(1),
      I5 => col_buf_0_val_2_0_reg_2729(5),
      O => tmp_160_0_0_2_cast_fu_1489_p1(5)
    );
\src_kernel_win_0_va_4_fu_262[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA8ABABABA8A8A8A"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(6),
      I1 => row_assign_9_0_2_t_reg_2565(1),
      I2 => tmp_3_reg_2522,
      I3 => col_buf_0_val_1_0_reg_2721(6),
      I4 => row_assign_9_0_2_t_reg_2565(0),
      I5 => col_buf_0_val_0_0_reg_2708(6),
      O => tmp_160_0_0_2_cast_fu_1489_p1(6)
    );
\src_kernel_win_0_va_4_fu_262[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => ap_reg_pp0_iter2_exitcond389_i_reg_2583,
      I1 => ap_block_pp0_stage0_subdone2_in,
      I2 => ap_enable_reg_pp0_iter3,
      O => src_kernel_win_0_va_1_fu_2500
    );
\src_kernel_win_0_va_4_fu_262[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BABABA8A8A8ABA8A"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(7),
      I1 => row_assign_9_0_2_t_reg_2565(1),
      I2 => tmp_3_reg_2522,
      I3 => col_buf_0_val_0_0_reg_2708(7),
      I4 => row_assign_9_0_2_t_reg_2565(0),
      I5 => col_buf_0_val_1_0_reg_2721(7),
      O => tmp_160_0_0_2_cast_fu_1489_p1(7)
    );
\src_kernel_win_0_va_4_fu_262_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => tmp_160_0_0_2_cast_fu_1489_p1(0),
      Q => src_kernel_win_0_va_4_fu_262(0),
      R => '0'
    );
\src_kernel_win_0_va_4_fu_262_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => tmp_160_0_0_2_cast_fu_1489_p1(1),
      Q => src_kernel_win_0_va_4_fu_262(1),
      R => '0'
    );
\src_kernel_win_0_va_4_fu_262_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => tmp_160_0_0_2_cast_fu_1489_p1(2),
      Q => src_kernel_win_0_va_4_fu_262(2),
      R => '0'
    );
\src_kernel_win_0_va_4_fu_262_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => tmp_160_0_0_2_cast_fu_1489_p1(3),
      Q => src_kernel_win_0_va_4_fu_262(3),
      R => '0'
    );
\src_kernel_win_0_va_4_fu_262_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => tmp_160_0_0_2_cast_fu_1489_p1(4),
      Q => src_kernel_win_0_va_4_fu_262(4),
      R => '0'
    );
\src_kernel_win_0_va_4_fu_262_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => tmp_160_0_0_2_cast_fu_1489_p1(5),
      Q => src_kernel_win_0_va_4_fu_262(5),
      R => '0'
    );
\src_kernel_win_0_va_4_fu_262_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => tmp_160_0_0_2_cast_fu_1489_p1(6),
      Q => src_kernel_win_0_va_4_fu_262(6),
      R => '0'
    );
\src_kernel_win_0_va_4_fu_262_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => tmp_160_0_0_2_cast_fu_1489_p1(7),
      Q => src_kernel_win_0_va_4_fu_262(7),
      R => '0'
    );
\src_kernel_win_0_va_5_fu_266_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_4_fu_262(0),
      Q => src_kernel_win_0_va_5_fu_266(0),
      R => '0'
    );
\src_kernel_win_0_va_5_fu_266_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_4_fu_262(1),
      Q => src_kernel_win_0_va_5_fu_266(1),
      R => '0'
    );
\src_kernel_win_0_va_5_fu_266_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_4_fu_262(2),
      Q => src_kernel_win_0_va_5_fu_266(2),
      R => '0'
    );
\src_kernel_win_0_va_5_fu_266_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_4_fu_262(3),
      Q => src_kernel_win_0_va_5_fu_266(3),
      R => '0'
    );
\src_kernel_win_0_va_5_fu_266_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_4_fu_262(4),
      Q => src_kernel_win_0_va_5_fu_266(4),
      R => '0'
    );
\src_kernel_win_0_va_5_fu_266_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_4_fu_262(5),
      Q => src_kernel_win_0_va_5_fu_266(5),
      R => '0'
    );
\src_kernel_win_0_va_5_fu_266_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_4_fu_262(6),
      Q => src_kernel_win_0_va_5_fu_266(6),
      R => '0'
    );
\src_kernel_win_0_va_5_fu_266_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_4_fu_262(7),
      Q => src_kernel_win_0_va_5_fu_266(7),
      R => '0'
    );
\src_kernel_win_0_va_6_reg_2805[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBFFFFB8880000"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(0),
      I1 => tmp_32_reg_2572(1),
      I2 => col_buf_0_val_1_0_reg_2721(0),
      I3 => tmp_32_reg_2572(0),
      I4 => tmp_3_reg_2522,
      I5 => col_buf_0_val_0_0_reg_2708(0),
      O => src_kernel_win_0_va_6_fu_1442_p3(0)
    );
\src_kernel_win_0_va_6_reg_2805[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBFFFFB8880000"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(1),
      I1 => tmp_32_reg_2572(1),
      I2 => col_buf_0_val_1_0_reg_2721(1),
      I3 => tmp_32_reg_2572(0),
      I4 => tmp_3_reg_2522,
      I5 => col_buf_0_val_0_0_reg_2708(1),
      O => src_kernel_win_0_va_6_fu_1442_p3(1)
    );
\src_kernel_win_0_va_6_reg_2805[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBFFFFB8880000"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(2),
      I1 => tmp_32_reg_2572(1),
      I2 => col_buf_0_val_1_0_reg_2721(2),
      I3 => tmp_32_reg_2572(0),
      I4 => tmp_3_reg_2522,
      I5 => col_buf_0_val_0_0_reg_2708(2),
      O => src_kernel_win_0_va_6_fu_1442_p3(2)
    );
\src_kernel_win_0_va_6_reg_2805[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBFFFFB8880000"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(3),
      I1 => tmp_32_reg_2572(1),
      I2 => col_buf_0_val_1_0_reg_2721(3),
      I3 => tmp_32_reg_2572(0),
      I4 => tmp_3_reg_2522,
      I5 => col_buf_0_val_0_0_reg_2708(3),
      O => src_kernel_win_0_va_6_fu_1442_p3(3)
    );
\src_kernel_win_0_va_6_reg_2805[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBFFFFB8880000"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(4),
      I1 => tmp_32_reg_2572(1),
      I2 => col_buf_0_val_1_0_reg_2721(4),
      I3 => tmp_32_reg_2572(0),
      I4 => tmp_3_reg_2522,
      I5 => col_buf_0_val_0_0_reg_2708(4),
      O => src_kernel_win_0_va_6_fu_1442_p3(4)
    );
\src_kernel_win_0_va_6_reg_2805[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBFFFFB8880000"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(5),
      I1 => tmp_32_reg_2572(1),
      I2 => col_buf_0_val_1_0_reg_2721(5),
      I3 => tmp_32_reg_2572(0),
      I4 => tmp_3_reg_2522,
      I5 => col_buf_0_val_0_0_reg_2708(5),
      O => src_kernel_win_0_va_6_fu_1442_p3(5)
    );
\src_kernel_win_0_va_6_reg_2805[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBFFFFB8880000"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(6),
      I1 => tmp_32_reg_2572(1),
      I2 => col_buf_0_val_1_0_reg_2721(6),
      I3 => tmp_32_reg_2572(0),
      I4 => tmp_3_reg_2522,
      I5 => col_buf_0_val_0_0_reg_2708(6),
      O => src_kernel_win_0_va_6_fu_1442_p3(6)
    );
\src_kernel_win_0_va_6_reg_2805[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8BBFFFFB8880000"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(7),
      I1 => tmp_32_reg_2572(1),
      I2 => col_buf_0_val_1_0_reg_2721(7),
      I3 => tmp_32_reg_2572(0),
      I4 => tmp_3_reg_2522,
      I5 => col_buf_0_val_0_0_reg_2708(7),
      O => src_kernel_win_0_va_6_fu_1442_p3(7)
    );
\src_kernel_win_0_va_6_reg_2805_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => src_kernel_win_0_va_6_fu_1442_p3(0),
      Q => src_kernel_win_0_va_6_reg_2805(0),
      R => '0'
    );
\src_kernel_win_0_va_6_reg_2805_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => src_kernel_win_0_va_6_fu_1442_p3(1),
      Q => src_kernel_win_0_va_6_reg_2805(1),
      R => '0'
    );
\src_kernel_win_0_va_6_reg_2805_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => src_kernel_win_0_va_6_fu_1442_p3(2),
      Q => src_kernel_win_0_va_6_reg_2805(2),
      R => '0'
    );
\src_kernel_win_0_va_6_reg_2805_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => src_kernel_win_0_va_6_fu_1442_p3(3),
      Q => src_kernel_win_0_va_6_reg_2805(3),
      R => '0'
    );
\src_kernel_win_0_va_6_reg_2805_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => src_kernel_win_0_va_6_fu_1442_p3(4),
      Q => src_kernel_win_0_va_6_reg_2805(4),
      R => '0'
    );
\src_kernel_win_0_va_6_reg_2805_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => src_kernel_win_0_va_6_fu_1442_p3(5),
      Q => src_kernel_win_0_va_6_reg_2805(5),
      R => '0'
    );
\src_kernel_win_0_va_6_reg_2805_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => src_kernel_win_0_va_6_fu_1442_p3(6),
      Q => src_kernel_win_0_va_6_reg_2805(6),
      R => '0'
    );
\src_kernel_win_0_va_6_reg_2805_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => src_kernel_win_0_va_6_fu_1442_p3(7),
      Q => src_kernel_win_0_va_6_reg_2805(7),
      R => '0'
    );
\src_kernel_win_0_va_7_reg_2811[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB8FFFF8B880000"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(0),
      I1 => row_assign_9_0_1_t_reg_2558(1),
      I2 => row_assign_9_0_1_t_reg_2558(0),
      I3 => col_buf_0_val_0_0_reg_2708(0),
      I4 => tmp_3_reg_2522,
      I5 => col_buf_0_val_1_0_reg_2721(0),
      O => src_kernel_win_0_va_7_fu_1456_p3(0)
    );
\src_kernel_win_0_va_7_reg_2811[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB8FFFF8B880000"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(1),
      I1 => row_assign_9_0_1_t_reg_2558(1),
      I2 => row_assign_9_0_1_t_reg_2558(0),
      I3 => col_buf_0_val_0_0_reg_2708(1),
      I4 => tmp_3_reg_2522,
      I5 => col_buf_0_val_1_0_reg_2721(1),
      O => src_kernel_win_0_va_7_fu_1456_p3(1)
    );
\src_kernel_win_0_va_7_reg_2811[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB8FFFF8B880000"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(2),
      I1 => row_assign_9_0_1_t_reg_2558(1),
      I2 => row_assign_9_0_1_t_reg_2558(0),
      I3 => col_buf_0_val_0_0_reg_2708(2),
      I4 => tmp_3_reg_2522,
      I5 => col_buf_0_val_1_0_reg_2721(2),
      O => src_kernel_win_0_va_7_fu_1456_p3(2)
    );
\src_kernel_win_0_va_7_reg_2811[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB8FFFF8B880000"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(3),
      I1 => row_assign_9_0_1_t_reg_2558(1),
      I2 => row_assign_9_0_1_t_reg_2558(0),
      I3 => col_buf_0_val_0_0_reg_2708(3),
      I4 => tmp_3_reg_2522,
      I5 => col_buf_0_val_1_0_reg_2721(3),
      O => src_kernel_win_0_va_7_fu_1456_p3(3)
    );
\src_kernel_win_0_va_7_reg_2811[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB8FFFF8B880000"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(4),
      I1 => row_assign_9_0_1_t_reg_2558(1),
      I2 => row_assign_9_0_1_t_reg_2558(0),
      I3 => col_buf_0_val_0_0_reg_2708(4),
      I4 => tmp_3_reg_2522,
      I5 => col_buf_0_val_1_0_reg_2721(4),
      O => src_kernel_win_0_va_7_fu_1456_p3(4)
    );
\src_kernel_win_0_va_7_reg_2811[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB8FFFF8B880000"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(5),
      I1 => row_assign_9_0_1_t_reg_2558(1),
      I2 => row_assign_9_0_1_t_reg_2558(0),
      I3 => col_buf_0_val_0_0_reg_2708(5),
      I4 => tmp_3_reg_2522,
      I5 => col_buf_0_val_1_0_reg_2721(5),
      O => src_kernel_win_0_va_7_fu_1456_p3(5)
    );
\src_kernel_win_0_va_7_reg_2811[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB8FFFF8B880000"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(6),
      I1 => row_assign_9_0_1_t_reg_2558(1),
      I2 => row_assign_9_0_1_t_reg_2558(0),
      I3 => col_buf_0_val_0_0_reg_2708(6),
      I4 => tmp_3_reg_2522,
      I5 => col_buf_0_val_1_0_reg_2721(6),
      O => src_kernel_win_0_va_7_fu_1456_p3(6)
    );
\src_kernel_win_0_va_7_reg_2811[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB8FFFF8B880000"
    )
        port map (
      I0 => col_buf_0_val_2_0_reg_2729(7),
      I1 => row_assign_9_0_1_t_reg_2558(1),
      I2 => row_assign_9_0_1_t_reg_2558(0),
      I3 => col_buf_0_val_0_0_reg_2708(7),
      I4 => tmp_3_reg_2522,
      I5 => col_buf_0_val_1_0_reg_2721(7),
      O => src_kernel_win_0_va_7_fu_1456_p3(7)
    );
\src_kernel_win_0_va_7_reg_2811_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => src_kernel_win_0_va_7_fu_1456_p3(0),
      Q => src_kernel_win_0_va_7_reg_2811(0),
      R => '0'
    );
\src_kernel_win_0_va_7_reg_2811_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => src_kernel_win_0_va_7_fu_1456_p3(1),
      Q => src_kernel_win_0_va_7_reg_2811(1),
      R => '0'
    );
\src_kernel_win_0_va_7_reg_2811_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => src_kernel_win_0_va_7_fu_1456_p3(2),
      Q => src_kernel_win_0_va_7_reg_2811(2),
      R => '0'
    );
\src_kernel_win_0_va_7_reg_2811_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => src_kernel_win_0_va_7_fu_1456_p3(3),
      Q => src_kernel_win_0_va_7_reg_2811(3),
      R => '0'
    );
\src_kernel_win_0_va_7_reg_2811_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => src_kernel_win_0_va_7_fu_1456_p3(4),
      Q => src_kernel_win_0_va_7_reg_2811(4),
      R => '0'
    );
\src_kernel_win_0_va_7_reg_2811_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => src_kernel_win_0_va_7_fu_1456_p3(5),
      Q => src_kernel_win_0_va_7_reg_2811(5),
      R => '0'
    );
\src_kernel_win_0_va_7_reg_2811_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => src_kernel_win_0_va_7_fu_1456_p3(6),
      Q => src_kernel_win_0_va_7_reg_2811(6),
      R => '0'
    );
\src_kernel_win_0_va_7_reg_2811_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_block_pp0_stage0_subdone2_in,
      D => src_kernel_win_0_va_7_fu_1456_p3(7),
      Q => src_kernel_win_0_va_7_reg_2811(7),
      R => '0'
    );
\src_kernel_win_0_va_fu_246_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_6_fu_1442_p3(0),
      Q => src_kernel_win_0_va_fu_246(0),
      R => '0'
    );
\src_kernel_win_0_va_fu_246_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_6_fu_1442_p3(1),
      Q => src_kernel_win_0_va_fu_246(1),
      R => '0'
    );
\src_kernel_win_0_va_fu_246_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_6_fu_1442_p3(2),
      Q => src_kernel_win_0_va_fu_246(2),
      R => '0'
    );
\src_kernel_win_0_va_fu_246_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_6_fu_1442_p3(3),
      Q => src_kernel_win_0_va_fu_246(3),
      R => '0'
    );
\src_kernel_win_0_va_fu_246_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_6_fu_1442_p3(4),
      Q => src_kernel_win_0_va_fu_246(4),
      R => '0'
    );
\src_kernel_win_0_va_fu_246_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_6_fu_1442_p3(5),
      Q => src_kernel_win_0_va_fu_246(5),
      R => '0'
    );
\src_kernel_win_0_va_fu_246_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_6_fu_1442_p3(6),
      Q => src_kernel_win_0_va_fu_246(6),
      R => '0'
    );
\src_kernel_win_0_va_fu_246_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => src_kernel_win_0_va_1_fu_2500,
      D => src_kernel_win_0_va_6_fu_1442_p3(7),
      Q => src_kernel_win_0_va_fu_246(7),
      R => '0'
    );
\start_once_reg_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5444"
    )
        port map (
      I0 => \^ap_cs_fsm_reg[0]_0\,
      I1 => start_once_reg_reg_0,
      I2 => start_for_CvtColor_1_U0_full_n,
      I3 => Sobel_U0_ap_start,
      O => start_once_reg_reg
    );
\t_V_2_reg_577[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F700FF00"
    )
        port map (
      I0 => ap_block_pp0_stage0_subdone2_in,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => exitcond389_i_fu_936_p2,
      I3 => ap_CS_fsm_state4,
      I4 => ap_enable_reg_pp0_iter0,
      O => t_V_2_reg_577
    );
\t_V_2_reg_577[10]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => ap_block_pp0_stage0_subdone2_in,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => exitcond389_i_fu_936_p2,
      I3 => ap_enable_reg_pp0_iter0,
      O => t_V_2_reg_5770
    );
\t_V_2_reg_577[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(10),
      I1 => \t_V_2_reg_577_reg__0\(9),
      I2 => \t_V_2_reg_577_reg__0\(7),
      I3 => \t_V_2_reg_577[10]_i_4_n_2\,
      I4 => \t_V_2_reg_577_reg__0\(6),
      I5 => \t_V_2_reg_577_reg__0\(8),
      O => j_V_fu_942_p2(10)
    );
\t_V_2_reg_577[10]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(4),
      I1 => \t_V_2_reg_577_reg__0\(2),
      I2 => \t_V_2_reg_577_reg__0__0\(0),
      I3 => \t_V_2_reg_577_reg__0\(1),
      I4 => \t_V_2_reg_577_reg__0\(3),
      I5 => \t_V_2_reg_577_reg__0\(5),
      O => \t_V_2_reg_577[10]_i_4_n_2\
    );
\t_V_2_reg_577[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0__0\(0),
      I1 => \t_V_2_reg_577_reg__0\(1),
      O => j_V_fu_942_p2(1)
    );
\t_V_2_reg_577[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(2),
      I1 => \t_V_2_reg_577_reg__0\(1),
      I2 => \t_V_2_reg_577_reg__0__0\(0),
      O => j_V_fu_942_p2(2)
    );
\t_V_2_reg_577[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(3),
      I1 => \t_V_2_reg_577_reg__0\(2),
      I2 => \t_V_2_reg_577_reg__0__0\(0),
      I3 => \t_V_2_reg_577_reg__0\(1),
      O => j_V_fu_942_p2(3)
    );
\t_V_2_reg_577[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(4),
      I1 => \t_V_2_reg_577_reg__0\(3),
      I2 => \t_V_2_reg_577_reg__0\(1),
      I3 => \t_V_2_reg_577_reg__0__0\(0),
      I4 => \t_V_2_reg_577_reg__0\(2),
      O => j_V_fu_942_p2(4)
    );
\t_V_2_reg_577[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(5),
      I1 => \t_V_2_reg_577_reg__0\(4),
      I2 => \t_V_2_reg_577_reg__0\(2),
      I3 => \t_V_2_reg_577_reg__0__0\(0),
      I4 => \t_V_2_reg_577_reg__0\(1),
      I5 => \t_V_2_reg_577_reg__0\(3),
      O => j_V_fu_942_p2(5)
    );
\t_V_2_reg_577[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(6),
      I1 => \t_V_2_reg_577[10]_i_4_n_2\,
      O => j_V_fu_942_p2(6)
    );
\t_V_2_reg_577[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(7),
      I1 => \t_V_2_reg_577_reg__0\(6),
      I2 => \t_V_2_reg_577[10]_i_4_n_2\,
      O => j_V_fu_942_p2(7)
    );
\t_V_2_reg_577[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(8),
      I1 => \t_V_2_reg_577_reg__0\(7),
      I2 => \t_V_2_reg_577[10]_i_4_n_2\,
      I3 => \t_V_2_reg_577_reg__0\(6),
      O => j_V_fu_942_p2(8)
    );
\t_V_2_reg_577[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(9),
      I1 => \t_V_2_reg_577_reg__0\(8),
      I2 => \t_V_2_reg_577_reg__0\(6),
      I3 => \t_V_2_reg_577[10]_i_4_n_2\,
      I4 => \t_V_2_reg_577_reg__0\(7),
      O => j_V_fu_942_p2(9)
    );
\t_V_2_reg_577_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_2_reg_5770,
      D => \p_assign_2_reg_2622[0]_i_1_n_2\,
      Q => \t_V_2_reg_577_reg__0__0\(0),
      R => t_V_2_reg_577
    );
\t_V_2_reg_577_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_2_reg_5770,
      D => j_V_fu_942_p2(10),
      Q => \t_V_2_reg_577_reg__0\(10),
      R => t_V_2_reg_577
    );
\t_V_2_reg_577_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_2_reg_5770,
      D => j_V_fu_942_p2(1),
      Q => \t_V_2_reg_577_reg__0\(1),
      R => t_V_2_reg_577
    );
\t_V_2_reg_577_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_2_reg_5770,
      D => j_V_fu_942_p2(2),
      Q => \t_V_2_reg_577_reg__0\(2),
      R => t_V_2_reg_577
    );
\t_V_2_reg_577_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_2_reg_5770,
      D => j_V_fu_942_p2(3),
      Q => \t_V_2_reg_577_reg__0\(3),
      R => t_V_2_reg_577
    );
\t_V_2_reg_577_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_2_reg_5770,
      D => j_V_fu_942_p2(4),
      Q => \t_V_2_reg_577_reg__0\(4),
      R => t_V_2_reg_577
    );
\t_V_2_reg_577_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_2_reg_5770,
      D => j_V_fu_942_p2(5),
      Q => \t_V_2_reg_577_reg__0\(5),
      R => t_V_2_reg_577
    );
\t_V_2_reg_577_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_2_reg_5770,
      D => j_V_fu_942_p2(6),
      Q => \t_V_2_reg_577_reg__0\(6),
      R => t_V_2_reg_577
    );
\t_V_2_reg_577_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_2_reg_5770,
      D => j_V_fu_942_p2(7),
      Q => \t_V_2_reg_577_reg__0\(7),
      R => t_V_2_reg_577
    );
\t_V_2_reg_577_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_2_reg_5770,
      D => j_V_fu_942_p2(8),
      Q => \t_V_2_reg_577_reg__0\(8),
      R => t_V_2_reg_577
    );
\t_V_2_reg_577_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => t_V_2_reg_5770,
      D => j_V_fu_942_p2(9),
      Q => \t_V_2_reg_577_reg__0\(9),
      R => t_V_2_reg_577
    );
\t_V_reg_566[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => ap_CS_fsm_state2,
      I1 => tmp_5_reg_555(0),
      I2 => tmp_5_reg_555(1),
      O => ap_NS_fsm1
    );
\t_V_reg_566_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state11,
      D => i_V_reg_2495(0),
      Q => t_V_reg_566(0),
      R => ap_NS_fsm1
    );
\t_V_reg_566_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state11,
      D => i_V_reg_2495(1),
      Q => t_V_reg_566(1),
      R => ap_NS_fsm1
    );
\t_V_reg_566_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state11,
      D => i_V_reg_2495(2),
      Q => t_V_reg_566(2),
      R => ap_NS_fsm1
    );
\t_V_reg_566_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state11,
      D => i_V_reg_2495(3),
      Q => t_V_reg_566(3),
      R => ap_NS_fsm1
    );
\t_V_reg_566_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state11,
      D => i_V_reg_2495(4),
      Q => t_V_reg_566(4),
      R => ap_NS_fsm1
    );
\t_V_reg_566_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state11,
      D => i_V_reg_2495(5),
      Q => t_V_reg_566(5),
      R => ap_NS_fsm1
    );
\t_V_reg_566_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state11,
      D => i_V_reg_2495(6),
      Q => t_V_reg_566(6),
      R => ap_NS_fsm1
    );
\t_V_reg_566_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state11,
      D => i_V_reg_2495(7),
      Q => t_V_reg_566(7),
      R => ap_NS_fsm1
    );
\t_V_reg_566_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state11,
      D => i_V_reg_2495(8),
      Q => t_V_reg_566(8),
      R => ap_NS_fsm1
    );
\t_V_reg_566_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state11,
      D => i_V_reg_2495(9),
      Q => t_V_reg_566(9),
      R => ap_NS_fsm1
    );
tmp57_fu_1547_p2_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => tmp57_fu_1547_p2_carry_n_2,
      CO(2) => tmp57_fu_1547_p2_carry_n_3,
      CO(1) => tmp57_fu_1547_p2_carry_n_4,
      CO(0) => tmp57_fu_1547_p2_carry_n_5,
      CYINIT => '0',
      DI(3) => r_V_7_0_1_fu_1511_p2(3),
      DI(2) => tmp57_fu_1547_p2_carry_i_1_n_2,
      DI(1) => src_kernel_win_0_va_3_fu_258(0),
      DI(0) => '0',
      O(3 downto 0) => tmp57_fu_1547_p2(3 downto 0),
      S(3) => tmp57_fu_1547_p2_carry_i_2_n_2,
      S(2) => tmp57_fu_1547_p2_carry_i_3_n_2,
      S(1) => tmp57_fu_1547_p2_carry_i_4_n_2,
      S(0) => src_kernel_win_0_va_1_fu_250(0)
    );
\tmp57_fu_1547_p2_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => tmp57_fu_1547_p2_carry_n_2,
      CO(3) => \tmp57_fu_1547_p2_carry__0_n_2\,
      CO(2) => \tmp57_fu_1547_p2_carry__0_n_3\,
      CO(1) => \tmp57_fu_1547_p2_carry__0_n_4\,
      CO(0) => \tmp57_fu_1547_p2_carry__0_n_5\,
      CYINIT => '0',
      DI(3) => \tmp57_fu_1547_p2_carry__0_i_1_n_2\,
      DI(2 downto 0) => r_V_7_0_1_fu_1511_p2(6 downto 4),
      O(3 downto 0) => tmp57_fu_1547_p2(7 downto 4),
      S(3) => \tmp57_fu_1547_p2_carry__0_i_2_n_2\,
      S(2) => \tmp57_fu_1547_p2_carry__0_i_3_n_2\,
      S(1) => \tmp57_fu_1547_p2_carry__0_i_4_n_2\,
      S(0) => \tmp57_fu_1547_p2_carry__0_i_5_n_2\
    );
\tmp57_fu_1547_p2_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tmp_54_reg_2827[7]_i_2_n_2\,
      I1 => src_kernel_win_0_va_3_fu_258(6),
      O => \tmp57_fu_1547_p2_carry__0_i_1_n_2\
    );
\tmp57_fu_1547_p2_carry__0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"56A9A956"
    )
        port map (
      I0 => src_kernel_win_0_va_1_fu_250(7),
      I1 => \tmp_56_reg_2832[7]_i_2_n_2\,
      I2 => src_kernel_win_0_va_1_fu_250(6),
      I3 => src_kernel_win_0_va_3_fu_258(6),
      I4 => \tmp_54_reg_2827[7]_i_2_n_2\,
      O => \tmp57_fu_1547_p2_carry__0_i_2_n_2\
    );
\tmp57_fu_1547_p2_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => src_kernel_win_0_va_1_fu_250(6),
      I1 => \tmp_56_reg_2832[7]_i_2_n_2\,
      I2 => r_V_7_0_1_fu_1511_p2(6),
      O => \tmp57_fu_1547_p2_carry__0_i_3_n_2\
    );
\tmp57_fu_1547_p2_carry__0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999999999999996"
    )
        port map (
      I0 => tmp_160_0_2_cast_fu_1539_p1(5),
      I1 => src_kernel_win_0_va_3_fu_258(4),
      I2 => src_kernel_win_0_va_3_fu_258(2),
      I3 => src_kernel_win_0_va_3_fu_258(0),
      I4 => src_kernel_win_0_va_3_fu_258(1),
      I5 => src_kernel_win_0_va_3_fu_258(3),
      O => \tmp57_fu_1547_p2_carry__0_i_4_n_2\
    );
\tmp57_fu_1547_p2_carry__0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA955555556"
    )
        port map (
      I0 => src_kernel_win_0_va_1_fu_250(4),
      I1 => src_kernel_win_0_va_1_fu_250(2),
      I2 => src_kernel_win_0_va_1_fu_250(0),
      I3 => src_kernel_win_0_va_1_fu_250(1),
      I4 => src_kernel_win_0_va_1_fu_250(3),
      I5 => r_V_7_0_1_fu_1511_p2(4),
      O => \tmp57_fu_1547_p2_carry__0_i_5_n_2\
    );
\tmp57_fu_1547_p2_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tmp57_fu_1547_p2_carry__0_n_2\,
      CO(3 downto 2) => \NLW_tmp57_fu_1547_p2_carry__1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \tmp57_fu_1547_p2_carry__1_n_4\,
      CO(0) => \tmp57_fu_1547_p2_carry__1_n_5\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \tmp57_fu_1547_p2_carry__1_i_1_n_2\,
      DI(0) => \tmp57_fu_1547_p2_carry__1_i_2_n_2\,
      O(3) => \NLW_tmp57_fu_1547_p2_carry__1_O_UNCONNECTED\(3),
      O(2 downto 0) => tmp57_fu_1547_p2(10 downto 8),
      S(3 downto 2) => B"01",
      S(1) => \tmp57_fu_1547_p2_carry__1_i_3_n_2\,
      S(0) => \tmp57_fu_1547_p2_carry__1_i_4_n_2\
    );
\tmp57_fu_1547_p2_carry__1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1E"
    )
        port map (
      I0 => src_kernel_win_0_va_3_fu_258(6),
      I1 => \tmp_54_reg_2827[7]_i_2_n_2\,
      I2 => src_kernel_win_0_va_3_fu_258(7),
      O => \tmp57_fu_1547_p2_carry__1_i_1_n_2\
    );
\tmp57_fu_1547_p2_carry__1_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A9"
    )
        port map (
      I0 => src_kernel_win_0_va_3_fu_258(7),
      I1 => \tmp_54_reg_2827[7]_i_2_n_2\,
      I2 => src_kernel_win_0_va_3_fu_258(6),
      O => \tmp57_fu_1547_p2_carry__1_i_2_n_2\
    );
\tmp57_fu_1547_p2_carry__1_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1F"
    )
        port map (
      I0 => \tmp_54_reg_2827[7]_i_2_n_2\,
      I1 => src_kernel_win_0_va_3_fu_258(6),
      I2 => src_kernel_win_0_va_3_fu_258(7),
      O => \tmp57_fu_1547_p2_carry__1_i_3_n_2\
    );
\tmp57_fu_1547_p2_carry__1_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A9A9A9A9A9A9A956"
    )
        port map (
      I0 => src_kernel_win_0_va_3_fu_258(7),
      I1 => \tmp_54_reg_2827[7]_i_2_n_2\,
      I2 => src_kernel_win_0_va_3_fu_258(6),
      I3 => src_kernel_win_0_va_1_fu_250(7),
      I4 => \tmp_56_reg_2832[7]_i_2_n_2\,
      I5 => src_kernel_win_0_va_1_fu_250(6),
      O => \tmp57_fu_1547_p2_carry__1_i_4_n_2\
    );
tmp57_fu_1547_p2_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => src_kernel_win_0_va_3_fu_258(0),
      I1 => src_kernel_win_0_va_3_fu_258(1),
      O => tmp57_fu_1547_p2_carry_i_1_n_2
    );
tmp57_fu_1547_p2_carry_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA95556"
    )
        port map (
      I0 => src_kernel_win_0_va_1_fu_250(3),
      I1 => src_kernel_win_0_va_1_fu_250(1),
      I2 => src_kernel_win_0_va_1_fu_250(0),
      I3 => src_kernel_win_0_va_1_fu_250(2),
      I4 => r_V_7_0_1_fu_1511_p2(3),
      O => tmp57_fu_1547_p2_carry_i_2_n_2
    );
tmp57_fu_1547_p2_carry_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"56A9A956"
    )
        port map (
      I0 => src_kernel_win_0_va_1_fu_250(2),
      I1 => src_kernel_win_0_va_1_fu_250(0),
      I2 => src_kernel_win_0_va_1_fu_250(1),
      I3 => src_kernel_win_0_va_3_fu_258(1),
      I4 => src_kernel_win_0_va_3_fu_258(0),
      O => tmp57_fu_1547_p2_carry_i_3_n_2
    );
tmp57_fu_1547_p2_carry_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => src_kernel_win_0_va_1_fu_250(1),
      I1 => src_kernel_win_0_va_1_fu_250(0),
      I2 => src_kernel_win_0_va_3_fu_258(0),
      O => tmp57_fu_1547_p2_carry_i_4_n_2
    );
\tmp57_reg_2837_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => tmp57_fu_1547_p2(0),
      Q => tmp57_reg_2837(0),
      R => '0'
    );
\tmp57_reg_2837_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => tmp57_fu_1547_p2(10),
      Q => tmp57_reg_2837(10),
      R => '0'
    );
\tmp57_reg_2837_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => tmp57_fu_1547_p2(1),
      Q => tmp57_reg_2837(1),
      R => '0'
    );
\tmp57_reg_2837_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => tmp57_fu_1547_p2(2),
      Q => tmp57_reg_2837(2),
      R => '0'
    );
\tmp57_reg_2837_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => tmp57_fu_1547_p2(3),
      Q => tmp57_reg_2837(3),
      R => '0'
    );
\tmp57_reg_2837_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => tmp57_fu_1547_p2(4),
      Q => tmp57_reg_2837(4),
      R => '0'
    );
\tmp57_reg_2837_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => tmp57_fu_1547_p2(5),
      Q => tmp57_reg_2837(5),
      R => '0'
    );
\tmp57_reg_2837_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => tmp57_fu_1547_p2(6),
      Q => tmp57_reg_2837(6),
      R => '0'
    );
\tmp57_reg_2837_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => tmp57_fu_1547_p2(7),
      Q => tmp57_reg_2837(7),
      R => '0'
    );
\tmp57_reg_2837_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => tmp57_fu_1547_p2(8),
      Q => tmp57_reg_2837(8),
      R => '0'
    );
\tmp57_reg_2837_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => tmp57_fu_1547_p2(9),
      Q => tmp57_reg_2837(9),
      R => '0'
    );
\tmp_116_0_1_reg_2518[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F870"
    )
        port map (
      I0 => ap_CS_fsm_state3,
      I1 => \ap_CS_fsm[3]_i_2__0_n_2\,
      I2 => \tmp_116_0_1_reg_2518_reg_n_2_[0]\,
      I3 => \tmp_4_reg_2535[10]_i_1_n_2\,
      O => \tmp_116_0_1_reg_2518[0]_i_1_n_2\
    );
\tmp_116_0_1_reg_2518_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_116_0_1_reg_2518[0]_i_1_n_2\,
      Q => \tmp_116_0_1_reg_2518_reg_n_2_[0]\,
      R => '0'
    );
\tmp_1_reg_2500[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"15FF15FF15FF55FF"
    )
        port map (
      I0 => t_V_reg_566(8),
      I1 => t_V_reg_566(7),
      I2 => t_V_reg_566(6),
      I3 => t_V_reg_566(9),
      I4 => t_V_reg_566(5),
      I5 => t_V_reg_566(4),
      O => tmp_1_fu_637_p2
    );
\tmp_1_reg_2500_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => tmp_1_fu_637_p2,
      Q => tmp_1_reg_2500,
      R => '0'
    );
\tmp_2_reg_2514[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"70FA707070707070"
    )
        port map (
      I0 => ap_CS_fsm_state3,
      I1 => \ap_CS_fsm[3]_i_2__0_n_2\,
      I2 => \tmp_2_reg_2514_reg_n_2_[0]\,
      I3 => t_V_reg_566(1),
      I4 => \icmp_reg_2509[0]_i_2_n_2\,
      I5 => t_V_reg_566(0),
      O => \tmp_2_reg_2514[0]_i_1_n_2\
    );
\tmp_2_reg_2514_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_2_reg_2514[0]_i_1_n_2\,
      Q => \tmp_2_reg_2514_reg_n_2_[0]\,
      R => '0'
    );
tmp_31_fu_984_p2_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 2) => NLW_tmp_31_fu_984_p2_carry_CO_UNCONNECTED(3 downto 2),
      CO(1) => tmp_31_fu_984_p2,
      CO(0) => tmp_31_fu_984_p2_carry_n_5,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => tmp_31_fu_984_p2_carry_i_1_n_2,
      DI(0) => tmp_31_fu_984_p2_carry_i_2_n_2,
      O(3 downto 0) => NLW_tmp_31_fu_984_p2_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => tmp_31_fu_984_p2_carry_i_3_n_2,
      S(0) => tmp_31_fu_984_p2_carry_i_4_n_2
    );
tmp_31_fu_984_p2_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555555555555557"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(10),
      I1 => \t_V_2_reg_577_reg__0\(8),
      I2 => \t_V_2_reg_577_reg__0\(9),
      I3 => \t_V_2_reg_577_reg__0\(7),
      I4 => \t_V_2_reg_577_reg__0\(6),
      I5 => \ImagLoc_x_reg_2592[10]_i_2_n_2\,
      O => tmp_31_fu_984_p2_carry_i_1_n_2
    );
tmp_31_fu_984_p2_carry_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00015554"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(9),
      I1 => \t_V_2_reg_577_reg__0\(7),
      I2 => \t_V_2_reg_577_reg__0\(6),
      I3 => \ImagLoc_x_reg_2592[10]_i_2_n_2\,
      I4 => \t_V_2_reg_577_reg__0\(8),
      O => tmp_31_fu_984_p2_carry_i_2_n_2
    );
tmp_31_fu_984_p2_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAAA8"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(10),
      I1 => \t_V_2_reg_577_reg__0\(8),
      I2 => \t_V_2_reg_577_reg__0\(9),
      I3 => \t_V_2_reg_577_reg__0\(7),
      I4 => \t_V_2_reg_577_reg__0\(6),
      I5 => \ImagLoc_x_reg_2592[10]_i_2_n_2\,
      O => tmp_31_fu_984_p2_carry_i_3_n_2
    );
tmp_31_fu_984_p2_carry_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FE00"
    )
        port map (
      I0 => \ImagLoc_x_reg_2592[10]_i_2_n_2\,
      I1 => \t_V_2_reg_577_reg__0\(6),
      I2 => \t_V_2_reg_577_reg__0\(7),
      I3 => \t_V_2_reg_577_reg__0\(8),
      I4 => \t_V_2_reg_577_reg__0\(9),
      O => tmp_31_fu_984_p2_carry_i_4_n_2
    );
\tmp_31_reg_2602_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => tmp_31_fu_984_p2,
      Q => tmp_31_reg_2602,
      R => '0'
    );
\tmp_32_reg_2572[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F40"
    )
        port map (
      I0 => row_assign_9_0_1_t_reg_2558(0),
      I1 => ap_CS_fsm_state4,
      I2 => tmp_3_reg_2522,
      I3 => tmp_32_reg_2572(0),
      O => \tmp_32_reg_2572[0]_i_1_n_2\
    );
\tmp_32_reg_2572[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => tmp_32_fu_896_p2(1),
      I1 => ap_CS_fsm_state4,
      I2 => tmp_3_reg_2522,
      I3 => tmp_32_reg_2572(1),
      O => \tmp_32_reg_2572[1]_i_1_n_2\
    );
\tmp_32_reg_2572[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0059FF6A"
    )
        port map (
      I0 => \tmp_32_reg_2572[1]_i_3_n_2\,
      I1 => \tmp_4_reg_2535_reg_n_2_[10]\,
      I2 => p_assign_7_reg_2553(1),
      I3 => or_cond_i425_i_reg_2542,
      I4 => tmp_4_reg_2535(1),
      O => tmp_32_fu_896_p2(1)
    );
\tmp_32_reg_2572[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFE2EEEEEE"
    )
        port map (
      I0 => \tmp_32_reg_2572[1]_i_4_n_2\,
      I1 => \tmp_4_reg_2535_reg_n_2_[10]\,
      I2 => \icmp_reg_2509_reg_n_2_[0]\,
      I3 => p_assign_7_reg_2553(9),
      I4 => \tmp_32_reg_2572[1]_i_5_n_2\,
      I5 => row_assign_9_0_1_t_reg_2558(0),
      O => \tmp_32_reg_2572[1]_i_3_n_2\
    );
\tmp_32_reg_2572[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"15151555FFFFFFFF"
    )
        port map (
      I0 => \tmp_4_reg_2535_reg_n_2_[8]\,
      I1 => \tmp_4_reg_2535_reg_n_2_[7]\,
      I2 => \tmp_4_reg_2535_reg_n_2_[6]\,
      I3 => \tmp_4_reg_2535_reg_n_2_[5]\,
      I4 => \tmp_4_reg_2535_reg_n_2_[4]\,
      I5 => \tmp_4_reg_2535_reg_n_2_[9]\,
      O => \tmp_32_reg_2572[1]_i_4_n_2\
    );
\tmp_32_reg_2572[1]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EAEAEAAA"
    )
        port map (
      I0 => p_assign_7_reg_2553(8),
      I1 => p_assign_7_reg_2553(6),
      I2 => p_assign_7_reg_2553(7),
      I3 => p_assign_7_reg_2553(4),
      I4 => p_assign_7_reg_2553(5),
      O => \tmp_32_reg_2572[1]_i_5_n_2\
    );
\tmp_32_reg_2572_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_32_reg_2572[0]_i_1_n_2\,
      Q => tmp_32_reg_2572(0),
      R => '0'
    );
\tmp_32_reg_2572_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_32_reg_2572[1]_i_1_n_2\,
      Q => tmp_32_reg_2572(1),
      R => '0'
    );
tmp_33_fu_1022_p2_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 2) => NLW_tmp_33_fu_1022_p2_carry_CO_UNCONNECTED(3 downto 2),
      CO(1) => tmp_33_fu_1022_p2,
      CO(0) => tmp_33_fu_1022_p2_carry_n_5,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => tmp_31_fu_984_p2_carry_i_1_n_2,
      DI(0) => tmp_33_fu_1022_p2_carry_i_1_n_2,
      O(3 downto 0) => NLW_tmp_33_fu_1022_p2_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => tmp_33_fu_1022_p2_carry_i_2_n_2,
      S(0) => tmp_33_fu_1022_p2_carry_i_3_n_2
    );
tmp_33_fu_1022_p2_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00005556"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(8),
      I1 => \ImagLoc_x_reg_2592[10]_i_2_n_2\,
      I2 => \t_V_2_reg_577_reg__0\(6),
      I3 => \t_V_2_reg_577_reg__0\(7),
      I4 => \t_V_2_reg_577_reg__0\(9),
      I5 => \or_cond_i_reg_2640[0]_i_2_n_2\,
      O => tmp_33_fu_1022_p2_carry_i_1_n_2
    );
tmp_33_fu_1022_p2_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAAA8"
    )
        port map (
      I0 => \t_V_2_reg_577_reg__0\(10),
      I1 => \t_V_2_reg_577_reg__0\(8),
      I2 => \t_V_2_reg_577_reg__0\(9),
      I3 => \t_V_2_reg_577_reg__0\(7),
      I4 => \t_V_2_reg_577_reg__0\(6),
      I5 => \ImagLoc_x_reg_2592[10]_i_2_n_2\,
      O => tmp_33_fu_1022_p2_carry_i_2_n_2
    );
tmp_33_fu_1022_p2_carry_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FE00"
    )
        port map (
      I0 => \ImagLoc_x_reg_2592[10]_i_2_n_2\,
      I1 => \t_V_2_reg_577_reg__0\(6),
      I2 => \t_V_2_reg_577_reg__0\(7),
      I3 => \t_V_2_reg_577_reg__0\(8),
      I4 => \t_V_2_reg_577_reg__0\(9),
      O => tmp_33_fu_1022_p2_carry_i_3_n_2
    );
\tmp_33_reg_2617_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => tmp_33_fu_1022_p2,
      Q => tmp_33_reg_2617,
      R => '0'
    );
\tmp_3_reg_2522[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000A2AA0424"
    )
        port map (
      I0 => t_V_reg_566(9),
      I1 => \tmp_3_reg_2522[0]_i_2_n_2\,
      I2 => t_V_reg_566(7),
      I3 => \tmp_3_reg_2522[0]_i_3_n_2\,
      I4 => t_V_reg_566(8),
      I5 => \tmp_4_reg_2535[10]_i_1_n_2\,
      O => tmp_3_fu_677_p2
    );
\tmp_3_reg_2522[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000010"
    )
        port map (
      I0 => t_V_reg_566(1),
      I1 => t_V_reg_566(0),
      I2 => \tmp_4_reg_2535[6]_i_2_n_2\,
      I3 => t_V_reg_566(5),
      I4 => t_V_reg_566(4),
      I5 => t_V_reg_566(6),
      O => \tmp_3_reg_2522[0]_i_2_n_2\
    );
\tmp_3_reg_2522[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55555555777E7777"
    )
        port map (
      I0 => t_V_reg_566(6),
      I1 => t_V_reg_566(4),
      I2 => t_V_reg_566(1),
      I3 => t_V_reg_566(0),
      I4 => \tmp_4_reg_2535[6]_i_2_n_2\,
      I5 => t_V_reg_566(5),
      O => \tmp_3_reg_2522[0]_i_3_n_2\
    );
\tmp_3_reg_2522_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => tmp_3_fu_677_p2,
      Q => tmp_3_reg_2522,
      R => '0'
    );
\tmp_47_reg_2597[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \ImagLoc_x_reg_2592[10]_i_2_n_2\,
      I1 => \t_V_2_reg_577_reg__0\(6),
      I2 => \t_V_2_reg_577_reg__0\(7),
      I3 => \t_V_2_reg_577_reg__0\(8),
      I4 => \t_V_2_reg_577_reg__0\(9),
      I5 => \t_V_2_reg_577_reg__0\(10),
      O => ImagLoc_x_fu_964_p2(11)
    );
\tmp_47_reg_2597_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ImagLoc_x_reg_25920,
      D => ImagLoc_x_fu_964_p2(11),
      Q => tmp_47_reg_2597,
      R => '0'
    );
\tmp_49_reg_2644_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_addr_reg_26490,
      D => p_assign_2_reg_2622(0),
      Q => tmp_49_reg_2644(0),
      R => '0'
    );
\tmp_49_reg_2644_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => k_buf_0_val_3_addr_reg_26490,
      D => k_buf_0_val_5_U_n_19,
      Q => tmp_49_reg_2644(1),
      R => '0'
    );
\tmp_4_reg_2535[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => t_V_reg_566(0),
      I1 => \icmp_reg_2509[0]_i_2_n_2\,
      I2 => t_V_reg_566(1),
      O => \tmp_4_reg_2535[10]_i_1_n_2\
    );
\tmp_4_reg_2535[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => t_V_reg_566(0),
      I1 => t_V_reg_566(1),
      O => \tmp_4_reg_2535[1]_i_1_n_2\
    );
\tmp_4_reg_2535[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAAAA9"
    )
        port map (
      I0 => t_V_reg_566(4),
      I1 => t_V_reg_566(1),
      I2 => t_V_reg_566(0),
      I3 => t_V_reg_566(2),
      I4 => t_V_reg_566(3),
      O => \tmp_4_reg_2535[4]_i_1_n_2\
    );
\tmp_4_reg_2535[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAAA9"
    )
        port map (
      I0 => t_V_reg_566(5),
      I1 => t_V_reg_566(4),
      I2 => t_V_reg_566(3),
      I3 => t_V_reg_566(2),
      I4 => t_V_reg_566(0),
      I5 => t_V_reg_566(1),
      O => \tmp_4_reg_2535[5]_i_1_n_2\
    );
\tmp_4_reg_2535[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEFF00000100"
    )
        port map (
      I0 => t_V_reg_566(0),
      I1 => t_V_reg_566(5),
      I2 => t_V_reg_566(4),
      I3 => \tmp_4_reg_2535[6]_i_2_n_2\,
      I4 => t_V_reg_566(1),
      I5 => t_V_reg_566(6),
      O => \tmp_4_reg_2535[6]_i_1_n_2\
    );
\tmp_4_reg_2535[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => t_V_reg_566(2),
      I1 => t_V_reg_566(3),
      O => \tmp_4_reg_2535[6]_i_2_n_2\
    );
\tmp_4_reg_2535[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA9AAAA"
    )
        port map (
      I0 => t_V_reg_566(7),
      I1 => t_V_reg_566(6),
      I2 => t_V_reg_566(4),
      I3 => t_V_reg_566(5),
      I4 => \tmp_4_reg_2535[8]_i_2_n_2\,
      O => \tmp_4_reg_2535[7]_i_1_n_2\
    );
\tmp_4_reg_2535[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEFF00000100"
    )
        port map (
      I0 => t_V_reg_566(6),
      I1 => t_V_reg_566(4),
      I2 => t_V_reg_566(5),
      I3 => \tmp_4_reg_2535[8]_i_2_n_2\,
      I4 => t_V_reg_566(7),
      I5 => t_V_reg_566(8),
      O => \tmp_4_reg_2535[8]_i_1_n_2\
    );
\tmp_4_reg_2535[8]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => t_V_reg_566(3),
      I1 => t_V_reg_566(2),
      I2 => t_V_reg_566(0),
      I3 => t_V_reg_566(1),
      O => \tmp_4_reg_2535[8]_i_2_n_2\
    );
\tmp_4_reg_2535[9]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB04"
    )
        port map (
      I0 => t_V_reg_566(7),
      I1 => \tmp_3_reg_2522[0]_i_2_n_2\,
      I2 => t_V_reg_566(8),
      I3 => t_V_reg_566(9),
      O => \tmp_4_reg_2535[9]_i_1_n_2\
    );
\tmp_4_reg_2535_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \tmp_4_reg_2535[10]_i_1_n_2\,
      Q => \tmp_4_reg_2535_reg_n_2_[10]\,
      R => '0'
    );
\tmp_4_reg_2535_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \tmp_4_reg_2535[1]_i_1_n_2\,
      Q => tmp_4_reg_2535(1),
      R => '0'
    );
\tmp_4_reg_2535_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \tmp_4_reg_2535[4]_i_1_n_2\,
      Q => \tmp_4_reg_2535_reg_n_2_[4]\,
      R => '0'
    );
\tmp_4_reg_2535_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \tmp_4_reg_2535[5]_i_1_n_2\,
      Q => \tmp_4_reg_2535_reg_n_2_[5]\,
      R => '0'
    );
\tmp_4_reg_2535_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \tmp_4_reg_2535[6]_i_1_n_2\,
      Q => \tmp_4_reg_2535_reg_n_2_[6]\,
      R => '0'
    );
\tmp_4_reg_2535_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \tmp_4_reg_2535[7]_i_1_n_2\,
      Q => \tmp_4_reg_2535_reg_n_2_[7]\,
      R => '0'
    );
\tmp_4_reg_2535_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \tmp_4_reg_2535[8]_i_1_n_2\,
      Q => \tmp_4_reg_2535_reg_n_2_[8]\,
      R => '0'
    );
\tmp_4_reg_2535_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => \tmp_4_reg_2535[9]_i_1_n_2\,
      Q => \tmp_4_reg_2535_reg_n_2_[9]\,
      R => '0'
    );
\tmp_53_reg_2822_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => p_Val2_10_0_0_2_fu_1493_p2_carry_n_9,
      Q => tmp_53_reg_2822(0),
      R => '0'
    );
\tmp_53_reg_2822_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => p_Val2_10_0_0_2_fu_1493_p2_carry_n_8,
      Q => tmp_53_reg_2822(1),
      R => '0'
    );
\tmp_53_reg_2822_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => p_Val2_10_0_0_2_fu_1493_p2_carry_n_7,
      Q => tmp_53_reg_2822(2),
      R => '0'
    );
\tmp_53_reg_2822_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => p_Val2_10_0_0_2_fu_1493_p2_carry_n_6,
      Q => tmp_53_reg_2822(3),
      R => '0'
    );
\tmp_53_reg_2822_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => \p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_9\,
      Q => tmp_53_reg_2822(4),
      R => '0'
    );
\tmp_53_reg_2822_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => \p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_8\,
      Q => tmp_53_reg_2822(5),
      R => '0'
    );
\tmp_53_reg_2822_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => \p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_7\,
      Q => tmp_53_reg_2822(6),
      R => '0'
    );
\tmp_53_reg_2822_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => \p_Val2_10_0_0_2_fu_1493_p2_carry__0_n_6\,
      Q => tmp_53_reg_2822(7),
      R => '0'
    );
\tmp_54_reg_2827[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => src_kernel_win_0_va_3_fu_258(0),
      I1 => src_kernel_win_0_va_3_fu_258(1),
      O => r_V_7_0_1_fu_1511_p2(2)
    );
\tmp_54_reg_2827[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1E"
    )
        port map (
      I0 => src_kernel_win_0_va_3_fu_258(1),
      I1 => src_kernel_win_0_va_3_fu_258(0),
      I2 => src_kernel_win_0_va_3_fu_258(2),
      O => r_V_7_0_1_fu_1511_p2(3)
    );
\tmp_54_reg_2827[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"01FE"
    )
        port map (
      I0 => src_kernel_win_0_va_3_fu_258(2),
      I1 => src_kernel_win_0_va_3_fu_258(0),
      I2 => src_kernel_win_0_va_3_fu_258(1),
      I3 => src_kernel_win_0_va_3_fu_258(3),
      O => r_V_7_0_1_fu_1511_p2(4)
    );
\tmp_54_reg_2827[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFE"
    )
        port map (
      I0 => src_kernel_win_0_va_3_fu_258(3),
      I1 => src_kernel_win_0_va_3_fu_258(1),
      I2 => src_kernel_win_0_va_3_fu_258(0),
      I3 => src_kernel_win_0_va_3_fu_258(2),
      I4 => src_kernel_win_0_va_3_fu_258(4),
      O => r_V_7_0_1_fu_1511_p2(5)
    );
\tmp_54_reg_2827[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000001FFFFFFFE"
    )
        port map (
      I0 => src_kernel_win_0_va_3_fu_258(4),
      I1 => src_kernel_win_0_va_3_fu_258(2),
      I2 => src_kernel_win_0_va_3_fu_258(0),
      I3 => src_kernel_win_0_va_3_fu_258(1),
      I4 => src_kernel_win_0_va_3_fu_258(3),
      I5 => src_kernel_win_0_va_3_fu_258(5),
      O => r_V_7_0_1_fu_1511_p2(6)
    );
\tmp_54_reg_2827[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tmp_54_reg_2827[7]_i_2_n_2\,
      I1 => src_kernel_win_0_va_3_fu_258(6),
      O => r_V_7_0_1_fu_1511_p2(7)
    );
\tmp_54_reg_2827[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => src_kernel_win_0_va_3_fu_258(4),
      I1 => src_kernel_win_0_va_3_fu_258(2),
      I2 => src_kernel_win_0_va_3_fu_258(0),
      I3 => src_kernel_win_0_va_3_fu_258(1),
      I4 => src_kernel_win_0_va_3_fu_258(3),
      I5 => src_kernel_win_0_va_3_fu_258(5),
      O => \tmp_54_reg_2827[7]_i_2_n_2\
    );
\tmp_54_reg_2827_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => src_kernel_win_0_va_3_fu_258(0),
      Q => tmp_54_reg_2827(1),
      R => '0'
    );
\tmp_54_reg_2827_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => r_V_7_0_1_fu_1511_p2(2),
      Q => tmp_54_reg_2827(2),
      R => '0'
    );
\tmp_54_reg_2827_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => r_V_7_0_1_fu_1511_p2(3),
      Q => tmp_54_reg_2827(3),
      R => '0'
    );
\tmp_54_reg_2827_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => r_V_7_0_1_fu_1511_p2(4),
      Q => tmp_54_reg_2827(4),
      R => '0'
    );
\tmp_54_reg_2827_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => r_V_7_0_1_fu_1511_p2(5),
      Q => tmp_54_reg_2827(5),
      R => '0'
    );
\tmp_54_reg_2827_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => r_V_7_0_1_fu_1511_p2(6),
      Q => tmp_54_reg_2827(6),
      R => '0'
    );
\tmp_54_reg_2827_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => r_V_7_0_1_fu_1511_p2(7),
      Q => tmp_54_reg_2827(7),
      R => '0'
    );
\tmp_56_reg_2832[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => src_kernel_win_0_va_1_fu_250(0),
      I1 => src_kernel_win_0_va_1_fu_250(1),
      O => tmp_160_0_2_cast_fu_1539_p1(1)
    );
\tmp_56_reg_2832[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1E"
    )
        port map (
      I0 => src_kernel_win_0_va_1_fu_250(1),
      I1 => src_kernel_win_0_va_1_fu_250(0),
      I2 => src_kernel_win_0_va_1_fu_250(2),
      O => tmp_160_0_2_cast_fu_1539_p1(2)
    );
\tmp_56_reg_2832[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"01FE"
    )
        port map (
      I0 => src_kernel_win_0_va_1_fu_250(2),
      I1 => src_kernel_win_0_va_1_fu_250(0),
      I2 => src_kernel_win_0_va_1_fu_250(1),
      I3 => src_kernel_win_0_va_1_fu_250(3),
      O => tmp_160_0_2_cast_fu_1539_p1(3)
    );
\tmp_56_reg_2832[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFE"
    )
        port map (
      I0 => src_kernel_win_0_va_1_fu_250(3),
      I1 => src_kernel_win_0_va_1_fu_250(1),
      I2 => src_kernel_win_0_va_1_fu_250(0),
      I3 => src_kernel_win_0_va_1_fu_250(2),
      I4 => src_kernel_win_0_va_1_fu_250(4),
      O => tmp_160_0_2_cast_fu_1539_p1(4)
    );
\tmp_56_reg_2832[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000001FFFFFFFE"
    )
        port map (
      I0 => src_kernel_win_0_va_1_fu_250(4),
      I1 => src_kernel_win_0_va_1_fu_250(2),
      I2 => src_kernel_win_0_va_1_fu_250(0),
      I3 => src_kernel_win_0_va_1_fu_250(1),
      I4 => src_kernel_win_0_va_1_fu_250(3),
      I5 => src_kernel_win_0_va_1_fu_250(5),
      O => tmp_160_0_2_cast_fu_1539_p1(5)
    );
\tmp_56_reg_2832[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tmp_56_reg_2832[7]_i_2_n_2\,
      I1 => src_kernel_win_0_va_1_fu_250(6),
      O => tmp_160_0_2_cast_fu_1539_p1(6)
    );
\tmp_56_reg_2832[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1E"
    )
        port map (
      I0 => src_kernel_win_0_va_1_fu_250(6),
      I1 => \tmp_56_reg_2832[7]_i_2_n_2\,
      I2 => src_kernel_win_0_va_1_fu_250(7),
      O => tmp_160_0_2_cast_fu_1539_p1(7)
    );
\tmp_56_reg_2832[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => src_kernel_win_0_va_1_fu_250(4),
      I1 => src_kernel_win_0_va_1_fu_250(2),
      I2 => src_kernel_win_0_va_1_fu_250(0),
      I3 => src_kernel_win_0_va_1_fu_250(1),
      I4 => src_kernel_win_0_va_1_fu_250(3),
      I5 => src_kernel_win_0_va_1_fu_250(5),
      O => \tmp_56_reg_2832[7]_i_2_n_2\
    );
\tmp_56_reg_2832_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => src_kernel_win_0_va_1_fu_250(0),
      Q => tmp_56_reg_2832(0),
      R => '0'
    );
\tmp_56_reg_2832_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => tmp_160_0_2_cast_fu_1539_p1(1),
      Q => tmp_56_reg_2832(1),
      R => '0'
    );
\tmp_56_reg_2832_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => tmp_160_0_2_cast_fu_1539_p1(2),
      Q => tmp_56_reg_2832(2),
      R => '0'
    );
\tmp_56_reg_2832_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => tmp_160_0_2_cast_fu_1539_p1(3),
      Q => tmp_56_reg_2832(3),
      R => '0'
    );
\tmp_56_reg_2832_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => tmp_160_0_2_cast_fu_1539_p1(4),
      Q => tmp_56_reg_2832(4),
      R => '0'
    );
\tmp_56_reg_2832_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => tmp_160_0_2_cast_fu_1539_p1(5),
      Q => tmp_56_reg_2832(5),
      R => '0'
    );
\tmp_56_reg_2832_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => tmp_160_0_2_cast_fu_1539_p1(6),
      Q => tmp_56_reg_2832(6),
      R => '0'
    );
\tmp_56_reg_2832_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => p_Val2_10_0_0_2_reg_28170,
      D => tmp_160_0_2_cast_fu_1539_p1(7),
      Q => tmp_56_reg_2832(7),
      R => '0'
    );
\tmp_57_reg_2927[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ap_reg_pp0_iter3_or_cond_i_reg_2640,
      I1 => ap_block_pp0_stage0_subdone2_in,
      O => isneg_1_reg_29320
    );
\tmp_57_reg_2927_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => isneg_1_reg_29320,
      D => p_Val2_2_fu_1946_p2(8),
      Q => tmp_57_reg_2927(0),
      R => '0'
    );
\tmp_57_reg_2927_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => isneg_1_reg_29320,
      D => p_Val2_2_fu_1946_p2(9),
      Q => tmp_57_reg_2927(1),
      R => '0'
    );
\tmp_57_reg_2927_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => isneg_1_reg_29320,
      D => p_Val2_2_fu_1946_p2(10),
      Q => tmp_57_reg_2927(2),
      R => '0'
    );
\tmp_5_reg_555[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00262626"
    )
        port map (
      I0 => tmp_5_reg_555(0),
      I1 => ap_CS_fsm_state2,
      I2 => tmp_5_reg_555(1),
      I3 => ap_reg_grp_Filter2D_fu_96_ap_start,
      I4 => \ap_CS_fsm_reg_n_2_[0]\,
      O => \tmp_5_reg_555[0]_i_1_n_2\
    );
\tmp_5_reg_555[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"006A6A6A"
    )
        port map (
      I0 => tmp_5_reg_555(1),
      I1 => tmp_5_reg_555(0),
      I2 => ap_CS_fsm_state2,
      I3 => ap_reg_grp_Filter2D_fu_96_ap_start,
      I4 => \ap_CS_fsm_reg_n_2_[0]\,
      O => \tmp_5_reg_555[1]_i_1_n_2\
    );
\tmp_5_reg_555_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_5_reg_555[0]_i_1_n_2\,
      Q => tmp_5_reg_555(0),
      R => '0'
    );
\tmp_5_reg_555_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_5_reg_555[1]_i_1_n_2\,
      Q => tmp_5_reg_555(1),
      R => '0'
    );
\tmp_72_not_reg_2504[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0F0F0F0E0000000"
    )
        port map (
      I0 => t_V_reg_566(4),
      I1 => t_V_reg_566(5),
      I2 => t_V_reg_566(9),
      I3 => t_V_reg_566(6),
      I4 => t_V_reg_566(7),
      I5 => t_V_reg_566(8),
      O => tmp_72_not_fu_643_p2
    );
\tmp_72_not_reg_2504_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm(3),
      D => tmp_72_not_fu_643_p2,
      Q => tmp_72_not_reg_2504,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sobel is
  port (
    start_once_reg : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : out STD_LOGIC_VECTOR ( 0 to 0 );
    grp_Filter2D_fu_96_p_src_data_stream_2_V_read : out STD_LOGIC;
    \mOutPtr_reg[1]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \mOutPtr_reg[1]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \mOutPtr_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    shiftReg_ce : out STD_LOGIC;
    \ap_CS_fsm_reg[0]_0\ : out STD_LOGIC;
    \SRL_SIG_reg[0][7]\ : out STD_LOGIC;
    \SRL_SIG_reg[0][0]\ : out STD_LOGIC;
    \SRL_SIG_reg[0][1]\ : out STD_LOGIC;
    \SRL_SIG_reg[0][2]\ : out STD_LOGIC;
    \SRL_SIG_reg[0][3]\ : out STD_LOGIC;
    \SRL_SIG_reg[0][4]\ : out STD_LOGIC;
    \SRL_SIG_reg[0][5]\ : out STD_LOGIC;
    \SRL_SIG_reg[0][6]\ : out STD_LOGIC;
    \SRL_SIG_reg[0][7]_0\ : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_rst_n_inv : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    shiftReg_ce_0 : in STD_LOGIC;
    \mOutPtr_reg[1]_1\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \mOutPtr_reg[1]_2\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \mOutPtr_reg[1]_3\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    shiftReg_ce_1 : in STD_LOGIC;
    img1_data_stream_0_s_empty_n : in STD_LOGIC;
    img1_data_stream_2_s_empty_n : in STD_LOGIC;
    img1_data_stream_1_s_empty_n : in STD_LOGIC;
    img2_data_stream_0_s_full_n : in STD_LOGIC;
    img2_data_stream_1_s_full_n : in STD_LOGIC;
    img2_data_stream_2_s_full_n : in STD_LOGIC;
    start_for_CvtColor_1_U0_full_n : in STD_LOGIC;
    Sobel_U0_ap_start : in STD_LOGIC;
    \SRL_SIG_reg[0][7]_1\ : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sobel;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sobel is
  signal \^q\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \ap_CS_fsm_reg_n_2_[0]\ : STD_LOGIC;
  signal ap_reg_grp_Filter2D_fu_96_ap_start : STD_LOGIC;
  signal grp_Filter2D_fu_96_n_10 : STD_LOGIC;
  signal grp_Filter2D_fu_96_n_12 : STD_LOGIC;
  signal grp_Filter2D_fu_96_n_13 : STD_LOGIC;
  signal grp_Filter2D_fu_96_n_9 : STD_LOGIC;
  signal \^start_once_reg\ : STD_LOGIC;
  attribute FSM_ENCODING : string;
  attribute FSM_ENCODING of \ap_CS_fsm_reg[0]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[1]\ : label is "none";
begin
  Q(0) <= \^q\(0);
  start_once_reg <= \^start_once_reg\;
\ap_CS_fsm_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => grp_Filter2D_fu_96_n_10,
      Q => \ap_CS_fsm_reg_n_2_[0]\,
      S => ap_rst_n_inv
    );
\ap_CS_fsm_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => grp_Filter2D_fu_96_n_9,
      Q => \^q\(0),
      R => ap_rst_n_inv
    );
ap_reg_grp_Filter2D_fu_96_ap_start_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => grp_Filter2D_fu_96_n_13,
      Q => ap_reg_grp_Filter2D_fu_96_ap_start,
      R => ap_rst_n_inv
    );
grp_Filter2D_fu_96: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Filter2D
     port map (
      D(0) => D(0),
      E(0) => grp_Filter2D_fu_96_p_src_data_stream_2_V_read,
      Q(1) => \^q\(0),
      Q(0) => \ap_CS_fsm_reg_n_2_[0]\,
      \SRL_SIG_reg[0][0]\ => \SRL_SIG_reg[0][0]\,
      \SRL_SIG_reg[0][1]\ => \SRL_SIG_reg[0][1]\,
      \SRL_SIG_reg[0][2]\ => \SRL_SIG_reg[0][2]\,
      \SRL_SIG_reg[0][3]\ => \SRL_SIG_reg[0][3]\,
      \SRL_SIG_reg[0][4]\ => \SRL_SIG_reg[0][4]\,
      \SRL_SIG_reg[0][5]\ => \SRL_SIG_reg[0][5]\,
      \SRL_SIG_reg[0][6]\ => \SRL_SIG_reg[0][6]\,
      \SRL_SIG_reg[0][7]\ => \SRL_SIG_reg[0][7]\,
      \SRL_SIG_reg[0][7]_0\ => \SRL_SIG_reg[0][7]_0\,
      \SRL_SIG_reg[0][7]_1\(7 downto 0) => \SRL_SIG_reg[0][7]_1\(7 downto 0),
      Sobel_U0_ap_start => Sobel_U0_ap_start,
      \ap_CS_fsm_reg[0]_0\ => \ap_CS_fsm_reg[0]_0\,
      \ap_CS_fsm_reg[1]_0\(1) => grp_Filter2D_fu_96_n_9,
      \ap_CS_fsm_reg[1]_0\(0) => grp_Filter2D_fu_96_n_10,
      ap_clk => ap_clk,
      ap_reg_grp_Filter2D_fu_96_ap_start => ap_reg_grp_Filter2D_fu_96_ap_start,
      ap_reg_grp_Filter2D_fu_96_ap_start_reg => grp_Filter2D_fu_96_n_13,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      img1_data_stream_0_s_empty_n => img1_data_stream_0_s_empty_n,
      img1_data_stream_1_s_empty_n => img1_data_stream_1_s_empty_n,
      img1_data_stream_2_s_empty_n => img1_data_stream_2_s_empty_n,
      img2_data_stream_0_s_full_n => img2_data_stream_0_s_full_n,
      img2_data_stream_1_s_full_n => img2_data_stream_1_s_full_n,
      img2_data_stream_2_s_full_n => img2_data_stream_2_s_full_n,
      \mOutPtr_reg[0]\(0) => E(0),
      \mOutPtr_reg[0]_0\(0) => \mOutPtr_reg[0]\(0),
      \mOutPtr_reg[1]\(0) => \mOutPtr_reg[1]\(0),
      \mOutPtr_reg[1]_0\(0) => \mOutPtr_reg[1]_0\(0),
      \mOutPtr_reg[1]_1\(1 downto 0) => \mOutPtr_reg[1]_1\(1 downto 0),
      \mOutPtr_reg[1]_2\(1 downto 0) => \mOutPtr_reg[1]_2\(1 downto 0),
      \mOutPtr_reg[1]_3\(1 downto 0) => \mOutPtr_reg[1]_3\(1 downto 0),
      shiftReg_ce => shiftReg_ce,
      shiftReg_ce_0 => shiftReg_ce_0,
      shiftReg_ce_1 => shiftReg_ce_1,
      start_for_CvtColor_1_U0_full_n => start_for_CvtColor_1_U0_full_n,
      start_once_reg_reg => grp_Filter2D_fu_96_n_12,
      start_once_reg_reg_0 => \^start_once_reg\
    );
start_once_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => grp_Filter2D_fu_96_n_12,
      Q => \^start_once_reg\,
      R => ap_rst_n_inv
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_edge_detect is
  port (
    stream_in_TDATA : in STD_LOGIC_VECTOR ( 23 downto 0 );
    stream_in_TKEEP : in STD_LOGIC_VECTOR ( 2 downto 0 );
    stream_in_TSTRB : in STD_LOGIC_VECTOR ( 2 downto 0 );
    stream_in_TUSER : in STD_LOGIC_VECTOR ( 0 to 0 );
    stream_in_TLAST : in STD_LOGIC_VECTOR ( 0 to 0 );
    stream_in_TID : in STD_LOGIC_VECTOR ( 0 to 0 );
    stream_in_TDEST : in STD_LOGIC_VECTOR ( 0 to 0 );
    stream_out_TDATA : out STD_LOGIC_VECTOR ( 23 downto 0 );
    stream_out_TKEEP : out STD_LOGIC_VECTOR ( 2 downto 0 );
    stream_out_TSTRB : out STD_LOGIC_VECTOR ( 2 downto 0 );
    stream_out_TUSER : out STD_LOGIC_VECTOR ( 0 to 0 );
    stream_out_TLAST : out STD_LOGIC_VECTOR ( 0 to 0 );
    stream_out_TID : out STD_LOGIC_VECTOR ( 0 to 0 );
    stream_out_TDEST : out STD_LOGIC_VECTOR ( 0 to 0 );
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    stream_in_TVALID : in STD_LOGIC;
    stream_in_TREADY : out STD_LOGIC;
    stream_out_TVALID : out STD_LOGIC;
    stream_out_TREADY : in STD_LOGIC
  );
  attribute hls_module : string;
  attribute hls_module of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_edge_detect : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_edge_detect;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_edge_detect is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal AXIvideo2Mat_U0_img_data_stream_0_V_din : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal AXIvideo2Mat_U0_img_data_stream_1_V_din : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal AXIvideo2Mat_U0_img_data_stream_2_V_din : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal AXIvideo2Mat_U0_n_5 : STD_LOGIC;
  signal CvtColor_1_U0_ap_start : STD_LOGIC;
  signal CvtColor_1_U0_n_3 : STD_LOGIC;
  signal CvtColor_U0_ap_start : STD_LOGIC;
  signal CvtColor_U0_n_10 : STD_LOGIC;
  signal CvtColor_U0_n_11 : STD_LOGIC;
  signal CvtColor_U0_n_12 : STD_LOGIC;
  signal CvtColor_U0_n_13 : STD_LOGIC;
  signal CvtColor_U0_n_14 : STD_LOGIC;
  signal CvtColor_U0_n_15 : STD_LOGIC;
  signal CvtColor_U0_n_16 : STD_LOGIC;
  signal CvtColor_U0_n_3 : STD_LOGIC;
  signal CvtColor_U0_n_4 : STD_LOGIC;
  signal CvtColor_U0_n_6 : STD_LOGIC;
  signal CvtColor_U0_n_8 : STD_LOGIC;
  signal Mat2AXIvideo_U0_ap_start : STD_LOGIC;
  signal Mat2AXIvideo_U0_n_3 : STD_LOGIC;
  signal Mat2AXIvideo_U0_n_4 : STD_LOGIC;
  signal Mat2AXIvideo_U0_n_5 : STD_LOGIC;
  signal Sobel_U0_ap_start : STD_LOGIC;
  signal Sobel_U0_n_11 : STD_LOGIC;
  signal Sobel_U0_n_12 : STD_LOGIC;
  signal Sobel_U0_n_13 : STD_LOGIC;
  signal Sobel_U0_n_14 : STD_LOGIC;
  signal Sobel_U0_n_15 : STD_LOGIC;
  signal Sobel_U0_n_16 : STD_LOGIC;
  signal Sobel_U0_n_17 : STD_LOGIC;
  signal Sobel_U0_n_18 : STD_LOGIC;
  signal Sobel_U0_n_19 : STD_LOGIC;
  signal Sobel_U0_n_20 : STD_LOGIC;
  signal Sobel_U0_n_3 : STD_LOGIC;
  signal Sobel_U0_n_6 : STD_LOGIC;
  signal Sobel_U0_n_7 : STD_LOGIC;
  signal Sobel_U0_n_8 : STD_LOGIC;
  signal Sobel_U0_n_9 : STD_LOGIC;
  signal ap_CS_fsm_state2 : STD_LOGIC;
  signal ap_rst_n_inv : STD_LOGIC;
  signal grp_Filter2D_fu_96_p_src_data_stream_2_V_read : STD_LOGIC;
  signal img0_data_stream_0_s_dout : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal img0_data_stream_0_s_empty_n : STD_LOGIC;
  signal img0_data_stream_0_s_full_n : STD_LOGIC;
  signal img0_data_stream_1_s_dout : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal img0_data_stream_1_s_empty_n : STD_LOGIC;
  signal img0_data_stream_1_s_full_n : STD_LOGIC;
  signal img0_data_stream_2_s_dout : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal img0_data_stream_2_s_empty_n : STD_LOGIC;
  signal img0_data_stream_2_s_full_n : STD_LOGIC;
  signal img1_data_stream_0_s_U_n_4 : STD_LOGIC;
  signal img1_data_stream_0_s_U_n_5 : STD_LOGIC;
  signal img1_data_stream_0_s_dout : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal img1_data_stream_0_s_empty_n : STD_LOGIC;
  signal img1_data_stream_0_s_full_n : STD_LOGIC;
  signal img1_data_stream_1_s_U_n_4 : STD_LOGIC;
  signal img1_data_stream_1_s_U_n_5 : STD_LOGIC;
  signal img1_data_stream_1_s_empty_n : STD_LOGIC;
  signal img1_data_stream_1_s_full_n : STD_LOGIC;
  signal img1_data_stream_2_s_U_n_4 : STD_LOGIC;
  signal img1_data_stream_2_s_U_n_5 : STD_LOGIC;
  signal img1_data_stream_2_s_empty_n : STD_LOGIC;
  signal img1_data_stream_2_s_full_n : STD_LOGIC;
  signal img2_data_stream_0_s_U_n_3 : STD_LOGIC;
  signal img2_data_stream_0_s_dout : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal img2_data_stream_0_s_full_n : STD_LOGIC;
  signal img2_data_stream_1_s_empty_n : STD_LOGIC;
  signal img2_data_stream_1_s_full_n : STD_LOGIC;
  signal img2_data_stream_2_s_empty_n : STD_LOGIC;
  signal img2_data_stream_2_s_full_n : STD_LOGIC;
  signal img3_data_stream_0_s_U_n_4 : STD_LOGIC;
  signal img3_data_stream_0_s_U_n_5 : STD_LOGIC;
  signal img3_data_stream_0_s_dout : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal img3_data_stream_0_s_empty_n : STD_LOGIC;
  signal img3_data_stream_0_s_full_n : STD_LOGIC;
  signal img3_data_stream_1_s_U_n_4 : STD_LOGIC;
  signal img3_data_stream_1_s_U_n_5 : STD_LOGIC;
  signal img3_data_stream_1_s_dout : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal img3_data_stream_1_s_empty_n : STD_LOGIC;
  signal img3_data_stream_1_s_full_n : STD_LOGIC;
  signal img3_data_stream_2_s_dout : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal img3_data_stream_2_s_empty_n : STD_LOGIC;
  signal img3_data_stream_2_s_full_n : STD_LOGIC;
  signal internal_empty_n4_out : STD_LOGIC;
  signal shiftReg_ce : STD_LOGIC;
  signal shiftReg_ce_1 : STD_LOGIC;
  signal shiftReg_ce_3 : STD_LOGIC;
  signal start_for_CvtColoocq_U_n_4 : STD_LOGIC;
  signal start_for_CvtColor_1_U0_full_n : STD_LOGIC;
  signal start_for_CvtColor_U0_full_n : STD_LOGIC;
  signal start_for_Mat2AXIvideo_U0_full_n : STD_LOGIC;
  signal start_for_Sobel_U0_full_n : STD_LOGIC;
  signal start_once_reg : STD_LOGIC;
  signal start_once_reg_0 : STD_LOGIC;
  signal start_once_reg_2 : STD_LOGIC;
  signal start_once_reg_4 : STD_LOGIC;
  signal tmp_99_fu_294_p3 : STD_LOGIC;
begin
  stream_out_TDEST(0) <= \<const0>\;
  stream_out_TID(0) <= \<const0>\;
  stream_out_TKEEP(2) <= \<const1>\;
  stream_out_TKEEP(1) <= \<const1>\;
  stream_out_TKEEP(0) <= \<const1>\;
  stream_out_TSTRB(2) <= \<const0>\;
  stream_out_TSTRB(1) <= \<const0>\;
  stream_out_TSTRB(0) <= \<const0>\;
AXIvideo2Mat_U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AXIvideo2Mat
     port map (
      D(7 downto 0) => AXIvideo2Mat_U0_img_data_stream_2_V_din(7 downto 0),
      \SRL_SIG_reg[0][7]\(7 downto 0) => AXIvideo2Mat_U0_img_data_stream_1_V_din(7 downto 0),
      \SRL_SIG_reg[0][7]_0\(7 downto 0) => AXIvideo2Mat_U0_img_data_stream_0_V_din(7 downto 0),
      \SRL_SIG_reg[1][0]\ => AXIvideo2Mat_U0_n_5,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      img0_data_stream_0_s_full_n => img0_data_stream_0_s_full_n,
      img0_data_stream_1_s_full_n => img0_data_stream_1_s_full_n,
      img0_data_stream_2_s_full_n => img0_data_stream_2_s_full_n,
      start_for_CvtColor_U0_full_n => start_for_CvtColor_U0_full_n,
      start_once_reg => start_once_reg,
      stream_in_TDATA(23 downto 0) => stream_in_TDATA(23 downto 0),
      stream_in_TLAST(0) => stream_in_TLAST(0),
      stream_in_TREADY => stream_in_TREADY,
      stream_in_TUSER(0) => stream_in_TUSER(0),
      stream_in_TVALID => stream_in_TVALID
    );
CvtColor_1_U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_CvtColor_1
     port map (
      CvtColor_1_U0_ap_start => CvtColor_1_U0_ap_start,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      internal_empty_n_reg => img2_data_stream_0_s_U_n_3,
      shiftReg_ce => shiftReg_ce,
      start_for_Mat2AXIvideo_U0_full_n => start_for_Mat2AXIvideo_U0_full_n,
      start_once_reg => start_once_reg_0,
      start_once_reg_reg_0 => CvtColor_1_U0_n_3
    );
CvtColor_U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_CvtColor
     port map (
      B(7 downto 0) => img0_data_stream_2_s_dout(7 downto 0),
      CvtColor_U0_ap_start => CvtColor_U0_ap_start,
      D(7 downto 0) => img0_data_stream_1_s_dout(7 downto 0),
      E(0) => CvtColor_U0_n_3,
      Q(7) => tmp_99_fu_294_p3,
      Q(6) => CvtColor_U0_n_10,
      Q(5) => CvtColor_U0_n_11,
      Q(4) => CvtColor_U0_n_12,
      Q(3) => CvtColor_U0_n_13,
      Q(2) => CvtColor_U0_n_14,
      Q(1) => CvtColor_U0_n_15,
      Q(0) => CvtColor_U0_n_16,
      \SRL_SIG_reg[0][7]\ => CvtColor_U0_n_8,
      \SRL_SIG_reg[0][7]_0\(7 downto 0) => img0_data_stream_0_s_dout(7 downto 0),
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      \exitcond_reg_420_reg[0]\ => AXIvideo2Mat_U0_n_5,
      img0_data_stream_0_s_empty_n => img0_data_stream_0_s_empty_n,
      img0_data_stream_1_s_empty_n => img0_data_stream_1_s_empty_n,
      img0_data_stream_2_s_empty_n => img0_data_stream_2_s_empty_n,
      img1_data_stream_0_s_full_n => img1_data_stream_0_s_full_n,
      img1_data_stream_1_s_full_n => img1_data_stream_1_s_full_n,
      img1_data_stream_2_s_full_n => img1_data_stream_2_s_full_n,
      internal_empty_n4_out => internal_empty_n4_out,
      internal_full_n_reg => CvtColor_U0_n_4,
      shiftReg_ce => shiftReg_ce_1,
      start_for_Sobel_U0_full_n => start_for_Sobel_U0_full_n,
      start_once_reg => start_once_reg_2,
      start_once_reg_reg_0 => CvtColor_U0_n_6
    );
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
Mat2AXIvideo_U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mat2AXIvideo
     port map (
      D(23 downto 16) => img3_data_stream_2_s_dout(7 downto 0),
      D(15 downto 8) => img3_data_stream_1_s_dout(7 downto 0),
      D(7 downto 0) => img3_data_stream_0_s_dout(7 downto 0),
      E(0) => Mat2AXIvideo_U0_n_3,
      Mat2AXIvideo_U0_ap_start => Mat2AXIvideo_U0_ap_start,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      img3_data_stream_0_s_empty_n => img3_data_stream_0_s_empty_n,
      img3_data_stream_1_s_empty_n => img3_data_stream_1_s_empty_n,
      img3_data_stream_2_s_empty_n => img3_data_stream_2_s_empty_n,
      \mOutPtr_reg[1]\ => Mat2AXIvideo_U0_n_4,
      \mOutPtr_reg[1]_0\ => Mat2AXIvideo_U0_n_5,
      shiftReg_ce => shiftReg_ce,
      stream_out_TDATA(23 downto 0) => stream_out_TDATA(23 downto 0),
      stream_out_TLAST(0) => stream_out_TLAST(0),
      stream_out_TREADY => stream_out_TREADY,
      stream_out_TUSER(0) => stream_out_TUSER(0),
      stream_out_TVALID => stream_out_TVALID
    );
Sobel_U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sobel
     port map (
      D(0) => Sobel_U0_n_3,
      E(0) => Sobel_U0_n_8,
      Q(0) => ap_CS_fsm_state2,
      \SRL_SIG_reg[0][0]\ => Sobel_U0_n_13,
      \SRL_SIG_reg[0][1]\ => Sobel_U0_n_14,
      \SRL_SIG_reg[0][2]\ => Sobel_U0_n_15,
      \SRL_SIG_reg[0][3]\ => Sobel_U0_n_16,
      \SRL_SIG_reg[0][4]\ => Sobel_U0_n_17,
      \SRL_SIG_reg[0][5]\ => Sobel_U0_n_18,
      \SRL_SIG_reg[0][6]\ => Sobel_U0_n_19,
      \SRL_SIG_reg[0][7]\ => Sobel_U0_n_12,
      \SRL_SIG_reg[0][7]_0\ => Sobel_U0_n_20,
      \SRL_SIG_reg[0][7]_1\(7 downto 0) => img1_data_stream_0_s_dout(7 downto 0),
      Sobel_U0_ap_start => Sobel_U0_ap_start,
      \ap_CS_fsm_reg[0]_0\ => Sobel_U0_n_11,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      grp_Filter2D_fu_96_p_src_data_stream_2_V_read => grp_Filter2D_fu_96_p_src_data_stream_2_V_read,
      img1_data_stream_0_s_empty_n => img1_data_stream_0_s_empty_n,
      img1_data_stream_1_s_empty_n => img1_data_stream_1_s_empty_n,
      img1_data_stream_2_s_empty_n => img1_data_stream_2_s_empty_n,
      img2_data_stream_0_s_full_n => img2_data_stream_0_s_full_n,
      img2_data_stream_1_s_full_n => img2_data_stream_1_s_full_n,
      img2_data_stream_2_s_full_n => img2_data_stream_2_s_full_n,
      \mOutPtr_reg[0]\(0) => Sobel_U0_n_9,
      \mOutPtr_reg[1]\(0) => Sobel_U0_n_6,
      \mOutPtr_reg[1]_0\(0) => Sobel_U0_n_7,
      \mOutPtr_reg[1]_1\(1) => img1_data_stream_2_s_U_n_4,
      \mOutPtr_reg[1]_1\(0) => img1_data_stream_2_s_U_n_5,
      \mOutPtr_reg[1]_2\(1) => img1_data_stream_1_s_U_n_4,
      \mOutPtr_reg[1]_2\(0) => img1_data_stream_1_s_U_n_5,
      \mOutPtr_reg[1]_3\(1) => img1_data_stream_0_s_U_n_4,
      \mOutPtr_reg[1]_3\(0) => img1_data_stream_0_s_U_n_5,
      shiftReg_ce => shiftReg_ce_3,
      shiftReg_ce_0 => shiftReg_ce_1,
      shiftReg_ce_1 => shiftReg_ce,
      start_for_CvtColor_1_U0_full_n => start_for_CvtColor_1_U0_full_n,
      start_once_reg => start_once_reg_4
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
img0_data_stream_0_s_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A
     port map (
      D(7 downto 0) => AXIvideo2Mat_U0_img_data_stream_0_V_din(7 downto 0),
      E(0) => CvtColor_U0_n_3,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      \exitcond_reg_420_reg[0]\(0) => AXIvideo2Mat_U0_n_5,
      img0_data_stream_0_s_empty_n => img0_data_stream_0_s_empty_n,
      img0_data_stream_0_s_full_n => img0_data_stream_0_s_full_n,
      internal_empty_n4_out => internal_empty_n4_out,
      \tmp_101_reg_364_reg[7]\(7 downto 0) => img0_data_stream_0_s_dout(7 downto 0),
      \tmp_20_reg_355_reg[0]\ => CvtColor_U0_n_4
    );
img0_data_stream_1_s_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_0
     port map (
      D(7 downto 0) => img0_data_stream_1_s_dout(7 downto 0),
      E(0) => CvtColor_U0_n_3,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      \axi_data_V_1_reg_236_reg[15]\(7 downto 0) => AXIvideo2Mat_U0_img_data_stream_1_V_din(7 downto 0),
      \exitcond_reg_420_reg[0]\(0) => AXIvideo2Mat_U0_n_5,
      img0_data_stream_1_s_empty_n => img0_data_stream_1_s_empty_n,
      img0_data_stream_1_s_full_n => img0_data_stream_1_s_full_n,
      internal_empty_n4_out => internal_empty_n4_out,
      \tmp_20_reg_355_reg[0]\ => CvtColor_U0_n_4
    );
img0_data_stream_2_s_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_1
     port map (
      B(7 downto 0) => img0_data_stream_2_s_dout(7 downto 0),
      D(7 downto 0) => AXIvideo2Mat_U0_img_data_stream_2_V_din(7 downto 0),
      E(0) => CvtColor_U0_n_3,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      \exitcond_reg_420_reg[0]\ => AXIvideo2Mat_U0_n_5,
      img0_data_stream_2_s_empty_n => img0_data_stream_2_s_empty_n,
      img0_data_stream_2_s_full_n => img0_data_stream_2_s_full_n,
      internal_empty_n4_out => internal_empty_n4_out,
      \tmp_20_reg_355_reg[0]\ => CvtColor_U0_n_4
    );
img1_data_stream_0_s_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_2
     port map (
      D(0) => Sobel_U0_n_7,
      E(0) => Sobel_U0_n_8,
      Q(0) => ap_CS_fsm_state2,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      grp_Filter2D_fu_96_p_src_data_stream_2_V_read => grp_Filter2D_fu_96_p_src_data_stream_2_V_read,
      img1_data_stream_0_s_empty_n => img1_data_stream_0_s_empty_n,
      img1_data_stream_0_s_full_n => img1_data_stream_0_s_full_n,
      \p_Val2_15_reg_394_reg[7]\(7) => tmp_99_fu_294_p3,
      \p_Val2_15_reg_394_reg[7]\(6) => CvtColor_U0_n_10,
      \p_Val2_15_reg_394_reg[7]\(5) => CvtColor_U0_n_11,
      \p_Val2_15_reg_394_reg[7]\(4) => CvtColor_U0_n_12,
      \p_Val2_15_reg_394_reg[7]\(3) => CvtColor_U0_n_13,
      \p_Val2_15_reg_394_reg[7]\(2) => CvtColor_U0_n_14,
      \p_Val2_15_reg_394_reg[7]\(1) => CvtColor_U0_n_15,
      \p_Val2_15_reg_394_reg[7]\(0) => CvtColor_U0_n_16,
      \r_V_1_reg_389_reg[29]\ => CvtColor_U0_n_8,
      \reg_588_reg[7]\(1) => img1_data_stream_0_s_U_n_4,
      \reg_588_reg[7]\(0) => img1_data_stream_0_s_U_n_5,
      \reg_588_reg[7]_0\(7 downto 0) => img1_data_stream_0_s_dout(7 downto 0),
      shiftReg_ce => shiftReg_ce_1
    );
img1_data_stream_1_s_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_3
     port map (
      D(0) => Sobel_U0_n_6,
      E(0) => Sobel_U0_n_8,
      Q(0) => ap_CS_fsm_state2,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      grp_Filter2D_fu_96_p_src_data_stream_2_V_read => grp_Filter2D_fu_96_p_src_data_stream_2_V_read,
      img1_data_stream_1_s_empty_n => img1_data_stream_1_s_empty_n,
      img1_data_stream_1_s_full_n => img1_data_stream_1_s_full_n,
      \mOutPtr_reg[1]_0\(1) => img1_data_stream_1_s_U_n_4,
      \mOutPtr_reg[1]_0\(0) => img1_data_stream_1_s_U_n_5,
      shiftReg_ce => shiftReg_ce_1
    );
img1_data_stream_2_s_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_4
     port map (
      D(0) => Sobel_U0_n_3,
      E(0) => Sobel_U0_n_8,
      Q(0) => ap_CS_fsm_state2,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      grp_Filter2D_fu_96_p_src_data_stream_2_V_read => grp_Filter2D_fu_96_p_src_data_stream_2_V_read,
      img1_data_stream_2_s_empty_n => img1_data_stream_2_s_empty_n,
      img1_data_stream_2_s_full_n => img1_data_stream_2_s_full_n,
      \mOutPtr_reg[1]_0\(1) => img1_data_stream_2_s_U_n_4,
      \mOutPtr_reg[1]_0\(0) => img1_data_stream_2_s_U_n_5,
      shiftReg_ce => shiftReg_ce_1
    );
img2_data_stream_0_s_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_5
     port map (
      E(0) => Sobel_U0_n_9,
      \ap_CS_fsm_reg[3]\ => img2_data_stream_0_s_U_n_3,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      img2_data_stream_0_s_dout(7 downto 0) => img2_data_stream_0_s_dout(7 downto 0),
      img2_data_stream_0_s_full_n => img2_data_stream_0_s_full_n,
      img2_data_stream_1_s_empty_n => img2_data_stream_1_s_empty_n,
      img2_data_stream_2_s_empty_n => img2_data_stream_2_s_empty_n,
      img3_data_stream_0_s_full_n => img3_data_stream_0_s_full_n,
      img3_data_stream_1_s_full_n => img3_data_stream_1_s_full_n,
      img3_data_stream_2_s_full_n => img3_data_stream_2_s_full_n,
      \p_Val2_1_reg_2922_reg[0]\ => Sobel_U0_n_13,
      \p_Val2_1_reg_2922_reg[1]\ => Sobel_U0_n_14,
      \p_Val2_1_reg_2922_reg[2]\ => Sobel_U0_n_15,
      \p_Val2_1_reg_2922_reg[3]\ => Sobel_U0_n_16,
      \p_Val2_1_reg_2922_reg[4]\ => Sobel_U0_n_17,
      \p_Val2_1_reg_2922_reg[5]\ => Sobel_U0_n_18,
      \p_Val2_1_reg_2922_reg[6]\ => Sobel_U0_n_19,
      \p_Val2_1_reg_2922_reg[7]\ => Sobel_U0_n_20,
      shiftReg_ce => shiftReg_ce,
      shiftReg_ce_0 => shiftReg_ce_3,
      \tmp_57_reg_2927_reg[2]\ => Sobel_U0_n_12
    );
img2_data_stream_1_s_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_6
     port map (
      E(0) => Sobel_U0_n_9,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      img2_data_stream_1_s_empty_n => img2_data_stream_1_s_empty_n,
      img2_data_stream_1_s_full_n => img2_data_stream_1_s_full_n,
      shiftReg_ce => shiftReg_ce,
      shiftReg_ce_0 => shiftReg_ce_3
    );
img2_data_stream_2_s_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_7
     port map (
      E(0) => Sobel_U0_n_9,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      img2_data_stream_2_s_empty_n => img2_data_stream_2_s_empty_n,
      img2_data_stream_2_s_full_n => img2_data_stream_2_s_full_n,
      shiftReg_ce => shiftReg_ce,
      shiftReg_ce_0 => shiftReg_ce_3
    );
img3_data_stream_0_s_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_8
     port map (
      E(0) => Mat2AXIvideo_U0_n_3,
      Q(1) => img3_data_stream_0_s_U_n_4,
      Q(0) => img3_data_stream_0_s_U_n_5,
      \ap_CS_fsm_reg[2]\ => Mat2AXIvideo_U0_n_4,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      img3_data_stream_0_s_empty_n => img3_data_stream_0_s_empty_n,
      img3_data_stream_0_s_full_n => img3_data_stream_0_s_full_n,
      shiftReg_ce => shiftReg_ce
    );
img3_data_stream_1_s_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_9
     port map (
      E(0) => Mat2AXIvideo_U0_n_3,
      Q(1) => img3_data_stream_1_s_U_n_4,
      Q(0) => img3_data_stream_1_s_U_n_5,
      \ap_CS_fsm_reg[2]\ => Mat2AXIvideo_U0_n_4,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      img3_data_stream_1_s_empty_n => img3_data_stream_1_s_empty_n,
      img3_data_stream_1_s_full_n => img3_data_stream_1_s_full_n,
      shiftReg_ce => shiftReg_ce
    );
img3_data_stream_2_s_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_w8_d1_A_10
     port map (
      D(23 downto 16) => img3_data_stream_2_s_dout(7 downto 0),
      D(15 downto 8) => img3_data_stream_1_s_dout(7 downto 0),
      D(7 downto 0) => img3_data_stream_0_s_dout(7 downto 0),
      E(0) => Mat2AXIvideo_U0_n_3,
      Q(1) => img3_data_stream_1_s_U_n_4,
      Q(0) => img3_data_stream_1_s_U_n_5,
      \ap_CS_fsm_reg[2]\ => Mat2AXIvideo_U0_n_4,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      img2_data_stream_0_s_dout(7 downto 0) => img2_data_stream_0_s_dout(7 downto 0),
      img3_data_stream_2_s_empty_n => img3_data_stream_2_s_empty_n,
      img3_data_stream_2_s_full_n => img3_data_stream_2_s_full_n,
      \mOutPtr_reg[1]_0\(1) => img3_data_stream_0_s_U_n_4,
      \mOutPtr_reg[1]_0\(0) => img3_data_stream_0_s_U_n_5,
      shiftReg_ce => shiftReg_ce
    );
start_for_CvtColoocq_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_CvtColoocq
     port map (
      CvtColor_U0_ap_start => CvtColor_U0_ap_start,
      \ap_CS_fsm_reg[1]\ => CvtColor_U0_n_6,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      \mOutPtr_reg[1]_0\ => start_for_CvtColoocq_U_n_4,
      start_for_CvtColor_U0_full_n => start_for_CvtColor_U0_full_n,
      start_for_Sobel_U0_full_n => start_for_Sobel_U0_full_n,
      start_once_reg => start_once_reg_2,
      start_once_reg_0 => start_once_reg
    );
start_for_CvtColopcA_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_CvtColopcA
     port map (
      CvtColor_1_U0_ap_start => CvtColor_1_U0_ap_start,
      Sobel_U0_ap_start => Sobel_U0_ap_start,
      \ap_CS_fsm_reg[1]\ => CvtColor_1_U0_n_3,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      start_for_CvtColor_1_U0_full_n => start_for_CvtColor_1_U0_full_n,
      start_once_reg => start_once_reg_4
    );
start_for_Mat2AXIqcK_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Mat2AXIqcK
     port map (
      CvtColor_1_U0_ap_start => CvtColor_1_U0_ap_start,
      Mat2AXIvideo_U0_ap_start => Mat2AXIvideo_U0_ap_start,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      internal_empty_n_reg_0 => Mat2AXIvideo_U0_n_5,
      start_for_Mat2AXIvideo_U0_full_n => start_for_Mat2AXIvideo_U0_full_n,
      start_once_reg => start_once_reg_0
    );
start_for_Sobel_U0_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_start_for_Sobel_U0
     port map (
      CvtColor_U0_ap_start => CvtColor_U0_ap_start,
      Sobel_U0_ap_start => Sobel_U0_ap_start,
      \ap_CS_fsm_reg[1]\ => Sobel_U0_n_11,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      ap_rst_n_inv => ap_rst_n_inv,
      internal_empty_n_reg_0 => start_for_CvtColoocq_U_n_4,
      start_for_Sobel_U0_full_n => start_for_Sobel_U0_full_n,
      start_once_reg => start_once_reg_2
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    stream_in_TVALID : in STD_LOGIC;
    stream_in_TREADY : out STD_LOGIC;
    stream_in_TDATA : in STD_LOGIC_VECTOR ( 23 downto 0 );
    stream_in_TKEEP : in STD_LOGIC_VECTOR ( 2 downto 0 );
    stream_in_TSTRB : in STD_LOGIC_VECTOR ( 2 downto 0 );
    stream_in_TUSER : in STD_LOGIC_VECTOR ( 0 to 0 );
    stream_in_TLAST : in STD_LOGIC_VECTOR ( 0 to 0 );
    stream_in_TID : in STD_LOGIC_VECTOR ( 0 to 0 );
    stream_in_TDEST : in STD_LOGIC_VECTOR ( 0 to 0 );
    stream_out_TVALID : out STD_LOGIC;
    stream_out_TREADY : in STD_LOGIC;
    stream_out_TDATA : out STD_LOGIC_VECTOR ( 23 downto 0 );
    stream_out_TKEEP : out STD_LOGIC_VECTOR ( 2 downto 0 );
    stream_out_TSTRB : out STD_LOGIC_VECTOR ( 2 downto 0 );
    stream_out_TUSER : out STD_LOGIC_VECTOR ( 0 to 0 );
    stream_out_TLAST : out STD_LOGIC_VECTOR ( 0 to 0 );
    stream_out_TID : out STD_LOGIC_VECTOR ( 0 to 0 );
    stream_out_TDEST : out STD_LOGIC_VECTOR ( 0 to 0 );
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "system_edge_detect_0_0,edge_detect,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "edge_detect,Vivado 2017.4";
  attribute hls_module : string;
  attribute hls_module of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of ap_clk : signal is "xilinx.com:signal:clock:1.0 ap_clk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of ap_clk : signal is "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF stream_in:stream_out, ASSOCIATED_RESET ap_rst_n, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1";
  attribute X_INTERFACE_INFO of ap_rst_n : signal is "xilinx.com:signal:reset:1.0 ap_rst_n RST";
  attribute X_INTERFACE_PARAMETER of ap_rst_n : signal is "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {RST {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}";
  attribute X_INTERFACE_INFO of stream_in_TREADY : signal is "xilinx.com:interface:axis:1.0 stream_in TREADY";
  attribute X_INTERFACE_INFO of stream_in_TVALID : signal is "xilinx.com:interface:axis:1.0 stream_in TVALID";
  attribute X_INTERFACE_INFO of stream_out_TREADY : signal is "xilinx.com:interface:axis:1.0 stream_out TREADY";
  attribute X_INTERFACE_INFO of stream_out_TVALID : signal is "xilinx.com:interface:axis:1.0 stream_out TVALID";
  attribute X_INTERFACE_INFO of stream_in_TDATA : signal is "xilinx.com:interface:axis:1.0 stream_in TDATA";
  attribute X_INTERFACE_INFO of stream_in_TDEST : signal is "xilinx.com:interface:axis:1.0 stream_in TDEST";
  attribute X_INTERFACE_PARAMETER of stream_in_TDEST : signal is "XIL_INTERFACENAME stream_in, TDATA_NUM_BYTES 3, TDEST_WIDTH 1, TID_WIDTH 1, TUSER_WIDTH 1, LAYERED_METADATA undef, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1";
  attribute X_INTERFACE_INFO of stream_in_TID : signal is "xilinx.com:interface:axis:1.0 stream_in TID";
  attribute X_INTERFACE_INFO of stream_in_TKEEP : signal is "xilinx.com:interface:axis:1.0 stream_in TKEEP";
  attribute X_INTERFACE_INFO of stream_in_TLAST : signal is "xilinx.com:interface:axis:1.0 stream_in TLAST";
  attribute X_INTERFACE_INFO of stream_in_TSTRB : signal is "xilinx.com:interface:axis:1.0 stream_in TSTRB";
  attribute X_INTERFACE_INFO of stream_in_TUSER : signal is "xilinx.com:interface:axis:1.0 stream_in TUSER";
  attribute X_INTERFACE_INFO of stream_out_TDATA : signal is "xilinx.com:interface:axis:1.0 stream_out TDATA";
  attribute X_INTERFACE_INFO of stream_out_TDEST : signal is "xilinx.com:interface:axis:1.0 stream_out TDEST";
  attribute X_INTERFACE_PARAMETER of stream_out_TDEST : signal is "XIL_INTERFACENAME stream_out, TDATA_NUM_BYTES 3, TDEST_WIDTH 1, TID_WIDTH 1, TUSER_WIDTH 1, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} TDATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 24} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TDATA_WIDTH 24 TUSER {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TUSER_WIDTH 1}, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 150000000, PHASE 0.0, CLK_DOMAIN system_clk_wiz_0_0_clk_out1";
  attribute X_INTERFACE_INFO of stream_out_TID : signal is "xilinx.com:interface:axis:1.0 stream_out TID";
  attribute X_INTERFACE_INFO of stream_out_TKEEP : signal is "xilinx.com:interface:axis:1.0 stream_out TKEEP";
  attribute X_INTERFACE_INFO of stream_out_TLAST : signal is "xilinx.com:interface:axis:1.0 stream_out TLAST";
  attribute X_INTERFACE_INFO of stream_out_TSTRB : signal is "xilinx.com:interface:axis:1.0 stream_out TSTRB";
  attribute X_INTERFACE_INFO of stream_out_TUSER : signal is "xilinx.com:interface:axis:1.0 stream_out TUSER";
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_edge_detect
     port map (
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      stream_in_TDATA(23 downto 0) => stream_in_TDATA(23 downto 0),
      stream_in_TDEST(0) => stream_in_TDEST(0),
      stream_in_TID(0) => stream_in_TID(0),
      stream_in_TKEEP(2 downto 0) => stream_in_TKEEP(2 downto 0),
      stream_in_TLAST(0) => stream_in_TLAST(0),
      stream_in_TREADY => stream_in_TREADY,
      stream_in_TSTRB(2 downto 0) => stream_in_TSTRB(2 downto 0),
      stream_in_TUSER(0) => stream_in_TUSER(0),
      stream_in_TVALID => stream_in_TVALID,
      stream_out_TDATA(23 downto 0) => stream_out_TDATA(23 downto 0),
      stream_out_TDEST(0) => stream_out_TDEST(0),
      stream_out_TID(0) => stream_out_TID(0),
      stream_out_TKEEP(2 downto 0) => stream_out_TKEEP(2 downto 0),
      stream_out_TLAST(0) => stream_out_TLAST(0),
      stream_out_TREADY => stream_out_TREADY,
      stream_out_TSTRB(2 downto 0) => stream_out_TSTRB(2 downto 0),
      stream_out_TUSER(0) => stream_out_TUSER(0),
      stream_out_TVALID => stream_out_TVALID
    );
end STRUCTURE;
