# Issue with Vivado 


--> Probably linked to TCL mode 


## Remove auto configuration of Zybo board 


- Go to the config folder of Vivado and remove the initialisation script 


        cd ~/.Xilinx/Vivado/2017.4/
        rm Vivado_init.tcl 
        cd 


- close and re-open a new terminal 



## Launch Vivado the old way 


- Go to the directory of source file of GIT 

        gerzague@barn-e-01:~/Documents/3a_sysnum_soc_labs$ pwd
        /users/archi/gerzague/Documents/3a_sysnum_soc_labs
        gerzague@barn-e-01:~/Documents/3a_sysnum_soc_labs$ ls
        init.tcl               Lab1_EmbeddedSystem  Lab3_RealTimeVideo  launch_vivado.sh  Vivado_init.tcl  vivado.log
        Lab0_InitiationVivado  Lab2_Partitionning   Lab4_FIR_HW_SW      README.md         vivado.jou       zybo-z7-10
        gerzague@barn-e-01:~/Documents/3a_sysnum_soc_labs$


- Source the configuration file for Vivado 

        source /usr/local/Vivado-2017.4/Vivado/2017.4/settings64.sh


- Launch Vivado 

        vivado & 


- Add the Zybo board configuration to Vivado. In Vivado TCL terminal, do the following commands 


        set boards_path "./zybo-z7-10/"
        set_param board.repoPaths ${boards_path}


